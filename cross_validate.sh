#!/usr/bin/env bash

for i in `seq 0 9`; do
    cat "../DBSS_DATA/experiments/cross_validation_alternative/sample_$i" | \
    sed 's!.*!../DBSS_DATA/data/allcandids/&.compact!g' | \
    parallel -j 30 ./helpers/cross_validate.sh {} "../DBSS_DATA/experiments/cross_validation/classfiier_fold_$i.pickle"
done
