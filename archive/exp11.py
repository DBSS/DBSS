#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import os

from dbss import config
from dbss.core import sentence, token
from dbss.core.simplifier import simplify, take_n_best, remove_punct
from dbss.plugins.ranker.mlranker.mlranker import MLRanker
from dbss.plugins.lfg_parser import xle

my_ranker = MLRanker()


## Sentence
def deflate(original, simplified):
    """Return sentence id followed by id's of tokens deleted from it."""
    tokens_of_simplified = {token.get_id(t.id) for t in simplified}
    tokens_of_original = [token.get_id(t.id) for t in original]
    return sentence.get_id(original) + '\t' + ' '.join(id for id in tokens_of_original if id not in tokens_of_simplified)

def test_deflate():
    assert deflate(sentence.S_1,
                   next(simplify(sentence.S_1,
                                 'OneTokenShorterNonRecursive',
                                 'DoNotSort',
                                 1))) ==\
           '1\t1'


## Iterator Filehandle -> Iterator
def log_deflate(iterator, file):
    """Write all generated sentences to a file in compact form."""
    for orig, simpler in iterator:
        file.write(deflate(orig, simpler) + '\n')
        yield orig, simpler


## Void -> Void
def main():
    for orig in sentence.read_sentences(sys.stdin):
        allcandids_file = 'data/allcandids/' + sentence.get_id(orig) + '.compact'
        if not os.path.exists(os.path.dirname(allcandids_file)):
            os.makedirs(os.path.dirname(allcandids_file))

        with open(allcandids_file, 'w') as acf:
            ten_best = list(take_n_best(10,
                                        my_ranker.score((orig, simpler, []) for orig, simpler in
                                                        log_deflate(((orig, simpler) for simpler in simplify(orig,
                                                                             'OneSubtreeShorterRecursive',
                                                                             'DoNotSort',
                                                                             config.DEFAULT_MAX_NUMBER_OF_CANDIDATES)),
                                                                    acf)),

                                        lambda orig_simpler_score: -orig_simpler_score[2]))

            if ten_best:
                no_punct = [(ten_best[0][0], remove_punct(ten_best[0][1]), ten_best[0][2])]
                candids = no_punct + ten_best

                for i, orig_simpler_score in enumerate(candids):
                    orig, simpler, score = orig_simpler_score
                    simpler_plain = sentence.s2string(simpler)
                    isparsed = xle.xle_gives_complete_parse(simpler_plain)
                    print(sentence.get_id(orig), '\t',
                          simpler_plain, '\t',
                          score, '\t',
                          isparsed)
                    if isparsed:
                        for orig, simpler, score in candids[i + 1:]:
                            print(sentence.get_id(orig), '\t',
                                  sentence.s2string(simpler), '\t',
                                  score, '\t',
                                  'unknown')
                        break

            else:
                print(sentence.get_id(orig), '\t',
                      None, '\t',
                      None, '\t',
                      None)


if __name__ == "__main__":
    main()
