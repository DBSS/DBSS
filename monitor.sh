#!/usr/bin/env bash

parsed=$(grep -c "true" ../DBSS_DATA/experiments/cross_validation_alternative/results.pseudojson)
total=$(wc -l ../DBSS_DATA/experiments/cross_validation_alternative/results.pseudojson | grep -o "[0-9]\+")

echo "$parsed"
echo "$total"

echo $(calc $parsed / $total)

