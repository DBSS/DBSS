Parsed 	 All 	 Percentage 	 Experiment
1 	 2 	 0.5 	 ./acceptance-tests/T_Evaluator/Scenario1/ParsedSentences
4188 	 9160 	 ~0.45720524017467248908 	 ./experiments/1_OneSubtreeShorter/ParsedSentences
4714 	 9160 	 ~0.51462882096069868996 	 ./experiments/2_10Shortest+ShortestWithoutPunctuation/ParsedSentences
5265 	 9160 	 ~0.57478165938864628821 	 ./experiments/3_CombinationOf1And2/ParsedSentences
3548 	 9160 	 ~0.38733624454148471616 	 ./experiments/4_10Longest+LongestWithoutPunctuation/ParsedSentences
4657 	 9160 	 ~0.50840611353711790393 	 ./experiments/5_CombinationOf1And4/ParsedSentences
5650 	 9160 	 ~0.61681222707423580786 	 ./experiments/6_OneTokenShorter/ParsedSentences
4490 	 9160 	 ~0.49017467248908296943 	 ./experiments/7_10WithLeastParsableSubtreesRemoved+FirstCandidateWithoutPunctuation/ParsedSentences

Experiment: 1_OneSubtreeShorter
Parsed 4188 out of 9160 (45.72052401746725%)

On average:
  0.0 possible candidates per sentence
  4.87882096069869 candidates explored before at most n solutions were found

In case of sentences which received a successful parse after
simplification, on average:
  3.522922636103152 candidates explored before finding n successful parses.

Average length of successfully parsed simplified candidates: 18.094078319006687 tokens.

real	2521m18.123s
real	55m58.256s
real	348m55.559s
real	2605m33.564s

--------------------------------------------
Experiment: 2_10Shortest+ShortestWithoutPunctuation
Parsed 4714 out of 9160 (51.462882096069876%)

On average:
  0.0 possible candidates per sentence
  4.823908296943231 candidates explored before at most n solutions were found

In case of sentences which received a successful parse after
simplification, on average:
  2.040729741196436 candidates explored before finding n successful parses.

Average length of successfully parsed simplified candidates: 7.501909206618583 tokens.

real	808m25.070s
real	769m27.184s
real	41m51.073s
real	194m28.553s
real	105m19.564s
real	0m11.980s
real	56m23.467s
real	51m37.536s
real	2737m42.034s
real	41m56.878s
real	101m59.113s
real	19m50.949s
real	0m10.278s

--------------------------------------------
Experiment: 3_CombinationOf1And2
Parsed 5265 out of 9160 (57.478165938864635%)

On average:
  0.0 possible candidates per sentence
  0.0 candidates explored before at most n solutions were found

In case of sentences which received a successful parse after
simplification, on average:
  0.0 candidates explored before finding n successful parses.

Average length of successfully parsed simplified candidates: 15.51965811965812 tokens.


--------------------------------------------
Experiment: 4_10Longest+LongestWithoutPunctuation
Parsed 3548 out of 9160 (38.733624454148476%)

On average:
  0.0 possible candidates per sentence
  6.394213973799126 candidates explored before at most n solutions were found

In case of sentences which received a successful parse after
simplification, on average:
  3.197012401352875 candidates explored before finding n successful parses.

Average length of successfully parsed simplified candidates: 17.89177001127396 tokens.

real	99m19.913s
real	1199m10.793s
real	22m44.993s
real	244m41.417s
real	1357m26.361s
real	423m13.433s
real	339m43.089s
real	82m47.384s

--------------------------------------------
Experiment: 5_CombinationOf1And4
Parsed 4657 out of 9160 (50.84061135371179%)

On average:
  0.0 possible candidates per sentence
  0.0 candidates explored before at most n solutions were found

In case of sentences which received a successful parse after
simplification, on average:
  0.0 candidates explored before finding n successful parses.

Average length of successfully parsed simplified candidates: 18.480566888554865 tokens.


--------------------------------------------
Experiment: 6_OneTokenShorter
Parsed 5650 out of 9160 (61.68122270742358%)

On average:
  0.0 possible candidates per sentence
  15.522816593886462 candidates explored before at most n solutions were found

In case of sentences which received a successful parse after
simplification, on average:
  9.81221238938053 candidates explored before finding n successful parses.

Average length of successfully parsed simplified candidates: 20.789026548672567 tokens.

real	4967m19.118s

--------------------------------------------
Experiment: 7_10WithLeastParsableSubtreesRemoved+FirstCandidateWithoutPunctuation
Parsed 4490 out of 9160 (49.0174672489083%)

On average:
  0.0 possible candidates per sentence
  5.825873362445415 candidates explored before at most n solutions were found

In case of sentences which received a successful parse after
simplification, on average:
  3.5853006681514477 candidates explored before finding n successful parses.

Average length of successfully parsed simplified candidates: 16.35033407572383 tokens.

real	5626m57.687s

--------------------------------------------
