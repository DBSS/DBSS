% http://www.ijcla.bahripublications.com/format.html
\documentclass[11pt]{llncs}

\usepackage[a4paper,twoside=false,top=2.32in,bottom=2.69in,left=2.25in,right=2in]{geometry}
\usepackage{times}
\usepackage{epsf}
\usepackage{amsmath}
\usepackage{array}
\newcolumntype{L}[1]{>{\raggedright\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}
\newcolumntype{C}[1]{>{\centering\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}
\newcolumntype{R}[1]{>{\raggedleft\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}
\usepackage{todonotes}
\usepackage{gb4e}
\usepackage{rotating}
\usepackage{tikz-dependency}
\usepackage{hyperref}
\usepackage[sort, numbers]{natbib}
\usepackage{rotating}
\input{ps-fig.tex} % why is this needed?

\begin{document}

\title{Dependency-based~Sentence~Simplification
       for~Large-scale~LFG~Parsing: ~Selecting~Simplified~Candidates
       for~Efficiency~and~Coverage}
\author{Ilnar Salimzianov and \"Ozlem \c{C}etino\u{g}lu}
\institute{University of Stuttgart \\
           Institute for Natural Language Processing \\
           ilnar.salimzianov@ims.uni-stuttgart.de, ozlem@ims.uni-stuttgart.de}
\maketitle

\begin{abstract}

Large scale LFG grammars achieve high coverages on corpus data, yet can fail to give a full analysis for each sentence. One approach proposed to gain at least the argument structure of those failed sentences is to simplify them by deleting subtrees from their dependency structure (provided by a more robust statistical dependency parser). The simplified versions are then re-parsed to receive a full analysis. However, the number of simplified sentences this approach generates is infeasible for parsing. As a solution, only a subset of candidates is selected based on a metric. In this work we apply the so-called parsability metric \cite{vanNoord2004}, introduced as an error-mining technique for grammar writing, for selecting among simplified candidates to be parsed and show that we improve over the previous results that use sentence length as the selection metric. 
\end{abstract}

\section{Introduction}
\label{sec:intro}

Hand-crafted Lexical Functional Grammars (LFGs) can provide deeper analyses for sentences when they are able to parse them, but usually are less robust than statistical parsers and cannot give full-fledged solutions for 100\% of sentences. Of course it is possible to increase the coverage of an LFG grammar by adding more rules or by relaxing existing ones. But such modifications are, firstly, labor-intensive and time-consuming, and secondly, they require a high level of linguistic expertise. Thirdly, the sentences to be parsed could be in non-standard orthography or grammar. Hence we would prefer an automatic way of dealing with sentences an LFG grammar failed to parse. This paper is on one of the ways to deal with such sentences, namely on dependency-based sentence simplification.

Figure \ref{fig:example} illustrates a sentence in German from the TIGER corpus \cite{Brants2004tiger} German ParGram grammar failed to fully  parse\footnote{The example was taken from \cite{CetinogluEtAl2013}.}. The problem with the sentence is that the article \textit{des} `of the' and the adjective \textit{japanischen} `Japanese' are both in genitive case, indicating that that they are part of a genitive construction, but the noun \textit{Au{\ss}enministerium} `foreign ministry' is in nominative case and therefore does not agree with the surrounding words. Because of that the German ParGram grammar outputs only a fragmented analysis for the sentence as shown in Figure \ref{fig:c-struct}.

Fragmented analysis is what XLE\footnote{XLE \cite{maxwell1996} is a 
% In this paper https://dl.acm.org/citation.cfm?id=1073129
% and this paper https://dl.acm.org/citation.cfm?id=1118786
% authors cite maxwellkaplan1993 for XLE, although surprisingly that paper
% does not mention XLE at all.
framework for parsing and generating with an LFG grammar.} delivers when it cannot produce a complete parse. That is, no rule would allow connecting the fragments found. This is not enough for our research purposes, in part because fragments identified are often incorrect. E.g., as Figure \ref{fig:c-struct} shows, the problematic phrase \textit{des japanischen Au{\ss}enministerium} ended up in two separate phrases. The functional structure\footnote{LFG theory \cite{bresnan2015lexical} distinguishes between two core syntactic structures -- c-structure (constituent structure) and f-structure (functional structure). C-structures are constituent or phrase structure trees. F-structures are sets of attribute-value pairs; attributes may be features, such as tense or gender, or functions, such as subject or object.} is similarly affected and \textit{des japanischen} does not modify the noun \textit{Au{\ss}enministerium} as it would normally do. Thus, sentences with fragments are lost in terms of getting information from the analyses. 

\begin{figure}
\centering
\begin{exe}
\ex	
\gll \textit{Ein} \textit{Sprecher} \textbf{\textit{des}} \textbf{\textit{japanischen}} \textbf{\textit{Au{\ss}enministerium}} \textit{verk\"undete} \textit{daraufhin} \textit{,} \textit{man} \textit{werde} \textit{Jelzins} \textit{Aussage} \textit{``} \textit{vorsichtig} \textit{analysieren} \textit{''} \textit{,} \textit{bevor} \textit{man} \textit{sie} \textit{kommentiere} \textit{,} \textit{aber} \textit{:} \\
A speaker {of the} japanese {foreign ministry} proclaimed then , one would Yelzin's statement `` carefully analyze '' , before one it comment , but : \\
\trans `A speaker \textbf{of the Japanese foreign ministry} then proclaimed that Yeltsin's
statement would be `` carefully analyzed '' , before commenting on it , but :'
\end{exe}
  \caption{A sentence which the German ParGram grammar failed to parse. The problem is in the genitive phrase \textit{des japanischen Au{\ss}enministerium}, where the noun is missing the genitive ending \textit{-s} and hence does not agree with the article and adjective in case.}
  \label{fig:example}
\end{figure}

\begin{sidewaysfigure}
  \centering
    \includegraphics[scale=0.5]{cstruct}
  \caption{The c-structure German ParGram Grammar provides for the sentence in Figure \ref{fig:example} (to focus on the problematic part we only show part of the tree).} 
  \label{fig:c-struct}
\end{sidewaysfigure}

\c{C}etino\u{g}lu et al. \cite{CetinogluEtAl2013} propose a dependency-based sentence simplification approach to achive a full parse of a simplified version, where at least the arguments of the original sentence are kept. Sentences with no full parses are simplified by deleting one or more subtrees, and each shorter sentence is a candidate to reparse and get a full analysis. However, reparsing all candidates is not a feasible approach, as the number of candidates gets too high for long sentences with the combination of multiple subtree options to be deleted. To overcome this problem, the authors take 10 shortest simplified sentences as their candidates for full parses. Although this method proves that for more than half of the cases, the full analysis for the argument structure of failed parses could be obtained, it has a drawback: the system does not give an opportunity to longer candidates where the arguments \textit{and} other constituents might be kept and parsed into a full analyses. In this work we keep the argument preservation criterion and we propose a more informed metric to select the candidates that lead to higher coverages with longer simplified sentences.

A few words have to be said on what we mean by coverage. Original sentence is considered ``covered" if at least one of its simplified variants receives a full parse. However, our aim is not to get a full parse for just any of the simplified candidates, but for candidates that are as long as possible.  

The remainder of the paper proceeds as follows. In section \ref{sec:dbss} we describe in more detail the dependency-based sentence simplification, as proposed by \cite{CetinogluEtAl2013} and the problem with a number of candidates infeasible for parsing this approach generates. In section \ref{sec:pmetric} we define the parsability metric which will be used for selecting candidates likely to receive a parse. In section \ref{sec:experiments} we describe the experiments we conducted. We discuss the experimental results in section \ref{sec:discussion} and give a short overview of other methods for increasing the coverage of LFG grammars in section \ref{sec:relatedWork}. We conclude with section \ref{sec:conclusion}

\section{Dependency-based Sentence Simplification}
\label{sec:dbss}

The idea of dependency-based sentence simplification is as follows: sentences which LFG parser failed to fully parse are parsed with a more robust statistical dependency parser. Having a dependency tree for the sentence allows to identify subtrees which are considered to be deletable. A subtree is defined to be deletable if removing it would not lead to a change in the arguments structure. For instance, subjects are not deleteble, whereas adjuncts are. The list of deletable subtrees is prepared manually based on heuristics, the actual list is language and treebank dependent. Once the subtrees are deleted from the dependency tree, the shorter candidates are reparsed with the LFG parser.

Figure \ref{fig:deptree} shows a part of the dependency parse tree for the sentence in Figure \ref{fig:example}. Despite the mistake on the noun \textit{Au{\ss}enministerium}, \textit{des} and \textit{japanischen} correctly modifiy the noun as noun kernels (NK) and the whole phrase is identified as the genitive adjunct (AG) of the noun \textit{Sprecher} `speaker'. 

\begin{figure}
  \centering
    \includegraphics[scale=0.31]{deptree}
  \caption{Part of the dependency tree for the sentence in Figure \ref{fig:example}. The tagset from the TIGER Corpus \cite{Brants2004tiger} is used as the edge labels.}
  \label{fig:deptree}
\end{figure}

%\begin{center}
%	\begin{dependency}[theme = simple, font = \small]
%    	\begin{deptext}[column sep=0.08cm]
%        $\langle$ROOT$\rangle$ \& Ein \& Sprecher \& des \& japanischen \& Au{\ss}enministerium \& verk\"undete \\
%    	\end{deptext}
%		\depedge{3}{2}{nmod}
%        \depedge{1}{2}{case}
%        \depedge{4}{3}{dobj}
%        \deproot[edge unit distance=1.3ex]{4}{root}
%    \end{dependency}
%\end{center}

The way the deletion of subtrees works is the following: if the edge label of the node is in the list of deletable labels, then the node itself and all its descendants are deleted from the tree. Edge labels of the heads of deletable subtrees for German are shown in Table \ref{tab:deletable-subtrees}. So, for the parse tree shown in Figure \ref{fig:deptree}, the following subtrees would be deletable: \textit{japanischen} and \textit{des japanischen Au{\ss}enministerium}.

\begin{table}
\caption{Edge labels of the heads of deletable subtrees. Tags are the tags used in the TIGER corpus \cite{Brants2004tiger}.}
\label{tab:deletable-subtrees}
\begin{center}
\begin{tabular}{ll}
\hline
AG  & genitive adjuncts \\
APP & appositions \\
JU & discourse-marker like \\
MNR & PP adjuncts (in noun-phrases) \\
MO & modifiers \\
NG & negation \\
PAR & head of parenthesis \\
PG & possessive PP adjuncts \\
PH & placeholders (e.g. German Vorfeld \textit{es}) \\
PNC & proper noun components \\
RC & relative clauses \\
RE & infinite clauses attached to nominals \\
SBP & PP subjects in passive \\
UC & inside foreign language phrases \\
VO & vocatives \\
NK & noun kernels \\
DA & datives \\
\hline 
\end{tabular}
\end{center}
\end{table}

A careful reader might have noticed that the subtree covering the word \textit{des} `of the' was not deleted from the tree although its edge label -- NK -- is in the list. This is because the edge label NK is subject to additional conditions for being deleted, namely it is deletable only when it functions as an adjunct (see Figure \ref{fig:function1} for the implementation).

% TODO: might be too detailed, re-consider later

\begin{figure}[h]
\hrule
{\strut\footnotesize \bf Function 1: Produce True if the token (read: the node) is the head of a deletable subtree.} 
\hrule
\scriptsize
\begin{verbatim}
## Token Sentence -> Boolean
def head_of_deletable_subtree(t, s):
    """
    Produce True if token is the head of
    a deletable subtree of the sentence.
    """
    if t.deprel in DELETABLE_SUBTREES:
        if t.deprel == 'NK':
            if t.pos in ['ADJA', 'ADJD', 'ADV', 'KOUS']:
                return True
            elif t.pos == 'NN' and \
                    s[int(t.head) - 1].pos == 'NN':
                return True
            else:
                return False
        else:
            return True
    else:
        return False
\end{verbatim}
\normalsize
\hrule
\caption{A function (in Python 3) for deciding whether the node is the head of a deletable subtree.}
\label{fig:function1}
\end{figure}

If all possible combinations of deletable subtrees are deleted from the original sentence, the number of  generated candidates is infeasible for parsing. \c{C}etino\u{g}lu et al. \cite{CetinogluEtAl2013} report it to reach up to 608255 candidates per sentence while the average number is 924. The authors address this problem in two ways. In the first approach, they delete only one subtree at a time from the sentence (`one subtree shorter'). In the second approach, all possible combinations of subtrees are deleted, and only 10 shortest candidates plus the shortest candidate without punctuation symbols are taken (`ten shortest').

\section{Parsability Metric}
\label{sec:pmetric}

Parsing one subtree shorter or ten shortest candidates improves the coverage of fully parsed candidates. However, it does not give a chance to longer hence more informative candidates that would also receive full parses.

It is easier to see the reason why this is the case when 10 shortest candidates are taken compared to the `one subtree shorter' setting. In the latter case we are deleting at most one subtree to produce the candidate, and we might think that candidates produced would be longest possible. However, subtrees can yield n-grams of different length (and we measure length of sentences in terms of tokens) or two or more subtrees may yield n-grams of the same length. Maybe there are longer sentences which would also receive a full parse or two or more candidates of equal length with one being more likely to receive a full parse than the other(s)? % TODO: Good (points are valid), but paraphrase

This work is about exploring metrics other than the length to select simplified candidates to be parsed. For this purpose, we use van Noord's parsability metric\cite{vanNoord2004}. Van Noord used the metric for the purposes of error detection in grammar writing. In one of their experiments, \cite{CetinogluEtAl2013} use the metric to identify and then delete least parsable n-grams from problematic sentences.  Nonetheless, that is not the best way to simplify sentences since deleting n-grams only according to their parsability does not guarantee the grammaticality of the remaining sentence or preservation of the core arguments. 

Parsability score of an n-gram is defined as follows:

$$ parsability(\text{n-gram}) = \frac{count(\text{n-gram occurs in parsed sentences)}}{count(\text{n-gram occurs in *all* sentences)}}$$

We prioritize the removal of subtrees with zero or very low parsability scores to narrow down the candidate space.

The parsability score table we work with contains scores for 1, 2 or 3-grams, but no longer than that. If the subtree yields an n-gram longer than 3 tokens, or there are no scores for that particular n-gram in the table, we take the minimum of the parsability scores of lower order n-grams it contains. If there is no score for 1-gram in the parsability table, we set its parsability score to 1.0. Here is how parsability score of an n-gram is defined formally:

\begin{verbatim}
if ngram in ParsabilityTable:
    score = ParsabilityTable[ngram]
elif len(ngram) == 1:
    score = 1.0
else:
    min(scores of n-1-grams ngram contains)
\end{verbatim}

In other words, the algorithm deletes subtrees for which no parsability score is known latest, if at all.

We hypothesize that by deleting least parsable subtrees first we can recover a number of sentences comparable to that reported by \cite{CetinogluEtAl2013} while finding longer solutions.

\section{Experimental Setup}
\label{sec:experiments}
In this section we first give the data and tools we use in our experiments and then continue with the description of different experiments we conducted.

\subsection{General Setup}

We use TIGER treebank \cite{Brants2004tiger} as the source of German sentences which German LFG parser fails to parse but for which a gold standard dependency tree is available. We use the dependency version of the TIGER treebank which was converted by \cite{seeker2012making}.

The number of failed sentences is 9160. We use the same version used in \cite{CetinogluEtAl2013} of the German ParGram grammar\cite{rohrer2006improving}.
% Your comment about the paragraph above was: "The version of the grammar is the same, the dataset is the same -- how come the numbers are different? We have to explain that".
% I (Ilnar) just had taken the sentences in ozlem/research/GermanLFG/Tiger2DepData/tiger_train_easyPunct_xleFail.list as that was the most recent list of failed sentences.
Although the dataset and grammar versions are the same, the number of sentences XLE fails to parse fully is 9373 in \cite{CetinogluEtAl2013}. Our reasoning is that either the newer version of XLE we used in these experiments brought improvements which lead to a higher coverage or the hardware (e.g. memory size) that experiments ran on played a role.

For a fair comparison between previous and new approaches, we start the next section by reproducing the experiments from \cite{CetinogluEtAl2013}. The reconducted experiments serve as baseline numbers for our setting.

\subsection{Reproducing the Results of (\c{C}etino\u{g}lu et al., 2013)}

\subsubsection*{One Subtree Shorter}

In the first experiment, only one deletable subtree at a time is deleted from the original sentence. The number of possible candidates equals the number of deletable subtrees in the original sentence.

\subsubsection*{10 Shortest + ShortestNoPunct}

Sometimes the problem which prevents the sentence from being parsed involves several subtrees. To account for that, in this experiment we delete all possible combinations of subtrees from the sentence and then take the 10 shortest candidates. From the shortest candidate, we remove punctuation symbols and include the result as the eleventh candidate.

\subsection{New Experiments}

\subsubsection*{10 Longest + LongestNoPunct}

Following our goal of finding parses for sentences as long as possible, we take 10 longest candidates out of the set of all possible candidates. The longest candidate without punctuation symbols is included as the eleventh candidate.

The motivation for this experiment is twofold. First, we would like to see how naively taking longest candidates without applying the parsability metric for selecting candidates affects coverage. Second, intuitively, taking longest candidates should lead to longest solutions. This experiment is an intuitive upper bound for the average length of successfully parsed candidates.

\subsubsection*{10 Without Least Parsable Subtrees + FirstNoPunct}

In this experiment, up to ten (depending on how many deletable subtrees there are in the original sentence) subtrees are deleted, starting with least parsable ones. Punctuation symbols are removed from the first candidate, and this version is included as the eleventh candidate.

\bigskip

We delete punctuation symbols from one of the candidates because we think that sometimes they can cause the parser to fail to parse a sentence. Punctuation symbols are deleted from the first candidate generated. In the `10 Shortest + ShortestNoPunct' and `10 Longest + LongestNoPunct' experiments these are the shortest and the longest candidates, respectively. In the `10 Without Least Parsable Subtrees + FirstNoPunct' experiment, this is the sentence after deleting the least parsable subtree from it.

The reason for deleting the punctuation symbols from the first candidate was initially mere technical, but the result lines up with the motivation behind experiments. `10 Shortest + ShortestNoPunct' can be seen as the pessimistic approach -- we start with the shortest candidate possible, which we get by deleting punctuation from the shortest candidate. In the `10 Longest + LongestNoPunct' experiment, our goal is to preserve as much information as possible, and punctuation, putting examples like ``A woman, without her man, is nothing'' vs ``A woman: without her, man is nothing'' aside, arguably contains least amount of information. In the `10 Without Least Parsable Subtrees + FirstNoPunct' the motivation is again to preserve as much information as possible and therefore it is a good idea to start by deleting punctuation.

However, deleting punctuation symbols always from the shortest candidate might have been a better approach.

\section{Experimental Results}
\label{sec:discussion}

Table \ref{tab:results} presents results for all experiments and some of their combinations. In the second column of it, coverage results are shown. By coverage we mean receiving a full parse after simplification. In addition, the average length of fully parsed simplified sentences (FPSS), calculated by \textit{count}(tokens in FPSS)  / \textit{count}(FPSS) and the average number of candidates explored before a solution is found are given.

Note that since \textit{count}(FPSS) is different for each of the experiments (the number in the second column of Table \ref{tab:results}), experiments are not strictly comparable against each other in terms of of the average length of solutions they generate. In particular, the `OneSubtreeShorter' experiment led to solutions on average longer than the `10Longest + LongestNoPunct' experiment. This is because the `OneSubtreeShorter' experiment found solutions for more input sentences than `10Longest + LongestNoPunct' setting did, and thus we are calculating the average length of two different sets of sentences. If we consider only the input sentences which received a solution in both of the approaches, then the average length of fully parsed sentences in `OneSubtreeShorter' is 17.46, and in `10Longest + LongestNoPunct' it is 19.16 tokens.

%\begin{equation}
%\label{eq:avglength}
%\text{avg. length of FPSS} = \frac{count(\text{tokens in FPSS)}}{count(\text{FPSS)}}
%\end{equation}


When evaluating effectiveness of a combination of two approaches, if two solutions were found, only the longest solution was considered.


%This is because the number of candidates considered in the first experiment can be more than 10, i.e. the state space is larger and therefore the chance of finding a solution is higher. There can be cases when the `10Longest + LongestNoPunct' setting does not lead to a solution but the `OneSubtreeShorter' setting does. % When no solution is found, the length of the solution is set to 0, and thus the average solution length gets smaller.
% Your comment was: "so what is the average when 0 long sentences are excluded?"
% In fact, my explanation involving 0 length was wrong. I looked at the evaluation script, and the average length of the solutions (i.e. of fully parsed sentences) is calculated in the following way:
% <...snip...>
% print('Average length of successfully parsed simplified candidates: '
%      + str(tokens_in_parsed_sents / parsed_sents) + ' tokens.\n')
% <...snip...>
% i.e., only the parsed sentences are considered. So it has nothing to do 0, we simply evaluate them on two different sets of input sentences.
% To compare the approaches against each other, we need to evaluate them on input sentences which received solutions in *both* approaches (of course, solutions themselves might be different).
% TODO: my wording is lame
%In other words, we are evaluating the average length of fully parsed sentences on two different sets of sentences.
%If we consider only the input sentences which received a solution in \textbf{both} of the approaches, then the average length of fully parsed sentences in `OneSubtreeShorter' is 17.46, and in `10Longest + LongestNoPunct' it is 19.16 tokens.

\begin{table}
\caption{Number of original sentences which are fully parsed after simplification, average length of fully parsed simplified sentences and average number of candidates explored before the first full parse is found.}
\label{tab:results}
\centering
\begin{tabular}{ c  L{3cm}  R{2.1cm}  R{2.1cm}  R{2.1cm} }
  \hline
  & \textbf{Experiment} & \textbf{Sent. parsed after simplification} & \textbf{Avg. length of fully parsed simplified sent. (tokens)} & \textbf{Avg. \# of candidates explored} \\
  \hline
  \textbf{1} & OneSubtreeShorter & 4188 (45.72\%) & 18.09 & 4.87  \\
  \hline
  \textbf{2} & 10Shortest + ShortestNoPunct & 4714 (51.46\%) & 7.50 & 4.82 \\
  \hline
  \textbf{3} & Combination of 1 and 2 & 5265 (57.48\%) & 15.52 & -- \\
  \hline
  \textbf{4} & 10Longest + LongestNoPunct & 3548 (38.73\%) & 17.89 & 6.39 \\
  \hline
  \textbf{5} & Combination of 1 and 4 & 4657 (50.84\%) & 18.48 & -- \\
  \hline
  \textbf{6} & 10WithoutLeastParsable + FirstNoPunct & 4490 (49.02\%) & 16.35 & 5.83 \\
  \hline
  \textbf{7} & Combination of 1 and 6 & 4648 (50.74\%) & 17.20 & -- \\
  \hline
\end{tabular}
\end{table}

The parsability metric proposed by \cite{vanNoord2004} as the error mining technique for hand-coded grammars proves to be useful for selecting among simplified versions of a sentence to be parsed.

\begin{itemize}
\item So far, taking 10 shortest candidates from the set of candidates which can be generated by deleting all possible combinations of subtrees (experiment 2) allowed to recover the highest number of original sentences (51.46\%),
\item while taking the combination of 10 longest and one subtree shorter (experiment 5) on average expectedly led to longest solutions.  
\item Deleting 10 subtrees yielding least parsable n-grams in a greedy manner (experiment 6) delivers solutions comparable in length to that obtained by considering 10 longest candidates per each sentence (experiment 4), but allows to re-parse almost as many sentences as in the `10 shortest' setting (experiment 2). 
\end{itemize}

\section{Related Work}
\label{sec:relatedWork}

Several other researchers have worked on achieving high coverage scores with LFG grammars. For English, Riezler et al. \cite{riezler2002parsing} report a full coverage on the Wall Street Journal, by including fragmented and skimmed analyses in the results. For German, Rohrer et al. \cite{rohrer2006improving} employ the same techniques and parse the TIGER Treebank. Additionally they modify the German ParGram Grammar to better handle troublesome phenomena such as coordination, subject gaps, reported speech clauses, and parentheticals. Dost and King \cite{dost2009using} go beyond the standard treebank sentences to test the robustness and coverage of the ParGram English Grammar. They choose Wikipedia articles as their test medium to discover lexical and syntactic shortcomings, and use their findings for improving the grammar coverage.

%Achieving high parsing coverage of LFG grammars has been in the scope of sev-
%eral researchers. Riezler et al. (2002) parse the Wall Street Journal with the En-
%glish ParGram Grammar. They reach 100% coverage at the expense of fragmented
%and skimmed analyses. Rohrer and Forst (2006) also make use of skimming and
%fragmenting in parsing the TIGER Treebank with the German ParGram Grammar.
%Besides these standard mechanisms, they implement additional rules for linguis-
%tic phenomena that cause non-full analyses. Coordination, parentheticals, subject
%gaps, reported speech clauses are among the refined constructions. Dost and King

%(2009) parse and analyse a large set of Wikipedia articles to determine lexical and
%syntactic gaps in the ParGram English grammar, and improve the grammar cover-
%age by incorporating their findings.

Cahill et al. \cite{cahill2004long} guarantee a high coverage by using a statistical constituency parser to obtain the c-structure of a sentence, and then annotating the nodes of the c-structure with f-structure constrains and applying a constraint solver. Despite that the c-structure coverage is 100\% thanks to the robust statistical parser, not all sentences can have an f-structure due to the constraint solver. Moreover f-structure outputs carry much less information than usual ParGram grammar outputs. Hautli et al. \cite{hautli2010closing} close this gap by enriching f-structures, yet cannot achieve a full coverage.

%An alternative approach for handling coverage problems is to generate f-struc-
%tures by annotating statistical phrase-structure parser output with f-structure constraints and by solving those constraints (Cahill et al., 2004). After all, the statistical parsers they are based on are robust. Although the approach is applied to several languages, only the English system’s output comes close to XLE f-structure duplicates, which also cannot reach 100% coverage (Hautli et al., 2010).

Although sentence simplification is not commonly used in LFG grammars for coverage purposes, there has been research on sentence simplification itself. Riezler et al. \cite{riezler2003statistical} employ transfer rules to simplify English computer news articles, paying attention to meaning preservation. They apply transfer rules to parsed f-structures and obtain reduced versions. The reduced f-structures are then disambiguated and passed into the generator to get shorter versions. The transfer rules used in this system are described in more detail in \cite{crouch2004exploiting}.

%Sentence simplification in LFG systems has been studied before (Riezler et al.,
%2003; Crouch et al., 2004), in a different context than ours, paying attention to
%meaning preservation. Riezler et al. (2003) carry out sentence simplification on
%English computer news articles by converting parsed f-structures to reduced ones
%with transfer rules. They then disambiguate and generate from reduced f-structures
%to obtain shorter sentences. Crouch et al. (2004) further describes the type of rules used in the transfer system.

%Although utilising dependencies in sentence simplification is a novel approach
%in LFG research, dependency-based sentence simplification is an accepted method,
%often used to extract the important information out of the data in applications such as summarisation (Vanderwende et al., 2006) and spoken language understanding
%(Tür et al., 2011). Vanderwende et al. (2006) pays attention to grammaticality
%of simplified sentences whereas Tür et al. (2011) can ignore it, as the simplified
%dependency structures are used as features in a statistical classifier. Both systems work on English data.

%On the German front, there has been research on deep parsing systems that
%utilise dependencies, mainly by developing hybrid approaches. Schiehlen (2003)
%parses the NEGRA Treebank with a combination of a shallow approach based on
%machine learning and a cascaded finite state parser. Frank et al. (2003) bring to-
%gether a topological fields parser with a wide-coverage HPSG parser. Our system
%differs from those in making use of dependencies in sentence simplification deci-
%sions.

The most similar work to ours is a recent paper from \cite{przepiorkowskisupporting}, where authors rely on dependency relations obtained from an independent data-driven dependency parser to compose fragment f-structures for parsable parts of Polish sentences into a full f-structure. This approach is not applicable to us as the fragment f-structures might be incorrect themselves in the German ParGram Grammar, as discussed in section \ref{sec:intro} and shown in Figure \ref{fig:example} (although on the figure you see the c-structure for the sentence, not the f-structure, f-structure is affected in a similar way). However, authors claim that ``in contrast to English, where partial parsing is used to parse incorrect sentences, Polish sentences with FRAGMENT parses are not necessarily incorrect'' and that ``many of them are well-formed but contain linguistic phenomena for which POLFIE\footnote{Polish LFG grammar.} rules have not been defined yet''. One might argue that such cases where fragments themselves are wrong are indeed infrequent.

% Relevant bits from the paper:

% 1)
%This view of FRAGMENT parsing seems to be language-independent: FRAG-
%MENTS are combined in a linear manner consistent with the order of tokens –
%the sub-f-structure of the FRAGMENT which is first on the list is a value of the
%attribute FIRST and the sub-f-structure of all other FRAGMENTS on the list is a
%value of the attribute REST. However, what kinds of phrases or tokens can con-
%stitute FRAGMENTS is decided by the designers of the grammar, so the resulting
%FRAGMENT grammar is to some extent language-dependent.

% 2) 
%In order to increase the coverage of the Polish LFG grammar, the technique of partial parsing described in the previous section is applied (cf. the penultimate
%row in Table 1). The procedure of partial parsing makes it possible to parse a larger number of sentences which otherwise receive no analysis. However, in contrast
%to English, where partial parsing is used to parse incorrect sentences, Polish sen-
%tences with FRAGMENT parses are not necessarily incorrect. Many of them are
%well-formed but contain linguistic phenomena for which POLFIE rules have not
%been defined yet. FRAGMENT analyses of such sentences are candidates for im-
%provement with the proposed method.

% 3)
% As dependency trees may contain errors, the internal structures of rule-based sub-
% f-structures are not modified when they disagree with the dependency tree, unless
% such a modification is essential for constructing a coherent f-structure for the whole sentence.
%         Which means they do modify f-structures, but not sure whether such
%         modification would solve the problem of 'japanischen' and
%         'Aussenministerium' being in two different phrases (I.S.)

\section{Conclusion And Future Work}
\label{sec:conclusion}
In this work we make use of a metric in selecting better candidates to be parsed in a sentence simplification system that aims to increase the number of sentences that have a full LFG analysis. We use the German ParGram Grammar to parse sentences from the TIGER corpus and fail to get a complete analyses in 19.44\% of the cases. % This increases the full XLE parses derived from the TIGER Treebank from 80.66% to 90.79%.
In order to get the analyses of the core arguments of the sentences, we simplify them and parse the shorter sentences. To decide what constituents to retain and what to delete during simplification we utilize dependency parses. We define deletable subtrees, that is subtrees that would not be part of the core arguments of a sentence, and delete one or more of them to generate shorter candidates.

The combination of multiple deletions lead to high number of candidates, making the task infeasible for parsing. To overcome this problem we select a subset of candidates to be parsed. We first repeat the experiments from \cite{CetinogluEtAl2013} as our baseline (`OneSubtreeShorter' and `10Shortest + ShortestNoPunct' in Table \ref{tab:results}) and then suggest the use of the parsability metric (`10WithoutLeastParsable'), which gives us an on par coverage, with more than double the average length of fully-parsed candidates. It shows that with a more informed metric, it is possible to get more complete analyses out of sentences that failed to have a full parse in their original form. 

%In very broad terms, the problem we were to solve is about increasing the coverage of a constituency parser without modifying the grammar behind it, but by simplifying sentences it fails to parse. 

%In particular, there is a German LFG parser we would like to increase the coverage of. Given a sentence German LFG parser failed to parse, our goal is to simplify it so that we can get a full ('non-fragmented' in XLE's terminology\footnote{XLE is one of the implementations of an LFG parser.}, since it allows for parse trees to be fragmented) parse tree for at least one of its simplified variants. If at least one of the simplified variants receives a full parse tree, the original sentence is considered to be "covered".

%There are several ways to simplify a sentence. The most straightforward way would be to delete words from it. However, deleting words token by token without any constraints regarding their syntactic function in the sentence inevitably will lead to cases when the verb and its core arguments (subject, direct or indirect objects) get deleted. We would like to avoid that so that the core informational content of a sentence is preserved.

%Here is where a dependency parser comes in -- given a dependency parse tree for a failed sentence, we can delete only modifier phrases from it and preserve all of the core arguments. \todo{cite?}It is generally assumed that a statistical (dependency) parser is more robust than a manually written (constituency) parser and that it will identify most of the modifier phrases correctly.

%One might argue that instead of trying to parse a simplified sentence with the LFG parser to obtain phrase structures an easier way of getting them would be to transform dependency structures into phrase structures, as described e.g. in \cite{xia2001converting} but, in fact, phrase structures (or 'c-structures', as they are called in LFG terminology) \todo{Don't say we are not interested in c-structures, say that we are interested in f-structures *more*.} are not our main concern. What we are interested in are the f-structures -- a deeper level of analysis LFG parsers provide.

The work presented can be extended in several ways. Firstly, the problem of searching for a simplified candidate can be phrased as a learning problem. Secondly, we would like to make the program learn to paraphrase subtrees (including core parts of a sentence) before or instead of deleting them. Thirdly, a factorized representation for sentences so that subtrees causing problems can be identified and candidates containing them pruned might be a good idea. Finally, it is yet to be seen whether taking shortest possible candidates is an upper bound for what can be recovered after simplification/paraphrasing.

The source code and data are publicly available for research purposes. The project repository can be found at the following address: \url{https://gitlab.com/selimcan/DBSS}.

%% acknowledgement is present only in the final copy of the paper
\section*{Acknowledgements}
This research was funded by the German Research Foundation (DFG) grant SFB 732 ``Incremental Specification in Context", project D2.

\bibliographystyle{splncs}

{\let\clearpage\relax \bibliography{references}} % NO page break before bibliography
%\bibliography{references}                       % with a page break before bibliography

\end{document}
