#!/bin/bash

./1_compile_figures.sh

for f in $(find . -name "*.mp" -o -name "*.svg"); do
    git add $f
done

git commit -m "$1"
