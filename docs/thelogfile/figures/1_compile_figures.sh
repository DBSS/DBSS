#!/bin/bash

for f in $(find . -name "*.mp"); do
    mpost $f
done

for f in $(find . -name "*.[0-9]"); do
    mv $f ${f/[0-9]/svg}
done
