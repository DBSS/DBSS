#!/usr/bin/env bash

apt-get update
apt-get -y upgrade

if [ ! -e /home/ubuntu/.bashrc ] ; then
    touch /home/ubuntu/.bashrc
fi

echo '' >> /home/ubuntu/.bashrc
echo 'export XLEPATH=/vagrant/dbss/lib/xle/linux-64' >> /home/ubuntu/.bashrc
echo 'export PATH=$XLEPATH/bin:$PATH' >> /home/ubuntu/.bashrc
echo 'export LD_LIBRARY_PATH=$XLEPATH/lib:$LD_LIBRARY_PATH' >> /home/ubuntu/.bashrc
echo 'export DYLD_LIBRARY_PATH=$XLEPATH/lib:$DYLD_LIBRARY_PATH' >> /home/ubuntu/.bashrc

. /home/ubuntu/.bashrc

apt-get -y install python3-pip

pip3 install pexpect behave pytest spacy zerorpc nltk

apt-get -y install language-pack-de default-jre
