#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Split the set of non-fully-parsed original sentences (FAILED_ORIGINALS) into
K >= 2 samples,

for each sample:
    reserve it,

    train a classifier using fully parsed and non-fully parsed simplified
    versions of the original sentences in K-1 non-reserved samples
    as training data (they can be found in FULLY_PARSED_SENTENCES and
    in FAILED_SENTENCES, respectively)

    assign a score to simplified versions of sentences in the reserved sample
    (candidates for all sentences in FAILED_ORIGINALS are pre-generated and
    stored in a compact form in ALL_CANDIDATES directory),

    take 10 best (read: most likely to receive the 'fully parsed' label
    from the classifier) and the best without punctuation and parse them
    with XLE,

combine parsing results from each fold.
"""

import pytest
import nltk
import pickle

from dbss.core import sentence
from dbss.plugins.ranker.mlranker import feature_extractor
from nltk import MaxentClassifier

## =================
## Data definitions:


## TrainingData is a (listof (tupleof feature_extractor.FeatureSet, String).
## interp. sentences represented as feature sets along with a label for them,
##         telling whether they were fully parsed by XLE or not.

TD_1 = [
    (
        {('lbl_hf_df', ('SB', 'sg|1|pres|ind', 'nom|sg|*|1')): True,
         ('ww', ('Ich', '.')): True,
         ('lbl_hf_df', ('--', 'sg|1|pres|ind', '_')): True,
         ('w', '.'): True,
         ('hwp', 'lese', 'VVFIN'): True,
         ('dw', '.'): True,
         ('ppp', ('PPER', '$.', '</s>')): True,
         ('p', 'PPER'): True,
         ('ww', ('.', '</s>')): True,
         ('hw', 'lese'): True,
         ('dp', '$.'): True,
         ('dwp', 'Ich', 'PPER'): True,
         ('pp', ('$.', '</s>')): True,
         ('hwp_dwp_dir', ('lese', 'VVFIN', 'Ich', 'PPER', 'left')): True,
         'length': 2,
         ('ppp', ('<s>', 'PPER', '$.')): True,
         ('p', '$.'): True,
         ('ww', ('<s>', 'Ich')): True,
         ('dw', 'Ich'): True,
         ('w', 'Ich'): True,
         ('pp', ('PPER', '$.')): True,
         ('hp', 'VVFIN'): True,
         ('www', ('Ich', '.', '</s>')): True,
         ('pp', ('<s>', 'PPER')): True,
         ('dwp', '.', '$.'): True,
         ('www', ('<s>', 'Ich', '.')): True,
         ('hwp_dwp_dir', ('lese', 'VVFIN', '.', '$.', 'right')): True,
         ('dp', 'PPER'): True},
        'FullyParsed'
    )
]


## Classifier can be any of the nltk classes inhering from ClassifierI.


## ==========
## Constants:


FAILED_ORIGINALS = '../DBSS_DATA/experiments/input/tiger_train_easyPunct_xleFail.conll09'
FULLY_PARSED_SENTENCES = '../DBSS_DATA/parsed.conll09'
FAILED_SENTENCES = '../DBSS_DATA/failed.conll09'
K = 10
PATH_TO_MEGAM = 'dbss/lib/megam-64.opt'
EXPERIMENT_RESULTS = '../DBSS_DATA/experiments/cross_validation/'


## ==========
## Functions:


## (listof Any) Integer -> (listof (listof Any))
def samples(things, k):
    """Split the list of things given into k samples.

    ASSUME: 2 <= k <= len(things)
    """
    result = []
    chunk_size = round(len(things) / k)
    i = 0
    while i < k - 1:
        chunk = things[i * chunk_size:(i+1) * chunk_size]
        result.append(chunk)
        i += 1
    result.append(things[i * chunk_size:])
    return result

def test_samples():
    assert samples(['a', 'b'], 2) == [['a'], ['b']]
    assert samples(['a', 'b', 'c'], 2) == [['a', 'b'], ['c']]
    assert samples(['a', 'b', 'c', 'd'], 3) == [['a'], ['b'], ['c', 'd']]
    assert samples([sentence.S_1, sentence.S_2], 2) == \
           [[sentence.S_1], [sentence.S_2]]


## (listof Sentence) (listof Sentence) -> (listof Sentence)
def filter(to_filter, relevant):
    """Given two lists of sentences, return sentences from the first list
    if a sentence with the same ID is also found in the second list.
    """
    relevant_ids = {sentence.get_id(s) for s in relevant}
    return [s for s in to_filter if sentence.get_id(s) in relevant_ids]

def test_filter():
    assert filter([], [sentence.S_1]) == []
    assert filter([sentence.S_1], []) == []
    assert filter([sentence.S_1, sentence.S_2],
                  [sentence.S_2, sentence.S_3]) == \
           [sentence.S_2]


## (listof Sentence) (listof Sentence) String -> TrainingData
def create_training_data(origs, simplified, label):
    """Given a list with original sentences, a list with their simplified
    versions, and a label telling whether these were fully parsed or failed
    sentences, turn them into training data suitable for NLTK's classifiers.
    """
    id2orig = {sentence.get_id(s): s for s in origs}
    try:
        return [
            (feature_extractor.extract_features(id2orig[
                                                    sentence.get_id(simple_s)],
                                                simple_s,
                                                []),
             label) for simple_s in simplified
        ]
    except KeyError:
        raise ValueError('The list of original sentences does not contain '
                         'a corresponding original sentence for one of the '
                         'candidates!')

def test_create_training_data():
    assert create_training_data([sentence.S_7],
                                [[sentence.S_7[0]] + [sentence.S_7[2]]],
                                'FullyParsed') == \
           TD_1
    assert create_training_data([sentence.S_7],
                                [[sentence.S_7[0]] + [sentence.S_7[2]],
                                 [sentence.S_7[0]] + [sentence.S_7[2]]],
                                'FullyParsed') == \
           TD_1 + TD_1
    with pytest.raises(ValueError):
        create_training_data([],
                             [[sentence.S_7[0]] + [sentence.S_7[2]]],
                             'FullyParsed')


## TrainingData -> TrainingData
def shuffle(training_data):
    """Randomize the training data so that it is not a concatenation
    of instances with another label to instances with another label.
    """
    pass


## TrainingData -> Classifier
def train(training_data):
    """Return a classifier given training data."""
    nltk.config_megam(PATH_TO_MEGAM)
    classifier = MaxentClassifier.train(training_data,
                                        algorithm='megam',
                                        trace=0,
                                        max_iter=10)
    return classifier


def main():
    with open(FAILED_ORIGINALS) as fof:
        failed_originals = list(sentence.read_sentences(fof))

    with open(FULLY_PARSED_SENTENCES) as fpsf:
        fully_parsed_sentences = list(sentence.read_sentences(fpsf))

    with open(FAILED_SENTENCES) as fsf:
        failed_sentences = list(sentence.read_sentences(fsf))

    print('Splitting failed sentences into ', K, ' samples.\n')
    sentence_samples = samples(failed_originals, K)

    for i in range(len(sentence_samples)):
        print('Using sample ', i, ' as test sample.')
        test_originals = sentence_samples[i]
        training_originals = []
        for sample in sentence_samples[:i] + sentence_samples[i + 1:]:
            training_originals.extend(sample)
        training_data = create_training_data(training_originals,
                                             filter(fully_parsed_sentences, training_originals),
                                             'FullyParsed')
        print(type(training_data), len(training_data))
        training_data2 = create_training_data(training_originals,
                                             filter(failed_sentences, training_originals),
                                             'Failed')
        print(type(training_data2), len(training_data2))
        print('Training a classifier using labeled simplified versions of sentences '
              'in the rest of the samples.\n')
        classifier = train(training_data + training_data2)

        with open(EXPERIMENT_RESULTS + 'classfiier_' + 'fold_' + str(i) + '.pickle', 'wb') as outf:
            pickle.dump(classifier, outf)

        for s in test_originals[:10]:
            print(sentence.s2string(s), classifier.prob_classify(feature_extractor.extract_features(s, s, [])).prob('FullyParsed'))


if __name__ == "__main__":
    main()
