Feature: Simplify and Parse
  As users and developers of the LFG Parser
  In order to get a full parse for every input sentence and to learn when
    the LFG Parser fails to do so,
  We want to simplify sentences the LFG Parser fails to fully parse,
    try out (some of the) simpler versions,
    and log the original sentence, simpler versions the system explores,
    and the answer the system returns.


  Scenario Outline: While timeout not reached, generate simpler candidates and try to parse them

    Given the function "simplify_and_parse" from the package "simplify_and_parse"
    When the user gives <original sentence> and <timeout> (in seconds) 
    Then the system should return the <output>

  Examples: A fully parsable simpler version exists and can be found within the given time limit

    | original sentence | timeout | output     |
    | Ich sehe ihm .    | 5       | Ich sehe . |

  Examples: Original sentence can also be fully parsed, but the system does not try it because it assumes the opposite

    | original sentence | timeout | output     |
    | Ich sehe es .     | 5       | Ich sehe . |

  Examples: A fully parsable simpler version may exist, but timeout was reached

    | original sentence                                                                                                          | timeout | output              |
    | `` Ich glaube kaum , daß mit seinem , naja , etwas undiplomatischen Stil im Weißen Haus dem Land ein Gefallen getan wäre . | 1       | timeout was reached |

  Examples: All possible candidates explored => a fully parsable simpler version does not exist (well, at least for the particular simplifier in question)

    | original sentence | timeout  | output |
    | a b c d f .       | 5        | False  |
