from behave import *
from dbss.use_cases import simplify_and_parse

@given(u'the function "simplify_and_parse" from the package "simplify_and_parse"')
def step_impl(context):
    context.simplify_and_parse = simplify_and_parse.simplify_and_parse

@when(u'the user gives {original_sentence} and {timeout:d} (in seconds)')
def step_impl(context, original_sentence, timeout):
    context.original_sentence = original_sentence
    context.timeout = timeout

@then(u'the system should return the {output}')
def step_impl(context, output):
    assert context.simplify_and_parse(context.original_sentence, context.timeout) == output
