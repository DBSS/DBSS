#!/usr/bin/bash

# Feature: Limit the number of simplified candidates explored or solutions found.
#
# Takes only m simplified candidates to parse and/or stops after n successful
# parses were found. Otherwise the setup is similar to T_OneTokenShorterRecursive


all_scenarios_passed=true


# Scenario1: take all candidates and return all solutions
  # Given
    rm -rf Candidates/ XLEParses/ ParsedSentences/
  # When
    python3 dbss/use_cases/persistent_parser.py -i acceptance_tests/T_MaxCandidatesMaxSolutions/Scenario1/input.conll09 \
    -c Candidates/ -p XLEParses/ -o ParsedSentences/ -m OneTokenShorterRecursiveBFSearch
  # Then
    if diff acceptance_tests/T_MaxCandidatesMaxSolutions/Scenario1/expected_candidates/3.conll09 Candidates/3.conll09 && \
       diff acceptance_tests/T_MaxCandidatesMaxSolutions/Scenario1/expected_candidates/3.lfg Candidates/3.lfg && \
       diff acceptance_tests/T_MaxCandidatesMaxSolutions/Scenario1/expected.conll09 ParsedSentences/3.conll09;
    then
      echo "     PASSED: Scenario 1 of $0"
      rm -rf Candidates/ XLEParses/ ParsedSentences/
    else
      all_scenarios_passed=false
      echo "     FAILED: Scenario 1 of $0"
      rm -rf Candidates/ XLEParses/ ParsedSentences/
    fi


# Scenario2: stop if one soluton was found
  # Given
    rm -rf Candidates/ XLEParses/ ParsedSentences/
  # When
    python3 dbss/use_cases/persistent_parser.py -i acceptance_tests/T_MaxCandidatesMaxSolutions/Scenario2/input.conll09 \
    -c Candidates/ -p XLEParses/ -o ParsedSentences/ -m OneTokenShorterRecursiveBFSearch \
    --max_solutions 1
  # Then
    if diff acceptance_tests/T_MaxCandidatesMaxSolutions/Scenario2/expected_candidates/3.conll09 Candidates/3.conll09 && \
       diff acceptance_tests/T_MaxCandidatesMaxSolutions/Scenario2/expected_candidates/3.lfg Candidates/3.lfg && \
       diff acceptance_tests/T_MaxCandidatesMaxSolutions/Scenario2/expected.conll09 ParsedSentences/3.conll09;
    then
      echo "     PASSED: Scenario 2 of $0"
      rm -rf Candidates/ XLEParses/ ParsedSentences/
    else
      all_scenarios_passed=false
      echo "     FAILED: Scenario 2 of $0"
      rm -rf Candidates/ XLEParses/ ParsedSentences/
    fi


# Scenario3: take 10 candidates (not counting the original sentence) and stop if one soluton was found
  # Given
    rm -rf Candidates/ XLEParses/ ParsedSentences/
  # When
    python3 dbss/use_cases/persistent_parser.py -i acceptance_tests/T_MaxCandidatesMaxSolutions/Scenario3/input.conll09 \
    -c Candidates/ -p XLEParses/ -o ParsedSentences/ -m OneTokenShorterRecursiveBFSearch \
    --max_candidates 10 --max_solutions 1
  # Then
    if diff acceptance_tests/T_MaxCandidatesMaxSolutions/Scenario3/expected_candidates/3.conll09 Candidates/3.conll09 && \
       diff acceptance_tests/T_MaxCandidatesMaxSolutions/Scenario3/expected_candidates/3.lfg Candidates/3.lfg && \
       diff acceptance_tests/T_MaxCandidatesMaxSolutions/Scenario3/expected.conll09 ParsedSentences/3.conll09;
    then
      echo "     PASSED: Scenario 3 of $0"
      rm -rf Candidates/ XLEParses/ ParsedSentences/
    else
      all_scenarios_passed=false
      echo "     FAILED: Scenario 3 of $0"
      rm -rf Candidates/ XLEParses/ ParsedSentences/
    fi


# Scenario4: take 10 candidates (not counting the original sentence) and stop if two solutons were found
  # Given
    rm -rf Candidates/ XLEParses/ ParsedSentences/
  # When
    python3 dbss/use_cases/persistent_parser.py -i acceptance_tests/T_MaxCandidatesMaxSolutions/Scenario4/input.conll09 \
    -c Candidates/ -p XLEParses/ -o ParsedSentences/ -m OneTokenShorterRecursiveBFSearch \
    --max_candidates 10 --max_solutions 2
  # Then
    if diff acceptance_tests/T_MaxCandidatesMaxSolutions/Scenario4/expected_candidates/3.conll09 Candidates/3.conll09 && \
       diff acceptance_tests/T_MaxCandidatesMaxSolutions/Scenario4/expected_candidates/3.lfg Candidates/3.lfg && \
       diff acceptance_tests/T_MaxCandidatesMaxSolutions/Scenario4/expected.conll09 ParsedSentences/3.conll09;
    then
      echo "     PASSED: Scenario 4 of $0"
      rm -rf Candidates/ XLEParses/ ParsedSentences/
    else
      all_scenarios_passed=false
      echo "     FAILED: Scenario 4 of $0"
      rm -rf Candidates/ XLEParses/ ParsedSentences/
    fi


if [ "$all_scenarios_passed" = true ]; then
    echo "PASSED: $0"
    exit 0;
else
    echo "FAILED: $0"
    exit 1
fi
