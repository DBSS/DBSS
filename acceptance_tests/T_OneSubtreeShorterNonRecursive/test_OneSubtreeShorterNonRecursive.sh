#!/usr/bin/bash

# Feature: Generate all possible sentences with *one* deletable subtree removed
#          and write the ones which are parsed to a file named after sentence's id.
# Edge labels that correspond to heads of deletable subtrees (e.g. AG, MO, RC)
# are taken from [Çetinoğlu et al 2013] paper.
# Corresponds to the 'one subtree deletion' mode described in the paper.


# Scenario: removing only one subtree is enough to get a parsable sentence
  # Given
    rm -rf Candidates/ XLEParses/ ParsedSentences/
  # When
    python3 dbss/use_cases/persistent_parser.py -i acceptance_tests/T_OneSubtreeShorterNonRecursive/input.conll09 \
    -c Candidates/ -p XLEParses/ -o ParsedSentences/ -m OneSubtreeShorterNonRecursive
  # Then
    if diff acceptance_tests/T_OneSubtreeShorterNonRecursive/expected_candidates/880.conll09 Candidates/880.conll09 && \
       diff acceptance_tests/T_OneSubtreeShorterNonRecursive/expected_candidates/880.lfg Candidates/880.lfg && \
       diff acceptance_tests/T_OneSubtreeShorterNonRecursive/expected.conll09 ParsedSentences/880.conll09;
    then
      echo "PASSED: $0"
      rm -rf Candidates/ XLEParses/ ParsedSentences/
      exit 0
    else
      echo "FAILED: $0"
      rm -rf Candidates/ XLEParses/ ParsedSentences/
      exit 1
    fi
