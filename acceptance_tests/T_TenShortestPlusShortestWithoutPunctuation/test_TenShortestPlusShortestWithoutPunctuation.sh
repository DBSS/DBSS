#!/usr/bin/bash

# Feature: Generate all candidates up-front, take only the 10 shortest
#          candidates into account when a sentence has more than 10 candidates,
#          remove punctuation from the shortest candidate and include it as the
#          eleventh (or just one more) candidate.


all_scenarios_passed=true


# Scenario1: OneSubtreeShorterRecursive mode, which will produce candidates much more then 10,
#            thus we expect 11 candidates in expected_candidates/888.lfg.
#
#            Currently: first shortest with punctuation removed, then at most 10 more in ascending
#            order of their length, but TODO better way -- same set of sentences, but longest first.
  # Given
    rm -rf Candidates/ XLEParses/ ParsedSentences/
  # When
    python3 dbss/use_cases/persistent_parser.py -i acceptance_tests/T_TenShortestPlusShortestWithoutPunctuation/Scenario1/input.conll09 \
    -c Candidates/ -p XLEParses/ -o ParsedSentences/ -m 10Shortest+ShortestWithoutPunctuation
  # Then
    if diff acceptance_tests/T_TenShortestPlusShortestWithoutPunctuation/Scenario1/expected_candidates/880.conll09 Candidates/880.conll09 && \
       diff acceptance_tests/T_TenShortestPlusShortestWithoutPunctuation/Scenario1/expected_candidates/880.lfg Candidates/880.lfg;
    then
      echo "     PASSED: Scenario 1 of $0"
      rm -rf Candidates/ XLEParses/ ParsedSentences/
    else
      all_scenarios_passed=false
      echo "     FAILED: Scenario 1 of $0"
      rm -rf Candidates/ XLEParses/ ParsedSentences/
    fi


if [ "$all_scenarios_passed" = true ]; then
    echo "PASSED: $0"
    exit 0;
else
    echo "FAILED: $0"
    exit 1
fi

