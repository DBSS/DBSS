#!/usr/bin/env bash

declare -A id2sample

IFS=','

while read id sample; do
   id2sample[$id]=$sample
done < ../DBSS_DATA/experiments/cross_validation/id2sample

while read id; do
   sed 's!.*!../DBSS_DATA/data/allcandids/&.compact!g' | \
   parallel -j 30 ./helpers/cross_validate.sh {} "../DBSS_DATA/experiments/cross_validation/classfiier_fold_${id2sample[$id]}.pickle"
done < $1
