from setuptools import setup

setup(
    # Application name:
    name='dbss',

    # Version number (initial):
    version='0.1.0',

    # Application author details:
    author='Ilnar Salimzianov',
    author_email='ilnar.salimzianov@ims.uni-stuttgart.de',

    # Packages
    packages=['dbss',
              'dbss.core',
              'dbss.use_cases',
              'dbss.plugins',
              'dbss.plugins.dependency_parser',
              'dbss.plugins.lfg_parser',
              'dbss.plugins.ranker',
              'dbss.plugins.ranker.mlranker',
              'dbss.plugins.ranker.mlranker.models',
              'dbss.user_interfaces',
    ],

    data_files = [('', ['dbss/plugins/ranker/mlranker/models/me_classifier.pickle'])],

    # Include additional files into the package
    include_package_data=True,

    # Details
    url='https://www.overleaf.com/read/rqzsvfrwctft',

    # license='LICENSE.txt',
    description='Useful towel-related stuff.',

    # long_description=open('README.txt').read(),

    # Dependent packages (distributions)
    install_requires=[
        'pytest',
        'behave',
        'spacy',
        'zerorpc',
        'nltk',
        'pexpect'
    ],
)
