#!/usr/local/bin/perl -w

# Skript, das ein Lexikon nach folgendem Muster durchsucht:
# Lemma bla { ...
#           }
# d.h. 1 Subkatrahmen in { .. } ohne Disjunktion
# Skript loescht Klammern

$in = shift @ARGV;


open(IN,"$in") or die "Error reading file $in";

$kandidat = 0;


while ($line=<IN>) {
  chomp $line;
  
  if ($kandidat == 0) {

      if ($line =~ m/^(\s*)\{/ ) {
	  $kandidat = 1; 
	  $out_old = "$line";

	  $out_new = "$1$'"; # delete {
      }
  
      else { print "$line\n"; }

      next;
  }
  

  if  ($kandidat == 1) {

      if ($line =~ m/^\s*\|/ ) { 
	  $kandidat = 0; 
	  print "$out_old\n$line\n";
	  $out = "";
	  $out_old = "";
	  $out_new = "";
      }
      
      elsif ($line =~ m/^\s*\}/ ) { 
	  $kandidat = 0; 
	  $line = $';
	  print  "$out_new$line\n";
	  $out = "";
	  $out_old = "";
	  $out_new = "";
      }
      
      else { print "ERROR \n\n";
	 }
  }


}

close(IN)  or die "$in could not be closed";


