################################################################################
## Makefile to construct the tokenizer for the German grammar.
## 
## nobi@ims 19.08.97
## sd@ims 1.10.2002
################################################################################
# variables
#
FSC		= fsc				# finite-state compiler
MF 		= Makefile
DEFINES		= ../defines.fsc		# generally used abbreviations in fsc

TOKENS		= tokens			# list of multi-character delimiters
TOKENS-FST	= $(TOKENS).fst			# fst for multi-character delimiters

# sd: note: tried to take list of abbreviation from dmor1
#     but automata then become too large, apparently
ABBREV		= abbreviations			# list of abbreviations
MW-OPT-FST	= muwo.optional.fst		# FST for all optional multiwords
MW-REQ-FST	= muwo.required.fst		# FST for all required multiwords
MW-FST		= muwo.fst			# FST handling all multiwords

PARSE		= tokenize-uncap.fst		# tokenizers to construct
GENERATE	= untokenize.fst


################################################################################
# high-level targets
#

all: $(PARSE) $(GENERATE)


################################################################################
# Unconditional Tokens
#
# These are multi-character delimiters which are separated to prevent 
# interference with multiwords. The FST is used before tokenization to transform 
# them to multi-character symbols, and after tokenization in inverse direction
# to recreate them. 

$(TOKENS-FST): $(MF) $(DEFINES) $(TOKENS)
	$(FSC) 	-e "source $(DEFINES)" \
		-e "regex [ SAVETOKENS ];" \
		-e "stack save $@" \
		-stop


################################################################################
# Multiword handling
#
# Wraps all words not tokenized normally into MWbegin/MWend pairs. Handles
# abbreviations, numbers, formally defined names. 
#
# Note: 
# - Empty lines in any multiword list will cause problems!
# - A multiword which is a prefix of another multiword will not show up.
# - Multiwords are assumed to begin and end on orthographic word boundaries. 

$(MW-OPT-FST): $(MF) $(DEFINES)
	$(FSC)	-e "source $(DEFINES)" \
		-e "regex QUOTED;" \
		-e "stack save $@" \
		-stop

$(MW-REQ-FST): $(MF) $(DEFINES) $(ABBREV)
	$(FSC)	-e "source $(DEFINES)" \
		-e "fs read-text $(ABBREV)" \
		-e "define ABBR" \
		-e "regex [ ABBR %. ];" \
		-e "define ABBR" \
		-e "regex [ ABBR | NUMBER | SERIAL ];" \
		-e "stack save $@" \
		-stop

caps-%.fst: %.fst
	$(FSC)	-e "source $(DEFINES)" \
		-e "stack load $<" \
		-e "define FST" \
		-e "regex  [ FST | [ [ [ CAPITAL .o. LOWERCASE ] ?* ] .o. FST .o. [ UPPERCASE ?* ] ] ]" \
		-e "stack save $@" \
		-stop

$(MW-FST): $(MF) $(DEFINES) caps-$(MW-OPT-FST) caps-$(MW-REQ-FST)
	$(FSC) 	-e "source $(DEFINES)" \
		-e "stack load caps-$(MW-OPT-FST)" \
		-e "define MWopt" \
		-e "regex [ [ MULTIWORD | NOMULTIWORD | [ MWopt @-> MWbegin ... MWend || MWboundary _ MWboundary ] ]+ ];" \
		-e "define MWopt" \
		-e "stack load caps-$(MW-REQ-FST)" \
		-e "define MWreq" \
		-e "regex [ MWreq @-> MWbegin ... MWend || .#. [ NOMULTIWORD | MULTIWORD ]* MWboundary _ MWboundary ];" \
		-e "define MWreq" \
		-e "regex [ MWopt .o. MWreq .o. ORDINAL .o. MUWOWITHDOT ];" \
		-e "stack save $@" \
		-stop


################################################################################
# The parsing tokenizers
#
# The name tags have the following meanings:
# uncap = optionally lowercases the first letter of the input
#    	  We require the next char to not be a CAPITAL 
#	  to exclude all-capitalized words.
#
# muwo = handles multiwords/abbreviations
#
tokenize.fst: $(DEFINES) $(TOKENS-FST) $(MW-FST)
	$(FSC) 	-e "source $(DEFINES)" \
		-e "stack load $(TOKENS-FST)" \
		-e "define TOKENS" \
		-e "regex [ TOKENS ];" \
		-e "fs invert" \
		-e "define TOKENSi" \
		-e "stack load $(MW-FST)" \
		-e "define MUWOs" \
		-e "define pre [ SINGLESPACE .o. TOKENS .o. MUWOs .o. OPTCOMMATA  ];" \
		-e "define post [ RESTOREMULTIWORD .o. TASTEHYPHEN .o. TOKENSi .o. HYPHENCAP ];" \
		-e "define bounded [ pre .o. BOUNDARIES .o. post ];" \
		-e "regex [ bounded ];" \
		-e "fs invert" \
		-e "stack save $@" \
		-stop

tokenize-uncap.fst: $(DEFINES) tokenize.fst
	$(FSC)	-e "source $(DEFINES)" \
		-e "stack load tokenize.fst" \
		-e "fs invert" \
		-e "define TOK" \
		-e "regex [ [ (SENTENCEID) (DECAPITALIZE \[CAPITAL])  ?* | \[LETTER] ?* ] .o. TOK ];" \
		-e "fs invert" \
		-e "stack save $@" \
		-stop

# sd: adds ^DECAP in front of decapitalized first token (not used)
new-tokenize-uncap.fst: $(DEFINES) tokenize.fst
	$(FSC)	-e "source $(DEFINES)" \
		-e "stack load tokenize.fst" \
		-e "fs invert" \
		-e "define TOK" \
		-e "regex [ [ (SENTENCEID) 0:%^DECAP 0:%@ DECAPITALIZE \[CAPITAL] ?* | \[LETTER] ?* ] .o. TOK ];" \
		-e "fs invert" \
		-e "stack save $@" \
		-stop

################################################################################
# The generation tokenizers
#
# These tokenizers are used for generation. They lack the normalization stuff
# for white space and punctuation.
#
untokenize.fst: $(MF) $(DEFINES) 
	$(FSC) 	-e "source $(DEFINES)" \
		-e "regex [ [ %@ -> [] || _ [ PUNCT | OPENBRACKET ] , CLOSEBRACKET _ ] .o. [ COMMA -> [] || _ PUNCT ] .o. [ %@ -> SPACE ] ]" \
		-e "stack save $@" \
		-stop
