% -*- coding: iso-8859-1 -*-

fstructure('die Forscher werteten Messungen auf den Seychellen aus.',
	% Properties:
	[
	'xle_version'('XLE release of Jan 10, 2007 13:00.'),
	'grammar'('/mount/projekte16/pargram/rohrer-20apr05/german.lfg'),
	'grammar_date'('Jan 18, 2007 10:58'),
	'word_count'('8'),
	'statistics'('4+16 solutions, 0.25 CPU seconds, 221 subtrees unified'),
	'rootcategory'('ROOT')
	],
	% Choices:
	[
	choice([A1,A2,A3,A4], 1)
	],
	% Equivalences:
	[
	select(A2, 1)
	],
	% Constraints:
	[
	cf(1,eq(attr(var(0),'PRED'),semform('werten',7,[var(16),var(12)],[]))),
	cf(1,eq(attr(var(0),'OBJ'),var(12))),
	cf(1,eq(attr(var(0),'SUBJ'),var(16))),
	cf(or(A1,A2),eq(attr(var(0),'ADJUNCT'),var(1))),
	cf(1,eq(attr(var(0),'CHECK'),var(9))),
	cf(1,eq(attr(var(0),'TNS-ASP'),var(18))),
	cf(1,eq(attr(var(0),'TOPIC'),var(19))),
	cf(1,eq(attr(var(0),'CLAUSE-TYPE'),'declarative')),
	cf(1,eq(attr(var(0),'PART-FORM'),'aus')),
	cf(1,eq(attr(var(0),'STMT-TYPE'),'declarative')),
	cf(1,eq(attr(var(0),'VTYPE'),'main')),
	cf(or(A3,A1),eq(var(12),var(19))),
	cf(or(A4,A2),eq(attr(var(12),'PRED'),semform('Messung',8,[],[]))),
	cf(A4,eq(attr(var(12),'ADJUNCT'),var(13))),
	cf(or(A4,A2),eq(attr(var(12),'NSEM'),var(14))),
	cf(or(A4,A2),eq(attr(var(12),'NTYPE'),var(15))),
	cf(1,eq(attr(var(12),'CASE'),'acc')),
	cf(or(A4,A2),eq(attr(var(12),'GEND'),'fem')),
	cf(or(A4,A2),eq(attr(var(12),'NUM'),'pl')),
	cf(or(A4,A2),eq(attr(var(12),'PERS'),'3')),
	cf(or(A3,A4),in_set(var(2),var(13))),
	cf(1,eq(attr(var(2),'PRED'),semform('auf',9,[var(3)],[]))),
	cf(1,eq(attr(var(2),'OBJ'),var(3))),
	cf(1,eq(attr(var(2),'PSEM'),'loc')),
	cf(1,eq(attr(var(2),'PTYPE'),'sem')),
	cf(1,eq(attr(var(3),'PRED'),semform('Seychellen',12,[],[]))),
	cf(1,eq(attr(var(3),'CHECK'),var(4))),
	cf(1,eq(attr(var(3),'NTYPE'),var(6))),
	cf(1,eq(attr(var(3),'SPEC'),var(7))),
	cf(1,eq(attr(var(3),'CASE'),'dat')),
	cf(1,eq(attr(var(3),'INFL'),'strong-det')),
	cf(1,eq(attr(var(3),'NUM'),'pl')),
	cf(1,eq(attr(var(3),'PERS'),'3')),
	cf(1,eq(attr(var(4),'_SPEC-TYPE'),var(5))),
	cf(1,eq(attr(var(5),'_COUNT'),'+')),
	cf(1,eq(attr(var(5),'_DEF'),'+')),
	cf(1,eq(attr(var(5),'_DET'),'attr')),
	cf(1,eq(attr(var(6),'GRAIN'),'proper')),
	cf(1,eq(attr(var(7),'DET'),var(8))),
	cf(1,eq(attr(var(8),'PRED'),semform('die',11,[],[]))),
	cf(1,eq(attr(var(8),'DET-TYPE'),'def')),
	cf(1,eq(attr(var(14),'COMMON'),'count')),
	cf(or(A3,A1),eq(var(15),var(17))),
	cf(1,eq(attr(var(15),'GRAIN'),'common')),
	cf(or(A4,A2),eq(var(16),var(19))),
	cf(or(A3,A1),eq(attr(var(16),'PRED'),semform('Messung',8,[],[]))),
	cf(A3,eq(attr(var(16),'ADJUNCT'),var(13))),
	cf(or(A3,A1),eq(attr(var(16),'NSEM'),var(14))),
	cf(or(A3,A1),eq(attr(var(16),'NTYPE'),var(17))),
	cf(1,eq(attr(var(16),'CASE'),'nom')),
	cf(or(A3,A1),eq(attr(var(16),'GEND'),'fem')),
	cf(1,eq(attr(var(16),'NUM'),'pl')),
	cf(or(A3,A1),eq(attr(var(16),'PERS'),'3')),
	cf(or(A4,A2),eq(var(17),var(24))),
	cf(or(A3,A1),eq(attr(var(17),'GRAIN'),'common')),
	cf(or(A1,A2),in_set(var(2),var(1))),
	cf(1,eq(attr(var(9),'_VLEX'),var(10))),
	cf(1,eq(attr(var(9),'_VMORPH'),var(11))),
	cf(1,eq(attr(var(9),'_PASSIVE'),'-')),
	cf(1,eq(attr(var(10),'_AUX-SELECT'),'haben')),
	cf(1,eq(attr(var(11),'_PART-VERB'),'+')),
	cf(1,eq(attr(var(11),'_PARTICLE'),'detached')),
	cf(1,eq(attr(var(18),'MOOD'),'indicative')),
	cf(1,eq(attr(var(18),'TENSE'),'past_')),
	cf(1,eq(attr(var(19),'PRED'),semform('Forscher',3,[],[]))),
	cf(1,eq(attr(var(19),'CASE'),var(20))),
	cf(1,eq(attr(var(19),'CHECK'),var(21))),
	cf(1,eq(attr(var(19),'NSEM'),var(23))),
	cf(1,eq(attr(var(19),'NTYPE'),var(24))),
	cf(1,eq(attr(var(19),'SPEC'),var(25))),
	cf(1,eq(attr(var(19),'GEND'),'masc')),
	cf(1,eq(attr(var(19),'INFL'),'strong-det')),
	cf(1,eq(attr(var(19),'NUM'),'pl')),
	cf(1,eq(attr(var(19),'PERS'),'3')),
	cf(or(A3,A1),eq(var(20),'acc')),
	cf(or(A4,A2),eq(var(20),'nom')),
	cf(1,eq(attr(var(21),'_SPEC-TYPE'),var(22))),
	cf(1,eq(attr(var(22),'_COUNT'),'+')),
	cf(1,eq(attr(var(22),'_DEF'),'+')),
	cf(1,eq(attr(var(22),'_DET'),'attr')),
	cf(1,eq(attr(var(23),'COMMON'),'count')),
	cf(1,eq(attr(var(24),'GRAIN'),'common')),
	cf(1,eq(attr(var(25),'DET'),var(26))),
	cf(1,eq(attr(var(26),'PRED'),semform('die',1,[],[]))),
	cf(1,eq(attr(var(26),'DET-TYPE'),'def')),
	cf(1,eq(proj(var(27),'o::'),var(28))),
	cf(1,in_set('DieAsDet',var(28))),
	cf(1,eq(attr(var(29),'LEFT_SISTER'),var(30))),
	cf(1,eq(attr(var(29),'RIGHT_DAUGHTER'),var(31))),
	cf(1,eq(attr(var(29),'RIGHT_SISTER'),var(32))),
	cf(or(A1,A2),eq(proj(var(29),'o::'),var(33))),
	cf(or(A1,A2),eq(var(30),var(60))),
	cf(or(A3,A4),eq(var(30),var(61))),
	cf(or(A1,A2),eq(var(31),var(36))),
	cf(or(A3,A4),eq(var(31),var(34))),
	cf(1,eq(attr(var(32),'LEFT_SISTER'),var(29))),
	cf(1,in_set('PPAdjunct',var(33))),
	cf(or(A1,A2),eq(var(34),var(29))),
	cf(1,eq(attr(var(34),'LEFT_SISTER'),var(35))),
	cf(1,eq(attr(var(34),'RIGHT_DAUGHTER'),var(36))),
	cf(1,eq(proj(var(34),'o::'),var(33))),
	cf(or(A1,A2),eq(var(35),var(60))),
	cf(or(A3,A4),eq(var(35),var(59))),
	cf(1,eq(attr(var(36),'RIGHT_DAUGHTER'),var(37))),
	cf(1,eq(attr(var(37),'LEFT_SISTER'),var(38))),
	cf(1,eq(attr(var(37),'RIGHT_DAUGHTER'),var(39))),
	cf(1,eq(attr(var(38),'RIGHT_SISTER'),var(37))),
	cf(1,eq(attr(var(39),'RIGHT_DAUGHTER'),var(40))),
	cf(1,eq(attr(var(40),'LEFT_SISTER'),var(41))),
	cf(1,eq(attr(var(41),'RIGHT_SISTER'),var(40))),
	cf(1,eq(proj(var(42),'o::'),var(43))),
	cf(1,in_set('DieAsDet',var(43))),
	cf(1,eq(proj(var(44),'o::'),var(45))),
	cf(1,in_set('Name',var(45))),
	cf(1,eq(proj(var(46),'o::'),var(47))),
	cf(1,in_set('VerbParticle',var(47))),
	cf(1,eq(attr(var(50),'LEFT_SISTER'),var(51))),
	cf(1,eq(attr(var(51),'RIGHT_DAUGHTER'),var(52))),
	cf(1,eq(attr(var(51),'RIGHT_SISTER'),var(50))),
	cf(1,eq(attr(var(52),'LEFT_SISTER'),var(53))),
	cf(1,eq(attr(var(52),'RIGHT_DAUGHTER'),var(57))),
	cf(1,eq(attr(var(53),'RIGHT_DAUGHTER'),var(54))),
	cf(1,eq(attr(var(53),'RIGHT_SISTER'),var(52))),
	cf(1,eq(attr(var(54),'RIGHT_DAUGHTER'),var(55))),
	cf(1,eq(attr(var(55),'LEFT_SISTER'),var(56))),
	cf(1,eq(attr(var(56),'RIGHT_SISTER'),var(55))),
	cf(1,eq(attr(var(57),'RIGHT_DAUGHTER'),var(32))),
	cf(or(A3,A4),eq(attr(var(59),'RIGHT_SISTER'),var(34))),
	cf(or(A1,A2),eq(attr(var(60),'RIGHT_SISTER'),var(29))),
	cf(or(A3,A4),eq(attr(var(61),'RIGHT_SISTER'),var(29))),
	cf(or(A4,A2),eq(var(48),var(12))),
	cf(or(A3,A1),eq(var(48),var(16))),
	cf(1,eq(attr(var(48),'PRED'),semform('Messung',8,[],[]))),
	cf(or(A3,A4),eq(attr(var(48),'ADJUNCT'),var(13))),
	cf(1,eq(attr(var(48),'CASE'),var(49))),
	cf(1,eq(attr(var(48),'NSEM'),var(14))),
	cf(1,eq(attr(var(48),'NTYPE'),var(15))),
	cf(1,eq(attr(var(48),'GEND'),'fem')),
	cf(1,eq(attr(var(48),'NUM'),'pl')),
	cf(1,eq(attr(var(48),'PERS'),'3')),
	cf(or(A4,A2),eq(var(49),'acc')),
	cf(or(A3,A1),eq(var(49),'nom'))
	],
	% C-Structure:
	[
	cf(1,subtree(1932,'ROOT',2275,328)),
	cf(1,phi(1932,var(0))),
	cf(1,subtree(2788,'DPx[std]',550,907)),
	cf(1,phi(2788,var(19))),
	cf(1,cproj(2788,var(54))),
	cf(or(A1,A2),subtree(2630,'Cbar-flat',2628,1690)),
	cf(or(A1,A2),phi(2630,var(0))),
	cf(or(A1,A2),cproj(2630,var(57))),
	cf(or(A3,A4),subtree(2630,'Cbar-flat',2203,1909)),
	cf(or(A3,A4),phi(2630,var(0))),
	cf(or(A3,A4),cproj(2630,var(57))),
	cf(or(A1,A2),subtree(2628,'Cbar-flat',2203,1898)),
	cf(or(A1,A2),phi(2628,var(0))),
	cf(1,subtree(2275,'ROOT',-,2270)),
	cf(1,phi(2275,var(0))),
	cf(1,subtree(2270,'CProot[std]',960,2267)),
	cf(1,phi(2270,var(0))),
	cf(1,cproj(2270,var(51))),
	cf(1,subtree(2267,'Cbar',-,2233)),
	cf(1,phi(2267,var(0))),
	cf(1,cproj(2267,var(52))),
	cf(1,subtree(2233,'Cbar-flat',2630,2123)),
	cf(1,phi(2233,var(0))),
	cf(1,cproj(2233,var(57))),
	cf(1,subtree(2203,'Cbar-flat',-,2106)),
	cf(1,phi(2203,var(0))),
	cf(1,subtree(2123,'VPART',2122,287)),
	cf(1,phi(2123,var(0))),
	cf(1,subtree(2122,'VPART',-,293)),
	cf(1,phi(2122,var(0))),
	cf(1,subtree(2106,'V[v,fin]',-,2103)),
	cf(1,phi(2106,var(0))),
	cf(1,subtree(2103,'Vx[v,fin]',2102,132)),
	cf(1,phi(2103,var(0))),
	cf(1,subtree(2102,'Vx[v,fin]',2101,135)),
	cf(1,phi(2102,var(0))),
	cf(1,subtree(2101,'Vx[v,fin]',2100,138)),
	cf(1,phi(2101,var(0))),
	cf(1,subtree(2100,'Vx[v,fin]',2099,139)),
	cf(1,phi(2100,var(0))),
	cf(1,subtree(2099,'Vx[v,fin]',2098,141)),
	cf(1,phi(2099,var(0))),
	cf(1,subtree(2098,'Vx[v,fin]',-,145)),
	cf(1,phi(2098,var(0))),
	cf(or(A3,A4),subtree(1909,'DP[std]',1899,1690)),
	cf(or(A3,A4),phi(1909,var(48))),
	cf(or(A3,A4),cproj(1909,var(29))),
	cf(or(A3,A4),subtree(1899,'DP[std]',-,1895)),
	cf(or(A3,A4),phi(1899,var(48))),
	cf(or(A1,A2),subtree(1898,'DP[std]',-,1895)),
	cf(or(A1,A2),phi(1898,var(48))),
	cf(1,subtree(1895,'DPx[std]',-,1138)),
	cf(1,phi(1895,var(48))),
	cf(1,subtree(1690,'PP[std]',-,1671)),
	cf(1,phi(1690,var(2))),
	cf(1,cproj(1690,var(34))),
	cf(1,subtree(1671,'PPx[std]',1260,1443)),
	cf(1,phi(1671,var(2))),
	cf(1,cproj(1671,var(36))),
	cf(1,subtree(1443,'DP[std]',-,1440)),
	cf(1,phi(1443,var(3))),
	cf(1,cproj(1443,var(37))),
	cf(1,subtree(1440,'DPx[std]',1276,1433)),
	cf(1,phi(1440,var(3))),
	cf(1,cproj(1440,var(39))),
	cf(1,subtree(1433,'NP',-,1411)),
	cf(1,phi(1433,var(3))),
	cf(1,subtree(1411,'NAMEP',-,1408)),
	cf(1,phi(1411,var(3))),
	cf(1,subtree(1408,'NAME',1406,269)),
	cf(1,phi(1408,var(3))),
	cf(1,cproj(1408,var(44))),
	cf(1,subtree(1406,'NAME',1404,270)),
	cf(1,phi(1406,var(3))),
	cf(1,subtree(1404,'NAME',1402,272)),
	cf(1,phi(1404,var(3))),
	cf(1,subtree(1402,'NAME',1398,274)),
	cf(1,phi(1402,var(3))),
	cf(1,subtree(1398,'NAME',-,280)),
	cf(1,phi(1398,var(3))),
	cf(1,subtree(1276,'DPx[std]',-,1208)),
	cf(1,phi(1276,var(3))),
	cf(1,cproj(1276,var(39))),
	cf(1,subtree(1260,'PPx[std]',-,1111)),
	cf(1,phi(1260,var(2))),
	cf(1,subtree(1208,'D[std]',1206,225)),
	cf(1,phi(1208,var(3))),
	cf(1,cproj(1208,var(41))),
	cf(1,subtree(1206,'D[std]',1204,243)),
	cf(1,phi(1206,var(3))),
	cf(1,subtree(1204,'D[std]',1202,244)),
	cf(1,phi(1204,var(3))),
	cf(1,subtree(1202,'D[std]',1199,246)),
	cf(1,phi(1202,var(3))),
	cf(1,subtree(1199,'D[std]',1197,233)),
	cf(1,phi(1199,var(3))),
	cf(1,subtree(1197,'D[std]',1194,235)),
	cf(1,phi(1197,var(3))),
	cf(1,subtree(1194,'D[std]',-,240)),
	cf(1,phi(1194,var(3))),
	cf(1,subtree(1138,'NP',-,1097)),
	cf(1,phi(1138,var(48))),
	cf(1,subtree(1111,'P[pre]',1109,217)),
	cf(1,phi(1111,var(2))),
	cf(1,subtree(1109,'P[pre]',1107,219)),
	cf(1,phi(1109,var(2))),
	cf(1,subtree(1107,'P[pre]',-,193)),
	cf(1,phi(1107,var(2))),
	cf(1,subtree(1097,'N[comm]',1095,171)),
	cf(1,phi(1097,var(48))),
	cf(1,subtree(1095,'N[comm]',1093,172)),
	cf(1,phi(1095,var(48))),
	cf(1,subtree(1093,'N[comm]',1091,174)),
	cf(1,phi(1093,var(48))),
	cf(1,subtree(1091,'N[comm]',1085,176)),
	cf(1,phi(1091,var(48))),
	cf(1,subtree(1085,'N[comm]',-,181)),
	cf(1,phi(1085,var(48))),
	cf(1,subtree(960,'CProot[std]',-,952)),
	cf(1,phi(960,var(0))),
	cf(1,cproj(960,var(51))),
	cf(1,subtree(952,'DP[std]',-,2788)),
	cf(1,phi(952,var(19))),
	cf(1,cproj(952,var(53))),
	cf(1,subtree(907,'NP',-,850)),
	cf(1,phi(907,var(19))),
	cf(1,subtree(861,'N[comm]',858,73)),
	cf(1,phi(861,var(19))),
	cf(1,subtree(858,'N[comm]',856,57)),
	cf(1,phi(858,var(19))),
	cf(1,subtree(856,'N[comm]',829,59)),
	cf(1,phi(856,var(19))),
	cf(1,subtree(850,'N[comm]',861,72)),
	cf(1,phi(850,var(19))),
	cf(1,subtree(829,'N[comm]',-,65)),
	cf(1,phi(829,var(19))),
	cf(1,subtree(550,'DPx[std]',-,545)),
	cf(1,phi(550,var(19))),
	cf(1,cproj(550,var(54))),
	cf(1,subtree(545,'D[std]',534,7)),
	cf(1,phi(545,var(19))),
	cf(1,cproj(545,var(56))),
	cf(1,subtree(542,'D[std]',539,11)),
	cf(1,phi(542,var(19))),
	cf(1,subtree(539,'D[std]',528,13)),
	cf(1,phi(539,var(19))),
	cf(1,subtree(534,'D[std]',542,10)),
	cf(1,phi(534,var(19))),
	cf(1,subtree(528,'D[std]',525,15)),
	cf(1,phi(528,var(19))),
	cf(1,subtree(525,'D[std]',503,17)),
	cf(1,phi(525,var(19))),
	cf(1,subtree(503,'D[std]',-,22)),
	cf(1,phi(503,var(19))),
	cf(1,subtree(328,'PERIOD',-,325)),
	cf(1,phi(328,var(0))),
	cf(1,terminal(325,'.',[325])),
	cf(1,phi(325,var(0))),
	cf(1,subtree(293,'VPART-S_BASE',-,290)),
	cf(1,phi(293,var(0))),
	cf(1,terminal(290,'aus',[284])),
	cf(1,phi(290,var(0))),
	cf(1,cproj(290,var(46))),
	cf(1,terminal(288,'+VPRE',[284])),
	cf(1,phi(288,var(0))),
	cf(1,subtree(287,'VPART-T_BASE',-,288)),
	cf(1,phi(287,var(0))),
	cf(1,subtree(280,'NAME-S_BASE',-,277)),
	cf(1,phi(280,var(3))),
	cf(1,terminal(277,'Seychellen',[264])),
	cf(1,phi(277,var(3))),
	cf(1,terminal(275,'+NPROP',[264])),
	cf(1,phi(275,var(3))),
	cf(1,subtree(274,'NAME-T_BASE',-,275)),
	cf(1,phi(274,var(3))),
	cf(1,terminal(273,'.NoGend',[264])),
	cf(1,phi(273,var(3))),
	cf(1,subtree(272,'GEND-F_BASE',-,273)),
	cf(1,phi(272,var(3))),
	cf(1,terminal(271,'.NGDA',[264])),
	cf(1,phi(271,var(3))),
	cf(1,subtree(270,'CASE-F_BASE',-,271)),
	cf(1,phi(270,var(3))),
	cf(1,subtree(269,'NUM-F_BASE',-,268)),
	cf(1,phi(269,var(3))),
	cf(1,terminal(268,'.Pl',[264])),
	cf(1,phi(268,var(3))),
	cf(1,terminal(247,'.MFN',[221])),
	cf(1,phi(247,var(3))),
	cf(1,subtree(246,'GEND-F_BASE',-,247)),
	cf(1,phi(246,var(3))),
	cf(1,terminal(245,'.Dat',[221])),
	cf(1,phi(245,var(3))),
	cf(1,subtree(244,'CASE-F_BASE',-,245)),
	cf(1,phi(244,var(3))),
	cf(1,subtree(243,'NUM-F_BASE',-,242)),
	cf(1,phi(243,var(3))),
	cf(1,terminal(242,'.Pl',[221])),
	cf(1,phi(242,var(3))),
	cf(1,subtree(240,'D-S_BASE',-,238)),
	cf(1,phi(240,var(3))),
	cf(1,terminal(238,'die',[221])),
	cf(1,phi(238,var(3))),
	cf(1,cproj(238,var(42))),
	cf(1,terminal(236,'+ART',[221])),
	cf(1,phi(236,var(3))),
	cf(1,subtree(235,'ART-T_BASE',-,236)),
	cf(1,phi(235,var(3))),
	cf(1,terminal(234,'.Def',[221])),
	cf(1,phi(234,var(3))),
	cf(1,subtree(233,'POS-F_BASE[attr]',-,234)),
	cf(1,phi(233,var(3))),
	cf(1,subtree(225,'INFL-F_BASE[det]',-,223)),
	cf(1,phi(225,var(3))),
	cf(1,terminal(223,'.St',[221])),
	cf(1,phi(223,var(3))),
	cf(1,terminal(220,'+PREP',[188])),
	cf(1,phi(220,var(2))),
	cf(1,subtree(219,'ADPOS-T_BASE[pre]',-,220)),
	cf(1,phi(219,var(2))),
	cf(1,terminal(218,'.DA',[188])),
	cf(1,phi(218,var(3))),
	cf(1,subtree(217,'CASE-F_BASE',-,218)),
	cf(1,phi(217,var(3))),
	cf(1,terminal(194,'auf',[188])),
	cf(1,phi(194,var(2))),
	cf(1,subtree(193,'ADPOS-S_BASE',-,194)),
	cf(1,phi(193,var(2))),
	cf(1,subtree(181,'N-S_BASE',-,179)),
	cf(1,phi(181,var(48))),
	cf(1,terminal(179,'Messung',[168])),
	cf(1,phi(179,var(48))),
	cf(1,terminal(177,'+NN',[168])),
	cf(1,phi(177,var(48))),
	cf(1,subtree(176,'N-T_BASE',-,177)),
	cf(1,phi(176,var(48))),
	cf(1,terminal(175,'.Fem',[168])),
	cf(1,phi(175,var(48))),
	cf(1,subtree(174,'GEND-F_BASE',-,175)),
	cf(1,phi(174,var(48))),
	cf(1,terminal(173,'.NGDA',[168])),
	cf(1,phi(173,var(48))),
	cf(1,subtree(172,'CASE-F_BASE',-,173)),
	cf(1,phi(172,var(48))),
	cf(1,subtree(171,'NUM-F_BASE',-,170)),
	cf(1,phi(171,var(48))),
	cf(1,terminal(170,'.Pl',[168])),
	cf(1,phi(170,var(48))),
	cf(1,subtree(145,'V-S_BASE',-,144)),
	cf(1,phi(145,var(0))),
	cf(1,terminal(144,'werten',[129])),
	cf(1,phi(144,var(0))),
	cf(1,terminal(142,'+V',[129])),
	cf(1,phi(142,var(0))),
	cf(1,subtree(141,'V-T_BASE',-,142)),
	cf(1,phi(141,var(0))),
	cf(1,terminal(140,'.13',[129])),
	cf(1,phi(140,var(16))),
	cf(1,subtree(139,'PERS-F_BASE',-,140)),
	cf(1,phi(139,var(16))),
	cf(1,subtree(138,'NUM-F_BASE',-,137)),
	cf(1,phi(138,var(16))),
	cf(1,terminal(137,'.Pl',[129])),
	cf(1,phi(137,var(16))),
	cf(1,subtree(135,'TENSE-F_BASE',-,134)),
	cf(1,phi(135,var(0))),
	cf(1,terminal(134,'.Past',[129])),
	cf(1,phi(134,var(0))),
	cf(1,subtree(132,'MOOD-F_BASE',-,131)),
	cf(1,phi(132,var(0))),
	cf(1,terminal(131,'.IS',[129])),
	cf(1,phi(131,var(0))),
	cf(1,terminal(74,'.NGA',[51])),
	cf(1,phi(74,var(19))),
	cf(1,subtree(73,'CASE-F_BASE',-,74)),
	cf(1,phi(73,var(19))),
	cf(1,subtree(72,'NUM-F_BASE',-,71)),
	cf(1,phi(72,var(19))),
	cf(1,terminal(71,'.Pl',[51])),
	cf(1,phi(71,var(19))),
	cf(1,subtree(65,'N-S_BASE',-,62)),
	cf(1,phi(65,var(19))),
	cf(1,terminal(62,'Forscher',[51])),
	cf(1,phi(62,var(19))),
	cf(1,terminal(60,'+NN',[51])),
	cf(1,phi(60,var(19))),
	cf(1,subtree(59,'N-T_BASE',-,60)),
	cf(1,phi(59,var(19))),
	cf(1,terminal(58,'.Masc',[51])),
	cf(1,phi(58,var(19))),
	cf(1,subtree(57,'GEND-F_BASE',-,58)),
	cf(1,phi(57,var(19))),
	cf(1,subtree(22,'D-S_BASE',-,20)),
	cf(1,phi(22,var(19))),
	cf(1,terminal(20,'die',[1])),
	cf(1,phi(20,var(19))),
	cf(1,cproj(20,var(27))),
	cf(1,terminal(18,'+ART',[1])),
	cf(1,phi(18,var(19))),
	cf(1,subtree(17,'ART-T_BASE',-,18)),
	cf(1,phi(17,var(19))),
	cf(1,terminal(16,'.Def',[1])),
	cf(1,phi(16,var(19))),
	cf(1,subtree(15,'POS-F_BASE[attr]',-,16)),
	cf(1,phi(15,var(19))),
	cf(1,terminal(14,'.MFN',[1])),
	cf(1,phi(14,var(19))),
	cf(1,subtree(13,'GEND-F_BASE',-,14)),
	cf(1,phi(13,var(19))),
	cf(1,terminal(12,'.NA',[1])),
	cf(1,phi(12,var(19))),
	cf(1,subtree(11,'CASE-F_BASE',-,12)),
	cf(1,phi(11,var(19))),
	cf(1,subtree(10,'NUM-F_BASE',-,9)),
	cf(1,phi(10,var(19))),
	cf(1,terminal(9,'.Pl',[1])),
	cf(1,phi(9,var(19))),
	cf(1,subtree(7,'INFL-F_BASE[det]',-,5)),
	cf(1,phi(7,var(19))),
	cf(1,terminal(5,'.St',[1])),
	cf(1,phi(5,var(19))),
	cf(1,semform_edge(1,22)),
	cf(1,semform_edge(3,65)),
	cf(1,semform_edge(7,145)),
	cf(1,semform_edge(8,181)),
	cf(1,semform_edge(9,193)),
	cf(1,semform_edge(11,240)),
	cf(1,semform_edge(12,280)),
	cf(1,surfaceform(325,'.',55,56)),
	cf(1,surfaceform(284,'aus',52,55)),
	cf(1,surfaceform(264,'Seychellen',41,51)),
	cf(1,surfaceform(221,'den',37,40)),
	cf(1,surfaceform(188,'auf',33,36)),
	cf(1,surfaceform(168,'Messungen',23,32)),
	cf(1,surfaceform(129,'werteten',14,22)),
	cf(1,surfaceform(51,'Forscher',5,13)),
	cf(1,surfaceform(1,'die',1,4))
	]).
