%% File xleprologlib.pl
%%   Set up foreign language interface to allow sicstus prolog to call
%%   on xle library procedures.
%%   To build xleprologlib.so:   make xleprologlib
%%   To load library into prolog:  compile(xleprologlib).


:- module(xleprologlib,
		 [init_xle/2,
		  process_tcl_events/0,
		  execute_tcl_command/2,
		  tcl/2, tcl_no_tk/2,
		  create_parser/2,
		  create_parser_tcl/2,
		  parse_sentence/4,
		  most_probable_structure/3,
		  next_graph_solution/3,
		  free_graph_solution/1,
		  reset_storage/1,
		  create_graph/2,
		  create_generator/2,
		  generate_from_graph/3,
		  %print_net_as_regexp/4, % obsolete
		  %print_genstrings/4,
		  generate_string_from_graph/3,
		  generate_from_graph_to_file/3,
		  generate_string_from_fs/3,
		  set_gen_adds/4, set_gen_adds/3,
		  read_prolog_graph_file/3,
		  print_prolog_graph_file/2,
		  make_new_choice_disjunction/3,
		  create_disjunction/4, 
		  timed_make_new_choice_disjunction/5,
		  timed_create_disjunction/4,
		  graphs_chart/2,
		  set_graph_timeout/2,
		  get_choice/4, 
		  conjoin_clauses/5, 
		  disjoin_clauses/4, 
		  subtract_clause/4, 
		  negate_clause/4, 
		  not_clause/3, 
		  assert_nogood/2,
		  assert_nogood/7,
		  evaluate_clause/3,
		  evaluate_choices/2,
		  covers_clause/3,
		  get_edge_solutions/2,
		  first_dnf_solution/3,
		  next_dnf_solution/3,
		  count_solutions/2,
		  set_solution_choice_values/2,
		  xle_true_context/1,
		  xle_false_context/1,
		  use_primary_choice_space/0,
		  use_alternate_choice_space/1,
		  reset_choice_space/0,
		  reset_choice_space/1,
		  create_fs_choice_space/3, 
		  xle_context/3, 
		  xle_safe_context/3,
		  select_choice/1,
		  set_choice_values/2,
		  unpack_choice_space/2, 
		  xle_unpack_fstr/3,
		  xle_unpack_xfr/2,
		  xle_unpack_xfr/3,
		  collect_true_facts/3,
		  safely_collect_true_facts/3,
		  name_internal_choices/2,
		  name_internal_selected_choices/2,
		  name_internal_equivs/2,
		  name_internal_context/2,
		  name_internal_boolean/2,
		  ext2int_contexts/3,
		  ext2int_contexts/5,
		  int2ext_contexts/5,    
		  named2ext_contexts/6,
		  named2int_contexts/5,
		  int2named_contexts/5,
		  ext2named_contexts/6,
		  instantiated2named_contexts/6,
		  choice_space_type/2,
		  external_context/3,
		  external_choice_nametab/2,
		  choice_name/3,
		  write_fs/1, write_fs/2, write_fs_file/2,
		  write_named_structure/2,
		  write_cf_list/1, write_cf_list/2,
		  write_named_context/1, write_named_context/2,
		  write_named_context_no_commas/1,
		  write_named_context_no_commas/2,
		  fs2graph/2,
		  graph2fs/2,
		  morph_analysis/3,
		  local_subtract_clause/5,
		  mark_choices_non_local/1,
		  clear_non_local_marks/1,
		  extract_n_best_graph/4,
		  break_text_file/3,
		  break_text/3,
		  %set_chart_prop_int/3,
		  set_performance_var/3,
		  prune_choice_space/4,
		  convert_tcl_pointer/3
		 ]).

%%% :- multifile user:file_search_path/2.

% In order to allow saved states to locate the xleprologlib.so foreign
% resource, we need to copy this file to foreign_language_interface
% subdiretories of wherever we might create a saved image.  Therefore
% we have to search around to find out where the utilities are.

%%% % 1. we are in xle/* 
%%% user:file_search_path(utls, '../prolog/utilities').
%%% % 2. we are in xle/*/*
%%% user:file_search_path(utls, '../../prolog/utilities').
%%% % 3. we are in xle/*/*/*
%%% user:file_search_path(utls, '../../../prolog/utilities').
%%% % 4. we are in xle/prolog/*
%%% user:file_search_path(utls, '../utilities').

:- use_module(library(system)).
:- use_module(library(assoc)).
:- use_module(library(lists)).
:- use_module(arrays).
:- use_module(restrict_choicespace).

:- ensure_loaded(context_convert).
:- ensure_loaded(prune_choicespace).
	  
foreign_resource(xleprologlib,
		 [init_tcl,
		  process_tcl_events,
		  execute_tcl_command,
		  create_parser,
		  
		  parse_sentence,
		  next_graph_solution,
		  free_graph_solution,
		  reset_storage,
		  p_create_graph,
		  most_probable_structure,
		  extract_n_best_graph,
		  
		  create_generator,
		  generate_from_graph,
		  %print_net_as_regexp,
		  generate_from_graph_to_file,
		  set_gen_adds,

		  read_prolog_graph_file,
		  print_prolog_graph_file,

		  create_disjunction,
		  timed_create_disjunction,
		  graphs_chart,
		  get_choice,
		  conjoin_clauses,
		  disjoin_clauses,
		  subtract_clause,
		  negate_clause,
		  not_clause,
		  p_assert_nogood,
		  evaluate_clause,
		  evaluate_choices,
		  covers_clause,
		  p_get_edge_solutions,
		  first_dnf_solution,
		  next_dnf_solution,
		  p_count_solutions,
		  select_choice,
		  set_choice_values,
		  set_solution_choice_values,
		  xle_true_context,
		  xle_false_context,

		  name_prolog_choice_space,
		  external_prolog_graph,
		  external_prolog_choices,
		  external_prolog_equivs,
		  external_prolog_context,
		  external_prolog_selected_choices,
		  morphanal,
		  
		  local_subtract_clause,
		  mark_choices_non_local,
		  clear_non_local_marks,

		  break_text_file,
		  %set_chart_prop_int
		  set_performance_var,
		  parse_Graph_ptr,
		  parse_Chart_ptr,
		  parse_NETptr_ptr,
		  parse_Clause_ptr,
		  parse_Disjunction_ptr,
		  parse_RestrictedSolution_ptr
		 ]).

% init_tcl/2: Initialize the xle
%-------------------------------------------
%  Typically, call init_xle(0,0)
foreign(init_tcl, init_xle(+integer, +integer)).


% process_tcl_events/0: 
%-------------------------------------------
foreign(process_tcl_events, process_tcl_events).


% execute_tcl_command/2: 
%-------------------------------------------
foreign(execute_tcl_command, execute_tcl_command(+string, [-string])).



% create_parser(+GrammarFile,-ParserPointer)
%-------------------------------------------
foreign(create_parser, create_parser(+string, [-address('Chart')])).





% parse_sentence(+ParserPointer,+Sentence,+RootCat,-FStrPointer)
%  RootCat is typically ''  (NULL) to use grammar's root cat
%-------------------------------------------
foreign(parse_sentence,
	parse_sentence(+address('Chart'), +string, +string,
		       [-address('Graph')])
       ).


% most_probable_structure(+FStrPointer,+WeightsFile,-FStrPointer)
%-------------------------------------------
foreign(most_probable_structure,
	most_probable_structure(+address('Graph'), +string,
				[-address('Graph')])
       ).


% extract_n_best_graph(+FStrPointer,+N, +WeightsFile,-FStrPointer)
%-------------------------------------------
foreign(extract_n_best_graph,
	extract_n_best_graph(+address('Graph'), +integer, +string,
			     [-address('Graph')])
       ).


% next_graph_solution(+FStrPointer,+LastFstrPtr,-NextFStrPt)
%  On first call, LastFstrPtr should be NULL pointer (= 0)
%-------------------------------------------
foreign(next_graph_solution,
	next_graph_solution(+address('Graph'), +address('Graph'), 
			    [-address('Graph')])
       ).





% free_graph_solution(+FStrPointer)
%-------------------------------------------
foreign(free_graph_solution,
	free_graph_solution(+address('Graph'))
       ).





% reset_storage(+ParserPointer)
%-------------------------------------------
foreign(reset_storage,
	reset_storage(+address('Chart'))
       ).





% create_graph(+ParserPointer,-GraphPointer)
%-------------------------------------------
foreign(p_create_graph,
	create_graph(+address('Chart'),[-address('Graph')])
       ).








% create_generator(+GrammarFile,-GeneratorPointer)
%-------------------------------------------
foreign(create_generator, create_generator(+string, [-address('Chart')])).






%%% % generate_from_graph(+GeneratorPointer,+FStrPointer,
%%% %                     +NormalizeInt,+UTF8Int,-WordNet)
%%% %-------------------------------------------
%%% foreign(p_generate_from_graph,
%%% 	generate_from_graph(+address('Chart'), +address('Graph'),
%%% 			    +string, +string,
%%% 			    [-address('NETptr')])
%%%        ).

% generate_from_graph(+GeneratorPointer,+FStrPointer,-WordNet)
%-------------------------------------------
foreign(generate_from_graph,
	generate_from_graph(+address('Chart'), +address('Graph'),
			    [-address('NETptr')])
       ).


% generate_from_graph_to_file(+GeneratorPointer,+FStrPointer,+File)
%-------------------------------------------
foreign(generate_from_graph_to_file,
	generate_from_graph_to_file(+address('Chart'),
				    +address('Graph'), +string) 
       ).






% print_net_as_regexp(+WordNet,+File,+NormalizeInt,+UTF8Int)
% OBSOLETE
%-------------------------------------------
%foreign(print_net_as_regexp,
%	print_net_as_regexp(+address('NETPtr'), +string,
%			    +integer, +integer)
%       ).




% set_gen_adds(+ParserPointer,+MOde, +Attrs, +OT)
%-------------------------------------------
foreign(set_gen_adds,
	set_gen_adds(+address('Chart'), +string, +string, +string)
       ).


% read_prolog_graph_file(+FileName,+ParserPointer,-FStrPointer)
%-------------------------------------------
foreign(read_prolog_graph_file,
	read_prolog_graph_file(+string, +address('Chart'), [-address('Graph')])
       ).



% print_prolog_graph_file(FileName,+FStrPointer)
%-------------------------------------------
foreign(print_prolog_graph_file,
	print_prolog_graph_file(+string, +address('Graph'))
       ).








% create_disjunction(+Graph,+InContext,+NumDisjuncts,-Disjunction)
%-------------------------------------------
foreign(create_disjunction,
	create_disjunction(+address('Graph'),+address('Clause'),+integer,
			   [-address('Disjunction')])
       ).


% timed_create_disjunction(+Graph,+InContext,+NumDisjuncts,-Disjunction)
%-------------------------------------------
foreign(timed_create_disjunction,
	timed_create_disjunction(+address('Graph'),+address('Clause'),+integer,
			   [-address('Disjunction')])
       ).



% graphs_chart(+Graph, -Chart)
%-----------------------------
foreign(graphs_chart,
	graphs_chart(+address('Graph'),[-address('Chart')])
       ).



% get_choice(+Graph,+Disjunction,+NthDisjunct,-Context)
%-------------------------------------------
foreign(get_choice,
	get_choice(+address('Graph'),+address('Disjunction'),+integer,
			   [-address('Clause')])
       ).






% conjoin_clauses(+Graph,+Context,+Context,+Prune,-Context)
%  If Prune is 1, returns false is result is nogood
%-------------------------------------------
foreign(conjoin_clauses,
	conjoin_clauses(+address('Graph'),
			+address('Clause'),+address('Clause'),
			+integer,
			[-address('Clause')])
       ).









% disjoin_clauses(+Graph,+Context,+Context,-Context)
%-------------------------------------------
foreign(disjoin_clauses,
	disjoin_clauses(+address('Graph'),
			+address('Clause'),+address('Clause'),
			[-address('Clause')])
       ).






% subtract_clause(+Graph,+Context,+FromContext,-Context)
%-------------------------------------------
foreign(subtract_clause,
	subtract_clause(+address('Graph'),
			+address('Clause'),+address('Clause'),
			[-address('Clause')])
       ).







% negate_clause(+Graph,+Context,+InContext,-NegContext)
%-------------------------------------------
foreign(negate_clause,
	negate_clause(+address('Graph'),
		      +address('Clause'),+address('Clause'),
		      [-address('Clause')])
       ).







% not_clause(+Graph,+Context,-NegContext)
%-------------------------------------------
foreign(not_clause,
	not_clause(+address('Graph'),
		   +address('Clause'),
		   [-address('Clause')])
       ).







% assert_nogood(+Graph,+Context)
%-------------------------------------------
foreign(p_assert_nogood,
	assert_nogood(+address('Graph'),
		      +address('Clause'))
       ).








% evaluate_clause(+Context,+ClosedInt,-Value)
%-------------------------------------------
foreign(evaluate_clause,
	evaluate_clause(+address('Clause'),
		        +integer,[-integer])
       ).





% evaluate_choice(+Context,-Value)
%-------------------------------------------
foreign(evaluate_choices,
	evaluate_choices(+address('Clause'),
		        [-integer])
       ).





% covers_clause(+Graph,+CoverContext,+CoveredContext,-Int)
%-------------------------------------------
foreign(covers_clause,
	covers_clause(+address('Graph'),
		      +address('Clause'),
		      +address('Clause'),
		      [-integer])
       ).





% get_edge_solutions(+EdgeGraph,-RestrictedSolution)
%  If MotherGraph is NULL, returns a single packed solution
%-------------------------------------------
foreign(p_get_edge_solutions,
	get_edge_solutions(+address('Graph'),
			   [-address('RestrictedSolution')])
       ).



% get_edge_solutions(+EdgeGraph,-RestrictedSolution)
%  If MotherGraph is NULL, returns a single packed solution
%-------------------------------------------
foreign(p_count_solutions,
	count_solutions(+address('Graph'),
			[-float])
       ).



% first_dnf_solution(+RestrictedSolution,+PrevInt,-RestrictedSolution)
%  If PrevInt = 1, start with the last solution
%-------------------------------------------
foreign(first_dnf_solution,
	first_dnf_solution(+address('RestrictedSolution'),+integer,
			   [-address('RestrictedSolution')])
       ).






% next_dnf_solution(+RestrictedSolution,+PrevInt,-RestrictedSolution)
%  If PrevInt = 1, get the previous solution
%-------------------------------------------
foreign(next_dnf_solution,
	next_dnf_solution(+address('RestrictedSolution'),+integer,
			   [-address('RestrictedSolution')])
       ).






% set_solution_choice_values(+RestrictedSolution,+Value)
%-------------------------------------------
foreign(set_solution_choice_values,
	set_solution_choice_values(+address('RestrictedSolution'),+integer)
      ).




% set_choice_values(+Clause,+integer)
%-------------------------------------------
foreign(set_choice_values,
	set_choice_values(+address('Clause'),+integer)
      ).





% select_choice(+Clause,+integer)
%-------------------------------------------
foreign(select_choice,
	select_choice(+address('Clause'))
      ).


% xle_true_context(-True)
%-------------------------------------------
foreign(xle_true_context,
	xle_true_context([-address('Clause')])
       ).


% xle_false_context(-True)
%-------------------------------------------
foreign(xle_false_context,
	xle_false_context([-address('Clause')])
       ).



% break_text_file(+Parser,+InputFileName,+OutputFileName, -Return)
%--------------------------------------------------------
foreign(break_text_file,
	break_text_file(+address('Chart'), +string, +string,
			[-integer])).


foreign(name_prolog_choice_space,
	name_prolog_choice_space(+address('Graph'))).

foreign(external_prolog_graph,
	external_prolog_graph(+address('Graph'), +string, [-term])).

foreign(external_prolog_choices,
	external_prolog_choices(+address('Graph'), [-term])).

foreign(external_prolog_equivs,
	external_prolog_equivs(+address('Graph'), [-term])).

foreign(external_prolog_context,
	external_prolog_context(+address('Clause'), +integer, [-term])).

foreign(external_prolog_selected_choices,
	external_prolog_selected_choices(+address('Graph'), [-term])).

foreign(morphanal,
	morphanal(+address('Graph'),  +string, [-string])).


		  
foreign(local_subtract_clause,
	local_subtract_clause(+address('Graph'), +address('Clause'),
			      +address('Clause'), -integer,
			      [-address('Clause')])).
foreign(mark_choices_non_local,
	mark_choices_non_local(+address('Clause'))).
foreign(clear_non_local_marks,
	clear_non_local_marks(+address('Graph'))).

%%% foreign(set_chart_prop_int,
%%% 	set_chart_prop_int(+address('Chart'), +string, +integer)). 

foreign(set_performance_var,
	set_performance_var(+address('Chart'), +string, +string)). 





foreign(parse_Graph_ptr,
	parse_Graph_ptr(+string, [-address('Graph')])).
foreign(parse_Chart_ptr,
	parse_Chart_ptr(+string, [-address('Chart')])).
foreign(parse_NETptr_ptr,
	parse_NETptr_ptr(+string, [-address('NETptr')])).
foreign(parse_Clause_ptr,
	parse_Clause_ptr(+string, [-address('Clause')])).
foreign(parse_Disjunction_ptr,
	parse_Disjunction_ptr(+string, [-address('Disjunction')])).
foreign(parse_RestrictedSolution_ptr,
	parse_RestrictedSolution_ptr(+string, [-address('RestrictedSolution')])).

:- load_foreign_resource(xleprologlib).

%======================================================================

tcl(Command,Result) :-
	process_tcl_events,
	execute_tcl_command(Command, Result),
	execute_tcl_command('set suspend_tcl 1',_).


tcl_no_tk(Command, Result) :-
	execute_tcl_command('silent_tk',_),
	execute_tcl_command(Command, Result),
	execute_tcl_command('restore_tk',_).
	

%======================================================================



covers_clause(Cover, Covered, Graph) :-
	covers_clause(Graph, Cover, Covered, Result),
	Result = 1.

%======================================================================

load_parse_grammar(Grammar, Chart) :-
	user:parse_grammar_chart(Grammar, Chart),
	!.
load_parse_grammar(Grammar, Chart) :-
	create_parser(Grammar,Chart),
	assert(user:parse_grammar_chart(Grammar, Chart)).

%======================================================================

generate_string_from_fs(Generator, FStr, String) :-
	fs2graph(FStr, Graph),
	generate_string_from_graph(Generator, Graph, String).

generate_string_from_graph(Generator, FStr, String) :-
	tmpnam(TmpFile),
	generate_from_graph_to_file(Generator, FStr, TmpFile),
	open(TmpFile, read, In),
	read_all_chars(In, String),
	close(In),
	delete_file(TmpFile).

read_all_chars(In, Chars) :-
	get_code(In, C),
	(
	  C = -1 -> Chars = []
	;
	  Chars = [C|Chars0],
	  read_all_chars(In,Chars0)
	).



set_gen_adds(Chart, Mode, Attr) :-
	set_gen_adds(Chart, Mode, Attr, '').

%======================================================================
% The external_prolog_... functions actually return named contexts in
% the forms
%      and([cv(L1,N1), cv(L2,N2)])
%       or([cv(L1,N1), cv(L2,N2)])
%       not(and([cv(L1,N1), cv(L2,N2)]))
%       cv(L1,N1)
% We need to "functorize" the boolean expressions to get
%      and(cv(L1,N1), cv(L2,N2))
%       or(cv(L1,N1), cv(L2,N2))
%       not(and(cv(L1,N1), cv(L2,N2)))
%       cv(L1,N1)

name_internal_choices(choice(ChoiceSpace),NamedChoices) :- !,
	name_internal_choices(ChoiceSpace,NamedChoices).
name_internal_choices(ChoiceSpace,NamedChoices) :-
	external_prolog_choices(ChoiceSpace,PrologChoices),
	functorize_choices(PrologChoices, NamedChoices).

name_internal_equivs(choice(ChoiceSpace),NamedEquivs) :- !,
	name_internal_equivs(ChoiceSpace,NamedEquivs).
name_internal_equivs(ChoiceSpace,NamedEquivs) :-
	external_prolog_equivs(ChoiceSpace,PrologEquivs),
	functorize_choices(PrologEquivs, NamedEquivs).

name_internal_context(XLEClause, NamedContext) :-
	external_prolog_context(XLEClause, 1, PrologContext),
	functorize_connectives(PrologContext,NamedContext).

name_internal_selected_choices(ChoiceSpace,NamedSelect) :-
	external_prolog_selected_choices(ChoiceSpace,NamedSelect).


name_internal_boolean(IntBool, Named) :-
	(
	  integer(IntBool) ->
	  name_internal_context(IntBool, Named)
	;
	  functor(IntBool, F, N) ->
	  functor(Named0, F, N),
	  name_internal_boolean_arg(N, IntBool, Named0),
	  simplify_boolean(Named0,Named)
	).

name_internal_boolean_arg(0, _IntBool, _Named) :- !.
name_internal_boolean_arg(N, IntBool, Named) :-
	arg(N, IntBool, IntBoolArg),
	arg(N, Named, NamedArg),
	N1 is N-1,
	name_internal_boolean(IntBoolArg, NamedArg),
	!,
	name_internal_boolean_arg(N1, IntBool, Named).

simplify_boolean(and(0,0), 0) :- !.
simplify_boolean(and(1,X), X) :- !.
simplify_boolean(and(X,1), X) :- !.
simplify_boolean(or(1,1), 1) :- !.
simplify_boolean(or(0,X), X) :- !.
simplify_boolean(or(X,0), X) :- !.
simplify_boolean(X,X).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Bundle up calls to context management interface in a more
%  convenient form
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



% This record is used to hold a pointer to an empty parser, which is
% used to set up choice spaces (indepedently of any particular
% grammar)
%   NOTE:  It turns out that it is *not* a good idea to use
%   choice_space_chart(Chart) as a global variable recording the
%   pointer to the choice space, to avoid passing the choice
%   space as an explicit argument to prolog predicates.
%   I'm not sure why this is --- perhaps asserting the pointer to the
%   chart does not allow you to track all the changes to the object
%   pointed at.  In any case, calling test4 in prologlibtest twice
%   causes a segmentation error, while calling test3 twice is OK.

:- dynamic xleprologlib:choice_space_chart/2,
	   xleprologlib:current_chart_num/1.

% The following two commands set a global variable to control which
% chart is used to set up a new choice space.  This is to allow
% several different choice spaces to be in use simultaneously.
% To open a second choice space, do something like:
%     use_alternate_choice_space(1),
%     create_empty_choice_space(CS),
%     use_primary_choice_space,

use_primary_choice_space :-
	retractall(xleprologlib:current_chart_num(_)),
	assert(xleprologlib:current_chart_num(0)).

use_alternate_choice_space(N) :-
	retractall(xleprologlib:current_chart_num(_)),
	assert(xleprologlib:current_chart_num(N)).

reset_choice_space :-
	xleprologlib:current_chart_num(N),
	(
	  xleprologlib:choice_space_chart(N,Chart) ->
	  reset_storage(Chart)
	;
	  otherwise -> true
	).
reset_choice_space(N) :-
	(
	  xleprologlib:choice_space_chart(N,Chart) ->
	  reset_storage(Chart)
	;
	  otherwise -> true
	).
	
:- use_primary_choice_space.

create_empty_choice_space(ChoiceSpace) :-
	get_chart(Chart),
	create_graph(Chart,ChoiceSpace).

get_chart(Chart) :-
	xleprologlib:current_chart_num(N),
	(
	    xleprologlib:choice_space_chart(N,Chart) -> true
	;
	    otherwise ->
	    create_parser('',Chart),
	    retractall(xleprologlib:choice_space_chart(N,_)),
	    assert(xleprologlib:choice_space_chart(N,Chart))
	),
	reset_storage(Chart).
	

%---------------------------------------------------------

% Don't use this --- see NOTE above:
create_fs_choice_space(PrologChoices, PrologEquivs) :-
	create_fs_choice_space(PrologChoices, PrologEquivs, _XLEChoices).

create_fs_choice_space(PrologChoices, PrologEquivs, XLEChoices) :-
	restrict_choice_space(PrologChoices,PrologEquivs,Choices,Equivs),
	create_empty_choice_space(XLEChoices),
	create_fs_choice_xors(Choices,XLEChoices),
	create_fs_choice_equivs(Equivs,XLEChoices).



%------------------

create_fs_choice_xors([],_).
create_fs_choice_xors([NonChoice|PChoices], XLEChoices) :-
	NonChoice \= choice(_,_),
	!,
	create_fs_choice_xors(PChoices, XLEChoices).
create_fs_choice_xors([choice(CVars,Condition)|PChoices], XLEChoices) :-
	make_new_choice_disjunction(CVars,Condition,XLEChoices),
	create_fs_choice_xors(PChoices, XLEChoices).

make_new_choice_disjunction(CVars,Condition,XLEChoices) :-
	(
	    ground(Condition) -> true
	;
	    otherwise ->
	    raise_exception('Under-constrained context for choice'(Condition))
	),		   
	binarize_connectives(Condition, BCondition),
	xle_context(BCondition, XLEChoices, Context),
	length(CVars,N),
	(
	    N = 1 ->  % strangely, it can happen
	    CVars = [Context]
	;
	    create_disjunction(XLEChoices,Context,N,Disjunction),
	    unify_xle_choice_xors(CVars,1,Disjunction,XLEChoices)
	).
	
	
unify_xle_choice_xors([],_,_,_).
unify_xle_choice_xors([CV|CVs],N,Disjunction,XLEChoices) :-
	get_choice(XLEChoices,Disjunction,N,CV),
	N1 is N+1,
	unify_xle_choice_xors(CVs,N1,Disjunction,XLEChoices).



% Time in ms converted to atom in sec
set_graph_timeout(Graph, Time) :-
	Time1 is (Time//1000)+1,
	number_chars(Time1,Cs),
	atom_chars(Time2,Cs),
	graphs_chart(Graph,Chart),
	set_performance_var(Chart,timeout,Time2).
	

timed_make_new_choice_disjunction(CVars,Condition,XLEChoices,
				  Time,TimeOut) :-
	(
	    ground(Condition) -> true
	;
	    otherwise ->
	    raise_exception('Under-constrained context for choice'(Condition))
	),		   
	binarize_connectives(Condition, BCondition),
	xle_context(BCondition, XLEChoices, Context),
	length(CVars,N),
	(
	  N = 1 ->		% strangely, it can happen
	  CVars = [Context],
	  TimeOut = fail
	;
	  Time < 0 ->
	  TimeOut = true
	;
	  otherwise ->
	  set_graph_timeout(XLEChoices,Time),
	  timed_create_disjunction(XLEChoices,Context,N,Disjunction),
	  (
	    Disjunction = 0 ->
	    TimeOut = true
	  ;
	    otherwise ->
	    TimeOut = fail,
	    unify_xle_choice_xors(CVars,1,Disjunction,XLEChoices)
	  )
	).


%--------------------------



create_fs_choice_equivs([],_).
create_fs_choice_equivs([define(CVar,Condition)|Equivs],XLEChoices) :- !,
	(
	    ground(Condition) -> true
	;
	    otherwise ->
	    raise_exception('Under-constrained context for define'(Condition))
	),		   
	binarize_connectives(Condition, BCondition),
	xle_context(BCondition, XLEChoices, Context),
	CVar = Context,
	create_fs_choice_equivs(Equivs,XLEChoices).
create_fs_choice_equivs([nogood(C)|Equivs],XLEChoices) :- !,
	xle_context(C,XLEChoices,Ctx),
	assert_nogood(XLEChoices,Ctx),
	create_fs_choice_equivs(Equivs,XLEChoices).
create_fs_choice_equivs([select(_CVar,_Value)|Equivs],XLEChoices) :-
	% ask John how to set values. This doesn't seem to work
	%%%select_choice(CVar),
	%%%set_choice_values(CVar,Value),
	%(
	%  Value = 1 ->
	%  xle_context(not(CVar),XLEChoices,NoGood),
	%  assert_nogood(XLEChoices,NoGood)
	%;
	%  Value = 0 ->
	%  assert_nogood(XLEChoices,CVar)
	%),
	create_fs_choice_equivs(Equivs,XLEChoices).




%---------------------------------------------------------------
% convert or(A, B, C, D)  to    or(A, or(B, or(C, D))), etc

binarize_connectives(Expr, BExpr) :-
	(
	    compound(Expr) ->
	    functor(Expr, F, N),
	    binarize_connectives(1,N,F,Expr,BExpr)
	;
	    otherwise ->
	    BExpr = Expr
	).



binarize_connectives(N,Max,Con,Expr,BExpr) :-
	arg(N,Expr,Arg),
	binarize_connectives(Arg,BArg),
	(
	    Max =:= 1, Con == not ->
	    functor(BExpr,Con,1), arg(1,BExpr,BArg)
	;
	    N =:= Max ->
	    BExpr = BArg
	;
	    otherwise ->
	    N1 is N+1,
	    functor(BExpr,Con,2),
	    arg(1,BExpr,BArg),
	    arg(2,BExpr,BRest),
	    binarize_connectives(N1,Max,Con,Expr,BRest)
	).

%------------------
% convert and([A,B,C])   to   and(A,B,C)

functorize_connectives(and([C|Cs]), And) :- !,
	functorize_connectives([C|Cs], 0, and, Ands),
	And =.. [and|Ands].
functorize_connectives(or([C|Cs]), Or) :- !,
	functorize_connectives([C|Cs], 0, or, Ors),
	Or =..[or|Ors].
functorize_connectives(not(N), not(FN)) :- !,
	functorize_connectives(N,FN).
functorize_connectives(C,C).

functorize_connectives([],_,_,[]).
functorize_connectives([C|Cs], Arity, Con, [FC|FCs]) :-
	functorize_connectives(C,FC),
	(
	  (Arity > 98, Cs =[_|_]) ->
	  functorize_connectives(Cs,0,Con,FCList),
	  FC1 =.. [Con|FCList],
	  FCs = [FC1]
	;
	  Arity1 is Arity+1,
	  functorize_connectives(Cs, Arity1, Con, FCs)
	).

functorize_choices([],[]).
functorize_choices([choice(CVs,Ctx)|Choices],
		   [choice(CVs,FCtx)|FChoices]) :-
	functorize_connectives(Ctx,FCtx),
	functorize_choices(Choices, FChoices).
functorize_choices([define(CV,Ctx)|Equivs],
		  [define(CV,FCtx)|FEquivs]) :-
	functorize_connectives(Ctx,FCtx),
	functorize_choices(Equivs, FEquivs).
functorize_choices([select(CV,Val)|Equivs],
		  [select(CV,Val)|FEquivs]) :-
	functorize_choices(Equivs, FEquivs).
functorize_choices([weight(CV,Val)|Equivs],
		  [weight(CV,Val)|FEquivs]) :-
	functorize_choices(Equivs, FEquivs).

	


%---------------------------------------------------------
% xle_context(+BooleanCondition, +ChoiceSpace, ?Context)
%  Will convert BooleanCondition to its XLE notation
%  This assumes that
%    (a) Condition is in binarized form, and
%    (b) Condition does not contain any uninstantiated variables.
%  Otherwise call xle_safe_context
%---------------------------------------------------------

xle_safe_context(Condition, XLEChoices, Context) :-
	(
	    ground(Condition) ->
	    binarize_connectives(Condition, BCondition),
	    xle_context(BCondition, XLEChoices, Context)
	;
	    % If Condition contains uninstantiated variables, do not
	    % evaluate it:
	    Context = Condition
	).


xle_context(and(A,B), XLEChoices, Context) :- !,
	xle_context(A, XLEChoices, ACtx),
	xle_context(B, XLEChoices, BCtx),
	conjoin_clauses(XLEChoices, ACtx, BCtx, 1, Context).
xle_context(or(A,B), XLEChoices, Context) :- !,
	xle_context(A, XLEChoices, ACtx),
	xle_context(B, XLEChoices, BCtx),
	disjoin_clauses(XLEChoices, ACtx, BCtx, Context).
xle_context(not(A), XLEChoices, Context) :- !,
	xle_context(A, XLEChoices, ACtx),
	xle_true_context(True),
	negate_clause(XLEChoices, ACtx, True, Context).
xle_context(A, _XLEChoices, A) :- integer(A).


%---------------------------------------------------------
%  unpack_choice_space(+ChoiceSpace, -Solution)
%    Successive backtracking through this will unpack a sequence of
%    choice space solutions.  When there are no more solutions left,
%    will set Solutions = 0.
%
%  Typical calling sequence:
%
%     repeat,
%        unpack_choice_space(ChoiceSpace, Solution),
%        do_something(Solution, ChoiceSpace, ContextedFacts),
%        solution == 0,
%        !



unpack_choice_space(ChoiceSpace,Solution) :-
	get_edge_solutions(ChoiceSpace,Solutions),
	first_dnf_solution(Solutions,0,FirstSolution),
	set_solution_choice_values(FirstSolution,1),
	!,
	(
	    Solution = FirstSolution
	;
	    unpack_next_choice(FirstSolution, Solution)
	).

unpack_next_choice(LastSolution, Solution) :-
	next_dnf_solution(LastSolution,0,NextSolution),
	set_solution_choice_values(LastSolution,0),
	(
	    NextSolution = 0 ->
	    Solution = NextSolution
	;
	    otherwise ->
	    set_solution_choice_values(NextSolution,1),
	    (	
		Solution = NextSolution
	    ;
		unpack_next_choice(NextSolution, Solution)
	    )
	).
	

xle_unpack_xfr(PackedXfr, Xfr) :-
	PackedXfr = xfr(choice(ChoiceSpace),_Eqv,_Eqs,PFS,Doc),
	Xfr = xfr([],[],[],FS,Doc),
	unpack_choice_space(ChoiceSpace,Solution),
	\+ Solution = 0,
	collect_true_facts(PFS, ChoiceSpace, FS).

xle_unpack_xfr(PackedXfr, ChoiceSpace, Xfr) :-
	PackedXfr = xfr(Choice,Eqv,Eqs,PFS,Doc),
	Xfr = xfr(Choice,Eqv,Eqs,FS,Doc),
	unpack_choice_space(ChoiceSpace,Solution),
	\+ Solution = 0,
	collect_true_facts(PFS, ChoiceSpace, FS).






xle_unpack_fstr(PackedFstr, ChoiceSpace, Fstr) :-
	PackedFstr = fstructure(Sent,Props,_Choice,_Eqv,PFS,PCS),
	Fstr = fstructure(Sent,Props,[],[],FS,CS),
	unpack_choice_space(ChoiceSpace,Solution),
	\+ Solution = 0,
	collect_true_facts(PFS, ChoiceSpace, FS),
	collect_true_facts(PCS, ChoiceSpace, CS).

xle_unpack_fstr(PackedFstr, ChoiceSpace, Fstr, NamedSelectedChoices) :-
	xle_unpack_fstr(PackedFstr, ChoiceSpace, Fstr),
	name_internal_selected_choices(ChoiceSpace, NamedSelectedChoices).

collect_true_facts([],_,[]).
collect_true_facts([cf(C,Fact)|Facts],ChoiceSpace,TrueFacts) :-
	xle_context(C,ChoiceSpace,Context),
	evaluate_clause(Context,1,Value),
	(
	    Value == 1 ->
	    TrueFacts = [cf(1,Fact)|TrueFacts1]
	;
	    otherwise ->
	    TrueFacts = TrueFacts1
	),
	!,
	collect_true_facts(Facts,ChoiceSpace,TrueFacts1).
	

safely_collect_true_facts([],_,[]).
safely_collect_true_facts([cf(C,Fact)|Facts],ChoiceSpace,TrueFacts) :-
	xle_safe_context(C,ChoiceSpace,Context),
	evaluate_clause(Context,1,Value),
	(
	    Value == 1 ->
	    TrueFacts = [cf(1,Fact)|TrueFacts1]
	;
	    otherwise ->
	    TrueFacts = TrueFacts1
	),
	!,
	safely_collect_true_facts(Facts,ChoiceSpace,TrueFacts1).
	
%---------------------------------------------------------



fs2graph(FS,Graph) :-
	tmpnam(Tmp),
	open(Tmp,write,Str),
	write_fs(Str,FS),
	format(Str,".~n",[]),
	close(Str),
	get_chart(Chart),
	read_prolog_graph_file(Tmp,Chart,Graph),
	delete_file(Tmp).

graph2fs(Graph,FS) :-
	(
	  convert_tcl_pointer(Graph, 'Graph', Graph1) ->
	  true
	;
	  integer(Graph) ->
	  Graph1 = Graph
	;
	  otherwise ->
	  raise_exception('graph2fs not provided with a graph pointer'(Graph))
	),
	external_prolog_graph(Graph1,'f:: c o::',FS0),
	FS0 = fstructure(A, B, Choices0, Eqvs0, Fstr0, Cstr0),
	(
	  member(cf(_,eq(attr(_,UnMappedAttr),_)), Fstr0),
	  'ATTR' @< UnMappedAttr, UnMappedAttr @< 'ATTS' ->
	  format(user_error, "WARNING: Unmapped ATTR in fs-graph~n",
		 []),
	  write_term(Fstr0, [indented(true)])
	;
	  otherwise -> true
	),
	FS  = fstructure(A, B, Choices1, Eqvs1, Fstr1, Cstr1),
	named2ext_contexts(Choices0,Eqvs0,x(Fstr0,Cstr0),
			   Choices1,Eqvs1,x(Fstr1,Cstr1)).
%%% graph2fs(Graph,FS) :-
%%% 	tmpnam(Tmp),
%%% 	print_prolog_graph_file(Tmp,Graph),
%%% 	open(Tmp,read,Str),
%%% 	read(Str,FS),
%%% 	close(Str),
%%% 	delete_file(Tmp).


%-------------------------------------------

morph_analysis(Chart, Word, Analyses) :-
	morphanal(Chart, Word, AnalysisAtom),
	atom_chars(AnalysisAtom, MorphString0),
	strip_leading_junk(MorphString0, MorphString1),
	strip_trailing_junk(MorphString1, MorphString),
	morph(Analyses, MorphString, []).


strip_trailing_junk(S0, S1) :-
	reverse(S0, RS0),
	strip_leading_junk(RS0, RS1),
	reverse(RS1, S1).

strip_leading_junk([0'  |RS0], RS1) :- !,
	strip_leading_junk(RS0, RS1).
strip_leading_junk([0'\n |RS0], RS1) :- !,
	strip_leading_junk(RS0, RS1).
strip_leading_junk(RS, RS).


%  DCG for morph analyses:

morph(M) --> (" " ; [0'\n]), !, morph(M).
morph(or(D))   --> open_brace, !, morph_disj(D).
morph(M) --> single_morph(M).
	
single_morph(mr(Stem,B)) --> stem(Stem),  morph_body(B).

morph_disj([D|Ds])   -->
	single_morph(D),
	(close_brace, {Ds=[]} ; bar, morph_disj(Ds)) .

stem(S) --> (" " ; [0'\n]), !, stem(S).
stem(or(S,Ss)) --> open_brace, !, stem(S), "|", stem(Ss), close_brace.
stem(S) --> stem_chars(SCs), {atom_chars(S,SCs)}.

stem_chars([C|W]) --> [C],
	        {\+ C = 0'  , \+ C = 0'\n, \+ C = 0'|,
		 \+ C = 0'}, \+ C = 0'{ },
		!, stem_chars(W).
stem_chars([])    --> [].

morph_body([or(B)|Bs])  -->
	open_brace, !, mbody_disj(B), morph_body(Bs).
morph_body([derives(B)])  -->
	derives, !, morph_body(B).
morph_body([M|Bs]) -->
	morpheme(B), !, {atom_chars(M,B)}, morph_body(Bs).
morph_body([]) --> [].

mbody_disj([D|Ds]) -->
	morph_body(D),
	(close_brace, {Ds=[]} ; bar, mbody_disj(Ds)).

morpheme(M) --> (" " ; [0'\n]), !, morpheme(M).
morpheme(M) --> [0'"], mchars(M).

mchars([]) --> [0'"], !.
mchars([C|W]) --> [C], mchars(W).

open_brace --> "{".
open_brace --> (" " ; [0'\n]), open_brace.

close_brace --> "}".
close_brace --> (" " ; [0'\n]), close_brace.

bar --> "|".
bar --> (" " ; [0'\n]), bar.

derives --> "`}".
derives --> (" " ; [0'\n]), derives.




%==============================================================

break_text_file(Parser,InFile,OutFile) :-
	break_text_file(Parser,InFile,OutFile,Result),
	Result =:= 0.

break_text(Parser,TextAtom,SentenceAtoms) :-
	tmpnam(InFile),
	tmpnam(OutFile),
	open(InFile,write,S1),
	format(S1,"~w",[TextAtom]),
	close(S1),
	break_text_file(Parser,InFile,OutFile),
	delete_file(InFile),
	open(OutFile,read,S2),
	read_broken_text(S2,[],SentenceAtoms),
	close(S2),
	delete_file(OutFile).


read_broken_text(In, CharsSoFar, Atoms) :-
	read_line(In, LineChars0),
	strip_initial_spaces(LineChars0,LineChars),
	(
	  LineChars == end_of_file ->
	  strip_initial_spaces(CharsSoFar,Chars),
	  (
	    Chars == [] -> Atoms = []
	  ;
	    otherwise ->
	    atom_chars(Atom,Chars),
	    Atoms = [Atom]
	  )
	;
	  LineChars == [0'|] -> % blank line in source
	  strip_initial_spaces(CharsSoFar,Chars),
	  (
	    Chars == [] -> 
	    Atoms = ['$blank_line'|Atoms1]
	  ;
	    otherwise ->
	    atom_chars(Atom,Chars),
	    Atoms = [Atom, '$blank_line'|Atoms1]
	  ),
	  read_broken_text(In, [], Atoms1)
	;
	  LineChars == [] -> % blank line
	  strip_initial_spaces(CharsSoFar,Chars),
	  (
	    Chars == [] ->
	    Atoms = Atoms1
	  ;
	    otherwise ->
	    atom_chars(Atom,Chars),
	    Atoms = [Atom|Atoms1]
	  ),
	  read_broken_text(In, [], Atoms1)
	;
	  otherwise ->
	  append(CharsSoFar,[32|LineChars], CharsSoFar1),
	  read_broken_text(In, CharsSoFar1, Atoms)
	).


strip_initial_spaces(end_of_file,end_of_file) :- !.
strip_initial_spaces([],[]) :- !.
strip_initial_spaces([32|Cs],SCs) :- !, %space
	strip_initial_spaces(Cs,SCs).
strip_initial_spaces([9|Cs],SCs) :- !,  %tab
	strip_initial_spaces(Cs,SCs).
strip_initial_spaces(Cs,SCs) :-
	% replace any embedded tabs by spaces
	substitute(9, Cs, 32, SCs).


%=========================================================

convert_tcl_pointer(TclPtr,Type,Ptr) :-
	atom(TclPtr),
	atom_chars(TclPtr, [0'( |Chars]),
	append(TypeChars, [0') | PtrChars], Chars),
	atom_chars(Type,TypeChars),
	atom_chars(PtrAtom,PtrChars),
	(
	  Type == 'Graph' ->
	  parse_Graph_ptr(PtrAtom,Ptr)
	;
	  Type == 'Chart' ->
	  parse_Chart_ptr(PtrAtom,Ptr)
	;
	  Type == 'NETptr' ->
	  parse_NETptr_ptr(PtrAtom,Ptr)
	;
	  Type == 'Clause' ->
	  parse_Clause_ptr(PtrAtom,Ptr)
	;
	  Type == 'Disjunction' ->
	  parse_Disjunction_ptr(PtrAtom,Ptr)
	;
	  Type == 'RestrictedSolution' ->
	  parse_RestrictedSolution_ptr(PtrAtom,Ptr)
	;
	  otherwise ->
	  raise_exception('Unexpected Tcl pointer type'(Type))
	).
