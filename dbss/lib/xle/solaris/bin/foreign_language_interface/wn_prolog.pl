% This version correctly does not sort on hyper_lists:

% (C) PARC Inc 2004
% wn_prolog.pl
% Prolog interface to wordnet libraries
% Note that your WNHOME environment variables needs to be properly set
% -- see /project/aquaint/external-resources/WordNet/README

:- module(wn_prolog, [wn/4,
		      synset/6, all_synsets/3,
		      hyper/3, hyper/4,
		      all_hypers/3, all_hypers/4,
		      all_hypers_and_synsets/4,
		      all_hyper_lists_and_synsets/4,
		      hyper_lists/3, hyper_lists/4,
		      hyponyms/4,
		      wninit/1, findtheinfo/5,
		      wn_synset_word/3,
		      wn_adv_word_adj_map/2, wn_adv_adj_map/2]).


% wn/4 Raw prolog interface, returns results as unanlyzed atom
%------------------------------------------------------------
% Example: wn(dog, ['-o', '-g'], ['-synsn'], Result).
%
% synset/6
% --------
% This is a basic call to wordnet, which will get the synsets and
% hypernym synsets for a given word / part of speech.  Backtracks
% through possible values.
% Examples
% synset(dog, noun, Sense, Class, Synset, Hypernyms)
% synset(cat, wn(1, 5, 0),  Sense, Class, Synset, Hypernyms)
%  -- the latter set up to retrieve synsets from VerbNet
%  specifications
% Note that hypernyms is a list of (immediate) hypernyms -- there may
% be more than one.
%
% hyper(+Word,+POS,-ListOfSynsets)
% hyper(+Word,+POS,+WordSynset,-ListOfSynsets)
% --------------------------------------------
% For a given word, non-deterministically return lists of hypernyms,
% one for each distinct sense
%
% all_hypers(+Word,+POS,-ListOfSynsets)
% all_hypers(+Word,+POS,+WordSynsets,-ListOfSynsets)
% --------------------------------------------
% For a given word, return lists of all hypernyms,
% merged across distinct senses
%
% hyponyms(+Word,+POS,?WordSynset,-ListOfWords)
% ---------------------------------------------
% For a given word, non-deterministically return lists of hyponymous
% words, one for each distinct sense




:- use_module(library(system)).
:- use_module(library(lists)).
:- use_module(library(ordsets)).

foreign_resource(wn_prolog,
		 [myfindtheinfo,
		  %findtheinfo,
		  wninit,
		  convert_wn_option,
		  wn_pos_argument,
		  wn_searchtype_argument,
		  wn_helpidx_argument,
		  wn_label_argument,
		  wn_all_senses,
		  wn_all_pos,
		  wn_max_pos,
		  reset_wn_opt_flags,
		  set_g_flag,
		  set_a_flag,
		  set_o_flag,
		  set_s_flag,
		  wn_help_text,
		  mymorphstr,
		  wn_lookup_synset]).

foreign(myfindtheinfo,
        findtheinfo(+string, +integer, +integer, +integer, [-string])).
%foreign(findtheinfo,
%        findtheinfo(+string, +integer, +integer, +integer, [-string])).
foreign(wninit, wninit([-integer])).
foreign(convert_wn_option, convert_wn_option(+string, [-integer])).
foreign(wn_pos_argument, wn_pos_argument([-integer])).
foreign(wn_searchtype_argument, wn_searchtype_argument([-integer])).
foreign(wn_helpidx_argument, wn_helpidx_argument([-integer])).
foreign(wn_label_argument, wn_label_argument([-string])).
foreign(wn_all_senses, wn_all_senses([-integer])).
foreign(wn_all_pos, wn_all_pos([-integer])).
foreign(wn_max_pos, wn_max_pos([-integer])).
foreign(reset_wn_opt_flags, reset_wn_opt_flags).
foreign(set_g_flag, set_g_flag).
foreign(set_a_flag, set_a_flag).
foreign(set_o_flag, set_o_flag).
foreign(set_s_flag, set_s_flag).
foreign(wn_help_text, wn_help_text(+integer, +integer, [-string])).
foreign(mymorphstr, morphstr(+string, +integer, [-string])).
foreign(wn_lookup_synset, wn_lookup_synset(+integer, +integer, [-string])).



:- load_foreign_resource(wn_prolog).

:-dynamic wn_inited/1.

:- assert(wn_inited(0)).

%------------------------------------------------------------
% wn/4 Raw prolog interface, returns results as unanlyzed atom
% Example: wn(cat, ['-o', '-g'], ['-synsn'], Result).


wn(Word, Options, SearchTypes, Results) :-
	init_wn,
	copy_term(Word,Word1),
	spaces_to_underscores(Word1,Word2),
	set_wn_options(Options, SenseNum, Help),
	map_wn(SearchTypes,Word2,SenseNum,Help,'',Results).



%-------------------------------------------------
init_wn :-
	wn_inited(1), !.
init_wn :-
	(
	  environ('WNHOME',_) ->
	  wninit(Return),
	  (
	    Return = 0 ->
	    retractall(wn_inited(_)),
	    assert(wn_inited(1))
	  ;
	    otherwise ->
	    format(user_error,"Unable to open WN database: ~w~n",
		   [Return]),
	    fail
	  )
	;
	  otherwise ->
	  format(user_error,"Environment variable WNHOME not set~n",[]),
	  fail
	).
%-------------------------------------------------

spaces_to_underscores(At0,At1) :-
	atom(At0), !,
	atom_chars(At0,AtCs0),
	spaces_to_underscores1(AtCs0,AtCs1),
	atom_chars(At1,AtCs1).
spaces_to_underscores(Num,NumAtom) :-
	number(Num),
	number_chars(Num,NumChars),
	atom_chars(NumAtom,NumChars).

spaces_to_underscores1([],[]).
spaces_to_underscores1([C |Cs], [C1 |Cs1]) :-
	(
	  C = 0'  -> C1 = 0'_
	;
	  otherwise -> C1 = C
	),
	spaces_to_underscores1(Cs,Cs1).


%-------------------------------------------------

set_wn_options(Options, SenseNum, Help) :-
	reset_wn_opt_flags,
	( member('-g', Options) -> set_g_flag ; otherwise -> true),
	( member('-a', Options) -> set_a_flag ; otherwise -> true),
	( member('-o', Options) -> set_o_flag ; otherwise -> true),
	( member('-s', Options) -> set_s_flag ; otherwise -> true),
	( member('-h', Options) -> Help = true ; otherwise -> Help=fail),
	(
	  (member(X,Options), atom_chars(X,[0'-,0'n|NChars])) ->
	  numberchars(SenseNum,NChars)
	;
	  otherwise ->
	  wn_all_senses(SenseNum)
	).

wn_search_options(ST,POSInt,SearchInt,HelpInt,Label) :-
	convert_wn_option(ST,Return),
	(
	  Return = 0 ->
	  wn_pos_argument(POSInt),
	  wn_searchtype_argument(SearchInt),
	  wn_helpidx_argument(HelpInt),
	  wn_label_argument(Label)
	;
	  otherwise ->
	  format(user_error, "Unknown search option: ~w~n", [ST]),
	  fail
	).

%-------------------------------------------------



	
map_wn([],_,_,_,Results,Results).
map_wn([ST|SearchTypes],Word,SenseNum,Help,R0,R2) :-
	wn_search_options(ST,POSInt,SearchInt,HelpInt,_Label),
	wn_all_pos(AllPOS),
	(
	  AllPOS = POSInt ->
	  wn_max_pos(MaxPOS),
	  map_wn_pos(POSInt, MaxPOS, Word, SenseNum, Help,
		     SearchInt, HelpInt, R0,R1)
	;
	  otherwise ->
	  map_wn_morph(Word,SenseNum, Help,
		       POSInt, SearchInt, HelpInt, R0,R1)
	),
	map_wn(SearchTypes,Word,SenseNum,Help,R1,R2).

map_wn_pos(MaxPOS, MaxPOS, _Word, _SenseNum, _Help,
	   _SearchInt, _HelpInt, R0,R0) :- !.
map_wn_pos(POSInt0, MaxPOS, Word, SenseNum, Help,
	   SearchInt, HelpInt, R0,R2) :-
	POSInt is POSInt0+1,
	map_wn_morph(Word,SenseNum, Help,
		     POSInt, SearchInt, HelpInt, R0,R1),
	map_wn_pos(POSInt, MaxPOS, Word, SenseNum, Help,
		   SearchInt, HelpInt, R1,R2).

map_wn_morph(Word,SenseNum, Help, POSInt, SearchInt, HelpInt, 
	     R0,R2) :-
	  (
	    (Help, HelpInt >= 0) ->
	    wn_help_text(POSInt,HelpInt,Text),
	    format("~w~n",[Text])
	  ;
	    otherwise -> true
	  ),
	  findtheinfo(Word,POSInt,SearchInt,SenseNum,R),
	  atom_concat(R0,R,R1),
	  morphstr(Word,POSInt,Lemma0),
	  map_wn_morph1(Lemma0,SenseNum, POSInt, SearchInt, R1,R2).



map_wn_morph1('',_SenseNum, _POSInt, _SearchInt, R,R) :- !.
map_wn_morph1(Lemma,SenseNum, POSInt, SearchInt, R0,R2) :-
	findtheinfo(Lemma,POSInt,SearchInt,SenseNum,R),
	atom_concat(R0,R,R1),
	morphstr('',POSInt,Lemma1),
	map_wn_morph1(Lemma1,SenseNum, POSInt, SearchInt, R1,R2).


%=========================================================================
% synset/6
% This is a basic call to wordnet, which will get the synsets and
% hypernym synsets for a given word / part of speech.  Backtracks
% through possible values.
% Examples
% synset(cat, noun, Sense, Class, Synset, Hypernyms)
% synset(cat, wn(1, 5, 0),  Sense, Class, Synset, Hypernyms)
%  -- the latter set up to retrieve synsets from VerbNet specifications


synset(Word, POS, SenseNum, Class, Synset, Hyper) :-
	wn_pos_convert(POS, POS1),
	!,
	atom_concat('-syns', POS1, SearchOpt),
	wn(Word,['-o','-a'], [SearchOpt],ResultAtom),
	atom_chars(ResultAtom,ResultString),
	parse_synset_string(ResultString,SynSetResults),
	(
	  SynSetResults = [] ->
	  SenseNum = 0, Class = null, Synset = 0, Hyper = 0
	;
	  otherwise ->
	  member(synset(SenseNum,Class,Synset, Hyper), SynSetResults)
	).

synset(Word, wn(POSNum,ClassNum,CSenseNum), SenseNum, Class, Synset, Hyper) :-
	wn_posnum_convert(POSNum,POS),
	wn_classnum_convert(ClassNum,Class),
	atom_concat('-syns', POS, SearchOpt),
	wn(Word,['-o','-a'], [SearchOpt],ResultAtom),
	atom_chars(ResultAtom,ResultString),
	parse_synset_string(ResultString,SynSetResults),
	class_sense_entry(SynSetResults,Class, CSenseNum, 
			  SenseNum, Synset, Hyper).


all_synsets(Word,POS,SynSets) :-
	wn_pos_convert(POS, POS1),
	!,
	atom_concat('-syns', POS1, SearchOpt),
	wn(Word,['-o','-a'], [SearchOpt],ResultAtom),
	atom_chars(ResultAtom,ResultString),
	parse_synset_string(ResultString,SynSetResults),
	collect_synset_results(SynSetResults,SynSets).

collect_synset_results([],[]).
collect_synset_results([synset(_SenseNum,_Class,Synset,_Hyper)|Results],
		       [Synset|SynSets]) :- !,
	collect_synset_results(Results,SynSets).
	
class_sense_entry([synset(SenseNum,Class,Synset, Hyper)|_SynSets],
		  Class, 0, SenseNum, Synset, Hyper) :- !.
class_sense_entry([synset(_,Class,_,_)|SynSets],
		  Class, N, SenseNum, Synset, Hyper) :- !,
	N > 0, N1 is N-1,
	class_sense_entry(SynSets, Class, N1, SenseNum, Synset,
			  Hyper).
class_sense_entry([_|SynSets],
		  Class, N, SenseNum, Synset, Hyper) :- !,
	class_sense_entry(SynSets, Class, N, SenseNum, Synset,
			  Hyper).




parse_synset_string(ResultString,[Sense|SynSetResults]) :-
	append(_,[0'S, 0'e, 0'n, 0's, 0'e, 0' |Tail], ResultString),
	!,
	chk_err(parse_synset_sense(Sense,Tail, ResultString1)),
	parse_synset_string(ResultString1,SynSetResults).
parse_synset_string(_,[]).

parse_synset_sense(synset(SenseNum,POS-Class,Synset, Hyper)) -->
	ws, parse_num(SenseNum), ws, "{", parse_num(Synset), "}", ws, "<",
	parse_atom(POS),".",parse_atom(Class), ">",
	read_to_eol, ws, hyperlist(Hyper).
		   
ws --> [C], {member(C, " \t\n\v\f\r")}, !, ws.
ws --> [].

pure_ws --> [C], {member(C, " \t")}, !, pure_ws.
pure_ws --> [].

parse_num(N) --> parse_num_chars(NChars), {number_chars(N,NChars)}.
parse_atom(A) --> parse_atom_chars(AChars), {atom_chars(A,AChars)}.

parse_num_chars([N|Ns]) --> [N],
	{N >= 48, N=< 57},
	!,
	parse_num_chars(Ns).
parse_num_chars([]) --> [].

parse_atom_chars([A|As]) --> [A],
	{\+ member(A, " ([{}]):.,;|*=&\n\t\v\r\f<>")},
	!,
	parse_atom_chars(As).
parse_atom_chars([]) --> [].

read_to_hyper(0) --> [C], {member(C, "\n\v\f\r")}, !.
read_to_hyper(Hyper) --> "=> ", !, read_to_hyper1(Hyper).
read_to_hyper(Hyper) --> [_], !, read_to_hyper(Hyper).
read_to_hyper(0) --> [].

read_to_hyper1(Hyper) --> ws, "{", parse_num(Hyper), "}".

read_to_eol --> [C], {member(C, "\n\v\f\r")}, !.
read_to_eol --> [_], !, read_to_eol.
read_to_eol --> [].


wn_pos_convert(noun,n).
wn_pos_convert(n,n).
wn_pos_convert(verb,v).
wn_pos_convert(v,v).
wn_pos_convert(adj,a).
wn_pos_convert(adjective,a).
wn_pos_convert(a,a).
wn_pos_convert(adv,r).
wn_pos_convert(adverb,r).
wn_pos_convert(r,r).

wn_posnum_convert(1,n).
wn_posnum_convert(2,v).
wn_posnum_convert(3,a).
wn_posnum_convert(4,r).
wn_posnum_convert(5,s).

	
wn_classnum_convert(0,adj-all).
wn_classnum_convert(1,adj-pert).
wn_classnum_convert(2,adv-all).
wn_classnum_convert(3,noun-tops).
wn_classnum_convert(4,noun-act).
wn_classnum_convert(5,noun-animal).
wn_classnum_convert(6,noun-artifact).
wn_classnum_convert(7,noun-attribute).
wn_classnum_convert(8,noun-body).
wn_classnum_convert(9,noun-cognition).
wn_classnum_convert(10,noun-communication).
wn_classnum_convert(11,noun-event).
wn_classnum_convert(12,noun-feeling).
wn_classnum_convert(13,noun-food).
wn_classnum_convert(14,noun-group).
wn_classnum_convert(15,noun-location).
wn_classnum_convert(16,noun-motive).
wn_classnum_convert(17,noun-object).
wn_classnum_convert(18,noun-person).
wn_classnum_convert(19,noun-phenomenon).
wn_classnum_convert(20,noun-plant).
wn_classnum_convert(21,noun-possession).
wn_classnum_convert(22,noun-process).
wn_classnum_convert(23,noun-quantity).
wn_classnum_convert(24,noun-relation).
wn_classnum_convert(25,noun-shape).
wn_classnum_convert(26,noun-state).
wn_classnum_convert(27,noun-substance).
wn_classnum_convert(28,noun-time).
wn_classnum_convert(29,verb-body).
wn_classnum_convert(30,verb-change).
wn_classnum_convert(31,verb-cognition).
wn_classnum_convert(32,verb-communication).
wn_classnum_convert(33,verb-competition).
wn_classnum_convert(34,verb-consumption).
wn_classnum_convert(35,verb-contact).
wn_classnum_convert(36,verb-creation).
wn_classnum_convert(37,verb-emotion).
wn_classnum_convert(38,verb-motion).
wn_classnum_convert(39,verb-perception).
wn_classnum_convert(40,verb-possession).
wn_classnum_convert(41,verb-social).
wn_classnum_convert(42,verb-stative).
wn_classnum_convert(43,verb-weather).
wn_classnum_convert(44,adj-ppl).


%==================================================
%hyper(+Word,+POS,-ListOfSynsets)
%hyper(+Word,+POS,+WordSynset,-ListOfSynsets)
% For a given word, non-deterministically return lists of hypernyms,
% one for each distinct sense

hyper(Word,POS,WordSynSet,Hyper) :-
	hyper(Word,POS,Hyper),
	Hyper = [WordSynSet|_].

hyper(Word,POS,Hyper) :-
	wn_pos_convert(POS,POS1),
	(POS1 == n ; POS1 == v),
	!,
	atom_concat('-hype',POS1,SearchOpt),
	wn(Word,['-o'], [SearchOpt],ResultAtom),
	atom_chars(ResultAtom,ResultString),
	chk_err(parse_hyper_string(HyperResults,ResultString,[])),
	(
	  HyperResults = [] -> Hyper = []
	;
	  otherwise ->
	  member(Hyper, HyperResults)
	).
hyper(Word,POS,[Synset|Hyper]) :-
	wn_pos_convert(POS,POS1),
	POS1 == a,
	!,
	atom_concat('-syns', POS1, SearchOpt),
	wn(Word,['-o','-a'], [SearchOpt],ResultAtom),
	atom_chars(ResultAtom,ResultString),
	parse_synset_string(ResultString,SynSetResults),
	member(synset(_SenseNum,_Class,Synset,Hyper), SynSetResults).
hyper(Word,POS,[Synset]) :-
	wn_pos_convert(POS,POS1),
	POS1 == r,
	!,
	atom_concat('-syns', POS1, SearchOpt),
	wn(Word,['-o','-a'], [SearchOpt],ResultAtom),
	atom_chars(ResultAtom,ResultString),
	parse_synset_string(ResultString,SynSetResults),
	member(synset(_SenseNum,_Class,Synset,_Hyper), SynSetResults).

parse_hyper_string(HyperLists) -->
	read_to_eol, 
	read_to_eol, 
	hyperlists(HyperLists).

hyperlists([[BaseSynSet|Hyper]|HyperLists]) -->
	ws, "Sense", !, read_to_eol, ws,
	"{", parse_num(BaseSynSet),"}", read_to_eol,
	ws, hyperlist(Hyper),
	ws, hyperlists(HyperLists).
hyperlists(HyperLists) -->
	ws, parse_num(_), ws, "sense", !, read_to_eol,
	ws, hyperlists(HyperLists).
hyperlists(HyperLists) -->
	ws, parse_num(_), ws, "of ", parse_num(_)," senses", !, read_to_eol,
	ws, hyperlists(HyperLists).
hyperlists([]) --> [].


hyperlist([Synset|Synsets]) -->
	"=> {", !, parse_num(Synset),"}", read_to_eol,
	ws, hyperlist(Synsets).
hyperlist([Synset|Synsets]) -->
	"=>{", !, parse_num(Synset),"}", read_to_eol,
	ws, hyperlist(Synsets).
hyperlist(Synsets) -->
	"Also See-> {", !,  read_to_eol,
	ws, hyperlist(Synsets).
hyperlist(Synsets) -->
	"Pertains to", !,  read_to_eol,
	ws, hyperlist(Synsets).
hyperlist(Synsets) -->
	"Participle of ", !,  read_to_eol,
	ws, hyperlist(Synsets).
hyperlist(Synsets) -->
	"Derived from", !,  read_to_eol,
	ws, hyperlist(Synsets).
hyperlist([Synset|Synsets]) -->
	"INSTANCE OF=> {", !,  parse_num(Synset),"}",
	read_to_eol,
	ws, hyperlist(Synsets).
hyperlist([]) --> [].



%==================================================
% all_hypers(+Word,+POS,-ListOfSynsets)
% all_hyper(+Word,+POS,+WordSynsets,-ListOfSynsets)
% --------------------------------------------
% For a given word, return lists of all hypernyms,
% merged across distinct senses, or just for the hypernyms of the
% specified word synsets in all_hyper/4

all_hypers(Word,POS,WordSynSets,AllHypers) :-
	findall(Hyper,
	        (hyper(Word,POS,Hyper),
	         member(wn(WSS,_), WordSynSets),
	         memberchk(WSS, Hyper)),
	         %%Hyper = [WSS|_]),
		HyperSets),
	merge_hyper_sets(HyperSets,[],AllHypers).

all_hypers(Word,POS,AllHypers) :-
	findall(Hyper,
		hyper(Word,POS,Hyper),
		HyperSets),
	merge_hyper_sets(HyperSets,[],AllHypers).


hyper_lists(Word,POS,WordSynSets,HyperSets) :-
	findall(Hyper,
		(hyper(Word,POS,Hyper0),
		 member(wn(WSS,_), WordSynSets),
		 memberchk(WSS, Hyper0),
		 reverse(Hyper0,RHyper0),
		 remove_duplicates(RHyper0,RHyper),
		 reverse(RHyper,Hyper)),
		 %%sort(Hyper0,Hyper)),
		 %%Hyper = [WSS|_]),
		HyperSets).
hyper_lists(Word,POS,HyperSets) :-
	findall(Hyper,
	        (hyper(Word,POS,Hyper0),
		 reverse(Hyper0,RHyper0),
		 remove_duplicates(RHyper0,RHyper),
		 reverse(RHyper,Hyper)),
	        HyperSets).


merge_hyper_sets([],AllHypers,AllHypers).
merge_hyper_sets([H|Hs],AllHypers0,AllHypers2) :-
	sort(H, SH),
	ord_union(SH, AllHypers0, AllHypers1),
	!,
	merge_hyper_sets(Hs,AllHypers1,AllHypers2).

all_hypers_and_synsets(Word,POS,AllHypers, AllSyns) :-
	findall(Hyper,
		hyper(Word,POS,Hyper),
		HyperSets),
	synsets_from_hypers(HyperSets, AllSyns),
	merge_hyper_sets(HyperSets,[],AllHypers).

synsets_from_hypers([],[]).
synsets_from_hypers([[S|_]|Hs], [S|Ss]) :- !,
	synsets_from_hypers(Hs, Ss).


all_hyper_lists_and_synsets(Word,POS,HyperLists, AllSyns) :-
	findall(Hyper,
		hyper(Word,POS,Hyper),
		HyperLists),
	synsets_from_hypers(HyperLists, AllSyns).
	%sort_hypers(HyperSets, HyperLists).

sort_hypers([],[]).
sort_hypers([H|T],[SH|ST]) :-
	sort(H,SH),
	!,
	sort_hypers(T,ST).


%==================================================
%hyponyms(+Word,+POS,?WordSynset,-ListOfWords)
% For a given word, non-deterministically return lists of hyponymous
% words, one for each distinct sense

hyponyms(Word,POS,Synset, Words) :-
	wn_pos_convert(POS,POS1),
	atom_concat('-tree',POS1,SearchOpt),
	wn(Word,['-o'], [SearchOpt],ResultAtom),
	atom_chars(ResultAtom,ResultString),
	chk_err(parse_tree_string(HypoResults,ResultString,[])),
	(
	  HypoResults = [] -> Words = [], Synset = 0
	;
	  otherwise ->
	  member(hypo(Synset,Words), HypoResults)
	).


parse_tree_string(HypoLists) -->
	read_to_eol, 
	read_to_eol, 
	read_to_eol, 
	hypolists(HypoLists).


hypolists([hypo(SynSet,Words)|HypoLists]) -->
	ws, "Sense", !, read_to_eol, ws,
	"{", parse_num(SynSet),"}", ws, read_words(Words1),
	{append(Words1,Words2,Words)},
	hypolist(Words2),
	hypolists(HypoLists).
hypolists([]) --> [].


hypolist(Words) -->
	ws, "=> {", !, parse_num(_Synset),"}", ws, read_words(Words1),
	{append(Words1,Words2,Words)},
	hypolist(Words2).
hypolist([]) --> [].

read_words([]) --> [C], {member(C, "\n\v\f\r")}, !.
read_words([Word|Words]) -->
	read_word_chars(WordChars),
	{WordChars \= []},
	{atom_chars(Word,WordChars)},
	(",",pure_ws ; pure_ws),
	!,
	read_words(Words).
read_words([]) --> [].

read_word_chars([C|Cs]) --> [C],
	{\+ memberchk(C, ",\n\v\f\r")},
	!, read_word_chars(Cs).
read_word_chars([]) --> [].


chk_err(Call) :-
	Call, !.
chk_err(Call) :-
	functor(Call,F,3),
	arg(2,Call,String),
	atom_chars(Atom,String),
	raise_exception(wordnet_parse_failure(F,Atom)).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

wn_synset_word(POS,Offset,Word) :-
	integer(Offset),
	init_wn,
	(
	  integer(POS) -> POS2 = POS
	;
	  otherwise ->
	  wn_pos_convert(POS,POS1),
	  wn_posnum_convert(POS2,POS1)
	),
	on_exception(_P,
		     wn_lookup_synset(POS2,Offset,Word),
		     (format(user_error,"Unknown synset ~w~n",[Offset]),
		      fail)).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

wn_adv_word_adj_map(Adv, PertResults) :-
	wn(Adv,['-o'], ['-pertr'],ResultAtom),
	atom_chars(ResultAtom,ResultString),
	chk_err(parse_pertr_string(PertResults,ResultString,[])).
wn_adv_adj_map(AdvSynSet, AdjSynSet) :-
	wn_synset_word(r, AdvSynSet, Adv),
	wn(Adv,['-o'], ['-pertr'],ResultAtom),
	atom_chars(ResultAtom,ResultString),
	chk_err(parse_pertr_string(PertResults,ResultString,[])),
	member(AdvSynSet-AdjSynSet, PertResults).


parse_pertr_string([AdvSynSet-AdjSynSet|PertrLists]) -->
	read_to_sense,
	!,
	"{", parse_num(AdvSynSet),"}", read_to_eol,
	ws, "Derived from adj {", parse_num(AdjSynSet),"}",
	read_to_eol, read_to_eol,
	parse_pertr_string(PertrLists).
parse_pertr_string([]) --> read_to_end.


read_to_sense --> "Sense ", !, read_to_eol.
read_to_sense --> 
	[_], !, read_to_sense.

read_to_end --> 
	[_], !, read_to_end.
read_to_end --> [].