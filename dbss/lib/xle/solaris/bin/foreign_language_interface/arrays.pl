/* (c) 2001 by the Xerox Corporation.  All rights reserved */

% File: arrays.pl
% Author: Dick Crouch
%% Creating and accessing prolog "arrays":
%%   Array represented as a single prolog vector if size is 255 or less
%%   Otherwise, a root vector contains subvectors holding values.
%%   At the moment, max array size is 64770 (255 * 254)
%%      where 255 is max size of a vector,
%%        and 254 because first arg of root vector holds size of array
%%   No update of array values possible, other than through instantiating
%%      variables
%%   Gives constant time access to values


create_array(Size,Array) :-
	(
	    Size < 256 ->
	    functor(Array,'$array',Size)
	;
	    Size > 64770 ->
	    nl,write('Maximum array size (64770) exceeded: '), write(Size),nl,
	    fail
	;
	    NumberOfVectors is (Size//255)+1,
	    RootVecSize is NumberOfVectors+1,
	    functor(Array,'$root_array',RootVecSize),
	    arg(1,Array,Size),
	    create_sub_arrays(2,Size,Array)
	).

array_size(Array, Size) :-
	functor(Array, F, N),
	(
	  F = '$array' -> Size = N
	;
	  otherwise -> arg(1, Array, Size)
	).


create_sub_arrays(Arg,Size,RootArray) :-
	Size =< 255,
	!,
	arg(Arg,RootArray,SubArray),
	functor(SubArray,'$array',Size).
create_sub_arrays(Arg,Size,RootArray) :-
	Size > 255,
	arg(Arg,RootArray,SubArray),
	functor(SubArray,'$array',255),
	Arg1 is Arg+1,
	Size1 is Size-255,
	!,
	create_sub_arrays(Arg1,Size1,RootArray).



array_val(Arg,Val,Array) :-
	(
	    functor(Array,'$array',Max) ->
	    Arg =< Max,
	    arg(Arg,Array,Val)
	;
	    arg(1,Array,Max) ->
	    Arg =< Max,
	    SubArrayNum is ((Arg-1)//255)+2,
	    Offset0 is Arg mod 255,
	    (	Offset0 = 0 -> Offset = 255 ; Offset = Offset0),
	    arg(SubArrayNum,Array,SubArray),
	    arg(Offset,SubArray,Val)
	).

