" PRS (1.0)"

"=================================================================
Trying to do partial normalization to give triples nodes uniform
labels. It's an absolute nightmare to get this stuff right.

We use the lexical id to the node number, as this is more likely to
be the same across different parses, though not guaranteed

This file defines three templates:

  label_nodes
      -- call this (with no arguments), to associate PRED derived
         labels with f-structure nodes

  label_atomic_feat(%Attr)
      -- call this for each attribute name that takes an atomic value

  label_complex_feat(%Attr)
      -- call this for each attribute name that takes a complex /
         node-valued value
		    
================================================================="

:- set_transfer_option(include_eqs, 1).

"=================
 Determine labels for nodes
 ================="


label_nodes ::
" Label fstr node having PRED but no lex_id values by final node
  Follow equalities down"

  ![PRED(%A, var(%N)), +eq(var(%N), %P),
    {%P \= var(%%)}
  ]!
  ==> PRED(%A, %P);
	   
  ![
    PRED(%A, %P), lex_id(%A, %I)
  ]!
   ==> node_label(%A, %P:%I);

  ![
    +node_label(%A, %L), +eq(%A, var(%B)) 
  ]!
   ==> node_label(var(%B), %L);

			
" Nodes that stand for atomic attribute values: "
			     
  ![eq(var(%M), %Val), {\+ %Val = var(%%)}]!
         ==> node_value(var(%M), %Val);


  ![+node_value(%A, %V), +eq(%A, var(%B)) ]!
	 ==> node_value(var(%B), %V).





" -------------------------------------
  Label attributes taking atomic values
  ------------------------------------- "
			   
label_atomic_feat(%Attr) ::
   gf(%Attr, var(%N), var(%M)),
   +node_label(var(%N), %L:%X),
   +node_value(var(%M), %MAttr) ==>
      gf(%Attr, %L:%X, %MAttr);

   "   Use this if you want *var* on disconnected attributes:
     gf(%Attr, %X, var(%N)) ==>  gf(%Attr, %X, *var*:%N);
      Or this if you want to delete them:"
   gf(%Attr, %%, var(%%)) ==> 0;

   gf(%Attr, var(%N), %Val),
   +node_label(var(%N), %L:%X)
     ==>
      gf(%Attr, %L:%X, %Val);

      
   "   Use this if you want *var* on disconnected attributes:
    gf(%Attr, var(%N), %Val) ==>  gf(%Attr, *var*:%N, %Val).
       Or this if you want to delete them:"
   gf(%Attr, var(%%), %%) ==> 0.


      

" ---------------------------------------
  Label attributes taking complex values:
  ---------------------------------------"
     
label_complex_feat(%Attr) ::
   gf(%Attr, var(%N), var(%M)),
   +node_label(var(%N), %LN),
   +node_label(var(%M), %LM) ==>
      gf(%Attr, %LN, %LM);


      
   "   Use this if you want *var* on disconnected attributes:
    gf(%Attr, var(%N), %Val) ==>  gf(%Attr, *var*:%N, %Val);
      Or this if you want to delete them:"
   gf(%Attr, var(%%), %%) ==> 0;

   "   Use this if you want *var* on disconnected attributes:
   gf(%Attr, %X, var(%N)) ==>  gf(%Attr, %X, *var*:%N).
      Or this if you want to delete them:"
   gf(%Attr, %%, var(%%)) ==> 0.

  