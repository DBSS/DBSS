" PRS (1.0)"

" (C) PARC 2004-2005
  File: fs_triples.pl
  Purpose:  This is a file that maps f-structures through to a
  basic triples format, preserving all f-structure attributes.  It is
  used for regression testing of f-structures.

"



grammar = fs_triples.

:- set_transfer_option(conflict_resolution, 0).
:- set_transfer_option(garbage_collection, 0).
:- set_transfer_option(treat_subsumes_as_eq, 1).
:- set_transfer_option(include_eqs, 1).
:- set_transfer_option(include_proj, 1).
:- set_transfer_option(extra, [cf(1,root(var(0),x))]).


arg(%%,%%,%%) ==> 0.
nonarg(%%,%%,%%) ==> 0.
LEFT-SISTER(%%,%%) ==> 0.
RIGHT-SISTER(%%,%%) ==> 0.
LEFT-DAUGHTER(%%,%%) ==> 0.
RIGHT-DAUGHTER(%%,%%) ==> 0.
LEFT_SISTER(%%,%%) ==> 0.
RIGHT_SISTER(%%,%%) ==> 0.
LEFT_DAUGHTER(%%,%%) ==> 0.
RIGHT_DAUGHTER(%%,%%) ==> 0.
MOTHER(%%,%%) ==> 0.
f::(%%,%%) ==> 0.
in_set(rule_trace(%%,%%,%%,%%,%%,%%),%%) ==> 0.
in_set(rule_trace(%%,%%,%%,%%),%%) ==> 0.
RULE-TRACE(%%,%%) ==> 0.
		  

"==============
Remove projections
==============="

o::(%%, var(%X)) ==>  to_delete(var(%X)).

  +to_delete(%X),
  ((qp(%P, [%X, var(%Y)]), {%P \= in_set})
  |
   in_set(var(%Y), %X)
  )
 *=>
  to_delete(var(%Y)).

+to_delete(%X), in_set(%%, %X) ==> 0.
+to_delete(%X), qp(%%, [%X, %%]) ==> 0.
to_delete(%%) ==> 0.

	  
"==============
 flatten sets:
=============="

+ADJUNCT(%X,%Y), in_set(%Z,%Y) ==> ADJUNCT_x(%X,%Z).

+NAME-MOD(%X,%Y), in_set(%Z,%Y) ==> NAME-MOD_x(%X,%Z).

+MOD(%X,%Y), in_set(%Z,%Y) ==> MOD_x(%X,%Z).
     
" for older grammars: "
+APP(%X,%Y), -in_set(%%,%Y) ==> NAME-MOD_x(%X,%Y).
+APP(%X,%Y), in_set(%Z,%Y)   ==> NAME-MOD_x(%X,%Z).



     
" Try to improve pred labelling on coordinations
  =============================================="
      

+COORD(%X, +_)  ==> is_coord(%X).
+CONJ(%X, +_)   ==> is_coord(%X).
"For sets that have no preds, and are
  not adjuncts, mods or coordinations.   Treat them as list
  coordinations.  See for example /project/nltt-2/TESTDATA/10-01-02/G5.pl"
+in_set(var(%%), var(%X)), -PRED(var(%X),%%), -is_coord(var(%X)) ==> 
   is_coord(var(%X)).


	
+is_coord(%X)  ==>
  coord_preds(%X, []),
  "introduce temporary PRED for coordinations"
  PRED(%X, coord).

+is_coord(%X), +in_set(%Z, %X), +PRED(%Z,%P) **
	[coord_preds(%X,%L) ==> coord_preds(%X,[%P|%L])].

" remove temporary PRED for coordinations"
PRED(%%, coord) ==> 0.

coord_preds(%X,%L0), {sort(%L0, %L)} ==> coord_preds(%X, %L).

+is_coord(%X), in_set(%Z, %X) ==> COORD-ELEM(%X, %Z).


+COORD-FORM( %X,%F), {%F \= var(%%)} ==> coord_form(%X,%F).

![+COORD-FORM( %X,%F), {%F = var(%%)}, +eq(%F, %V), {%V \= var(%%)}]!
	    ==> coord_form(%X,%V).

+is_coord(%X), -coord_form(%X,%%) ==> coord_form(%X, null).

![coord_preds(%X,%L), coord_form(%X,%F),
   -PRED(%X,%%), -lex_id(%X,%%),
   {concat_atom_list([coord,%F|%L],_,%Pred)},
   {%X = var(%I)}
 ]!
 ==> PRED(%X,%Pred), lex_id(%X,%I).
     

is_coord(%%) ==> 0.
coord_preds(%%,%%) ==> 0.
coord_form(%%,%%) ==> 0.
      

ADJUNCT(%%,%%) ==> 0.
ADJUNCT_x(%X,%Z) ==> ADJUNCT(%X,%Z).

NAME-MOD(%%,%%) ==> 0.
APP(%%,%%) ==> 0.
NAME-MOD_x(%X,%Z) ==> NAME-MOD(%X,%Z).

MOD(%%,%%) ==> 0.
MOD_x(%X,%Z) ==> MOD(%X,%Z).




root(%B,%%), -PRED(%B,%%) ==> PRED(%B,root), lex_id(%B,0). 


" Now that PSEM can have set values, we don't want to remove all
these. The following commented out  rule was originally meant to get
    rid of optimility values"
    
"in_set(%Atom,%%), {%Atom \= var(%%)} ==> 0."

"============================
 flatten internal structure:
============================"

  ![
    qp( %P1,[%A,var(%B)]), {%P1 \= in_set}, {%P1 \= eq},  {%P1 \= scopes},
    -(eq(var(%B), %V), {%V \= var(%%)}), -PRED(var(%B),%%),
    +qp(%P2,[var(%B),%%]), {%P2 \= in_set}, {%P2 \= eq}, {%P2 \= scopes}
   ]!
==>
  internal_node(%A, var(%B), %P1).


  ![
    +internal_node(%A, %B, %PA), +internal_node(%B, %C, %PB),
    +internal_node(%C, %D, %PC), +internal_node(%D, %E, %PD),
    qp(%P,[%E,%V]), {%P \= eq}, -internal_node(%E,%%,%P)
   ]!
==>
  qp(%P,[%A,%V,[%PA,%PB,%PC,%PD]]).


  ![
    +internal_node(%A, %B, %PA), +internal_node(%B, %C, %PB),
    +internal_node(%C, %D, %PC), 
    qp(%P,[%D,%V]), {%P \= eq}, -internal_node(%D,%%,%P) 
   ]!
==>
  qp(%P,[%A,%V,[%PA,%PB,%PC]]).

     
  ![
    +internal_node(%A, %B, %PA), +internal_node(%B, %C, %PB),
    qp(%P,[%C,%V]), {%P \= eq}, -internal_node(%C,%%,%P) 
   ]!
==>
  qp(%P,[%A,%V,[%PA,%PB]]).

     
  ![
    +internal_node(%A, %B, %PA), 
    qp(%P,[%B,%V]), {%P \= eq}, -internal_node(%B,%%,%P) 
  ]!
==>
  qp(%P,[%A,%V,[%PA]]).


  internal_node(%%,%%,%%) ==> 0.

"=================
 labelling nodes:
================="

include(node_label_rules).

@label_nodes.


node_label(%%,eq:%%) ==> 0.
" We need to keep some the of the equalities in case of dangling fstr
nodes. But make it a 4-place relation, so that it doesn't get relabelled"
eq(var(%A),var(%B)) ==> eq(%A, %B, var, var).
eq(%%, %%) ==> 0.

+in_set(%Atom,var(%V)), {%Atom \= var(%%)}, -node_label(var(%V),%%) ==>
 node_label(var(%V), set_valued:%V).
   

label_feats ::
  ![
    qp(%P, [var(%N), %Y]), {%P \= node_label},
    +node_label(var(%N), %L:%X)
   ]!
   ==>
    qp(%P, [%L:%X, %Y]);


  ![
    qp(%P, [var(%N), %Y, %PATH]),
    +node_label(var(%N), %L:%X)
   ]!
   ==>
    qp(%P, [%L:%X, %Y, %PATH]);


  ![
    qp(%P, [%L, var(%M)]),
    +node_value(var(%M), %Val)
   ]!
  ==>
    qp(%P, [%L, %Val]);


  ![
    qp(%P, [%L, var(%M), %PATH]),
   +node_value(var(%M), %Val)
   ]!
  ==>
    qp(%P, [%L, %Val, %PATH]);


   ![
    qp(%P, [%L, var(%M), %PATH]),
    +node_label(var(%M), %L1:%Y)
    ]!
  ==>
    qp(%P, [%L, %L1:%Y, %PATH]);


 ![
    qp(%P, [%L, var(%M)]),
    +node_label(var(%M), %Label)
   ]!
  ==>
    qp(%P, [%L, %Label]).




@label_feats.

node_label(%%,%%) ==> 0.
node_value(%%,%%) ==> 0.
internal_node(%%,%%, %%) ==> 0.
lex_id(%%,%%) ==> 0.
root(%%,%%) ==> 0.

" last ditch, in case node labelling hasn't worked.
  Unlike fs_full_match_triples, this will leave in attributes that
    have not been fully resolved, marking the arguments as var:N"

var_eq(%A, %B) :=
    ((-eq(%A,%%,var,var), {%B = %A})
    |
     (+eq(%A, %B, var, var), -eq(%B, %%, var, var))
    ).
    
qp(%P, [var(%N), %Val]), var_eq(%N, %M) ==> qp(%P, [var:%M, %Val]).
qp(%P, [%Val, var(%N)]), var_eq(%N, %M) ==> qp(%P, [%Val, var:%M]).
qp(%P, [var(%N), %Val, %Path]), var_eq(%N, %M)
   ==> qp(%P, [var:%M, %Val, %Path]).
qp(%P, [%Val, var(%N), %Path]), var_eq(%N, %M)
   ==> qp(%P, [%Val, var:%M, %Path]).

eq(%%,%%,%%,%%) ==> 0.
   



