/* (c) 2002-2004 by the Palo Alto Research Center.  All rights reserved */
/* (c) 1996-2001 by the Xerox Corporation.  All rights reserved */

#ifndef DU_TYPEDEFS_
#define DU_TYPEDEFS_

/* basic stuff */

typedef struct Storage Storage;

/* from compstate.h */

typedef struct DUProp DUProp;
typedef struct DUCompState DUCompState;
typedef struct OTMark OTMark;
typedef struct ConstraintData ConstraintData;
typedef enum FieldName FieldName;
typedef struct StringList StringList;
typedef struct NumberList NumberList;
typedef struct GlobalGensym GlobalGensym;
typedef struct DynamicAttrSymbolsTable DynamicAttrSymbolsTable;

/* from graph.h */

typedef struct Graph Graph;
typedef struct GraphList GraphList;
typedef struct GraphClauses GraphClauses;
typedef enum GraphFlags GraphFlags;
typedef struct Gensym Gensym;
typedef struct CopiedGensym CopiedGensym;
typedef struct TypedValueList TypedValueList;
typedef struct HeadPrecedenceCache HeadPrecedenceCache;
typedef struct PushDef PushDef;
typedef struct PushedFU PushedFU;
typedef struct FUPusher FUPusher;
typedef struct AVPairList AVPairList;
typedef struct SuppressionIndex SuppressionIndex;
typedef struct SuppressedFact SuppressedFact;

typedef struct CSubTree CSubTree;
typedef struct CSubTreeList CSubTreeList;

/* from justifications.h */

typedef struct Justification Justification;
typedef struct JustifiedFact JustifiedFact;
typedef char Rule;

/* from values.h */

typedef struct CVPair  CVPair;
typedef enum CVProps CVProps;
typedef enum Completed Completed;
/* #define CVProps enum CVPropsEnum */

typedef struct TypedValue  TypedValue;
typedef enum ValueType     ValueType;

/* from attributes.h */

typedef struct AVPair AVPair;
typedef enum AttrID AttrID;
typedef enum VarType VarType;
typedef struct SetAbstraction SetAbstraction;
typedef struct AVPArray AVPArray;

/* from relations.h */

typedef struct RVPair  RVPair;
typedef enum RelID     RelID;
typedef enum RelType   RelType;

/* from clause.h */

typedef struct Clause Clause;
typedef struct ClauseCache ClauseCache;
typedef struct ClauseStack ClauseStack;
typedef struct Disjunction Disjunction;
typedef struct DisjunctionList DisjunctionList;
typedef enum ClauseType    ClauseType;
typedef enum ClauseProps   ClauseProps;
typedef enum ClauseOp   ClauseOp;
typedef enum ChoiceLabelType ChoiceLabelType;
typedef enum NogoodReason  NogoodReason;
typedef struct feature_weight_pair feature_weight_pair;

/* from solutions.h */

typedef struct InternalSolution      InternalSolution;
typedef struct InternalSolutionsList InternalSolutionsList;
typedef struct RestrictedSolution    RestrictedSolution;
typedef struct SolutionList          SolutionList;
typedef struct RestrictionSet        RestrictionSet;
typedef struct SolutionIndex         SolutionIndex;
typedef signed short OTCount;

/* from unify-queue.h */
typedef struct UnificationQueue UnificationQueue;
typedef struct SubsumptionQueue SubsumptionQueue;
typedef Clause *(ContextPair)[2];

typedef unsigned int ArgList;
typedef unsigned int LocalAttrs;

#ifndef CHART_TYPEDEFS_
typedef struct Chart Chart;
#ifndef C_FSM_TYPES /* defined in the c-fsm package */
typedef struct NETtype *NETptr;
typedef struct STATE *STATEptr;
typedef struct HEAP *HEAPptr;
typedef struct HASH_TABLE *HASH_TABLEptr;
#endif

#ifndef __LFG_H_INCLUDED__
typedef struct Grammar Grammar;
typedef struct SExp SExp;
typedef struct SExpArray SExpArray;
#endif
#endif

#endif
