/* (c) 2002-2005 by the Palo Alto Research Center.  All rights reserved */
/* (c) 1998-2001 by the Xerox Corporation.  All rights reserved */
#ifndef REGEXP_H
#define REGEXP_H

/*************************************************************/
/* A simple regular expression package.                      */
/*************************************************************/

typedef enum RegExpType RegExpType;

enum RegExpType {
  RE_LABEL,      /* The label from a network */
  RE_SEQUENCE,   /* a sequence (concatenation) */
  RE_ALTSET,     /* set of alternatives */
  RE_ZEROSTAR,   /* Kleene star */
  RE_OPTION,     /* an optional regular expression */
  RE_SYMBOLPAIR, /* a pair of symbols */
};

struct RegExp {
  RegExpType type;
  int size;
  int char_pos;  /* the left character position of an RE_LABEL */
  int16 label;   /* used by RE_LABEL */
  RegExp *op1;   /* used by everything but RE_LABEL */
  RegExp *op2;   /* used by RE_SEQUENCE & RE_ALTSET */
};

RegExp *re_option(RegExp *re1, HEAPptr heap);
RegExp *re_altset(RegExp *re1, RegExp *re2, HEAPptr heap);
RegExp *re_sequence(RegExp *re1, RegExp *re2, HEAPptr heap);
RegExp *re_zerostar(RegExp *re1, HEAPptr heap);

RegExp *re_symbol_pair(char *upper, char *lower, HEAPptr heap);
/* Create a regular expression that represents the upper and  */
/* lower symbols of a two-level pair.  If upper = lower, then */
/* this is equivalent to a one-level symbol.  Epsilon is      */
/* represented by NULL or "", not "0".  When re_symbol_pair   */
/* is being used to produce output for analyze_to_regexp in   */
/* a grammar library, upper and lower should be the same.     */

RegExp *re_symbol_pair_pos(char *upper, char *lower, int char_pos, 
			   HEAPptr heap);
/* Like re_symbol_pair, but includes the character position. */

HEAPptr init_heap(size_t cell_size, size_t block_size, char *name);
void free_heap(HEAPptr heap);
void reset_heap(HEAPptr heap);

NETptr regexp_to_fsm(RegExp *regexp);

RegExp *fsm_to_regexp(NETptr net, HEAPptr re_heap);
/* Convert an fsm to a regular expression with:     */
/* heap = init_heap(sizeof(RegExp), 100, "RegExp"); */
/* regexp = fsm_to_regexp(net, heap);               */
/* ...                                              */
/* free_heap(heap);                                 */
#endif
