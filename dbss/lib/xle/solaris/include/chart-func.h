/* (c) 2002-2004 by the Palo Alto Research Center.  All rights reserved */
/* (c) 1996-2001 by the Xerox Corporation.  All rights reserved */

#ifndef CHART_FUNC_INCLUDED_
#define CHART_FUNC_INCLUDED_

Chart *create_chart(enum ChartMode, enum ChartDirection, Grammar *,
		    ScheduleFN, UnScheduleFN);

void register_parametrized_functions(Chart *, Hash_HashFN, Hash_CompFN,
				     SpanningEdgeFN);

void initialize_chart(Chart *);

void cleanup_chart(Chart *);

void free_chart(Chart *);

Agenda *create_agenda(int);

void increase_agenda(Agenda *);

void combine_partial_and_complete(Requestor *, Edge *, Chart *);

Edge *put_lexical_edge(Chart *, Vertex *, int, int, char *, SExp *,
		       int, int, int, struct Graph *);

void update_requestors(Vertex *, Requestor *, Chart *);

void update_completes(Vertex *, Edge *, Chart *);

Vertex *get_vertex(Chart *, CategoryID, int, int,int, STATEptr);

Vertex *get_subrule_vertex(Chart *, CategoryID, int, int,int, 
			   STATEptr, STATEptr);

unsigned int vertexhashfn(Vertex *);

int vertexcomparefn(Vertex *, Vertex *);

Vertex *vertexcombinefn(Vertex *, Vertex *);

void enlarge_vertex_data_array(Chart *, int);

Edge *get_edge(Chart *, Vertex *, STATEptr, int, long);

Edge *get_source_edge(Chart *, Vertex *, STATEptr, int, long);

Edge *duplicate_edge(Chart *, Edge *);

unsigned int edgehashfn(Edge *);

int edgecomparefn(Edge *, Edge *);

Edge *edgecombinefn(Edge *, Edge *);

unsigned int strhashfn(CategoryID);

SubTree *create_subtree(Edge *, Edge *, ARCptr, SExp *, struct Graph *,
			Chart *);

Requestor *create_requestor(Edge *, ARCptr, struct Graph *, Chart *);

void fInsert_SubTreeLink(SubTreeLink **ppSubTreeLink, 
                                SubTree *pSubtree, Edge *edge);

void apply_to_edges(Chart *, void (*)(Edge *));

void apply_to_edges1(Chart *, void (*)(Edge *, void *), void *);

void apply_to_edges2(Chart *, void (*)(Edge *, void *, void *), 
		     void *, void *);

void print_charts_forest(Chart *);

void print_forest(Edge *, int);

void print_edge(char *label, Edge* edge);

void print_chart_soln_info(Chart *chart, Edge *edge, int dograph);

void print_edge_subtree_info(Edge *edge, int dograph);

Edge *get_root_edge(Chart *);

void clear_root_edge(Chart *);

void mark_edges(Edge *, int);

void mark2_edges(Edge *, int);

void mark_chart_edges(Chart *, int);

void break_nonbranching_cycles(Chart *);

void unwind_cycles(Edge *);

void set_edge_prop(Edge *edge, char* propname, void *value);
/* Set edge's propname property to be value. */

void *get_edge_prop(Edge *edge, char* propname);
/* Get edge's propname property value. */

void set_chart_prop(Chart *chart, char* propname, void *value);
/* Set chart's propname property to be value. */

void *get_chart_prop(Chart *chart, char* propname);
/* Get chart's propname property value. */

Edge *exists_edge(Chart *chart, Vertex *vertex, STATEptr state, 
		  int right_vertex, int);

int analyze_subtree_component(Chart *, Edge *);

void process_gen_complete(Chart *, Edge *partial,
			  struct Graph *subtree_graph, STATEptr state,
			  Edge *complete, Requestor *requestor, int, int);

SExp *get_subtree_constraints(SubTree *subtree, Edge *edge);
/* Get the constraints associated with a subtree. */

void sort_subtrees(SubTree **list, int orderfn(SubTree *, SubTree *));

void fClear_edge_counts(Chart *pChart);

float count_edge_trees(Edge *pEdge, int is_partial);

void save_network_on_chart(Chart *chart, NETptr net);

void save_hash_table_on_chart(Chart *chart, void *hash_table);

int sublexical_edge(Edge *edge);

int sublexical_cat(char *cat);

void add_surface_corr(Edge *edge, Edge *surface);

EdgeList *intersect_surface_forms(EdgeList *list1, EdgeList *list2);

int set_performance_vars(Chart *chart, char *filename);

int edge_always_headed(Edge *edge);

void fAppend_edgelist(EdgeList **ppFront, EdgeList *pBack);

/* ------------------------------------------------------------------------- */

void check_chart(Chart *chart);
void check_edge(Edge *void_edge);

#endif
