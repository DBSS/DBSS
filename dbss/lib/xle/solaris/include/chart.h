/* (c) 2002-2004 by the Palo Alto Research Center.  All rights reserved. */
/* (c) 1996-2001 by the Xerox Corporation.  All rights reserved */
#ifndef CHART_INCLUDED_
#define CHART_INCLUDED_
#include <time.h>

enum ChartMode {
  ChartParsing, ChartGeneration, ChartTransfer
};

enum ChartDirection {
  ChartTopDown, ChartBottomUp
};

#define  MAX_HEADWORDS 150 
#define  MAX_FU_FILTER_RESULTS  100 
#define  MAX_PARTIAL_PATHS 1000
#define  MAX_UNKNOWN_MORPHEMES 100

struct FUFRslt {
  int goal;
  void *context;
  void *avpair; 
};

struct TokenVertexMap {
  int token_id;
  int vertex_id;
  TokenVertexMap *next;
};

struct VertexData {
  TokenVertexMap *map;
  EdgeList *tokens;
  Edge *unknown_token;
  unsigned int left_char_pos;
  unsigned int right_char_pos;
  unsigned int avpair_id;
  unsigned int connected : 1;
  unsigned int allows_preceding_nonterminal : 1;
  unsigned int embedded : 1;
  unsigned int end_of_input : 1;
  unsigned int mark : 1;
};

struct FactData {
  int id;
  struct AVPair *avp;
  struct AVPair *attr;
  struct CVPair *cvp;
};

struct Chart {

  void *compstate;			/* The DUCompState for this chart. */

  enum ChartMode mode;			/* is this chart being used for */
					/* parsing or generation? */
		
  enum ChartDirection direction;	/* should the chart be built in a */
					/* top-down or bottom up fashion? */

  union ChartGoal {			/* the top level goal */

    NETptr expression;			/* in parsing, input word net */

    void *goal;				/* used as generic type by chart */

  } goal;

  int missing_fact_ok;                  /* missing facts are ok in
					   generation */
  int duplicate_semform_id_ok;          /* duplicate semforms are ok in
					   generation */

  int aborted;				/* chart processing was aborted. */
  int truncated;                        /* chart processing was truncated. */
  int input_count;                      /* number of inputs since last
					   reset */
  int lexicon_resets;                   /* number of times the lexicon */
                                        /* cache was reset by grammar. */

  Grammar *grammar;			/* the grammar used with this chart */
  void *string_table;                   /* the lexicon string table */
  void *lexicon_heap;                   /* the heap for the lexicon */
  void *eff_info;                       /* effective info for the lexicon */
  void *lexicon_section;                /* lexicon_section[0] of grammar */
  char *rootcat;			/* the name of the root category */
  char *reparse_cat;                    /* the name of the reparse category */
  int reparsing;                        /* Whether we are reparsing. */

  STATEptr morph_fs_start;		/* in the new generator, this is a */
					/* part of the root category */

  /* ----------------------------------------------------------------------- */

  HASH_TABLEptr vertices;		/* all of the vertices */

  HASH_TABLEptr edges;			/* all of the non-duplicate edges */

  Hash_HashFN edgehashfn;		/* hash key function */
  Hash_CompFN edgecompfn;		/* hash comparison function */

  Edge *duplicates;                     /* All the duplicate edges */

  /* ----------------------------------------------------------------------- */

  int num_edges;			/* the number of edges in use */

  int num_subtrees;			/* the number of subtrees in use */

  int num_vertices;			/* the number of vertices in use */

  int max_vertex_index;			/* the maximum Vertex->id.index. */

  int *embedded_vertex;                   /* whether the vertex is embedded
					     in the chart. */

  Agenda *agenda;			/* THE AGENDA */

  int suppress_agenda;                  /* don't add to agenda */

  SpanningEdgeFN spanning_edge;  /* a function to determine whether an edge */
				 /* is a spanning edge.  The parser and */
				 /* generator use different functions. */

  HEAPptr *heaps;			/* array of fsm heap pointers */

  int max_heap;				/* index of last heap in the array */

  NETptr *networks;                     /* array of networks used. */

  int networks_size;                    /* the size of the network. */

  int num_networks;                     /* number of networks in the array */
 
  void **hash_tables;                   /* array of hash tables used. */

  int hash_tables_size;                 /* the size of the array. */

  int num_hash_tables;                  /* number of hashtables in the array */
 
  NETptr words_fsm;			/* the input network. */

  NETptr genstrings;			/* the output of the generator. */

  struct Graph *goal_graph;		/* generation goal graph */

  int max_goal_semform_id;		/* for gen - maximum semform id */

  int min_goal_semform_id;		/* for gen - minimum semform id */

  int num_subgoals;			/* for gen - number of goals */

  struct AVPair **subgoals;

  void  *gen_good_trees;		/* gen output trees */

  GoalCtl *goalctl;			/* gen cache for att_value pairs in */
					/* each goal used in lexicon lookup */

  /* for storage of goal attribute-value lists */
  char *goal_stg_buf;  
  char *goal_stg_next;
  char *goal_stg_max;

  int16 gen_pred_ct;			/* for regression statistics */

  Edge *empty_edges;			/* gen: allow edges to be recycled */

  int last_agenda_n_of_edges;		/* gen: size of prev agenda */

  VertexData *vertex_index;		/* Index of information stored */
					/* with vertices */

  int vertex_index_size;		/* Number of VertexData allocated. */

  Edge *tagedge;

  ChartProp *props;			/* client properties */

  int coord_partial_inc; 
  int constraint_has_uncertainties;	/* gen goal predict */

  char            last_attr_matched[64];
  int             last_attr_addable;  

  /* for storage of goal partial paths, for underspecified input */
  int             num_partial_paths; 
  char           *partial_paths[MAX_PARTIAL_PATHS];
  char           *partial_path_stg_buf;
  char           *partial_path_stg_next;
  char           *partial_path_stg_max; 
  int             unspecified_synfuns;
  int             headword_ct;		/* temp for gen-lex lookup, */
  char           *headwords[MAX_HEADWORDS];  /* to avoid duplicates */
  struct SynMap  *synmap;
  int            num_unknown_morphemes;
  char           *unknown_morphemes[MAX_UNKNOWN_MORPHEMES];

  EdgeList *cycle_entry_points;
  void *nec_parent_coverage;
  void *dependent_coverage;
  HASH_TABLEptr prediction_graphs;

  HASH_TABLEptr threaded_edge_hash;
  HASH_TABLEptr threaded_constraints_hash;

  /* --------------- */
  /* For translation */
  /* --------------- */
  int next_fact_id;
  FactData **facts;
  HASH_TABLEptr fact_hash;
  
  char *transfer_rule_file;
  HASH_TABLEptr transfer_rule_hash;
  HASH_TABLEptr transfer_index_hash;
  void *lexical_cooccurrence_db;
  void *dominance_db;
  void *LM;

  /* --------------------- */
  /* Performance variables */
  /* --------------------- */
  int performance_variables_initialized;
  int timeout;
  int max_xle_events;
  long int bytes_in_use;		/* number of bytes in use. */
  long int max_bytes_in_use;
  long int orig_bytes_in_use;
  int max_medial_constituent_weight;
  int max_medial2_constituent_weight;
  int max_raw_subtrees;
  int start_skimming_when_scratch_storage_exceeds;
  int start_skimming_when_bytes_used_exceed;
  int start_skimming_when_total_events_exceed;
  int max_new_events_per_graph_when_skimming;
  int max_selection_additional_storage;
  long int max_selection_bytes_in_use;
  char *skimming_nogoods;
  char *property_weights_file; /* used for select_best_parse. */
  char *orig_property_weights_file; /* used by make-bug-grammar. */
  char *gen_property_weights_file; /* used for select_best_parse. */
  char *orig_gen_property_weights_file; /* used by make-bug-grammar. */
  char *language_model_file;
  char *orig_language_model_file;
  char *gen_selector;          /* function for selecting a generation
				  output */
  char *ignoreProjections;     /* used for ignoring projections */
  void *sbp_state;             /* state used by select_best_parse. */
  
  char *prolog_encoding;       /* the character encoding of Prolog files */

  void *sentence_tokenizer;    /* for sentence breaking */

  clock_t morph_tics;
  clock_t chart_tics;
  clock_t unifier_tics;
  clock_t completer_tics;
  clock_t solver_tics;
};

/* ------------------------------------------------------------------------- */

struct AgendaArray {

  Agenda **agenda;    /* array of agendas */

  int size;            /* size of the array */

  int n_of_edges;      /* number of edges in all of the agendas */

  int min_agenda_id;    /* id of first non-empty agenda in array */

};

/* ------------------------------------------------------------------------- */

struct Agenda {             /* an agenda of edges to be processed */

  int size;                 /* size of the agenda array */

  Edge **edges;             /* array of edges on the agenda */

  int n_of_edges;           /* number of edges in the edge array */

  AgendaArray small;        /* agenda array of small edges */

  AgendaArray non_embedded; /* agenda array of large non-embedded completes */

  AgendaArray partial;      /* agenda array of large partials */

  AgendaArray complete;     /* agenda array of large embedded completes */

  Chart *chart;

  ScheduleFN schedule;      /* function for adding an edge to the agenda */

  UnScheduleFN unschedule;  /* function for removing an edge from the */
			    /* agenda */
};

/* ------------------------------------------------------------------------- */

#define VTYPE_REGULAR 0             /* regular edges */
#define VTYPE_LEXICAL 1             /* lexical edges (different name space) */
#define VTYPE_SUBRULE 2             /* edges for a subrule */
#define VTYPE_SUBRULE_COMPLETE 3    /* edges for the complete of a subrule */

struct VertexID {

  CategoryID category;	/* Rule category, e.g. NP, VP.  The VertexID */
			/* conflates string position and category because */
			/* this produces a more efficient indexing scheme.  */

  int16 index;		/* in gen: semantic index, in parse: left vertex */

  int16 type;           /* The type of a vertex. See VTYPE_XXX. */

  STATEptr fs_state;    /* in gen; transducer pos for begin  */ 

  STATEptr rule_state;  /* The beginning state in the rule */

};

/* ------------------------------------------------------------------------- */

struct Vertex {

  VertexID id;		/* a vertex identifier.  See VertexID. */

  Requestor *requestors;/* partial edges that are looking for complete */
			/* edges which have this vertex as their left vertex */

  Edge *completes;      /* complete edges that have this vertex as */
		        /* their left vertex */

  int     filtered : 1;			/* generator lookahead checking done */
  int     mustfail : 1;			/* rule lookahead indicates must fail*/
  int     lex_edges_searched : 1;	/* to avoid repeating fsm filter */
  int     lex_edges_created :  1;
  int     maynext_stem : 1;		/* gen property, from transducer */
  int     stem_constraints_built : 1;	/* to avoid rechecking lex */
  int     stem_constraint_ct : 20;      /* cached results of lex_lookup */
  StemConstraint *stem_constraint_list;  /* from filter. */
  int     rule_fsm_index;	/* cache index to avoid repeated lookup */
  int     max_depth;
};

/* ------------------------------------------------------------------------- */

struct SubTreeLink {
  SubTree *pSubtree;
  SubTreeLink *next;
};

struct Edge {
  int id;                  /* a unique identifier */

  int preference_count : 24;  
                          /* number of OT marks of a particular rank */

  unsigned int source : 1; /* This is a source edge, used to initiate a */
			   /* category. */

  unsigned int is_surface : 1;   /* This is a surface edge (token). */

  unsigned int lexical : 1;   /* This is a lexical (terminal) edge. */

  unsigned int preterm : 1;   /* This is a preterminal edge. */
  
  unsigned int cyccheck : 1;   /* This edge has been checked for cycles. */
  
  unsigned int cycfixed : 1;   /* A cycle has been removed. */
  
  unsigned int cyclic      : 1;    /* for the generation guide */

  unsigned int cycle_root  : 1;    /* for unwinding cycles */

  unsigned int on_cycle    : 1;    /* for the generation guide */
  unsigned int along_cycle : 1;    /* for the generation guide */
  unsigned int refined     : 1;    /* whether coverage info. is recorded */
  unsigned int on_agenda   : 1;    /* for generation */

  unsigned int mark : 1;      /* for tree walking algorithms */

  unsigned int mark2 : 1;      /* for checking for well-formedness */

  unsigned int unknown : 1;  /* for lex edges - word not in lex */

  unsigned int lexentry_found : 1; /* for lex edge unknowns - used an lunknown*/

  unsigned int analyzed : 1; /* token was analyzable. */

  unsigned int has_gen_solns : 1; /* temp for gen - completes of top */

  unsigned int nbd : 1;   /* possible non-branching dominator */

  unsigned int ctnsnbd : 1;

  unsigned int tagedge :  1;     /* for gen surface forms*/ 

  unsigned int prune :  1;     /* for chart navigation */ 

  unsigned int unrooted : 1;    /* for chart navigation */

  unsigned int root : 1;    /* root edge of chart */

  unsigned int nogood : 1;  /* constraints are nogood. */

  unsigned int unresourced : 1;  /* used for off-line generability. */

  unsigned int has_pred : 1;  /* Whether the edge has (^ PRED)='xxx' */

  unsigned int always_headed : 1;  /* Whether the edge always has a head,
				      where the head has a PRED or is a
				      coordination or is a resourced
				      expletive. */

  unsigned int always_headed_checked : 1;  /* Whether always_headed has 
					    already been computed. */

  unsigned int has_coord_attr : 1;  /* Result of edge_has_coord_attr */

  unsigned int has_coord_attr_checked : 1; /* Whether edge_has_coord_attr has
					      already been computed. */

  unsigned int has_element : 2;  /* Whether the edge has ! $ ^ in ^=! chain */

  unsigned int no_uncertainties : 1; /* Whether the edge contains
					uncertainties in the edges
					connected by ^ = !. */

  unsigned int has_arg : 1;   /* Whether the edge has a governable arg */

  unsigned int has_args : 1;  /* Whether the edge has some governable args */

  unsigned int duplicated : 1;  /* Edge is duplicated version of shared edge */

  unsigned int max_slash_id : 5;  /* max slash id for this edge */

  unsigned int prolog : 1;   /* created by read_prolog_graph */

  unsigned int arglist;      /* the arglist for coherence checking. */ 

#if 0
  unsigned int local_attributes; /* the set of local attributes */ 

  unsigned int external_attributes; /* the set of external attributes */ 
#endif

#ifdef FILLER_COUNTS
  unsigned int filler_counts; /* A vector of possible filler counts. */ 

  unsigned int good_filler_counts; /* A vector of good filler counts. */ 
#endif

  Vertex *vertex;       /* The vertex this edge is associated with */
			/* The vertex provides a primary indexing for edges */

  STATEptr state;       /* the state in the rule fsm that this edge ends in */

  int right_vertex;	/* for parsing: the righrt vertex of this edge */
                        /* for generation: number of facts (for cycles) */

  void *coverage;	/* a bit array recording coverage of the edge in gen */

#define TREE_TRIMMING_EXPERIMENT
#ifdef TREE_TRIMMING_EXPERIMENT
  void *nec_coverage;	   /* a bit array recording necessary coverage */
  void *nec_parent_coverage;   /* a bit array recording parent coverage */
  void *local_coverage;	   /* a bit array recording local coverage */
#endif

  EdgeList *refined_edges; /* List of refined edges for this edge. */

  long rt_fs_states;    /* for gen list of rt states for edge */
                        /* also used in the parser for recording */
                        /* heavy center embedding. */

  SubTree *subtrees;    /* the subtrees of this edge */

  SubTreeLink *mothers;    /* the subtrees that contain this edge */

  ChartProp *props;     /* client properties */

  float count;   /* the number of trees this edge contains */
                 /* We use 'float' because the number can exceed 2^32. */

  struct Graph *graph;  /* the contexted graph for this edge.  It */
			/* represents the disjunction of all of the subtree */
			/* graphs. */

  EdgeList *surface_corr;    /* 0 if non_lexical or surface edge, */
                             /* else list of associated surface edges */
  int first_tc_offset; /* for dummy surface edge, used in gen output */

  Edge *next;           /* linked list used by the vertices */

  Chart *chart;         /* the chart that this edge is a part of */

  Edge *original_edge;		/* these fields serve in the unwinding alg. */
  Edge *vertex_partial;
  Edge *vertex_complete;
};

struct EdgeList {
  Edge *pEdge;
  int prune;
  EdgeList *next;
};

/* used in right vertex */
#define GEN_NBD_INDICATOR  20
#define ACTIVE_EDGE(E)		\
	((E)->state != NULL && STATE_arc_set((E)->state) != NULL)

#define INACTIVE_EDGE(E)	\
	((E)->state == NULL || STATE_final((E)->state))

/* ----------------------------------------------------------------------- */
/* ChartProp is a Lisp-like property value mechanism that allows clients   */
/* of XLE to add properties to a Graph without having to recompile XLE.    */
/* ----------------------------------------------------------------------- */

struct ChartProp {
  char *name;  /* the name of the property */
  void *value; /* its value */
  ChartProp *next;
};

/* ------------------------------------------------------------------------- */

struct SubTree {

  Edge *partial;       /* the left daughter */

  Edge *complete;      /* the right daughter */

  ARCptr arc;          /* the arc in the grammar fsm that was used to */
		       /* combine the partial and the complete. */

  SExp *constraint;    /* the LFG constraints for this subtree.  This */
                       /* is either derived from SubTree->arc, */
                       /* or it is added to preterminals from the lexicon. */

  struct Graph *graph; /* the contexted graph that combines the partial and */
		       /* complete graphs with the arc constraint. */

  void *coverage;      /* Coverage for the local subtree constraints. */

  SubTree *next;       /* the next subtree in the edge */

  Edge *edge;        /* the edge this is a subtree of */

#ifdef FILLER_COUNTS
  unsigned int filler_counts; /* A vector of filler counts. */
#endif

  unsigned int mark : 1;    /* a temporary mark bit */

  unsigned int prune : 1;    /* a temporary mark bit */

  unsigned int p_cycle : 1;  /* indicates a cycle through the partial */

  unsigned int c_cycle : 1;  /* indicates a cycle through the complete */

  unsigned int nogood : 1;  /* This subtree was determined to be
				      nogood by pre-filtering. */

  unsigned int dispreferred : 1;  /* subtree is dispreferred. */

  unsigned int required : 1;    /* this subtree is required */

  unsigned int allpred : 1;       /* for gen recursives */

  unsigned int recursion_checked : 1;    /* for gen recursives */

  unsigned int mbr_arcs_indet : 1;    /* temp for gen coordination */

  unsigned int gen_mark : 1;     /* temporary mark bit */

  unsigned int maybe_lr_sublex : 1;     /* experimental */

  int preference_count;   /* number of OT marks of a particular rank */
};

/* ------------------------------------------------------------------------- */

struct Requestor {

  Edge *edge;  /* the edge that is making the request */

  unsigned int nbdbegin :1;   /* gen, possible begin non branching dom i->i */
  unsigned int fu_reduced : 1; /* gen, some mod/nogood on funct. uncertainty*/

  unsigned int mark : 1;

  ARCptr arc;  /* the arc that will combine the requestor with the */
	       /* requested */

  struct Graph *graph;  /* the result of combining the requestor with the */
			/* arc's constraint (only used by the generator) */

  Requestor *next; /* next requestor in the vertex's list */

};

/* ------------------------------------------------------------------------- */

struct GoalCtl {
  struct AVPair *goal_avp;   
  GoalAV   *goal_av_list;        /* for lexicon compare */
  GoalAV   *partial_gav_list;    /* this esp for underspecified input*/
  int       av_list_built;       /* boolean             */
  int       explicit_coord;   
  int       num_goal_avs;        /* size of av-list      */
  int       num_indexed_avs;     /* first n indexed     */
  int       num_partial_avs;     /* up to that with const value */   
  int       goal_is_upconst;     /* for goal prediction */
  int       maybe_shared_goal;   /* for coordination */
  void     *sample_mbr_avp;      /* for coordination */ 
  int       sample_mbr_goal;     /* for coordination */ 
  int       goal_container;      /* if goal member of set, goalid of set*/
  int       goal_size;           /* if goal is set, element count */
  char    **set_att_list;        /* list of dist and non dist atts */   
  int       set_att_list_built;
  int       num_common_mbr_atts; /* if goal is set,possibly distrib atts*/ 
  int       num_nonident_mbr_atts; 
  int       num_ident_mbr_atts;
  int       unspecified_synfuns; /* for goals with preds */ 
  char     *pred_str;              
  int       goal_arg_ct;        
  char      goal_args[4];        /* encoded fn refs or constants or 0*/
  int       first_synmap;        /* pos in chart synmap struct */ 
  int       num_synmaps;         
  struct AVPair *goal_arg_avps[4];   /* NULL for non-avpair args */
  int       arg_is_avp[4];
};

/* ------------------------------------------------------------------------- */

struct GoalAV {
  char *attname;
  char *attvalue;
  int16 predvalue; 
  int16 argclass;		/* F, M, U, ...*/   
  char  argsynfuns[4]; 
};

/* ------------------------------------------------------------------------- */

struct StemConstraint {
  char *stem;
  SExp *constraint;
  unsigned int mcodestar    : 1;
  unsigned int allpred      : 1;
  unsigned int continuation : 1;
};

/* ------------------------------------------------------------------------- */

struct FSState {
  STATEptr rt_fs_state;		/* FS state */
  CategoryID *rt_fs_categories;	/* FS follow categories after state */
  int rt_fs_cat_count;		/* -1 if follow cat not initialized for edge */
  int rt_fs_id;		/* FS identifier */
  char maynext_int_stem;	/* part of compound or contraction */  
};

struct SynMap {
  int attid;      /* of a pred arg syntactic function */ 
  struct AVPair *valavp[4];  /* associated goal avps or NULL     */
}; 
     
#endif /* CHART_INCLUDED_ */
