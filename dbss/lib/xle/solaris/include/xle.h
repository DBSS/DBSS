/* (c) 2002-2005 by the Palo Alto Research Center.  All rights reserved */

#ifndef CHART_TYPEDEFS_ /* defined in chart-typedefs.h */
typedef struct Chart Chart;
typedef struct NETtype *NETptr;
#endif
#ifndef DU_TYPEDEFS_  /* defined in DU-typedefs.h */
typedef struct Graph Graph;
#endif
/* ----------------- */
/* PARSING INTERFACE */
/* ----------------- */
Chart *create_parser(char *grammar_name);
/* grammar_name is the name of the root grammar file.  */
/* There can only be one grammar loaded at a time.     */

char *next_sentence_in_stream(FILE *stream, Chart *chart);
/* This returns the next sentence in the stream given. It uses */
/* the BREAKTEXT transducer given in the grammar of chart.     */
/* The storage for the sentence comes from malloc.  If NULL is */
/* returned, then there are no more sentences.  If you stop    */
/* before NULL is returned, call with a NULL stream to reset   */
/* the state of chart.                                         */

Graph *parse_sentence(Chart *parser, char *sentence, char *root_cat);
/* root_cat is the category the sentence should be parsed to.     */
/* If it is NULL, then the root category of the grammar is used.  */
/* parse_sentence produces a contexted graph that has all of the  */
/* solutions packed in it. NB: parse_sentence resets the storage, */
/* so the storage for sentence and root_cat must come from        */
/* somewhere else.                                                */

Graph *parse_regexp(Chart *parser, RegExp *regexp, char *root_cat, 
		    int no_morphology);
/* Parse a regular expression (as defined in regexp.h).  If      */
/* no_morphology, then the regexp should represent the two-level */
/* output that the morphology would normally produce.  Use the   */
/* Tcl function "morphemes {...} twolevel" to determine what     */
/* symbol pairs are the output of the morphology.                */

Graph *parse_lattice_file(Chart *parser, char *lattice_file, char *root_cat,
			  int no_morphology);
/* Parse a finite state network (as produced by xfst).  If       */
/* no_morphology, then the regexp should represent the two-level */
/* output that the morphology would normally produce.  Use the   */
/* Tcl function "morphemes {...} twolevel" to determine what     */
/* symbol pairs are the output of the morphology.                */

Graph *most_probable_structure(Graph *graph, char *weightsFName);
/* Returns a Graph of the most probable structure in graph, where        */
/* graph is a packed representation.  weightsFName is a file of weights. */

Graph *next_graph_solution(Graph *graph, Graph *prior);
/* next_graph_solution can be used to enumerate the solutions      */
/* in a packed graph.  To use it, make prior be NULL the first     */
/* call and then make prior be the result of the last call from    */
/* then on.  When there are no more solutions, NULL is returned.   */
/* It is better to operate on the packed representation            */
/* directly since there can be an exponential number of solutions. */

void free_graph_solution(Graph *graph);
/* This frees the storage allocated by next_graph_solution. */

void print_prolog_graph(FILE *stream, Graph *graph, char *structures);
/* Print the graph as a Prolog term on stream.  'structures'  */
/* determines which structures should be included.  If it is  */
/* NULL, then all of the structures will be printed.  See the */
/* documentation on print-prolog-chart-graph in xle.html for  */
/* more details.                                              */ 

/* -------------------- */
/* GENERATION INTERFACE */
/* -------------------- */
Chart *create_generator(char *grammar_name);
/* grammar_name is the name of the root grammar file. */
/* There can only be one grammar loaded at a time.    */

void set_gen_adds(Chart *chart, char *mode, char *attributes, char *OTmark);
/* if mode is "add", make the attributes addable.                 */
/* if mode is "add", OTmark also gets added.                      */
/* if mode is "remove", remove the attributes from the input.     */
/* if mode is "ignore", call set_gen_add with "add" and "remove". */

Graph *read_prolog_graph(FILE *file, Chart *chart);
/* Reads a prolog graph file, using chart for storage. */

NETptr generate_from_graph(Chart *generator, Graph *graph);
/* This returns a NETptr that represents the strings         */
/* that, when parsed, would produce the graph as one of its  */
/* solutions.  The storage for the graph cannot be from the  */
/* generator, since generate_from_graph resets the storage.  */

int print_genstrings(Chart *chart, NETptr net, FILE *file);
/* This prints the result of generate_from_graph. It calls */
/* print_net_as_regexp unless chart->gen_selector is set.  */

int print_net_as_regexp(NETptr net, FILE *file, int normalize, int utf8);
/* This prints a NETptr as a regular expression to file.   */
/* If normalize is non-zero, then the regular expression   */
/* is massaged to make it more readable (this assumes that */
/* it is not important to preserve spaces exactly).        */

/* ---------------- */
/* TCL/TK INTERFACE */
/* ---------------- */
void init_tcl(int run_Tk, int run_rcfiles);
/* init_tcl must be called before doing anything with Tcl. */
/* if run_Tk, then initialize the Tcl/Tk Tk interface.     */
/* if run_rcfiles, then load the .xlerc files in the       */
/* current directory and the user's home directory.        */

char *execute_tcl_command(char *command);
/* Passes the command to the Tcl interpreter.  This has the */
/* same effect as typing the command to the Tcl shell.  The */
/* result of the command is returned.  To pass pointers,    */
/* give the type in parentheses and then use %x for the     */
/* address.  For instance:                                  */
/* sprintf(buff, "show-solutions (Chart)%x", (int)parser);  */
/* execute_tcl_command(buff);                               */
/* The storage of the result belongs to Tcl/Tk, and the     */
/* content will most likely change after the next call to   */
/* Tcl/Tk.                                                  */

void process_tcl_events();
/* Gives control of Tcl/Tk to the user.                      */
/* Processes button clicks and typed-in commands until       */
/* "set suspend_tcl 1" is executed in Tcl, in which case it  */
/* suspends Tcl/Tk until process_tcl_events is called again. */

void init_xle(int run_Tk, int run_rcfiles);
/* This is no longer necessary to use XLE.  Use init_tcl */
/* instead if you want to use Tcl.                       */

/* ---------------------- */
/* PERFORMANCE PARAMETERS */
/* ---------------------- */

int set_performance_vars(Chart *chart, char *file);
/* Set the performance variables in chart */
/* based on the values given in file.     */
/* Returns 0 if there was an error.       */

int set_performance_var(Chart *chart, char *var, char *value);
/* Set the performance variable in chart to the specified value.  */
/* See the user documentation for what is a performance variable. */
/* Returns 0 if "var" is not a valid performance variable.        */ 

int set_OT_rank(Chart *chart, char *name, char *rank);
/* Sets the OT rank of name to that of rank.                  */
/* rank can be NOGOOD, NEUTRAL, another OT mark, or a number. */ 
/* Returns the old rank of name as a number.                  */

/* ---------------- */
/* CONTEXTED GRAPHS */
/* ---------------- */

/* The declarations for a Graph and its pieces are in graph.h, attributes.h
   relations.h, values.h, and clause.h.  Useful functions are in
   graph-func.h, relations-func.h, and clause-func.h, among others.  If you
   use any of these files, then you will also need to include
   chart-typedefs.h and DU-typedefs.h.  */

/* The root AVPair of a graph can be obtained by with "root =
   find_metavariable(graph, "UP")".  You can get a particular attribute
   from this AVPair using something like find_attribute(root, "SUBJ") or
   you can enumerate attributes using "for (attr = root->attrs; attr; attr
   = attr->next)".  Attribute names are stored as integers in AVPair->attr.
   You can get the string name using get_attr_str(attr). The context in
   which an AVPair is defined is stored in AVPair->defined[0].  The context
   in which an AVPair has constraints (including negated and sub-c
   constraints) is stored in AVPair->contexts[0]. */

/* Attribute values are stored in AVPair->equals as a threaded list of
   CVPairs that can be enumerated using "for (cvp = avp->equals; cvp; cvp =
   cvp->next".  Each CVPair's value is typed.  For instance,
   CVPair->value.type == VT_AVP is an AVPair, CVPair->value.type == VT_STR
   is a string.  The context of the value is stored in
   CVPair->contexts[0]. */

/* The elements of an AVPair representing a set can be obtained using
   get_relation_values(avp, REL_HAS_ELEMENT).  This returns a threaded list
   of CVPairs.  Use set_element_type(CVPair->value) to get the set element
   type, set_element_value(CVPair->value) to get the value, and
   set_element_gensym(CVPair->value) to get the gensym for the set element
   (mostly used as an aid for set distribution). There are also
   'by_reference' versions of these functions for Allegro Common Lisp. */

/* Facts can be duplicated in a contexted graph when an AVPair has an
   equality to another AVPair because the context of the equality is not
   subtracted from the other facts of the original AVPair for performance
   reasons.  Thus, a fact can appear on the original AVPair and the other
   AVPair in the same context, which causes it to be duplicated.  If you
   want to avoid duplicate facts, then use normalized_attr_defined to
   compute the defined context of an AVPair's attr with the AVPair's
   equality links subtracted out, and normalized_attr_contexts to compute
   the context of an AVPair's attr with the AVPair's equality links
   subtracted out, and normalized_relation_context to compute the context
   of a relation value (i.e. a CVPair obtained from AVPair->equals or
   get_relation_values) with an AVPair's equality links subtracted out. */

/* There can be cycles in the equality links between AVPairs, although only
   in non-overlapping contexts.  This is because the direction of an
   equality link is determined at run time based on lazy copying.  Thus, it
   is possible to have f1 => f2 in context P and f2 ==> f1 in context Q.
   However, P /\ Q is always false. */

/* The disjunctions in a contexted graph form an and/or forest, where each
   disjunction is an or-node of choices, and each choice is an and-node
   that may have a number of different disjunctions that depend on it.  A
   disjunction can have more than one choice in its context, but the
   choices are all mutually exclusive. It is also possible to have a
   disjunction with a single choice in it.  Since the Disjunction structure
   is binary-branching, this is represented by setting
   Disjunction->arm[1]->skipped to 1. N-ary disjunctions are represented
   internally using cascaded binary branching disjunctions.  Operations on
   disjunctions should use the binary-branching representation rather than
   the N-ary representation (e.g. use disj->arm[0] and disj->arm[1] instead
   of get_choice unless you know what you are doing). */

/* If a choice has the selected field set, then this is a user-selected
   choice.  select_best_parse also sets this field.  You can evaluate
   whether a context is selected using selected_context.  XLE assumes that
   selections are always complete -- you can't have partial selections or
   make extraneous selections.  You should select a choice using
   select_choice. */

/* When you call conjoin_clauses on clauses drawn from a contexted graph,
   XLE rearranges the choice space so that it remains a choice forest.  For
   instance, if TRUE --> oneof(a1,a2) and TRUE --> oneof(b1, b2) and you
   conjoin a1 and b1, then XLE replaces the two disjunctions with

      TRUE --> oneof(c1,c2,c3,c3)
      or(c1,c2) --> a1
      or(c3,c4) --> a2
      or(c1,c3) --> b1
      or(c2,c4) --> b2

   Thus, the result of conjoining a1 and b1 is c1.
   NB: You must call sort_chart_disjunctions(graph) before processing
   the disjunctions in graph->disjunctions if you have called
   conjoin_clauses. */
