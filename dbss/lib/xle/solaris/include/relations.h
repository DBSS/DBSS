/* (c) 2002-2004 by the Palo Alto Research Center.  All rights reserved */
/* (c) 1996-2001 by the Xerox Corporation.  All rights reserved */
#ifndef RELATIONS_H
#define RELATIONS_H

#include "values.h"

extern TypedValue null_arg;

/* ------------------------------------------------------------------------ */
/* DU uses Relations to extend the unifier to relations other than          */
/* equality.  For instance, the REL_HAS_ELEMENT relation is used to         */
/* indicate that something has an element.  In particular, when             */
/* assert_relation gets called with rel equal to REL_HAS_ELEMENT and arg1   */
/* an AVPair, then REL_HAS_ELEMENT gets added to arg1's rels in an RVPair   */
/* with arg2 as its value to indicate that arg1 has arg2 as an element.  It */
/* makes a difference which argument REL_HAS_ELEMENT gets added to, since   */
/* deductions on pairs of relations can only occur between relations that   */
/* are added to the same AVPair. If deductions are possible on more than    */
/* one argument, then it may be necessary to have two different relations   */
/* which are correlated (such as REL_SUBSUME and REL_SUBSUME_INV.)          */
/* ------------------------------------------------------------------------ */

enum RelID {
  REL_DUMMY,        /* This is a dummy relation with no deductions. */

  /* ------------------------------------------------- */
  /* Relations related to attribute-value unification. */
  /* ------------------------------------------------- */
  REL_EQUALS,       /* This is built into AVPair's equals, but is included */
		    /* here for completeness. */
  REL_COPY,         /* This is built into AVPair's copies, but is included */
		    /* here for completeness. */
  REL_EXISTS,       /* The existence relation. */
  REL_ATTR,         /* dummy relation for printing purposes. */
  REL_ATTR_INV,     /* inverse attribute used for inside-out fu. */
  REL_ATTR_CONTEXT, /* used for queued deductions */
  REL_ATTR_DEFINED, /* used for queued deductions */
  REL_INV_DEFINED,  /* used for (^ FOO) = (! FOO) constriants. */
  REL_UNKNOWN_ATTR, /* used for (^ (! PCASE)) (rename to UKS ?) */
  REL_UNKNOWN_ATTR2,/* used for (^ (! PCASE)) (rename to UKS ?) */
  REL_PHI_INVERSE,  /* used for the phi inverse map */
  REL_CAT,          /* used for the CAT predicate */
  REL_CAT_INVERSE,  /* used for the CAT predicate */
  REL_FID,          /* used for labeling an F-structure with an ID */
  REL_PROLOG_ID,    /* used for recording the Prolog ID of an f-structure */
  REL_COPY_PATH,    /* used to cache copy_path */
  REL_CONCAT,       /* used for concatenating symbols. */

  /* ------------------------------------ */
  /* Relations related to semantic forms. */
  /* ------------------------------------ */
  REL_ARGLIST,      /* used for predicate coherence */
  REL_PUSHED_HEAD,  /* used for pushing uncertainties */
  REL_HAS_HEAD,     /* has an instantiated head (PRED or set id + ELT_PRED) */
  REL_SLASH_HAS_HEAD, /* used for threading uncertainties */
  REL_HAS_PRED2,    /* used for predicate uniqueness. This should precede */
		    /* REL_HAS_PRED, since it produces more nogoods.  */
  REL_HAS_PRED,     /* used for predicate completeness */
  REL_SEMFORM,      /* Is the value a SEMFORM? */
  REL_SEMANTIC,     /* used for semantic completeness */

  /* -------------------------- */
  /* Relations related to sets. */
  /* -------------------------- */
  REL_SET,          /* AVPair is a set */
  REL_SET_ID,       /* The id of the set's instantiated symbol */
  REL_HAS_ELEMENT,  /* used for set membership */
  REL_IS_MEMBER_OF, /* used for set membership */
  REL_ELT_PRED,     /* used for early nogoods and head precedence. */
  REL_ELT_ARGLIST,  /* used for predicate coherence. */
  REL_EQUATIVE,     /* use equality instead of subsumption for set
		       distribution (experimental) */
  REL_ORIGINAL,     /* stores uncollapsed subsumees for debugging */

  /* --------------------------------------------------------- */
  /* Relations related to closed sets and closed f-structures. */
  /* --------------------------------------------------------- */
  REL_CLOSED_FS,    /* This f-structure cannot get new attributes. */
  REL_CLOSED_SET,   /* used to indicate that a set is closed */
  REL_DISTRIBUTION, /* tests whether a variable is distributed (used for
		       closed sets) */
  REL_DISTRIBUTE_VALUES, 
                    /* A set of values from a closed set that needs to */
                    /* to be distributed across set elements and unified. */ 

  /* --------------------------------- */
  /* Relations related to subsumption. */
  /* --------------------------------- */
  REL_SUBSUME,      /* used for distribution of attributes across sets */
  REL_SUBSUME2,     /* keeps track of interactions between REL_SUBSUMEs */
  REL_SUBSUME_INV,  /* The inverse of REL_SUBSUME */
  REL_SUBSUME_DISCHARGED, /* cancels a lazy subsumption link. */
  REL_SUBSUMPTION_REQUEST,  /* Used for pending subsumptions. */

  /* -------------------------------------------- */
  /* Relations related to functional uncertainty. */
  /* -------------------------------------------- */
  REL_UNCERTAINTY,  /* used for functional uncertainty */
  REL_FILLER,       /* An uncertainty with PRED can fill one slot. */
  REL_FU_PATH,      /* used for keeping track of which path an uncertainty
		       took. */
  REL_FU_ATTR,      /* used for keeping track of whether or not an fu
		       attribute has been defined. */
  REL_FU_ATTR1,      /* used for keeping track of whether or not a singleton
			fu attribute has been defined. */
  REL_FU_DISCHARGED,/* used for keeping track of which uncertainty arms
                       have been discharged. */
  REL_INSIDE_OUT_FU, /* inside out functional uncertainty */
  REL_INSIDE_OUT_PATH, /* inside out functional uncertainty */
  REL_SUBC_MATCH,        /* used for sub-c uncertainties */
  REL_SUBC_MATCH_AVP1,        /* used for sub-c uncertainties */
  REL_SUBC_MATCH_AVP2,        /* used for sub-c uncertainties */
  REL_SUBC_MATCH_AVP3,        /* used for sub-c uncertainties */
  REL_SUBC_MATCH_AVP4,        /* used for sub-c uncertainties */

  /* -------------------------------------- */
  /* Relations related to local completion. */
  /* -------------------------------------- */
  REL_COMPLETE,     /* This AVPair is declared complete. */
  REL_INCOMPLETE,   /* All the contexts in which the AVPair is incomplete. */

  /* ----------------------------------------------- */
  /* Relations related to head precedence and scope. */
  /* ----------------------------------------------- */
  REL_HEAD_PRECEDES,    /* tests head precedence order */
  REL_HEAD_PRECEDES2,   /* helps test head precedence order */
  REL_IN_SCOPE_OF,    /* asserts scope order */
  REL_OUT_SCOPES,   /* inverse of REL_IN_SCOPE_OF */
  REL_LEFT_SCOPE,   /* elements to the left outscope those to the right */
  REL_LEFT_SCOPE2,  /* helps test REL_LEFT_SCOPE */
  REL_LEFT_SCOPE3,  /* helps test REL_LEFT_SCOPE */
  REL_RIGHT_SCOPE,  /* elements to the right outscope those to the left */
  REL_RIGHT_SCOPE2, /* helps test REL_RIGHT_SCOPE */
  REL_RIGHT_SCOPE3, /* helps test REL_RIGHT_SCOPE */
  
  /* --------------------------------- */
  /* Relations related to restriction. */
  /* --------------------------------- */
  REL_RESTRICTED_EQUALS, /* Restricted version of equality. */

  /* -------------------------------- */
  /* Relations related to generation. */
  /* -------------------------------- */
  REL_GOAL,         /* used as the goal in generation */
  REL_GOAL_SET,     /* used as the goal with uncertainties */
  REL_UNC_GOAL,     /* used as possible goal in gen for goal prediction */
  REL_SUBSUMED_GOAL, /* used for goal prediction */
  REL_PREDLESS_GOAL, /* used for storing predless versions */
  REL_HAS_GOAL_ELEMENT, /* used for set membership testing in generator */
  REL_IN_SCOPE_OF_GOAL, /* helps the generator detect scope conflicts. */
  REL_ADDED_ATTR,       /* Used for grammar debugging. */
  REL_MISSING_ATTR,     /* Used for grammar debugging. */
  REL_MISSING_VALUE,    /* Used for grammar debugging. */

  /* ---------------------------------------------------------- */
  /* Relations related to abstraction over sets (experimental). */
  /* ---------------------------------------------------------- */
  REL_ABSTRACT,     /* instead of distributing, we are abstracting. */
  REL_ABSTRACT_ATTRS, /* There are attributes in what we are
			 abstracting. */
  REL_ABSTRACT_FU,  /* There is a functional uncertainty in what we are
		       abstracting. */
  REL_ABSTRACT_FU_ATTRS, /* There are attributes that could be part of a
			    functional uncertainty in what we are
			    abstracting. */
  REL_ABSTRACT_OTHER, /* There are other relations in what we are
			 abstracting. */
  REL_MISSING,      /* Used for indicating that a relation is missing. */
  REL_ABSTRACTABLE, /* This AVPair can be abstracted. */
  REL_EQUALS_ABS,   /* The abstraction of REL_EQUALS. */

  /* ------------------------------------------------------- */
  /* Relations related to pushing facts down (experimental). */
  /* ------------------------------------------------------- */
  REL_PUSHER,       /* used by push-facts.c. */

  /* ----------------------- */
  /* Relations over numbers. */
  /* ----------------------- */
  REL_LESS_THAN,
  REL_GREATER_THAN,

  /* -------------------------------------- */
  /* Relations used for transfer induction. */
  /* -------------------------------------- */
  REL_CORR1,
  REL_CORR2,
  REL_VAR_ALIGNMENT,
  REL_TRANSFER,
  REL_PIVOT,
  REL_HEAD_SWITCH,

  /* -------------------------------- */
  /* Relations reserved for the user. */
  /* -------------------------------- */
  REL_USER1,        /* reserved for user relations.  Use */
                    /* get_next_rel_user_id to get the next unused */
                    /* relation id. */
  REL_USER2,
  REL_USER3,
  REL_MAX_ID,       /* the maximum number of relations */
};

#define HAS_ELEMENT_TYPE VT_2_TUPLE
/* the data type for REL_HAS_ELEMENT. */

/*------------------------------------------------------------------------- */
/* Each Relation can have an assertion function, a deduction function, and  */
/* a completion function.  Relations are accessed through the 'relation'    */
/* array and installed by install_relations.  See relations.c and           */
/* semantic-forms.c for examples of how relations are implemented.          */
/*------------------------------------------------------------------------- */

typedef struct Relation Relation;

typedef void (*AssertionFN)(Graph *graph, TypedValue arg1, TypedValue arg2,
			   Clause *context, CVProps props);
/* If a relation has an assertion function, then it gets called by */
/* assert_relation whenever the relation is asserted.  Otherwise, */
/* add_relation gets called.  This is a convenient place for adding other */
/* relations that are correlated with this one (such as REL_SUBSUME and */
/* REL_SUBSUMED). */

typedef void (*DeductionFN)(Graph *graph, AVPair *avp, CVPair *values);
/* If a relation has a deduction function, then it gets called by */
/* process_new_facts whenever new facts get added to an AVPair that has */
/* this relation in its rels field.  The 'values' parameter contains the */
/* values in the relation's RVPair.  The deduction function should look in */
/* avp for other relations or attributes that this relation interacts with */
/* and call conjoin_oldnew on the context arrays to determine whether */
/* there are any new deductions.  If you have a deduction that involves */
/* three relations on the same AVPair (e.g. A & B & C --> D), then you */
/* might consider breaking it up into two deductions (e.g. A & B --> AB, AB */
/* & C --> D).  Otherwise, the code to determine when you have a new */
/* deduction may be a little complex. */

typedef void (*AbsDeductionFN)(Graph *graph, AVPair *avp, 
			       AVPair *set, CVPair *values);
/* A deduction function on an abstract relations. */

typedef void (*CompletionFN)(Graph *graph, AVPair *avp, CVPair *values, 
			     Clause *context);
/* If a deduction has a completion function, then it gets called by */
/* complete_avpair.  Otherwise, complete_values gets called.  The 'values' */
/* parameter contains the values in the relation's RVPair.  The 'context' */
/* parameter is the context in which completion should be checked.  It */
/* corresponds to the context left when the equalities have been subtracted */
/* out.  When you detect that a relation is incomplete, then you should */
/* call distribute_incomplete to distribute the incomplete across the */
/* graphs where it is incomplete. */

typedef Clause *(*IsCompletedFN)(Graph *graph, AVPair *avp, RelID rel,
				 TypedValue tv);
/* Determines the contexts in which the given subc constraint is completed
   (e.g. is not incomplete). */

enum RelType {
  InternalRel = 0, /* This relation is internal (the default). */
  DefiningRel,     /* This is a defining relation. It should be included
		      in any output.  */
  ConstrainingRel, /* This is a constraining relation.  It should be
		      included whenever the "constraints" button is
		      enabled.  */
  HighlightRel,    /* This relation should be highlighted. */
};

struct Relation {
  AssertionFN assert_func;    /* Called by assert_relation. */
  DeductionFN deduce_func;    /* Called by process_new_facts. */
  AbsDeductionFN deduce_abs_func;    /* Called by process_new_facts. */
  CompletionFN complete_func; /* Called by complete_avpair. */
  IsCompletedFN is_completed_func;
  RelType type;               /* See the definition for RelType. */
  int distributive;           /* Whether this relation is distributed
				 across sets. */
  int multipotent;            /* Whether this relation is not
				 idempotent. */
  int subc;                   /* Whether this relation ever has a
				 SUBC constraint. */
};

extern Relation relation[REL_MAX_ID];

/* -------------------------------------------------------------------------*/
/* The macros below are just for convenience in declaring and defining      */
/* functions.                                                               */
/* -------------------------------------------------------------------------*/

#define ASSERT_REL(rel) \
        void assert_##rel(Graph *graph, TypedValue arg1, TypedValue arg2, \
			  Clause *context, CVProps props)
#define DEDUCE_REL(rel) \
        void deduce_##rel(Graph *graph, AVPair *avp, CVPair *cvp)
#define DEDUCE_ABS_REL(rel) \
        void deduce_abs_##rel(Graph *graph, AVPair *avp, \
			      AVPair *set, CVPair *cvp)
#define COMPLETE_REL(rel) \
        void complete_##rel(Graph *graph, AVPair *avp, CVPair *cvp, \
			  Clause *context)

/* -------------------------------------------------------------------------*/
/* Relations are added to an AVPair by adding an RVPair to the AVPair's     */
/* rels field.  There can only be one RVPair per relation per AVPair, but   */
/* the RVPair can have multiple contexted values that represent different   */
/* relation arguments in different contexts.  NEGATED, SUBC, and LAZY       */
/* relations are all stored under the same RVPair along with ordinary       */
/* relations.  They are distinguished by the props field of their CVPair.   */
/* A particular combination of props and value can only appear once per     */
/* relation per AVPair.  If the same combination was going to be added      */
/* twice, then add_value would disjoin their contexts.  (This is also true  */
/* of AVPair->equals and AVPair->copies.)                             */
/* ------------------------------------------------------------------------ */

struct RVPair {
  RelID rel;			/* relation designator */
  
  CVPair *cvalues;		/* list of its contexted values */

  RVPair *next;                 /* the next relation in AVPair->rels */
};

#ifdef notdef
/* ------------------------------------------------------------------------ */
/* Each data type (except tuples) should have a deduction function and a    */
/* comparison function. The deduction function is of the same type as the   */
/* relation's function.  It works on only one cvpair at a time. The         */
/* comparison function is used to compare types.                            */
/* ------------------------------------------------------------------------ */

typedef int (*CompareValuesFN)(TypedValue value1, TypedValue value2);

extern DeductionFN deduce_value_func[VT_USER_MAX];
extern CompareValuesFN compare_values_func[VT_USER_MAX];
#endif

#endif
