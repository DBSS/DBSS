/* (c) 2002-2006 by the Palo Alto Research Center.  All rights reserved */
/* (c) 1996-2001 by the Xerox Corporation.  All rights reserved */

/* ------------------------------------------------------------------------ */
/* These procedures look for incomplete fstructures (in the LFG sense).  In */
/* particular, they look for SUBC constraints that aren't satisfied.        */
/* ------------------------------------------------------------------------ */

void complete_edge(Graph *graph);
/* Recursively complete the edges below graph.  Call complete_avpair on */
/* every AVPair in every graph. */

void complete_avpair(Graph *graph, AVPair *avp);
/* Call complete_values on all of the values of all of avp's relations. */

void local_complete_avpairs(Graph* graph, AVPair* avp);
/* Call local_complete_avpair on avp and its dependents. */

void local_complete_avpair(Graph* graph, AVPair* avp, Clause *context);
/* Call complete_values on local relations in avp. */

void complete_local_attributes(Graph *graph);
/* Complete the attributes in graph that are local. */

void complete_values(Graph *graph, AVPair *avp, RelID rel_id,
		     CVPair *cvalues, Clause *eq_contexts);
/* Check values for SUBC constraints that aren't satisfied.  eq_contexts */
/* is the context in which this check should be performed. */

Clause *subtract_completes(Graph *graph, AVPair *avp, RelID rel_id,
			   CVPair *fact);
/* Subtract the contexts of the facts in avp that complete fact. */

void index_chart_constraints(void *chart, DUCompState *compstate);
/* Index chart constraints for global completeness. */

void index_goal_constraints(Graph *graph, DUCompState *compstate);
/* Index constraints for global completeness. */

int avpair_copied(AVPair *avp);
/* there is a forward copy link in avp. */

int avpair_copied_completely(Graph *graph, AVPair *avp);
/* Return 1 if avp is copied by all of the consumers of graph. */

void mark_consumers(AVPair *avp, int mark);
/* Mark all of the consumers of avp's graph with mark. */

void distribute_incomplete(Graph *graph, Clause *c, AVPair *avp, 
			   CVPair *fact, char *reason, int noOT);
/* Distribute the incomplete clause c to avp's graph if it isn't consumed */
/* and to the graphs that don't consume avp if it is conconsumed. */

Clause *satisfying_contexts(AVPair *avp, RelID rel_id, TypedValue tv,
			    AVPair *set, int abstracted);

Clause *subtract_links(AVPair *avp, int equal_subc_constants_only);

Clause *subtract_lazy_subsumptions(AVPair *avp, Clause *context);
