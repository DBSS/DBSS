/* (c) 2002-2004 by the Palo Alto Research Center.  All rights reserved */
/* (c) 1996-2001 by the Xerox Corporation.  All rights reserved */

/* ----------------------------------------------------------------------- */
/* Entry procedures for the unification package.                           */
/* ----------------------------------------------------------------------- */

AVPair *assert_attribute(Graph *graph, AVPair *avp, AttrID attr_id, 
			 Clause *clause, CVProps props);
/* Assert that the attribute named 'attr_id' is in avp->attrs.  If */
/* avp=NULL, then assert that attr_id is in graph->attrs (e.g. is a */
/* metavariable).  If props != 0, then the attribute is being used in a */
/* constraining equation (e.g. sub_c or negated). Return an AVPair. */

void unify(Graph *graph, TypedValue term1, TypedValue term2, 
	   Clause *context, CVProps props);
/* This is the general procedure for unifying any two types of terms. */
/* For instance, if you wanted to implement (^ NUM)=+ on an existing graph, */
/* then you could do the following:  */
/*
   TypedValue tv1 = {VT_AVP, NULL};
   TypedValue tv2 = {VT_STR, "+"};
   AVPair *avp = assert_attribute(graph, NULL, GR_UP, True_Context, 0);
   tv1.value = assert_attribute(graph, avp, GR_NUM, True_Context, 0);
   unify(graph, tv1, tv2, True_Context, 0);
*/  
/* 'context' is the context in which the unification occurs, and 'props' */
/* indicates whether this is a SUBC or NEGATED constraint (e.g. f1=c f2 or */
/* f1 ~= f2.) */

void copy_avpair(Graph *graph, AVPair *avp1, AVPair *avp2, Clause *context);
/* Establish a vertical link between AVPairs in two different graphs. */
/* 'avp1' is in the lower graph, and 'avp2' is in the upper graph. */

AVPair *restrict_avpair(Graph *graph, AVPair *avp, 
			AttrID attr_id, Clause *context);
/* Create an AVPair that is the avp with attr_id removed. */

void assert_restricted_equals(Graph *graph, TypedValue var1, TypedValue var2,
			      TypedValue attrs1, TypedValue attrs2,
			      Clause *context, CVProps);
/* Make var1 and var2 be partially equal. */

/* ----------------------------------------------------------------------- */
/* Procedures that implement equality (unification).                       */
/* ----------------------------------------------------------------------- */

void unify_avpairs(Graph *graph, AVPair *avp1, AVPair *avp2, 
		   Clause *context, CVProps props);
/* This procedure implements unification when the two terms are both */
/* AVPairs.  It is called by unify. */

int occurs_check(Graph *graph, CVPair *cvps, AVPair *value, 
		 Clause *context, CVProps props);
/* Check whether the cvps already contain 'value' in the right 'context' */
/* with the correct 'props'.  This is used check whether we have already */
/* done a unification so that we can avoid infinite loops when there are */
/* cycles in the unification structure. The name comes from the Prolog term */
/* for this. */

void copy_facts(Graph *graph, AVPair *avp1, AVPair *avp2, 
		CVPair *link, enum RelID copyrel, 
		void *gensym, AVPair *set, int skip_constraints);
/* Copy facts from avp1 to avp2 across link.  This simulates the */
/* copy-equality rule t2=t1 && phi(t2) --> phi(t1).  'copyrel' indicates */
/* which relation is causing the copy: REL_COPY, REL_EQUALS, or */
/* REL_SUBSUME. gensym and set are used for subsumptions. */

void deduce_equals(Graph *graph, AVPair *avp, CVPair *cvp);
/* Make deductions that involve equality with constants. */

void deduce_equals_abs(Graph *graph, AVPair *avp, CVPair *cvp);
/* Make deductions that involve abstract equality with constants. */

int always_constant(AVPair *avp, Clause *context, CVProps props);
/* Determines whether avp is always a constant in context.  */
/* If props == SUBC, then includes SUBC constants as well.  */

Clause* avp_has_head(AVPair *avp);
/* Returns the contexts in which avp has a head. */

/* ---------------------------------------------------------------------- */
/* Procedures that deal with negation.                                    */
/* ---------------------------------------------------------------------- */

void deduce_negations(Graph *graph, AVPair *avp);
/* Make all deductions over the negations in avp.  For instance, */ 
/* p -> f1=a &&  q -> f1~=a --> nogood(p & q). */

void deduce_negation(Graph *graph, AVPair *avp, 
		     CVPair *cvps, CVPair *cvp, RelID rel_id);
/* Make all deductions over the single negation given in cvp. */

/* ---------------------------------------------------------------------- */
/* Procedures that handle the interactions between old and new facts.     */
/* process_new_facts is the heart of the unification package.             */
/* ---------------------------------------------------------------------- */

int process_new_facts(Graph *graph, AVPair *avp, Clause *contexts[2], 
		      int ignore_lock, int unlock);
/* Process the new facts that have just been added to avp in 'contexts' (if */
/* given).  If ignore_lock is 1, then don't worry about the fact that avp */
/* is already locked. If unlock is 1, then clear new contexts and unlock */
/* before returning. */

void expand_attributes(Graph *graph, AVPair *avp);
/* Call this after process_new_facts if new attributes have been added. */

int redundant_new_context(Graph *graph, Clause *contexts[2]);
/* Returns 1 if contexts[1] is redundant with contexts[0]. */

int subsumed_context(Graph *graph, Clause *clause, Clause *contexts[2]);
/* Returns 1 if clause is subsumed by the contexts in contexts[2]. */

Clause *conjoin_oldnew(Graph *graph, ContextPair *fact1, ContextPair *fact2);
/* Take the cross-product of the contexts in fact1 and fact2.  This */
/* is crucial for eliminating redundant deductions. */

Clause *conjoin_facts(Graph *graph, 
		      Clause *contexts1[2], Clause *contexts2[2],
		      AVPair *avp, Rule *reason);
/* Take the cross-product of the contexts in contexts1 and contexts2.  Save
 the context arrays in a Justified clause. */

void clear_avpair_contexts(Graph *graph, AVPair *avpair);
/* Call clear_new_context on all of the contexts[1] fields in avpair->attrs */
/* and all of the relations' values. */

void clear_new_context(Graph *graph, Clause *contexts[2]);
/* Disjoin contexts[1] into contexts[0] and clear contexts[1]. */

/* ----------------------------------------------------------------------- */
/* Procedures that deal with locking and queueing.                         */
/* ----------------------------------------------------------------------- */

void lock_avpair(AVPair *avp);
/* Lock the avp so that new facts cannot be added without their contexts */
/* being queued. */

void queue_unification(Graph *graph, AVPair *avp, ContextPair *contexts,
		       Clause *new_context, CVPair *cvp, RelID rel_id,
		       AVPair *target);
/* Put new_context on graph's queue.  contexts is the context array that */
/* new_context would have been added to if avp hadn't been locked. */
/* cvp and rel_id are for debugging purposes.                      */

void unlock_avpair(AVPair *avp);
/* Unlock the avp and call process_queued_facts. */

void process_queued_facts(Graph *graph);
/* Process the facts that are on graph's queue. For each avp that is */
/* now unlocked, set contexts[1] to new_context and call process_new_facts. */

void discharge_chart_queues(void *chart);

/* ----------------------------------------------------------------------- */
/* Procedures for dealing with attributes.                                 */
/* ----------------------------------------------------------------------- */

AVPair *add_avpair(Graph *graph, AVPair *avp, AttrID attr_id, 
		   Clause *context, Clause *defined, 
		   int ignore_lock);
/* Add an AVPair to avp->attrs named 'attr_id' if it doesn't already exist. */
/* 'context' is the context in which it is mentioned, and 'defined' is the */
/* context in which it is defined (see attributes.h). If ignore_lock is not */
/* 0, then ignore the lock (e.g. don't queue the contexts).  This procedure */
/* shouldn't be used directly, use assert_attribute instead. */

AVPair *add_metavariable(Graph *graph, AttrID attr, 
			 Clause *context, Clause *defined);
/* This is like add_avpair, only it is used for metavariables. */

AttrID get_anonymous_attr_id(Graph *graph, AVPair *attrs, char *key);
/* Get an anonymous attribute id.  This is used for local named variables */
/* and for anonymous variables introduced by, say, functional uncertainty. */

AVPair *get_avpair(AVPair *avp, AttrID attr);
/* Get the AVPair in avp->attrs named 'attr'. */

AVPair *get_metavar(Graph *graph, AttrID attr);
/* Get the AVPair in graph->attrs named 'attr'. */

AVPair *new_avpair(Graph *graph, AttrID attr);
/* Allocate a new AVPair in graph named 'attr'. */

Graph *get_avpair_graph(AVPair *avpair);
/* Return the graph that avpair is contained in. */

void invert_attribute(AVPair *avp);
/* Add REL_ATTR_INV(avp->attr, avp->prefix) to avp. */

Clause *normalized_attr_context(AVPair *attr);
/* AVPair->contexts[0] minus the contexts of attr->prefix->equals. */

Clause *normalized_attr_defined(AVPair *attr);
/* AVPair->defined[0] minus the contexts of attr->prefix->equals. */

Clause *normalized_relation_context(CVPair *cvp, AVPair *avp);
/* cvp->contexts[0] minus the contexts of avp->equals. */

/* ---------------------------------------------------------------------- */
/* Procedures for dealing with values.                                    */
/* ---------------------------------------------------------------------- */

CVPair* add_value(Graph *graph, AVPair *avp, RelID rel_id, 
		  void *value, ValueType vt, 
		  Clause *context, CVProps props, int ignore_lock);
/* Add the value of type vt to relation re;_id on avp */
/* in context with props=props. If ignore_lock is true, then */
/* don't queue the value's context if avp is locked. This procedure */
/* shouldn't be used directly, instead you should call unify, */
/* copy_avpair, or assert_relation to add values. */

int compare_typed_values(TypedValue value1, TypedValue value2, int constraint);
/* Return 1 if value1 and value2 are equal. If constraint is not 0, */
/* then we are comparing negated or subc values with non-negated and */
/* non-subc values.  This means that semantic forms get coerced to */
/* their FNS. */

int equivalent_typed_values(TypedValue value1, TypedValue value2);
/* Return 1 if value1 and value2 are equivalent except for gensyms. */

int avpair_greater_than(AVPair *avp1, AVPair *avp2);
/* Return 1 if information should flow from avp1 to avp2, and 0 otherwise. */

int compare_attr_order(AttrID attr1, AttrID attr2);
/* Return 1 if information should flow from attr1 to attr2, 0 if */
/* attr1=attr2, and -1 otherwise. */

int avp_value(TypedValue value);
/* Return 1 if value contains an VT_AVP. */

/* ----------------------------------------------------------------------- */
/* Procedures for following equality links.                                */
/* ----------------------------------------------------------------------- */

AVPair *follow_bypass(AVPair *avp);
/* Follow any bypass links in avp. Return the terminating AVPair. */

Clause *next_bypass_context(Graph *graph, TypedValue value, int *done);
/* Return the next bypass context for value and graph's DOWN variable.  */
/* If done is NULL, this is the first (e.g. initialization call).  */
/* If done is not NULL, then set the value of done to 1 when done iterating. */

AVPair *follow_contexts(AVPair *avp, Clause *context);
/* Follow any links in context.  Return the final AVPair. */

AVPair *get_link_target(CVPair *cvp);
/* Extract the AVPair link from the cvp (works for subsumptions, too). */

/* ----------------------------------------------------------------------- */
/* Procedures that deal with vertical copying between graphs.              */
/* ----------------------------------------------------------------------- */

void expand_lazy_links(Graph *graph, AVPair *avp, int reason);
/* Expand all lazy links in avp.  'reason' is just used for debugging. */

void add_lazy_link(Graph *graph, AVPair *avp, AVPair *avp2, 
		   Clause *context, CVProps props, 
		   enum RelID copyrel, int discharged);
/* Add a lazy link from avp to avp2 in context. 'copyrel' indicates the
   relation of the lazy link. */

void *copy_typed_value(Graph *graph, TypedValue tv, AVPair *from, 
		       Clause *context, unsigned int relID, AVPair *to);
/* Make a copy of value in graph.  The AVPair 'from' is used for gensym */
/* values. */
 
AVPair *copy_link(Graph *graph, AVPair *avp, Clause *context);
/* Make a copy of avp in graph, following any bypass links. */

AVPair *find_common_root(AVPair *avp1, AVPair *avp2);
/* Find the greatest commmon prefix of avp1 and avp2.  If avp1 and avp2 */
/* belong to different meta-variables, this returns NULL. */

AVPair *add_primed_metavariable(Graph *graph, AVPair *mv);
/* We want to copy the metavariable mv from its graph to the graph */
/* given. This usually involves renaming the meta-variable, since there is */
/* probably already a metavariable with mv's name in graph. */
 
int attribute_distributed(AVPair *avp, AVPair *attr, RelID rel, 
			  CVPair *fact);
/* Return 1 if attr was already distributed over fact somewhere below. */

#ifdef ATTRVARS
AVPair *create_attr_var(Graph *graph, AVPair *avp, Clause *context,
			VarType type);
/* Create a new attribute var on avp. */

void copy_attr_vars(AVPair *lower, AVPair *upper, Clause *context,
		    int subsumees);
/* Copy the attribute vars in lower to upper in context. */

AVPair *copy_attr_var(Graph *graph, AVPair *avp, AVPair *orig, 
		      Clause *context);
/* Make a copy of an existing attribute var on avp. */

VarType attr_var_type(AVPair *avp);
/* Return the VarType of an attribute variable. */

#endif

/* ---------------------------------------------- */
/* Procedures used in pushing facts.              */
/* ---------------------------------------------- */

void push_facts(Graph *graph, AVPair *avp, CVPair *link);
/* Push facts from avp down link. */

int copied_fact(AVPair *avp, RelID rel, CVPair *fact);
/* See if fact is the result of a copy. */

int copied_attribute(AVPair *attr);
/* See if attr is the result of a copy. */

void complete_pushed_facts(AVPair *avp);
/* Assert incompletes on contexts where the pushed facts aren't pushed. */

int suppress_copy(AVPair *avp, RelID rel, CVPair *cvp, Graph *target);
/* Should the copy of this fact be suppressed? */

int suppress_var_copy(AVPair *var, Graph *target);
/* Should the copy of this attribute var be suppressed? */

/* ---------------------------------------------- */
/* Procedures that deal with gensyms.             */
/* ---------------------------------------------- */

#ifdef notdef
int next_gensym(Graph *graph, int gensym, Graph *gengraph);
/* Get the next gensym in graph. If gengraph is given, then return the */
/* gensym given unless that gensym is already in graph but with a different */
/* gengraph (this is used for predicate uniqueness in generation, when the */
/* same predicate 'type' may be used as different 'tokens' within a tree */
/* because the generation chart can be reentrant). */
#endif

int new_gensym(Graph *graph, TypedValue data, Clause *context,
	       unsigned int relID, AVPair *new);
/* Create a new gensym that is local to graph */

int copy_gensym(Graph *graph, int old, Graph *oldgraph, 
		TypedValue data, RelID rel, AVPair *new, Clause *context);
/* Copy a gensym from one graph into another.  Since gensyms are local,
   the copied gensym may be different from the original.  copy_gensym uses
   'data'  and 'rel' to try to reuse gensyms that will conflate in 
   disjunctive graphs. */

int new_global_gensym(Graph *graph, TypedValue data, Clause *context);
/* Create a new gensym whose scope is the entire chart. */
/* Reuse ids from equivalent gensyms in disjoint contexts. */

RelID get_gensym_relation(Graph *graph, void *gensym);
/* Return the RelID that the gensym was created with. */

void set_gensym_edge(DUCompState *compstate, int id, void *edge, 
		     Clause *context);
/* Store the edge where a gensym was instantiated */
/* in compstate->global_gensyms. */

void set_gensym_contexts(Graph *graph);
/* Set the gensym contexts in graph->compstate->global_gensyms. */

/* ----------------------------------------------------------------------- */
/* General utility procedures.                                             */
/* ----------------------------------------------------------------------- */

int print_path(AVPair *avp, int flush);
/* Print the path of avp.  If flush is not 0, then flush the stream. */

void print_fact(AVPair *avp, RelID rel_id, TypedValue value, CVProps props,
		ContextPair *fact, Clause *context, char *comment);
/* Print a fact to stdout. */

extern int print_graph_id;
extern int print_graph_indent;
extern int print_added_facts;
extern int max_print_fact_id;

int empty_avpair(Graph *graph, AVPair *avp, Clause *context);
/* Return 1 if avp has no facts in 'context'. */

int lazy_interaction(Graph *graph, AVPair *avp, Clause *context, 
		     ValueType type, void *value, enum RelID copyrel);
/* Check whether the proposed lazy link to value in context interacts with */
/* any of the existing lazy links in avp. copyrel indicates the type of
   lazy link proposed. */

int avp_needs_expansion(AVPair *avp);
/* Check whether avp neeeds to be expanded. */

int attr_needs_expansion(AVPair *avp);
/* Check whether avp neeeds to be expanded. */
/* makes use of avp->contexts.              */

Clause *avp_are_equal(AVPair *avp1, AVPair *avp2);

char *get_relation_str(unsigned int rel_id);
/* Return the print string for this RVPair. */

int nogood_avpair(AVPair *avp);
/* Returns 1 if the AVPair's context is nogood. */

/* ----------------------------------------------------------------- */
/* Debugging procedures.                                             */
/* ----------------------------------------------------------------- */

void print_avpair(AVPair* avp);
/* Print the contents of an AVPair for debugging purposes. */

void print_avpair_long(AVPair* avp, int indentparm);
/* Print the contents of an AVPair for debugging purposes. */

void print_cvalue(CVPair *cvp, int n, char *left, char *right, char *dir);
/* Print the contents of a CVPair for debugging purposes. */

void print_tv(TypedValue tv, DUCompState *compstate);
/* Print a typed value for debugging purposes. */

void print_graph(Graph *graph);
/* Print the contents of a Graph for debugging purposes. */

void print_graph_attrs(Graph *graph);
/* Print the graph's attributes for debugging purposes. */

void print_graph_soln_info(Graph *graph);
/* Print the solutions of a Graph for debugging purposes. */

void print_fu_rels (AVPair *avp);
/* Print the functional uncertainties. */

char *str_print_cvpair(ValueType type, void *value);

void print_fu_net(void *net);
/* ------------------------------------------------------------------------- */

