/* (c) 2002-2005 by the Palo Alto Research Center.  All rights reserved */
/* (c) 1996-2001 by the Xerox Corporation.  All rights reserved */

#ifndef RELATIONS_FUNC_H
#define RELATIONS_FUNC_H

int add_relation(Graph*, AVPair*, RelID, TypedValue, Clause*, CVProps);
CVPair *add_inert_relation(Graph *graph, AVPair *avp, RelID rel_id, 
			   void *value, ValueType type, 
			   Clause *context, CVProps props);
RVPair* get_relation(RVPair*, RelID);
RVPair *create_new_rvpair(Graph *, AVPair *, RelID);
CVPair* get_relation_values(AVPair *avp, RelID rel_id);
int relation_has_value(CVPair *values, ValueType type, void *value, 
		       CVProps props, int constraint);

void assert_relation(Graph*, RelID, TypedValue, TypedValue, Clause*,
		     CVProps);
void assert_unknown_attr(Graph *graph, TypedValue pcase, AVPair *avp, AVPair *dummy, Clause *context, CVProps props);
void assert_cat_inverse(Graph *graph, char* catname);
void replace_concat_arg(Graph *graph, TypedValue *tuple, int n, 
			ValueType type, void *value, Clause *context);

int get_next_rel_user_id();
void install_relations();
void install_restriction();
ValueType tuple_type(TypedValue tuple, int index);
void *tuple_value(TypedValue tuple, int index);

/* ---------------------------------------------- */
/* Functions relationed to sets and set elements. */
/* ---------------------------------------------- */
void assert_has_element(Graph *graph, TypedValue arg1, TypedValue arg2,
			void *gensym, Clause *context, CVProps props);
void distribute_relation_to_elements(Graph *graph, AVPair *avp, 
				RelID rel, CVPair *cvp);
void replace_set_element(TypedValue tuple, ValueType type, void *value);

/* -------------------------------------------- */
/* Access functions for sets and set elements.  */
/* The 'by_reference' functions are for Allegro */
/* Common Lisp, which cannot handle structure   */
/* arguments.                                   */
/* -------------------------------------------- */

ValueType set_element_type(TypedValue tv);
ValueType set_element_type_by_reference(TypedValue *tv);

void *set_element_value(TypedValue tv);
void *set_element_value_by_reference(TypedValue *tv);

void *set_element_gensym(TypedValue tv);
void *set_element_gensym_by_reference(TypedValue *tv);

AVPair *set_element(TypedValue tv);
AVPair *set_element_by_reference(TypedValue *tv);

/* ---------------------------------- */
/* Access functions for REL_ATTR_INV. */
/* ---------------------------------- */

AttrID attr_inv_attr_id(TypedValue tv);
AVPair *attr_inv_avpair(TypedValue tv);

/* ------------------------------------ */
/* Access functions for REL_RULE_TRACE. */
/* arg ids are in [1 ... count].        */
/* (arg = 0 is the length.)             */
/* You can also use RT_INSTANCE, etc.   */
/* ------------------------------------ */

#define RT_LENGTH 0
#define RT_ID 1
#define RT_INSTANCE 2
#define RT_ABSOLUTE_FREQUENCY 3
#define RT_RELATIVE_FREQUENCY 4
#define RT_HEAD_COUNT 5
#define RT_HEAD_COUNT_DIFF 6
#define RT_LEXICAL_FREQUENCY 7
#define RT_LEXICAL_FREQUENCY2 8
#define RT_RELATIVE_FREQUENCY2 9
#define RT_MAX_ARG 9

ValueType rule_trace_arg_count(TypedValue tv);
ValueType rule_trace_arg_type(TypedValue tv, int i);
void *rule_trace_arg_value(TypedValue tv, int i);

#endif
