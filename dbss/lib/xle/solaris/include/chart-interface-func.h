/* (c) 1996 by the Xerox Corporation.  All rights reserved */

/* This is DU's interface to the chart data structures.  Instead of giving */
/* the data structures, we provide procedural interfaces. Here is how one */
/* uses this interface:
   
   state = initialize_subtrees_traversal(graph->edge);
   while (traverse_subtrees(state, &mother, &dtrs[0], &dtrs[1])) {
      ... process daughters ...
   }
   terminate_subtrees_traversal(state);

*/

void *initialize_subtrees_traversal(struct Edge*); /* use incomplete type so */
						/* that we don't have to */
						/* define Edge. */

int traverse_subtrees(void* state, Graph** mother, 
		      Graph** left_daughter, Graph** right_daughter);

void terminate_subtrees_traversal(void* state);

Graph *get_edge_graph(struct Edge*); /* use incomplete type */
