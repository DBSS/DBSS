/* (c) 2002-2004 by the Palo Alto Research Center.  All rights reserved */
/* (c) 1996-2001 by the Xerox Corporation.  All rights reserved */
#ifndef INSTANTIATE_ATTRIBUTE_ID_H
#define INSTANTIATE_ATTRIBUTE_ID_H

/* ------------------------------------------------------------------------ */
/* The order of these attributes is important for determining the direction */
/* in which information flows during unification.  See avpair_greater_than  */
/* for more details.                                                        */
/* ------------------------------------------------------------------------ */

enum AttrID {
  
  /* ----------------------------------------------------- */
  /* Negative values represent local meta variables and    */
  /* anonymous variables (used get_anonymous_attr_id and   */
  /* add_primed_metavariable).  There is no limit on the   */
  /* number of negative values.                            */
  /* ----------------------------------------------------- */
  GR_LOCAL6 = -6,
  GR_LOCAL5 = -5,
  GR_LOCAL4 = -4,
  GR_LOCAL3 = -3,
  GR_LOCAL2 = -2,
  GR_LOCAL1 = -1,
  GR_LOCAL0 = 0,

  /* -------------- */
  /* Meta Variables */
  /* -------------- */
  GR_ME = 1,     /* = *. used for projections off of the c-structure */
  GR_DOWN = 2,
  GR_MOTHER = 3, /* = M*. used for projections off of the c-structure */
  GR_UP = 4,
  GR_GLOBAL = 5, /* for all of the global variables.  We make it higher */
		 /* than the other meta-variables so that they will point */
		 /* to it on the assumption that the global variables will */
		 /* be shallow. */
  

  /* --------------- */
  /* Tree Attributes */
  /* --------------- */
  GR_LEFT_SISTER,
  GR_RIGHT_SISTER,
  GR_RIGHT_DAUGHTER,
  GR_NODE_LABEL,
  GR_WEIGHT,

  /* -------------------------- */
  /* Constant Valued Attributes */
  /* -------------------------- */
  GR_NUM,
  GR_PERS,
  GR_GEND,
  GR_CASE,
  GR_FORM,
  GR_VFORM,
  GR_VTYPE,
  GR_INV,
  GR_AUX,
  GR_TNS_ASP,
  GR_MOOD,
  GR_PERF,
  GR_PROG,
  GR_TENSE,
  GR_SENTENCE_ID,       /* distinguished attribute */
  GR_NUMBER_TYPE,

  GR_PRED,
  GR_PRON_FORM,
  GR_PRON_TYPE,
  GR_SIGMA,		/* tentative */
  GR_COND,		/* tentative */
  GR_LOC,
  GR_IND,
  GR_ID,
  GR_REL,		/* tentative */
  GR_BASE,
  GR_RESTRICT,

  /* ------------------------------------- */
  /* Variables for passing information up. */
  /* ------------------------------------- */
  GR_PRON_REL,

  /* ------------------------------------------------ */
  /* XXX variables are for debugging attribute order. */
  /* ------------------------------------------------ */
  GR_XXX1,  
  GR_XXX2,
  GR_XXX3,
  GR_XXX4,
  GR_XXX5,
  GR_XXX6,

  /* ------------------ */
  /* Semantic Functions */
  /* ------------------ */
  GR_TOPIC,
  GR_TOPIC_REL,
  GR_FOCUS_INT,
  GR_PRON_INT,

  /* ---------------- */
  /* Slash Categories */ 
  /* ---------------- */
  GR_SLASH1,
  GR_SLASH2,
  GR_SLASH3,
  GR_SLASH4,
  GR_SLASH5,
  GR_SLASH6,
  GR_SLASH7,
  GR_SLASH8,
  GR_SLASH9,
  
  /* -------------------- */
  /* Governable Functions */
  /* -------------------- */
  GR_SUBJ,
  GR_OBJ,
  GR_OBJ2,
  GR_OBL,
  GR_XCOMP,
  GR_COMP,

  GR_COMPL,

  GR_PCASE,
  GR_PTYPE,

  GR_OBL_ABOUT,
  GR_OBL_AG,
  GR_OBL_FOR,
  GR_OBL_FROM,
  GR_OBL_IN,
  GR_OBL_INST,
  GR_OBL_INTO,
  GR_OBL_OF,
  GR_OBL_ON,
  GR_OBL_TO,
  GR_OBL_WITH,

  /* -------------------------------------------------- */
  /* OBL-{PCASE} should be treated slightly differently */
  /* -------------------------------------------------- */
  /* - - - - - - - - - - - - - - - - */
  /* also, update attr_is_governable */
  /* - - - - - - - - - - - - - - - - */

  GR_ADJUNCT,
  GR_ADJUNCT_REL,
  GR_ADV_TYPE,
  GR_XADJ,
  GR_MOD,
  GR_NAME_MOD,
  GR_MODIFIER,
  GR_MODIFICATION,
  GR_RELMOD,

  /* unknown yet */

  GR_POL,
  GR_NEG,
  GR_INDe,

  GR_POSS,
  GR_SPEC,
  GR_NUMBER,
  GR_NTYPE,
  GR_NSYN,
  GR_DET_TYPE,
  GR_DET,
  GR_DEF,
  GR_DEICTIC,
  GR_PROXIMITY,
  GR_POS,
  GR_CAT,
  GR_SITSCHEMA,

  GR_WH,

  GR_WHO_KNOWS,

  GR_HUMAN,
  GR_COUNT,
  GR_MEASURE,

  GR_ATYPE,
  GR_HAS_PARTICLE,
  GR_QUANT,
  GR_PARTICLE,
  GR_PHI,
  GR_MORPH_PROJ,
  GR_OPTIMALITY,
  GR_SEMANTIC_PROJ,
  GR_CONJ,
  GR_CONJ_FORM,
  GR_COORD,
  GR_COORD_FORM,
  GR_COMP_FORM,
  GR_PRECOORD_FORM,
  GR_PRT_FORM,
  GR_PFORM,
  GR_CHECK,
  GR_TOKEN,
  GR_FIRST,
  GR_REST,

  /* ----------------------------------------- */
  /* Attributes for pieces of a semantic form. */
  /* ----------------------------------------- */

  GR_FN,
  GR_SFID,
  GR_ARG1,
  GR_ARG2,
  GR_ARG3,
  GR_ARG4,
  GR_ARG5,
  GR_ARG6,
  GR_ARG7,
  GR_NOTARG1,
  GR_NOTARG2,
  GR_NOTARG3,
  GR_NOTARG4,
  GR_NOTARG5,
  GR_NOTARG6,
  GR_NOTARG7,

  /* ----------------------------- */
  /* Attributes used for transfer. */
  /* ----------------------------- */
  GR_RULE_TRACE,
  GR_DOMINANCE_SCORE,
  GR_DOMINANCE_COUNT,

  /* ------------------------------------------------ */
  /* YYY variables are for debugging attribute order. */
  /* ------------------------------------------------ */
  GR_YYY1,
  GR_YYY2,
  GR_YYY3,
  GR_YYY4,
  GR_YYY5,
  GR_YYY6,
  
  /* ---------------------------------------------------------- */
  /* Included by RSC as temporary fix for external_prolog_graph */
  /* -- makes remaining attrs in standard/english.lfg global    */
  /* ---------------------------------------------------------- */

  GR_ANIM,
  GR_STMT_TYPE,
  GR_TIME,
  GR_COMMON,
  GR_PROPER_TYPE,
  GR_CLAUSE_TYPE,
  GR_INT_TYPE,
  GR_RESTR,
  GR_ADJUNCT_TYPE,
  GR_DEVERBAL,
  GR_PSEM,
  GR_SPEC_TYPE,
  GR_DEIXIS,
  GR_QUANT_TYPE,
  GR_COORD_LEVEL,
  GR_POSTCOORD_FORM,
  GR_EMPHASIS,
  GR_TOPIC_TYPE,
  GR_PASSIVE,
  GR_NSEM,
  GR_GEND_SEM,
  GR_PROPER,
  GR_NAME_TYPE,
  GR_LAST,
  GR_ADJUNCT_QT,
  GR_AQUANT,
  GR_DEG_DIM,
  GR_OBL_COMPAR,
  GR_DEGREE,
  GR_FOCUS,
  GR_OBL_PART,
  GR_OBJ_TH,
  GR_LOCATION_TYPE,
  GR_TOPIC_CLEFT,
  GR__ABBREV,
  GR__ACONSTR,
  GR__ADVNP_SET,   
  GR__AUX_TYPE,   
  GR__BASE,   
  GR__DEG_TYPE,   
  GR__LDD,  
  GR__INF_TYPE,   
  GR__INV,   
  GR__NCONSTR,   
  GR__NUMBER_MORPH,   
  GR__ROOT_COORD_CLAUSE_TYPE,
  GR__SOURCE,   
  GR__STRANDED,   
  GR__STRANDEDP,
  GR__TAG,   
  GR__TAG_MORPH,   
  GR__TAG_NEG,   
  GR__TAG_PRON,   
  GR__TAG_VFORM,   
  GR__VCONSTR,   
  GR__SUBCAT_SOURCE,   
  GR__LEX_SOURCE,   
  GR__VERBNET_TYPE,
  GR__UNBAL_LQT,   
  GR__UNBAL_RQT,   
  GR__BAL_QT, 
  GR__UNGRAMMATICAL, 





  GR_HAS_ELEMENT, /* used to indicate the presence of set elements. */
  GR_MAX_ATTR_ID, /* THIS MUST ALWAYS BE LAST! */
  /* ----------------------------------------------------------- */
  /* Values greater than GR_MAX_ATTR_ID represent new attributes */
  /* from the grammar (created by get_path_symbol_id).           */
  /* ----------------------------------------------------------- */
};

/* --------------------------- */
/* Predicates on path strings. */
/* --------------------------- */

int distributed_path_string(void *avp, char *path);
/* Determines whether the path would be distributed. */


#endif
