/* (c) 2002-2005 by the Palo Alto Research Center.  All rights reserved */
/* (c) 1996-2001 by the Xerox Corporation.  All rights reserved */

/* ------------- */
/* Instantiation */
/* ------------- */

void instantiate_constraint(SExp*, Graph*, Clause*, char*);

void instantiate_constraint1(SExp*, Graph*, Clause*, CVProps, char*);

void instantiate_has_element(Graph *graph, 
			     TypedValue *settv, TypedValue *elttv,
			     Clause *context, CVProps props);

int instantiate_term(SExp*, Graph*, Clause*,
		     CVProps props, TypedValue*, TypedValue);
/* Instantiate the SExp given.  The last TypedValue is given as a possible
   dummy variable.  If it is used, instantiate_term returns 1. */

AVPair *instantiate_attribute(Graph *graph, AVPair *avp, char *attr_name, 
			      Clause *context, CVProps props);

int instantiate_transfer_term(SExp*, Graph*, Clause*,
			      CVProps props, int, TypedValue*);
/* Instantiate the transfer SExp given.  */

int instantiated_symbol_p(char *string);

void instantiate_isym(char *name, Graph *graph, Clause *context,
		      CVProps props, TypedValue *iterm);

void instantiate_number(SExp *term, TypedValue *iterm);

int get_path_symbol_code(char *, DUCompState *);

/* -------------------- */
/* Attribute Properties */
/* -------------------- */

void set_projection_prop(AttrID id, DUCompState *compstate);

void set_constant_prop(AttrID id, DUCompState *compstate);

void set_uncertainty_prop(AttrID id, DUCompState *compstate);

void set_all_uncertainty_props(DUCompState *compstate);

void clear_uncertainty_props(DUCompState *compstate);

void set_invertible_prop(AttrID id, DUCompState *compstate);

void set_non_local_prop(AttrID id, DUCompState *compstate);

/* -------- */
/* Printing */
/* -------- */

void print_sexp(FILE *file, SExp *sexp, 
		CVProps printprops, CVProps filterprops,
		int depth, Graph *graph);

void print_lex_entry(Chart *chart, char *headword, 
		     char *cat, FILE *file);

char *get_relation_str(unsigned int rel_id);

/* -------------------- */
/* Indexing Constraints */
/* -------------------- */

int filter_constraint(SExp *constraint, ArgList arglist, 
		      DUCompState *compstate);

int filter_constraint1(SExp *constraint, ArgList arglist, CVProps props,
		       DUCompState *compstate);

void check_disjunctions(SExp *constraint, DUCompState *compstate, 
			int all, char *entry);

void print_attr_index(DUCompState *compstate);

void psexp (SExp *sexp);

int changing_constraint(SExp*, DUCompState*);

/* -------------------------- */
/* Predicates on constraints. */
/* -------------------------- */

int eq_meta_var(SExp *term, char *name);
/* Returns 1 if term is a meta variable named 'name'. */

int up_equals_down(SExp *constraints);
/* Returns 1 if up equals down in all contexts. */

int down_is_member_of_up(SExp *constraints);
/* Returns 1 if down is a member of up in all contexts. */

int always_has_pred(SExp *constraints);
/* Returns 1 if (^ PRED)='...' in all contexts. */

int has_governable_arg(SExp *constraints, DUCompState *compstate);
/* Returns 1 if (^ GF ...)=! in some context. */

int is_uncertainty(SExp *term);


char *last_attribute(SExp *term);
