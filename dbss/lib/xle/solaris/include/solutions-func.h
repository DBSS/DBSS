/* (c) 2002-2004 by the Palo Alto Research Center.  All rights reserved */
/* (c) 1996-2001 by the Xerox Corporation.  All rights reserved */

RestrictedSolution* get_edge_solutions(Graph *edge_graph, 
				       Graph *mother_graph);
/* ----------------------------------------------------- */
/* If mother_graph is NULL, then this returns a single   */
/* packed solution that contains all of the solutions to */
/* edge_graph.  Use first_dnf_solution and               */
/* next_dnf_solution to enumerate unpacked solutions.    */
/* ----------------------------------------------------- */

RestrictedSolution *first_dnf_solution(RestrictedSolution *solutions,
				       int prev);
/* If prev == 1, then start with the last solution. */

RestrictedSolution *next_dnf_solution(RestrictedSolution *solution,
				      int prev);
/* If prev == 1, then get the previous solution. */

void set_solution_choice_values(RestrictedSolution *dnf_solution, int value);
/* Set the value of the solution's choices to value. */

int pull_edge_nogoods_down(Graph*);

int has_solution(Graph*);

Clause *extract_daughter_clauses(Graph*, Graph*, int all);

void add_internal_solution(Graph*, Clause*, 
			   RestrictedSolution*,
			   RestrictedSolution*,
			   InternalSolutionsList**,
			   Graph*);

void mark_internal_solution(Clause*,
			    Clause*,
			    RestrictedSolution*, 
			    RestrictedSolution*,
			    int);

void mark_embedding_contexts(Clause*, int);

int evaluate_restriction(Clause *, Clause**, Graph*);

int valid_solutions(RestrictedSolution*, Graph*, Graph*);

RestrictedSolution *cache_solution(Graph*, RestrictionSet*, Clause**, int,
				   int, int, int);

void print_solution_combination(Clause*,
				RestrictedSolution*, RestrictedSolution*);

void print_solution_clauses(char*, RestrictedSolution*);

float count_restricted_solutions(RestrictedSolution*);
void clear_solution_counts(RestrictedSolution *r);
void clear_solution_marks(RestrictedSolution *r);

void initialize_OT_data(DUCompState *compstate);
float count_unoptimal_solutions(RestrictedSolution*);
int ungrammatical_solution(RestrictedSolution *class, 
			   DUCompState *compstate);
OTCount *get_optimal_marks(RestrictedSolution *class, 
			   DUCompState *compstate);
void separate_unoptimal_solutions(RestrictedSolution *class,
				  OTCount *best,
				  DUCompState *compstate);
void set_OT_override(DUCompState *compstate, char *mark, int it);
OTMark *gather_active_OT_marks(RestrictedSolution *class,
			       DUCompState *compstate,
			       OTMark *values);
OTMark *gather_graph_OT_marks(Graph *graph);
int set_OT_rank(Chart *chart, char *name, char *rank);
int get_OT_rank(DUCompState *compstate, char *name);
int get_orig_OT_rank(DUCompState *compstate, char *name);
int preference_mark(DUCompState *compstate, char *name);
int ungrammatical_mark(DUCompState *compstate, char *name);
int nogood_mark(DUCompState *compstate, char *name);
void set_optimality_ranking_prop(Graph *graph);
int rank_has_preferred_mark(DUCompState *compstate, int rank);
int count_ranked_OT_marks(SExp *constraint, int rank, int preferred, 
			  DUCompState *compstate);
void record_conditioning_optimality_marks(RestrictedSolution *solution);
void recompute_optimality_marks(RestrictedSolution *solution);
void set_skimming_nogoods(DUCompState *compstate, char *skimming_nogoods);
int nogood_OT_mark_enabled(DUCompState *compstate, char *mark_name);
int ignore_OT_mark(DUCompState *compstate, char *mark_name, int rank);

Clause *multiply_choices(Graph *graph, int prune,
			 Clause *initial_choices,
			 RestrictedSolution *dtr1,
			 RestrictedSolution *dtr2);
/* Create a DNF of all choices that are local to graph. */

int multiply_solutions(Graph *mother,
		       RestrictedSolution *partial, 
		       RestrictedSolution *complete,
		       Clause *local_choices,
		       RestrictionSet *cache,
		       int all, int goods_done);

RestrictedSolution *
refine_solution(RestrictedSolution *solution, Graph *mother);

Clause *extract_solution_clauses(RestrictedSolution *s);

int equal_solutions(RestrictedSolution *s1, RestrictedSolution *s2);

void construct_solution(Graph *graph, RestrictedSolution *s, Clause
			**clauses, int n);

RestrictedSolution *create_predefined_solution(Graph *, Clause *);

RestrictedSolution* get_pushed_solutions(Graph *edge_graph, 
					 Clause *restriction,
					 Clause *pushed,
					 int all, int open);

RestrictedSolution *get_debugging_solutions(Graph *edge_graph, 
					    Clause *restriction, 
					    int all);

void print_ambiguity_sources(void *edge, int marked);
void mark_accessible_solutions(RestrictedSolution *rs, int mark);
void mark_ambiguity_sources(RestrictionSet *set, Graph *subtree_graph);
int mark_solution_grammar_choices(RestrictedSolution *rs);
void mark_chosen_solution_choices(RestrictedSolution *rs);
void unmark_all_choices(Graph *graph);

double get_solution_weight(RestrictedSolution *solution);

RestrictedSolution *get_chart_solutions(Graph *graph, int selected);
void sort_chart_disjunctions(Graph *graph);
