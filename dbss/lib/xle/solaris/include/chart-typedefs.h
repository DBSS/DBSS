/* (c) 2002-2004 by the Palo Alto Research Center.  All rights reserved */
/* (c) 1996-2001 by the Xerox Corporation.  All rights reserved */

#ifndef CHART_TYPEDEFS_
#define CHART_TYPEDEFS_

typedef struct Chart Chart;

typedef struct Agenda Agenda;

typedef struct AgendaArray AgendaArray;

typedef struct Vertex Vertex;

typedef struct VertexID VertexID;

typedef struct VertexData VertexData;

typedef struct TokenVertexMap TokenVertexMap;

typedef struct Edge Edge;

typedef struct ChartProp ChartProp;

typedef enum EdgeProps EdgeProps;

typedef struct SubTree SubTree;

typedef struct SubTreeLink SubTreeLink;

typedef struct EdgeList EdgeList;

typedef struct Requestor Requestor;

typedef struct GoalCtl   GoalCtl;

typedef struct GoalAV    GoalAV;

typedef struct StemConstraint StemConstraint;

typedef struct SynMap    SynMap; 

typedef struct FactData FactData;

/* ---------------------------------- */
/* Agenda manipulation function types */
/* ---------------------------------- */
typedef void (*ScheduleFN)(Edge*, Agenda*);
typedef Edge *(*UnScheduleFN)(Agenda*);


typedef int (*SpanningEdgeFN)(Edge*, void*);

typedef struct FSState FSState;

/* from hash-interface.h */

typedef unsigned int (*Hash_HashFN)(void*);
typedef int (*Hash_CompFN)(void*, void*);
typedef void *(*Hash_CombFN)(void*, void*);

/* from trees.h */
typedef struct DTree DTree;
typedef struct DTreeList DTreeList;
typedef enum DTreeProps DTreeProps;
typedef struct TreeSelection TreeSelection;

#ifndef C_FSM_TYPES /* defined in the c-fsm package */
typedef struct NETtype *NETptr;
typedef struct STATE *STATEptr;
typedef struct ARC *ARCptr;
typedef struct HEAP *HEAPptr;
typedef struct HASH_TABLE *HASH_TABLEptr;
#endif

typedef struct RegExp RegExp;

#ifndef __LFG_H_INCLUDED__
typedef struct Grammar Grammar;
typedef struct SExp SExp;
typedef struct SExpArray SExpArray;
typedef char *CategoryID;
#endif

#define int16	short 
#define uint16	unsigned short 
#define int32	int
#define uint32	unsigned int

#endif
