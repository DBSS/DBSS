/* (c) 2002-2004 by the Palo Alto Research Center.  All rights reserved */
/* (c) 1996-2001 by the Xerox Corporation.  All rights reserved */

#ifndef GRAPH_FUNC_H
#define GRAPH_FUNC_H

/* -------------------------------------------------------------------- */
/*                   GRAPH UTILITY PROCEDURES                           */
/* -------------------------------------------------------------------- */

Graph *create_graph(DUCompState *compstate);
/* Create a new graph.  The storage is allocated from compstate. */

int graph_p(Graph *graph); 
/* Graph is not NULL or Bad_Graph. */

int graph_is_nogood(Graph *graph); 
/* Graph is NULL, Bad_Graph, or nogood. */

void unmark_attributes(Graph *graph); 
/* Unmark all of the attributes in graph. */

void set_graph_prop(Graph *graph, char* propname, void *value);
/* Set graph's propname property to be value. */

void *get_graph_prop(Graph *graph, char* propname);
/* Get graph's propname property value. */

void set_external_graph_prop(Graph *graph, char* propname, char *value);
/* Set graph's propname property to be value. */

char *get_external_graph_prop(Graph *graph, char* propname);
/* Get graph's propname property value. */

void check_graph_skimming_events(Graph *graph);

void eliminate_variables(Graph *graph);

void *get_category_from_graph(Graph *graph);

/* -------------------------------------------------------------------- */
/*                 GRAPH-CHART INTERFACE PROCEDURES                     */
/* -------------------------------------------------------------------- */

int disjunctive_graph(Graph *graph);
/* Return 1 if this is a disjunctive graph (e.g. combines several subtree */
/* graphs disjunctively). */

void add_graph_consumer(Graph *daughter, Graph *mother);
/* Add mother to daughter's list of consuming graphs. */

Graph *incorporate_complete_graph(Graph *subtree_graph, Graph *complete);
/* Add the complete graph to a subtree graph that already has the partial */
/* graph and constraints.  This is separated out so that the generator can */
/* use it. */

void set_graph_edge(Graph *graph, void *edge);
/* Set graph's edge to be edge. */

void *get_graph_edge(Graph *graph);
/* Return the graph's edge. */

void add_filler_constraints(Graph *graph);
/* Add REL_FILLER constraints.  Defined in uncertainty.c. */
#endif



