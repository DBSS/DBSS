# kinput.tcl --
#
# This file contains Tcl procedures used to input Japanese text.
#
# $Header: /home/m-hirano/cvsroot/tcltk/tk8/library/kinput.tcl,v 1.3 1998/05/11 07:02:09 m-hirano Exp $
#
# Copyright (c) 1993  Software Research Associates, Inc.
#
# Permission to use, copy, modify, and distribute this software and its
# documentation for any purpose and without fee is hereby granted, provided
# that the above copyright notice appear in all copies and that both that
# copyright notice and this permission notice appear in supporting
# documentation, and that the name of Software Research Associates not be
# used in advertising or publicity pertaining to distribution of the
# software without specific, written prior permission.  Software Research
# Associates makes no representations about the suitability of this software
# for any purpose.  It is provided "as is" without express or implied
# warranty.
#

# ----------------------------------------------------------------------
# Class bindings for start Japanese text input (Kana-Kanji conversion).
# Use over-the-spot style for text widgets, root style for entry widgets.
# ----------------------------------------------------------------------

#bind Text <Shift-space> {kinput_start %W over}
bind Text <Control-backslash> {kinput_start %W over}
bind Text <Control-Kanji> {kinput_start %W over}
bind Text <Control-Shift_R> {kinput_start %W over}

#bind Entry <Shift-space> {kinput_start %W}
bind Entry <Control-backslash> {kinput_start %W over}
bind Entry <Control-Kanji> {kinput_start %W}
bind Entry <Control-Shift_R> {kinput_start %W}


# The procedure below is invoked in order to start Japanese text input
# for the specified widget.  It sends a request to the input server to
# start conversion on that widget.
# Second argument specifies input style.  Valid values are "over" (for
# over-the-spot style) and "root" (for root window style). See X11R5
# Xlib manual for the meaning of these styles). The default is root
# window style.

proc kinput_start { w {style root} } {
    update
    global _kinput_priv
    if { ![ string compare $style "over" ] } {
	set spot [ $w xypos insert ]
	if { [ string length $spot ] <= 0 } {
	    set spot "[ $w cget -bo ] [ winfo height $w ]"
	}
	set font [ $w cget -font ]
	set attr [ font actual $font ]
	set compound [ lindex $attr end ]
	if { [ llength $compound ] < 1 } {
	    set font "{$font}"
	}
	trace variable _kinput_priv($w) w _kinput_trace_over
	kanjiInput start $w \
	    -variable _kinput_priv($w) \
	    -inputStyle over \
	    -foreground [ $w cget -foreground ] \
	    -background [ $w cget -background ] \
	    -fonts "$font" \
	    -clientArea [_kinput_area $w] \
	    -spot $spot
	return
    }
    trace variable _kinput_priv($w) w _kinput_trace_root
    kanjiInput start $w -variable _kinput_priv($w) -inputStyle root
}

# The procedure below is invoked to send the spot location (the XY
# coordinate of the point where characters to be inserted) to the
# input server.  It should be called whenever the location has
# been changed while in over-the-spot conversion mode.

proc kinput_send_spot {w} {
    if { [ catch { kanjiInput attribute $w } ] == 0 } {
	set spot [_kinput_spot $w]
	if { [ string length $spot ] > 0 } then {
	    kanjiInput attribute $w -spot $spot
	}
    }
}

#
# All of the procedures below are the internal procedures for this
# package.
#

# The following procedure returns the list of XY coordinate of the
# current insertion point of the specified widget.

proc _kinput_spot {w} {
    $w xypos insert
}

# The following procedure returns the list of drawing area of the
# specified widget. { x y width height }

proc _kinput_area {w} {
    set bw [ $w cget -bo ]
    return "$bw $bw [expr {[winfo width $w] - $bw*2}] [expr {[winfo height $w] - $bw*2}]"
}

# The following procedure returns the value of the specified option
# (resource).
#proc _kinput_attr {w option} {lindex [$w configure $option] 4}
proc _kinput_attr {w option} { $w cget $option }

# The two procedures below are callbacks of a variable tracing.
# The traced variable contains the text string sent from the
# input server as a conversion result.

# for root style
proc _kinput_trace_root {name1 name2 op} {
    upvar #0 $name1 trvar
    $name2 insert insert $trvar($name2)
    unset $trvar($name2)
}

# for over-the-spot style
proc _kinput_trace_over {name1 name2 op} {
    upvar #0 $name1 trvar
    $name2 insert insert $trvar($name2)
    update
    kinput_send_spot $name2
    unset $trvar($name2)
}


# tkEntrySetCursor - redefine.
# Move the insertion cursor to a given position in an entry.  Also
# clears the selection, if there is one in the entry, and makes sure
# that the insertion cursor is visible.
#
# Arguments:
# w -		The entry window.
# pos -		The desired new position for the cursor in the window.

proc tkEntrySetCursor {w pos} {
    $w icursor $pos
    $w selection clear
    tkEntrySeeInsert $w
    kinput_send_spot $w
}

# tkTextSetCursor - redefine.
# Move the insertion cursor to a given position in a text.  Also
# clears the selection, if there is one in the text, and makes sure
# that the insertion cursor is visible.  Also, don't let the insertion
# cursor appear on the dummy last line of the text.
#
# Arguments:
# w -		The text window.
# pos -		The desired new position for the cursor in the window.

proc tkTextSetCursor {w pos} {
    global tkPriv

    if [$w compare $pos == end] {
	set pos {end - 1 chars}
    }
    $w mark set insert $pos
    $w tag remove sel 1.0 end
    $w see insert
    kinput_send_spot $w
}

bind Text <1> {
    tkTextButton1 %W %x %y
    %W tag remove sel 0.0 end
    tkTextSetCursor %W insert
}

bind Text <Delete> {
    if {[%W tag nextrange sel 1.0 end] != ""} {
	%W delete sel.first sel.last
	tkTextSetCursor %W insert
    } else {
	tkTextSetCursor %W insert-1c
	%W delete insert
	%W see insert
    }
}

bind Text <Control-h> {
    if {[%W tag nextrange sel 1.0 end] != ""} {
	%W delete sel.first sel.last
	tkTextSetCursor %W insert
    } else {
	tkTextSetCursor %W insert-1c
	%W delete insert
	%W see insert
    }
}

bind Text <BackSpace> {
    if {[%W tag nextrange sel 1.0 end] != ""} {
	%W delete sel.first sel.last
	tkTextSetCursor %W insert
    } else {
	tkTextSetCursor %W insert-1c
	%W delete insert
	%W see insert
    }
}

bind Text <Return> {
    tkTextInsert %W \n
    tkTextSetCursor %W insert
}
