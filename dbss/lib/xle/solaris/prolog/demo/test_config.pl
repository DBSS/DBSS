% Config file for prolog f-struct format to syntax testsuite
% constraints format.  Anything beginning with a capital letter,
% e.g. SUBJ, must be enclosed in back quotes: 'SUBJ'.

% Definition of abbreviations.  

ggf means ['SUBJ', 'OBJ', 'OBJth', 'COMP', 'XCOMP', 'COMP-EX' ].

sf means [ 'ADJUNCT' ].

% Specification of the features with values that have the attribute
% PRED that  we are interested in.

preds [ggf, 'ADJUNCT'].

% Other possible values for preds: "all", "none" or set of GGF
% functions, as above.  Default is all.  Here we are interested in all
% the attributes defined as ggf, plus the attribute 'ADJUNCT'.  Given
% the definitions above, the following specification is equivalent:
%
% preds [ggf,sf].
%   
% Other examples: 
% preds all.  % All attributes with values that have preds appear in
%               testfile.  This is the default.
% preds none. % Only top-level pred appears in testfile.
% preds ['SUBJ', 'OBJ', 'OBJth'].
% would only include preds for SUBJ, OBJ and OBJth.  Note that the set
% elements are delimited by commas (",").
% preds [ggf, 'ADJUNCT'].  % All the attributes defined above as ggf
%    are included, plus the attribute 'ADJUNCT'.
% preds [ggf, sf].  % All the attributes defined as ggf and sf are included.

% Specification of other features that we are interested in.

features none.   

% Possible values for FEATURES: "all", "none" or set of features to
% include.  Default is "none".  
%
% Each feature can include an optional "exist" or "eq" tag.  The
% "exist" tag indicates that if the feature exists then a statement
% should be written expressing only that existence.  The "eq" tag
% indicates that if the feature exists then a full deep encoding of
% that feature's value will be written.  Note that the default is
% "eq", so that the following specifications means that the full
% encoding of the NTYPE feature will be written:
% features ['NUM':eq,
%          'PERS':exists,
%          'NTYPE'].







