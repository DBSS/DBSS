% Config file for prolog f-struct format to syntax testsuite
% constraints format.

% Definition of abbreviations.  ggf should be set of all governable
% grammatical functions. 

ggf means ['SUBJ', 'OBJ', 'OBJth', 'OBL',  'OBL-AGT', 'COMP', 'XCOMP', 'COMP-EX' ].

sf means [ 'ADJUNCT' ].

% Specification of what preds we are interested in.

preds [ggf, sf].

% Possible values for preds: "all", "none" or set of GGF functions. 
% Default is all.
%   
% Examples: 
% PREDS all.  % All attributes with values that have preds appear in testfile.
% PREDS none. % Only top-level pred appears in testfile.
% PREDS ['SUBJ', 'OBJ', 'OBJth'].
% would only include preds for SUBJ, OBJ and OBJth.  Note that the set
% elements are delimited by commas (",").
% PREDS [ggf, 'ADJUNCT'].  % All the attributes defined above as ggf
%    are included, plus the attribute 'ADJUNCT'.
% PREDS [ggf, sf].  % All the attributes defined as ggf and sf are included.

% Specification of what features we are interested in.

 features ['AUX-FORM',
	   'INF-FORM',
	   'NEG',
	   'PASSIVE',
	   'TNS-ASP',
	   'PERF',
	   'PROG',
	   'TENSE',
	   'VTYPE'].
% features none.

% Possible values for FEATURES: "all", "none" or set of features to
% include.  Default is "none".  
%
% Each feature can include an optional "exist" or "eq" tag.  The
% "exist" tag indicates that if the feature exists then a statement
% should be written expressing only that existence.  The "eq" tag
% indicates that if the feature exists then a full deep encoding of
% that feature's value will be written.  Note that the default is
% "eq".





