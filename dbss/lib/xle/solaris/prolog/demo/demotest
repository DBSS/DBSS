# directive to read in macro definitions:
include macros demomacros

# simple, unambiguous
ROOT: Ed saw the man.

# ambiguous, no parse selected
ROOT: Ed saw the elephant on the mountain.

# ambiguous, parse 1
ROOT: Ed saw the man with a telescope.
fd
eq:[p:[^, ADJUNCT, $, PRED],sf(with,_,_)]
efd

# same sentence, parse 2
ROOT: Ed saw the man with a telescope.
fd
eq:[p:[^, OBJ, ADJUNCT, $, PRED],sf(with,_,_)]
efd

# ungrammatical, and expected to be so:
ROOT: Ed Mary the and.
fd
fail
efd

# ungrammatical, but not given an f-description saying it should fail:
ROOT: Ed Mary the the the and.

# grammatical, but does not match description
ROOT: Ed saw the man.
fd
eq:[p:[^,'ADJUNCT','$','PRED'],sf(with,_,_)]
efd



# a comment
; another comment
  # another one
  ; yet another
  % this too

ROOT: it is actuated.
fd
and:[eq:[p:[^,'PRED'],sf(actuate,['NULL','%1'],[])],
     eq:[p:[^,'PASSIVE'],+],
     eq:[p:[^,'TNS-ASP'],'%2'],
     eq:[p:['%2','MOOD'],indicative],
     eq:[p:['%2','TENSE'],pres],
     eq:[p:[^,'VTYPE'],main],
     eq:[p:[^,'SUBJ'],'%1'],
     eq:[p:['%1','PRED'],sf(pro,[],[])]]
efd


ROOT: it is actuate.
fd
never:[and:[eq:[p:[^,'PRED'],sf(actuate,['NULL','%1'],[])],
     eq:[p:[^,'PASSIVE'],+],
     eq:[p:[^,'VTYPE'],main],
     eq:[p:[^,'SUBJ'],'%1'],
     eq:[p:['%1','PRED'],sf(pro,[],[])]]]
efd


% Includes example of a macro call in the f-description
ROOT: it is appearing.
fd
and:[eq:[p:[^,PRED],sf(appear,[%1],[])], 
     +@testmacro(%1)]
efd



ROOT: it is being being appearing.
fd
never:[and:[eq:[p:[^,'PRED'],sf(appear,['%1'],[])],
     eq:[p:[^,'PASSIVE'],-],
     eq:[p:[^,'TNS-ASP'],'%2'],
     eq:[p:['%2','MOOD'],indicative],
     eq:[p:['%2','PROG'],'+_'],
     eq:[p:['%2','TENSE'],pres],
     eq:[p:[^,'VTYPE'],main],
     eq:[p:[^,'SUBJ'],'%1'],
     eq:[p:['%1','PRED'],sf(pro,[],[])]]]
efd

