/* (c) 2001 by the Xerox Corporation.  All rights reserved */

/* ------------------------------------------------------------------------ */
/* Project:   XLE Test Suite                                                */
/* Author:    Dick Crouch                                                   */
/* File:      fdescriptions.pl                                              */
/* Language:  SICStus Prolog 3.7.1                                          */
/* Standalone code for check fstr descriptions                              */
/* ------------------------------------------------------------------------ */


check_descriptions(fail,_Root,_Locals) :- !, fail.
check_descriptions(true,_Root,_Locals).
check_descriptions(never:Desc,Root,Locals) :- !,
	% Looks odd.  But we treat never: as a top level only operator
	% ranging across different unpackings of an
	% f-structures. Succeed on any f-structure on which the
	% description is supposed to fail, and rely on handling
	% elsewhere to flag that these are unwanted successes. 
	check_descriptions(Desc,Root,Locals).
check_descriptions(and:[],_Root,_Locals).
check_descriptions(and:[Conjunct|Conjuncts],Root,Locals) :-
	check_descriptions(Conjunct,Root,Locals),
	check_descriptions(and:Conjuncts,Root,Locals).
check_descriptions(or:[Disjunct|Disjuncts],Root,Locals) :-
	(
	    check_descriptions(Disjunct,Root,Locals) -> true
	;
	    check_descriptions(or:Disjuncts,Root,Locals)
        ).
check_descriptions(findall:[Value,Description,Values],Root,Locals) :-
	findall(Value,
		check_descriptions(Description,Root,Locals),
		Values).
check_descriptions(eval:[Call],Root,Locals) :-
	Call =.. CallList,
	append(CallList,[Root,Locals],CallList1),
	Call1 =.. CallList1,
	call(Call1).
check_descriptions(ior:[Disjunct|Disjuncts],Root,Locals) :-
	% inclusive / non-deterministic or:
	% doesn't stop after first true disjunct, but looks for others
	(
	    check_descriptions(Disjunct,Root,Locals) 
	;
	    check_descriptions(ior:Disjuncts,Root,Locals)
        ).
check_descriptions(not:[Description],Root,Locals) :-
	\+ check_descriptions(Description,Root,Locals).
check_descriptions(exists:[p:Path],Root,Locals) :-
	\+ \+ path_value(Path,Root,Locals,_Val).
check_descriptions(fprecedes:[Value1,Value2],Root,Locals) :-
	eval_desc_value(Value1,EV1,Root,Locals),
	eval_desc_value(Value2,EV2,Root,Locals),
	fprecedes(EV1,EV2). % asserted fact
check_descriptions(phicat:[Value1,CatValue],Root,Locals) :-
	eval_desc_value(Value1,FNode,Root,Locals),
	phi(MorphNode,var(FNode)),
	subtree(_Mother,CatValue,_Partial,MorphNode).
check_descriptions(eq:[Value1,Value2],Root,Locals) :-
	eval_desc_value(Value1,EV1,Root,Locals),
	eval_desc_value(Value2,EV2,Root,Locals),
	chk_desc_eq(EV1,EV2,Root,Locals).


eval_desc_value(V,V,_Root,_Locals) :- var(V), !.
eval_desc_value('^',Root,Root,_Locals) :- !.
eval_desc_value('%'(Loc),EValue,_Root,Locals) :- !,
	member(Loc-EValue,Locals).
eval_desc_value(p:Path,EValue,Root,Locals) :- !,
	path_value(Path,Root,Locals,EValue).
eval_desc_value(Value,Value,_Root,_Locals).



chk_desc_eq(V,V,_,_) :- !.
chk_desc_eq(V,_,_,_) :- atomic(V), !, fail.
chk_desc_eq(semform(P1,A,B1,C1),semform(P2,A,B2,C2),Root,Locals) :- !,
	(
	    P1 = P2 -> true
	;
	    atom(P1), atom(P2),
	    atom_chars(P1,P1Chars),
	    atom_chars(P2,P2Chars),
	    (	
		append(P1Chars,[0'-|_],P2Chars) -> true
	    ;	
		append(P2Chars,[0'-|_],P1Chars)
	    )
	),
	chk_desc_eq_arg(2,sf(B1,C1),sf(B2,C2),Root,Locals).
chk_desc_eq(V1,V2,Root,Locals) :-
	functor(V1,F,N),
	functor(V2,F,N),
	chk_desc_eq_arg(N,V1,V2,Root,Locals).

chk_desc_eq_arg(0,_V1,_V2,_Root,_Locals) :- !.
chk_desc_eq_arg(N,V1,V2,Root,Locals) :-
	arg(N,V1,A1),
	arg(N,V2,A2),
	eval_desc_value(A1,EA1,Root,Locals),
	eval_desc_value(A2,EA2,Root,Locals),
	chk_desc_eq(EA1,EA2,Root,Locals),
	M is N-1,
	chk_desc_eq_arg(M,V1,V2,Root,Locals).



path_value([^|Path],FNode,Locals,Value) :- !,               % path in
	path_value_in(Path,FNode,Locals,Value).
path_value(['%'(Local)|Path],_FNode,Locals,Value) :- !,     % path in from
	member(Local-FNode1,Locals),                        % non-root
	nonvar(FNode1),
	path_value_in(Path,FNode1,Locals,Value).
path_value(['^'(NRoot)|Path],_FNode,Locals,Value) :- !,     % path in from
	nonvar(NRoot),                                      % non-root
	path_value_in(Path,NRoot,Locals,Value).
path_value([p:Path1|Path2],FNode,Locals,Value) :- !,        % path in from
	path_value(Path1,FNode,Locals,FNode1),              % embedded path out
	path_value_in(Path2,FNode1,Locals,Value).
path_value(Path,FNode,Locals,Value) :-                      % path out
	reverse(Path,PathOut),
	rpath_value(PathOut,FNode,Locals,Value).

rpath_value([^|PathOut],FNode,Locals,Value) :- !,
	path_value_out(PathOut,FNode,Locals,Value).         % path out
rpath_value(['%'(Local)|PathOut],_FNode,Locals,Value) :- !,
	member(Local-FNode1,Locals),                        % path out from
	nonvar(FNode1),                                     % non-root
	path_value_out(PathOut,FNode1,Locals,Value).
rpath_value(['^'(NRoot)|PathOut],_FNode,Locals,Value) :- !,
	nonvar(NRoot),                                      % path out from
	path_value_out(PathOut,NRoot,Locals,Value).         % non-root
rpath_value([p:Path1|PathOut],FNode,Locals,Value) :- !, 
	path_value(Path1,FNode,Locals,FNode1),
	path_value_out(PathOut,FNode1,Locals,Value).


path_value_in([p:Path|Attrs],FNode,Locals,Value) :- !,
	% should we allow this?
	path_value(Path,FNode,Locals,FNode1),
	path_value_in(Attrs,FNode1,Locals,Value).
path_value_in(['$'|Path],FNode,Locals,Value) :-
	set_in(FNode,FNode1),
	path_value_in(Path,FNode1,Locals,Value).
path_value_in([Attr|Path],FNode,Locals,Value) :-
	attr_eq(FNode,Attr,var(FNode1)),
	path_value_in(Path,FNode1,Locals,Value).
path_value_in(['$'],FNode,_Locals,Value) :-
	set_in(FNode,Value).
path_value_in([Attr],FNode,_Locals,Value) :-
	attr_eq(FNode,Attr,Value1),
	(
	    Value1 = var(Value) -> true
	;
	    Value1 = Value
	).


path_value_out([p:Path|Attrs],FNode,Locals,Value) :- !,
	% should we allow this?
	path_value(Path,FNode,Locals,FNode1),
	path_value_out(Attrs,FNode1,Locals,Value).
path_value_out([Attr|Path],FNode,Locals,Value) :-
	eq_attr(FNode,Attr,FNode1),
	path_value_out(Path,FNode1,Locals,Value).
path_value_out(['$'|Path],FNode,Locals,Value) :-
	in_set(FNode,FNode1),
	path_value_out(Path,FNode1,Locals,Value).
path_value_out(['$'],FNode,_Locals,Value) :-
	in_set(FNode,Value).
path_value_out([Attr],FNode,_Locals,Value) :-
	eq_attr(FNode,Attr,Value1),
	(
	    Value1 = var(Value) -> true
	;
	    Value1 = Value
	).





	
path_var_value([PathExpr-PathVar|_],PathVar1,Root) :-
	PathVar == PathVar1, !,
	path_value(PathExpr,Root,Value),
	PathVar = Value.
path_var_value([_|Paths],PathVar,Root) :-
	path_var_value(Paths,PathVar,Root).





%==================================================================
%  Stuff to assert and retract facts about (unpacked) c- and f-structure
%  Need to do this before checking f-description

assert_fstr(fstructure(_,_Properties,_,_,FS,CS)) :-
	abolish_facts,
	assert_facts(FS,CS).


assert_facts(CS,FS) :-
	assert_facts(CS),
	remove_irrelevant_facts(FS,FS1),
	fix_coordination(FS1,FS2),  % now do this for semantics too.
	assert_facts(FS2).
% 	(
% 	    Discourse = 1 ->
% 	    fix_coordination(FS1,FS2),
% 	    assert_facts(FS2)
% 	;
% 	    assert_facts(FS1)
% 	).


remove_irrelevant_facts([],[]).
remove_irrelevant_facts([cf(_,eq(proj(_,_),_))|FSs],RFSs) :- !,
	remove_irrelevant_facts(FSs,RFSs).
remove_irrelevant_facts([FS|FSs],[FS|RFSs]) :- 
	remove_irrelevant_facts(FSs,RFSs).

% Don't store all facts describing f-structures and c-structures as
% fact/1.  Give separate predicate names for more efficient indexing.
assert_facts([]).
assert_facts([cf(1,Fact)|Fs]) :-
	assert_fact(Fact),
	%assert(fact(Fact)),
	assert_facts(Fs).

assert_fact(eq(attr(var(N),Attr),Val)) :- !,
	assert(attr_eq(N,Attr,Val)),
	(
	    Val = var(M) ->
	    assert(eq_attr(M,Attr,N)) % reversed, for out paths
	;
	    true
	).
assert_fact(in_set(var(N),var(M))) :- !,
	assert(in_set(N,M)),
	assert(set_in(M,N)). % reversed, for out paths
assert_fact(in_set(N,var(M))) :- !,
	assert(in_set(N,M)),
	assert(set_in(M,N)). % reversed, for out paths

assert_fact(subtree(Mother,Cat1,Partial,MorphNode)) :- !,
	split_complex_cat_atom(Cat1,Cat),
	assert(subtree(Mother,Cat,Partial,MorphNode)).
assert_fact(phi(MorphNode,FNode)) :- !,
	assert(phi(MorphNode,FNode)).
assert_fact(cproj(CNode,FNode)) :- !,
	assert(cproj(CNode,FNode)).
assert_fact(terminal(MorphNode,Morph,SurfNode)) :- !,
	assert(terminal(MorphNode,Morph,SurfNode)).
assert_fact(surfaceform(SurfNode,SurfForm,From,To)) :- !,
	assert(surfaceform(SurfNode,SurfForm,From,To)).
assert_fact(eq(proj(_,_),_)) :- !.  % ignore
assert_fact(eq(var(_),var(_))) :- !.  % ignore
assert_fact(scopes(var(A),var(B))) :- !,
	assert(fprecedes(A, B)).
%assert_fact(scopes(var(A),var(B))) :- !,
%	assert(scopes(sigma(A), sigma(B))).
%assert_fact(notscopes(var(A),var(B))) :- !,
%	assert(notscopes(sigma(A), sigma(B))).
assert_fact(UnknownFact) :-
	nl, write('WARNING: ignoring unknown type of c/f-str fact'),
	nl, write(UnknownFact), nl.


abolish_facts :-
	retractall(attr_eq(_,_,_)),
	retractall(eq_attr(_,_,_)),
	retractall(set_in(_,_)),
	retractall(in_set(_,_)),
	retractall(fprecedes(_,_)),
	%retractall(scopes(_,_)),
	%retractall(notscopes(_,_)),
	retractall(subtree(_,_,_,_)),
	retractall(phi(_,_)),
	retractall(cproj(_,_)),
	retractall(terminal(_,_,_)),
	retractall(surfaceform(_,_,_,_)).




fix_coordination(FS,FS1) :-
	reentrant_semforms(FS,[],SemFormSets),
	(
	    SemFormSets = [_|_] ->
	    reentrancy_substs(FS,SemFormSets,FS1)
	;
	    FS1 = FS
	).



reentrant_semforms([],SFs,SFs).
reentrant_semforms([cf(V,eq(attr(var(N),'PRED'),semform(P,Id,_,_))) | FSs],
		   SFs0,SFs1) :-
	\+ (   member(Ns-_,SFs0),
	       member(N,Ns)
	   ),
	coindexed_semforms(FSs,Id,V,P,SFNodes),
	SFNodes = [_|_],
	!,
	principal_semform_node([N|SFNodes],PN,SFNsMinus),
	reentrant_semforms(FSs,[SFNsMinus-PN | SFs0], SFs1).
reentrant_semforms([_|FSs], SFs0, SFs1) :-
	reentrant_semforms(FSs, SFs0, SFs1).


coindexed_semforms([],_,_,_,[]).
coindexed_semforms([cf(V,eq(attr(var(N),'PRED'),semform(P,Id,_,_))) | FSs],
		   Id,V,P,[N|Ns]) :- !,
	coindexed_semforms(FSs,Id,V,P,Ns).
coindexed_semforms([_|FSs],Id,V,P,Ns) :-
	coindexed_semforms(FSs,Id,V,P,Ns).


principal_semform_node(Nodes,PN,NodesMinus) :-
	select(PN,Nodes,NodesMinus),
	phi(_,var(PN)), !.
principal_semform_node([PN|NodesMinus],PN,NodesMinus).

reentrancy_substs([],_SemFormSets,[]).
reentrancy_substs([cf(_V,eq(attr(var(N),_),_))|FS],
		  SemFormSets,FS1) :-
	member(Ns-_,SemFormSets),
	member(N,Ns),
	!,
	reentrancy_substs(FS,SemFormSets,FS1).
reentrancy_substs([F|Fs],SemFormSets,[SF|SFs]) :-
	reent_subst(F,SemFormSets,SF),
	reentrancy_substs(Fs,SemFormSets,SFs).

reent_subst(V,_,V) :- var(V), !.
reent_subst(N,SemFormSets,PN) :-
	member(Ns-PN,SemFormSets),
	member(N,Ns),
	!.
reent_subst(Exp,SemFormSets,SExp) :-
	functor(Exp,F,N),
	functor(SExp,F,N),
	reent_subst_arg(N,Exp,SemFormSets,SExp).

reent_subst_arg(0,_Exp,_SemFormSets,_SExp) :- !.
reent_subst_arg(N,Exp,SemFormSets,SExp) :-
	arg(N,Exp,Arg),
	arg(N,SExp,SArg),
	reent_subst(Arg,SemFormSets,SArg),
	M is N-1,
	reent_subst_arg(M,Exp,SemFormSets,SExp).




split_complex_cat_atom(Cat1,Cat) :-
	% we can get things like 'AUX[perf,fin]' as an atom, which we
	% want to reformat as 'AUX'(['perf','fin'])
	atom_chars(Cat1,CatChars),
	split_at_first_char(CatChars,0'[,AtCatChars,ArgChars),
	atom_chars(AtCat,AtCatChars),
	(
	    ArgChars = [] ->
	    Cat = AtCat
	;
	    split_complex_cat_arg_chars(ArgChars,ArgList),
	    Cat =.. [AtCat,ArgList]
	).


% cut a string in two at the first occurrence of the character SC
% note: removes SC
split_at_first_char([],_,[],[]).
split_at_first_char([SC|Cs],SC,[],Cs) :- !.
split_at_first_char([C|Cs],SC,[C|Hs],T) :-
	split_at_first_char(Cs,SC,Hs,T).


split_complex_cat_arg_chars(ArgChars,ArgList) :-
	reverse(ArgChars,[0']|RArgChars]),
	reverse(RArgChars,ArgChars1),
	split_complex_cat_arg_chars1(ArgChars1,ArgList).

split_complex_cat_arg_chars1(ArgChars,[Arg|ArgList]) :-
	split_at_first_char(ArgChars,0',,A1Chars,AsChars),
	atom_chars(Arg,A1Chars),
	(
	    AsChars = [] ->
	    ArgList = []
	;
	    split_complex_cat_arg_chars1(AsChars,ArgList)
	).
	

%===================================================================
%
% Compilation of f-descriptions
%

compile_fdescription(FDesc0,CDesc,Locals) :-
	reformat_local_names(FDesc0,FDesc1),
	reformat_semforms(FDesc1,CDesc),
	locals_in_expression(CDesc,[],Locals), !. % shouldn't need cut


% local names are atoms starting with '%'
%   --- a really terrible choice of notation for prolog,
%       but there you are...

reformat_local_names(V,V) :- var(V), !.
reformat_local_names(Loc,'%'(Name)) :-
	atom(Loc),
	atom_chars(Loc,[0'%,H|T]),
	!,
	atom_chars(Name,[H|T]).
reformat_local_names(A,B) :-
	functor(A,F,N),
	functor(B,F,N),
	reformat_local_names_arg(N,A,B).
reformat_local_names_arg(0,_A,_B) :- !.
reformat_local_names_arg(N,A,B) :-
	arg(N,A,AA),
	arg(N,B,BB),
	reformat_local_names(AA,BB),
	M is N-1,
	!,
	reformat_local_names_arg(M,A,B).




reformat_semforms(V,V) :- var(V), !.
reformat_semforms(sf(Pred,Paths1,Paths2),
		  semform(Pred,_,VPaths1,VPaths2)) :- !,
	semform_path_var_wraps(Paths1,VPaths1),
	semform_path_var_wraps(Paths2,VPaths2).
reformat_semforms(Expr,Expr1) :-
	functor(Expr,F,N),
	functor(Expr1,F,N),
	reformat_semforms_arg(N,Expr,Expr1).
reformat_semforms_arg(0,_Expr,_Expr1) :- !.
reformat_semforms_arg(N,Expr,Expr1) :-
	arg(N,Expr,Arg),
	arg(N,Expr1,Arg1),
	reformat_semforms(Arg,Arg1),
	M is N-1,
	!,
	reformat_semforms_arg(M,Expr,Expr1).

semform_path_var_wraps(V,V) :- var(V), !.
semform_path_var_wraps([],[]).
semform_path_var_wraps([p:Path|Ps],[var(p:Path)|VPs]) :- !,
	semform_path_var_wraps(Ps,VPs).
semform_path_var_wraps(['^'|Ps],[var('^')|VPs]) :- !,
	semform_path_var_wraps(Ps,VPs).
semform_path_var_wraps(['%'(X)|Ps],[var('%'(X))|VPs]) :- !,
	semform_path_var_wraps(Ps,VPs).
semform_path_var_wraps([X|Ps],[X|VPs]) :- 
	semform_path_var_wraps(Ps,VPs).




locals_in_expression(V,Locals,Locals) :- var(V), !.
locals_in_expression('%'(Loc),Locals0,Locals1) :- !,
	(
	    member(Loc-_,Locals0) ->
	    Locals1 = Locals0
	;
	    Locals1 = [Loc-_|Locals0]
	).
locals_in_expression(Desc,Locals0,Locals1) :-
	functor(Desc,_,N),
	locals_in_expression_arg(N,Desc,Locals0,Locals1).

locals_in_expression_arg(0,_Desc,Locals,Locals) :- !.
locals_in_expression_arg(N,Desc,Locals0,Locals2) :-
	arg(N,Desc,Arg),
	locals_in_expression(Arg,Locals0,Locals1),
	M is N-1,
	locals_in_expression_arg(M,Desc,Locals1,Locals2).


