%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%                                                             %%%
%%%         File: xleinterface.pl                               %%%
%%%       Author: Martin Emele                                  %%%
%%%      Revised: Dick Crouch                                   %%%
%%%      Purpose: Run XLE as subroutine, via pipes              %%%
%%%      Created: Wed Jul  2 21:47:12 1997                      %%%
%%%    Copyright: Institut fuer maschinelle Sprachverarbeitung  %%%
%%%               Universitaet Stuttgart                        %%%
%%%                                                             %%%
%%% see ~kay/xle/transfer.xle.pl for original version of file   %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

:- use_module(library(system),[exec/3,wait/2]).
:- use_module(library(charsio)).

:- dynamic  xle_pid/1, xle_input/2, xle_output/2, xle_err/2,
	    xle_name_pid/2.

%%% we use the symbol  '#%' as the end_of_stream marker 
%%% This allows us to check for command termination and
%%% can be used for process synchronization.

xle_start :- xle_start(_).
xle_start(PID) :-
	exec('xle',[pipe(Input),pipe(Output),std],PID),
	format(Input,'puts "#%"~n',[]),
	format('XLE process started with PID: ~d~n',[PID]),
	format('To kill it, use command: xle_exit(~d).~n',[PID]),
	retractall(xle_pid(PID)),
	retractall(xle_input(PID,_)),
	retractall(xle_output(PID,_)),
	asserta(xle_pid(PID)),
	asserta(xle_input(PID,Input)),
	asserta(xle_output(PID,Output)),
	copy_until_eos(Output),
	format('~n~n********************************************~n',[]),
	format(    '* XLE process started with PID: ~d~n',[PID]),
	format(    '* To kill it, use command: xle_exit(~d).~n',[PID]),
	format(    '* Or xle_exit_all. to kill all XLE processes~n',[]),      
	format(    '********************************************~n',[]).


xle_exit :- xle_exit(_).
xle_exit(PID) :-
	retract(xle_pid(PID)),
	retract(xle_input(PID,Input)),
	retract(xle_output(PID,Output)),
	retractall(xle_name_pid(_Name,PID)),
	format(Input,'exit~n',[]),
	close(Input),
	close(Output),
	on_exception( Error, 
		wait(PID,Status),
		print_message( error, Error ) ),
	format(user_error,'~Nexited xle process ~d with status: ~p~n',[PID,Status]).

xle_exit_all :-
	repeat,
	( xle_pid(PID) -> xle_exit(PID),fail ; true ).
 
xle_format(PID,Format,Arguments) :-
	xle_input(PID,Stream),
	format(Stream,Format,Arguments).

xle(PID,Command) :-
	xle_format(PID,'~a~n',[Command]),
	xle_format(PID,'puts "#%"~n',[]),
	xle_output(PID,Output),
	copy_until_eos(Output).
xle(PID,Format,Arguments) :-
	xle_format(PID,Format,Arguments),
	xle_format(PID,'puts "#%"~n',[]),
	xle_output(PID,Output),
	copy_until_eos(Output).


xle_ret(PID,Command,Return) :-
	xle_format(PID,'~a~n',[Command]),
	xle_format(PID,'puts "#%"~n',[]),
	xle_output(PID,Output),
	copy_until_eos(Output,Return).
xle_ret(PID,Format,Arguments,Return) :-
	xle_format(PID,Format,Arguments),
	xle_format(PID,'puts "#%"~n',[]),
	xle_output(PID,Output),
	copy_until_eos(Output,Return).


xle_exec(Command) :-
	xle_pid(PID),
	xle(PID,Command).
xle_exec(Format,Arguments) :-
	xle_pid(PID),
	xle(PID,Format,Arguments).

xle_exec_ret(Command,Return) :-
	xle_pid(PID),
	xle_ret(PID,Command,Return).
xle_exec_ret(Format,Arguments,Return) :-
	xle_pid(PID),
	xle(PID,Format,Arguments,Return).


check_xle_running :-
	(
	    xle_pid(_) -> true  % running already
	;
	    xle_start           % start it off
	).


check_xle_running(Name,PId) :-
	(
	    xle_name_pid(Name,PId) -> true
	;
	    xle_start(PId),
	    asserta(xle_name_pid(Name,PId))
	).



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% we use the symbol  '#%' as the end_of_stream marker 
%%% This allows us to check for command termination. 

copy_until_eos(Stream) :-
	repeat,
	get0(Stream,Character), put(Character),
	check_for_eos(Character,Stream),
	flush_output,
	!.

copy_until_eos(Stream,String) :-
	get0(Stream,Character), format("~s",[[Character]]),
	(
	    check_for_eos(Character,Stream) ->
	    String = [],
	    flush_output
	;
	    String = [Character|String1],
	    copy_until_eos(Stream,String1)
	).


check_for_eos(0'#, Stream) :- !,
	peek_char(Stream,Char),
	Char = 0'%,
	get0(Stream,Char).
check_for_eos(_, Stream) :-
	at_end_of_stream(Stream).


read_line(Stream,[]) :-
	at_end_of_line(Stream),
	get0(Stream,_),!.
read_line(Stream,[C1|Cs]) :-
	get0(Stream,C1),
	( C1 < 0 -> Cs=[] 
	; put(C1), 
	  read_line(Stream,Cs)
	).

read_line(Stream) :-
	repeat,
	get0(Stream,C1),
	( C1 < 0 -> true
	; put(C1),
	  at_end_of_line(Stream)
	),
	!.




