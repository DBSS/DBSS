/* (c) 2001 by the Xerox Corporation.  All rights reserved */

/* ------------------------------------------------------------------------ */
/* Project:   XLE Test Suite                                                */
/* Author:    Dick Crouch                                                   */
/* File:      testreport.pl                                                 */
/* Language:  SICStus Prolog 3.7.1                                          */
/* Stuff for reporting on results of test suites                            */
/* ------------------------------------------------------------------------ */


%  Assumes that running the test suite will have set up
%
%      ts_syn_result(SentenceString,MatchedContexts,PackedFS,FDescription)
%
%  records.
%
%  Assumes that TSPreviousResultsFile contains parallel records
%
%      ts_syn_previous(SentenceString,MatchedContexts,PackedFS,FDescription)
%
%  from some earlier test run, which can be consulted
 

ts_report(no_comparison_file) :- !,
	tsr_failed_parses,
	retractall(ts_syn_previous(_,_,_,_)),
	format("~n~n*** No earlier results to compare with. **~n",
	       []).
ts_report(TSPreviousResultsFile) :-
	tsr_failed_parses,
	format("~n~n*** Comparing with previous results in ~w. **~n~n",
	       [TSPreviousResultsFile]),
	safe_abolish(ts_syn_previous/4),
	consult(TSPreviousResultsFile), nl,nl,
	ts_syn_report.

ts_report :-
	(
	    current_predicate(ts_syn_previous,_) -> true
	;
	    retractall(ts_syn_previous(_,_,_,_))
	),
	tsr_failed_parses,
	ts_syn_report.

ts_syn_report :-
	tsr_added_sentences,
	tsr_removed_sentences,
	tsr_changed_parses,
	tsr_more_parses,
	tsr_fewer_parses.


tsr_failed_parses :-
	findall(Stce_Fail_Desc, ts_failed_parse(Stce_Fail_Desc), SFDs),
	format("~n~nSentences that failed to parse/match description:~n",[]),
	format(    "=================================================~n~n",[]),
	map_format("~n~s~n**~w~p~w",SFDs).

tsr_added_sentences :-
	findall(StceFD, ts_added_sentence(StceFD), StceFDs),
	(
	    StceFDs = [] -> true
	;
	    format("~n~nNew sentences added to test suite:~n",[]),
	    format(    "==================================~n~n",[]),
	    map_format('~n"~s"~n**under description:~p',StceFDs)
	).

tsr_removed_sentences :-
	findall(StceFD, ts_removed_sentence(StceFD), StceFDs),
	(
	    StceFDs = [] -> true
	;
	    format("~n~nSentences removed from test suite:~n",[]),
	    format(    "==================================~n~n",[]),
	    map_format('~n"~s"~n**under description:~p',StceFDs)
	).

tsr_changed_parses :-
	findall(S_B_A, ts_changed_parses(S_B_A), SBAs),
	(
	    SBAs = [] -> true
	;
	    format("~n~nSentences with altered f-structures:~n",[]),
	    format(    "====================================~n~n",[]),
	  map_format('~n"~s"~n  Contexts before:~w~n  Contexts after:~w~n~n',
		     SBAs)
	).
tsr_more_parses :-
	findall(S_B_A, ts_more_parses(S_B_A), SBAs),
	(
	    SBAs = [] -> true
	;
	    format("~n~nSentences with more matching parses:~n",[]),
	    format(    "====================================~n~n",[]),
	  map_format('~n"~s"~n  Contexts before:~w~n  Contexts after:~w~n~n',
		     SBAs)
	).
tsr_fewer_parses :-
	findall(S_B_A, ts_fewer_parses(S_B_A), SBAs),
	(
	    SBAs = [] -> true
	;
	    format("~n~nSentences with fewer matching parses:~n",[]),
	    format(    "=====================================~n~n",[]),
	  map_format('~n"~s"~n  Contexts before:~w~n  Contexts after:~w~n~n',
		     SBAs)
	).



ts_added_sentence([Stce,FDesc]) :-
	ts_syn_result(Stce,_,_,FDesc),
	\+ ts_syn_previous(Stce,_,_,FDesc).

ts_removed_sentence([Stce,FDesc]) :-
	ts_syn_previous(Stce,_,_,FDesc),
	\+ ts_syn_result(Stce,_,_,FDesc).

ts_failed_parse([Stce,'no parses','**','**']) :-
	ts_syn_result(Stce,no_parse,_,NotFail),
	\+ NotFail = fail,
	\+ NotFail = never:_. 
ts_failed_parse([Stce,'should not parse, but does','**','**']) :-
	ts_syn_result(Stce,[],Parse,fail),
	\+ Parse = no_parse. 
ts_failed_parse([Stce,'should never match description, but does',
		 never:Desc,matching_contexts([C|Cs])]) :-
	ts_syn_result(Stce,[C|Cs],Parse,never:Desc),
	\+ Parse = no_parse. 
ts_failed_parse([Stce,'no parse matching description',Desc,'**']) :-
	ts_syn_result(Stce,[],_,Desc).

ts_fewer_parses([Stce,Before,After]) :-
	ts_syn_result(Stce,After,_,Desc),
	ts_syn_previous(Stce,Before,_,Desc),
	(
	    After = no_parses ->
	    \+ Before = no_parses
	;
	    Before = no_parses -> fail
	;
	    length(Before,BL),
	    length(After,AL),
	    BL > AL
	).

ts_more_parses([Stce,Before,After]) :-
	ts_syn_result(Stce,After,_,Desc),
	ts_syn_previous(Stce,Before,_,Desc),
	(
	    Before = no_parses ->
	    \+ After = no_parses
	;
	    After = no_parses -> fail
	;
	    length(Before,BL),
	    length(After,AL),
	    AL > BL
	).

ts_changed_parses([Stce,Before,After]) :-
	ts_syn_result(Stce,After,AFS,Desc),
	\+ After = no_parses, \+ After = [],
	ts_syn_previous(Stce,Before,BFS,Desc),
	\+ Before = no_parses, \+ Before = [],
	length(Before,BL),
	length(After,AL),
	BL = AL,
	findall(AfterFstr,
		(unpack_fstr(AFS,ACtx-AfterFstr),
		 member(ACtx,After)),
		AfterFSs),
	findall(BeforeFstr,
		(unpack_fstr(BFS,BCtx-BeforeFstr),
		 member(BCtx,Before)),
		BeforeFSs),
	disjoint_lists(AfterFSs,BeforeFSs).


disjoint_lists(As,Bs) :-
	member(A,As),
	non_member(A,Bs),
	!.

map_format(_,[]) :- !.
map_format(Format,[H|T]) :-
	format(Format,H),
	map_format(Format,T).
	



%=================================================================

list_sentences :-
	ts_syn_result(Stce,_,_,_),
	format('~n"~s"',[Stce]),
	fail.
list_sentences.


about_sentence(SubString) :-
	ts_syn_result(Stce,Context,_FStr,FDesc),
	substring(SubString,Stce),
	format('~n~n~n=====================',[]),
	format('~nResults for sentence: "~s"~n',[Stce]),
	format("~n**Description specified: ~p",[FDesc]),
	(
	    ts_syn_previous(Stce,PContext,_PFstr,FDesc) ->
	    New = n
	;
	    format("~n**Added to test suite",[]),
	    New = y
	),
	(
	    Context = no_parse ->
	    format("~n**No parses at all for sentence",[])
	;
	    Context = [] ->
	    format("~n**No parses matching description",[])
	;
	    Context = [[]] ->
	    format("~n**Unambiguous parse and matches description",[])
	;
	    format("~n**Parse context(s) matching description: ~w",Context)
	),
	(
	 New = n ->
	  (
	    PContext = no_parse ->
	    format("~n**Previously: no parses at all for sentence",[])
	  ;
	    PContext = [] ->
	    format("~n**Previously: no parses matching description",[])
	  ;
	    PContext = [[]] ->
	    format("~n**Previously: unambiguous parse and matched description",[])
	  ;
	    format("~n**Previously: parse context(s) matching description: ~w",PContext)
	  )
	;
	  true
	),
	fail.
about_sentence(_SubString).



display_current_fstr(SubString) :-
	ts_syn_result(Stce,_Context,FStr,_FDesc),
	substring(SubString,Stce),
	(
	    other_matching_sentence(c,SubString,Stce,OtherStce) ->
	    oms_failure_msg(SubString,Stce,OtherStce),
	    !, fail
	;
	    copy_term(FStr,FStr1),
	    FStr1 = fstructure(_,_,Choices,_,_,_),
	    instantiate_choices(Choices),
	    write_fstr_to_file(FStr1,'tmp_current_fschart.pl'),
	    check_xle_running(current,PID),
	    xle(PID,'read-prolog-chart-graph tmp_current_fschart.pl'),
	    delete_file('tmp_current_fschart.pl',[ignore])
	).

display_previous_fstr(SubString) :-
	ts_syn_previous(Stce,_Context,FStr,_FDesc),
	substring(SubString,Stce),
	(
	    other_matching_sentence(p,SubString,Stce,OtherStce) ->
	    oms_failure_msg(SubString,Stce,OtherStce),
	    !, fail
	;
	    copy_term(FStr,FStr1),
	    FStr1 = fstructure(_,_,Choices,_,_,_),
	    instantiate_choices(Choices),
	    write_fstr_to_file(FStr1,'tmp_prev_fschart.pl'),
	    check_xle_running(previous,PID),
	    xle(PID,'read-prolog-chart-graph tmp_prev_fschart.pl'),
	    delete_file('tmp_prev_fschart.pl',[ignore])
	).

write_term_to_file(Term,File) :-
        open(File,write,Stream),
        portray_clause(Stream,Term),
        close(Stream).

write_fstr_to_file(fstructure(Sent,Props,Choices,Abbrevs,FS,CS),File) :-
        open(File,write,Stream),
        format(Stream,"fstructure(~q, ~q, ~w, ~w,",
	       [Sent,Props,Choices,Abbrevs]),
	format(Stream,"~n[",[]),
	write_fstr_clauses_to_stream(FS,Stream),
	format(Stream,"~n],~n[~n",[]),
	write_fstr_clauses_to_stream(CS,Stream),
	format(Stream,"]~n).~n",[]),
        close(Stream).

write_fstr_clauses_to_stream([],_Stream).
write_fstr_clauses_to_stream([cf(Ctx,Val)|FSs],Stream) :-
	format(Stream,"~ncf(~w,~q)",[Ctx,Val]),
	(
	    FSs = [] ->
	    format(Stream,"~n",[])
	;
	    format(Stream,",~n",[])
	),
	write_fstr_clauses_to_stream(FSs,Stream).

other_matching_sentence(c,SubString,Stce,OtherStce) :-
	ts_syn_result(OtherStce,_,_,_),
	substring(SubString,OtherStce),
	\+ Stce = OtherStce,
	!.
other_matching_sentence(p,SubString,Stce,OtherStce) :-
	ts_syn_previous(OtherStce,_,_,_),
	substring(SubString,OtherStce),
	\+ Stce = OtherStce,
	!.

oms_failure_msg(SubString,Stce,OtherStce) :-
	format('~n!!!Search string "~s" has multiple matches!!!',
	       [SubString]),
	format('~n!!! e.g. "~s"~n!!!      "~s"',[Stce,OtherStce]),
	format('~n!!!Please try again with fuller search string~n',[]).


substring([],_) :- !.
substring([H|ST],[H|T]) :-
	append(ST,_,T),
	!.
substring(SubString,[_|T]) :-
	substring(SubString,T).


bye :- ts_exit.
quit :- ts_exit.
exit :- ts_exit.
ts_exit :- xle_exit_all, halt.

user_help :- ts_help.
ts_help :-
 format(
'                  XLE Test Suite Commands

run_test(<File>).
      * Run test suite in <File>
ts_report.
      * Report on results of test suite
ts_report(<PreviousFile>).
      * Report on results of test suite
        and compare with previous results stored in <PreviousFile>

list_sentences.
      * List all sentences in test suite
about_sentence("<SubString>").
      * Report on results for all sentences containing <SubString>
display_current_fstr("<SubString>").
      * Display f-structure for sentence containing <SubString>
        (SubString must uniquely identify a sentence)
display_previous_fstr("<SubString>").
      * Display f-structure from previous test run
        for sentence containing <SubString>

ts_help.
      * Print this message
ts_exit.
      * Exit test suite facility

pwd.
      * Prints out current working directory
cd(\'<Dir>\').
      * Changes current working directory to <Dir>
        (single quotes necessary if Dir contains / symbols)
purge_old_results(<TestFile>).
      * Deletes all old results files associated with TestFile

% Prolog note: periods terminating commands are required
%              as are double quotes around strings~n~n',
 []).

