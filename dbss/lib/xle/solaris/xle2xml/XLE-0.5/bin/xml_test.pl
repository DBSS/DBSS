#!/usr/bin/perl


#Author: Chris Culy (culy@fxpal.com)
#Date: 15 October 2003
#Revised: 2 February 2004
#Copyright 2003-2004 FXPAL All Rights Reserved

#test script for XLEStruct.pm (and CStruct.pm and FStruct.pm)
#usage: xml_test.pl xle_info_file.pl

use XLE::XLEStruct;

BEGIN {
    my $module = "XML::Simple";
    eval "use $module";
    if ($@) {
	warn "Warning: couldn't load $module"; # $@";
    }
}

#change $check to 1 if XML::Simple is installed and you want to check to see if the XML that is generated is well-forme
my $check = 1; 
my $xml_to_check;

my $fsFile = shift;

undef($/);
open(FS, $fsFile) or die "Couldn't open $fsFile!\n";
my $fsInfo = <FS>;
close(FS);

binmode(STDOUT, ":utf8"); #file output should be in utf8


#******************************************************************#
#This section illustrates how to create a c-structure only XML file
#uncomment the following lines to see it work

#my $cs = CStruct->new(fs=>$fsInfo);
##my $cs = CStruct->new(fs=>$fsInfo, mixedJ=>1); #for Japanese files with mixed encodings
#my $cs_xml = $cs->toXML(SENTENCE=>1, HEADER=>1, NODENUMS=>0, SUBLEXICAL=>0);
#print "$cs_xml\n";
#if ($check) {
#    $xml_to_check = $cs_xml;
#}

#******************************************************************#

#******************************************************************#
#This section illustrates how to create a f-structure only XML file
#uncomment the following lines to see it work

#my $fs = FStruct->new(fs=>$fsInfo);
##my $fs = FStruct->new(fs=>$fsInfo, mixedJ=>1); #for Japanese files with mixed encodings
#$fs_xml = $fs->toXML(SENTENCE=>1, HEADER=>1);
#print "$fs_xml\n";
#if ($check) {
#    $xml_to_check = $fs_xml;
#}


#******************************************************************#

#******************************************************************#
#This section illustrates how to create an XML file with all of the information from the .pl file for a single structure

my $xle = new XLEStruct(info=>$fsInfo);
#my $xle = new XLEStruct(info=>$fsInfo, mixedJ=>1); #for Japanese files with mixed encodings
my $combo_xml = $xle->toXML(NODENUMS=>1, PROPERTIES=>1, SUBLEXICAL=>1); #full
#my $combo_xml = $xle->toXML(NODENUMS=>1, PROPERTIES=>1, SUBLEXICAL=>1, INCLUDE_ATTRS=>"PRED,SUBJ,COMP,XCOMP,OBJ,ADJUNCT");
#my $combo_xml = $xle->toXML(NODENUMS=>1, PROPERTIES=>1, SUBLEXICAL=>1, EXCLUDE_ATTRS=>"SUBJ");

print $combo_xml;
if ($check) {
    $xml_to_check = $combo_xml;
}

#******************************************************************#

#This is bit is for checking that the xml created is well-formed
if ($check) {
    eval {
	XMLin($xml_to_check);
    };

    if ($@) {
	print STDERR "There was a problem: $@\n";
    } else {
	print STDERR "XML OK\n";
    }
}

