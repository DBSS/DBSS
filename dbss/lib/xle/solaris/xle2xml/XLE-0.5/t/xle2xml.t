#!/usr/bin/perl

#Author: Chris Culy
#Date: 26 November 2003
#Revised: 2 February 2004
#Copyright 2003-2004 FXPAL, All Rights Reserved

#Purpose: do regression testing on the samples

use Test;
#use XLE::XLEStruct;

my $sampleDir;
my $tmpFile;
my $numErrs;
my $cmd;
my @files;
my $numFiles;
my $check; 


BEGIN {
    my $module = "XML::Simple";
    eval "use $module";
    if ($@) {
	warn "\nWarning: couldn't load $module"; # $@";
	$check = 0;
    } else {
	$check = 1;
    }

    $sampleDir = "samples";
    $tmpFile = "/tmp/xlexml_tmp.xml";
    $cmd = "ls $sampleDir/*.pl";
    @files = split(/\n/, `$cmd`);

    $numFiles = scalar(@files);
    my $numTests = 1+ (2+$check)*$numFiles; #test require then: open file; maybe validate XML; compare with original;

    plan tests => $numTests;

    eval { require XLE::XLEStruct; return 1;};
    ok($@,'');
    die() if $@;
}

binmode(STDOUT, ":utf8"); #file output should be in utf8
undef($/);

foreach $fsFile (@files) {

    if (! open(FS, $fsFile)) {
	warn "Couldn't open $fsFile!\n";
	ok(0);
	next;
    } else {
	ok(1);
    }
    my $fsInfo = <FS>;
    close(FS);
    
    my $xle = new XLE::XLEStruct(info=>$fsInfo);
    my $combo_xml = $xle->toXML(NODENUMS=>1, PROPERTIES=>1, SUBLEXICAL=>1); #full

    if ($check) {
	eval {
	    XMLin($combo_xml);
	};
	
	if ($@) {
	    warn "\nThere was a problem creating XML for $fsFile: $@\n";
	    ok(0);
	} else {
	    #print STDERR "XML OK for $fsFile\n";
	    ok(1);
	}
    }
    
    my $xmlFile;
    ($xmlFile = $fsFile) =~ s/pl$/xml/;
    if (! open(TF, "> $tmpFile") ) {
	warn "\nCouldn't open $tmpFile!\n";
	ok(0);
	next;
    }
    binmode(TF, ":utf8"); #file output should be in utf8
    print TF $combo_xml;
    close TF;
    
    $cmd = "diff $xmlFile $tmpFile";
    my $diff = `$cmd`;

    if ($diff) {
	warn "\nWarning: Old and new XML files for $fsFile are different!\n";
	#print STDERR "$diff\n\n";
	$numErrs++;
	ok(0);

    } else {
	#print STDERR "$fsFile passed test\n";
	ok(1);
    }
}

#print STDERR $numErrs+0, " of $numFiles files failed.\n";
