package XLE::CStruct;

=head1 NAME
XLE::CStruct the module for manipulating c-structures

=head1 SYNOPSIS
(see also XLE::XLEStruct)

$cs = CStruct->new(fs=>$fsInfo);
$cs_xml = $cs->toXML(SENTENCE=>1, HEADER=>1, NODENUMS=>0, SUBLEXICAL=>0);

=head1 AUTHOR 
Chris Culy (culy@fxpal.com)

=head1 DATE 
15 October 2003
Revised: 30 January 2004

=head1 COPYRIGHT
Copyright 2003-2004 FXPAL All Rights Reserved

=head1 METHODS
 

=cut

#variables:
#NodeNum
#NodeName
#Dtrs
#RtUpLinker
#LeftDownLinker
#Phi
#Surface (just for terminals)
#LeftEdge: based on indices in frontier #this is a vertex in the chart, do we need this?
#RtEdge: based on indices in frontier # this is a vertex in the chart, do we need this?
#SENTENCE


use XLE::Utilities;

sub new {

=head2 new
    new(fs=>val): root
    new(fs=>val, node=>val): non-root

=cut

    my $classname = shift;
    my $self = {};
    bless($self, $classname);
    $self->_init(@_);
    return $self;
}

sub _init {

    my $self = shift;
    
    my %args = (@_);

    my $fs = $args{fs};

    my ($node, $rtCat);

    my $sentence;
    if (! defined($args{node}) ) {
	#need to decode fs; mixedJ indicates special case where XLE outputs sentence in UTF-8 but rest in euc-jp
	#in that case, we need to get the sentence before we decode fs
	if ($args{mixedJ}) {
	    $sentence = &find_sentence( info=>$fs, mixedJ=>1);
	}

	$fs = &decode_fs_to_utf8($fs);

	($node, $rtCat) = $self->_find_root($fs);

	#but the rest of the algorithm needs the decoded sentence, since the leaves are all euc-jp
	$self->{SENTENCE} = &find_sentence(info=>$fs);

	#$fs =~ /^fstructure\((.*),$/m;
	#my $tmp = $1;
	#$tmp =~ s/^\'(.*)\'$/\1/;
	#$tmp =~ s/\\(['"\\])/\1/g; #"' remove prolog escaping
	#$self->{SENTENCE} = $tmp;

    } else {
	$node = $args{node};
    }
    $self->_fs2tree($fs, $node);

    if ($sentence) {
	$self->{SENTENCE} = $sentence;
    }
}


sub _find_root {
#find the root node of the tree; return node # and root category
#subtree(n,$rootCat,...), where n doesn't occur in any other subtree

    my $self = shift;
    my $fs = shift;

    my $rootCat;

    if ( $fs =~ /subtree\((\d+),\'\*TOP\*\',/ ) {  #special case for *TOP*, which isn't recorded as root
	return ($1, "\'*TOP*\'");
    } elsif ($fs =~ /\'rootcategory\'\((.+)\)$/m) {
	$rootCat = $1;
    } else {
	return; 
    }

    my $what;

    while ( $fs =~ /subtree\((\d+),$rootCat/g ) {
	$what = $1;
	if ($fs !~ /subtree.*,$what,/) {
   	    return ($what, $rootCat)
	}
    }
    return; #error
}

#Terminal nodes are represented as having a node id, a label, and a list of surface forms that they map to. Surface forms are represented as having a node id, a label, and a left and right vertex. 

sub _fs2tree {
    my $self = shift;
    my ($fs, $m) = @_;

    $self->{NodeNum} = $m;
    $fs =~ /phi\($m,var\((\d+)\)\)\),$/m;
    $self->{Phi} = $1;

   #find daughters of m
    my @dtrs;

    #first figure out where we are
    if ($fs =~ /subtree\($m,(.*?),([-\d]+),(\d+)/ ) { #subtree(m,name,downLinker,rtDtr)
	my ($name, $downLeftLinker, $rtDtr) = ($1, $2, $3);

	$self->{Name} = $name;
	$self->{LeftDownLinker} = $downLeftLinker;
	$self->{RtUpLinker} = $rtDtr;
	
	@dtrs = ($rtDtr);

	#subtree(oldDownLeftLinker,name,newDownLeftLinker,dtr)
	my $d;
	while($downLeftLinker ne "-") {
	    $fs =~ /subtree\($downLeftLinker,\Q$name\E,([-\d]+),(\d+)/;
	    ($downLeftLinker, $d) = ($1, $2);
	    unshift(@dtrs, $d);
	}

    } elsif ($fs =~ /terminal\($m,(.*?),\[(\d+)\]\)\),$/m) {
	$self->{Name}=$1;
	#@dtrs = ($2);
	#NB: not doing anything with the surface forms mentioned in the terminal predicate
	#    the pattern above is probably incorrect in general, since we could have a list of surfaceforms: [a,b,c]

	#we need to get the surfaceform here, since it may have the same node # as terminal

	if ($fs =~ /surfaceform\($2,(.*?),(\d+),(\d+)\)\)/) {
	    $self->{Surface} = $1;
	    $self->{LeftEdge} = $2;
	    $self->{RtEdge} = $3;
	} else {
	    print STDERR "Terminal without surfaceform at ", $self->{Name}, "\n";
	}

    } elsif ( $fs =~ /surfaceform\($m,(.*?),(\d+),(\d+)\)\)/) {
	#not really used...
	print STDERR "Shouldn't have gotten to this surfaceform: $1\n";
	$self->{Surface} = $1;

    } else {
	return("Error: no correct match for $m");
    }
    
    foreach $d (@dtrs) {
	push(@{$self->{DTRS}}, XLE::CStruct->new(fs=>$fs, node=>$d)); 
        #this works, too
	#my $tmp = CStruct->new(fs=>$fs, node=>$d);
	#push(@{$self->{DTRS}}, $tmp);
    }

    if (@dtrs) {
	$self->{LeftEdge} = $self->{DTRS}->[0]->{LeftEdge};
	$self->{RtEdge} = $self->{DTRS}[scalar(@dtrs) -1]->{RtEdge};
    }
     
}

sub walkTree {

=head2 walkTree(f)
    pass a function ref f to be evaluated
    the function gets the current node plus the evaluation of the dtrs in l-r order

=cut

    my $self = shift;

    my $f = shift;
    my @vals = ();
    my @dtrs = @{$self->{DTRS}};
    foreach $d (@dtrs) {
	my $tmp = $d->walkTree($f);
	push(@vals, $tmp);
    }
    return &$f($self, @vals);
}

sub toParens {

=head2 toParens() 
    parenthized c-structure

=cut

    my $self = shift;
    
    my $what;

    my $name;
    ($name = $self->{Name}) =~ s/^\'(.*)\'$/\1/;
    
    $what = "(" . $name . ":" . $self->{NodeNum} . ":phi:" . $self->{Phi};
    
    if ($self->{Surface}) {
	$what .= " (" . $self->{Surface} . ")";
    } else {
    
	foreach $d (@{$self->{DTRS}}) {
	    $what .= " " . $d->toParens();
	}
	$what .= ")";
    }

    return $what;
}

sub toXML {

=head2 toXML
	Parameter	Values	Meaning
	HEADER		0,1	1: Include XML header
	SENTENCE	0,1	1: Include the original sentence
	NODENUMS	0,1	1: Include the nodenumbers in the c-structure
	SUBLEXICAL	0,1	1: Include sublexical nodes in the c-structure
	(symbol) 	replacement	replace the symbol by its textual value in node names

Non-alphanumeric symbols are not valid XML names, so they need to be replaced in some of the node-names. The currently defined replacements are:

 			"*TOP*" => "STAR_TOP_STAR", 
		       "+" => "PLUS.", 
		       "`" => "BACKQUOTE",
		       "\\'" => "QUOTE",
		       "\"" => "DOUBLEQUOTE",
		       "," => "COMMA",
		       "?" => "QUESTION_MARK",
		       "!" => "EXCLAMATION_POINT",
		       ":" => "COLON",
		       ";" => "SEMICOLON",
		       "(" => "LEFT_PAREN",
		       ")" => "RIGHT_PAREN",
		       "{" => "LEFT_BRACE",
		       "}" => "RIGHT_BRACE",
		       "[" => "LEFT_BRACKET",
		       "]" => "RIGHT_BRACKET",
		       "/" => "FORWARD_SLASH",
		       "\$" => "DOLLAR_SIGN",
		       "*" => "ASTERISK", 
		       "%" => "PERCENT",
		       "&" => "AMPERSAND",

	It is possible to add new symbol/replacement pairs, as well as to override existing ones. For example, to use "CURLY_BRACKET" instead of "BRACE":

	$myXMLOBJ->toXML("{"=>"OPEN_CURLY_BRACKET", "}"=>"CLOSE_CURLY_BRACKET");

If you have suggestions for additions/modifications, please let me know.

=cut

    my $self = shift;

    my %symbolNames = (
		       "*TOP*" => "STAR_TOP_STAR", 
		       "+" => "PLUS.", 
		       "`" => "BACKQUOTE",
		       "\\'" => "QUOTE",
		       "\"" => "DOUBLEQUOTE",
		       "," => "COMMA",
		       "?" => "QUESTION_MARK",
		       "!" => "EXCLAMATION_POINT",
		       ":" => "COLON",
		       ";" => "SEMICOLON",
		       "(" => "LEFT_PAREN",
		       ")" => "RIGHT_PAREN",
		       "{" => "LEFT_BRACE",
		       "}" => "RIGHT_BRACE",
		       "[" => "LEFT_BRACKET",
		       "]" => "RIGHT_BRACKET",
		       "/" => "FORWARD_SLASH",
		       "\$" => "DOLLAR_SIGN",
		       "*" => "ASTERISK", 
		       "%" => "PERCENT",
		       "&" => "AMPERSAND",
		       );

    my %args = (
		HEADER => 1,     #include XML directive or not
		SENTENCE => 1,   #include the sentence or not
		NODENUMS => 1,   #include the node numbers or not 
		SUBLEXICAL => 1, #include the sublexical info or not
		(%symbolNames),
		@_,
		);

    
    my $what;

    my $name;
    ($name = $self->{Name}) =~ s/^\'(.*)\'$/\1/;
    $name =~ s/\[(.*?)\]/\.\1/g; #brackets aren't legal, change to period

    #my $loop = 1;
    #if (!$loop) {
	#$name =~ s/\*TOP\*/STAR_TOP_STAR/g; 
	#$name =~ s/\+/PLUS./g; #+ isn't legal
	#$name =~ s/\`/BACKQUOTE/g;
	#$name =~ s/\\\'/QUOTE/g;
	#$name =~ s/\"/DOUBLEQUOTE/g;
	#$name =~ s/,/COMMA/g;
	#$name =~ s/\?/QUESTION_MARK/g;
	#$name =~ s/\!/EXCLAMATION_POINT/g;
	#$name =~ s/:/COLON/g;
	#$name =~ s/;/SEMICOLON/g;
	#$name =~ s/\(/LEFT_PAREN/g;
	#$name =~ s/\)/RIGHT_PAREN/g;
	#$name =~ s/\{/LEFT_BRACE/g;
	#$name =~ s/\}/RIGHT_BRACE/g;
	#$name =~ s/\[/LEFT_BRACKET/g;
	#$name =~ s/\]/RIGHT_BRACKET/g;
	#$name =~ s/\//FORWARD_SLASH/g;
	#$name =~ s/\$/DOLLAR_SIGN/g;
	#$name =~ s/\*/ASTERISK/g; 
	#$name =~ s/\%/PERCENT/g;
	#$name =~ s/\&/AMPERSAND/g;
    #} else {
	
	foreach $sym (sort {length($b) <=> length($a)} keys %symbolNames) {
	    $name =~ s/\Q$sym\E/$symbolNames{$sym}/g;
	}
    #}

    $name =~ s/ /_/g; #change spaces to underscores
    $name =~ s/^([^A-Za-z])/_\1/; #can't start with non-alphbetic

    #$what .= "<" . $name;
    $what .= "<node label=\"$name\""; #new
    if ($args{NODENUMS}) {
	$what .= " nodenum = \"" . $self->{NodeNum} . "\"";
    } 
    $what .=  " phi= \"" . $self->{Phi} . "\">\n";



    if (! defined($self->{Surface}) ) {
	if (! $args{SUBLEXICAL} ) {
	    #if a dtr is *_BASE, then take the SURFACE of that dtr's dtr and ignore the others
	    #not sure if this is correct

	    my $dtrInfo;
	    foreach $d (@{$self->{DTRS}}) {
		if ($d->{Name} =~ /_BASE/) {
		    $dtrInfo = $d->{DTRS}->[0]->{Surface};
		    $dtrInfo =~ s/^\'(.*)\'$/\1/;
		    $dtrInfo = &escape($dtrInfo) . "\n";
		    last;
		} else {
		    $dtrInfo .= " " . $d->toXML(@_);
		}
	    }
	    $what .= $dtrInfo;

	} else {
	    foreach $d (@{$self->{DTRS}}) {
		$what .= " " . $d->toXML(@_);
	    }
	}

    } else {
	my $content = $self->{Surface};
	$content =~ s/^\'(.*)\'$/\1/;
	$what .= &escape($content) . "\n";

    } 

    #$what .= "</$name>\n";
    $what .= "</node>\n"; #new


    if (defined($self->{SENTENCE})) {
       	my $header = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
	my $preamble;
	if ($args{HEADER}) {
	    $preamble = "$header"
	}
	$preamble .= "<cs>\n";
	if ($args{SENTENCE}) {
	    $preamble .= "<sentence>" . &escape($self->{SENTENCE}) . "</sentence>\n\n";
	}
	
	$what = "$preamble$what\n</cs>\n";
    }

    return $what;
}



sub find_end {
    #find start and end of XLE tokenized string in Sentence, starting at offset

    my ($self, $str, $offset, $sent) = @_;
    
    my @wds = split(/\s+/, $str);
    
    my $start;
    foreach $w (@wds) {
	if ($w eq "_not") {
	    $w = "n\\'t";
	} 
	my $rest = substr($sent, $offset);

	if ($w eq "will") {
	    while( $rest =~ /w(ill)|o/ig) {
		#we'll do case-insensitive, to handle sentence-initial case in English; this should work, since we're confident we'll find the right string, and we're really just worried about skipping whitespace; and little annoying thing about won't
		last; #this is stupid; there should be a better way to find where the string is
	    }
	} else {
	    while( $rest =~ /\Q$w\E/ig) {
		 #we'll do case-insensitive, to handle sentence-initial case in English; this should work, since we're confident we'll find the right string, and we're really just worried about skipping whitespace
		 last; #this is stupid; there should be a better way to find where the string is
	    }
	}
	my $pos = pos($rest);
	if (! defined($pos) ) {
	    print STDERR "Didn't find $w starting at $offset in $sent\n";
	} else {
	    $offset+=$pos;
	    if (! defined($start) ) {
		$start = $offset - length($w);
	    }
	}
    }
    return ($start,$offset);
    
}




##########################################################################################
#to use this file
1;
