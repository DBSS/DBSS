package XLE::FStruct;

=head1 NAME
XLE::FStruct the module for manipulating f-structures

=head1 SYNOPSIS
(see also XLE::XLEStruct)
$fs = FStruct->new(fs=>$fsInfo);
$fs_xml = $fs->toXML(SENTENCE=>1, HEADER=>1);

=head1 AUTHOR
Chris Culy (culy@fxpal.com)

=head1 DATE
15 October 2003
Revised: 30 January 2004

=head1 COPYRIGHT
Copyright 2004 FXPAL All Rights Reserved

=head1 METHODS

=cut

#variables:
#FSLIST  #all the f-structures 
#SENTENCE

use XLE::Utilities;

#my $EXCLUDE_ATTRS;
#my $INCLUDE_ATTRS;

sub new {

=head2 new
	new(fs=>val): avm corresponding to var(0)
	new(fs=>val, root=>rt, avm=>val, fslist=>arrayref): for other var(avm)

=cut

    my $classname = shift;
    my $self = {};
    bless($self, $classname);
    $self->_init(@_);
    return $self;
}

sub _init {
    my $self = shift;
    my %args = (@_);

    my $fs = $args{fs}; 
    my $sentence;
    my $avm = 0;
    if (defined($args{avm})) {
	$avm = $args{avm};
    } else {
	#need to decode fs; mixedJ indicates special case where XLE outputs sentence in UTF-8 but rest in euc-jp
	#in that case, we need to get the sentence before we decode fs
	if ($args{mixedJ}) {
	    $sentence = &find_sentence( info=>$fs, mixedJ=>1 );
	} 
	$args{fs} = &decode_fs_to_utf8( $fs );
	$self->{SENTENCE} = &find_sentence( info=>$fs ); #just in case anything needs the decoded


    }
    #print STDERR "avm is $avm\n";
    if (defined($self->{FSLIST}->[$avm])) {
	print STDERR "avm $avm is already defined!\n";
    }
    $self->{FSLIST} = $args{fslist};
    $self->{FSLIST}->[$avm] = -1; #a placeholder so we shouldn't get infinite loops when there is a circular FS 
    #$self->_fs2AVM(avm=>$avm, @_);
    $self->_fs2AVM(avm=>$avm, %args);
    $self->{FSLIST}->[$avm] = $self;
    
    if ($sentence) {
	$self->{SENTENCE} = $sentence;
    }
}

sub _fs2AVM {
    my $self = shift;
    #my ($fs, $avm) = @_;
    %args = (@_);

    my $fs = $args{fs};
    my $avm = $args{avm};
    my $root = $args{root};

    $self->{PHI} = $avm;

    #if ($avm == 0) {
    #	$fs =~ /^fstructure\('(.*)',$/m;
    #	my $tmp = $1;
    #	$tmp =~ s/\\(['"\\])/\1/g; #"' remove prolog escaping
    #	$self->{SENTENCE} = $1;
    #}
    
    #types of info
    #eq(attr(var(v),aName),value)
    #in_set(var(v1),var(v2))  #i.e. v1 is elt-of v2
    #scopes(var(v1),var(v2))
    #eq(proj(var(v1),proj),v2)

    #There is also issue of "floating" f-structures: in coordination the phi associated with a c-s node is "cloned", and the copies are given to the conjuncts, and the original phi is not part of the main f-s
    #Eventually, John Maxwell wants to include something like subsume(var(9), var(2)), where 9 is the original phi, and 2 the clone
    #We will use "subsumed_by" as a feature of the cloned f-s
    #We can (roughly) test whether a phi is a clone or not by seeing if its PRED's lexform is used somewhere else and if so, if that phi is in the c-structure

    #let's do attributes first -- they're easiest
    while ($fs =~ /eq\(attr\(var\($avm\),(.*?)\),(.*)\)\)/g) {
	my ($attr,$val) = ($1, $2);
	#3 types of vals: 
	#simple atom e.g +
	#semform(string,ID,[args], [non-args])
	#var(n)
	

	if ( $val =~ /semform\((.*?),(\d+),(\[.*?\]),(\[.*?\])\)/ ) {
	    my ($str, $id, $args, $nonargs) = ($1, $2, $3, $4);
	    $self->{$attr}->{VAL}->{SEMFORM}->{STRING} = $str;
	    $self->{$attr}->{VAL}->{SEMFORM}->{ID} = $id;
	    #@{$self->{$attr}->{VAL}->{SEMFORM}->{ARGS}} = eval($args);
	    #@{$self->{$attr}->{VAL}->{SEMFORM}->{NONARGS}} = eval($nonargs);
	    $args =~ s/[var()]//g;
	    $self->{$attr}->{VAL}->{SEMFORM}->{ARGS} = eval($args);
	    $nonargs =~ s/[var()]//g;
	    $self->{$attr}->{VAL}->{SEMFORM}->{NONARGS} = eval($nonargs);

	    #is our id used somewhere else?
	    my $tmpFS = $fs; #so we can have a new search posn
	    while($tmpFS =~ /var\((\d+)\).*semform\(\'.*\',$id,/g) {
		my $otherPhi = $1;
		next if ($otherPhi == $avm);

		#see if that phi is in c-structure	     
		if ($tmpFS =~ /phi.*var\($otherPhi\)/) {
		    $self->{"subsumed_by"}->{VAL}->{ATOM} = $otherPhi;
		    last;
		}
	    }

	} elsif ($val =~ /var\((\d+)\)/) {
	    my $link = $1;

	    my $tmpFS;
	    #if (! defined( $self->{ROOT}->{FSLIST}->[$link]) ) {
	    #	FStruct->new(fs=>$fs,root=>$self->{ROOT}, avm=>$link);
	    #}

	    if (! defined( $self->{FSLIST}->[$link]) ) {
	    	XLE::FStruct->new(fs=>$fs,root=>$root, avm=>$link, fslist=>$self->{FSLIST});
	    }
	    $self->{$attr}->{VAL}->{FS} = $link;

	} else {
	    $self->{$attr}->{VAL}->{ATOM} = $val;
	}
    } #end attributes

    #in_set: x is elt of this node
    #representation will be fs that is list of fs links that is hash (not attribute) value of _SET_
    while ($fs =~ /in_set\(var\((\d+)\),var\($avm\)\)/g) {
	my $elt = $1;
	if (! defined($self->{FSLIST}->[$elt]) ) {
	    XLE::FStruct->new(fs=>$fs,root=>$root, avm=>$elt, fslist=>$self->{FSLIST});
	}
	push(@{$self->{_SET_}}, $elt);
    }

    #in_set: x is elt of this node
    #representation will be fs that is list of fs links that is hash (not attribute) value of _SET_
    #but here x is atom, not link, so we'll make it a reference so we don't get confused??
    while ($fs =~ /in_set\(\'(.*)\',var\($avm\)\)/g) {
	my $elt = $1;
	push(@{$self->{_SET_}}, \$elt);
    }

    #scopes; only at the top level
    if ($avm == 0) {     
	while ( $fs =~ /scopes\(var\((\d+)\),var\((\d+)\)/g ) {
	    push(@{$self->{SCOPES}}, [$1, $2]);
	}
    }
    #still TBD: proj (fs -> OT) and cproj (cs -> OT)

}

{
    my %seen;
sub walkFS {

=head2 walkFS(f)
    pass a function ref f to be evaluated
    the function gets the current fstruct plus 
    the evaluation of the values by alphabetical order of the attribute they are a value of
    plus additional argument indicating if value is semform or atomic

=cut

    my $self = shift;

    my $f = shift;
    my $init = shift; #is this the first time we're calling walkFS?
    #want to initialize %seen if this is the first time
    #also should rewrite below to use only general case and already seen (phi=0 isn't needed or correct)
    

    #if ($self->{PHI} == 0) {
    #	%seen = (0=> 1);

    #} elsif ($seen{$self->{PHI}}) {
    #	return; #don't do same avm twice
    
    #} else {
    # 	$seen{$self->{PHI}} = 1;
    #}
    #print STDERR "seen: ", join(" ", (sort keys %seen)), "\n";

    if ($init) {
	%seen = {}
    }
    if ($seen{$self->{PHI}}) {
	return; #don't do same avm twice
    } else {
	$seen{$self->{PHI}} = 1;
    }

    my @vals = ();

    foreach $attr (sort keys %$self) {

	next if ($attr eq "FSLIST");
	
	my $tempVal;

	if ($attr eq "SCOPES") {
	    push(@vals, &$f($self, $self->{"SCOPES"}, "scopes"));

	} elsif ($attr eq "_SET_") {
	    foreach $elt (@{$self->{_SET_}}) {
		#push(@vals, $self->{FSLIST}->[$elt]->walkFS($f));
		if (ref($elt)) {
		    push(@vals, &$f($self, $$elt, "atom"));
		} else {

		    $tempVal = $self->{FSLIST}->[$elt]->walkFS($f);
		    if (defined($tempVal)) {
			push(@vals, $tempVal);
		    } else {
			push(@vals, &$f($self, $elt, "link"));
		    }
		}
	    }

	} elsif ( exists $self->{$attr}->{VAL} ) {
	    if (exists $self->{$attr}->{VAL}->{FS}) {
		my $dtr = $self->{$attr}->{VAL}->{FS};
		$tempVal = $self->{FSLIST}->[$dtr]->walkFS($f);
		if (defined($tempVal)) {
		    push(@vals, $tempVal);
		} else {
		    push(@vals, &$f($self, $dtr, "link"));
		}
		next;
	    }

	    my $semform = $self->{$attr}->{VAL}->{SEMFORM};
	    if ( defined($semform) ) {
		push(@vals, &$f($self, $semform, "semform" ));
		next;
	    }	
	    my $atom = $self->{$attr}->{VAL}->{ATOM};
	    if ( defined($atom) ) {
		push(@vals, &$f($self, $atom, "atom" ));
		next;
	    }	
	} else {
	    #print "Unknown  attribute: $attr\n";
	}
    }
    return &$f($self, [@vals]);
}
}

sub has_atomic_value {
#does this attribute have an atomic value in me [atomic is ATOM or SEMFORM]
#attribute must exist in me

    my $self = shift;
    my $attr = shift;
    
    return 0 if ($attr eq "_SET_");
    return 0 if (! exists($self->{$attr}->{VAL}) );
    if (exists($self->{$attr}->{VAL}->{SEMFORM}) || exists($self->{$attr}->{VAL}->{ATOM})) {
	return 1;
    } else {
	return 0;
    }

}

sub toString {

=head2 toString()
    return a string-graphical representation of the f-structure

=cut

    my $self = shift;
    return $self->walkFS(\&_toStr);
}

sub toXML {

=head2 toXML
	Parameter	Values	Meaning
	IS_ROOT		0,1	1: Serialize as complete XML document, with XML header, sentence and <fs></fs> around the XML for the f-structure. HEADER and SENTENCE override those parts of this specification. phi 0 is always treated as IS_ROOT = 1.
	HEADER		0,1	1: Include XML header (only if IS_ROOT is also true)
	SENTENCE	0,1	1: Include the original sentence (only if IS_ROOT is also true)

	INCLUDE_ATTRS	String of comma-delimited attributes to include 
	EXCLUDE_ATTRS	String of comma-delimited attributes to exclude

=cut

    my $self = shift;
    my %args;
    if ($self->{PHI} == 0) { #default for root is to include header and sentence
	%args = (
		 HEADER=>1,
		 SENTENCE=>1,	      
		 @_,
		 );
    } else { #default for non-root is to exclude header and sentence
	%args = (
		HEADER=>1,
		SENTENCE=>1,	      
		@_,
		);
    }
    #NB: could have EXCULDE_ATTRS or INCLUDE_ATTRS

    #set up include and exlude attributes
    if (defined($args{EXCLUDE_ATTRS})) {
	foreach $a (split(/,\s*/,$args{EXCLUDE_ATTRS})) {
	    $EXCLUDE_ATTRS->{$a}=1;
	}
    } else {
	undef($INCLUDE_ATTRS);
    }
    if (defined($args{INCLUDE_ATTRS})) {
	foreach $a (split(/,\s*/,$args{INCLUDE_ATTRS})) {
	    $INCLUDE_ATTRS->{$a}=1;
	}
    } else {
	if (defined($args{EXCLUDE_ATTRS})) {
	    undef($INCLUDE_ATTRS);
	} else {
	    $INCLUDE_ATTRS = "DEFAULT";
	}
    }

    my $what = $self->walkFS( &_toXML($INCLUDE_ATTRS, $EXCLUDE_ATTRS), "init"); #added init argument

    my $isRoot = $self->{PHI} == 0 || $args{IS_ROOT};
    my $preamble;
    if ($isRoot) { 
	#add xml header and root tag
	my $header = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";

	if ($args{HEADER}) {
	    $preamble = "$header"
	}
	$preamble .= "<fs>\n";
	if ($args{SENTENCE}) {
	    $preamble .= "<sentence>" . &escape($self->{SENTENCE}) . "</sentence>\n\n";
	}
    }

    $what = "$preamble$what\n";

    if ($isRoot) {
	$what .= "</fs>\n";
    }


    return $what;
}


sub _toStr {
    my ($self, $dtrResults, $type) = @_;
    
    my $what = "";

    if ($type eq "atom") {
	$dtrResults =~ s/^'(.*)'$/\1/;
	$what .= $dtrResults;
    } elsif ($type eq "semform") {
	$what .= $dtrResults->{STRING};
	if ( @{$dtrResults->{ARGS}} || @{$dtrResults->{NONARGS}} ) {
	    $what .= "<";
	    if ( @{$dtrResults->{ARGS}} ) {
		$what .= join(",", @{$dtrResults->{ARGS}});
	    }
	    $what .=  ">";
	    if (@{$dtrResults->{NONARGS}}) {
		$what .= "(" . join(",", @{$dtrResults->{NONARGS}}) . ")";
	    }
	}

    } elsif ($type eq "link") {
	$what .= "[$dtrResults]\n";

    } elsif ($type eq "scopes") {

	foreach $sc (@$dtrResults) {
	    $what .= "$sc->[0] < $sc->[1]\t";
	}
	$what .= "\n";

    } else {

	if (exists($self->{SCOPES})) {
	    #put scopes at top
	    my $which;
	    foreach $attr (sort keys %$self) {
		next if ($attr eq "PHI" || $attr eq "FSLIST");
		last if ($attr eq "SCOPES");
		$which++;
	    }
	    $what .= "SCOPES\t$dtrResults->[$which]";
	}
	if ($self->{PHI} > 0) {
	    $what .= "--" . $self->{PHI} . "--\n";
	}
	my $i;
	my $prefix;
	foreach $attr (sort keys %$self) {
	    next if ($attr eq "PHI" || $attr eq "FSLIST" || $attr eq "SCOPES" || $attr eq "SENTENCE");
	    #if ($self->{PHI} > 0 && $i==0) {
	    #	$prefix = "[" . $self->{PHI} . "]  ";
	    #	$what .= $prefix;
	    #} 
	    if ($attr eq "_SET_") {
		my $eltDelim = "-----------------------\n";
		$what .= "$eltDelim";
		#foreach $elt (@$dtrResults) {
		while ($i < @$dtrResults) {
		    my $elt = $dtrResults->[$i++];
		    $what .= $elt;
		    $what .= $eltDelim;
		}
	    } elsif ($attr eq "'CHECK'") {
		#ignore
		$i++;

	    } else {
		$attr =~ s/^'(.*)'$/\1/;
		my $val = $dtrResults->[$i++];
		if ($val =~ /\n./) {
		    $what .= "$attr";
		    $val =~ s/^(.)/\t\1/mg;
		    $what .= "$val";
		} elsif ($val =~ /\n$/) {
		    $what .= "$attr\t$val";
		} else {
		    $what .= "$attr\t$val\n";
		}
	    }
	}
    }
    
    return $what;
}

sub _toXML {
#CC: this solution was suggested by David AHN

    my ($INCLUDE_ATTRS, $EXCLUDE_ATTRS) = @_;

    return sub {
	my ($self, $dtrResults, $type) = @_;
	
	my $what = "";

	if ($type eq "atom") {
	    $dtrResults =~ s/^'(.*)'$/\1/;
	    $what .= &escape($dtrResults);
	} elsif ($type eq "semform") {
	    $what .= "<semform>\n";
	    my $strTmp;
	    ($strTmp = $dtrResults->{STRING}) =~ s/^\'(.*)\'$/\1/; 
	    $strTmp = &escape($strTmp);

	    $what .= "\t<string>" . $strTmp . "</string>\n";
	    $what .= "\t<id>" . $dtrResults->{ID} . "</id>\n";
	    if ( @{$dtrResults->{ARGS}} || @{$dtrResults->{NONARGS}} ) {
		#$what .= "<";
		if ( @{$dtrResults->{ARGS}} ) {
		    #$what .= "\t<args>\n";
		    my $aNum = 1;
		    foreach $a (@{$dtrResults->{ARGS}}) {
			$what .= "\t<arg num = \"" . $aNum++ . "\">$a</arg>\n";
		    }
		    #$what .= "\t</args>\n";
		}

		if (@{$dtrResults->{NONARGS}}) {
		    #$what .= "\t<nonargs>\n";
		    my $nNum = 1;
		    foreach $n (@{$dtrResults->{NONARGS}}) {
			$what .= "\t<nonarg num = \"" . $nNum++ . "\">$n</nonarg>\n";
		    }
		    #$what .= "\t</nonargs>\n";
		}
	    }
	    $what .= "</semform>\n";

	} elsif ($type eq "link") {
	    $what .= "<link>$dtrResults</link>\n";

	} elsif ($type eq "scopes") {
	    #$what .= "<scopes>\n";
	    foreach $sc (@$dtrResults) {
		$what .= "\t<scope wide = \"" . $sc->[0] . "\" narrow = \"" . $sc->[1] . "\"/>\n";
	    }
	    #$what .= "</scopes>\n";

	} else {
	    #attributes and their values

	    if (exists($self->{SCOPES})) {
		if (&show_attr($INCLUDE_ATTRS, $EXCLUDE_ATTRS, "SCOPES")) {
		    #put scopes at top
		    my $which;
		    foreach $attr (sort keys %$self) {
			next if ($attr eq "PHI" || $attr eq "FSLIST");
			last if ($attr eq "SCOPES");
			$which++;
		    }
		    $what .= "<SCOPES>\n" . $dtrResults->[$which] . "</SCOPES>\n";
		}
	    }
	    
	    #if ($self->{PHI} > 0 && &show_attr($INCLUDE_ATTRS, $EXCLUDE_ATTRS, "PHI")) {
	    if (&show_attr($INCLUDE_ATTRS, $EXCLUDE_ATTRS, "PHI")) {
		$what .= "<phi>" . $self->{PHI} . "</phi>\n";
	    }
	    my $i;
	    my $prefix;
	    foreach $attr (sort keys %$self) {
		next if ($attr eq "PHI" || $attr eq "FSLIST" || $attr eq "SCOPES" || $attr eq "SENTENCE");

		if ($attr eq "_SET_") {
		    #$what .= "<set>\n";
		    #foreach $elt (@$dtrResults) {
		    while ($i < @$dtrResults) {
			my $elt = $dtrResults->[$i++];
			next if ($elt =~ /^\s*<scope/); #scopes were getting repeated
			$what .= "<element>$elt</element>\n";
			#$what .= $eltDelim;
		    }
		    #$what .= "</set>\n";

		} else {
		    $attr =~ s/^'(.*)'$/\1/;
		    my $val = $dtrResults->[$i++];
		    next if (! &show_attr($INCLUDE_ATTRS, $EXCLUDE_ATTRS, $attr) );

		    if ($val =~ /\n./) {
			$what .= "<$attr>";
			$val =~ s/^(.)/\t\1/mg;
			$what .= "$val";
			$what .= "</$attr>\n"
			} elsif ($val =~ /\n$/) {
			    $what .= "<$attr>\t$val</$attr>\n";
			} else {
			    $what .= "<$attr>$val</$attr>\n";
			}
		}
	    }
	}
	
	return $what;
    } #end of sub

}

sub show_attr {
    my ($INCLUDE_ATTRS, $EXCLUDE_ATTRS, $attr) = @_;
    #show if default or specified
    #don't show if explicitly exluded or not explicitly included when others are
    if ($INCLUDE_ATTRS eq "DEFAULT" || defined($INCLUDE_ATTRS->{$attr})) {
	return 1;
    } elsif (keys(%$INCLUDE_ATTRS) || defined($EXCLUDE_ATTRS->{$attr})) {
	return 0;
    } else {
	return 1;
    }
}


sub get_fs_by_phi {

=head2 get_fs_by_phi($phi)
    given a phi, return the  FStruct object for it

=cut

    my $self = shift;
    my $phi = shift;

    return $self->{FSLIST}->[$phi];

}
###########################################################
1; #to use this file
