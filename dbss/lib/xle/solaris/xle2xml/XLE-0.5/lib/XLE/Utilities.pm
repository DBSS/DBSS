package XLE::Utilities;

=head1 NAME
XLE::Utilities simple non-object-oriented module of utility functions

=head1 SYNOPSIS
$utf8FS = decode_fs_to_utf8($fsInfo)
$sent = find_sentence(info=>$fsInfo)
$safeStr = escape($str)

=head1 AUTHOR
Chris Culy (culy@fxpal.com)

=head1 DATE
14 November 2003
Revised: 30 January 2004

=head1 COPYRIGHT
Copyright 2003-2004 FXPAL All Rights Reserved

=head1 FUNCTIONS
 

=cut

use Exporter;
@ISA = ('Exporter');
@EXPORT = qw(&decode_fs_to_utf8 &find_sentence &escape);

use Encode;
use Encode::Guess;

sub decode_fs_to_utf8 {

=head2 decode_fs_to_utf8($fsInfo)
    take fsInfo input and return utf-8 version
    if fsInfo doesn't specify encoding, then use a default
    if that doesn't work, just return fsInfo

=cut

    my $fs = shift;
    my $default_enc = "euc-jp"; #e.g. older Japanese files

    my $what;

    my $coding = _find_encoding($fs);

    if ($coding) {
	$what = decode($coding, $fs);
    } else {
	my $enc = guess_encoding($fs, $default_enc);
	ref($enc) or return $fs; #"Can't guess: $enc"; # trap error this way
	$what = $enc->decode($fs);
    }
   
    return $what;
}

sub _find_encoding {
    #return the encoding given in the .pl file, empty if there is none
    my $fs = shift;
    my $what = ""; #the default if no encoding is specified

    if ($fs =~ /^% -\*- coding: (.*?) -\*-$/m) {
	$what = $1;
    }
    return $what;
}

sub find_sentence {

=head2 find_sentence(info=>$fsInfo);
    find the sentence in the .pl info; 
    special case of Japanese, where sentence is in UTF-8 when rest is in euc-jp
    so in that case this needs to be called before the info has been decoded

=cut

    my %args= (@_);
    my $info = $args{info};
  
    $info =~ /^fstructure\('(.*)',$/m;
    my $sent = $1;
    $sent =~ s/\\(['"\\])/\1/g; #"' remove prolog escaping

		      return $sent;
}

sub escape {

=head2 escape($s)
    temporary(?) function to escape characters for XML

=cut

    my $s = shift;

    $s =~ s/\&/&amp;/g;
    $s =~ s/\</&lt;/g;
    $s =~ s/\>/&gt;/g;

    $s =~ s/([^\x20-\x7F])/'&#' . ord($1) . ';'/gse; #from Perl-XML FAQ: encode characters > 127 (me: ord < 256)
    return $s;
}


####################################################################################
1;
