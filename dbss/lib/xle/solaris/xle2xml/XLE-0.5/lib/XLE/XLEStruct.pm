package XLE::XLEStruct;

=head1 NAME
XLE::XLEStruct the main module for xle2xml, a way to manipulate the output of the XLE as well as convert it to XML.

=head1 SYNOPSIS

$xle = new XLEStruct(info=>$fsInfo); #the contents of the .pl XLE output

$combo_xml = $xle->toXML(NODENUMS=>1, PROPERTIES=>1, SUBLEXICAL=>1); #include everything

my $limited_fs_xml = $xle->{fs}->toXML(INCLUDE_ATTRS=>"PRED,SUBJ,COMP,XCOMP,OBJ,ADJUNCT"); #f-structure only, showing just these attributes

my $easier_xml = $xle->toXML(NODENUMS=>1, PROPERTIES=>1, SUBLEXICAL=>0, EXCLUDE_ATTRS=>"CHECK"); #c-structure and f-structure; don't include sublexical nodes in the c-structure or the CHECK feature in f-structure


=head1 DESCRIPTION
The goal of these Perl modules is to provide a way to take the Prolog
output of XLE and express it in XML. One possible use of these XML
files is for inter-application communication.

=head1 AUTHOR
Chris Culy (culy@fxpal.com)

=head1 DATE 
15 October 2003
Revised: 30 January 2004

=head1 COPYRIGHT
Copyright 2003-2004 FXPAL All Rights Reserved

=head1 METHODS

=cut

use XLE::CStruct;
use XLE::FStruct;
use XLE::Utilities;

our $VERSION = "0.5";

sub new {

=head2    new(info=>val) 
    info is from .pl file (i.e. a string, not the file name, for now) 

=cut

    my $classname = shift;
    my $self = {};
    bless($self, $classname);
    $self->_init(@_);
    return $self;

}

sub _init {
    my $self = shift;
    my %args = (@_);

    my $info = $args{info};

    $self->{"cs"} = XLE::CStruct->new(fs=>$info, mixedJ=>$args{mixedJ});
    $self->{"fs"} = XLE::FStruct->new(fs=>$info, mixedJ=>$args{mixedJ});

    $info = &decode_fs_to_utf8($info); #CStruct and FStruct each convert to utf8, so we'll do it for ourselves; seems silly to do it 3 times, though, but since CStruct and FStruct can be used independently, we seem stuck for now
        
    #find properties, except rootcategory, which is in cs
    foreach $p (xle_version, grammar, grammar_date, statistics) {
	$info =~ /\'$p\'\(\'(.*?)\'\)/m;
	$self->{$p} = $1;
    }
    
}

sub toString {

}

sub toXML {

=head2 toXML
optional parameters:
    	Parameter	Values	Meaning
	CS		0,1	1: Include the XML representation of the c-structure
	FS		0,1	1: Include the XML representation of the f-structure
	PROPERTIES	0,1	1: Include the properties of the parser run (e.g. name, time, etc)

passed to CStruct	
	Parameter	Values	Meaning
	NODENUMS	0,1	1: Include the nodenumbers in the c-structure
	SUBLEXICAL	0,1	1: Include sublexical nodes in the c-structure
	(symbol) 	replacement	replace the symbol by its textual value in node names

Non-alphanumeric symbols are not valid XML names, so they need to be replaced in some of the node-names. The currently defined replacements are:

 			"*TOP*" => "STAR_TOP_STAR", 
		       "+" => "PLUS.", 
		       "`" => "BACKQUOTE",
		       "\\'" => "QUOTE",
		       "\"" => "DOUBLEQUOTE",
		       "," => "COMMA",
		       "?" => "QUESTION_MARK",
		       "!" => "EXCLAMATION_POINT",
		       ":" => "COLON",
		       ";" => "SEMICOLON",
		       "(" => "LEFT_PAREN",
		       ")" => "RIGHT_PAREN",
		       "{" => "LEFT_BRACE",
		       "}" => "RIGHT_BRACE",
		       "[" => "LEFT_BRACKET",
		       "]" => "RIGHT_BRACKET",
		       "/" => "FORWARD_SLASH",
		       "\$" => "DOLLAR_SIGN",
		       "*" => "ASTERISK", 
		       "%" => "PERCENT",
		       "&" => "AMPERSAND",

	It is possible to add new symbol/replacement pairs, as well as to override existing ones. For example, to use "CURLY_BRACKET" instead of "BRACE":

	myXMLOBJ->toXML("{"=>"OPEN_CURLY_BRACKET", "}"=>"CLOSE_CURLY_BRACKET");

If you have suggestions for additions/modifications, please let me know.

=cut


    my $self = shift;

    my %args = (
		CS=>1,  
		FS=>1,
		PROPERTIES=>1,
		@_,
		);

    my $header = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
    my $what = "$header<xle>\n";
    $what .= "<sentence>" . &escape($self->{"cs"}->{SENTENCE}) . "</sentence>\n";

    if ($args{PROPERTIES}) {
	foreach $p (xle_version, grammar, grammar_date, statistics) {
	    $what .= "<$p>" . $self->{$p} . "</$p>\n";
	}
    }
    $what .= "\n";

    if ($args{CS}) {
	$what .= $self->{"cs"}->toXML(HEADER=>0,SENTENCE=>0,(%args)) . "\n\n";
    }
    if ($args{FS}) {
	$what .= $self->{"fs"}->toXML(HEADER=>0, SENTENCE=>0, (%args)) . "\n\n";
    }
    
    $what .= "</xle>\n";

    return $what;
}

sub escape {
    #temporary function to escape characters

    my $s = shift;

    $s =~ s/\&/&amp;/g;
    $s =~ s/\</&lt;/g;
    $s =~ s/\>/&gt;/g;

    $s =~ s/([^\x20-\x7F])/'&#' . ord($1) . ';'/gse; #from Perl-XML FAQ: encode characters > 127 (me: ord < 256)
    return $s;
}

1;
