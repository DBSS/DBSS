<HTML>
<TITLE>
Porting LFG Grammars between Medley and XLE: Lexical Aspects
</TITLE>
<h2>  
Porting LFG Grammars between Medley and XLE: Lexical Aspects
</h2>
<h3>
1. Aspects of Medley/XLE Grammar Porting
</h3>
Porting LFG grammars between the Medley LFG Workbench (Medley LFG, below) 
and XLE involves attention to two aspects: 
<DL>
<DT>Lexical Aspects
<DD>Preparing the lexicon for use in both systems taking into account current differences in morphological processing
<DT>File Structure
<DD>Modifying the grammar file structure, encoding, and configuration information with the help of tools designed for that purpose. 
</DL>
Current differences in morphological processing can be summarized as follows: 
<PRE>
               Medley                   XLE
Morph Tables     x                       -   
Finite_state     -                       x
Morph Codes
   symbolic      x                       x (different interpretation)
     *           x                       x
  @(..)          x                       - (subentries ignored) 
</PRE>
These differences are discussed in more detail in subsequent sections,
followed by sections describing how the differences are most easily accounted
for in the lexicon and grammar.
<P>
Following this is a brief discussion of the differences in file structure
and how files can be moved easily between the two systems.
<P>
Please note that the overviews of processing in Medley/LFG supplied
for purposes of comparison are very superficial, and should be
supplemented by reference to the extensive LFG Workbench documentation.

<h3> 
2. Overview: Morphological processing in Medley LFG 
</h3>
<P>In Medley-LFG, the interpretations of a lexical item (a token
of the input string) are developed by combining the results of
searching morph-tables, and lexicon entries with @(,.) and * morph codes.
In more detail, the process can be described as consisting of the following
steps:
<h4>Morph-table processing</h4>
Determine if there are matching morph-table based analyses, that is,
morph-table entries of the form:
<PRE>
         (takeoff-suffix  puton-suffix   cat  (morph-codes) affix)
</PRE>
e.g.,
<PRE>
         (    s               ""          N    (S)     -Npl)  
</PRE>
and matching lexical entries of the form
<PRE>
            headword  cat morph-code   constraints .
e.g.
            desk       N   S               ....
</PRE>
and
<PRE>
             affix    AFF  *    constraints
e.g.          -Npl    AFF  *    ....
</PRE>
If so, each such match is used as one interpretation of the token (here desks),
assumed to be in cat "cat", with constraints consisting of the unification
of those in the headword entry and those in the affix entry.
<LI>@(..) morphcode entry processing.
Determine if the entire token matches the headword of entry of the form
<PRED>
    headword   cat @(stem-headword affix*) constraints.
</PRE>
and if there is an entry for "stem_headword" in cat with either
the morph-code ROOT, or a single other symbolic morph-code, plus 
entries for each of the listed affixes in the above format.
<P>If so, each such match is also used as an interpretation of the token,
with constraints consisting of the constraints of the original headword,
unified with those of the stem-headword and those of the listed affixes.
<h4> "*" morphcode entry processing</h4>.
Determine if the entire word matches an entry of the form
<PRE>
             word  cat *  constraints
</PRE>
If so, the match is also used as an interpretation of the token.
<h4>Parse Chart</h4>
The initial parse chart consists (logically) of a graph with
ordered nodes representing the inter-token boundaries.  
For example, the initial parse chart for the sentence:
<PRE>
     "the desks are done"
</PRE>
might be depicted as:
<PRE>
    DET(the)   N(desk)       V(be)      V(do)
   1------> 2 ----------> 3 --------> 4 ------->5
                                         A(done)
                                      4 ------->5
</PRE>
with each edge carrying the unified constraints for the entire word.
Alternative interpretations of a word found are placed on 
different edges of the chart linking the same two nodes.
No edge starting at a node n may connect to any node except node n+1.
<h3> 
3. Overview: Morphological processing in XLE
</h3>
<P>
In XLE, in contrast, morphological analysis uses one or more
external finite-state-transducers to deliver analyses of the input.
The process can be described in terms of the following steps.
<h4>Tokenization</h4>
Find the tokens of the string using a specified or default
tokenizing transducer. The result of this process is a skeleton graph
(prior to the actual parse chart) such as:
<PRE>
     the        desks         are        taller
   1------> 2 ----------> 3 --------> 4 ------->5
</PRE>
Because the process can result in 
alternative tokenizations of the string (e.g, can detect compound  
words), two arcs starting at the same node can end at different
target nodes.
<h4>Morphological Analysis</h4>
If a morphological analysis transducer is specified, submit
each arc label to the analyzer. This will (generally) return
one or more sequences of the form:
<PRE>
       stem -tag1...-tagn
</PRE>
<P>
The returned analyses are placed in the interpretation graph
by adding nodes and arcs between the initial and final nodes
for the token so that each stem and tag is represented by a separate
arc, 
reflecting alternative analyses of the submitted surface token. 
The initial surface forms are also retained at this stage.
For example:
<PRE> 
         the                   desks                 are
   1----------------> 3 -------------------> 5 ------------------->
     the      -Det      desk         -Npl        be        -Vpl
   1------> 2 ------> 3 --------> 4 -------> 5 ------> 6 --------->

        taller
   7 ---------------> 9
      tall     -AdjC
   7 -----> 8 ----->  9
</PRE> 
<P>
Sometimes, in the case, e.g., of contractions and liaison, 
sequences of the form:
<PRE>
       stem1  -tag11...-tag1n | stem1  -tag21....-tag21
</PRE>
can also be returned, e.g.,
<PRE>
      can't   -->  can -tag.. | not   -tag.. 
</PRE>
<h4>Lexicon Lookup</h4>
For each arc of the graph, look up the label in the lexicon.
"Surface arcs" (i.e., which represent a full surface
token) can be matched by lexicon entries with "*" morph codes,
e.g.,
<PRE>
        are    V   *  constraints
</PRE>
Logically,  if such matches are found, the full category (here V)
and constraints are attached to the arcs.  
<P>
Non-surface-arcs can be matched by lexicon entries
with symbolic morph codes, e.g.,
<PRE>
       desk   N       S   constraints  
       -Npl   NSFX  DUMMY constraints 
</PRE>
roughly, but not exactly, paralleling the use of such morph
codes in Medley in conjunction with morph tables. A major difference is
that tags need not, and, in general, should not, bear the category "AFF".
Such matches result in slightly modified categories "cat_BASE"
and associated  constraints being attached to the edges.  The 
modified category names can be thought of a reflecting incomplete
categories.
<P>To simplify use of the a single lexicon with both systems,
a stem, e.g. "desk", will match both identical lexicon headwords,
and lexicon headwords with suffixed hyphens, e.g. "desk".
<P>
If a match is not found for a non-surface-arc,  an "unknown" stem
is checked.  For details on unknowns, see "additional considerations"
below.  
<P>
The resulting arcs are transferred to the edges of the initial chart.
For example, in the sample sentence, if the lexicon contained 
a * morph-code entry for "are" and symbolic morph-code entries
for "the", "desk", "do" and "done" plus all the affixes given,
the initial chart would have the form:
<PRE> 
     DET_BASE(the)   DSFX_BASE(-Det)  N_BASE(desk)   NSFX_BASE(-Npl)  V (are) 
   1-------------> 2--------------> 3 ------------>4 -------------> 5 ------>

      A_BASE(tall)  ASFX_BASE (-AdjC)
     6 ---------> 7 ---------------> 8
</PRE>
user-supplied rules in the grammar of the form:
<PRE>
      cat --> cat_BASE: ^=!  tagcat1_BASE ^=! ....
</PRE>
Arcs of the graph that have not been matched by lexical entries
are transferred to the chart but not given categories.  The
result is that these arcs are reflected in the lowest line of
XLE chart displays, but are not used in any syntactic rules.
<h3>
4. Building Portable Lexicons
</h3>
The construction of portable lexicons at this stage, except for
very small lexicons, consists of structuring lexicon entries
so that the combined use, in medley, of  morph-tables and lexicon
entries carrying @(...) morph codes  has the same result
as the use of an exterrnal morphology in XLE.
The following suggests a general approach; some special cases are
discussed further on.
<P>
The general idea is to start from an analysis of the tags to be
returned by the external lookup, specify lexical entries for those tags,
and also reflect the same partitioning of information in the morph tables
and @(.,.) morph code entries. In more detail:
<OL>
<LI>For each inflected form, construct an entry for the stem of the
form 
<LI> For each inflected form, construct an entry for the stem such as
<PRE >
            ....
                   sym-morph-code-2  constraints
for example:

     tall    A     morphcode   constraints.
</PRE>
<LI>for each tag  "-tag" expected from the external morphology,
construct a lexical entry of the form
<PRE>
     -tag   tagcat  dummy-morph-code  @template;

e.g.,
     -Adj      ASFX ANY .
     -AdjC     ASFX ANY  @(COMPARATIVE).
     -AdjS     ASFX ANY  @(SUPERLATIVE).

</PRE>
Where the template contains the constraints associated with the tag,
such that the appropriate sublexical rules can be written.
<P>
Tags returned by XLT analyzers are described in the documentation
for the associated language.  Tags documented as [XXX} are
actually returned as -XXX.  To determine the set of tags that will
be returned for a particular word, see notes further on. 
<LI>If morph tables are to be used in Medley-LFG for regular
paradigms, build morph table entries of the form
<PRE>
     (takeoff  puton  cat (morph-codes)  lfgtag)
</PRE>
Since only a single tag can be used in a morph-table entry,
but, in general, an external analyzer will return multiple tags,
the parallels can be maintained by constructing entries for the
lfgtag's like:
<PRE>
    -lfgtag  AFF   *  @template1
                      @template2
                      ...
</PRE>
where each @templatei reference matches that specified for
one of the tags associated with the inflection by the external
analyzer.
<LI>for inflectional morphology accounted for by the morph tables
(either irregular inflections or all inflection if morph-tables are
not used) construct lexicon entries of one of the following forms:
<PRE>
     headword  cat1  @(stem1  lfgtag)
               ... 
</PRE>
where "stem1" matches a headword entry, or
<PRE>
     headword  cat1  @(stem1  lfgtag1 ... lfgtagn)
</PRE>
to more precisely mirror the external analysis output.  In the
latter case,  each of the referenced tags wouldi have a lexicon entry
<PRE>
     lfgtagi   AFF   *  @templatei
</PRE>
</OL>

<h3>
5. Portable Lexicons: Additional Considerations
</h3>
This section discusses some miscellaneous considerations: Special
lexicon entry cases, handling of unknown forms, handling of "accidental
capitalization", and determining the tags returned by an external
morphology for a particular word. 
<H4>
Special Cases
</H4>
<OL>
<LI>Entries for uninflected words.  Many words are uninflectable. 
In English, these include non-gradeable adjectives, adverbs, prepositions,
conjunctions. In preparing a portable lexicon for use in XLE with
and external lookup, the user may choose
between
<UL>
<LI> representing these as stems, having  symbolic morph-codes,
together with rules absorbing the lookup supplied tags, e.g.,
"A --> A_BASE A_SFX", or
<LI> representing these as surface forms, with "*" morph-codes,
in which case the external lookup results will be ignored.
</UL>
<LI>Overriding external lookup results. A similar strategy should
be used for inflected forms where the stem+tags lookup results
do not deliver the required information in a convenient form.
For example, in English, various forms of BE might be given special
handling which cannot be conveniently expressed by simply combining
constraints associated with standard verb tags.  Instead, one can
create surface form (* morph-code) entries for each inflected
form, and omit a stem form. These entries may reference a common
template to minimize redundant code.
<LI>Category conflicts.  The same strategy should be used
where words are to be associated with categories other than those
given by the lookup.  For example, the English XLT morphological
analyzer does not use the category "PART" (particle).  
</OL>
<H4> Handling of Unknown forms </H4>
<P>
Two cases are of interest: surface forms unknown to the external 
morphology, and result network forms unknown to the lexicon lookup.
<P>
When a surface form is unknown to the external morphology,
the resultant analysis delivers the surface form augmented by
one of the tags "-MUnknown", if the form is capitalized, or
"-Munknown" if not.  
<P>When a non-surface-arc label is not found in the lexicon,
the label is replaced by "-LUnknown" if the label is capitalized,
and "-Lunknown" if not.
<P>
These provisions can be exploited in the lexicon and grammar rules to allow
unknowns to "go through". 
Entries can be added to the lexicon of the form:
<PRE>
    -Lunknown   LUNK     DUMMY .
    -LUnknown   LCAPUNK DUMMY.
    -Munknown   MUNK   DUMMY.
    -MUnknown   MCAPUNK DUMMY.
</PRE>
and then grammar rules written to get approximate analyses for common
cases.  For example, for words known to the morphology but not
the lexicon, approximate parses can be obtained by exploiting the
tags returned by the external morphology, e.g.
<PRE>
    N    --> LUNK_BASE     noun_tag_categories
    A    --> LUNK_BASE     adjective_tag_categories
</PRE>
<P> A special case of the latter is numbers; the morphological analysis
will generally deliver tags identifying the token as a number, which can
be used in conjunction with grammar rules such as:
<PRE>
    NUMBER --> LUNK_BASE  number-tag-categories
<PRE/>
(Note: the tokenizer does not currently accept numbers; this will be
remedied in the near future.)
:p.Proper names unknown to both the morphology and lexicon
will be reflected in edge sequencs of the form:
<PRE>
    -LUknown -MUnKnown
</PRE>
and  a rule such as
<PRE>
    Name --> LCAPUNK_BASE  MCAPUNK_BASE
</PRE?
can be used to account for the name.
<H4> Handling of connection tokens</H4>
<P>As mentioned above, contractions are analyzed by the external
morphology  by returning the expanded forms, separated by a connection
token, e.g. "can -Vsg  | not -Neg". (Note: the connection tokens
used are specific to the morphological analyzer for a particular
language.)
If only parsing were of interest, this could be handled by lexicon entries
and rules serving to just ignore the connector.  For example:
<PRE>
 |   CONTRACT  DUMMY.
 V --> V CONTRACT_BASE.
</PRE>
<P>
But more complex rules are needed if the grammar is to be used
for generation as well.

<H4>Handling of Accidental Capitalization</H4>
Initially capitalized words occurring where the capitalization may
be the result of sentence position (e.g. sentence-initial)
are looked up in the lexicon in
both capitalized and uncapitalized forms, and any matches found are used.

<H4> Determining Tags Returned By External Morphology </H4>
The results of the morphological analysis can always be found
in the lowest line of the chart resulting from a parse.
<P>
Also, command-line access to a version of the XLE morphological procesor
is provided in PARC and Grenoble to allow the full graph returned
for a sentence (or arbitrary list of words)  to be displayed.
The code is stored in the .../c-fsm/bin directory, and is accessed via
<PRE>
   xlemorph  - - config-file paths|blank
</PRE>
followed by input of the list of words to be investigated, and then
a "carriage return", "control-d".
<P>
For the "config file" see "grammar file structures", below".
<P>
Omitting "paths" gives the default "arcs" return; a list of
the arcs of the returned network.  With paths, an alternative formulation
listing interpretations of the input string with embedded alternatives
is returned. 
<h3>
6. File Structure Considerations
</h3>
<h4>
Grammar File Structures in Medley/LFG and XLE
</h4>
<P>
Using an LFG grammar in Medley/LFG consists of
<OL>
<LI>loading one or more LISP files, each containing sections of the grammar
<LI>selecting/editing one of the included "configuration" sections
identifying which grammar sections are to be used, plus other parameters.
The grammar sections specified may include a morph-table section,
one or more grammar-rule
sections, one or more lexentry sections, and one or more template
sections. Each type is specified in a separate configuration section
item.  Listing more than one section in an item specifies a (reverse)
precedence order for use of their content.  That is, if, for example,
a "lexentries" item lists lexicon sections a, b, and c,  and entries
for a word occur in all three sections, only those in section c will
be used.
</OL>
In contrast, using an LFG grammar under XLE consists of
specifying an initial ASCII grammar file using the
"create-parser" command under XLE.  That file should contain
a configuration section similar to that for LFG but elements
specifying sections to be used are ignored.  Instead, all the sections
in the file are used, and there should be only one of each type.
And some additional items may be included specifying the external 
finite-state transducers to be used for morphological analysis, and additional
files containing only lexical entries, for example
<PRE>
  MORPHOLOGYCONFIGFILE  /...path.../fsm-bf .
  LEXICONFILES  /...path.../lexicon_file1 /...path../lexicon_file2.
</PRE>
<P>
The "MORPHOLOGYCONFIGFILE" item specifies a file containing
a list of transducers to be used, and is needed unless only the
default tokenizer is required; the construction and use
of this file is described in a separate section. 
<P>
The LEXICONFILES item specifies one or more files each containing
only a lexical entry section, again in (reverse) precedence order,
with any lexicon section (if any) in the basic grammar file having the highest
precedence.
<h4>
File porting tools
</h4>
Two tools, operating under Medley, are provided to perform much of
the work involved in converting between the two formats.
"write-xle-grammar" selects sections of a loaded Medley/LFG grammar
and writes them into an ASCII file suitable for loading into XLE.
"read-xle-grammar" reads an XLE grammar file into Medley, where it can
then be stored using standard Medley tools. The tools are invoked
on the LISP command line, as follows:
<UL>
<LI> (WRITE-XLE-GRAMMAR content 'filename)
<P> where content is of the form:
<PRE>
     '((CONFIGS  (section identifier, e.g., MY ENGLISH))
       (TEMPLATES (section identifier))
       (RULES    (section identifier))
       (LEXENTRIES  (section identifier)))
</PRE>
<LI> (READ-XLE-GRAMMAR  'filename)
</UL>
Note that lexicon-only files can be written using only the "LEXENTRIES"
specification of WRITE-XLE-GRAMMAR.
<P>
After use of READ-XLE-GRAMMAR, either the file-manager or the
MASTERSCOPE DC function can be used to specify the content of
the LFG file or files used to store the result.
<h4>       
Morphology Configuration Files
</h4>
If only default tokenization is required, with all lexical
entries using * morph codes, no morphology configuration file need
be specified.  However, if specialized tokenization and/or 
morphological analysis is required, a morphology configuration
file must be specified within the grammar file.
<P>
The specified file indicates a sequence of transductions. Each
line indicates a single transduction, in terms of the name of a file  
containing a finite state transducer, and the symbolic name of a
method for applying the transducer. 
<P>
The first line such line must specify a tokenizer; 
a relatively simple one sufficient for English 
is found in the xle test directory and can be specified as
<PRE>
(fill in appropriate path)/test/NEWTOKEN.FSMFILE   TOK_BY_FSM             
</PRE>
If morphological analysis as well as tokenization is desired,
an XSOFT XLT language-specific analyzer should be specified on 
the following line, as
<PRE>
name-of-file-containing-finite-state-transducer   MORPH_BY_IX_DF      
</PRE>
<h4>
Additional Assistance
</h4>
For additional assistance, or if problems arise, please
contact xlecore@parc.com.













