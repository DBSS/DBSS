<html>

<head>
  <link href="http://www.powerset.com/stylesheets/psdocs.css" rel="stylesheet" type="text/css" title="ps-semantics docs" />
  <title>XLE prolog output formats</title>
</head>

<body>

<h1>Prolog output formats</h1>
<b class=sf>revision history</b>
<ul>
  <li>First version, May 2007, Brendan O'Connor
</ul>

<h2>Overview</h2>
<P>XLE supports the input and output of prolog-format forms of
F-Structures as well as Transfer data structures (including
SemReps).  These are their canonical formats for most purposes.  Both singleton
readings and packed structures are supported.

<P>This page documents both F-Structures as well as Transfer structures
together -- with special attention to the Prolog data structure representation
-- with pointers to more specific documentation elsewhere.  (Powerset
developers should be especially interested in SemReps, a particular kind of
Transfer structure; see the <a
  href="https://parc.svn.powerset.com/ps-semantics/dev/doc/semrep_dictionary.html">PS-Semantics
  Semrep Specification</a>.

<h2>The Prolog data structure format</h2>

Prolog data structures represent structured information in a nestable manner
similar to <a href=http://www.json.org/>JSON</a>.  They can have combinations
of the following types:
<ul>
  <li>atoms  (which are like strings)  (e.g. <i>PRED</i> or <i>'This is a sentence'</i>)
  <li>numbers of various sorts (integers, floats...)
  <li>lists (e.g. <i>[5, someatom, [3, 55]]</i>)
  <li>compounds (e.g. <i>predicatename(5, 4)</i>)
</ul>

Compounds (a.k.a. predicates or terms) are the structure that's somewhat unique
to Prolog.  They are similar to lists/arrays/tuples, except with a functor
(<i>"predicatename"</i>) out on front.

<P>Prolog structures do not support name-value maps (e.g. hashes or dictionaries).
Sometimes they are simulated via a list of predicates:  [name1(value1),
name2(value2)], though this convention does not guarantee uniqueness of names.

<P>(Note that since Prolog the language has strong support for recursive pattern
matching, it is often convenient to represent information as lists of related
terms, whereas in standard programming languages direct references and
name-value maps would be used.)

<P>You do not need to use Prolog to use a Prolog data structure file, just a
parser for it.  The most correct way to parse is to use Sicstus Prolog's own parser, accessed through read/2, or something similar. 
This can be exposed to C using the Sicstus C API;
this has been done for the XLE Ruby bindings, which can receive Prolog data
structures as native Ruby objects, via XLE::Xfr.plparse and
XLE::Xfr.plparse_file.

<P>For some notes on other XLE Prolog parsing code, see the bottom of this document.


<h2>fstructure/6</h2>
<P>
The <code>fstructure</code> format is a 6-place compound:
<pre> fstructure(string, properties, choices, equivs, fs_facts, cs_facts) </pre>
All elements but the first are lists.
<ul>
  <li><b>string</b>:  the surface form of the parsed sentence.
  <li><b>properties</b>: a list of meta information.
  <li><b>choices</b>: a description of the choice space, as a list of xor equivalencies (see the section <a href=#choice_spaces>Choice Spaces</a> below).
  <li><b>equivs</b>: equivalences list, see below.
  <li><b>fs_facts</b>: choiced facts of F-Structure constraints.
  <li><b>cs_facts</b>: choiced facts describing the C-Structure tree(s).
</ul>

<P>
An fstructure() compound is produced by the core XLE parser, and can also be
produced by transfer (where it is called the "fs_file" format.)  For more information
see the the main XLE manual, under the section 
<a href=xle.html#Prolog_Output>Prolog Output</a>.

<P>Confusingly, this sort of prolog literal is usually called an "F-Structure",
though it actually also contains information on the C-Structure (and therefore
some information about the morphological analysis.)

<h2>xfr/5</h2>

<P>
The native format of the transfer system is a 5-place compound:
<pre> xfr(choices, equivs, equalities, facts, doc) </pre>
All elements are lists.
<ul>
  <li><b>choices</b>: a description of the choice space, as a list of xor equivalencies.
  <li><b>equivs</b>: equivalences list, see below.
  <li><b>equalities</b>: see below.
  <li><b>facts</b>: a list of choiced facts.
  <li><b>doc</b>: list of meta information.  One of its elements is copied from the original F-Structure, if this xfr() originated from an F-Structure.
</ul>


<h2>Packed representations</h2>

fstructure() and xfr()'s use the same three mechanisms to describe packed structures: the choice space structure, a list of equivalences, and a list of choiced facts.

<a name=choice_spaces></a>
<h3>Choice space structure</h3>

fstructure's and xfr's represent the choice space in the same way.  The and-or graph of the choice space
is represented as the <code>choices</code> element as a list of disjunctive equivalencies:

<table border=0>
  <tr><td>
<pre>
  % Choices:
  [
  choice([A1,A2], 1),
  choice([B1,B2], 1),
  choice([C1,C2,C3,C4], A2)
  ]
</pre>
<td>
<img style="float: left" src=ChoiceSpaceSmallTree.png>
</table>

<P>
This says the true choice "1" is equivalent to the mutually exclusive
disjunction of A1 and A2; that is, (1 &hArr; A1 xor A2).  Exactly
one of A1,A2 must be true.

<P>The other choice declarations say (1 &hArr; B1 xor B2), and finally (1 &hArr;
oneof(C1,C2,C3,C4)).   So the true context 1 is also split by B1,B2, and then A2
is split by C1..C4.  For more information about choice spaces, see the page <a
  href=choice-spaces.html>Choice Spaces</a>.

<P>Also, this list can sometimes contain weights on different choices, e.g.
<pre>  weight(A1, -3.15)</pre>

(??? what is the option to ensure this gets printed?)

<h3>Choiced facts</h3>

F-Structure facts, C-Structure facts, and Transfer facts are all <i>choiced</i>
facts: each is true in a certain choice.  (Choiced facts are sometimes called
"contexted facts".)  Assertions are <code>cf(_,_)</code> two-place compounds.
An example from a semrep xfr structure is:

<pre>   cf(<span class=choice_name>A1</span>, <span class=fact_in_cf>in_context(t,role(vgrel,see:n(1,1),telescope:n(7,1))))</span>  </pre>

Where <span class=choice_name>A1</span> is the choice the fact is true for, and the second is the fact itself.
The expression in the first argument of cf() may, in principle, be any boolean
function of choice variables; in practice, it is almost always a disjunction.
(XLE's ambiguity management is based around <a href=http://citeseer.ist.psu.edu/maxwell91method.html>disjunctions of mutually exclusive
  variables</a>; that's why they can be thought of as "choices".)

<P>A complex disjunction might look like:  <code>or(A2, C3, or(B1,B3))</code>.
The <code>or</code> compounds appearing here can have any number of arguments
(??? but always greater than 1?), and expressions are not necessarily
normalized or flattened.

<P>XLE's choices used to be called "contexts", which can cause confusion when
reading old documentation or code.  In this sense, a "contexted fact" is the
same as what this document calls a "choiced fact".  This usage is particularly
common when dealing only with the XLE parser and not the transfer system.

<P>The "in_context" predicate is NOT about an XLE choice space node.  It is
about a linguistic context, a notion specific to the semantics system that
happens to be implemented in transfer.  It has nothing to do with the parser or
the transfer system.  From the perspective of the transfer system, it is merely
just another fact.  (In the history of XLE work, linguistic contexts came much
later than XLE choices; this is the reason for the terminology confusion.)

<P>For more information about semreps, see the <a
  href="https://parc.svn.powerset.com/ps-semantics/dev/doc/semrep_dictionary.html">PS-Semantics
  semrep specification</a>.

<P>F-Structure facts assert various constraints.  Most of the constraints are equality relations, e.g.
<pre> cf(A1,  eq(attr(var(0),'OBJ'), var(4))) </pre>

Other relations are possible, e.g.
<pre> cf(A1,  in_set('GenMunkForm', var(9))) </pre>

A <code>var(N)</code> construction is (usually?) defined by an eq(_,_) constraint.  They
can refer to semforms, attribute-value nodes, sets, or even other var(_)
constructs.  It can be challenging to write your own code to interpret an
F-Structure prolog structure, due to this pervasive (and choiced!) polymorphism,
as well as the need to create a keep a an equalities table over var(_) aliasing
(and compute transitive closures across it, when there are multiple names for
the same thing).

<P>C-Structure facts assert facts about the C-Structure trees in a packed
forest representation.  They also include the phi mappings that give the
correspondences between the C- and F-Structures.

<P>Much more information about F-Structure and C-Structure prolog format information is 
in the <a href=xle.html#Prolog_Output>section of the main XLE manual</a>.
<h3>Equivalences List</h3>

Equivalences is a list of the following types of compounds:
<ul>
 <li><b>define(_, _):</b> Defines an abbreviation of a large boolean
  combination (in practice, always a disjunction) of choices, e.g.:
  <pre>   define(CV_004, or(B1,or(A1,A2))</pre>
  Then <code>CV_004</code> can be used in place of the large disjuntion in the
  rest of the file.  (??? just in choiced facts cf's, or also in other
  equivalence list define's?)<br><br>
 <li><b>select(_, _):</b> A list of select's point out one fully specified
 singleton reading that was selected as the most preferred at time of printing.  Usually this describes the most probably parse (as chosen by the stochastic disambiguator).  It may be the case select's are specified for
 the reading selected in the Tk GUI when the prolog output was printed (??? need to confirm this?)
 <P>A select() is two-place, e.g. select(A1, 1).  This marks the A1 choice as
 being true.  Potentially many select's are necessary to fully specify a
 singleton reading.  (??? is it always the case that select's will always
 specify only a singleton reading?)
 <br><br>
 <li>??? Other predicates may be possible here?<br><br>
</ul>



<h2>Equalities List</h2>

<P>The equalities list is specific to xfr/5 structures.  It is always empty in
semreps xfr's.

<P>F-Structure fs_file equality
constraints can be tricky for transfer to deal with.  Equalities are a way for
transfer to cope with this: these constraints are treated in a special way
inside transfer.

<P>TODO give example and explain

<h2>Notes on other prolog parsing code</h2>
Contained within the XLE source code are several Prolog parsers for non-Prolog environments: 
<ul>
  <li>src/tcl/prolog_parser.[ch], a partially correct parser.  It is used for
      XLE's XML output, though it has various weaknesses.
  <li>prolog/bobs_db/pdg/prolog_parser.[ch], a very limited parser specialized for only one purpose.
  <li>src/prolog/prolog-read.g, another specialized parser.
</ul>

These alternatives are all not as good as making direct Sicstus calls for its
parser, though these have the advantage of not requiring the transfer.sav
Sicstus image to be loaded, and of not having to use the clunky (though
well-documented) Sicstus C API.

<h2>Older document</h2>

An older version of this document existed/exists on the Powerset wiki, at 
<a href=https://wiki.powerset.com/twiki/bin/view/Engineering/PlFileDocumentation>PlFileDocumentation</a>.

</body>
</html>
