
set fileindex testgen.lfg.fileindex
if {[file exists $fileindex]} {
    exec rm $fileindex
}

create-parser testgen.lfg

set XLE ../src/tcl/xle

set errors [regenerate-testfile testgen.testfile]

exit $errors
