/* (c) 2002-2004 by the Palo Alto Research Center.  All rights reserved */
/* (c) 1996-2001 by the Xerox Corporation.  All rights reserved */

/* The Clause data structure is the basic data structure for the simple */
/* boolean satisfaction package used by XLE to manipulate contexts. */

#define CONSISTENT   1
#define INCONSISTENT 0


#ifndef CLAUSE_H
#define CLAUSE_H
#define CLAUSEID

#define LAZY_NEGATION

// rdc: undefine for Windows
#ifdef OPAQUE
#undef OPAQUE
#endif

typedef unsigned int ClauseType;
#define CT_FALSE 0
#define CT_TRUE 1
#define CHOICE 2
#define NOT 3
#define OPAQUE 4
#define JUSTIFIED 5
#define AND 6
#define OR 7
#define CONS 8
#define MCONS 9



typedef enum {
  CL_SEXP,
  CL_NETWORK,
  CL_STATE,
  CL_ARC,
  CL_SOLUTION} ChoiceLabelType;

/* ----------------------------------------------------------------- */
/* Note that the order here is not completely arbitrary -- refer to  */
/* `clause_cache_order' and 'compound_clause' for more details.      */
/* CT_FALSE is never actually used. It is here only for completeness.*/
/* Instead falsity is indicated by a NULL 'Clause' pointer.          */
/* ----------------------------------------------------------------- */

struct Clause {

  union {
    /* CHOICE */
    struct ChoiceClause {
   
      Disjunction *disj;          /* the disjunction this choice belongs to */

      int id;                     /* the id for this choice */
      
    } choice;

    /* AND/OR/CONS */
    struct BooleanClause {

      Clause *item;             /* the first item in the compound */

      Clause *next;             /* the rest of the items (never NULL) */

    } compound;

    /* OPAQUE */
    struct OpaqueClause {

      Clause *clause;           /* the imported clause */

      Graph *graph;             /* the graph the clause was imported from */

    } opaque;   

    /* JUSTIFIED */
    struct JustifiedClause {

      Clause *clause;           /* the justified clause */

      Justification *justification;   /* its justification */

    } justified;   

    /* NOT */
    Clause *not_clause;         /* the clause that was negated */
  } body;

  unsigned short selected: 1;     /* selected by the user. */

  unsigned short changeable: 1;    /* this Clause can be destructively
				   modified. */

  unsigned short mark : 1;      /* a temporary mark bit */

  unsigned short mark1 : 1;	/* a temporary mark bit */

  unsigned short mark2 : 1;	/* a temporary mark bit */

  unsigned short mark3 : 1;	/* a temporary mark bit */

  unsigned short disjoint : 1;	/* This clause is disjoint */

  unsigned short value : 1;     /* Whether this clause should evaluate */
                                /* to True or False.                   */
  
  unsigned short all1 : 1;      /* used by conjoin_chart_clauses. */

  unsigned short all2 : 1;      /* used by conjoin_chart_clauses. */

  unsigned short is_new : 1;    /* used by conjoin_chart_clauses. */
  
  unsigned short exported : 1;  /* this clause has been imported by another */
				/* graph */

  unsigned short nogood : 1;    /* this clause is nogood */
 
  unsigned short skip : 1;      /* This nogood should be skipped
				   when enumerating bad solutions. */

  unsigned int embedded_in_nogood: 1; /* This opaque clause is embedded */
                                      /* in a nogood somewhere. */

  unsigned int non_local : 1;   /* Used by local_subtract_clause */

  unsigned int data : 2;        /* this choice appears somewhere in the data */

  unsigned int offset : 5;      /* used by disjoin_clause_trees */

  unsigned int indexed_cache: 1;/* the cache has been indexed */

  unsigned int segment : 6;     /* If indexed_cache is 1, then this is the
				   segment of the cache index.  Otherwise,
				   it is the number of items in the cache. */

  unsigned int type: 5;         /* Same as ClauseType, but saves storage */

  Graph *graph;			/* pointer to containing graph */

  Clause *pruned;               /* the pruned version of this clause */

  void *cache;		        /* cache of previously computed
				   operations on clauses.  This will be a
				   ClauseCache if Clause->segment is 0 and
				   a ClauseCacheIndex otherwise. */
  Clause *next; /* the next clause in graph's list of clause */

  unsigned int prefix;  /* used in clause trees */

#ifdef CLAUSEID
  unsigned int id;  /* used for debugging */
#endif
};

/* The Clause cache is sorted iff it is index. */
#define sorted_clause_cache(c) ((c)->indexed_cache)

extern /*const*/ Clause *True_Context;
extern /*const*/ Clause *False_Context;
extern int test_clauses;

struct ClauseStack {
  Clause **item;         /* the array of clauses in the stack */
  int max_size;           /* the current size of the array */
  int index;              /* the index of the next empty slot */
};

struct feature_weight_pair {
  int featureID;
  double value;
  feature_weight_pair *next;
};

/* ------------------------------------------------------------------------- */

struct Disjunction {

  unsigned int n_choices;       /* the number of choices are represented by */
				/* this disjunction and its internal */
				/* disjunctions */

  unsigned int id : 20;         /* identifier used for printing. */

  unsigned short internal : 1;    /* is this an internal disjunction? */

  unsigned short mark : 3;	/* a temporary mark bit */

  unsigned short mark1 : 1;	/* a temporary mark bit */

  unsigned short mark2 : 1;     /* a temporary mark bit */

  unsigned short all1 : 1;      /* used by conjoin_chart_clauses. */

  unsigned short all2 : 1;      /* used by conjoin_chart_clauses. */
  
  unsigned short split : 1;     /* Whether the Disjunction has been split. */

  unsigned short dependent_of_least_upper_bound : 1;

  Clause *context;		/* context in which disj is constructed */

  Clause *root_context;         /* root context (first non-choice) */

  int root_depth;               /* depth to root context */

  Clause *arm[2];               /* the two choices of this disjunction (all */
				/* disjunctions are binary disjunctions; */
				/* n-ary disjunctions are reduced to binary */
				/* disjunctions, some of which are */
				/* internal.) */

  char *label[2];               /* Label for each arm. */
  void *labeldata[2];           /* Label data for each arm. */     
  ChoiceLabelType labeltype[2]; /* Data type of label data. */
  void *labelavp[2];            /* AVPair of the choice. */

  Disjunction *disj[2];         /* the internal disjunctions needed for */
				/* n_choices > 2.  the context of disj[0] */
				/* is arm[0], the context of disj[1] is */
				/* arm[1] */

  DisjunctionList *dependents[2]; /* The dependents of each choice. */

  Disjunction *next; /* the next disjunction in graph's list of */
		     /* disjunctions */

  DUProp *props; /* a Lisp-style property list for extensions */
};

/* ----------------------------------------- */
/* example: when constructing a disjunction  */
/* of three choices, two disj. are created,  */
/* 'p' and 'q' and the contexts representing */
/* the three choices are 'q1', 'q2' and 'p2' */
/* where q1 and q2 are in the context of p1  */
/* ----------------------------------------- */

/* ------------------------------------------------------------------------- */

struct DisjunctionList {
  Disjunction *item;
  DisjunctionList *next;
};

/* ------------------------------------------------------------------------- */

typedef enum {CONJOIN, CONJOINABLE, DISJOIN, IMPORT, NEGATE, SUBTRACT,
	       LOCAL_SUBTRACT, NEW_AND, NEW_OR, SUBTRACT_NOGOOD, ADD_NOGOOD, 
	       COVERS, UNCOVERED, IN_NOGOOD, NEW_CONS, ENLARGE_NOGOOD, 
	       SIMPLIFY_CLAUSE, RESOLVE, PUSH_CONTEXT, 
	       IN_EXPORTED, EXPORTED_ID} ClauseOp;

/* The Clause cache is used to cache clause operations.  It is stored as a
   sorted linked list of ClauseCache data structures. The ClauseCache data
   structures are accessed by a key that consists of a ClauseOp, a pruned
   bit, and a Clause operand.  For efficiency, the linked list is indexed
   by ClauseCacheIndex data structures (see clause-cache-index.h). */

struct ClauseCache {  /* for caching the results of various clause */
		      /* operations */

  int operation : 15;       /* Same as ClauseOp, but saves storage */

  unsigned int pruned : 1;  /* whether or not the operation was pruned. */

  Clause *operand;     /* one of the operands of the operation.  the other */
		       /* operand is implicitly the clause that this cache */
		       /* is attached to. */

  Clause *result;      /* the result of the operation */

  ClauseCache *next;   /* next item in the cache */

};  

#endif

