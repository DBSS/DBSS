/* (c) 2002-2004 by the Palo Alto Research Center.  All rights reserved */
/* (c) 1996-2001 by the Xerox Corporation.  All rights reserved */
#ifndef VALUES_H
#define VALUES_H

#include "clause.h"


/* ------------------------------------------------------------------------- */

typedef enum {
  VT_NULL,           /* no value */
  VT_STR,            /* string */
  VT_NUM,            /* number */
  VT_PDFLOAT,        /* pointer to double float */
  VT_SFID,           /* semantic form id (may be renumbered) */
  VT_AVP,            /* AVPair */
  VT_EDGE,         
  VT_ATTR_ID,        /* used by GGFS relation */
  VT_REL_ID,         /* a relation id.  used by REL_MISSING. */
  VT_GENSYM,         /* unique ID, regenerated across edges to handle */
		     /* generation correctly */
  VT_NETWORK,        /* c-fsm NETptr (used for functional uncertainty) */
  VT_STATE,          /* c-fsm STATEptr (used for functional uncertainty) */
  VT_ARC,            /* c-fsm ARCptr (used for functional uncertainty) */
  VT_ARC_LABEL,      /* label of an ARCptr (used for functional uncertainty) */
  VT_CVPAIR,         /* the typical fact type (used in justifications) */
  VT_CONTEXTS,       /* used by conjoin_facts2 (used in justifications) */
  VT_CLAUSE,         /* for FALSE and incomplete nogoods (ditto) */
  VT_TRUE,           /* always evaluates to true (ditto) */
  VT_LAZY,           /* for lazy links (used in justifications) */
  VT_COPYEDGE,       /* the edge that this fact was copied from */
  VT_PATTERN,        /* a pattern string (with Kleene stars) */
  VT_TRANSFER,       /* an instantiated transfer rule */
  VT_VAR_ALIGNMENT,  /* a variable alignment */
  VT_OPAQUE1,        /* user defined opaque values */
  VT_OPAQUE2,        /* use get_next_opaque_type_id */
  VT_BASE_MAX = 50,  /* boundary between opaque types and tuple types */
  VT_SEMFORM,        /* semantic form type */
  VT_ISYM,           /* instantiated symbol type */
  VT_SUBSUMPTION,    /* an AVPair plus generation number */
  VT_RULETRACE,      /* a rule trace */
  VT_USER1,          /* user defined tuple types. first slot gives size */
  VT_USER2,          /* use get_next_user_type_id */
  VT_USER3, 
  VT_USER_MAX = 100, /* last user-defined tuple type */
  VT_TUPLE,          /* generic tuple types. size given in type name */
  VT_1_TUPLE,        /* VT_1_TUPLE used in arglist of intransitive verbs */
  VT_2_TUPLE, 
  VT_3_TUPLE,
  VT_4_TUPLE,
  VT_5_TUPLE,        
  VT_MAX = 200,      /* so that the compiler knows how much storage to
			allocate */
} ValueType;

/* ------------------------------------------------------------------------- */

struct TypedValue {

  ValueType type;  /* the type of the value */

  void *value;     /* the value itself */
};

/* ------------------------------------------------------------------------- */

typedef int CVProps;

#define SUBC 1
               /* The constraint f =c a is satisfied only if there is */
               /* another constraint f = a.  This type of constraint is */
	       /* called a subc constraint because of the c that follows */
	       /* the equality sign. */

#define NEGATED 2
               /* For constraints like f ~= a. */

#define LAZY 4
               /* Used for lazy equality links, both vertical copy links, */
	       /* and horizontal re-entrancy links. */
#define DISCHARGED 8
                  /* Indicates that a lazy link has already been       */
		  /* discharged.  This is stored in CVPair->discharged */
                  /* instead of CVPair->props.                         */

/* ------------------------------------------------------------------------- */

typedef enum {

  UNCHECKED = 0,                /* hasn't been checked for completeness */

  NEVER_COMPLETED = 1,

  COMPLETED_OUT_OF_CONTEXT = 2, /* there was a completing fact, but it  */
                                /* was in a disjoint context */

  COMPLETED = 3,                /* eventually completed */

} Completed;

/* ------------------------------------------------------------------------ */
/* A CVPair represents a contexted value plus some properties of the        */
/* relation.  The relation and one of its arguments is implicit in the      */
/* location of the CVPair.  For instance, if a CVPair is under the RVPair   */
/* of a particular AVPair, then the RVPair gives the relation, the AVPair   */
/* one of its arguments, and the remainder of the arguments are given by    */
/* the value (it may be a tuple of values).  Whether this instance of the   */
/* relation is negated, subc, or lazy is stored on the CVPair.  This allows */
/* us to store negated and non-negated values together, as well as subc and */
/* non-subc values.                                                         */
/* ------------------------------------------------------------------------ */

struct CVPair {  
            
  Clause *contexts[2];      /* old and new contexts (used by */
			    /* process_new_facts to make sure that */
			    /* deductions are only made once. */

  TypedValue value;         /* the typed value (may be a tuple of values) */

  unsigned short freed : 1; /* for debugging. */

  unsigned short solutioncontext : 1; /* contexts[0] is a RestrictedSolution */
				    /* instead of a Clause */
  unsigned short bypass : 1;  /* This is the current bypass link. */

  unsigned short discharged : 1; /* This lazy link has been discharged. */
  
  unsigned short backlink : 1; /* This lazy link was added as a backlink. */
  
  unsigned short recopy : 1; /* This equality link needs to be recopied. */
  
  unsigned short props : 5;  /* same as CVProps, but saves storage */

  unsigned short completed : 2; /* same as Completed, but saves storage */

  CVPair *next;             /* next cvpair in the list */
}; 

/* fields of instantiated symbol */

#define IS_LENGTH 0
#define IS_GENSYM 1
#define IS_BASESYMBOL 2

#define coerce_to_avpair(tv) \
    ((tv).type == VT_AVP ? (AVPair *)(tv).value : \
     (AVPair *)(long)printf("Type fault at %s:%d\n", __FILE__, __LINE__))

#endif


