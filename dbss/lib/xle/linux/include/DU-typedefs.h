/* (c) 2002-2004 by the Palo Alto Research Center.  All rights reserved */
/* (c) 1996-2001 by the Xerox Corporation.  All rights reserved */

#ifndef DU_TYPEDEFS_
#define DU_TYPEDEFS_

/* basic stuff */

typedef struct Storage Storage;

/* from compstate.h */

typedef struct DUProp DUProp;
typedef struct DUCompState DUCompState;
typedef struct OTMark OTMark;
typedef struct ConstraintData ConstraintData;
typedef struct StringList StringList;
typedef struct NumberList NumberList;
typedef struct GlobalGensym GlobalGensym;
typedef struct DynamicAttrSymbolsTable DynamicAttrSymbolsTable;

struct StringList {
  char *item;
  StringList *next;
};

/* from graph.h */

typedef struct Graph Graph;
typedef struct GraphList GraphList;
typedef struct GraphIdList GraphIdList;
typedef struct GraphClauses GraphClauses;
typedef struct Gensym Gensym;
typedef struct CopiedGensym CopiedGensym;
typedef struct SemformEdge SemformEdge;
typedef struct TypedValueList TypedValueList;
typedef struct HeadPrecedenceCache HeadPrecedenceCache;
typedef struct AVPairList AVPairList;
typedef struct SuppressionIndex SuppressionIndex;
typedef struct SuppressedFact SuppressedFact;

typedef struct CSubTree CSubTree;
typedef struct CSubTreeList CSubTreeList;

/* from justifications.h */

typedef struct Justification Justification;
typedef struct JustifiedFact JustifiedFact;
typedef char Rule;

/* from values.h */

typedef struct CVPair  CVPair;
typedef struct TypedValue  TypedValue;

/* from attributes.h */

typedef struct AVPair AVPair;
typedef struct AVPArray AVPArray;

/* from relations.h */

typedef struct RVPair  RVPair;

/* from clause.h */

typedef struct Clause Clause;
typedef struct ClauseCache ClauseCache;
typedef struct ClauseStack ClauseStack;
typedef struct Disjunction Disjunction;
typedef struct DisjunctionList DisjunctionList;
typedef struct feature_weight_pair feature_weight_pair;

/* from solutions.h */

typedef struct InternalSolution      InternalSolution;
typedef struct InternalSolutionsList InternalSolutionsList;
typedef struct RestrictedSolution    RestrictedSolution;
typedef struct SolutionList          SolutionList;
typedef struct RestrictionSet        RestrictionSet;
typedef struct SolutionIndex         SolutionIndex;
typedef signed short OTCount;

/* from unify-queue.h */
typedef struct UnificationQueue UnificationQueue;
typedef struct SubsumptionQueue SubsumptionQueue;
typedef Clause *(ContextPair)[2];

typedef unsigned int ArgList;
typedef unsigned int LocalAttrs;

/* c-fsm types. */
#ifndef XLE_C_FSM_H
#define XLE_C_FSM_H

#undef inline
#define XLE_INLINE inline

typedef struct Chart Chart;
#ifndef C_FSM_TYPES /* defined in the c-fsm package */
typedef struct NETtype *NETptr;
typedef struct STATE *STATEptr;
typedef struct ARC *ARCptr;
#endif
#ifndef C_FSM_HEAP
typedef struct HEAP *HEAPptr;
#endif
#ifndef C_FSM_HASH_TBL
typedef struct HASH_TABLE *HASH_TABLEptr;
#endif

#ifndef __LFG_H_INCLUDED__
typedef struct Grammar Grammar;
typedef struct SExp SExp;
typedef struct SExpArray SExpArray;
typedef char *CategoryID;
#endif

#endif /* !XLE_C_FSM_H */
#endif /* !DU_TYPEDEFS_ */
