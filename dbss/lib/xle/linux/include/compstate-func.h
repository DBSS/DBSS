/* (c) 2002-2004 by the Palo Alto Research Center.  All rights reserved */
/* (c) 1996-2001 by the Xerox Corporation.  All rights reserved */
#ifndef COMPSTATE_FUNC_H
#define COMPSTATE_FUNC_H

DUCompState *create_du_comp_state(Grammar *grammar, struct Chart *chart);
void initialize_du_comp_state(DUCompState *compstate);
void free_du_comp_state(DUCompState *compstate);

int get_graph_count(DUCompState *compstate);

struct Chart *get_compstate_chart(DUCompState *compstate);

#endif
