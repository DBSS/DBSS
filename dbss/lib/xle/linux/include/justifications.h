/* (c) 2002-2005 by the Palo Alto Research Center.  All rights reserved */
/* (c) 1996-2001 by the Xerox Corporation.  All rights reserved */

/* Justifications are used to explicitly represent the deductions.  They */
/* are used for integrity checks and for grammar debugging. */

#ifndef JUSTIFICATIONS_H
#define JUSTIFICATIONS_H

/* a Justified Fact is true IFF its Justifications are true */

struct JustifiedFact {

  TypedValue fact; /* the fact justified */

  Justification *justifications; /* its justifications */

  JustifiedFact *next;  /* this is a threaded list */

};

/* a Justification is a deduction that justifies some fact */

struct Justification {

  unsigned int imported : 1; /* justification was imported from a daughter */

  unsigned int immutable : 1; /* next field cannot be modified; copy */
			      /* whole justification instead */

  AVPair *avpair;       /* the AVPair where the deduction took place */

  Clause *context;      /* the context of the deduction */
   
  Rule *rule;           /* information about the rule used */

  TypedValue ant1;      /* the antecedents of the rule used */

  TypedValue ant2;

  Justification *next;  /* this is a threaded list */

};

Justification *create_justification(Graph *graph, Clause *clause, AVPair *avp, 
				    ValueType type1, void *fact1,
				    ValueType type2, void *fact2);


#endif
