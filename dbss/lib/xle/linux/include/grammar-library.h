/* (c) 2004-2005 by the Palo Alto Research Center.  All rights reserved */
#ifndef __GRAMMAR_LIBRARY_H_INCLUDED__
#define __GRAMMAR_LIBRARY_H_INCLUDED__

/* ---------------------------------------------------------------- */
/* A grammar library is a library of code associated with a grammar */
/* that serves the function of a transducer in the morph config.    */
/* In the morph config, the notation for grammar libraries is       */
/* [libfoo initfile] or [libfoo]. When XLE encounters a grammar     */
/* library named "libfoo" in the morph config, it first             */
/* looks for "libfoo-<platform>.<ext>", where <platform> is the     */
/* current XLE platform (e.g. solaris, macosx, linux2.1, linux2.2,  */
/* or linux2.3) and <ext> is one of "so", "dylib", or "dll".        */
/* If it doesn't find anything that matches this, it then looks for */
/* "libfoo.<ext>". If it finds one of these files, it then calls    */
/* the functions described below.                                   */
/* To build a grammar library on MacOS X, use the -dynamic and      */
/* -bundle switches.                                                */
/* ---------------------------------------------------------------- */

void *initialize(char *init_file);
/* ---------------------------------------------------------- */
/* This function is called to initialize the grammar library. */
/* It is called with the name of an init file.  The init file */
/* can be either a data file or a file of parameters.         */
/* Any files that the init file needs should be included in   */
/* the OTHERFILES config field in the grammar & stored in the */
/* same directory as the init file so that make-bug-grammar   */
/* will work properly.  This function should return a pointer */
/* to a data structure that contains the state needed by the  */
/* grammar library.  If NULL is returned, then XLE assumes    */
/* that the initialization failed.  A grammar library can be  */
/* invoked with different init files within the same grammar. */
/* ---------------------------------------------------------- */

char *character_encoding(void *state);
/* ---------------------------------------------------------- */
/* This function returns the character encoding that the      */
/* grammar library will be using.  If this function doesn't   */
/* exists or returns NULL, then the character encoding will   */
/* default to the character encoding of the other transducers */
/* in the morph config or (if there are no others), the       */
/* character encoding of the grammar as a whole.              */
/* ---------------------------------------------------------- */

char *analyze(char *input, void *state);
/* --------------------------------------------------------------- */
/* This function is called to analyze a string when parsing.       */
/* It should return the analysis as a string.  If you want to      */
/* return more than one analysis, use analyze_to_regexp instead.   */
/* analyze must be the first transducer in the TOKENIZE section or */
/* the first transducer in a line of an ANALYZE section.           */
/* The library is responsible for freeing the output's storage     */
/* on the next call or when finalize is called (XLE makes a copy   */
/* of the output).  XLE is responsible for freeing the input's     */
/* storage.                                                        */
/* --------------------------------------------------------------- */

void *analyze_to_regexp(char *input, void *state);
/* ------------------------------------------------------------- */
/* This function is called to analyze a string when parsing.     */
/* It should return the analysis as a RegExp (see regexp.h).     */
/* It must be the first transducer in the TOKENIZE section or    */
/* the first transducer in a line of an ANALYZE section.         */
/* In the ANALYZE section, the output must have at least one     */
/* multi-character label at the end so that XLE can separate the */
/* morphemes.  XLE adds +Token to the input as an alternative    */
/* morphological analysis.  If NULL is returned, then the +Token */
/* analysis is the only morphological analysis given.            */
/* analyze_to_regexp takes priority over analyze.                */
/* The library is responsible for freeing the storage for the    */
/* regexp on the next call or when finalize is called.           */
/* XLE makes a copy of the regexp.  XLE is responsible           */
/* for freeing the storage for the input.                        */
/* ------------------------------------------------------------- */

void finalize(void *state);
/* ---------------------------------------------------------- */
/* This function is called when XLE is through with a grammar */
/* library.  It should free any storage being held by the     */
/* library and close any open files.                          */
/* ---------------------------------------------------------- */

#endif /* __GRAMMAR_LIBRARY_H_INCLUDED__ */
