/* (c) 2002-2004 by the Palo Alto Research Center.  All rights reserved */
/* (c) 1996-2001 by the Xerox Corporation.  All rights reserved */
#ifndef STORAGE_FUNC_H
#define STORAGE_FUNC_H

TypedValue *allocate_typed_value(Graph *, int);
Clause *allocate_clause(Graph *graph, ClauseType type);
char *allocate_string(Graph *graph, size_t size);

#define DECLARE_ALLOC_FUNCTION(type, item) type *allocate_##item(Graph *)

DECLARE_ALLOC_FUNCTION(Disjunction, disjunction);
DECLARE_ALLOC_FUNCTION(DisjunctionList, disjunction_list);
DECLARE_ALLOC_FUNCTION(Justification, justification);

DECLARE_ALLOC_FUNCTION(AVPair, avpair);
DECLARE_ALLOC_FUNCTION(CVPair, cvpair);
DECLARE_ALLOC_FUNCTION(ClauseCache, cached_clause);
DECLARE_ALLOC_FUNCTION(RVPair, rvpair);

DECLARE_ALLOC_FUNCTION(Gensym, gensym);
DECLARE_ALLOC_FUNCTION(CopiedGensym, copiedgensym);
DECLARE_ALLOC_FUNCTION(TypedValueList, typedvaluelist);
DECLARE_ALLOC_FUNCTION(HeadPrecedenceCache, head_precedence_cache);
DECLARE_ALLOC_FUNCTION(SemformEdge, semform_edge);

DECLARE_ALLOC_FUNCTION(GraphList, graph_list);
DECLARE_ALLOC_FUNCTION(UnificationQueue, unification_queue);
DECLARE_ALLOC_FUNCTION(SubsumptionQueue, subsumption_queue);

DECLARE_ALLOC_FUNCTION(InternalSolution, internal_solution);
DECLARE_ALLOC_FUNCTION(InternalSolutionsList, internal_solutions_list);
DECLARE_ALLOC_FUNCTION(RestrictedSolution, restricted_solution);
DECLARE_ALLOC_FUNCTION(RestrictionSet, restriction_set);
DECLARE_ALLOC_FUNCTION(SolutionIndex, solution_index);
DECLARE_ALLOC_FUNCTION(SolutionList, solution_list);

DECLARE_ALLOC_FUNCTION(AVPairList, avpair_list);
DECLARE_ALLOC_FUNCTION(SuppressionIndex, suppression_index);
DECLARE_ALLOC_FUNCTION(SuppressedFact, suppressed_fact);

void *du_alloc(DUCompState *compstate, size_t size, char *type);
void du_free_cell(void *cell, size_t size, char *type, 
		  DUCompState *compstate);

#endif /* !STORAGE_FUNC_H */
