/* (c) 2002-2005 by the Palo Alto Research Center.  All rights reserved */
/* (c) 1996-2001 by the Xerox Corporation.  All rights reserved */
#ifndef SEMFORMS_H
#define SEMFORMS_H

/* A semantic form is represented as a tuple of TypedValues.  The first
  item gives the total length of the tuple (including the first item), the
  second the predicate name (or designator), the third gives the lexical
  id, the fourth gives number of arguments, then follow arguments and
  non-arguments.  The number of non-arguments can be found by starting with
  the tuple length and subtracting the number of arguments and all of the
  fixed fields. */

#define SF_LENGTH    0  /* position of the tuple length */
#define SF_FUNCTION  1  /* predicate name or designator */
#define SF_GENSYM    2  /* unique identifier for predicate */
#define SF_ARG_COUNT 3  /* how many args does the semform have? */
#define SF_ARG1      4  /* position of first argument */

/* -------------------------------------------------------- */
/* Useful procedures for accessing parts of semantic forms. */
/* These functions take 'tv' by reference to make it easier */
/* for users of Allegro Common Lisp.                        */
/* -------------------------------------------------------- */

void semform_function(TypedValue *tv, TypedValue *fn);
/* Stores the function in the fn typed value. */

int semform_id(TypedValue *tv);
/* Returns the semform id as an integer. */

ValueType semform_id_type(TypedValue *tv);
/* Returns the semform id type. */

void set_semform_id(TypedValue *tv, int id);
/* Sets the semform id of the semantic form. */

int semform_arg_count(TypedValue *tv);
/* Returns the number of arguments. */

void semform_arg(TypedValue *tv, int i, TypedValue *arg);
/* Stores the ith argument in the arg typed value. */
/* If there isn't an ith argument, then arg.type will be VT_NULL. */

int semform_nonarg_count(TypedValue *tv);
/* Returns the number of non-arguments. */

void semform_nonarg(TypedValue *tv, int i, TypedValue *nonarg);
/* Stores the ith non-argument in the nonarg typed value. */
/* If there isn't an ith non-argument, then nonarg.type will be VT_NULL. */

/* ------------------- */
/* Utility procedures. */
/* ------------------- */

void install_semantic_forms();
AttrID semform_attrID(TypedValue tv, int i);
int incoherent_attr(Graph *graph, AVPair *avp, int attr_id,
		    Clause *context);
Clause *governed_context(Graph *graph, AVPair *avp, int attr_id);
int governed_attr(Graph *graph, AVPair *avp, int attr_id, 
		  Clause *context);
int disjoint_head_lists(TypedValue tv1, TypedValue tv2);
#endif
