/* (c) 2002-2004 by the Palo Alto Research Center.  All rights reserved */
/* (c) 1996-2001 by the Xerox Corporation.  All rights reserved */
#ifndef COMPSTATE_H
#define COMPSTATE_H
/* ----------------------------------------------------------------------- */
/* Each parser and generator has its own DUCompState.  It is responsible   */
/* for managing all of the information that is global to the computational */
/* process.  For instance, storage is managed at the level of the          */
/* DUCompState.  The DUCompState is separate from the Chart data structure */
/* because we wanted DU to be usable as a standalone unification package.  */
/* ----------------------------------------------------------------------- */

//#define OLD_DU_ALLOC

enum FieldName {CLAUSE_MARK,
		CLAUSE_MARK1,
		CLAUSE_MARK2,
		CLAUSE_MARK3,
		DISJUNCTION_MARK,
		DISJUNCTION_MARK1,
		DISJUNCTION_MARK2,
		GRAPH_MARK,
		GRAPH_MARK2,
                LAST_FIELD_NAME};
		
#define MAX_STOP_POINT_SIZE 10

struct DUCompState {

  int max_heap;     /* number of heaps in the array below */

  int du_heapcnt;
  struct DUheap *du_heaparray;

#ifdef OLD_DU_ALLOC
  HEAPptr *heaps;   /* array of fsm heap pointers */
#endif

  int     graph_ct; /* number of graphs allocated */

  DUProp *props;    /* user defined properties 
		       (see graph.h for definition of DUProp) */

  Grammar *grammar; /* the grammar being used */

  struct Chart *chart; /* the chart being used */

  unsigned int completing; /* we have started to look for incomplete
			      nogoods, and may generate some not
			      clauses. */

  unsigned int gen_goal_predicting; /* this bit is set when the generator
				       predicts new goal values, to prevent
				       an optimization in the FU code */
  AVPair *goal_filter;              /* used by filter_constraint */

  AVPair *path_avpair;  /* used for instantiating off-path constraints. */

  void *path_arc; /* used for determining which off-path attributes should
		     be nonconstructive. */

  unsigned int solving; /* we have started to solve the nogood database. */

  unsigned int prune;  /* whether or not clauses should be pruned. */

  unsigned int debugging;  /* We are debugging. */

  int total_events; /* Total events processed. */

  unsigned shared_gensyms; /* whether typed values that are equivalent */
                           /* should get the same gensym in overlapping */
                           /* contexts. This is used by check_disjuncts. */

  unsigned int next_global_gensym; /* next global gensym */

  AVPair **attr_index; /* This is an index of constraints by the attributes
			  that they are stored on.  It is used by the global
			  completeness code. */

  int attr_index_size; /* The attr_index size. */

  AttrID *local_attr;   /* An array of local attributes. */

  char **inverse_cat;  /* This is an array of categories that need to be
			  added as REL_CAT_INVERSE. */
  
  int next_inverse_cat; /* The inverse_cat size. */

  ArgList *arglists;   /* Array of arglists encountered so far. */
  
  int arglists_size;   /* Size of the arglist array. */

  int next_arglist;    /* The index of the next arglist available. */

  int filler_counts_invalid; /* We can't filter analyses based on filler
				  counts because there was an unlicenced
				  distribution encountered. */

  /* ----------------------------------------------------------------- */
  /* Optimality Theory information is cached on the DUCompState rather */
  /* than the grammar so that it can vary between the parser and       */
  /* the generator.                                                    */
  /* ----------------------------------------------------------------- */
  int num_OT_ranks;    /* The number of OT ranks stored in OT_rank */

  SExp **OT_rank;      /* Array of lists of OT marks sorted by rank */

  StringList **ignored_OT_marks;  /* Arrray of lists of ignored OT marks */

  HASH_TABLEptr OT_hash; /* Hash from name to OTMark */

  HASH_TABLEptr edge_span_hash; /* Used for local optimality marks */

  int neutral_OT_mark; /* The position of the NEUTRAL OT mark. */

  int nogood_OT_mark; /* The position of the NOGOOD OT mark. */

  int cstructure_OT_mark; /* The position of the CSTRUCTURE OT mark. */

  int ungrammatical_OT_mark; /* The position of the UNGRAMMATICAL OT mark. */

  int inconsistent_OT_mark; /* The position of the INCONSISTENT OT mark. */

  int incomplete_OT_mark; /* The position of the INCOMPLETE OT mark. */

  int incoherent_OT_mark; /* The position of the INCOHERENT OT mark. */

  int local_OT_mark;      /* whether there are any local OT marks. */

  int fragment_OT_mark_override;  /* temporary position of Fragment mark  */
			          /*  (used by the fragment guide)        */
  
  int disable_OT;   /* Disable Optimality Theory. */

  int OT_stop_point[MAX_STOP_POINT_SIZE]; /* intermediate stopping points. */

  int OT_stop_point_size;  /* the number of elements in the array above. */

  int current_stop_point; /* The mark that we are processing down to. */

  OTMark *OT_overrides; /* User overrides for optimality marks. */

  char *fieldlockfile[LAST_FIELD_NAME];  /* file where field name locked. */

  int fieldlockline[LAST_FIELD_NAME];  /* line where field name locked. */

  int queues;  /* number of graphs with non-empty queues. */

  int discharged_queue; /* whether a queue got discharged. */

  HASH_TABLEptr constraint_hash; /* Hash from SExp to data. */

  HASH_TABLEptr attribute_hash; /* Hash from strings to AttrID. */

  int has_complex_sisters;   /* The chart has complex sisters (e.g */
                             /* (* RIGHT_SISTER RIGHT_SISTER).     */

  int abbrev_contains_pred; /* abbreviation attributes that contain preds. */

  int unlocks;

  DynamicAttrSymbolsTable *dynamic_table;
};

struct NumberList {
  int item;
  NumberList *next;
};

struct OTMark {
  char *name;
  char *user_rank;
  unsigned int rank : 20;
  unsigned int preferred : 1;
  unsigned int ungrammatical : 1;
  unsigned int local : 1;
  unsigned int conditioning : 1;
  unsigned int skimming_nogood : 1;
  StringList *conditioning_OT_marks;
  OTMark *next;
};

struct ConstraintData {
  unsigned int local_up_attributes_computed : 1;
  unsigned int local_down_attributes_computed : 1;
  unsigned int up_equals_down_computed : 1;
  unsigned int down_is_member_of_up_computed : 1;
  unsigned int up_equals_down : 1;
  unsigned int down_is_member_of_up : 1;
  LocalAttrs local_up_attributes;
  LocalAttrs local_down_attributes;
};

struct GlobalGensym {
  Graph *graph;        /* The graph the global gensym was instantiated in */
  char *name;          /* The semantic function name */
  int new_id;          /* The remapped id for the gensym  */
};

void *get_compstate_prop(DUCompState *compstate, char* propname);
void set_compstate_prop(DUCompState *compstate, char* propname, void
			*value);
int compstate_aborted(DUCompState *compstate);

ConstraintData *get_constraint_data(SExp *constraint, 
				    DUCompState *compstate);

#define lock_field(compstate, fieldname) {                    \
  if ((compstate)->fieldlockfile[(fieldname)] &&              \
      !compstate_aborted((compstate))) {                      \
    fprintf(stderr, "field already locked by %s, line %d\n",  \
	    (compstate)->fieldlockfile[(fieldname)],          \
	    (compstate)->fieldlockline[(fieldname)]);         \
    kill(getpid(), SIGUSR1);}                                 \
  (compstate)->fieldlockfile[(fieldname)] = __FILE__;         \
  (compstate)->fieldlockline[(fieldname)] = __LINE__;         \
}                                                             \

#define unlock_field(compstate, fieldname) {              \
  assert((compstate)->fieldlockfile[(fieldname)]);        \
  (compstate)->fieldlockfile[(fieldname)] = NULL;         \
}                                                         \

#define check_field_lock(compstate, fieldname) {          \
  assert((compstate)->fieldlockfile[(fieldname)]);        \
}                                                         \

#endif
