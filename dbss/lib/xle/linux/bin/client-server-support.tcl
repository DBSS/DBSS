# (c) 2002-2005 by the Palo Alto Research Center.  All rights reserved.
# (c) 1997-2001 by the Xerox Corporation.  All rights reserved.

proc xle-server-accept {socket address port} {
    global xle_server_socket

    set xle_server_socket(addr,$socket) [list $address $port]
    fconfigure $socket -buffering line
    fileevent $socket readable [list xle-server $socket]
}

proc xle-server {socket} {
    global xle_server_socket

    if {[eof $socket] || [catch {gets $socket request}]} {
	close $socket
	unset xle_server_socket(addr,$socket)
	exit
    } else {
	if {[string compare $request "halt"] == 0} {
	    puts $socket halted
	    flush $socket
	    close $xle_server_socket(server)
	    exit
	} else {
	    ###################################
	    # Evaluation of client's requests #
	    ###################################
	    if {![catch {eval $request} result]} {
		puts $socket yes
	    } else {
		puts $socket no
		puts $result
	    }
	    flush $socket
	}
    }
}

proc xle-client {host port} {
    set socket [socket $host $port]
    fconfigure $socket -buffering line -blocking 0
    fileevent $socket readable [list xle-client-socket-handler $socket]
    return $socket
}

proc xle-client-socket-handler {socket} {
    global server_response wait_for_server
    if {[eof $socket]} {
	catch {close $socket}
	puts stderr ""
	puts stderr "----------------------"
	puts stderr "XLE server terminated."
	puts stderr "----------------------"
	puts stderr ""
	return
    }
    if {[gets $socket ack] < 0} {
    } else {
	set server_response $ack
	set wait_for_server 1
	return
    }
}

###############################################################################

proc start-xle-server {{port 2540}} {
    global xle_server_socket

    set xle_server_socket(server) [socket -server xle-server-accept $port]
}

proc start-xle-client  {{port 2540} {host "localhost"}} {
    set xle_client_socket [xle-client $host $port]
    return $xle_client_socket
}

proc wait-till-server-responds {} {
    global server_response wait_for_server
    vwait wait_for_server
    return $server_response
}

proc tell-xle-server {xle_client_socket command} {
    global wait_for_server
    set wait_for_server 0
    
    # puts "Sending: '$command'"
    puts $xle_client_socket $command
    flush $xle_client_socket

    set ack [wait-till-server-responds]
    # puts "Received: $ack"

    return $ack
}

proc xle-client-loop {xle_client_socket} {
    set response ""
    while {1} {
	puts -nonewline stdout "tell server% "
	flush stdout
	gets stdin request
	if {$request == "quit"} break
	set response [tell-xle-server $xle_client_socket $request]
	if {$response == "halted"} {
	    break
	}
    }
}
