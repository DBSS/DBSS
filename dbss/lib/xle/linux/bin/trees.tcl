# (c) 2002-2004 by the Palo Alto Research Center.  All rights reserved.  
# (c) 1997-2001 by the Xerox Corporation.  All rights reserved.  
# This is the Tcl/Tk interface to the tree displayer.  The entry procedures 
# that are defined in this file are: show-sample-tree, show-trees, find-tree, 
# print-tree, print-tree-as-sexp, show-chart, show-morph, chart-statistics

# DTREE displayer

# The user interface follows the Medley user interface
# <Button-1> -> show f-descriptions
# <Button-2> -> show projection
# <Button-3> -> help
# <Control-Button-x> -> show source
# <Control-Shift-Button-x> -> inspect data structures

set showTokens 1
set showAlternatives 1
set showSubTrees 0

proc show-sample-tree {edge} {
    if {$edge == ""} {return}
    set chart [get_field $edge chart]
    if {[root_edge $chart] == $edge} {
	if {[get_field $chart mode] == "ChartTransfer" &&
	    [extract_chart_graph $chart] != ""} {
	    display-chosen-solution $chart
	    return
	}
	if {[get-chart-prop $chart "property_weights_file"] != ""} {
	    set graph [extract_chart_graph $chart]
	    if {$graph != "" && $graph != "(Graph)x0"} {
		unmark_all_choices $graph
		select-most-probable-structure $graph
		display-chosen-solution $chart
		return
	    }
	}
    }
    # show the first tree
    set tree [get_next_tree (DTree)0 $edge]
    show-trees $tree $chart
}

proc show-subtree {subtree {chart ""}} {
    if {$chart == ""} {
	set chart [default-chart]
    }
    set count [scan $subtree "%d:%d" edge subtree]
    if {$count == 1} {
	set subtree 0
    } elseif {$count != 2} {
	puts "error: show-subtree expecting arguments of the form 17:1 or 23"
	return
    }
    set tree [get_subtree $chart $edge $subtree]
    show-trees $tree
    return ""
}

proc show-cat {cat left right} {
    set chart [default-chart]
    set mode [get_field $chart mode]
    foreach edge [get_edges $chart] {
	set edgecat [get_field $edge category]
	if {$edgecat != $cat} {continue}
	set state [get_field $edge state]
	if {$state != "" && $mode != "ChartGeneration"} {continue}
	set edgeright [get_field $edge right_vertex]
	set vertex [get_field $edge vertex]
	set edgeleft [get_field $vertex index]
	if {$left != $edgeleft} {continue}
	if {$right != $edgeright} {continue}
	set id [get_field $edge id]
	show-subtree "${id}"
    }
}

set chartgraph ""

proc show-trees {trees {chart ""}} {
    global defaultWindows
    if {$trees == ""} {set trees "(DTree)0"}
    if {$chart == ""} {
	set chart [default-chart $trees]
    }
    show-trees-only $trees $chart
    if {[lsearch $defaultWindows "fstructure"] != -1} {
	show-fstructures [lindex $trees 0] $chart
    }
}

proc show-trees-only {trees {chart ""}} {
    if {$trees == ""} {set trees "(DTree)0"}
    if {$chart == ""} {
	set chart [default-chart $trees]
    }
    set self [chart-window-name $chart tree]
    setup-tree-window $self $chart
    set-window-prop $self data $trees
    if {[winfo exists $self.sentence]} {
	destroy $self.sentence
    }
    display-dtrees $trees $self.value
    show-tree-count $chart
    set fschartwindow [chart-window-name $chart fschart]
    set chartgraph [get-window-prop $fschartwindow data]
    if {$chartgraph != ""} {
	# the chartgraph can change if import_clause clears solutions
	set chartgraph2 [extract_chart_graph $chart]
	if {$chartgraph2 != "" &&
	    $chartgraph2 != $chartgraph} {
	    display-fschart $chartgraph2
	    display-fschartchoices $chartgraph2
	}
    }
    raise $self
    return $self
}

proc show-tree-count {chart} {
    set self [chart-window-name $chart tree]
    set trees [get-window-prop $self data]
    set edge [get_field $trees edge]
    if {$edge != ""} {
	set-window-prop $self edge $edge
    }
    set edge [get-window-prop $self edge]
    set count [count_trees $edge]
    set good [count_trees $edge "-good"]
    set cat [get_field $edge category]
    set id [get_field $edge id]
    if {$cat != "*TOP*"} {
	set cat $cat:$id
    }
    if {$good != 0} {
	if {$good == 1} {set s ""} else {set s "s"}
	wm title $self "$good valid tree${s} for $cat"
    } else {
	if {$count == 1} {set s ""} else {set s "s"}
	wm title $self "$count invalid tree${s} for $cat"
    }
    update idletasks    
}

proc default-chart {{data ""}} {
    global defaultchart defaultparser defaultgenerator defaulttranslator
    if {$data != ""} {
	set chart [get_field $data chart]
	if {$chart != ""} {return $chart}
    }
    if {$defaultchart != ""} {
	return $defaultchart
    } elseif {$defaultparser != ""} {
	return $defaultparser
    } elseif {$defaultgenerator != ""} {
	return $defaultgenerator
    } else {
	return $defaulttranslator
    }
}

proc default-window {type {chart ""}} {
    global defaultchart noTk
    if {$noTk} {return ""}
    set windows ""
    if {$chart == "" && $defaultchart != ""} {
	set windows [default-window $type $defaultchart]
	if {$windows != ""} {return [lindex $windows 0]}
    }
    foreach child [winfo children .] {
	if {[get-window-prop $child type] != $type} {continue}
	if {[get-window-prop $child projpath] != ""} {continue}
	if {[get-window-prop $child chart] == $chart} {
	    if {$chart != ""} {return $child}
	}
	lappend windows $child
    }
    return [lindex $windows 0]
}

proc check-grammar-disjuncts {{all ""}} {
    check_grammar_disjuncts [default-chart] $all
}

set treeCommands {
    {command -label "Resize Window" -font xleuifont \
	 -command "resize-canvas $self.value" \
	 -accelerator "<r>" \
	 -doc "Toggles resizing a window to fit the tree."}
    {command -label "Copy Window" \
	-command "copy-window $self" \
	-doc "Makes a copy of the window."}
    {command -label "Print Postscript" -font xleuifont \
	 -command "print-tree * $self" \
	 -doc "Writes a postscript file (.eps)."}
    {command -label "Print SExp" -font xleuifont \
	 -command "print-tree-as-sexp * $self" \
	 -doc "Writes a parenthesized list that corresponds to the tree as a text file (.txt)."}
    {command -label "Show Input" -font xleuifont \
	 -command "show-input $self" \
	 -doc "Shows the input to the parser or generator."}
    {command -label "Show Morph Window" -font xleuifont \
	 -command "show-morph $self" \
	 -doc "Shows the morphology."}
    {command -label "Show Chart Window" -font xleuifont \
	 -command "show-chart $self" \
	 -doc "Shows the chart."}
    {command -label "Show Bracket Window" -font xleuifont \
	 -command  "show-chart-nav $self" \
	 -doc "Shows the window for bracketing constituents" \
	 -doc "(and non-constituents)."}
    {command -label "Check Disjunctions" -font xleuifont \
	 -command  "check-grammar-disjuncts" \
	 -doc "Looks for non-exclusive disjuncts that are possible" \
	 -doc "sources of spurious ambiguities.  Uses a heuristic" \
	 -doc "to filter out non-exclusive disjuncts that are unlikely" \
	 -doc "to produce spurious ambiguities." \
     }
    {command -label "Check All Disjunctions" -font xleuifont \
	 -command  "check-grammar-disjuncts -all" \
	 -doc "Looks for non-exclusive disjuncts that are possible" \
	 -doc "sources of spurious ambiguities.  Doesn't filter" \
	 -doc "the disjuncts at all." \
     }
}

set treeViews {
    {checkbutton -label "node numbers" \
	 -variable "showNumbers" \
	 -command  "redisplay-dtrees" \
	 -accelerator "<n>" \
	 -doc "Toggles display of node numbers." \
	 -default "set showNumbers 1"}
    {checkbutton -label "partials" \
	 -variable "showPartials" \
	 -command "redisplay-dtrees" \
	 -accelerator "<p>" \
	 -doc "Toggles display of partials." \
	 -default "set showPartials 0"}}

set tokenView {
    {checkbutton -label "tokens" \
	 -variable "showTokens" \
	 -command "redisplay-dtrees" \
	 -accelerator "<t>" \
	 -doc "Toggles display of tokens." \
	 -default "set showTokens 0"}}
    
proc setup-tree-window {self chart} {
    global window_width window_height
    global treeCommands treeViews
    if {[winfo exists $self]==0} {
	set-window-prop $self type tree
	set-window-prop $self chart $chart
	set position [window-position $self]
	toplevel $self
	wm minsize $self 200 200
	wm geometry $self ${window_width}x${window_height}${position}
	set-window-prop $self position $position
	set color [highlight-color $self]
	$self configure -highlightcolor $color -highlightbackground $color \
		-highlightthickness 1
	frame $self.panel
	pack $self.panel -in $self -anchor nw
	add-kill-button $self $self.panel
	button $self.prev -text "prev" -font xleuifont \
		-command "show-prev-tree $self"
	bind $self.prev <Button-3> {
	    puts ""
	    puts "Shows the previous tree."
	    print-button-command %W
	}
	pack $self.prev -in $self.panel -side left -anchor nw
	button $self.next -text "next" -font xleuifont \
		-command "show-next-tree $self"
	bind $self.next <Button-3> {
	    puts ""
	    puts "Shows the next tree."
	    print-button-command %W
	}
	pack $self.next -in $self.panel -side left -anchor nw
	
	# Commands
	set cmenu $self.panel.commands.menu
	menubutton $self.panel.commands -text Commands \
	    -font xleuifont -menu $cmenu
	pack $self.panel.commands -in $self.panel -side left
	create-xle-menu $self $cmenu treeCommands
	
	# Views
	set omenu $self.panel.views.menu
	menubutton $self.panel.views -text Views \
	    -font xleuifont -menu $omenu
	pack $self.panel.views -in $self.panel -side left
	create-xle-menu $self $omenu treeViews
	tk_menuBar $self.panel $self.panel.commands $self.panel.views
	
	# View accelerators
	# create-accelerator-buttons $self $self.panel treeViews

	set scrollable [scrollable-canvas $self.value $self]
	pack $scrollable -in $self -expand 1 -fill both
    } else {
	clear-window $self
	focus-window $self 1
    }
}

proc find-tree {filename {chart ""}} {
    if {$chart == ""} {set chart [default-chart]}
    set file [open $filename r]
    set tree [read $file]
    close $file
    show-trees [find_tree $chart $tree]
}
    

set printing 0

proc print-tree {{file "*"} {window ""}} {
    global printing
    if {$window == ""} {
	set chart [default-chart]
	set window [chart-window-name $chart tree]
    }
    set tree [get-window-prop $window data]
    if {$tree == ""} {
	puts "No tree to print!"
	return
    }
    if {$file == "*"} {set file [default-filename "tree" "eps" $tree]}
    set chart [get_field $tree chart]
    set printing 1
    show-trees $tree $chart
    print-canvas $file ${window}.value.1
    set printing 0
    show-trees $tree $chart
    puts "tree printed on $file."
}

proc print-canvas {file canvas} {
    global xleFontMap original_tk_scaling
    update idletasks
    tk scaling $original_tk_scaling
    set bbox [$canvas bbox all]
    set width [lindex $bbox 2]
    set height [lindex $bbox 3]
    if {$width == ""} {
	tk scaling 1.0
	return
    }
    set maxw [expr 8*72]
    set maxh 756; # expr 10.5*72
    set mw ${maxw}.0
    set mh ${maxh}.0
    if {$width > $maxw || $height > $maxh} {
	if {$width > $maxw && [expr {$width/$mw > $height/$mh}]} {
	    set scale $maxw
	} else {
	    set scale [expr {$width * $mh/$height}]
	}
	# scale the image so that it fits within the page
	$canvas postscript -file $file -fontmap xleFontMap \
	    -width $width -height $height -pagewidth $scale
    } else {
	# don't scale the image
	$canvas postscript -file $file -fontmap xleFontMap \
	    -width $width -height $height
    }
    tk scaling 1.0
}

proc print-tree-as-sexp {{file "*"} {window ""}} {
    if {$window == ""} {
	set chart [default-chart]
	set window [chart-window-name $chart tree]
    }
    set tree [get-window-prop $window data]
    if {$tree == ""} {
	puts "No tree to print!"
	return
    }
    if {$file == "*"} {set file [default-filename "tree" "txt" $tree]}
    print_tree_as_sexp $tree $file
    puts "tree printed on $file."
}

proc show-prev-tree {window} {
    set edge [get-window-prop $window edge]
    if {$edge == ""} return
    set tree [get-window-prop $window data]
    if {$tree == ""} return
    set chart [get-window-prop $window chart]
    show-trees [get_next_tree $tree $edge -previous] $chart
}

proc show-next-tree {window} {
    set edge [get-window-prop $window edge]
    if {$edge == ""} return
    set tree [get-window-prop $window data]
    if {$tree == ""} return
    set chart [get-window-prop $window chart]
    show-trees [get_next_tree $tree $edge] $chart
}

proc display-dtrees {trees canvas} {
    set bg [background-color $canvas]
    set frame $canvas.frame
    frame $frame -bg $bg
    $canvas create window 0 0 -window $frame -anchor nw
    set n 1
    set maxwidth 0
    set maxheight 0
    set sentence [extract-sentence $trees 1]
    set sentence \"$sentence\"
    label $canvas.sentence -text [fixBidi $sentence] -font xletextfont -bg $bg
    pack $canvas.sentence -in $frame -anchor nw
    foreach tree $trees {
	set tcanvas $canvas.$n
	canvas $tcanvas -bg $bg -highlightthickness 0
	pack $tcanvas -in $frame -side left -anchor n
	set treeID [tree_id $tree]
	display-dtree2 $tree $tcanvas "CS $treeID"
	$tcanvas addtag all all ;# mark all items on canvas
	set bbox [$tcanvas bbox all] ;# get bounding box of items
	set width [lindex $bbox 2]
	set height [lindex $bbox 3]
	incr width 20
	$tcanvas configure -width $width -height $height
	incr maxwidth $width
	set maxheight [max $maxheight $height]
	incr n 1
	}
    set maxwidth [max $maxwidth [expr [winfo width $canvas.sentence] + 20]]
    set maxheight [expr {$maxheight + [winfo height $canvas.sentence] + 20}]
    $canvas configure -scrollregion [list 0 0 $maxwidth $maxheight]
}    

proc redisplay-dtrees {} {
    foreach child [winfo children .] {
	if {[get-window-prop $child type] != "tree"} {continue}
	set tree [get-window-prop $child data]
	set chart [get-window-prop $child chart]
	show-trees-only $tree $chart
    }
}

proc display-node-fstructure {cat id} {
    set chart [default-chart]
    set window [chart-window-name $chart tree]
    foreach button [winfo children $window.value.1] {
	set index [string last . $button]
	if {$index == -1} {continue}
	set node [string range $button [expr {$index + 1}] end]
	set edge [get_field $node edge]
	if {[get_field $edge id] != $id} {continue}
	if {[get_field $edge category] != $cat} {
	    error "Edge ${id}'s category is not $cat"
	}
	set script [bind $button <Button-2>]
	eval $script
	return
    }
}

proc display-dtree2 {address canvas treeid} {
    global showNumbers showPartials showTokens showAlternatives printing \
	    CSTRUCTUREMOTHERD CSTRUCTUREPERSONALD
    if {$showNumbers} {set numParam "-numbers"} else {set numParam ""}
    if {$showPartials} {set partParam "-partials"} else {set partParam ""}
    if {$showTokens} {set tokenParam "-tokens"} else {set tokenParam ""}
    if {$showAlternatives && !$printing} {
	set alterParam "-alternatives"
    } else {
	set alterParam ""
    }
    ;# display_dtree is a C function that displays a DTree on $canvas
    ;# it calls add-node below for each node displayed
    display_dtree $address $canvas $treeid $numParam $partParam $alterParam \
	$tokenParam -xspace $CSTRUCTUREPERSONALD -yspace $CSTRUCTUREMOTHERD
}

proc get-mother {node} {
    set mother [get_field $node mother]
    if {$mother == ""} {return $node}
    return $mother
}

proc add-node {canvas address xpos ypos label boxed surf_node} {
    global printing
    ;# called by a C function
    set self $canvas.$address
#    set self.xpos $xpos
#    set self.ypos $ypos
    if {$boxed == 3 || $boxed == 2} {
	set relief "groove"
    } elseif {$boxed == 1} {
	set relief "raised"
    } else {
	set relief "flat"
    }
    if {$surf_node == 1} {
	set usefont xletextfont
	set label [fixBidi $label]
    } else {
	set usefont xletreefont
    }
    set bd 1
    if {$relief == "groove"} {set bd 2}
    if {$relief == "flat"} {set bd 0}
    set bg [background-color $canvas]
    button $self -text $label -bd $bd -relief $relief -padx 0 -pady 0 \
	    -font $usefont -bg $bg -highlightthickness 0
    if {$boxed == "grey"} {
#	$self configure -state disabled
       $self configure -foreground grey
    }
    set mother [get-mother $address]
    set chart [get-window-prop [winfo toplevel $canvas] chart]
    bind $self <Button-1> "display-next-subtree $self $address -next"
    bind $self <Shift-Button-1> "display-next-subtree $self $address -previous"
    bind $self <Control-Button-1> "node-menu $self $address"
    bind $self <Control-Shift-Button-1> "inspect $address"
    bind $self <Button-2> "show-fstructures $address $chart"
    bind $self <Shift-Button-2> "show-fstructures $mother $chart"
    bind $self <Control-Button-2> "show-subtree-constraints $address $chart"
    bind $self <Control-Shift-Button-2> "show-subtree-constraints $mother $chart"
    ;# the following line fixes a bug in Tcl
    set window [winfo toplevel $self]
    bindtags $self "Button $self $window all"
    bind $self <Button-3> "node-menu $self $address"
    if {$printing} {
	$canvas create text $xpos $ypos -text $label -font xletreefont
    } else {
	$canvas create window  $xpos $ypos -window $self -tags node
    }
}

proc node-menu {self address} {
    set window [winfo toplevel $self]
    set chart [get-window-prop $window chart]
    set x [winfo pointerx $window]
    set y [winfo pointery $window]
    set menu .nodemenu
    if {[winfo exists $menu]} {destroy $menu}
    menu $menu
    $menu add command -label "Show Morphemes" \
	    -command "toggle-show-internals $address $chart"
    $menu add command -label "Next Subtree" \
	    -command "display-next-subtree $self $address -next"
    $menu add command -label "Prev Subtree" \
	    -command "display-next-subtree $self $address -previous"
    $menu add command -label "Show Fstructures" \
	    -command "show-fstructures $address $chart"
    $menu add command -label "Show Constraints" \
	    -command "show-subtree-constraints $address $chart"
    $menu add command -label "Show Mother Fstructures" \
	    -command "show-fstructures [get-mother $address] $chart"
    $menu add command -label "Show Mother Constraints" \
	    -command "show-subtree-constraints [get-mother $address] $chart"
    $menu add command -label "Inspect Data" \
	    -command "inspect $address"
    $menu add command -label "Show Documentation" \
	    -command "document-node"
    tk_popup $menu $x $y
}

proc document-node {} {
    puts ""
    puts "<Button-1> shows next subtree of this node."
    puts "    If there is no subtree, then the button flashes white."
    puts "<Shift-Button-1> shows previous subtree of this node."
    puts "    If there is no subtree, then the button flashes white."
    puts "<Control-Button-1> creates a pop-up menu of alternatives,"
    puts "including Show Morphology."
    puts "<Control-Shift-Button-1> inspects this node."
    puts "<Button-2> shows fstructures."
    puts "<Shift-Button-2> shows fstructures of the mother node."
    puts "<Control-Button-2> shows subtree constraints"
    puts "    (e.g. constraints introduced by this subtree)."
    puts "<Control-Shift-Button-2> shows subtree constraints of mother node."
    puts "(NB: ALT-Button-1 or OPTION-Button-1 can be used instead of Button-2.)"
    puts "Grey lines indicate that there are alternatives."
    puts "Boxed nodes indicate that there are inconsistent solutions"
    puts "(Sometimes the inconsistent solutions are in the partials)."
}

proc document-left-bracket {} {
    puts ""
    puts "<Button-1> removes all bracketing"
    puts "<Button-2> shows the categories of this constituent"
}

proc show-subtree-constraints {subtree {chart ""}} {
    if {$chart == ""} {set chart [get_field $subtree chart]}
    set window [chart-window-name $chart constraints]
    set-window-prop $window chart $chart
    set-window-prop $window type constraint
    setup-constraints-window $window
    wm title $window "constraints for [get_field $subtree label]"
    set canvas $window.value
    set constraints [print_subtree_constraints $subtree]
    set y 5
    set linespace [font metric xlefsfont -linespace]
    # we have to add a line at a time because Tcl/Tk
    # can't handle text with more than 100 lines in it
    foreach constraint [split $constraints \n] {
	$canvas create text 3 $y -text $constraint \
	    -font xlefsfont -anchor nw
	incr y $linespace
    }
    set bbox [$canvas bbox all] ;# get bounding box of items
    set width [lindex $bbox 2]
    set height [lindex $bbox 3]
    if {$width == "" || $width < 100} {set width 100}
    if {$height == "" || $height < 15} {set height 15}
    ;# leave some space so that you know you are at the bottom
    incr height 13 
    $canvas configure -width $width -height $height
    $canvas configure -scrollregion [list 0 0 $width $height]
    raise $window
}

proc setup-constraints-window {self} {
    global window_width window_height
    if {[winfo exists $self]==0} {
	toplevel $self
	wm minsize $self 200 200
	set xloc [expr {$window_width  + 20}]
	set yloc [expr {$window_height + 40}]
	while {[window-exists-at $xloc $yloc]} {
	    incr xloc 15
	    incr yloc 15
	}
	set position +${xloc}+${yloc}
	wm geometry $self ${window_height}x${window_height}${position}
	set-window-prop $self position $position
	frame $self.panel
	pack $self.panel -in $self -anchor nw
	set color [highlight-color $self]
	$self configure -highlightcolor $color -highlightbackground $color \
		-highlightthickness 1
	add-kill-button $self $self.panel
	set scrollable [scrollable-canvas $self.value $self]
	pack $scrollable -in $self -expand 1 -fill both
	wm title $self "grammar constraints"
    } else {
	clear-window $self
	focus-window $self 1
    }
}

proc display-next-subtree {self node previous} {
    update idletasks
    set next [get_next_subtree $node $previous]
    if {$next == ""} {
	$self flash
    } else {
	set top [winfo toplevel $self]
	set chart [get-window-prop $top chart]
	show-trees $next $chart
    }
}

proc tree_root {node} {
    while {1} {
	set mother [get_field $node mother]
	if {$mother == ""} {return $node}
	set node $mother
    }
}

proc toggle-show-internals {node chart} {
    set edge [get_field $node edge]
    if {[get_field $edge is_surface] == "T"} {
	set node [get_field $node mother]
    }
    toggle_show_internals $node
    set tree [tree_root $node]
    show-trees-only $tree $chart
}   

proc get-tree-morphemes {{tree ""}} {
    global defaultparser treesOnly
    if {$tree == ""} {set tree $defaultparser}
    if {[string match "(Chart)*" $tree]} {
	# extract a packed graph from a chart
	set chart $tree
	if {$treesOnly} {
	    if {[root_edge $chart] != ""} {
		set edge [root_edge $chart]
		set tree [get_next_tree (DTree)0 $edge]
	    } else {
		set tree [get_chosen_tree $graph]
	    }
	} else {
	    set tree [extract_chart_graph $tree]
	}
    }
    if {[string match "(Graph)*" $tree]} {
	# choose a tree from a packed graph
	if {$tree == "(Graph)x0"} {return ""}
	set graph $tree
	set chart [get_field $graph chart]
	if {[get-chart-prop $chart "property_weights_file"] != ""} {
	    unmark_all_choices $graph
	    select-most-probable-structure $graph
	    set tree [get_chosen_tree $graph]
	} elseif {[root_edge $chart] != ""} {
	    set edge [root_edge $chart]
	    set tree [get_next_tree (DTree)0 $edge]
	} else {
	    set tree [get_chosen_tree $graph]
	}
    }
    return [get_tree_morphemes $tree]
} 

# SHOW A CHART

proc show-chart {{chartData ""}} {
    global chart depth stateChart
    if {[string index $chartData 0] == "."} {
	set chartData [get-window-prop $chartData chart]
    }
    if {$chartData == ""} {
	set chartData [default-chart]
    }
    if {[info exists chart]} {unset chart}
    if {[info exists depth]} {unset depth}
    if {[info exists stateChart]} {unset stateChart}
    set self .chart
    set top 0
    foreach edge [get_edges $chartData] {
	set edgedepth [add-to-chart $edge]
	if {$edgedepth > $top} {
	    set top $edgedepth
	}
    }
#    add-to-state-chart [root_edge]
    if {[winfo exists $self]!=0} {
	destroy $self
	update idletasks
    }
    toplevel $self
    wm title $self "chart"
    set position [window-position $self]
    wm geometry $self 500x400$position
    set-window-prop $self chart $chartData
    set-window-prop $self type chart
    frame $self.panel
    pack $self.panel -in $self -anchor nw
    add-kill-button $self $self.panel
    button $self.statistics -text "statistics" -font xleuifont \
	    -command "chart-statistics graphs 1 $self"
    bind $self.statistics <Button-3> {
	puts ""
	puts "Prints statistics about the chart."
	print-button-command %W
    }
    button $self.morph -text "morphology" -font xleuifont \
	    -command "show-morph $self"
    bind $self.morph <Button-3> {
	puts ""
	puts "Shows the morphology window."
	print-button-command %W
    }
    pack $self.statistics $self.morph -in $self.panel -anchor nw -side left
    set scrollable [scrollable-canvas $self.canvas $self]
    pack $scrollable -in $self -expand 1 -fill both
#    foreach state [array names stateChart] {
#	set edges [lsort -command edge-order $stateChart($state)]
#	if {[llength $edges] > 3} {
#	    display-chart-row $self.canvas $state $edges}
#    }
    set y 0
    frame $self.canvas.data 
    ;# must be daughter of $self.canvas for scrolling to work properly
    while {$top > 0} {
	display-chart-row $self.canvas.data $top \
	    [lsort -command edge-order $chart($top)] $y
	incr top -1
	incr y 20
    }
    $self.canvas create window 0 0 -window $self.canvas.data \
	-anchor nw
    set-scroll-region $self.canvas $self 800x400
}

proc show-morph {{chartData ""}} {
    global chart depth stateChart
    if {[string index $chartData 0] == "."} {
	set chartData [get-window-prop $chartData chart]
    }
    if {$chartData == ""} {
	set chartData [default-chart]
    }
    if {[info exists chart]} {unset chart}
    if {[info exists depth]} {unset depth}
    if {[info exists stateChart]} {unset stateChart}
    set self .chart
    set-window-prop $self chart $chartData
    set-window-prop $self type morph
    set top 0
    set surfaceForms ""
    foreach edge [get_edges $chartData] {
	set lexical [get_field $edge lexical]
	set preterm [get_field $edge preterm]
	if {$lexical != "T" && $preterm != "T"} {continue}
	set surfacecorr [get_field $edge surface]
	if {$surfacecorr != ""} {
	    foreach surface $surfacecorr {
		if {[lsearch $surfaceForms $surface] == -1} {
		    lappend surfaceForms $surface
		}
		lappend mothers($surface) $edge
	    }
	} elseif {$lexical == "T"} {
	    if {[lsearch $surfaceForms $edge] == -1} {
		lappend surfaceForms $edge
	    }
	    
	}
	    
	foreach subtree [get_field $edge subtrees] {
	    set complete [get_field $subtree complete]
	    lappend mothers($complete) $edge
	}
    }
    
    if {[winfo exists $self]!=0} {
	destroy $self
	update idletasks
    }
    toplevel $self
    wm title $self "morphology"
    set position [window-position $self]
    wm geometry $self 500x400$position
    frame $self.panel
    pack $self.panel -in $self -anchor nw
    add-kill-button $self $self.panel
    button $self.statistics -text "statistics" -font xleuifont \
	    -command "chart-statistics graphs 1 $self"
    bind $self.statistics <Button-3> {
	puts ""
	puts "Prints statistics about the chart."
	print-button-command %W
    }
    button $self.chart -text "chart" -command "show-chart $chartData" \
	-font xleuifont
    bind $self.chart <Button-3> {
	puts ""
	puts "Shows the chart window."
	print-button-command %W
    }
    pack $self.statistics $self.chart \
	-in $self.panel -anchor nw -side left
    set scrollable [scrollable-canvas $self.canvas $self]
    pack $scrollable -in $self -expand 1 -fill both
    
    frame $self.canvas.data 
    set surfaceForms [lsort -command edge-order $surfaceForms]
    foreach surface $surfaceForms {
	display-morph-row $self.canvas.data $surface 1
    }
    
    $self.canvas create window 0 0 -window $self.canvas.data \
	-anchor nw
    set-scroll-region $self.canvas $self 800x400
}

proc display-morph-row {parent edge depth} {
    upvar mothers mothers
    if {$depth > 5} {return}
    incr depth
    set self $parent.$edge
    if {[winfo exists $self]} {return}
    frame $self
    pack $self -in $parent -anchor nw
    set button $self.button
    set label [get-label $edge]
    button $button -text [fixBidi $label] -font xletextfont
    inspect-button $button $edge
    ;# the following is needed to block more specific bindings
    bind $button <Button-1> "" 
    bind $button <Button-2> "show-sample-tree $edge"
    set graph [get_field $edge graph]
    bind $button <Control-Shift-Button-1> "inspect $edge 0 .chart"
    bind $button <Button-3> {
	puts ""
	puts "<Button-1> is reserved."
	puts "<Button-2> shows a sample tree."
	puts "<Control-Shift-Button-1> inspects edge."
    }
    pack $button -in $self -side left -anchor nw
    
    if {[info exists mothers($edge)]} {
	foreach medge [lsort -command edge-order $mothers($edge)] {
	    if {$medge == $edge} {continue}
	    display-morph-row $self $medge $depth
	}
    }
}

proc inspect-button {button address} {
}

proc find-subtree-graphs {target {type "any"}} {
    global defaultparser defaultgenerator
    set chart $defaultparser
    set graphs ""
    foreach edge [get_edges $chart] {
	set cat [get_field $edge category]
	foreach subtree [get_field $edge subtrees] {
	    set graph [get_field $subtree graph]
	    if {$graph == "" || $graph == "BadGraph"} {
		continue
	    }
	    if {$type == "nogoods"} {
		set nogood  [get_field $graph "nogood"]
		if {$nogood == ""} {continue}
	    }
	    set right [get_field $subtree complete]
	    set rightcat [get_field $right category]
	    set label ${cat}/${rightcat}
	    if {$label != $target} {continue}
	    lappend graphs $graph
	}
    }
    return $graphs
}

proc chart-statistics {{type "graphs"} {cutoff 1} {chart ""}} {
    global treesOnly
    if {[string index $chart 0] == "."} {
	set chart [get-window-prop $chart chart]
    }
    if {$chart == ""} {set chart [default-chart]}
    if {$treesOnly} {set type "all"}
    if {$type != "graphs" && $type != "nogoods" && \
	    $type != "all" && $type != "markedgraphs"} {
	puts "unknown type: $type"
	puts "should be one of graphs, nogoods, or all"
	return
    }
    set catstotal 0
    set labelstotal 0
    foreach edge [get_edges $chart] {
	set cat [get_field $edge category]
	set edgedone 0
	set state [get_field $edge state]
	if {$state != ""} {set edgedone 1}
	foreach subtree [get_field $edge subtrees] {
	    if {$type != "all"} {
		set graph [get_field $subtree graph]
		if {$graph == "" || $graph == "BadGraph"} {
		    continue
		}
		set nogood  [get_field $graph "nogood"]
		if {$type == "nogoods" && $nogood == ""} {
		    continue
		}
		set mark [get_field $graph "mark"]
		if {$type == "markedgraphs" && $mark == ""} {
		    continue
		}
	    }
	    set right [get_field $subtree complete]
	    set lexical [get_field $right lexical]
	    if {$lexical == "T"} {continue}
	    set rightcat [get_field $right category]
	    set line [get_field $subtree line]
	    set line [get_field $subtree line]
	    set label ${cat}/${rightcat}(line:$line)
	    if {![info exists cats($cat)]} {
		set cats($cat) 0
	    }
	    if {![info exists labels($label)]} {
		set labels($label) 0
	    }
	    incr labels($label) 1
	    incr labelstotal 1
	    if {!$edgedone} {
		incr cats($cat) 1
		incr catstotal 1
		set edgedone 1
	    }
	}
    }
    puts ""
    puts "CATEGORY STATISTICS: ($catstotal total)"
    print-chart-statistics cats $cutoff
    puts ""
    puts "DAUGHTER STATISTICS: ($labelstotal total)"
    print-chart-statistics labels $cutoff
    puts ""
    puts "NB: You can use 'cat-ranges <category>' to find edges."
}

proc print-chart-statistics {name cutoff} {
    upvar $name statistics
    if {![info exists statistics]} {return}
    set sid [array startsearch statistics]
    while {[array anymore statistics $sid]} {
	set next [array nextelement statistics $sid]
	set nextvalue $statistics($next)
	lappend list [concat $next $nextvalue]
    }
    set list [lsort -command statistics-order $list]
    foreach item $list {
	set count [lindex $item 1]
	if {$count < $cutoff} {break}
	puts "[lindex $item 0] [lindex $item 1]"
    }
}

proc statistics-order {a b} {
    set count1 [lindex $a 1]
    set count2 [lindex $b 1]
    if {$count1 == $count2} {return 0}
    if {$count1 > $count2} {return -1}
    return 1
}

proc cat-ranges {target {type "graphs"} {showSubtreeCounts "0"}} {
    global defaultchart treesOnly
    set chart $defaultchart
    if {$treesOnly} {set type "all"}
    set mode [get_field $chart mode]
    if {$type != "graphs" && $type != "markedgraphs" && $type != "all"} {
	puts "unknown type: $type"
	puts "should be one of graphs or all"
	return
    }
    set ranges ""
    set totalEdges 0
    set totalSubtrees 0
    foreach edge [get_edges $chart] {
	set cat [get_field $edge category]
	if {$cat != $target} {continue}
	set state [get_field $edge state]
	# skip partial edges
	if {$state != "" && $mode != "ChartGeneration"} {continue}
	set good 0
	set has_graph 0
	set subtrees 0
	foreach subtree [get_field $edge subtrees] {
	    set graph [get_field $subtree graph]
	    if {($graph == "" || $graph == "BadGraph")} {
		if {$type != "all"} {continue}
		incr subtrees
		incr totalSubtrees
		continue
	    }
	    set mark [get_field $graph mark]
	    if {$type == "markedgraphs" && $mark != 1} {
		continue
	    }
	    set has_graph 1
	    incr subtrees
	    incr totalSubtrees
	    set nogood  [get_field $graph "nogood"]
	    if {$nogood == ""} {
		set good 1
	    }
	}
	if {$type != "all" && !$has_graph} {continue}
	
	set right [get_field $edge right_vertex]
	set vertex [get_field $edge vertex]
	set left [get_field $vertex index]
	set range [list $left $right $good $subtrees]
	lappend ranges $range
	incr totalEdges
    }
    set ranges [lsort -command range-order $ranges]
    set lastleft ""
    set lastright ""
    set first 1
    foreach range $ranges {
	if {[lindex $range 0] != $lastleft} {
	    set lastleft [lindex $range 0]
	    if {!$first} {puts ""}
	    set first 0
	    puts -nonewline "$lastleft: "
	    set lastright ""
	}
	if {[lindex $range 1] == $lastright} {continue}
	set lastright [lindex $range 1]
	if {[lindex $range 2]} {
	    set star ""
	} else {
	    set star "*"
	}
	set subtrees [lindex $range 3]
	if {$showSubtreeCounts} {
	    puts -nonewline "${lastright}${star}($subtrees) "
	} else {
	    puts -nonewline "${lastright}${star} "
	}
    }
    puts ""
    puts "total edges = $totalEdges"
    puts "total subtrees = $totalSubtrees"
    puts ""
    puts "NB: You can use 'show-cat <cat> <left> <right>' to display an edge."
}

proc range-order {a b} {
    set a0 [lindex $a 0]
    set b0 [lindex $b 0]
    if {$a0 < $b0} {return -1}
    if {$a0 > $b0} {return 1}
    set a1 [lindex $a 1]
    set b1 [lindex $b 1]
    if {$a1 < $b1} {return -1}
    if {$a1 > $b1} {return 1}
    set a2 [lindex $a 2]
    set b2 [lindex $b 2]
    if {$a2 < $b2} {return 1}
    if {$b2 < $a2} {return -1}
    return 0
}

proc find-edges {cat {left ""} {right ""}} {
    global defaultchart
    set chart $defaultchart
    set edges ""
    foreach edge [get_edges $chart] {
	if {[get_field $edge source] == "T"} {continue}
	set edgecat [get_field $edge category]
	if {$edgecat != $cat} {continue}
	set graph [get_field $edge graph]
	if {$graph == "" || $graph == "BadGraph"} {continue}
	set edgeright [get_field $edge right_vertex]
	set vertex [get_field $edge vertex]
	set edgeleft [get_field $vertex index]
	if {$left != "" && $left != $edgeleft} {continue}
	if {$right != "" && $right != $edgeright} {continue}
	lappend edges $edge
    }
    return $edges
}

proc set-scroll-region {canvas toplevel geometry
                        {minx 200} {miny 200}} {
    ;# set the scroll region of the canvas
    wm withdraw $toplevel
    update idletasks;# force positions to be calculated
    $canvas addtag all all ;# mark all items on canvas
    set bbox [$canvas bbox all] ;# get bounding box of items
    $canvas configure -scrollregion $bbox
    ;# determine the maximum size for toplevel based on the scroll region
    ;# save original width and height
    set origwidth [winfo width $toplevel]
    set origheight [winfo height $toplevel]
    wm geometry $toplevel ""
    ;# set canvas to maximum scroll region
    $canvas config -width [lindex $bbox 2] -height [lindex $bbox 3]
    update idletasks
    ;# set maxsize to resulting geometry
    set size [split [wm geometry $toplevel] x+-]
    wm maxsize $toplevel [lindex $size 0] [lindex $size 1]
    ;# restore canvas width and height
    $canvas config -width $origwidth -height $origheight
    ;# set geometry of toplevel before showing result on screen
    set init [split $geometry x+-]
    wm geometry $toplevel [join [list \
				     [min [lindex $size 0] [lindex $init 0]] \
				     [min [lindex $size 1] [lindex $init 1]] \
				    ] "x" ]
    wm minsize $toplevel $minx $miny
    wm deiconify $toplevel
}

proc reset-scroll-region {toplevel geometry} {
    ;# set the scroll region of the canvas
    update idletasks;# force positions to be calculated
    $toplevel.canvas addtag all all ;# mark all items on canvas
    set bbox [$toplevel.canvas bbox all] ;# get bounding box of items
    $toplevel.canvas configure -scrollregion $bbox
    ;# determine the maximum size for toplevel based on the scroll region
    ;# save original width and height
    set origwidth [winfo width $toplevel]
    set origheight [winfo height $toplevel]
    wm geometry $toplevel ""
    ;# set canvas to maximum scroll region
    $toplevel.canvas config -width [lindex $bbox 2] -height [lindex $bbox 3]
    update idletasks
    ;# set maxsize to resulting geometry
    set size [split [wm geometry $toplevel] x+-]
    wm maxsize $toplevel [lindex $size 0] [lindex $size 1]
    ;# restore canvas width and height
    $toplevel.canvas config -width $origwidth -height $origheight
    ;# set geometry of toplevel before showing result on screen
    set init [split $geometry x+-]
    wm geometry $toplevel [join [list \
				     [min [lindex $size 0] [lindex $init 0]] \
				     [min [lindex $size 1] [lindex $init 1]] \
				    ] "x" ]
    raise $toplevel
}

proc max {a b} {
    if {$a > $b} {return $a} else {return $b}
}

proc min {a b} {
    if {$a < $b} {return $a} else {return $b}
}

set edges-must-have-graphs 0

proc display-chart-row {parent row edges y} {
    global edges-must-have-graphs
    set self $parent.$row
    frame $self
    pack $self  -in $parent
    foreach edge $edges {
	set hasgraph 0
	foreach subtree [get_field $edge subtrees] {
	    set graph [get_field $subtree graph]
	    if {$graph != "" && $graph != "(Graph)1"} {
		set hasgraph 1
	    }
	}
	if {$hasgraph == 0 && ${edges-must-have-graphs}} {continue}
	set label [get-label $edge]
	button $parent.$edge -text [fixBidi $label] -font xletextfont
	inspect-button $parent.$edge $edge
	;# the following is needed to block more specific bindings
	bind $parent.$edge <Button-1> "" 
	bind $parent.$edge <Button-2> "show-sample-tree $edge"
	set graph [get_field $edge graph]
	bind $parent.$edge <Control-Shift-Button-1> "inspect $edge 0 .chart"
	bind $parent.$edge <Button-3> {
	    puts ""
	    puts "<Button-1> is reserved."
	    puts "<Button-2> shows a sample tree."
	    puts "<Control-Shift-Button-1> inspects edge."
	}
	pack $parent.$edge -in $self -side left
    }
}

proc add-to-chart {edge} {
    global chart depth
    if {[get_field $edge source] == "T"} {return 0}
    if {[info exists depth($edge)]} {return $depth($edge)}
    set depth($edge) 0
    set mdepth 0
    foreach subtree [get_field $edge subtrees] {
	set partial [get_field $subtree partial]
	set complete [get_field $subtree complete]
	set ddepth 1
	if {$partial != "" && [get_field $partial source] != "T"} {
	    if {[get_field $partial cyclic] != "T"} {
		set ddepth [add-to-chart $partial]
		if {$ddepth > $mdepth} {set mdepth $ddepth}
	    } else {
		# - - - - - - - - - - - - - - - - - - - - -
		# THIS IS COMPLETELY ARBITRARY (AND WRONG!)
		# - - - - - - - - - - - - - - - - - - - - -
		set ddepth 0
	    }
	}
	if {$complete != ""} {
	    if {[get_field $complete cyclic] != "T"} {
		set ddepth [add-to-chart $complete]
		if {$ddepth > $mdepth} {set mdepth $ddepth}
	    }
	}
    }
    incr mdepth
    lappend chart($mdepth) $edge
    set depth($edge) $mdepth
    return $mdepth
}

proc edge-order {edge1 edge2} {
    set from1 [get_field $edge1 from]
    set from2 [get_field $edge2 from]
    set to1 [get_field $edge1 to]
    set to2 [get_field $edge2 to]
    if {$from1 < $from2} {return -1}
    if {$from1 > $from2} {return 1}
    if {$to1 < $to2} {return 1}
    if {$to1 > $to2} {return -1}
    return 0
#    set pos1 [expr {[get_field $edge1 from] + [get_field $edge1 to]}]
#    set pos2 [expr {[get_field $edge2 from] + [get_field $edge2 to]}]
#    if {$pos1 == $pos2} {return 0}
#    if {$pos1 < $pos2} {return -1}
#    return 1
}
    
proc add-to-state-chart {edge} {
    global stateChart edgeDone
    if {[info exists edgeDone($edge)]} {return}
    set edgeDone($edge) 1
    foreach subtree [get_field $edge subtrees] {
	set partial [get_field $subtree partial]
	set complete [get_field $subtree complete]
	if {$partial != "" && [get-field $partial source] != "T"} {
	    add-to-state-chart $partial}
	if {$complete != ""} {add-to-state-chart $complete}
    }
    if {[get_field $edge to] > [get_field $edge from]} {
	set state [get_field $edge state]
	lappend stateChart($state) $edge}
}


proc show-chart-nav {{chartData ""}} {
    if {[string index $chartData 0] == "."} {
	set chartData [get-window-prop $chartData chart]
    }
    if {$chartData == ""} {
	    set chartData [default-chart]
	}
    if {[get_field $chartData mode] == "ChartGeneration"} {
	show-gen-vertices-window $chartData
	return
    }
    set chartPos [init_chart_nav $chartData]
    show-bracket-window $chartPos $chartData
}

proc get-word {address {root ""}} {
    set label [get_field $address *word*]
    if {$label == ""} {set label $address}
    return $label
}


proc show-bracket-window {chartPos chartData } {
    set self .bracket
    if {[winfo exists $self]} {destroy $self}
    toplevel $self
    set-window-prop $self chart $chartData
    set-window-prop $self type bracket
    wm title $self "bracketing"
    wm geometry $self +440+55
    set bg [background-color $self]
    frame $self.panel
    pack $self.panel -in $self -anchor nw
    add-kill-button $self $self.panel
    button $self.clear -text "clear" -font xleuifont \
	    -command "unprune $self $chartData"
    bind $self.clear <Button-3> {
	puts ""
	puts "Clear all bracketings."}
    pack $self.clear -in $self.panel -anchor nw -side left
#    frame $self.data 
#    set posFrame [make-pos-frames $self.data $chartPos "***"]
#    pack $self.data -in $self -expand 1 -fill both
    set scrollable [scrollable-canvas $self.canvas $self]
    pack $scrollable -in $self -expand 1 -fill both
    frame $self.canvas.data -bg $bg
    set posFrame [make-pos-frames $self.canvas.data $chartPos "***"]
    $self.canvas create window 0 0 -window $self.canvas.data \
	-anchor nw
#   set-brwin-scroll-region $self.canvas $self 800x400 200 80 300
    set-scroll-region $self.canvas $self 800x400 200 80
}

proc killBracketWin {brWin {chartData ""}} {
    if {$chartData != ""} {unprune_chart $chartData}
    destroy $brWin
}

proc make-pos-frames {parent chartPos endChartPos} {
    global brText
    set borderwidth 0
    set bg [background-color $parent]
    while {$chartPos != "" } {
        if {$chartPos == $endChartPos} {
            return
        }
	# Create a button for the chart position
        set pos [get_field $chartPos position]
        frame $parent.$pos -relief groove -borderwidth 0
        button $parent.$pos.pos -text \# -font xletextfont \
	    -relief flat -bd 0 -bg $bg -highlightthickness 0
        bind $parent.$pos.pos <Button-1> "select-bracket $chartPos \
                               $parent.$pos $parent.$pos.pos $pos"
        bind $parent.$pos.pos <Control-Button-1> "select-debracket $chartPos \
                               $parent.$pos $parent.$pos.pos $pos"
        bind $parent.$pos.pos <Control-Shift-Button-1> \
	    "inspect $chartPos 0 [get-toplevel $parent.$pos.pos]"
        bind $parent.$pos.pos <Button-3> {
	    puts ""
            puts "<Button-1> sets a bracket.  Setting a pair of brackets " 
            puts "    restricts the tree display to trees in which " 
            puts "    bracketed material is a constituent."
            puts "<Control-Button-1> sets a \"debracket\".  Setting a pair of "
            puts "    debrackets restricts the tree display to trees in which "
            puts "    debracketed material is NOT a constituent."
         }
        pack $parent.$pos.pos -in $parent.$pos
        pack $parent.$pos -in $parent -side left

	# Create the surface form(s) that follow, returning the first pinch point
        set sform [get_field $chartPos follSforms]
        set chartPos ""
        if {$sform != "" } {
           frame $parent.$sform
            set chartPos [make-sform-frames $parent.$sform $sform]
            pack $parent.$sform -in $parent -side left
        }
    }
}
    
proc make-sform-frames {parent sform} {
    set sform2 $sform
    set maxRPos 0
    set maxRPosptr ""
    set borderwidth 0
    set multiple [get_field $sform leftNext]
    if {$multiple != ""} {
        set borderwidth 1
    }
    # find the right pos of the longest surface form
   while {$sform2 != "" } {
      set rposptr [get_field $sform2 rightPos]
       set rpos [get_field $rposptr position]
        if {$rpos > $maxRPos} {
            set maxRPos $rpos
            set maxRPosptr $rposptr
        }
        set sform2 [get_field $sform2 leftNext]
    }
    if {![pinch-point $sform $maxRPos]} {
	# broaden the search for the pinch point
	set maxRPos 0
	set maxRPosptr ""
	foreach rposptr [gather-right-pos-ptrs $sform] {
	    set rpos [get_field $rposptr position]
	    if {![pinch-point $sform $rpos]} {continue}
	    if {$maxRPosptr == "" || $rpos < $maxRPos} {
		set maxRPos $rpos
		set maxRPosptr $rposptr
	    }
	}
    }
    while {$sform != "" } {
        set edge [get_field $sform Edge]
        set word [get-word $edge]
        set rposptr [get_field $sform rightPos]
       set next [get_field $sform leftNext]

        frame $parent.$sform -relief groove -borderwidth $borderwidth
        set $parent.$sform.edge $edge
        set bg [background-color $parent]
        button $parent.$sform.word -text [fixBidi $word] \
	    -font xletextfont -relief flat -bd 0 -bg $bg \
	    -highlightthickness 0
        inspect-button $parent.$sform.word $sform
        bind $parent.$sform.word <Button-1> "do-nothing"
        if {$rposptr != $maxRPosptr} {
            pack $parent.$sform.word -in $parent.$sform -side left -anchor e
            make-pos-frames $parent.$sform $rposptr $maxRPosptr
	} else {
            pack $parent.$sform.word -in $parent.$sform -anchor center
	}
        pack $parent.$sform -in $parent -side top -fill both
        set sform [get_field $sform leftNext]
    }
    return $maxRPosptr
}

proc gather-right-pos-ptrs {sforms} {
    set sform $sforms
    set ptrs ""
    while {$sform != ""} {
	set rposptr [get_field $sform rightPos]
	set rpos [get_field $rposptr position]
	set ptrs [union-lists $ptrs $rposptr]
	set temp [gather-right-pos-ptrs [get_field $rposptr follSforms]]
	set ptrs [union-lists $ptrs $temp]
	set sform [get_field $sform leftNext]
    }
    return $ptrs
}
	
proc union-lists {list1 list2} {
    foreach item $list2 {
	if {[lsearch -exact $list1 $item] != -1} {continue}
	lappend list1 $item
    }
    return $list1
}

proc pinch-point {sforms rpos} {
    # See if rpos is a pinch point for sforms
    set sform2 $sforms
    while {$sform2 != ""} {
	set rposptr [get_field $sform2 rightPos]
	set rpos2 [get_field $rposptr position]
	if {$rpos2 > $rpos} {
	    return 0
	}
	if {$rpos2 < $rpos} {
	    if {![pinch-point [get_field $rposptr follSforms] $rpos]} {
		return 0
	    }
	}
        set sform2 [get_field $sform2 leftNext]
    }
    return 1
}

proc do-nothing {} {
}

proc unbutton {button} {
    pack forget $button
    update idletasks
    # destroy $button
}

proc color {button} {
    set color [lindex [$button configure -activebackground] 4]
    $button configure -background $color
}

proc uncolor {button} {
    set color [lindex [$button configure -background] 3]
    $button configure -background $color
    $button configure -background #AE00B200C300
}

set bracketedChartPos ""
set bracketedFrame ""
set bracketedPosButton ""
set bracketedPos ""

set debracketedChartPos ""
set debracketedFrame ""
set debracketedPosButton ""
set debracketedPos ""

#set bracketFont "-adobe-courier-bold-r-normal--*-180-*-*-*-*-*-*"
set bracketFont xletextfont

proc set-selected-bracket {chartPos frame button pos} {
    global bracketedChartPos bracketedFrame bracketedPosButton bracketedPos
    set bracketedChartPos $chartPos
    set bracketedFrame $frame
    set bracketedPosButton $button
    set bracketedPos $pos
    color $bracketedPosButton
}

proc set-selected-debracket {chartPos frame button pos} {
    global debracketedChartPos debracketedFrame 
    global debracketedPosButton debracketedPos
    set debracketedChartPos $chartPos
    set debracketedFrame $frame
    set debracketedPosButton $button
    set deracketedPos $pos
    color $debracketedPosButton
}

proc clear-selected-bracket {} {
    global bracketedChartPos bracketedFrame bracketedPosButton bracketedPos
    uncolor $bracketedPosButton
    set bracketedChartPos ""
    set bracketedFrame ""
    set bracketedPosButton ""
    set bracketedPos ""
}

proc clear-selected-debracket {} {
    global debracketedChartPos debracketedFrame
    global debracketedPosButton debracketedPos
    uncolor $debracketedPosButton
    set debracketedChartPos ""
    set debracketedFrame ""
    set debracketedPosButton ""
    set debracketedPos ""
}

proc select-debracket {chartPos frame button pos} {
    global debracketedChartPos debracketedFrame 
    global debracketedPosButton debracketedPos 
    if {$debracketedChartPos == "" } {
      set-selected-debracket $chartPos $frame $button $pos
      return
    }
    if {$debracketedChartPos == "" } {
      set-selected-debracket $chartPos $frame $button $pos
      return
    }
    if {$chartPos == $debracketedChartPos} {
      clear-selected-debracket
      return
    } 
    if {$pos > $debracketedPos} {
      do_bracket $debracketedChartPos $debracketedFrame $debracketedPosButton \
                 $debracketedPos $chartPos $frame $button $pos debracket
    } else {
      do_bracket $chartPos $frame $button $pos $debracketedChartPos \
                 $debracketedFrame $debracketedPosButton $debracketedPos \
                 debracket
    }
    clear-selected-debracket
}

proc select-bracket {chartPos frame button pos} {
    global bracketedChartPos bracketedFrame bracketedPosButton bracketedPos 
    if {$bracketedChartPos == "" } {
      set-selected-bracket $chartPos $frame $button $pos
      return
    }
    if {$chartPos == $bracketedChartPos} {
      clear-selected-bracket
      return
    }
    if {$pos > $bracketedPos} {
      do_bracket $bracketedChartPos $bracketedFrame $bracketedPosButton \
                 $bracketedPos $chartPos $frame $button $pos bracket
    } else {
      do_bracket $chartPos $frame $button $pos $bracketedChartPos\
                 $bracketedFrame $bracketedPosButton $bracketedPos bracket
    }
    clear-selected-bracket
}

proc do_bracket {leftChartPos  leftFrame  leftPosButton  leftPos \
		 rightChartPos rightFrame rightPosButton rightPos bracketing} {
    global bracketFont
    global defaultparser
    # when there are multiple brackets at the same position, the
    # buttons should be ordered so brackets don't cross.
    # To be done.
    
    set leftBrBut  $leftFrame.lbr$leftPos$rightPos
    set rightBrBut $rightFrame.rbr$rightPos$leftPos
    if {[winfo exists $leftBrBut]} {destroy $leftBrBut}		     
    if {[winfo exists $rightBrBut]} {destroy $rightBrBut}		     

    set theBracket [bracket_debracket $leftChartPos  $leftBrBut \
			$rightChartPos $rightBrBut $bracketing]
    if {$bracketing == "bracket"} {
	button $leftBrBut -text \[ -font $bracketFont -relief flat -bd 0
	button $rightBrBut -text \] -font $bracketFont -relief flat -bd 0
	bind $leftBrBut <Button-1> "categoryMenu $theBracket $leftBrBut"
	bind $leftBrBut <Button-3> {
	    puts ""
	    puts "<Button-1> displays a menu of categories that span this"
	    puts "           bracketing.  Selecting among them affects the" 
	    puts "           constraints on the tree display." 
	    puts "<Control-Button-1> deletes this bracketing." 
	}
	bind $rightBrBut <Button-1> "categoryMenu $theBracket $rightBrBut"
	bind $rightBrBut <Button-3> {
	    puts ""
	    puts "<Button-1> displays a menu of categories that span this"
	    puts "           bracketing.  Selecting among them affects the" 
	    puts "           constraints on the tree display." 
	    puts "<Control-Button-1> deletes this bracketing." 
	}
    } else {
        button $leftBrBut -text !\[ -font $bracketFont -relief flat -bd 0
        button $rightBrBut -text !\] -font $bracketFont -relief flat -bd 0
	bind $leftBrBut <Button-3> {
	    puts ""
	    puts "<Control-Button-1> deletes this bracketing." 
	}
	bind $rightBrBut <Button-3> {
	    puts ""
	    puts "<Control-Button-1> deletes this bracketing." 
	} 
    }

    bind $leftBrBut  <Control-Button-1> "unbracket $theBracket"
    bind $rightBrBut <Control-Button-1> "unbracket $theBracket"
    bind $leftBrBut  <Enter> "highlight   $leftBrBut $rightBrBut"
    bind $rightBrBut <Enter> "highlight   $leftBrBut $rightBrBut" 
    bind $leftBrBut  <Leave> "unhighlight $leftBrBut $rightBrBut" 
    bind $rightBrBut <Leave> "unhighlight $leftBrBut $rightBrBut"  

    pack $leftBrBut -in $leftFrame -before $leftPosButton -side right
    pack $rightBrBut -in $rightFrame -before $rightPosButton -side left

    show-solutions $defaultparser
    set chart [chart-from-chartpos $leftChartPos]
    reset-scroll-region .bracket 800x400
    show-bracketed-tree-count $chart
}

proc highlight  {butn1 butn2} {
    color $butn1
    color $butn2
}

proc unhighlight  {butn1 butn2} {
    uncolor $butn1
    uncolor $butn2
}

proc chart-from-chartpos {chartPos} {
    set sf [get_field $chartPos follSforms]
    set edge [get_field $sf Edge]
    set chart [get_field $edge chart]
    return $chart
}

proc show-bracketed-tree-count {chart} {
    clear_all_edge_counts $chart
    show-tree-count $chart
}

proc recode-periods {text} {
    set first [string first "." $text]
    if {$first < 0} {return $text}
    set prefix [string range $text 0 [expr {$first - 1}]]
    set suffix [string range $text [expr {$first +1}] end]
    set suffix [recode-periods $suffix]
    return ${prefix}_period_${suffix}
}

proc categoryMenu {bracket butn} {
    set self $butn.catMenu
    toplevel $self
    wm geometry $self +440+130
    setSpanningMenu $bracket
    set catList [get_field $bracket categories]
    set cnt 0
    set spanning [get_field $bracket spanning]
    while {$catList != ""} {
        set text [get_field $catList category]
	set cat [recode-periods $text]
        global $bracket.$cat.status
	set $bracket.$cat.status [get_field $catList status]
        frame $self.cat$cat
        label $self.cat$cat.name -text $text -anchor w
        radiobutton $self.cat$cat.in -text in \
	    -variable $bracket.$cat.status -value in -anchor w
        radiobutton $self.cat$cat.out -text out \
	    -variable $bracket.$cat.status -value out -anchor w
        radiobutton $self.cat$cat.dontcare -text ? \
	    -variable $bracket.$cat.status -value ? -anchor w
	foreach edge $spanning {
	    if {[get_field $edge category] != $text} {continue}
	    if {[get_field $edge refined_edges] != ""} {continue}
	    if {[winfo exists $self.cat$cat$edge]} {continue}
	    set id [get_field $edge id]
	    button $self.cat$cat$edge -text "show" \
		-command "show-subtree ${id}:1"
	    bind $self.cat$cat$edge <Button-3> {
		puts ""
		puts "Shows a tree for this edge."
	    }
	    pack $self.cat$cat$edge -in $self.cat$cat -side right
	}
        pack $self.cat$cat.dontcare $self.cat$cat.out $self.cat$cat.in \
            $self.cat$cat.name -side right -in $self.cat$cat -fill x
        pack $self.cat$cat -side top -in $self -fill x
        set pair [list $catList $bracket.$cat.status]
        lappend cats $pair
        set catList [get_field $catList next]
    }
    set chart [chart-from-chartpos [get_field $bracket left]]
    button $self.apply -text Apply -command "applyMenu $self $chart $cats"
    button $self.cancel -text cancel -command "destroy $self"
    bind $self.apply <Button-3> {
	puts ""
	puts "Applies the constraints shown."
    }
    bind $self.cancel <Button-3> {
	puts ""
	puts "Cancels the current session without making any changes."
    }
    pack $self.apply $self.cancel -side top -in $self
}

proc applyMenu {menu chart args} {
    global defaultchart
    set len [llength $args]
    while {$len > 0} {
	set len [expr {$len - 1}]
	set pair [lindex $args 0]
	set args [lreplace $args 0 0]
	set cat [lindex $pair 0]
	set statusVar [lindex $pair 1]
        global $statusVar
	set status [set $statusVar]
        set_cat_status $cat $status
    }
    show-solutions $chart
    show-bracketed-tree-count $chart
    destroy $menu
}

proc unbracket {bracket} {
    global defaultparser
    set lButton [get_field $bracket leftButton]
    set rButton [get_field $bracket rightButton]
    set leftChartPos [get_field $bracket left]
    uninstantiate_bracketing $bracket
    set chart [chart-from-chartpos $leftChartPos]
    show-solutions $chart
    if {[winfo exists .bracket.panel.treeCnt]} {
	destroy .bracket.panel.treeCnt
    }
    show-bracketed-tree-count $chart
    unbutton $lButton
    unbutton $rButton
    raise .bracket
}

proc unspan {chart rPos parent} {
    global lPos defaultparser $parent.righttext
    set $parent.righttext !\]
    inconsistent_with $chart $lPos $rPos
    show-solutions $defaultparser
}

proc unprune {nav {chart ""}} {
    global defaultchart
    if {$chart != ""} {
	unprune_chart $chart
	clear_all_edge_counts $chart
	show-solutions $chart
    }
    destroy $nav
    if {$chart != ""} {
	show-chart-nav $chart
    }
}

proc prune-edge {Dtree} {
    global defaultparser
    set edge [get_field $Dtree edge]
    set chart  [get_field $edge chart]
    prune_edge $edge
    show-solutions $defaultparser
    if {[winfo exists .bracket]} {
	show-bracketed-tree-count $chart
    }   
}

proc get-label {address {root ""}} {
    set label ""
    switch [type $address] {
        AVPair {set label [get-avpair-label $address $root]}
	Clause {
	    set label [truncate [print-clause $address $root]]
	}
    }
    if {$label == ""} {set label [get_field $address *label*]}
    if {$label == ""} {set label $address}
    return $label
}

proc type {address} {
    string range $address \
	[expr {[string first ( $address]+1}] \
	     [expr {[string first ) $address]-1}]
}
       
# SUPPORT FOR WINDOWS

proc get-toplevel {window} {
# get the name of the top-level window
    set tail [string range $window 1 end]
    set top [string range $window 0 [string first . $tail]]
    if {$top == ""} {set top [string range $window 0 end]}
    return $top
}

proc focus-window {window {noflash ""}} {
#    global olwm
    wm deiconify $window
#    if {$olwm == 0} {raise $window}
    if {$noflash == ""} {$window.panel.recompute flash}
}

proc setscroll {self f1 f2} {
# set the scrollbar, then wait for displays to finish
    $self set $f1 $f2
    update idletasks
}

proc scrollable-canvas {canvas parent} {
# add vertical and horizontal scrollbars to canvas
    set self $parent
    frame $self.frame ;# must be created before slaves!
    scrollbar $self.ybar -command "$canvas yview"
    scrollbar $self.xbar -command "$canvas xview" -orient horizontal
    set bg [background-color $parent]
    canvas $canvas -bg $bg -highlightthickness 0
    $canvas configure -xscrollcommand "setscroll $self.xbar"
    $canvas configure -yscrollcommand "setscroll $self.ybar"
    pack $self.ybar -in $self.frame -side left -fill y
    pack $self.xbar -in $self.frame -side bottom -fill x
    pack $canvas -in $self.frame -side top -anchor nw \
	-expand 1 -fill both
    return $self.frame
}

proc x-scrollable-canvas {canvas parent} {
# add horizontal scrollbar to canvas
    set self $parent
    frame $self.frame ;# must be created before slaves!
    scrollbar $self.xbar -command "$canvas xview" -orient horizontal
    canvas $canvas -bg lightgrey
    $canvas configure -xscrollcommand "setscroll $self.xbar"
    pack $self.xbar -in $self.frame -side bottom -fill x
    pack $canvas -in $self.frame -side top -anchor nw \
	-expand 1 -fill both
    return $self.frame
}

proc y-scrollable-canvas {canvas parent} {
# add vertical scrollbar to canvas
    set self $parent
    frame $self.frame ;# must be created before slaves!
    scrollbar $self.ybar -command "$canvas yview"
    canvas $canvas
    $canvas configure -yscrollcommand "setscroll $self.ybar"
    pack $self.ybar -in $self.frame -side left -fill y
    pack $canvas -in $self.frame -side top -anchor nw \
	-expand 1 -fill both
    return $self.frame
}

proc resize-canvas {canvas} {
    set window [winfo toplevel $canvas]
    set geometry [get-window-prop $window orig-geometry]
    if {$geometry != ""} {
	# Restore original geometry
	set-window-prop $window orig-geometry ""
	wm geometry $window $geometry
	return
    }
    # Get the canvas' actual size
    set cwidth [lindex [$canvas bbox all] 2]
    set cheight [lindex [$canvas bbox all] 3]
    # Get the canvas' visible size
    set vwidth [winfo width $canvas]
    set vheight [winfo height $canvas]
    # Determine how much the window should grow
    set dw [expr {$cwidth - $vwidth}]
    set dh [expr {$cheight - $vheight}]
    # Get the window's size and placement
    set geometry [split [wm geometry $window] "x+"]
    set wwidth [lindex $geometry 0]
    set wheight [lindex $geometry 1]
    set x [lindex $geometry 2]
    set y [lindex $geometry 3]
    # Make sure that we don't violate minimum sizes
    set minw [lindex [wm minsize $window] 0]
    set minh [lindex [wm minsize $window] 1]
    if {$wwidth + $dw < $minw} {
	set dw [expr {$minw - $wwidth}]
    }
    if {$wheight + $dh < $minh} {
	set dh [expr {$minh - $wheight}]
    }
    # Make sure that we don't violate maximum sizes
    set swidth [winfo screenwidth $window]
    set sheight [winfo screenheight $window]
    incr swidth -16
    incr sheight -30
    if {$x > $swidth/3} {
	set maxw [expr {$x + $wwidth}]
    } else {
	set maxw [expr {$swidth - $x}]
    }
    if {$y > $sheight/3} {
	set maxh [expr {$y + $wheight}]
    } else {
	set maxh [expr {$sheight - $y}]
    }
    if {$wwidth + $dw > $maxw} {
	set dw [expr {$maxw - $wwidth}]
    }
    if {$wheight + $dh > $maxh} {
	set dh [expr {$maxh - $wheight}]
    }
    # Save the original geometry to make it easier to toggle
    set-window-prop $window orig-geometry [wm geometry $window]
    incr wwidth $dw
    incr wheight $dh
    # keep the windows on the screen
    if {$x > $swidth/3 && $dw > 0} {
	incr x -$dw
    }
    if {$y > $sheight/3 && $dh > 0} {
	incr y -$dh
    }
    # Resize the window
    wm geometry $window "${wwidth}x${wheight}+${x}+${y}"
    raise $window
}
