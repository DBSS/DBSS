# (c) 2002-2004 by the Palo Alto Research Center.  All rights reserved.
# (c) 1997-2001 by the Xerox Corporation.  All rights reserved.
# This is the Tcl/Tk interface to creating and using generation input.

#Functions included
#build-gen-input   fn  fmt  (lex prolog contexted)
#use-gen-input   fn (fmt)
#generate n   (n can be integer, FSinteger. Or arbitrary label if 
#              input file loaded with config
#generate-strings  n m                   m can be * 
#generate-all-strings  n m               m can be * 
#generate-from-file  filename (n (m))
#generate-all-from-file filename (n (m))
#set-gen-outputs 
#set-gen-addmode
#set-gen-adds
#mod-gen-adds
#print-gen-adds
#gentest-from-file

#
# This used to be called "generate-strings-from-file"
#
proc generate-from-file {input_file {enumerate "all"} {generator ""}} {
    reset_prolog_storage
    set fschart_graph [read_prolog_graph $input_file]
    set time [CPUtime {
	set ct [generate-from-graph $fschart_graph $enumerate $generator]
    }]
    set time [expr {$time/[clock_rate]}]
    puts "Generation took $time seconds."
    return $ct
}

proc get-default-generator {graph} {
    global defaultgenerator defaultparser
    
    # Look for a generator
    set chart [get_field $graph chart]
    set generator [get-chart-prop $chart defaultgenerator]
    if {$generator != ""} {return $generator}
    
    # Look for a grammar
    set grammar [get_field $chart grammarfile]
    if {$grammar != ""} {
	if {$defaultgenerator != ""} {
	    set defaultgrammar [get_field $defaultgenerator grammarfile]
	    if {$grammar != "" && $grammar == $defaultgrammar} {
		# Use the default generator, since it has the same grammar
		return $defaultgenerator
	    }
	}
	# Create a new generator
	set generator [create-generator $grammar]
	set-chart-prop $chart defaultgenerator $generator
	return $generator
    }
    if {$defaultgenerator != ""} {
	# Use the default generator
	return $defaultgenerator
    }
    if {$defaultparser != ""} {
	# Create a generator from the default parser.
	set grammar [get_field $defaultparser grammarfile]
	set generator [create-generator $grammar]
	set defaultgenerator $generator
	return $generator
    }
}

proc generate-from-graph {fschart_graph {enumerate "all"} {generator ""}} {
    global defaultgenerator defaultparser
    global gen_out_msgs gen_out_strings gen_out_format

    if {$fschart_graph == ""} {
	print_gen_msg $gen_out_strings "{ }\n\n"
	return 0
    }
    set count 0
    
    if {$generator == ""} {
	# Look for a generator
	set generator [get-default-generator $fschart_graph]
	if {$generator == ""} {
	    puts "Can't generate without a grammar."
	    print_gen_msg $gen_out_strings "{ }\n\n"
	    return 0 
	}
    }

    set count [count_choice_solutions $fschart_graph "all"]
    if {$count > 1 &&
	($enumerate == "first" ||
	 $enumerate == 1)} {
	# Just do the first graph
	set fschart_graph [get_next_fstructure $fschart_graph "(Graph)NULL"]
    }
#
#	Run the generator
#
    clear-all $generator
    set count [execute_generator $generator $fschart_graph $gen_out_msgs]
    if {$count == 0} {
	print_gen_msg $gen_out_msgs "\nGenerator failed.\n"
	print_gen_msg $gen_out_strings "{ }\n\n"	
    } elseif {$count == -1} {
	print_gen_msg $gen_out_msgs "\nGenerator timed out.\n"
	print_gen_msg $gen_out_strings "{ }\n\n"
    } elseif {$count == -2} {
	print_gen_msg $gen_out_msgs "\nGenerator ran out of storage.\n"
	print_gen_msg $gen_out_strings "{ }\n\n"
    } elseif {$count < -2} {
	print_gen_msg $gen_out_msgs "\nGenerator aborted.\n"
	print_gen_msg $gen_out_strings "{ }\n\n"
    } else {
#
#	    If generator succeeded, print the strings
#

	set count [print-genstrings $generator]

	if {$count == 0} {
	    print_gen_msg $gen_out_msgs "Generator didn't produce an output!\n"
	    print_gen_msg $gen_out_strings "{ }\n\n"
	}
    }
    return $count
}

proc print-genstrings {generator} {
    global gen_out_strings gen_out_msgs gen_out_format
    set selector [get-chart-prop $generator "gen_selector"]
    # puts "selector = $selector"
    if {[string first "Proc" $selector] != -1} {
	eval "$selector $generator"
	return 1
    }
    
    # print to stdout
    set stdoutFile [open_gen_output "stdout"]
    print_gen_msg $stdoutFile "\n"
    set count [print_genstrings $generator "" $stdoutFile \
		   $gen_out_msgs $gen_out_format]
    print_gen_msg $stdoutFile "\n"
    
    # print to the output file
    if {$gen_out_strings != $stdoutFile} {
	print_genstrings $generator ""  $gen_out_strings \
	    $gen_out_msgs $gen_out_format
	print_gen_msg $gen_out_strings "\n"
    }
    return $count
}

set enumerate_generations 10

proc ngramProc {generator} {
    global gen_out_msgs gen_out_format
    # write the generation strings to a file
    set pid [pid]
    set input /tmp/gen${pid}.txt
    set output /tmp/ngrams${pid}.txt
    set selector [get-chart-prop $generator "gen_selector"]
    set-chart-prop $generator "gen_selector" "allstrings"
    set file [open_gen_output $input]
    set count [print_genstrings $generator "" $file $gen_out_msgs $gen_out_format]
    close_gen_output $file
    set-chart-prop $generator "gen_selector" $selector
    # run the selector on the file
    set selector "/project/nltt-6/mt/SRILM/bin/i686/ngram -lm /project/nltt-6/mt/exp-2005feb24/lm-europarl-eng-train-all-lfg.srilm -debug 1 -ppl"
    set command "$selector $input > $output 2> /dev/null"
    # puts $command
    if {[catch {exec sh -c $command} error]} {
	    puts $error
    }
    # get the best sentence
    set best [best-ngram-sentence $output]
    set best_prob [lindex $best 1]
    set best [lindex $best 0]
    set bad ""
    if {[get_field $generator "ungrammatical"] != 0} {
	set bad "*"
    }
    set best "$bad$best"
    # print to stdout
    print-gen-output $best $generator
}

proc oldlcngramProc {generator} {
    global gen_out_msgs gen_out_format
    # write the generation strings to a file
    set pid [pid]
    set input /tmp/gen${pid}.txt
    set input2 /tmp/lcgen${pid}.txt
    set output /tmp/ngrams${pid}.txt
    set selector [get-chart-prop $generator "gen_selector"]
    set-chart-prop $generator "gen_selector" "allstrings"
    set file [open_gen_output $input]
    set count [print_genstrings $generator "" $file $gen_out_msgs $gen_out_format]
    close_gen_output $file
    set-chart-prop $generator "gen_selector" $selector
    # lower case the file
    lc-tokenize-file $input $input2
    # run the selector on the file
    # set selector "/project/nltt-6/mt/SRILM/bin/i686/ngram -lm /project/nltt-6/mt/exp-2004oct27/lm-europarl-eng-train-all-lc.srilm -debug 1 -ppl"
    set selector "/project/nltt-6/mt/SRILM/bin/i686/ngram -lm /tilde/maxwell/euparl/lm-europarl-eng-train-all-lc.srilm -debug 1 -ppl"
    set command "$selector $input2 > $output 2> /dev/null"
    # puts $command
    if {[catch {exec sh -c $command} error]} {
	    puts $error
    }
    # get the best sentence
    set best [best-ngram-sentence $output $input]
    set best_score [lindex $best 1]
    set best [lindex $best 0]
    set bad ""
    if {[get_field $generator "ungrammatical"] != 0} {
	set bad "*"
    }
    set best "$bad$best"
    set-chart-prop $generator "output" $best
    set-chart-prop $generator "ngram_score" $best_score
    print-gen-output $best $generator
}

proc lcngramProc {generator} {
    global gen_out_msgs gen_out_format gen_out_strings
    global enumerate_generations
    set root [root_edge $generator]
    if {$root == "" || $root == "(Edge)0"} {return}
    set train 0
    if {[get-chart-prop $generator "training"] == 1} {
	set train 1
    }
    set selector [get-chart-prop $generator "gen_selector"]
    set-chart-prop $generator "gen_selector" "allstrings"
    set best_score -1000000
    set best ""
    set i 0
    set max 1000
    set tree "(DTree)0"
    set file ""
    set outputs ""
    while {1} {
	set tree [get_next_tree $tree $root -goodOnly]
	if {$tree == "" || $tree == "(DTree)0"} {break}
	set gs "(Graph)0"
	while {1} {
	    set gs [get_next_fstructure $tree $gs]
	    if {$gs == ""} {break}
	    set lines [get_genstrings $generator $gs]
	    set lines [split $lines "\n"]
	    foreach string $lines {
		if {$string == ""} {continue}
		incr i
		if {$i > $max} {break}
		set lcstring [lc-tokenize $string]
		set prob [language_model $generator $lcstring]
		set word_count [count-tokens $lcstring]
		set moves [count_constituent_moves $gs]
		add_graph_feature $gs "GEN_NGRAM_SCORE" $prob VT_PDFLOAT
		add_graph_feature $gs "GEN_WORD_COUNT" $word_count VT_NUM
		add_graph_feature $gs "CONSTITUENT_MOVES" $moves VT_NUM
		set score [select_most_probable_structure $gs]
		if {!$train} {
		    if {$score > $best_score} {
			set best_score $score
			set best $string
		    }
		    continue
		}
		set data "$score ||| $string ||| $prob $word_count $moves"
		if {[lsearch -exact $outputs $data] != -1} {continue}
		lappend outputs $data
	    }
	    if {$i > $max} {break}
	}
	if {$i > $max} {break}
    }
    set-chart-prop $generator "gen_selector" $selector
    if {$train} {
	set outputs [lsort -command first-number-order $outputs]
	set i 0
	foreach output $outputs {
	    set data $output
	    set sep [string first "||| " $output]
	    set data [string range $output [expr {$sep+4}] end]
	    print_gen_msg $gen_out_strings $data $generator
	    print_gen_msg $gen_out_strings "\n" $generator
	    incr i
	    if {$i >= $enumerate_generations} {break}
	}
	return
    }
    set bad ""
    if {[get_field $generator "ungrammatical"] != 0} {
	set bad "*"
    }
    set best "$bad$best"
    set-chart-prop $generator "output" $best
    set-chart-prop $generator "ngram_score" $best_score
    print-gen-output $best $generator
}

proc first-number-order {a b} {
    set sp [string first " " $a]
    set a [string range $a 0 [expr {$sp-1}]]
    set sp [string first " " $b]
    set b [string range $b 0 [expr {$sp-1}]]
    if {$a > $b} {return -1}
    if {$a < $b} {return 1}
    return 0
}

proc print-gen-output {string {generator ""}} {
    global gen_out_strings
    if {$gen_out_strings == "/dev/null"} {return}
    # print to stdout
    set stdoutFile [open_gen_output "stdout"]
    print_gen_msg $stdoutFile "\n" $generator
    print_gen_msg $stdoutFile $string $generator
    print_gen_msg $stdoutFile "\n\n" $generator
    # print to the output file
    if {$gen_out_strings != $stdoutFile} {
	print_gen_msg $gen_out_strings $string $generator
	print_gen_msg $gen_out_strings "\n\n" $generator
    }
}

proc lc-tokenize-file {input output} {
    set file [open $input "r"]
    set file2 [open $output "w"]
    while {1} {
	set length [gets $file line]
	if {$length == -1} {break}
	if {[string compare [string trim $line] ""] == 0} {continue}
	set line2 [lc-tokenize $line]
	puts $file2 $line2
    }
    close $file
    close $file2
}

proc lc-tokenize {line} {
    set line [string tolower $line]
    set line2 ""
    set end 0
    set last [string length $line]
    while {1} {
	if {$end == $last} {break}
	set start [string wordstart $line $end]
	if {$start == -1} {break}
	set end [string wordend $line $end]
	set word [string range $line $start [expr {$end-1}]]
	if {[string trim $word] == ""} {continue}
	lappend line2 $word
    }
    return $line2
}

proc best-ngram-sentence {file {orig ""}} {
    set fileId [open $file "r"]
    set best_sentence ""
    set best_prob -10000
    set best_pos 0
    set pos 0
    while {1} {
	set length1 [gets $fileId line1]
	if {$length1 == -1} {break}
	set length2 [gets $fileId line2]
	if {$length2 == -1} {break}
	set length3 [gets $fileId line3]
	if {$length3 == -1} {break}
	set length4 [gets $fileId line4]
	if {$length4 == -1} {break}
	set pattern "%d sentences, %d words, %d OOVs"
	set length2 [scan $line2 $pattern sentences words other]
	if {$length2 != 3} {break}
	set pattern "%d zeroprobs, logprob= %e ppl= %e ppl1= %e"
	set length3 [scan $line3 $pattern zeroprobs logprob ppl ppl1]
	if {$length3 != 4} {break}
	set logprob [expr {$logprob + $words}]
	if {[expr {$logprob > $best_prob}]} {
	    set best_prob $logprob
	    set best_sentence $line1
	    set best_pos $pos
	}
	incr pos
    }
    close $fileId
    if {$orig == ""} {return [list $best_sentence $best_prob]}
    # get the corresponding sentence from the original file
    set fileId [open $orig "r"]
    set pos 0
    while {1} {
	set length [gets $fileId line]
	if {$length == -1} {break}
	set line [string trim $line]
	if {$line == ""} {continue}
	if {$pos == $best_pos} {
	    set best_sentence $line
	    break
	}
	incr pos
    }
    close $fileId
    return [list $best_sentence $best_prob]
}

##############################################################################

proc generate-from-directory {dir {enumerate "all"} {generator ""}} {
    global defaultgenerator
    if {$generator == ""} {
	set generator $defaultgenerator
    }
    if {$generator == ""} {
	puts stderr "Please create a generator."
	return
    }
    foreach file [glob -nocomplain ${dir}/S\[0-9\]*.pl] {
	generate-from-file $file $enumerate $generator
    }
}

proc generate-from-range {prefix from to {generator ""}} {
    global defaultgenerator gen_out_strings
    if {$generator == ""} {
	set generator $defaultgenerator
    }
    if {$generator == ""} {
	puts stderr "Please create a generator."
	return
    }
    set i $from
    while {$i <= $to} {
	set name [glob -nocomplain [format "%s%d.pl" $prefix $i]]
	print_gen_msg $gen_out_strings "$i: "
	if {$name == ""} {
	    set name [glob -nocomplain $prefix$i.pl]
	}
	if {$name != ""} {
	    generate-from-file $name all $generator
	} else {
	    puts "missing file: [format "%s%d.pl" $prefix $i]"
	    print-gen-output "{ }"
	}
	incr i
    }
}

proc create-gen-test-directory {grammar testfile testdir} {
    file mkdir $testdir
    create-parser $grammar
    parse-testfile $testfile -outputPrefix "${testdir}/"
}

### UI RELATED PROCEDURES #####################################################

proc show-gen-vertices-window {chartData} {
    init_chart_nav $chartData
    set self .goalchart
    toplevel $self
    wm title $self "Gen Vertices"
    wm geometry $self +440+55
    frame $self.panel
    pack $self.panel -in $self -anchor nw
    add-kill-button $self $self.panel
    button $self.clear -text "clear" -font xleuifont \
	-command "unprune $self $chartData"
    bind $self.clear <Button-3> {
	puts ""
	puts "Clear all edge and category restrictions."}
    pack $self.clear -in $self.panel -anchor nw -side left
    button $self.show -text "restrictions" -font xleuifont \
	-command "show-edge-restrictions $chartData"
    bind $self.show <Button-3> {
	puts ""
	puts "Show all of the edge and category restrictions."
    }
    pack $self.show -in $self.panel -anchor nw -side left
    set yscrollable [y-scrollable-canvas $self.canvas $self]
    pack $yscrollable -in $self -expand 1 -fill both
    frame $self.canvas.data 
    $self.canvas create window 0 0 -window $self.canvas.data \
	-anchor nw
    set window $self
    set self $self.canvas.data
    foreach goalpair [get_field $chartData goalpairs] {
	set index [lindex $goalpair 0]
	set name [lindex $goalpair 1]
	frame $self.$index
	pack $self.$index -in $self -anchor nw
	label $self.$index.label -text $index -anchor w
	label $self.$index.name -text $name -anchor w
	button $self.$index.button -text "show" \
	    -command "show-goal-cats $chartData $index"
	bind $self.$index.button <Button-3> {
	    puts ""
	    puts "Shows possible categories for item."
	}
	pack $self.$index.button $self.$index.label $self.$index.name \
	    -in $self.$index -side left
    }
    set-scroll-region $window.canvas $window 400x600
}

proc show-goal-cats {chart index} {
    set self .catmenu$index
    set cats [get_goal_cats $chart $index]
    show-status-menu $self $chart $cats
    wm title $self "Cat Menu for Index $index"
}

proc quote-cat {string} {
    set i -1
    while {1} {
	incr i
	set char [string index $string $i]
	if {$char == ""} {return $string}
	if {$char == {[} } {
	    set prefix [string range $string 0 [expr {$i - 1}]]
	    set suffix [string range $string [expr {$i +1}] end]
	    set string ${prefix}<${suffix}
	}
	if {$char == {]} } {
	    set prefix [string range $string 0 [expr {$i - 1}]]
	    set suffix [string range $string [expr {$i +1}] end]
	    set string ${prefix}>${suffix}
	}
    }
    return $string
}

proc show-goal-edges {chart edgeCat index} {
    set cat [get_field $edgeCat category]
    set quotedcat [quote-cat $cat]
    set self .menu${quotedcat}$index
    set cats [get_goal_cats $chart $index]
    foreach catData $cats {
	if {[get_field $catData category] != $cat} {continue}
	show-status-menu $self $chart [get_field $catData edges]
	wm title $self "Edge Menu for Index $index, Cat $cat"
	return
    }
}

proc show-edge-restrictions {chart} {
    set self .restrictions
    set edges [get_restricted_edges $chart]
    show-status-menu $self $chart $edges
    wm title $self "Gen Edge Restrictions"
}

proc show-status-menu {self chart edges} {
    if {[winfo exists $self]} {
	destroy $self
    }
    toplevel $self
    wm geometry $self +440+130
    set yscrollable [y-scrollable-canvas $self.canvas $self]
    pack $yscrollable -in $self -expand 1 -fill both
    frame $self.canvas.data
    $self.canvas create window 0 0 -window $self.canvas.data \
	-anchor nw
    set window $self
    set self $self.canvas.data
    set edges [lsort -command category-order $edges]
    set cats ""
    foreach edge $edges {
	set statusVar edge$edge.status
        global $statusVar
	set $statusVar [get_field $edge status]
	set name [get_field $edge *label*]
        frame $self.cat$edge
        label $self.cat$edge.name -text $name -anchor w
#        radiobutton $self.cat$edge.in -text in \
	    -variable $statusVar -value in -anchor w
        radiobutton $self.cat$edge.out -text out \
	    -variable $statusVar -value out -anchor w
        radiobutton $self.cat$edge.dontcare -text ? \
	    -variable $statusVar -value ? -anchor w
	if {[string match (Edge)* $edge]} {
	    set id [get_field $edge id]
	    button $self.show$edge -text "show" \
		-command "show-subtree ${id}:1 $chart"
	    bind $self.show$edge <Button-3> {
		puts ""
		puts "Shows a tree for this edge."
	    }
	} else {
	    set index [get_field $edge index]
	    button $self.show$edge -text "show" \
		-command "show-goal-edges $chart $edge $index"
	    bind $self.show$edge <Button-3> {
		puts ""
		puts "Shows the edges for this goal and category."
	    }
	}
        pack  $self.show$edge $self.cat$edge.dontcare \
	    $self.cat$edge.out $self.cat$edge.name \
	    -side right -in $self.cat$edge -fill x
        pack $self.cat$edge -side top -in $self -fill x
        set pair [list $edge $statusVar]
        lappend cats $pair
    }
    button $self.apply -text Apply -command "applyMenu $window $chart $cats"
    button $self.cancel -text cancel -command "destroy $window"
    bind $self.apply <Button-3> {
	puts ""
	puts "Applies the constraints shown."
    }
    bind $self.cancel <Button-3> {
	puts ""
	puts "Cancels the current session without making any changes."
    }
    pack $self.apply $self.cancel -side top -in $self
    set-scroll-region $window.canvas $window 400x600
}

proc category-order {A B} {
    set catA [get_field $A category]
    set catB [get_field $B category]
    if {$catA < $catB} {return -1}
    if {$catA > $catB} {return 1}
    return 0
}

###############################################################################

# GENERATION COMMANDS

proc set-gen-outputs {{stgf "stdout"} {msgf "stderr"} {ab "abbrev"}} { 
  global gen_out_strings gen_out_msgs gen_out_format

    set gen_out_strings [open_gen_output $stgf]

    if {$stgf != $msgf}  {
         set gen_out_msgs [open_gen_output $msgf]
    } else { 
        set gen_out_msgs $gen_out_strings
    }           

    if {$ab != "abbrev"} {
       if {$ab !=  "full"} {
          set gen_out_format "abbrev"
          puts "\nGen output format must be full or abbrev"
          puts "\nSetting to abbrev"
       } else {
           set gen_out_format $ab
       } 
     } else {
        set gen_out_format "abbrev"
     }
    return
 }   

proc close-gen-outputs {} { 
    global gen_out_strings gen_out_msgs
    close_gen_output $gen_out_strings
    if {$gen_out_msgs != $gen_out_strings} {
	close_gen_output $gen_out_msgs
    }
}   

proc create-generator {grammar} {
    global defaultgenerator defaultchart grammarProps
    if {$grammar != "" && [string index $grammar 0] != "/"} {
	set grammar "[pwd]/$grammar"
    }
    if {$grammar != ""} {
	puts "loading $grammar..."
    }
    set generator ""
    set time [CPUtime {set generator [create_generator $grammar]}]
    if {$defaultgenerator == "" ||
	[get_field $defaultgenerator grammarfile] == $grammar} {
	set defaultgenerator $generator
        set defaultchart $generator
    }
#   temporary until figure out how to restart
    if {$generator == ""} {
	puts "\nError: could not load generator"
	return
    }
    if {$grammar == ""} {return $generator}
    puts [format "%.2f CPU seconds" [expr {[lindex $time 0]/[clock_rate]}]]
    puts "$grammar loaded"
    puts "Grammar last modified on [grammar_date $generator]."
    set grammarProps($generator,filename) $grammar
    return $generator
}

proc generate {goal} {
    global defaultgenerator defaultchart gen_out_msgs
    global defaultparser
    global chartgraph
    global sum-print-subtrees print_subtrees print_subtrees_file
    clear-all

    set generator $defaultgenerator
    if {$generator == "" && $defaultparser != ""} {
	puts "No default generator defined, creating one from the default parser."
	set file [get_field $defaultparser grammarfile]
	set generator [create-generator $file]
    }
    if {$generator == ""} {
	puts "Can't generate without a grammar."
	return 0 
    }
    set defaultchart $generator
    set chartgraph ""
    
    if {${sum-print-subtrees}} {
	set print_subtrees ${sum-print-subtrees}
	if {$print_subtrees_file == ""} {
	    set print_subtrees_file "subtrees.txt"
	}
    }

    set time1 [CPUtime {execute_generator $generator $goal $gen_out_msgs}]

    set time2 [CPUtime {print-genstrings $generator}]

    puts [format "Generator took %.2f CPU seconds." [expr {[lindex [expr {$time1+$time2}] 0]/[clock_rate]}]]

    if {${sum-print-subtrees}} {
	sum-print-subtrees $print_subtrees_file
    }
    return
}

###############################################################################

proc generate-from-window {window} {
    set fstructure [get-window-prop $window data]
    if {$fstructure == ""} {
	puts "Please display something to generate."
	return
    }
    generate-from-graph $fstructure
}

############################################################################

proc sample-fstructure {chart} {
    if {[get-chart-prop $chart "property_weights_file"] == ""} {
	#  get the first f-structure
	set root [root_edge $chart]
	set tree [get_next_tree "(DTree)0" $root]
	return [get_next_fstructure $tree "(Graph)0"]
    }
    # get the most probable f-structure
    set graph [extract_chart_graph $chart]
    unmark_all_choices $graph
    select-most-probable-structure $graph
    set tree [get_chosen_tree $graph -unmarked]
    if {$tree == ""} {set tree "(DTree)0"}
    set graph [get_chosen_fstructure $graph $tree]
    return $graph
}

proc regenerate {string} {
    global defaultparser no_Tk
    set chart $defaultparser
    clear-all $chart
    set orig_no_Tk $no_Tk
    set no_Tk 1
    set count [parse $string $chart]
    set no_Tk $orig_no_Tk
    set count [optimal-count $count]
    if {$count <= 0} {return}
    set chart $defaultparser
    set fstructure [sample-fstructure $chart]
    set time [CPUtime {
	generate-from-graph $fstructure
    }]
    set time [expr {$time/[clock_rate]}]
    puts "regeneration took $time CPU seconds."
    return ""
}

proc regenerate-morphemes {{chart ""}} {
    global defaultparser
    if {$chart == ""} {
	set chart $defaultparser
    }
    if {![string match "(Chart)*" $chart]} {
	puts "ERROR: regenerate-morphemes takes a Chart as an argument."
	return
    }
    puts "Regenerating morphemes from chart..."
    set count [print-genstrings $chart]
    return $count
}

proc regenerate-testfile {sentenceFile {start ""} {stop ""}} {
    global defaultgenerator defaultparser
    set errors 0
    set mismatches 0
    set total 0
    set totalGenTime 0
    set maxGenTime 0
    if {$stop == ""} {
	if {$start == ""} {
	    set stop end
	} else {
	    set stop $start
	}
    }
    if {$start == ""} {set start 1}
    
    set filename ${sentenceFile}.regen
    set fileID [new-file $filename]

    if {$defaultgenerator == "" && $defaultparser != ""} {
	set file [get_field $defaultparser grammarfile]
	create-generator $file
    }

    parse-testfile $sentenceFile $start $stop \
	    -parseProc regenerate-test \
	    -parseData $fileID
    close $fileID
    puts "[pluralize $total regeneration s] printed on $filename."
    if {$errors != 0} {
	puts "[pluralize $errors regeneration s] failed."
    }
    if {$mismatches != 0} {
	puts "[pluralize $mismatches regeneration s] did not match input."
    }
    puts "Generator: $totalGenTime CPU secs total, $maxGenTime CPU secs max"
    return $errors
}

set regenerateFile 0

proc regenerate-test {sentence parser n output} {
    global xle_client_socket gen_serv_input gen_serv_output
    global defaultgenerator regenerateFile
    upvar 2 errors errors
    upvar 2 mismatches mismatches
    upvar 2 total total
    upvar 2 totalGenTime totalGenTime
    upvar 2 maxGenTime maxGenTime

    # Parse the sentence
    parse-sentence $sentence $parser

    # Extract an f-structure to generate from
    set root [root_edge $parser]
    if {$root == ""} {
	return [list 0 0 0]}
    set solutions [count_solutions $parser]
    set solutions [optimal-count $solutions]
    if {$solutions <= 0} {return [list 0 0 0]}
    set fstructure [sample-fstructure $parser]

    # Generate from the f-structure
    set sentence [get-chart-prop $parser sentence]
    puts "\nRegenerating '$sentence'"
    if {$regenerateFile} {
	print_graph_as_prolog $fstructure $gen_serv_input $sentence
	file attributes $gen_serv_input -permissions 0666
	set fstructure [read_prolog_graph $gen_serv_input]
    }
    set time [CPUtime {
	generate-from-graph $fstructure first
	set generation [get_genstrings $defaultgenerator]
	set generation [string trim $generation]}]
    set time [expr {$time/[clock_rate]}]
    set totalGenTime [expr {$totalGenTime + $time}]
    if {$time > $maxGenTime} {set maxGenTime $time}
    set count [count_genstrings $defaultgenerator]
    set nsubtrees [count_chart_subtrees $defaultgenerator]
    if {$output != ""} {
	puts $output "$generation\n"
    }
    if {$generation == "NULL" || $generation == ""} {
	set msg "$n:REGENERATION FAILED FOR '$sentence'."
	puts $msg
	if {$output != ""} {puts $output "$msg\n"}
	incr errors
    } elseif {![string-matches-regexp $sentence $generation]} {
	set msg "$n:REGENERATION DID NOT MATCH '$sentence'."
	puts $msg
	if {$output != ""} {puts $output "$msg\n"}
	incr mismatches
    }
    if {$output != ""} {flush $output}
    incr total
    return [list $count $time $nsubtrees]
}

proc get-gen-server-output {filename} {
    set fileID [open $filename r]
    set output ""
    set blank 0
    while {1} {
	set length [gets $fileID line]
	if {$length == -1} {
	    break
	}
	if {$length == 0} {
	    set blank 1
	    continue
	}
	if {$blank} {
	    set output ""
	}
	set blank 0
	if {$output == ""} {
	    set output $line
	} else {
	    set output "$output\n$line"
	}
    }
    close $fileID
    return $output
}

proc string-matches-regexp {sentence regexp {stack ""} {s 0} {r 0}} {
    set slength [string length $sentence]
    set rlength [string length $regexp]
    set mismatch 0
    while {$r < $rlength} {
	if {$s > $slength} {return 0}
	set schar [string index $sentence $s]
	set rchar [string index $regexp $r]
	if {[white-space $schar] && [white-space $rchar]} {
	    set s [skip-white-space $sentence $s]
	    set r [skip-white-space $regexp $r]
	    continue
	}
	incr r
	if {$schar == $rchar} {
	    incr s
	    continue
	}
	if {$rchar == "\{"} {
	    # We are starting a disjunction
	    # Save s so that we can restart on a new disjunct
	    set stack [concat $s $stack]
	    continue
	}
	if {$rchar == "|"} {
	    if {$mismatch} {
		# Try again on the next disjunct
		set s [lindex $stack 0]
		set mismatch 0
	    } else {
		# We successfully matched a disjunct
		# Try again on the next disjunct non-deterministically
		if {[string-matches-regexp $sentence $regexp \
			 $stack [lindex $stack 0] $r]} {
		    return 1
		}
		# Skip the next disjunct
		set r [skip-disjunct $regexp $r]
	    }
	    continue
	}
	if {$rchar == "\}"} {
	    # We are done processing a disjunction
	    # Pop the stack
	    set stack [lrange $stack 1 end]
	    if {$mismatch} {
		# We failed the whole disjunction
		# Try a higher level disjunct, if possible
		if {$stack == ""} {return 0}
		set r [skip-disjunct $regexp $r]
	    }
	    continue
	}
	if {[white-space $rchar]} {
	    continue
	}
	# The string doesn't match this disjunct
	# Skip the rest of the disjunct
	set mismatch 1
	set r [skip-disjunct $regexp $r]
    }
    if {$s == $slength} {return 1}
    return 0
}

proc white-space {char} {
    if {$char == " "} {return 1}
    if {$char == "\t"} {return 1}
    if {$char == "\n"} {return 1}
    return 0
}

proc skip-white-space {string pos} {
    while {1} {
	set char [string index $string $pos]
	if {![white-space $char]} {return $pos}
	incr pos
    }
    return $pos
}

proc skip-disjunct {regexp r} {
    set rlength [string length $regexp]
    set depth 0
    while {$r < $rlength} {
	set rchar [string index $regexp $r]
	if {$rchar == "\{"} {
	    incr depth
	} elseif {$rchar == "|"} {
	    if {$depth == 0} {return $r}
	} elseif {$rchar == "\}"} {
	    incr depth -1
	    if {$depth < 0} {return $r}
	}
	incr r
    }
    return $r
}

#########################################################################

proc set-gen-adds {addmode addlist {OTmark ""} {generator ""}} {
    global defaultgenerator
    if {$generator == ""} {
	set generator $defaultgenerator
    }
    if {$generator == ""} {
	puts "Generator has not been loaded yet."
	return
    }
    set pairs 0
    set nopairs 0
    if {$addmode == "add" && $OTmark == ""} {
	# addlist may be the output of get-gen-adds
	foreach pair $addlist {
	    if {[lindex $pair 1] == ""} {
		set nopairs 1
	    } else {
		set pairs 1
	    }
	    if {$pairs && $nopairs} {
		puts "Ill-formed attribute list: $addlist"
		return
	    }
	}
    }
    if {$pairs} {
	set_gen_adds $generator $addmode "" ""
	foreach pair $addlist {
	    set_gen_adds $generator $addmode [lindex $pair 0] [lindex $pair 1]
	}	
    } else {
	set_gen_adds $generator $addmode $addlist $OTmark
    }
}

#########################################################################

proc get-gen-adds {addmode {generator ""}} {
    global defaultgenerator
    if {$generator == ""} {
	set generator $defaultgenerator
    }
    if {$generator == ""} {
	puts "Generator has not been loaded yet."
	return
    }
    return [get_gen_adds $generator $addmode]
}


###############################################################################

if {[info exists env(USER)]} {
    set user $env(USER)
} else {
    set user "unknown"
}
set tmpdir "/tmp/$user"

set gen_serv_input  "$tmpdir-genin.pl"
set gen_serv_output "$tmpdir-genout.txt"
set gen_serv_started 0
set seconds_wait_for_gen_serv 10

set defaultSocketPorts(generator) 2540

set XLE xle

proc start-generation-server {{grammar ""}} {
    global defaultgenerator
    if {$defaultgenerator != ""} {return 1}
    
    if {$grammar == ""} {
	puts "Please specify a generation grammar."
	return 0
    }

    create-generator $grammar
}

proc old-start-generation-server {{grammar ""} \
			      {port "default"} {host "localhost"}} {
    global defaultparser grammarProps defaultSocketPorts
    global xle_client_socket seconds_wait_for_gen_serv
    global gen_serv_started gen_serv_input gen_serv_output
    global XLE

    if {$gen_serv_started && \
	[catch {eof $xle_client_socket}] == 0} {
	return 1
    }

    if {$grammar == ""} {
	if {[info exists grammarProps($defaultparser,filename)] == 0} {
	    puts stderr "Can't start the generator before a parser is created"
	    return 0
	} else {
	    set grammar $grammarProps($defaultparser,filename)
	}
    }
    puts "Starting a generation server.  Please wait..."

    if {$port == "default"} {
	set port $defaultSocketPorts(generator)
    }

    catch {exec xterm -sb -sl 1000 -iconic -title Generation_Server -e \
	    $XLE -noxlerc -exec "start-xle-server $port" &}

    catch {exec sleep $seconds_wait_for_gen_serv}

    set xle_client_socket [start-xle-client $port $host]

    set ack [tell-xle-server $xle_client_socket \
	    "uplevel 1 set gen_serv_input $gen_serv_input"]
		 
    if {$ack != "yes"} {
	puts "Error setting generation input file."
	return 0
    }

    set ack [tell-xle-server $xle_client_socket \
		 "uplevel 1 set gen_serv_output $gen_serv_output"]
    if {$ack != "yes"} {
	puts "Error setting generation output file."
	return 0
    }

    set ack [tell-xle-server $xle_client_socket \
		 "set-gen-outputs $gen_serv_output stderr full"]

    if {$ack != "yes"} {
	puts "Error setting generation output."
	return 0
    }

    set ack [tell-xle-server $xle_client_socket \
	    "create-generator $grammar default"]

    if {$ack == "no" || $ack == ""} {
	puts stderr "Generation server doesn't respond."
	return 0
    } 

    if {$ack != "yes"} {
	puts "Error setting gen adds."
	return 0
    }

    set gen_serv_started 1
    return 1
}

proc terminate-generation-server {} {
    global xle_client_socket gen_serv_started
    tell-xle-server $xle_client_socket exit
    set gen_serv_started 0
}

###############################################################################

#########################################
# This command runs in the client space #
#########################################
proc generate-from-this-solution {{prepare_input 1} {enumerate "all"} 
    {outstream stdout}} {
	global xfer_serv_output
	file attributes $xfer_serv_output -permissions 0666
	reset_prolog_storage
	set fschart_graph [read_prolog_graph $xfer_serv_output]
	generate-from-graph $fschart_graph $enumerate
	return 1
    }

proc old-generate-from-this-solution {{prepare_input 1} {enumerate "all"} \
					{outstream stdout}} {
    global xle_client_socket gen_serv_input gen_serv_output

    if {[start-generation-server] == 0} {
	puts stderr "Starting generation server failed.."
	return 0
    }

    if {$prepare_input} {
	print-fs-as-prolog $gen_serv_input [default-window fstructure]
	file attributes $gen_serv_input -permissions 0666
    }

    set command "server-generate-from-file $enumerate"

    set ack [tell-xle-server $xle_client_socket $command]

    if {$ack == "yes"} {
	set rc [display-generation-server-results $gen_serv_output $outstream]
	return $rc
    } else {
	puts "generation server failed to generate."
	return 0
    }
}

proc display-generation-server-results {file outstream} {
    set f [open $file r]
    set i 0

    while {[gets $f line] >= 0} {
	if {$line != ""} {
	    incr i
	}
	puts $outstream $line
    }
    if {$i == 0} {
	puts stderr "Generator didn't produce an output."
	return 0
    }
    close $f
    return 1
}


proc get-gen-server-output {filename} {
    set fileID [open $filename r]
    set output ""
    set blank 0
    while {1} {
	set length [gets $fileID line]
	if {$length == -1} {
	    break
	}
	if {$length == 0} {
	    set blank 1
	    continue
	}
	if {$blank} {
	    set output ""
	}
	set blank 0
	if {$output == ""} {
	    set output $line
	} else {
	    set output "$output\n$line"
	}
    }
    close $fileID
    return $output
}

###########################################
# While this one runs in the server space #
###########################################
proc server-generate-from-file {{enumerate "all"}} {
    global gen_out_strings gen_out_msgs gen_serv_input gen_serv_output
    reset_gen_out $gen_out_strings $gen_serv_output

generate-from-file $gen_serv_input $enumerate

# test
    puts stdout "--- Generation Results --------------------------------------"
    display-generation-server-results $gen_serv_output stderr
    puts stdout "-------------------------------------------------------------"

    file delete -force $gen_serv_input
    file attributes $gen_serv_output -permissions 0777
}

###############################################################################

proc generate-strings-from-file {input_file {enumerate "all"}} {
    reset_prolog_storage
    set fschart_graph [read_prolog_graph $input_file]
    if {$fschart_graph == ""} return
    return [generate-strings-from-graph $fschart_graph $enumerate]
}

proc generate-strings-from-graph {fschart_graph {enumerate "all"}} {
    global defaultgenerator defaultparser
    global gen_out_msgs gen_out_strings
    global gen_out_format gen_serv_input
    puts "generate-strings-from-graph $fschart_graph $enumerate"

    set generator $defaultgenerator
    if {$generator == "" && $defaultparser != ""} {
	set file [get_field $defaultparser grammarfile]
	set generator [create-generator $file]
    }
    if {$generator == ""} {
	puts "Can't generate without a grammar."
	return 0 
    }
    if {$fschart_graph == ""} {
	puts "Missing graph!"
	print-stack
	return
    }
    clear-all $generator
    set graph_file $gen_serv_input

    if {$enumerate == "first"} {
	set enumerate 1
    }
    if {$enumerate == "all"} {
	set enumerate 9999
    }

#
#   Before each enumeration, reset the prolog's compstate
#
#   reset_prolog_storage

#
#   Read the prolog input file
#
    set n_solutions [count_choice_solutions $fschart_graph "all"]

#
#   Get the first fstructure it includes (which may be itself if unambiguous)
#
    set solution [get_next_fstructure $fschart_graph "(Graph)NULL"]
    set solution_no 1

    while {$solution != ""} {

	if {$enumerate != 1 && $n_solutions > 1} {
	    puts "Generating from solution $solution_no ..."
	}

#
#	Prepare a file with each individual solution
#
	set goal "[file rootname $graph_file].${solution_no}.pl"

	print-prolog-chart-graph $goal $solution
	file attributes $goal -permissions 0666

#
#   Read the prolog input file
#
#       Index the input (this replaces 'use-gen-input')
#
	index_generator_input $generator $goal unspec

#
#	Run the generator
#
	set time [CPUtime {set genRC [ \
		execute_generator $generator $goal $gen_out_msgs]}]
	set time [expr {[lindex $time 0]/[clock_rate]}] 

	if {$genRC <= 0} {
	    set msg [format "Generation failed (%.2f seconds)." $time]
	    print_gen_msg $gen_out_msgs "\n${msg}\n\n"
	    print_gen_msg $gen_out_strings "Solution $solution_no: ${msg}\n\n"
	} else {
#
#	    If generator succeeded, print the strings
#
	    if {$enumerate != 1 && $n_solutions > 1} {
		print_gen_msg $gen_out_strings "Solution $solution_no:\n"
	    } else {
	    }
	    set count [print-genstrings $generator]

	    if {$count == 0} {
		if {$enumerate != 1 && $n_solutions > 1} {
		    print_gen_msg $gen_out_strings "\n"
		}
		print_gen_msg $gen_out_strings "Generator didn't produce an output!\n\n"
	    }

	    set msg [format "Generator produced %d string/s (%.2f seconds)." $count $time]
	    print_gen_msg $gen_out_msgs "${msg}\n"
	    puts ""
	}
#
#	Iterate to next fstructure, unless reached maximum solutions
#
	incr solution_no
	if {$solution_no > $enumerate} {
	    break
	}
	set solution [get_next_fstructure $fschart_graph $solution]
    }
}

###############################################################################

proc generation-training-data {graph dir numtrees {generator ""}} {
    global defaultgenerator
    if {$generator == ""} {
	set generator $defaultgenerator
    }
    if {[string first "." $graph] != -1} {
	set graph [read-prolog-chart-graph $graph]
    }
    file mkdir $dir
    set i 1
    set fs "(Graph)0"
    while {1} {
	set fs [get_next_fstructure $graph $fs]
	if {$fs == ""} break
	file mkdir $dir/$i
	print-prolog-chart-graph $dir/$i/fs.pl $fs
	generate-from-graph $fs "all" $generator
	set root [root_edge $generator]
	if {$root == "" || $root == "(Edge)0"} {continue}
	set tree "(DTree)0"
	set j 1
	while {1} {
	    set tree [get_next_tree $tree $root -goodOnly]
	    if {$tree == "" || $tree == "(DTree)0"} {break}
	    if {$j > $numtrees} {break}
	    set gs "(Graph)0"
	    while {1} {
		set gs [get_next_fstructure $tree $gs]
		if {$gs == ""} {break}
		print-prolog-chart-graph $dir/$i/gen$j.pl $gs
		set string [get_genstrings $generator $gs]
		set file [open $dir/$i/gen$j.txt w]
		puts $file $string
		close $file
		incr j
	    }
	}
	incr i
    }
}
