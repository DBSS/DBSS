package TraceBrowser1;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.text.JTextComponent;
import javax.swing.text.DefaultHighlighter;
import javax.swing.text.AbstractDocument;
import se.sics.prologbeans.*;

public class Browser extends JFrame {

	private int currentRecordIndex = 0;
	
	private static int  DEFAULT_PORT = 8066;
	private static int  RULE = 0, INPUT_FACTS = 1, CANDIDATE_FACTS = 2, 
						TRACE = 3, MATCHES = 4, RHS_FACTS=5, OUTPUT_FACTS = 6, 
						N_DISPLAY_PANES = 7;
	private static int  SEARCH_FORWARDS = 0, SEARCH_BACKWARDS = 1, NEXT = 2, PREVIOUS = 3, CURRENT = 4, 
						START = 5, END = 6, UNDO = 7;
	private Font DISPLAY_PANE_FONT = new Font( "Monospaced", Font.PLAIN, 14 );
	private ImageIcon HIGHLIGHTED_PANE_ICON = new ImageIcon( getClass().getResource( "yellow_tick.gif" ) );
	private Color HIGHLIGHTING_COLOR = Color.yellow;
	private Color CONSUMED_COLOR = Color.pink;
	private Color PRODUCED_COLOR = Color.green;
	private static int MAX_HISTORY_LENGTH = 100000;
	private int[] history = new int[MAX_HISTORY_LENGTH];
	private int history_pointer = 0;
	
	private JFrame frame;
	
	private GridBagLayout displayLayout = new GridBagLayout();
	private GridBagConstraints displayConstraints = new GridBagConstraints();
	
	private GridBagLayout buttonLayout = new GridBagLayout();
	private GridBagConstraints buttonConstraints = new GridBagConstraints();
	
	private JMenuBar menuBar = new JMenuBar();
	
	private JMenu fileMenu;
	private JMenuItem reloadMenuItem;
	private JMenuItem exitMenuItem;
	
	private JMenu viewMenu;
	
	private JMenu searchMenu;
	
	private JCheckBoxMenuItem[] DisplayCheckBoxes = new JCheckBoxMenuItem[N_DISPLAY_PANES];
	private JTextArea[] DisplayTextPanes = new JTextArea[N_DISPLAY_PANES];
	private JScrollPane[] DisplayScrollPanes = new JScrollPane[N_DISPLAY_PANES];
	private String[] DisplayTextSave = new String[N_DISPLAY_PANES];
	private JLabel[] DisplayLabels = new JLabel[N_DISPLAY_PANES];
	private final String[] DisplayLabelText = {"Rule", "Input facts", "Candidate facts", 
											   "Trace", "Matches", "RHS facts", "Output facts"};
	private final String[] DisplayLabelToolTipText = 
			{"Display/don't display rules", 
			 "Display/don't display input facts", 
			 "Display/don't display candidate facts", 
			 "Display/don't display trace", 
			 "Display/don't display matches", 
			 "Display/don't display RHS facts", 
			 "Display/don't display output facts"};
    //private Box answerCheckBox = Box.createVerticalBox();
	
	private JPanel displayPanel = new JPanel( displayLayout );
	
	private JTextArea searchStatusText = new JTextArea(6, 15);
	private JPanel searchStatusPanel = new JPanel(new BorderLayout());
	//private JPanel AnswerCheckAndSearchStatusPanel = new JPanel(new BorderLayout());
	//private JPanel fullAnswerPanel = new JPanel(new BorderLayout());

    //private JPanel inputPanel = new JPanel(new BorderLayout());
	private Box inputPanel = Box.createHorizontalBox();

    //private JPanel directionButtonPanel = new JPanel(new BorderLayout());
    //private JPanel originButtonPanel = new JPanel(new BorderLayout());
    //private JPanel buttonPanel = new JPanel(new BorderLayout());

    private JPanel listsPanelTop = new JPanel(new FlowLayout());
    //private JPanel listsPanel = new JPanel(new BorderLayout());
    private Box listsPanel = Box.createVerticalBox();

    //private Box executePanel = Box.createVerticalBox();
    private JPanel executePanel = new JPanel( buttonLayout );
    private JPanel searchPanel = new JPanel(new FlowLayout());

    private JPanel rsetsPanel = new JPanel(new BorderLayout());
    private JPanel positionsPanel = new JPanel(new BorderLayout());
    private JPanel successfulPanel = new JPanel(new BorderLayout());
    private JPanel ruleNumberPanel = new JPanel(new BorderLayout());
    private Box patternPanel = Box.createHorizontalBox();
    private Box highlightPanel = Box.createHorizontalBox();

    private final int MAX_RSETS = 100;
    private final String rsets[] = new String[MAX_RSETS];
    private DefaultListModel rsetsListModel = new DefaultListModel();
    private JList rsetsList = new JList( rsetsListModel );

    private final String positions[] = { "input", "candidate", "RHS", "output", "consumed", "produced" };
    private JList positionsList = new JList( positions );

    private final String successful[] = { "yes", "no" };
    private JList successfulList = new JList( successful );
   
    private JTextArea ruleNumberText = new JTextArea(1, 5);
    private JTextArea patternText = new JTextArea(1, 25);
    private JTextArea highlightText = new JTextArea(1, 25);

    private JButton find_next = new JButton("Find Next");
    private JButton find_previous = new JButton("Find Previous");
    private JButton next = new JButton("Next");
    private JButton previous = new JButton("Previous");
    private JButton start = new JButton("Start");
    private JButton end = new JButton("End");
    private JButton undo = new JButton("Undo");
    private JButton redisplay = new JButton("Redisplay");

    private PrologSession session = new PrologSession();
    
    public Browser() {
    	new Browser(DEFAULT_PORT);
    }

    public Browser(int port) {
    	session.setPort(port);
    	session.setTimeout(25000);
    	
    	setTitle("Trace Browser version 1.1");
     	setJMenuBar( menuBar ); 
    	
    	fileMenu = new JMenu( "File" );
    	fileMenu.setMnemonic( 'F');
    	      
        reloadMenuItem = new JMenuItem( "Reload" );
        reloadMenuItem.addActionListener( new ReloadButtonListener() );
        fileMenu.add( reloadMenuItem );
        
        exitMenuItem = new JMenuItem( "Exit" );
        exitMenuItem.addActionListener( new ExitButtonListener() );
        fileMenu.add( exitMenuItem );
        
        menuBar.add(fileMenu);
        
    	viewMenu = new JMenu( "View" );
    	viewMenu.setMnemonic( 'V' );

    	menuBar.add(viewMenu);
    	
    	searchMenu = new JMenu( "Search" );
    	searchMenu.setMnemonic( 'S' );

    	Container panel = this.getContentPane();
      
    	//rsetsList.setSelectionMode( ListSelectionModel.SINGLE_SELECTION );
    	rsetsList.setSelectionMode( ListSelectionModel.MULTIPLE_INTERVAL_SELECTION );
    	
    	//positionsList.setSelectionMode( ListSelectionModel.SINGLE_SELECTION );
    	positionsList.setSelectionMode( ListSelectionModel.MULTIPLE_INTERVAL_SELECTION );
    	
    	//successfulList.setSelectionMode( ListSelectionModel.SINGLE_SELECTION );
    	successfulList.setSelectionMode( ListSelectionModel.MULTIPLE_INTERVAL_SELECTION );

    	rsetsListModel.addElement("any");
    	rsetsList.setVisibleRowCount( 6 );
    	positionsList.setVisibleRowCount( 6 );
    	successfulList.setVisibleRowCount( 6 );
	
    	rsetsPanel.add( new JLabel( "RSet" ),  BorderLayout.NORTH );
    	rsetsPanel.add( new JScrollPane( rsetsList ), BorderLayout.SOUTH );
    	rsetsPanel.setToolTipText("Search only specified rule-set(s). Default = all");

    	positionsPanel.add( new JLabel( "Position" ),  BorderLayout.NORTH );
    	positionsPanel.add( new JScrollPane( positionsList ), BorderLayout.SOUTH );
    	positionsPanel.setToolTipText("Search only specified field(s) in rule application. Default = all");

    	successfulPanel.add( new JLabel( "Successful" ),  BorderLayout.NORTH );
    	successfulPanel.add( new JScrollPane( successfulList ), BorderLayout.SOUTH );
    	successfulPanel.setToolTipText("Search only successful/unsuccessful rule applications. Default = all");

    	ruleNumberPanel.add( new JLabel( "Rule #" ),  BorderLayout.NORTH );
    	ruleNumberPanel.add( ruleNumberText, BorderLayout.SOUTH );
    	ruleNumberPanel.setToolTipText("Search for for rule applications with specific rule number. Default = all rules");

    	patternPanel.add( new JLabel( "Find" ) );
    	patternPanel.add(Box.createHorizontalStrut(10));
    	patternPanel.add( patternText );
    	patternPanel.setToolTipText("Search for applications containing quoted text string or fact pattern. " +
    								"Use XRF notation, e.g. PRED(%X, %Y) or DET(%X, sg). " +
    								"Default = any pattern");
    	
    	highlightPanel.add( new JLabel( "Highlight " ) );
    	highlightPanel.add(Box.createHorizontalStrut(10));
    	highlightPanel.add( highlightText );
    	highlightPanel.setToolTipText("Highlight applications containing quoted text string or fact pattern. " +
    								  "Use XRF notation, e.g. PRED(%X, %Y) or DET(%X, sg). " +
    								  "Default = highlight search pattern");
    			
    	for ( int i = 0; i < N_DISPLAY_PANES; i++ ) {
    		DisplayLabels[i] = new JLabel( DisplayLabelText[i] );
    		
    		DisplayTextPanes[i] = new JTextArea(5, 20);
    		DisplayTextPanes[i].setFont( DISPLAY_PANE_FONT );
    		DisplayTextPanes[i].setEditable(false);
    		DisplayScrollPanes[i] = new JScrollPane(DisplayTextPanes[i]);
    		
    		displayConstraints.fill = GridBagConstraints.VERTICAL;
    		addDisplayComponent( DisplayLabels[i], i, 0, 1, 1, 0, 10 );
    		displayConstraints.fill = GridBagConstraints.BOTH;
    		addDisplayComponent( DisplayScrollPanes[i], i, 1, 10, 1, 10, 10 );
    		
    		DisplayCheckBoxes[i] = new JCheckBoxMenuItem( DisplayLabelText[i], true );
    		DisplayCheckBoxes[i].setToolTipText(DisplayLabelToolTipText[i]);
    		DisplayCheckBoxes[i].addItemListener( new AnswerCheckBoxHandler() );
    		viewMenu.add(DisplayCheckBoxes[i]);
    	}
    	
    	DisplayCheckBoxes[TRACE].setSelected(false);
    	DisplayCheckBoxes[MATCHES].setSelected(false);
    	
    	searchStatusPanel.add(new JLabel( "Search status" ),  BorderLayout.NORTH );
    	searchStatusPanel.add( new JScrollPane( searchStatusText ), BorderLayout.CENTER );
    	
    	listsPanelTop.add( rsetsPanel );
    	listsPanelTop.add( ruleNumberPanel );
    	listsPanelTop.add( positionsPanel );
    	listsPanelTop.add( successfulPanel );
    	
    	listsPanel.add(listsPanelTop);
    	listsPanel.add(patternPanel);
    	listsPanel.add(Box.createVerticalStrut(10));
    	listsPanel.add(highlightPanel);
    	listsPanel.add(Box.createVerticalStrut(10));
    	
    	buttonConstraints.fill = GridBagConstraints.VERTICAL;
    	addButtonToPanel( find_next, 1, 1 );
    	addButtonToPanel( find_previous, 1, 2 );
    	addButtonToPanel( next, 2, 1 );
    	addButtonToPanel( previous, 2, 2 );
    	addButtonToPanel( start, 3, 1 );
    	addButtonToPanel( end, 3, 2 );
    	addButtonToPanel( redisplay, 4, 1 );
    	addButtonToPanel( undo, 4, 2 );
		buttonConstraints.fill = GridBagConstraints.BOTH;
    	
    	find_next.setToolTipText("Search forwards");
    	find_previous.setToolTipText("Search backwards");
    	next.setToolTipText("Go to next record");
    	previous.setToolTipText("Go to previous record");
    	start.setToolTipText("Go to first record");
    	end.setToolTipText("Go to last record");
    	redisplay.setToolTipText("Redisplay current record");
    	undo.setToolTipText("Go back to last record visited");
    	    	
    	searchPanel.add( executePanel );

    	inputPanel.add(Box.createHorizontalStrut(20));
    	inputPanel.add(Box.createHorizontalGlue());
    	inputPanel.add(listsPanel);
    	inputPanel.add(Box.createHorizontalGlue());
    	inputPanel.add(Box.createHorizontalStrut(20));
    	inputPanel.add(searchStatusPanel);
    	inputPanel.add(Box.createHorizontalGlue());
    	inputPanel.add(Box.createHorizontalStrut(20));
    	inputPanel.add(searchPanel);
    	
     	panel.add(displayPanel, BorderLayout.CENTER);
    	panel.add(inputPanel, BorderLayout.SOUTH);
    	
    	find_next.addActionListener( new FindNextButtonListener() );
    	find_previous.addActionListener( new FindPreviousButtonListener() );
    	next.addActionListener( new NextButtonListener() );
    	previous.addActionListener( new PreviousButtonListener() );
    	start.addActionListener( new StartButtonListener() );
    	end.addActionListener( new EndButtonListener() );
    	redisplay.addActionListener( new RedisplayButtonListener() );
    	undo.addActionListener( new UndoButtonListener() );
    	
    	InitialiseRSetListFromProlog();
    	
    	setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    	pack();
    	setVisible(true);
    	
    }
    
    public void ReloadTraceFile() {
    	try { 
    		QueryAnswer answer =
    			session.executeQuery("reload");
    		if ( answer.queryFailed() ) {
    			searchStatusText.setText("Error when reloading trace file\n");
    		}
    		else {
    			searchStatusText.setText("Trace file reloaded\n");
    		}
    	}
    	catch (Exception e) {
    		//searchStatusText.setText("Trace file reloaded\n");
			searchStatusText.setText("Error when querying Prolog Server: " + e.getMessage() + '\n');
		    e.printStackTrace();
		    }
    }
    
    public void InitialiseRSetListFromProlog() {
    	try { 
    		QueryAnswer answer =
    			session.executeQuery("find_rule_sets(Sets)");
    		Term setsTerm = answer.getValue("Sets");
    		if ( setsTerm != null && setsTerm.isCompound() ) {
    			//Remove everything in RSets 
    			while ( rsetsListModel.getSize() > 0 ) {
    				rsetsListModel.remove(0);
    			}
    			//Don't need "any" with multiple selections
    			//Add first element "any"
    			//rsetsListModel.addElement("any");
    			//rsets[0] = "any";
    			for ( int i = 0; i < setsTerm.getArity(); i++ ) {
    				String rset = ( (PBString) setsTerm.getArgument(i+1) ).toString();
    				rsetsListModel.addElement(rset);
    				rsets[i] = rset;
    			}
    			searchStatusText.append("Read list of rule-sets\n");
    		}
    		else {
    			searchStatusText.append("Error: unable to read list of rule-sets\n");
    		}
    	}
    	catch (Exception e) {
			searchStatusText.setText("\nError when querying Prolog Server: " + e.getMessage() + '\n');
		    e.printStackTrace();
		    }
    }
		
	private void findAndDisplayRecord(int mode) {
		try {   
			
			for (int i = 0; i < N_DISPLAY_PANES; i++ ) {
				DisplayTextSave[i] = DisplayTextPanes[i].getText();
				DisplayTextPanes[i].setText("");
			}
			int paneWidth = DisplayTextPanes[RULE].getWidth();
			int fontWidth = (DisplayTextPanes[RULE].getFontMetrics(DISPLAY_PANE_FONT)).charWidth('A');
			int paneWidthInChars = (int) paneWidth / fontWidth;
			//System.out.println("\nPane width = " + paneWidth);
			//System.out.println("Font width = " + fontWidth);
			//System.out.println("Width in chars = " + paneWidthInChars);
		    
			int[] selectedRSetIndices = rsetsList.getSelectedIndices();
		    int[] selectedPositionIndices = positionsList.getSelectedIndices();
		    String selectedPattern0 = patternText.getText();
		    String selectedHighlight0 = highlightText.getText();
		    int[] selectedSuccessfulIndices = successfulList.getSelectedIndices();
		    String selectedRuleNumber0 = ruleNumberText.getText();
		
		    String selectedRSet = "any";
		    String selectedPosition = "any";
		    String selectedPattern = "any";
		    String selectedSuccessful = "any";
		    String selectedRuleNumber = "any";
		    String selectedIterationNumber = "any";
		    String selectedSearchDirection = "forward";
		    int selectedStartIndex = currentRecordIndex;
		    String selectedHighlight = "any";
		    
		    if ( mode == SEARCH_FORWARDS || mode == SEARCH_BACKWARDS ) {
		    	if ( selectedRSetIndices.length > 0 ) { 
		    		selectedRSet = "[ ";
		    		for ( int i = 0 ; i < selectedRSetIndices.length ; i++ ) {
		    			selectedRSet += "'" + rsets[ selectedRSetIndices[i] ] + "'";
		    			if ( i == selectedRSetIndices.length - 1) {
		    				selectedRSet += " ]";
		    			}
		    			else {
		    				selectedRSet += ", ";
		    			}
		    		} 
		    	}

		    	if ( selectedPositionIndices.length > 0 ) { 
		    		selectedPosition = "[ ";
		    		for ( int i = 0 ; i < selectedPositionIndices.length ; i++ ) {
		    			selectedPosition += "'" + positions[ selectedPositionIndices[i] ] + "'";
		    			if ( i == selectedPositionIndices.length - 1) {
		    				selectedPosition += " ]";
		    			}
		    			else {
		    				selectedPosition += ", ";
		    			}
		    		}
		    	}

		    	if ( selectedSuccessfulIndices.length == 1 ) { 
		    		selectedSuccessful = successful[ selectedSuccessfulIndices[0] ]; 
		    	}
		    	else {
		    		selectedSuccessful = "any";
		    	}

		    	if ( selectedRuleNumber0.length() > 0 ) { 
		    		selectedRuleNumber = selectedRuleNumber0; 
		    	}
		    		
		    	if ( selectedPattern0.length() > 0 ) { 
			    	selectedPattern = selectedPattern0;
		    	}
		    }
		    
		    // If there is a highlight pattern, use it, otherwise highlight search pattern if any
		    if ( selectedHighlight0.length() > 0 ) {
		    	selectedHighlight = selectedHighlight0;
		    }
		    else if ( selectedPattern0.length() > 0 ) {
		    	selectedHighlight = selectedPattern0;
		    }

		    if ( mode == SEARCH_BACKWARDS ) {
		    	selectedSearchDirection = "backward";
		    	} 
		    else if ( mode == SEARCH_FORWARDS ) {
		    	selectedSearchDirection = "forward";
		    }
		    else if ( mode == CURRENT || mode == START || mode == END || mode == UNDO ) {
		    	selectedSearchDirection = "current_only";
		    }
		    else if ( mode == NEXT ) {
		    	selectedSearchDirection = "next_only";
		    }
		    else if ( mode == PREVIOUS) {
		    	selectedSearchDirection = "previous_only";
		    }

		    if ( mode == START ) {
		    	selectedStartIndex = 1; }
		    else if ( mode == END ) {
		    	selectedStartIndex = -1; } // -1 conventionally used to mean "end"	
		    else if ( mode == UNDO ) {
		    	selectedStartIndex = undoFromHistory();
		    }
		    
		    //find(RSet, Position, Pattern, RuleNumber, IterationNumber, 
		    //     Successful, StartIndex, Direction, FoundIndex, FactInfo)
		    Bindings bindings = new Bindings();
		    bindings.bind("RSet", selectedRSet);
		    bindings.bind("Position", selectedPosition);
		    bindings.bind("Pattern", selectedPattern);
		    bindings.bind("RuleNumber", selectedRuleNumber);
		    bindings.bind("IterationNumber", selectedIterationNumber);
		    bindings.bind("Successful", selectedSuccessful);
		    bindings.bind("Highlight", selectedHighlight);
		    bindings.bind("StartIndex", selectedStartIndex);
		    bindings.bind("Direction", selectedSearchDirection);
		    bindings.bind("LineLength", paneWidthInChars);

		    QueryAnswer answer =
			session.executeQuery("find(RSet, Position, Pattern, RuleNumber, IterationNumber, Successful, " +
					             "Highlight, StartIndex, " +
								 "Direction, LineLength, FoundIndex, SearchStatus, ApplicationInfo, HighlightingInfo)", 
					     bindings);
		    Term search_status = answer.getValue("SearchStatus");
		    Term indexResult = answer.getValue("FoundIndex");
		    
		    if (search_status != null && indexResult != null && indexResult.isInteger() ) {
		    	searchStatusText.setText( ( (PBString) search_status ).getString() );
		    	currentRecordIndex = indexResult.intValue(); 
		    	updateHistory( currentRecordIndex );
		    	} 
		    else {
		    	searchStatusText.setText("Error: " + answer.getError() );
		    }
		    
		    Term applicationInfo = answer.getValue("ApplicationInfo");
		    Term highlightingInfo = answer.getValue("HighlightingInfo");
		    if (applicationInfo != null && applicationInfo.isCompound() && applicationInfo.getArity() == N_DISPLAY_PANES ) {
		    	for ( int i = 0 ; i < N_DISPLAY_PANES; i++ ) {
		    		Term arg = applicationInfo.getArgument(i + 1);
		    		if ( arg.isString() ) {
		    			DisplayTextPanes[i].setText( ( (PBString) arg ).getString() );
		    			DisplayTextPanes[i].setCaretPosition(0);
		    		} 
		    		else {
		    			DisplayTextPanes[i].setText( "" );
		    		}
		    	}		    	
		    	doHighlighting(highlightingInfo);
		    	} 
		    else if ( applicationInfo.isAtomic() && ( applicationInfo.getName() ).equals("no_match_found") ) {
		    	for ( int i = 0 ; i < N_DISPLAY_PANES; i++ ) {
		    		DisplayTextPanes[i].setText( DisplayTextSave[i] );
		    		DisplayLabels[i].setIcon( null );
		    		DisplayTextPanes[i].setCaretPosition(0);
		    	}
		    }
		    else {
		    	searchStatusText.append("\nError: can't unpack application info" );
		    }
		} catch (Exception e) {
			searchStatusText.setText("Error when querying Prolog Server: " + e.getMessage() + '\n');
		    e.printStackTrace();
		    }
		}
	
		private void doHighlighting(Term highlightingInfo) {
			if ( highlightingInfo.isCompound() && highlightingInfo.getArity() > 0 ) {	
				for ( int i = 1 ; i <= highlightingInfo.getArity() ; i++ ) {
					Term arg = highlightingInfo.getArgument(i);
					if ( arg.isCompound() || arg.isAtom() ) {
						String type = arg.getName();
						if ( type.equals("highlighted")) {
							highlightMatchingTermsOnAllPanes(arg, HIGHLIGHTING_COLOR);
						}
						else if ( type.equals("consumed") ) {
							doHighlightingOnPane(DisplayTextPanes[INPUT_FACTS], arg, CONSUMED_COLOR);
						}
						else if ( type.equals("produced") ) {
							doHighlightingOnPane(DisplayTextPanes[OUTPUT_FACTS], arg, PRODUCED_COLOR);
						}
					}
				}
			}
		}
	
		private void highlightMatchingTermsOnAllPanes(Term matchingPatternInfo, Color color) {
			//System.out.println("\nStart highlightMatchingTerms");
			for ( int pane = 0 ; pane < N_DISPLAY_PANES; pane++ ) {
				DisplayLabels[pane].setIcon( null );
				boolean didHighlightingOnPane = doHighlightingOnPane(DisplayTextPanes[pane], matchingPatternInfo, color);
				if ( didHighlightingOnPane ) {
					DisplayLabels[pane].setIcon( HIGHLIGHTED_PANE_ICON );
					DisplayTextPanes[pane].setCaretPosition(0);
				}
				else {
					DisplayLabels[pane].setIcon( null );
				}
			}
		}
		
		private boolean doHighlightingOnPane(JTextComponent pane, Term highlightingTerm, Color color) {
			boolean didHighlightingOnPane = false;
			if ( highlightingTerm.isCompound() && highlightingTerm.getArity() > 0 ) {	
				for ( int i = 1 ; i <= highlightingTerm.getArity() ; i++ ) {
					Term arg = highlightingTerm.getArgument(i);
					if ( arg.isString() ) {
						String matchingPattern = ( (PBString) arg ).toString();												
						boolean didHighlighting = highlight(pane, matchingPattern, color);
						if ( didHighlighting ) {
							didHighlightingOnPane = true;
						}
			    	}
					else {
						System.out.println("\nError: argument in MatchingPatternInfo not a string\n");
					}
				}
			}
			return didHighlightingOnPane;
		}
				
		private void addDisplayComponent( 
				Component component, 
				int row, int column, 
				int width, int height, 
				int weightx, int weighty)
		   {
			displayConstraints.gridx = column; // set gridx
			displayConstraints.gridy = row; // set gridy
			displayConstraints.gridwidth = width; // set gridwidth
			displayConstraints.gridheight = height; // set gridheight
			displayConstraints.weightx = weightx;  // can grow wider
			displayConstraints.weighty = weighty;     // can grow taller
			displayLayout.setConstraints( component, displayConstraints ); // set constraints
			displayPanel.add( component ); // add component
		   } 
		
		private void addButtonToPanel( Component component, int row, int column )
		   {
			displayConstraints.gridx = column; // set gridx
			displayConstraints.gridy = row; // set gridy
			displayConstraints.gridwidth = 1; // set gridwidth
			displayConstraints.gridheight = 1; // set gridheight
			displayConstraints.weightx = 0;  // can grow wider
			displayConstraints.weighty = 0;     // can grow taller
			buttonLayout.setConstraints( component, displayConstraints ); // set constraints
			executePanel.add( component ); // add component
		   } 
		
		private void updateHistory( int new_index ) {
			if ( history_pointer <  MAX_HISTORY_LENGTH ) {
				// Don't update history if we haven't changed position since last time.
				if ( history_pointer >= 1 && history[ history_pointer - 1 ] == new_index ) {
					return;
				}
				else {
					history[ history_pointer ] = new_index;
					history_pointer++;
				}
			}
		}
		
		private int undoFromHistory() {
			if ( history_pointer >= 2 ) {
				history_pointer -= 2;
				int old_index = history[ history_pointer ];
				return old_index;
			}
			else if ( history_pointer == 1 ){
				return history[ 0 ];
			}
			else {
				return 1;
			}
		}
	
		// For AnswerCheckBox event handling
		private class AnswerCheckBoxHandler implements ItemListener 
		{
			public void itemStateChanged( ItemEvent event )
			{
				for (int i = 0; i < N_DISPLAY_PANES ; i++ ) {
					if (event.getSource() == DisplayCheckBoxes[i] ) {
						DisplayLabels[i].setVisible(DisplayCheckBoxes[i].isSelected());
						DisplayScrollPanes[i].setVisible(DisplayCheckBoxes[i].isSelected());
					}
					}
	    	  
			} 
		} 
		
		// For "Reload" button
		private class ReloadButtonListener implements ActionListener 
		{
			public void actionPerformed(ActionEvent event) {
				ReloadTraceFile();
				InitialiseRSetListFromProlog();
			}
		}
		
		// For "Exit" button
		private class ExitButtonListener implements ActionListener
		{
			public void actionPerformed(ActionEvent event) {
				exitApp();
			}
		}
		
		// For "Find Next" button
		private class FindNextButtonListener implements ActionListener 
		{
			public void actionPerformed(ActionEvent event) {
				findAndDisplayRecord( SEARCH_FORWARDS );
			}
		}
		
		// For "Find Previous" button
		private class FindPreviousButtonListener implements ActionListener 
		{
			public void actionPerformed(ActionEvent event) {
				findAndDisplayRecord( SEARCH_BACKWARDS );
			}
		}
		
		// For "Start" button
		private class StartButtonListener implements ActionListener 
		{
			public void actionPerformed(ActionEvent event) {
				findAndDisplayRecord( START );
			}
		}
		
		// For "End" button
		private class EndButtonListener implements ActionListener 
		{
			public void actionPerformed(ActionEvent event) {
				findAndDisplayRecord( END );
			}
		}
		
		// For "Next" button
		private class NextButtonListener implements ActionListener 
		{
			public void actionPerformed(ActionEvent event) {
				findAndDisplayRecord( NEXT );
			}
		}
		
		// For "Previous" button
		private class PreviousButtonListener implements ActionListener 
		{
			public void actionPerformed(ActionEvent event) {
				findAndDisplayRecord( PREVIOUS );
			}
		}
		
		// For "Redisplay" button
		private class RedisplayButtonListener implements ActionListener 
		{
			public void actionPerformed(ActionEvent event) {
				findAndDisplayRecord( CURRENT );
			}
		}
		
		// For "Undo" button
		private class UndoButtonListener implements ActionListener 
		{
			public void actionPerformed(ActionEvent event) {
				findAndDisplayRecord( UNDO );
			}
		}		
		
		// Stuff for highlighting - slightly modified version of code from 
		// http://javaalmanac.com/egs/javax.swing.text/style_HiliteWords.html
		
		// Creates highlights around all occurrences of pattern in textComp
	    public boolean highlight(JTextComponent textComp, String pattern, Color color) {
	        // Include this if we want to remove old highlights first
	        //removeHighlights(textComp);
	    
	        try {
	            DefaultHighlighter hilite = (DefaultHighlighter) textComp.getHighlighter();
	            AbstractDocument doc = (AbstractDocument) textComp.getDocument();
	            String text = doc.getText(0, doc.getLength());
	            int pos = 0;
	            boolean didHighlighting = false;
	    
	            // Search for pattern
	            while ((pos = text.indexOf(pattern, pos)) >= 0) {
	                // Create highlighter using private painter and apply around pattern
	            	MyHighlightPainter highlightPainter = new MyHighlightPainter( color );
	                hilite.addHighlight(pos, pos+pattern.length(), highlightPainter);
	                pos += pattern.length();
	                didHighlighting = true;
	            }
	            return didHighlighting;
	        } catch (Exception e) {
	        	return false;
	        }
	    }
	    
//	  Removes only our private highlights
	    public void removeHighlights(JTextComponent textComp) {
	        DefaultHighlighter hilite = (DefaultHighlighter) textComp.getHighlighter();
	        DefaultHighlighter.Highlight[] hilites = hilite.getHighlights();
	    
	        for (int i=0; i<hilites.length; i++) {
	            if (hilites[i].getPainter() instanceof MyHighlightPainter) {
	                hilite.removeHighlight(hilites[i]);
	            }
	        }
	    }
	    
	    // An instance of the private subclass of the default highlight painter
	    MyHighlightPainter myHighlightPainter = new MyHighlightPainter(Color.red);
	    
	    // A private subclass of the default highlight painter
	    private class MyHighlightPainter extends DefaultHighlighter.DefaultHighlightPainter {
	        public MyHighlightPainter(Color color) {
	            super(color);
	        }
	    }
	    
	    public void exitApp() {
	    	frame.setVisible(false);
	        System.exit(0);
	    }

		public static void main(String[] args) {
			if ( args.length != 1 ) {
				System.out.println("Usage: java Browser <Port>");
			}
			else {
				int port = Integer.parseInt(args[0].trim());
				System.out.println("Starting browser client on port " + port);
				new Browser(port);
			}
		}
		

}
