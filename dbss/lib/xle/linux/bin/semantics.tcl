# (c) 2004-2006 by the Palo Alto Research Center.  All rights reserved.


###############################################################################
###############################################################################
# 
#  Interface to glue and semantic transfer
#
##############################################################################
###############################################################################
add-help-doc {stdout-to-file "stdout-to-file command file"\
		  "Run command, redirecting standard output to file."}

add-help-doc {regression-help "regression-help" \
	    "Separate help command for regression test commands"}
add-help-doc {semantics-help "semantics-help" \
	    "Separate help command for semantics commands"}

proc add-reg-help-doc {doc} {
    global reg_help_doc
    lappend reg_help_doc $doc
}
proc add-sem-help-doc {doc} {
    global sem_help_doc
    lappend sem_help_doc $doc
}

proc regression-help {{command "help"}} {
    global reg_help_doc
    if {$command == "help"} {
	puts "help <command>"
	puts "Displays help for a particular regression test command."
	puts -nonewline "Commands known: "
	set first 1
	set length 16
	foreach doc $reg_help_doc {
	    if {!$first} {
		puts -nonewline ", "
	    }
	    set first 0
	    set name [lindex $doc 0]
	    set delta [expr {[string length $name]+2}]
	    incr length $delta
	    if {$length > 75} {
		puts ""
		set length $delta
	    }
	    puts -nonewline "$name"
	}
	puts ""
	return
    }
    foreach doc $reg_help_doc {
	set name [lindex $doc 0]
	if {![string match $command $name]} {continue}
	foreach line [lrange $doc 1 end] {
	    puts $line
	}
	puts ""
    }
}
proc semantics-help {{command "help"}} {
    global sem_help_doc
    if {$command == "help"} {
	puts "help <command>"
	puts "Displays help for a particular semantics command."
	puts -nonewline "Commands known: "
	set first 1
	set length 16
	foreach doc $sem_help_doc {
	    if {!$first} {
		puts -nonewline ", "
	    }
	    set first 0
	    set name [lindex $doc 0]
	    set delta [expr {[string length $name]+2}]
	    incr length $delta
	    if {$length > 75} {
		puts ""
		set length $delta
	    }
	    puts -nonewline "$name"
	}
	puts ""
	return
    }
    foreach doc $sem_help_doc {
	set name [lindex $doc 0]
	if {![string match $command $name]} {continue}
	foreach line [lrange $doc 1 end] {
	    puts $line
	}
	puts ""
    }
}



set reg_help_doc {
    {create-ts-menu "create-ts-menu" \
	 "Install menu item for adding results to test suite"
    }
    {set-testsuite "set-testsuite <dir>" \
	    "Set <dir> as current test suite directory"
    }
    {run-syn-testsuite  \
	 "run-syn-testsuite (<from> <to>)" \
	 "Run text => fstructure testsuite on current testsuite directory" \
	 " (on sentences <from> to <to>)"
    }
    {run-xfs-testsuite  \
	 "run-xfs-testsuite (<from> <to>)" \
	 "Run text => transferred-fstructure testsuite on current testsuite directory "\
	 "(on sentences <from> to <to>)"
    }
    {run-xfr-testsuite  \
	 "run-xfr-testsuite (<from> <to>)" \
	 "Run text => transfer-structure testsuite on current testsuite directory "\
	 "(on sentences <from> to <to>)"
    }
    {run-multi-xfr-testsuite  \
	 "run-multi-xfr-testsuite (<from> <to>)" \
	 "Run text => transfer-structure testsuite on current testsuite directory "\
	 "(on sentences <from> to <to>)"
    }
    {run-synxfs-testsuite  \
	 "run-synxfs-testsuite (<from> <to>)" \
	 "Run fs => transferred-fstructure testsuite on current testsuite directory "\
	 "(on sentences <from> to <to>)"
    }
    {run-synxfr-testsuite  \
	 "run-synxfr-testsuite (<from> <to>)" \
	 "Run fs => transfer-structure testsuite on current testsuite directory "\
	 "(on sentences <from> to <to>)"
    }
    {run-syn-multi-xfr-testsuite  \
	 "run-syn-multi-xfr-testsuite (<from> <to>)" \
	 "Run fs => transfer-structure testsuite on current testsuite directory "\
	 "(on sentences <from> to <to>)"
    }
    {run-sem-testsuite  \
	 "run-sem-testsuite (<from> <to>)" \
	 "Run text => semantics testsuite on current testsuite directory "\
	 "(on sentences <from> to <to>)"
    }
    {run-synsem-testsuite  \
	 "run-synsem-testsuite (<from> <to>)" \
	 "Run fs => semantics testsuite on current testsuite directory "\
	 "(on sentences <from> to <to>)"
    }
    {set-testsuite-most-probable "set-testsuite-most-probable 1|0" \
    "If 1, testsuites will compare most probable parse against benchmark"
    }
    {set-testsuite-partial-benchmark  \
    "set-testsuite-partial-benchmark 1|0" \
    "If 1, testsuite benchmarks are construed as partial specifications"
    }
    {match-files  \
	 "match-files <source> <target>" \
	 "Run structure matching on two transfer files."
    } 
    {match-dep-files  \
	 "match-dep-files <source> <target>" \
	 "Run structure matching on two dependency (triples) files."
    } 
    {match-sem-files  \
	 "match-sem-files <source> <target>" \
	 "Run structure matching on two semantics files."
    } 
    {match-fs-files  \
	 "match-fs-files <source> <target>" \
	 "Run structure matching on two fstructure files, "\
	 "first converting to dependency triples via current transfer rules."
    } 
    {match-fs  \
	 "match-fs <Sentence1> <Sentence2>" \
	 "Parse sentences, then match fstructures."
    }
    {match-sem  \
	 "match-sem <Sentence1> <Sentence2>" \
	 "Parse and interpret sentences, then match semantics."
    }
}


set sem_help_doc {
    {create-semkr "create-semkr" \
	    "Install menu items for semantics and kr"
    }
    {fs  \
	 "fs <Sentence1> (<File>)" \
	 "Silently parse sentence, and write most probable fstructure to File "\
	 "(fs_out in current directory, if not specified)."
    }
    {packed-fs  \
	 "packed-fs <Sentence1> (<File>)" \
	 "Silently parse sentence, and write packed fstructure to File "\
	 "(fs_out in current directory, if not specified)."
    }
    {xml-fs  \
	 "xml-fs <Sentence1> (<File>)" \
	 "Silently parse sentence, and write most probable fstructure to File "\
	 "(fs_out.xml in current directory, if not specified)."
    }
    {packed-xml-fs  \
	 "packed-xml-fs <Sentence1> (<File>)" \
	 "Silently parse sentence, and write packed fstructure to File "\
	 "(fs_out.xml in current directory, if not specified)."
    }
    {sem  \
	 "sem <Sentence1> (<File>)" \
	 "Silently parse and interpret most probable fs, write semantics to File "\
	 "(sem_out in current directory, if not specified)."
    }
    {packed-sem  \
	 "packed-sem <Sentence1> (<File>)" \
	 "Silently parse and interpret packed fs, write packed semantics to File "\
	 "(sem_out in current directory, if not specified)."
    }
    {xml-sem  \
	 "xml-sem <Sentence1> (<File>)" \
	 "Silently parse and interpret most probable fs, write semantics to File "\
	 "(sem_out.xml in current directory, if not specified)."
    }
    {packed-xml-sem  \
	 "packed-xml-sem <Sentence1> (<File>)" \
	 "Silently parse and interpret packed fs, write packed semantics to File "\
	 "(sem_out.xml in current directory, if not specified)."
    }
    {xfr  \
	 "xfr <Sentence1> (<File>)" \
	 "Silently parse and transfer most probable fs, write xfr to File "\
	 "(xfr_out in current directory, if not specified)."
    }
    {packed-xfr  \
	 "packed-xfr <Sentence1> (<File>)" \
	 "Silently parse and transfer packed fs, write xfr to File "\
	 "(xfr_out in current directory, if not specified)."
    }
    {xml-xfr  \
	 "xml-xfr <Sentence1> (<File>)" \
	 "Silently parse and transfer most probable fs, write xfr to File "\
	 "(xfr_out.xml in current directory, if not specified)."
    }
    {packed-xml-xfr  \
	 "xml-packed-xfr <Sentence1> (<File>)" \
	 "Silently parse and transfer packed fs, write xfr to File "\
	 "(xfr_out.xml in current directory, if not specified)."
    }
    {triples  \
	 "triples <Sentence1> (<File>)" \
	 "Silently parse and transfer most probable fs, write triples to File "\
	 "(triples_out in current directory, if not specified)."
    }
    {packed-triples  \
	 "packed-triples <Sentence1> (<File>)" \
	 "Silently parse and transfer packed fs, write triples to File "\
	 "(triples_out in current directory, if not specified)."
    }
    {xml-triples  \
	 "xml-triples <Sentence1> (<File>)" \
	 "Silently parse and transfer most probable fs, write triples to File "\
	 "(triples_out in current directory, if not specified)."
    }
    {packed-xml-triples  \
	 "packed-xml-triples <Sentence1> (<File>)" \
	 "Silently parse and transfer packed fs, write triples to File "\
	 "(triples_out in current directory, if not specified)."
    }
}






# Make documentation conditional on being in PARC, on linux2.3:
if {[info exists env(XLEPATH)]} {
    if {$env(XLEPATH) == "/project/xle/external/linux2.3"} {

add-reg-help-doc \
    {run-kr-testsuite  \
	 "run-kr-testsuite (<from> <to>)" \
"Run text => KR testsuite on current testsuite directory \
 (on sentences <from> to <to>)"
    }

add-reg-help-doc \
    {run-semkr-testsuite \
"run-semkr-testsuite (<from> <to>)" \
"Run semantics => KR testsuite on current testsuite directory \
(on sentences <from> to <to>)"
    }

add-reg-help-doc \
    {run-synkr-testsuite \
	 "run-synkr-testsuite (<from> <to>)" \
"Run fs => KR testsuite on current testsuite directory \
(on sentences <from> to <to>)"
    }

add-reg-help-doc \
    {match-kr-files  \
	 "match-kr-files <source> <target>" \
	 "Run structure matching on two KR files."
    } 

add-reg-help-doc \
    {match-kr  \
	 "match-kr <Sentence1> <Sentence2>" \
	 "Parse sentences and map to KR , then match KR."
    }

add-reg-help-doc \
    {match  \
	 "match <Sentence1> <Sentence2>" \
"Parse sentences, do semantics and map to KR ,  \
 then match fs, sem and KR."
    }

}}







# Make documentation conditional on being in PARC, on linux2.3:
if {[info exists env(XLEPATH)]} {
    if {$env(XLEPATH) == "/project/xle/external/linux2.3"} {

add-sem-help-doc {kr  \
"kr (<Sentence1> [<File>])" \
"Silently parse and map most probable fs to KR, write KR to File \
  (kr_out in current directory, if not specified)."}

add-sem-help-doc {packed-kr  \
"packed-kr (<Sentence1> [<File>])" \
"Silently parse and map packed fs to KR, write packed KR to File \
  (kr_out in current directory, if not specified)."}

add-sem-help-doc {xml-kr  \
"xml-kr (<Sentence1> [<File>])" \
"Silently parse and map most probable fs to KR, write KR to File \
  (kr_out.xml in current directory, if not specified)."}

add-sem-help-doc {packed-xml-kr  \
"packed-xml-kr (<Sentence1> [<File>])" \
"Silently parse and map packed fs to KR, write packed KR to File \
  (kr_out.xml in current directory, if not specified)."}

}}


############################################################
# Set new tcl variables
############################################################

set useGlue 1
set printSemantics 1

set prover chart_prover
set prettyPrint 1
set verboseOutput 0
set outScope 1
set pruneEquiv 1
set pickFirstScope 1
set skolemize 1
set pickFirstParse 0
set robust 0

#set sempipe0 [glob ~/]/.sempipe0
#set sempipe1 [glob ~/]/.sempipe1
#set sempinged 0
#set sempingpipe [glob ~/]/.sem_ping
#set glueListener 0
set sempipe0 /tmp/sempipe0_[pid]
set sempipe1 /tmp/sempipe1_[pid]
set sempinged 0
set sempingpipe /tmp/sem_ping_[pid]
set glueListener 0

proc use-glue {{value 1}} {
    global useGlue

    set useGlue $value
    prolog "setp(use_glue($value))."
}


############################################################
#  Set up menus
############################################################

proc create-semkr {} {
    global glueListener
    global transferListener
    global useGlue

    set glueListener 0
    set transferListener 0
    if {$useGlue == 1 } {
	install-semkr-menu-items
    } else {
	install-nonglue-semkr-menu-items
    }
}


proc create-semkr-listener {} {
    global glueListener
    global transferListener
    global useGlue

    set glueListener 1
    set transferListener 1
    if {$useGlue == 1 } {
	install-semkr-menu-items
    } else {
	install-nonglue-semkr-menu-items
    }
}



# Make documentation conditional on being in PARC, on linux2.3:
if {[info exists env(XLEPATH)]} {
    if {$env(XLEPATH) == "/project/xle/external/linux2.3"} {
	add-help-doc {create-semkr "create-semkr" \
	    "Install menu items for semantics and kr"}
    }
}


proc install-semkr-menu-items {} {
    global fsCommands fsViews fsChartCommands fsChartViews

    add-item-to-xle-menu \
	{command -label "Semantics" \
	     -command "send-to-glue 0 0 $self" \
	     -doc "Derives glue-language semantic readings."} \
	fsCommands

    add-item-to-xle-menu \
	{command -label "Semantics2KR" \
	     -command "send-to-glue 0 1 $self" \
	     -doc "Convert skolemized semantics to KR."} \
	fsCommands

    add-item-to-xle-menu \
	{command -label "Semantics" \
	     -command "send-to-glue 1 0 $self" \
	     -doc "Derives glue-language semantic readings."} \
	fsChartCommands

    add-item-to-xle-menu \
      {command -label "Semantics2KR" \
	       -command "send-to-glue 1 1 $self" \
	       -doc "Transfer skolemized semantics."} \
	fsChartCommands

    add-item-to-xle-menu \
	{command -label "Transfer" \
	     -command "send-to-transfer 0 $self" \
	     -doc "Send f-structure through transfer."} \
	fsCommands

    add-item-to-xle-menu \
	{command -label "Transfer" \
	     -command "send-to-transfer 1 $self" \
	     -doc "Send f-structure through transfer."} \
	fsChartCommands

    add-item-to-xle-menu \
	{command -label "Load Glue Lexicon" \
		 -command "glue-command-to-transfer reload_glue." \
		 -doc "Loads transfer rules."} \
	fsCommands

    add-item-to-xle-menu \
	{command -label "Load Transfer Rules" \
		 -command "glue-command-to-transfer reload." \
		 -doc "Loads transfer rules."} \
	fsCommands

    add-item-to-xle-menu \
	{command -label "Debug Transfer" \
		 -command "glue-command-to-transfer debug." \
		 -doc "Loads transfer rules."} \
	fsCommands

    add-item-to-xle-menu \
	{command -label "Debug Off" \
		 -command "glue-command-to-transfer no_debug." \
		 -doc "Loads transfer rules."} \
	fsCommands

    add-item-to-xle-menu \
	{command -label "Break Loop" \
		 -command "glue-command-to-transfer break." \
		 -doc "Breaks out of transfer listener."} \
	fsCommands

    ##############################################################

    add-item-to-xle-menu \
	{radiobutton -label "chart_prover" \
	     -variable "prover" \
	     -doc "Selects chart-based Horn clause prover."} \
	fsViews

    add-item-to-xle-menu \
	{radiobutton -label "net_prover" \
	     -variable "prover" \
	     -doc "Selects proof net prover."} \
	fsViews

    add-item-to-xle-menu \
	{checkbutton -label "robust" \
	     -variable "robust" \
	     -doc "Robust semantic interpretation." \
	     -default "set robust 0"} \
	fsViews

    add-item-to-xle-menu \
	{checkbutton -label "verbose output" \
	     -variable "verboseOutput" \
	     -doc "Toggles display of verbose output." \
	     -default "set verboseOutput 0"} \
	fsViews

    add-item-to-xle-menu \
	{checkbutton -label "first scope" \
	     -variable "pickFirstScope" \
	     -doc "Derive all or first scopings." \
	     -default "set pickFirstScope 1"} \
	fsViews

    add-item-to-xle-menu \
	{checkbutton -label "apply scope constraints" \
	     -variable "outScope" \
	     -doc "Applies scoping constraints." \
	     -default "set outScope 1"} \
	fsViews

    add-item-to-xle-menu \
	{checkbutton -label "check equivalents" \
	     -variable "pruneEquiv" \
	     -doc "Prune out detectable meaning equivalents." \
	     -default "set pruneEquiv 1"} \
	fsViews

    add-item-to-xle-menu \
	{checkbutton -label "skolemize" \
	     -variable "skolemize" \
	     -doc "Pick first meaning derived, and skolemize." \
	     -default "set skolemize 1"} \
	fsViews


    add-item-to-xle-menu \
	{checkbutton -label "first parse" \
	     -variable "pickFirstParse" \
	     -doc "Semantics for first interpretable parse" \
	     -default "set pickFirstParse 0"} \
	fsChartViews



}






proc install-nonglue-semkr-menu-items {} {
    global fsCommands fsViews fsChartCommands fsChartViews

    add-item-to-xle-menu \
	{command -label "Semantics" \
	     -command "send-to-glue 0 0 $self" \
	     -doc "Derives transfer-based semantic readings."} \
	fsCommands

    add-item-to-xle-menu \
	{command -label "Semantics2KR" \
	     -command "send-to-glue 0 1 $self" \
	     -doc "Transfer semantics to KR."} \
	fsCommands

    add-item-to-xle-menu \
	{command -label "Semantics" \
	     -command "send-to-glue 1 0 $self" \
	     -doc "Derives transfer-based semantic readings."} \
	fsChartCommands

    add-item-to-xle-menu \
      {command -label "Semantics2KR" \
	       -command "send-to-glue 1 1 $self" \
	       -doc "Transfer semantics to KR."} \
	fsChartCommands

    add-item-to-xle-menu \
	{command -label "Reload Semantic Rules" \
		 -command "reload-semrules" \
		 -doc "Reloads transfer rules."} \
	fsCommands

    add-item-to-xle-menu \
	{command -label "Reload KR Rules" \
		 -command "reload-krrules" \
		 -doc "Reloads transfer rules."} \
	fsCommands

    add-item-to-xle-menu \
	{command -label "Debug Transfer" \
		 -command "glue-command-to-transfer debug." \
		 -doc "Turn on tracing of transfer."} \
	fsCommands

    add-item-to-xle-menu \
	{command -label "Debug Off" \
		 -command "glue-command-to-transfer no_debug." \
		 -doc "Turn off tracing of transfer."} \
	fsCommands


}



proc load-semrules {file} {
    prolog "load_semrules('$file')."
}
proc load-krrules {file} {
    prolog "load_krrules('$file')."
}

proc reload-semrules {} {
    prolog "reload_semrules."
}
proc reload-krrules {} {
    prolog "reload_krrules."
}



############################################################




proc glue-command-to-transfer {command} {
    global glueListener
    global useGlue
    global sempipe0
    global sempipe1
    global sempinged
    global sempingpipe

    if {$glueListener == 0} {
	set plgcmd "run_semantics('$command', '$command')."
	set ack [prolog $plgcmd]
	if {$ack == 1} {
	    return 1
	} else {
	    puts stderr "Semantics failed."
	    return 0
	}
    } else {
	if {$sempinged == 0} {
	    set semping 1
	} else {
	    set semping 0
	}

	set commandString1 "echo '$command' > '$sempipe1'"
	set commandString2 "echo '$command' > '$sempipe0'"
	exec sh -c $commandString1
	exec sh -c $commandString2

	if {$sempinged == 0} {
	    check-semping-result $sempingpipe
	}
    }
}




proc check-semping-result {sempingpipe} {
  global sempipe0
  global sempinged

  exec sh -c "sleep 1"

  if [file exists $sempingpipe] {
     exec sh -c "echo $sempingpipe | rm -f"
     set sempinged  1
  } else {
     puts stderr "Unable to contact listener via file $sempipe0"
     set sempinged  0
  }
}


proc send-to-glue {packed transfer window} {
    global useGlue

    if {$useGlue == 1} {
	send-to-glue-on $packed $transfer $window
    } else {
	send-to-glue-off $packed $transfer $window
    }
}


#-------------------
# version for when useGlue = 1
#-------------------


proc send-to-glue-on {packed transfer window} {

    global glueListener

    global prettyPrint
    global verboseOutput
    global prover
    global robust
    global outScope
    global pruneEquiv
    global skolemize
    global pickFirstScope
    global pickFirstParse
    global sempipe0
    global sempipe1
    global sempinged
    global sempingpipe

    if {$transfer == 1} {
	set sk 1
	set firstScope 1
    } else {
	set sk $skolemize
	set firstScope $pickFirstScope
    }

    if {$sempinged == 0} {
	set ping 1
    } else {
	set ping 0
    }

    set params "\
	params(\[packed($packed), sem2kr(0), sem2xfr($transfer), \
	print_to_file(0), prover($prover),\
        pretty($prettyPrint),verbose($verboseOutput),\
	addscope(0),  prune_equivs($pruneEquiv),\
        first_interpretable_parse($pickFirstParse), \
        first_scope($firstScope), outscope($outScope), \
        skolem($sk), robust_semantics($robust), ping($ping),\
        discourse(0), reset_disc(0), rollback_disc(0)\])."


    if {$glueListener == 0} {
	set data [get-window-prop $window data]
	set plgcmd "run_semantics('$data', '$params')."
	set ack [prolog $plgcmd]
	if {$ack == 1} {
	    return 1
	} else {
	    puts stderr "Semantics failed."
	    return 0
	}

    } else {

	
	if {$packed} {
	    print-prolog-chart-graph $sempipe0 $window
	} else {
	    print-fs-as-prolog $sempipe0 $window
	}

	set fileId [open $sempipe1 w]
	puts $fileId $params
	close $fileId

	if {$sempinged == 0} {
	    check-semping-result $sempingpipe
	}
    }
}






#-------------------
# versions for when useGlue = 0
#-------------------


proc send-to-glue-off {packed kr window} {

    global glueListener

    global sempipe0
    global sempipe1
    global sempinged
    global sempingpipe

    global printSemantics


    if {$kr == 1} {
	set command [get_kr_display_modes] 
    } else {
	set command [get_sem_display_modes] 
    }

    if {$glueListener == 0} {
	set data [get-window-prop $window data]
	set plgcmd "run_semantics('$data', '$command')."
	set ack [prolog $plgcmd]
	if {$ack != 1} {
	    puts stderr "Semantics failed."
	    return 0
	}
    } else {
	if {$sempinged == 0} {
	    set ping 1
	} else {
	    set ping 0
	}
	
	if {$packed} {
	    print-prolog-chart-graph $sempipe0 $window
	} else {
	    print-fs-as-prolog $sempipe0 $window
	}

	set fileId [open $sempipe1 w]
	puts $fileId $command
	close $fileId

	if {$sempinged == 0} {
	    check-semping-result $sempingpipe
	}
    }
    display_sem_kr_output $kr
}

#########################################################
#  Displaying semantics and KR results
#########################################################

set semMode file
set semDisplayFile /tmp/semout[pid]
set semPP sem-pp
set krMode file
set krDisplayFile /tmp/krout[pid]
set krPP kr-pp
set krDisplayFont ""

proc get_sem_display_modes {} {
    global semMode krMode
    global semDisplayFile

    if {$semMode == "file"} {
	return "xfr_sem(sem(\\\'$semDisplayFile\\\'))."
    } elseif {$semMode == "null" && $krMode == "file"} {
	return "xfr_sem(sem(\\\'$semDisplayFile\\\'))."
    } else {
	return "xfr_sem(sem(stdout))."
    }
}

proc get_kr_display_modes {} {
    global semMode
    global semDisplayFile
    global krMode
    global krDisplayFile

    if {$semMode == "file"} {
	set psemMode $semDisplayFile
    } else {
	set psemMode $semMode
    }
     if {$krMode == "file"} {
	set pkrMode $krDisplayFile
    } else {
	set pkrMode $krMode
    }
    return "xfr_sem_kr(sem(\\\'$psemMode\\\'), kr(\\\'$pkrMode\\\'))."
}


proc display_sem_kr_output {kr} {
    global semPP
    global krPP
    global semMode
    global semDisplayFile
    global krMode
    global krDisplayFile

    if {$semMode == "file"} {
	display_sem_output
    } elseif {$semMode == "null" && $kr != 1 && $krMode == "file"} {
	display_sem_output
    }
    if {$krMode == "file" && $kr == 1} {
	display_kr_output
    } 
}

proc display_sem_output {} {
    global semPP
    global semDisplayFile
    global krDisplayFont

    set cmd [list $semPP $semDisplayFile $semDisplayFile.pp]
    eval $cmd
    display-file  "$semDisplayFile.pp" ".semantics" upperright $krDisplayFont
    file delete $semDisplayFile
    file delete $semDisplayFile.pp
}

proc display_kr_output {} {
    global krPP
    global krDisplayFile
    global krDisplayFont

    if {[prolog "getp(cycL(1))."]} {
	display-file "$krDisplayFile" ".cycL" lowerright $krDisplayFont
	file delete $krDisplayFile
    } else {	
	set cmd [list $krPP $krDisplayFile $krDisplayFile.pp]
	eval $cmd
	display-file "$krDisplayFile.pp" ".akr" lowerright $krDisplayFont
	file delete $krDisplayFile
	file delete $krDisplayFile.pp
    }
}


# default value of semPP    
proc sem-pp {infile outfile} {
    file copy -force $infile $outfile
    #prolog "sem_pp('$infile', '$outfile')."
}

# default value of krPP    
proc kr-pp {infile outfile} {
    file copy -force $infile $outfile
    #prolog "kr_pp('$infile', '$outfile')."
}

proc create-text-file-window {self {size ""} {position ""}} {
    # self must be a window name (begins with a period)
    # size must be of the form 50x25 (measured in characters)
    # position must be of the form +100+100 (measured in pixels)
    # position can also be upperleft, upperright, lowerleft, or lowerright

    global window_width window_height
    if {[winfo exists $self]} {
	destroy $self
    }
    toplevel $self
    frame $self.panel
    pack $self.panel -in $self -anchor nw -side top
    if {$position == "" ||
	$position == "upperleft" ||
	$position == "upperright" ||
	$position == "lowerleft" ||
	$position == "lowerright"} {
	set-window-prop $self location $position
	set position [window-position $self]
    }
    set-window-prop $self position $position
    if {$size == ""} {
	set size 50x25
    }
    text $self.t -bg white -setgrid true -wrap none -width 80 -height 25 \
	-yscrollcommand [list $self.ys set] \
	-xscrollcommand [list $self.xs set]
    scrollbar $self.ys -command [list $self.t yview] -orient vertical
    scrollbar $self.xs -command [list $self.t xview] -orient horizontal
    pack $self.ys -side left -fill y
    pack $self.xs -side bottom -fill x
    pack $self.t -side right -fill both -expand true

    wm minsize $self 20 4
    if {$position != "" && [string index $position 0] != "+"} {
	set position "+$position"
    }
    wm geometry $self $size$position
    add-kill-button $self $self.panel
}

proc display-file {file window {position ""} {font ""} {size ""} } {
    # file is a filename
    # window must begin with a period
    # font can be of the form -adobe-courier-medium-r-normal--12-120*
    # font can also be xleuifont, xlefsfont, xletextfont, or xletreefont
    # if the latter is used, then try-new-look will affect it

    if {[winfo exists $window]==0} {
	create-text-file-window $window $size $position
    } else {
	raise $window
    }
    $window.t delete 1.0 end
    if [catch {open $file} in] {
	$window.t insert end "Nothing to display\n" all
	#$window.t insert end $in all
    } else {
	$window.t insert end [read $in] all
	close $in
    }
    if {$font != ""} {
	$window.t tag configure all -font $font
    }
}


proc clear-text-file-window {self} {
    if {![winfo exists $self]} {return}
    $self.t delete 1.0 end
}


####################################################################
# 

proc silent_tk {} {
    global no_Tk
    global previous_no_Tk

    set previous_no_Tk $no_Tk
    set no_Tk 1
}

proc restore_tk {} {    
    global no_Tk
    global previous_no_Tk

    set no_Tk $previous_no_Tk
}




proc create-ts-menu {} {
    global fsCommands fsChartCommands 

    add-item-to-xle-menu \
	{command -label "Add to Testsuite" \
	     -command "add-to-ts 0 $self" \
	     -doc "Add current analysis to current test suite"} \
	fsCommands

    add-item-to-xle-menu \
	{command -label "Add to Testsuite" \
	     -command "add-to-ts 1 $self" \
	     -doc "Add current analysis to current test suite"} \
	fsChartCommands
}





proc set-testsuite {dir} {
    global testsuite
    set testsuite $dir
}




# Setting to determine whether or not run compares only the most
# probable f-structure against the benchmark
proc set-testsuite-most-probable {value} {
    if {$value == 1} {
	prolog "setp(ts_most_probable(1))."
    } elseif {$value == 0} {
	prolog "setp(ts_most_probable(0))."
    } else {
	puts "Value must be 0 or 1 \n"
    }
}




# Setting to detemine whether benchmarks are partial or complete
# specifications of desired structures
proc set-testsuite-partial-benchmark {value} {
    if {$value == 1} {
	prolog "setp(maximize_recall(1))."
    } elseif {$value == 0} {
	prolog "setp(maximize_recall(0))."
    } else {
	puts "Value must be 0 or 1 \n"
    }
}



# Default value for testsuite dir: ts subdir in current directory

set testsuite ./ts


proc create-testsuite-dir {{path "."}} {
    set dirs { fs kr reports sem tmp/fs tmp/kr tmp/sem tmp/xfr tmp/xfs xfr xfs }       
    foreach {dir} $dirs { 
      file mkdir $path/ts/$dir 
    }
    return
}


proc run-syn-testsuite {{from 1} {to inf}} {
    global testsuite
    set grammar [default_grammar]
    prolog "run_syn_testsuite('$testsuite', $from, $to, '$grammar')."
    return
}


proc run-sem-testsuite {{from 1} {to inf}} {
    global testsuite
    set grammar [default_grammar]
    prolog "run_sem_testsuite('$testsuite', $from, $to, '$grammar')."
    return
}


proc run-kr-testsuite {{from 1} {to inf}} {
    global testsuite
    set grammar [default_grammar]
    prolog "run_kr_testsuite('$testsuite', $from, $to, '$grammar')."
    return
}


proc run-synsem-testsuite {{from 1} {to inf}} {
    global testsuite
    prolog "run_synsem_testsuite('$testsuite', $from, $to)."
    return
}


proc run-semkr-testsuite {{from 1} {to inf}} {
    global testsuite
    prolog "run_semkr_testsuite('$testsuite', $from, $to)."
    return
}

proc run-synkr-testsuite {{from 1} {to inf}} {
     global testsuite
     prolog "run_synkr_testsuite('$testsuite', $from, $to)."
     return
}

proc run-xfr-testsuite {{from 1} {to inf}} {
    global testsuite
    set grammar [default_grammar]
    prolog "run_xfr_testsuite('$testsuite', $from, $to, '$grammar')."
    return
}

proc run-multi-xfr-testsuite {{from 1} {to inf}} {
    global testsuite
    set grammar [default_grammar]
    prolog "run_multixfr_testsuite('$testsuite', $from, $to, '$grammar')."
    return
}

proc run-xfs-testsuite {{from 1} {to inf}} {
    global testsuite
    set grammar [default_grammar]
    prolog "run_xfs_testsuite('$testsuite', $from, $to, '$grammar')."
    return
}

proc run-synxfr-testsuite {{from 1} {to inf}} {
    global testsuite
    prolog "run_synxfr_testsuite('$testsuite', $from, $to)."
    return
}

proc run-syn-multi-xfr-testsuite {{from 1} {to inf}} {
    global testsuite
    prolog "run_synmulti_xfr_testsuite('$testsuite', $from, $to)."
    return
}

proc run-syn-xfs-testsuite {{from 1} {to inf}} {
    global testsuite
    prolog "run_synxfs_testsuite('$testsuite', $from, $to)."
    return
}

proc robust-semantics {{value 1}} {
    prolog "setp(robust_semantics($value))."
}





proc default_grammar {} {
    global grammarProps
    global defaultparser

    if {[info exists grammarProps($defaultparser,filename)] == 0} {
	return 1}
    return $grammarProps($defaultparser,filename)
}






# From menu

proc add-to-ts {packed window} {
    global sempipe0

    if {$packed} {
	print-prolog-chart-graph $sempipe0 $window
    } else {
	print-fs-as-prolog $sempipe0 $window
    }

    add_to_ts
}


# From command line

proc add-to-testsuite {} {
    global sempipe0

    print-fs-as-prolog $sempipe0 
    add_to_ts
}



proc add_to_ts {} {
    global glueListener
    global sempipe0
    global sempipe1
    global sempingpipe
    global sempinged
    global testsuite    

    if {$sempinged == 0} {
	set semping 1
    } else {
	set semping 0
    }

    set fileId [open $sempipe1 w]
    puts $fileId "add_to_ts('$testsuite')."
    close $fileId

    if {$glueListener == 0} {
	set plgcmd "run_semantics('$sempipe0', '$sempipe1')."
	set ack [prolog $plgcmd]
	if {$ack == 1} {
	    return 1
	} else {
	    puts stderr "Semantics failed."
	    return 0
	}
    } else {
	if {$sempinged == 0} {
	    check-semping-result $sempingpipe
	}
    }
}


#####################################################################
# Interface to matching (for Bob Cheslow)
#####################################################################



proc match-files {Source Target {file 0}} {
    if {$file == 0} {
	prolog "match_files('$Source', '$Target')."
    } else {
	prolog "match_files('$Source', '$Target', '$file')."
    }
}

proc match-dep-files {Source Target {file 0}} {
    if {$file == 0} {
	prolog "match_dep_files('$Source', '$Target')."
    } else {
	prolog "match_dep_files('$Source', '$Target', '$file')."
    }
}

proc match-sem-files {Source Target {file 0}} {
    if {$file == 0} {
	prolog "match_sem_files('$Source', '$Target')."
    } else {
	prolog "match_sem_files('$Source', '$Target', '$file')."
    }
}

proc match-kr-files {Source Target {file 0}} {
    if {$file == 0} {
	prolog "match_kr_files('$Source', '$Target')."
    } else {
	prolog "match_kr_files('$Source', '$Target', '$file')."
    }
}

proc match-fs-files {Source Target {file 0}} {
    if {$file == 0} {
	prolog "match_fs_files('$Source', '$Target')."
    } else {
	prolog "match_fs_files('$Source', '$Target', '$file')."
    }
}


proc match-file-time-limit {value} {
    prolog "setp(match_file_time_limit($value))."
}

#-----------------------------------------------------------------

#  Processing sentences direct to fs, sem, kr, cycl

# This variable controls whether the commands below write intermediate
# representations out to file

set viaFile 0


proc xfr {sentence {file "xfr_out"}} {
    global selectMostProbable no_Tk viaFile
    set lastMostProbable $selectMostProbable
    set lastTk $no_Tk
    set no_Tk 1
    set selectMostProbable 1
    if {$viaFile == 1} {
	silent_parse $sentence "fs_out"
	set fs_data "fs_out"
	set xfrMode "fs_file"
    } else {
	set fs_data [silent_parse_ptr $sentence]
	set xfrMode "xle_graph"
    }
    set selectMostProbable $lastMostProbable
    set no_Tk $lastTk
    prolog "transfer('$fs_data', '$file', '$xfrMode', xfr_file)."
}


proc triples {sentence {file "triples_out"}} {
    global selectMostProbable no_Tk viaFile
    set lastMostProbable $selectMostProbable
    set lastTk $no_Tk
    set no_Tk 1
    set selectMostProbable 1
    if {$viaFile == 1} {
	silent_parse $sentence "fs_out"
	set fs_data "fs_out"
	set xfrMode "fs_file"
    } else {
	set fs_data [silent_parse_ptr $sentence]
	set xfrMode "xle_graph"
    }
    set selectMostProbable $lastMostProbable
    set no_Tk $lastTk
    prolog "set_active_transfer_grammar(fs_triples),\
            transfer('$fs_data', '$file', '$xfrMode', xfr_file),\
            restore_previous_transfer_grammar."
}

proc cycl {sentence {file "cycl_out"}} {
    global selectMostProbable no_Tk viaFile
    set lastMostProbable $selectMostProbable
    set lastTk $no_Tk
    set no_Tk 1
    set selectMostProbable 1
    if {$viaFile == 1} {
	silent_parse $sentence "fs_out"
	set fs_data "fs_out"
    } else {
	set fs_data [silent_parse_ptr $sentence]
    }
    set selectMostProbable $lastMostProbable
    set no_Tk $lastTk
    prolog "fs_cycl('$fs_data', '$file')."
}

proc kr {sentence {file "kr_out"}} {
    global selectMostProbable no_Tk viaFile
    set lastMostProbable $selectMostProbable
    set lastTk $no_Tk
    set selectMostProbable 1
    set no_Tk 1
    if {$viaFile == 1} {
	silent_parse $sentence "fs_out"
	set fs_data "fs_out"
    } else {
	set fs_data [silent_parse_ptr $sentence]
    }
    set selectMostProbable $lastMostProbable
    set no_Tk $lastTk
    prolog "fs_kr('$fs_data', '$file')."
}

proc sem {sentence {file "sem_out"}} {
    global selectMostProbable no_Tk viaFile
    set lastMostProbable $selectMostProbable
    set lastTk $no_Tk
    set selectMostProbable 1
    set no_Tk 1
    if {$viaFile == 1} {
	silent_parse $sentence "fs_out"
	set fs_data "fs_out"
    } else {
	set fs_data [silent_parse_ptr $sentence]
    }
    set selectMostProbable $lastMostProbable
    set no_Tk $lastTk
    prolog "fs_sem('$fs_data', '$file')."
}

proc fs {sentence {file "fs_out"}} {
    global selectMostProbable no_Tk
    set lastMostProbable $selectMostProbable
    set lastTk $no_Tk
    set selectMostProbable 1
    set no_Tk 1
    silent_parse $sentence $file
    set selectMostProbable $lastMostProbable
    set no_Tk $lastTk
}


proc packed-xfr {sentence {file "xfr_out"}} {
    global selectMostProbable no_Tk viaFile
    set lastMostProbable $selectMostProbable
    set lastTk $no_Tk
    set selectMostProbable 0
    set no_Tk 1
    if {$viaFile == 1} {
	silent_parse $sentence "fs_out"
	set fs_data "fs_out"
	set xfrMode "fs_file"
    } else {
	set fs_data [silent_parse_ptr $sentence]
	set xfrMode "xle_graph_no_select"
    }
    set selectMostProbable $lastMostProbable
    set no_Tk $lastTk
    prolog "transfer('$fs_data', '$file', '$xfrMode', xfr_file)."
}


proc packed-triples {sentence {file "triples_out"}} {
    global selectMostProbable no_Tk viaFile
    set lastMostProbable $selectMostProbable
    set lastTk $no_Tk
    set no_Tk 1
    set selectMostProbable 0
    if {$viaFile == 1} {
	silent_parse $sentence "fs_out"
	set fs_data "fs_out"
	set xfrMode "fs_file"
    } else {
	set fs_data [silent_parse_ptr $sentence]
	set xfrMode "xle_graph_no_select"
    }
    set selectMostProbable $lastMostProbable
    set no_Tk $lastTk
    prolog "set_active_transfer_grammar(fs_triples),\
            transfer('$fs_data', '$file', '$xfrMode', xfr_file),\
            restore_previous_transfer_grammar."
}


proc packed-cycl {sentence {file "cycl_out"}} {
    global selectMostProbable no_Tk viaFile
    set lastMostProbable $selectMostProbable
    set lastTk $no_Tk
    set selectMostProbable 0
    set no_Tk 1
    if {$viaFile == 1} {
	silent_parse $sentence "fs_out"
	set fs_data "fs_out"
    } else {
	set fs_data [silent_parse_ptr $sentence]
    }
    set selectMostProbable $lastMostProbable
    set no_Tk $lastTk
    prolog "packed_fs_cycl('$fs_data', '$file')."
}

proc packed-kr {sentence {file "kr_out"}} {
    global selectMostProbable no_Tk viaFile
    set lastMostProbable $selectMostProbable
    set lastTk $no_Tk
    set selectMostProbable 0
    set no_Tk 1
    if {$viaFile == 1} {
	silent_parse $sentence "fs_out"
	set fs_data "fs_out"
    } else {
	set fs_data [silent_parse_ptr $sentence]
    }
    set selectMostProbable $lastMostProbable
    set no_Tk $lastTk
    prolog "packed_fs_kr('$fs_data', '$file')."
}

proc packed-sem {sentence {file "sem_out"}} {
    global selectMostProbable no_Tk viaFile
    set lastMostProbable $selectMostProbable
    set lastTk $no_Tk
    set selectMostProbable 0
    set no_Tk 1
    if {$viaFile == 1} {
	silent_parse $sentence "fs_out"
	set fs_data "fs_out"
    } else {
	set fs_data [silent_parse_ptr $sentence]
    }
    set selectMostProbable $lastMostProbable
    set no_Tk $lastTk
    prolog "packed_fs_sem('$fs_data', '$file')."
}

proc packed-fs {sentence {file "fs_out"}} {
    global selectMostProbable no_Tk
    set lastMostProbable $selectMostProbable
    set lastTk $no_Tk
    set selectMostProbable 0
    set no_Tk 1
    silent_parse $sentence $file
    set selectMostProbable $lastMostProbable
    set no_Tk $lastTk
}



proc xml-fs {sentence {file "fs_out.xml"}} {
    fs $sentence "fs_out.pl"
    to_xml "" "fs_out.pl" "" $file "FS"
}

proc xml-triples {sentence {file "triples_out.xml"}} {
    triples $sentence "triples_out.fdsc"
    to_xml "" "triples_out.fdsc" "" $file "TRIPLES"
}

proc xml-xfr {sentence {file "xfr_out.xml"}} {
    xfr $sentence "xfr_out.pl"
    to_xml "" "xfr_out.pl" "" $file "XFR"
}

proc xml-sem {sentence {file "sem_out.xml"}} {
    sem $sentence "sem_out.pl"
    to_xml "" "sem_out.pl" "" $file "SEM"
}

proc xml-kr {sentence {file "kr_out.xml"}} {
    kr $sentence "kr_out.pl"
    to_xml "" "kr_out.pl" "" $file "KR"
}




proc packed-xml-fs {sentence {file "fs_out.xml"}} {
    packed-fs $sentence "fs_out.pl"
    to_xml "" "fs_out.pl" "" $file "FS"
}

proc packed-xml-triples {sentence {file "triples_out.xml"}} {
    packed-triples $sentence "triples_out.fdsc"
    to_xml "" "triples_out.fdsc" "" $file "TRIPLES"
}

proc packed-xml-xfr {sentence {file "xfr_out.xml"}} {
    packed-xfr $sentence "xfr_out.pl"
    to_xml "" "xfr_out.pl" "" $file "XFR"
}

proc packed-xml-sem {sentence {file "sem_out.xml"}} {
    packed-sem $sentence "sem_out.pl"
    to_xml "" "sem_out.pl" "" $file "SEM"
}

proc packed-xml-kr {sentence {file "kr_out.xml"}} {
    packed-kr $sentence "kr_out.pl"
    to_xml "" "kr_out.pl" "" $file "KR"
}



#-----------------------------------------------------





proc match {sentence1 sentence2} {
    global selectMostProbable no_Tk
    set lastMostProbable $selectMostProbable
    set lastTk $no_Tk
    set selectMostProbable 1
    set no_Tk 1
    silent_parse $sentence1 "fs_out1"
    prolog "fs_sem_kr(fs_out1, sem_out1, kr_out1)."
    silent_parse $sentence2 "fs_out2"
    prolog "fs_sem_kr(fs_out2, sem_out2, kr_out2)."
    set selectMostProbable $lastMostProbable
    set no_Tk $lastTk
    prolog "set_active_transfer_grammar(fs_triples)."
    match-fs-files "fs_out1" "fs_out2"
    prolog "restore_previous_transfer_grammar."
    match-sem-files "sem_out1" "sem_out2"
    match-kr-files "kr_out1" "kr_out2"
}

proc match-fs {sentence1 sentence2} {
    global selectMostProbable no_Tk
    set lastMostProbable $selectMostProbable
    set lastTk $no_Tk
    set no_Tk 1
    set selectMostProbable 1
    silent_parse $sentence1 "fs_out1"
    silent_parse $sentence2 "fs_out2"
    set selectMostProbable $lastMostProbable
    set no_Tk $lastTk
    prolog "set_active_transfer_grammar(fs_triples)."
    prolog "match_fs_files(fs_out1, fs_out2)."
    prolog "restore_previous_transfer_grammar."
}


proc match-sem {sentence1 sentence2} {
    global selectMostProbable no_Tk
    set lastMostProbable $selectMostProbable
    set lastTk $no_Tk
    set selectMostProbable 1
    set no_Tk 1
    silent_parse $sentence1 "fs_out1"
    silent_parse $sentence2 "fs_out2"
    set selectMostProbable $lastMostProbable
    set no_Tk $lastTk
    prolog "fs_sem(fs_out1, sem_out1)."
    prolog "fs_sem(fs_out2, sem_out2)."
    prolog "match_sem_files(sem_out1, sem_out2)."
}





proc match-kr {sentence1 sentence2} {
    global selectMostProbable no_Tk
    set lastMostProbable $selectMostProbable
    set lastTk $no_Tk
    set selectMostProbable 1
    set no_Tk 1
    silent_parse $sentence1 "fs_out1"
    silent_parse $sentence2 "fs_out2"
    set no_Tk $lastTk
    set selectMostProbable $lastMostProbable
    prolog "fs_kr(fs_out1, kr_out1)."
    prolog "fs_kr(fs_out2, kr_out2)."
    prolog "match_kr_files(kr_out1, kr_out2)."
}





##############################################################

add-help-doc {poll  \
"poll (<StatusFile> <CommandFile>)" \
"Get XLE to poll commands from files.
When StatusFile contains 1, execute the command in CommandFile
and then write 0 to StatusFile to indicate it is done.
To quit polling loop, place the command done in CommandFile"}

proc poll {readyfile1 inputfile1} {
    global env
    regsub ^~/ $readyfile1 $env(HOME)/ readyfile
    regsub ^~/ $inputfile1 $env(HOME)/ inputfile

    set_poll_status $readyfile 0
    puts "XLE is polling on $readyfile and $inputfile"

    set done 0
    while {$done == 0} {
	while {[poll_status $readyfile] == 0} {
	    exec sh -c "sleep 0.25"
	}
	
	if [catch {open $inputfile r} fileId] {
	    puts stderr "Cannot open $inputfile: $fileId"
	} else {
	    set input [read $fileId]
	    close $fileId
	}

	if {$input == "done\n"} {
	    set_poll_status $readyfile 0
	    set done 1
	} else {
	    if [catch {eval $input} result] {puts stderr $result}
	    set_poll_status $readyfile 0
	}
    }
}


proc poll_status {readyfile} {
    if [catch {open $readyfile r} fileId] {
	return 0
    } else {
	set input [read $fileId]
	close $fileId
	return $input
    }
}


proc set_poll_status {readyfile status} {
    if [catch {open $readyfile w} fileId] {
	puts stderr "Cannot open $readyfile: $fileId"
    } else {
	puts $fileId $status
	close $fileId
    }
}


##############################################################################

add-help-doc {merge-prolog-charts  \
"merge-prolog-charts <MergedFile> <InputFiles>" \
"Link f-structures in InputFiles together to form a multi-sentence
f-structure, and write result to MergedFile. 
List of InputFiles should be enclosed between braces"}

add-help-doc {merge-charts-subroot  \
"merge-charts-subroot <SubRootCat>" \
"Sets the c-structure category dominating singleton sentences 
in merged f-structure charts (default: ROOTpunct)"}

add-help-doc {merge-charts-multiroot  \
"merge-charts-multiroot <MultiRootCat>" \
"Sets the c-structure category dominating multiple sentences 
in merged f-structure charts (default: ROOTmulti)"}

proc merge-prolog-charts {outFile inFiles} {
    set fileListString {[}
    foreach x [split $inFiles] {
	if {$x != {}} {
	    append fileListString "'" $x "',"
	}
    }
	set fileListString [string trimright $fileListString ","]
    append fileListString "]"
    prolog "merge_prolog_charts($fileListString, '$outFile')."
}


proc merge-charts-subroot {cat} {
    prolog "setp(subroot_category('$cat'))."
}

proc merge-charts-multiroot {cat} {
    prolog "setp(multiroot_category('$cat'))."
}



#######################################################
# To make aquaint evaluation runs a bit easier:
# Use transfer rules to convert between kr files, e.g. rules to strip
#out concept expansions.  $ruleset is the identifier associated with
#the conversion rules

proc kr-transfer {file1 file2 ruleset} {
    prolog "multiple_transfer('$file1', kr_file, \
                              \[phase($ruleset, '$file2', kr)\],_)."
}



#######################################################
# Run a command, redirecting all stdout to specified file.
# Note that we have to collect c and prolog stdout separately and then
# combine. This will miss the prolog output if the command is the
# first one to require loading the prolog image, but this is unlikely
# to occur in practice -- normally one would want to load transfer
# rules or something before calling on a prolog defined command

proc stdout-to-file {command file} {
    global prolog_image_loaded

    if {$prolog_image_loaded} {
	set_stdout $file.c_out w
	prolog "tell('$file.pl_out')."
	set return [eval $command]
	prolog "told."
	set_stdout
	set cmd "cat $file.c_out $file.pl_out > $file"
	exec sh -c $cmd
	file delete $file.c_out
	file delete $file.pl_out
    } else {
	set_stdout $file w
	set return [eval $command]
	set_stdout
    }

    return $return
}



##############################################################

add-sem-help-doc {convert-xfr-file  \
"convert-xfr-file File (Mode)" \
"Convert the format of the file from/to xfr format.
If the input format is sem or kr, will convert to xfr.
If the input format is xfr, then Mode nust be either sem or kr.
Overwrites the contents of the file."}

proc convert-xfr-file {file {mode "xfr"}} {
    prolog "convert_xfr_file('$file', '$mode')."
}

add-sem-help-doc {convert-dep-file  \
"convert-xfr-file DepFile XfrFile" \
"Convert dep file to xfr format."}

proc convert-dep-file {depfile xfrfile} {
    prolog "convert_dep_file('$depfile', '$xfrfile')."
}


##############################################################

add-xfr-help-doc {add-to-xfr-history  \
" add-to-xfr-history <StructureType> <Structure>" \
"Converts the structure to xfr format, and adds its facts to the
global xfr history.
StructureType should be one of: xle_graph, fs_file, xfr_file.
Structure should be a pointer to an f-structure, an f-structure file
or an xfr file, respectively."}

add-xfr-help-doc {delete-xfr-history  \
" delete-xfr-history " \
"Clears the global transfer history"}

proc add-to-xfr-history {structuretype structure} {
    prolog "xfr_history:add_to_xfr_history('$structuretype', '$structure')."
}

proc delete-xfr-history {} {
    prolog "xfr_history:delete_xfr_history."
}
