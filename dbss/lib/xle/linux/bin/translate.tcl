# (c) 2002-2006 by the Palo Alto Research Center.  All rights reserved.
# (c) 1997-2001 by the Xerox Corporation.  All rights reserved.
# This is the Tcl/Tk interface to creating and using generation input.

#####################################################################
# Command line transfer procedures
#####################################################################

proc add-xfr-help-doc {doc} {
    global xfr_help_doc
    lappend xfr_help_doc $doc
}

proc transfer-help {{command "help"}} {
    global xfr_help_doc
    if {$command == "help"} {
	puts "transfer-help <command>"
	puts "Displays help for a particular transfer command."
	puts -nonewline "Commands known: "
	set first 1
	set length 16
	foreach doc $xfr_help_doc {
	    if {!$first} {
		puts -nonewline ", "
	    }
	    set first 0
	    set name [lindex $doc 0]
	    set delta [expr {[string length $name]+2}]
	    incr length $delta
	    if {$length > 75} {
		puts ""
		set length $delta
	    }
	    puts -nonewline "$name"
	}
	puts ""
	return
    }
    foreach doc $xfr_help_doc {
	set name [lindex $doc 0]
	if {![string match $command $name]} {continue}
	foreach line [lrange $doc 1 end] {
	    puts $line
	}
	puts ""
    }
}


##############################################################

proc transfer {file1 file2 {mode "fs"}} {
    if {$mode == "fs"} {
	set inmode fs_file
	set outmode fs_file
    } elseif {$mode == "dep"} {
	set inmode  dep_file
	set outmode  dep_file
    } elseif {$mode == "xfr"} {
	set inmode  xfr_file
	set outmode  xfr_file
    } elseif {$mode == "fs-dep"} {
	set inmode  fs_file
	set outmode  dep_file
    } elseif {$mode == "fs-xfr"} {
	set inmode  fs_file
	set outmode  xfr_file
    } elseif {$mode == "dep-xfr"} {
	set inmode  dep_file
	set outmode  xfr_file
    }
    prolog "transfer('$file1', '$file2', $inmode, $outmode)."
}

proc timed-transfer {file1 file2 {mode "fs"}} {
    if {$mode == "fs"} {
	set inmode fs_file
	set outmode fs_file
    } elseif {$mode == "dep"} {
	set inmode  dep_file
	set outmode  dep_file
    } elseif {$mode == "xfr"} {
	set inmode  xfr_file
	set outmode  xfr_file
    } elseif {$mode == "fs-dep"} {
	set inmode  fs_file
	set outmode  dep_file
    } elseif {$mode == "fs-xfr"} {
	set inmode  fs_file
	set outmode  xfr_file
    } elseif {$mode == "dep-xfr"} {
	set inmode  dep_file
	set outmode  xfr_file
    }
    prolog "timed_transfer('$file1', '$file2', $inmode, $outmode)."
}

proc transfer-seq {file1 file2 rules {mode "fs"}} {
    if {$mode == "fs"} {
	set inmode fs_file
	set outmode fs_file
    } elseif {$mode == "dep"} {
	set inmode  dep_file
	set outmode  dep_file
    } elseif {$mode == "xfr"} {
	set inmode  xfr_file
	set outmode  xfr_file
    } elseif {$mode == "fs-dep"} {
	set inmode  fs_file
	set outmode  dep_file
    } elseif {$mode == "fs-xfr"} {
	set inmode  fs_file
	set outmode  xfr_file
    } elseif {$mode == "dep-xfr"} {
	set inmode  dep_file
	set outmode  xfr_file
    }
    prolog "transfer_seq('$file1', '$file2', $inmode, $outmode, '$rules')."
}

proc set-transfer-timeout {time} {
    prolog "set_transfer_timeout_limit($time)."
}


#proc transfer-graph-to-file {graph file} {
#    set pid [pid]
#    set tmp_out /tmp/tgf_${pid}
#    print_graph_as_prolog $graph $tmp_out
#    prolog "transfer('$tmp_out', '$file', fs_file, xfr_file)."
#    file delete $tmp_out
#}

proc transfer-graph-to-file {graph file} {
    prolog "transfer('$graph', '$file', xle_graph, xfr_file)."
}


proc load-transfer-rules {{rules ""}} {
    global transferRulesLoaded
    if {$rules == ""} {
	set command "reload_rules."
    } else {
	set command "load_rules('$rules')."
    }
    set ack [prolog $command]
    if {$ack != 1} {
	puts "Transfer system couldn't reload rules."
    } else {
	set transferRulesLoaded 1
    }
}


proc active-transfer-grammar {} {
    prolog "transfer:current_grammar(G), write(G), nl."
}


proc activate-transfer-grammar {ruleId} {
    prolog "set_active_transfer_grammar('$ruleId')."
}

proc deactivate-transfer-grammar {} {
    prolog "restore_previous_transfer_grammar."
}

proc list-transfer-grammars {} {
    prolog "print_loaded_transfer_grammars."
}

proc print-transfer-rules {{grammar 0}} {
    if {$grammar == 0} {
	prolog "print_compiled_transfer_rules."
    } else {
	prolog "print_compiled_transfer_rules('$grammar')."	
    }
}

proc print-transfer-rule {rule {grammar 0}} {
    if {$grammar == 0} {
	prolog "print_transfer_rule($rule)."
    } else {
	prolog "print_transfer_rule($rule, '$grammar')."	
    }

}

proc print-matching-transfer-rules {pattern {grammar 0}} {
    if {$grammar == 0} {
	prolog "print_matching_transfer_rules($pattern)."
    } else {
	prolog "print_matching_transfer_rules($pattern, '$grammar')."
    }
}

proc trace-rules {rules} {
    prolog "trace_rules($rules)."
}

proc tdbg-grammar {ruleset} {
    prolog "monitor_rules('$ruleset'), tdbg."
}

proc no-tdbg-grammar {{ruleset 0}} {
    if {$ruleset == 0} {
	prolog "unmonitor_rules."
    } else {  
        prolog "unmonitor_rules('$ruleset')."
    }
}

proc tdbg-levels {} {
    prolog "monitors."
}

proc tdbg {{level 1}} {
    if {$level == 1} {
	prolog "tdbg."
    } elseif {$level == 2} {
	prolog "full_tdbg."
    } else {
        prolog "tdbg($level)."
    }
}

proc no-tdbg {} {
    prolog "no_tdbg."
}


proc print-blocking-transfer-rules {rulenum {grammar 0}} {
    if {$grammar == 0} {
	prolog "print_blocking_transfer_rules($rulenum)."
    } else {
	prolog "print_blocking_transfer_rules($rulenum, '$grammar')."
    }
}




proc set-xfr-sequence {rules} {
    global xfrSequence

    set xfrSequence $rules
    prolog "set_xfr_sequence('$rules')."
}


proc display-xfr-procedure {ruleset procedure} {
    global xfrDisplayProc
    set xfrDisplayProc($ruleset) $procedure
}

proc set-xfr-results-display {ruleset {onoff 1}} {
    global xfrDisplayProc xfrDisplayFile

    if {$onoff == 0} {
	prolog "set_xfr_seq_ruleset_mode('$ruleset', null)."
    } elseif ![info exists xfrDisplayProc($ruleset)] {
	prolog "set_xfr_seq_ruleset_mode('$ruleset', stdout)."
    } elseif {$xfrDisplayProc($ruleset) == "stdout"} {
	prolog "set_xfr_seq_ruleset_mode('$ruleset', stdout)."
    } elseif ![info exists xfrDisplayFile($ruleset)] {
	set pid [pid]
	set xfrDisplayFile($ruleset) "/tmp/xfr_$ruleset_$pid"
	prolog "set_xfr_seq_ruleset_mode('$ruleset','$xfrDisplayFile($ruleset)')."
    } else {
	prolog "set_xfr_seq_ruleset_mode('$ruleset','$xfrDisplayFile($ruleset)')."
    }
}
 

proc sentence-to-fsdata {sentence parser {mostProbable 0}} {
    global selectMostProbable no_Tk 
    set lastMostProbable $selectMostProbable
    set lastTk $no_Tk
    set selectMostProbable $mostProbable
    set no_Tk 1
    set fs_data [silent_parse_ptr $sentence $parser]
    set no_Tk $lastTk
    set selectMostProbable $lastMostProbable
    return $fs_data
}

proc sentence-to-xfr {sentence parser ruleset {mostProbable 0}} {
    set fs_data [sentence-to-fsdata $sentence $parser $mostProbable]
    if {$fs_data == -1} {
	puts "Parse timed out"
	return $fs_data
    } else {
	fs-to-xfr $fs_data $ruleset $mostProbable
    }
}

#proc sentence-to-xfr {sentence parser ruleset {mostProbable 0}} {
#    global selectMostProbable no_Tk 
#    set lastMostProbable $selectMostProbable
#    set lastTk $no_Tk
#    set selectMostProbable $mostProbable
#    set no_Tk 1
#    set fs_data [silent_parse_ptr $sentence $parser]
#    set no_Tk $lastTk
#    set selectMostProbable $lastMostProbable
#    fs-to-xfr $fs_data $ruleset $mostProbable
#}




proc fs-to-xfr {fsData ruleset {mostProbable 0}} {
    if {$mostProbable == 0} {
	set inMode xle_graph_no_select
    } else {
	set inMode xle_graph
    }
    prolog "transfer_seq('$fsData', $inMode, '$ruleset')."
    # return Id of last rule in sequence:
    lindex $ruleset [expr [llength $ruleset] - 1]
}

proc fsfile-to-xfr {fsData ruleset {mostProbable 0}} {
    if {$mostProbable == 0} {
	set inMode fs_file_no_select
    } else {
	set inMode fs_file
    }
    prolog "transfer_seq('$fsData', $inMode, '$ruleset')."
    # return Id of last rule in sequence:
    lindex $ruleset [expr [llength $ruleset] - 1]
}


proc print-xfr-output {xfrId printMode file} {
    prolog "print_to_xfr_file('$xfrId', '$printMode', '$file')."
}


# Sets the value of a global variable only if it is currently undefined
proc set-if-undefined {var value} {
     global $var
     if {![info exists $var]} { set $var $value}
}




#############################################################
# Starting up transfer trace browswer
#############################################################

add-xfr-help-doc \
    {gui-trace \
	 "gui-trace " \
	 "Opens browser for transfer trace, if not already open, "\
         "  and turns on printing out of trace info when running transfer."\
	 "If argument 0 is provided, will stop printing out trace info,"\
	 "  but does not close down browser." \
	 "Use kill-trace-browser to close down browser."\
	 "Note: The correct version of Java must be installed."\
	 "      Log files are written to /tmp/gui_{server|java}_log_[pid]"
    }

   
set guiTracePort 8066
set guiTraceFile none
set guiServerPid none
set guiJavaPid none

proc get_trace_browser_path {} {
    global env
    if {[info exists env(TRACE_BROWSER_PATH)]} {
        return $env(TRACE_BROWSER_PATH)
    } elseif {[file exists /p/libexec/trace_browser]} {
        return  "/p/libexec/trace_browser"
    # This is where you would add a non-powerset default path like in XLEPATH or something
    } else {
        puts "ERROR: can't find the trace browser.  Try setting the"
        puts "TRACE_BROWSER_PATH environment variable, e.g."
        puts "    set env(TRACE_BROWSER_PATH) ~/trace_browser"
        puts "or set it before XLE startup."
    }
}

proc gui-trace {{onoff 1}} {
    global guiTraceFile guiTracePort guiServerPid guiJavaPid
    set trace_browser_path [get_trace_browser_path]
    puts "Using trace browser at \"$trace_browser_path\""
    puts "(if you don't like that, override via the env var TRACE_BROWSER_PATH)"
    if {$onoff == 1 
	 && $guiTraceFile == "none"
         && $guiServerPid == "none"
         && $guiJavaPid == "none"} {
	# Start up the browser server and gui
	set guiTraceFile "/tmp/xfr_gui_trace_[pid]"
	prolog "gui_trace($onoff, '$guiTraceFile')."
	prolog "gui_file_init."
	puts "Writing trace information to $guiTraceFile"
	set guiServerPid \
	    [exec $trace_browser_path/Prolog/trace_browser_server \
		 $guiTracePort $guiTraceFile \
		 >& /tmp/gui_server_log[pid] & ]
	exec sh -c "sleep 1"
	set guiJavaPid \
	    [exec java -classpath "$trace_browser_path/prologbeans.jar:$trace_browser_path" \
		 TraceBrowser1.Browser $guiTracePort \
		 >& /tmp/gui_java_log[pid] & ]
	puts "Processes $guiServerPid and $guiJavaPid communicating on port $guiTracePort\n"
    } else {
	prolog "gui_trace($onoff)."
    }
}

proc kill-gui-trace {} {
    global guiTraceFile guiServerPid guiJavaPid
    
    set guiTraceFile none
    if {$guiJavaPid != "none"} {
	# Is there a Tcl command to kill a process?
	catch {[exec kill -9 $guiJavaPid]} result
	set guiJavaPid none}
    if {$guiServerPid != "none"} {
	# Is there a Tcl command to kill a process?
	catch {[exec kill -9 $guiServerPid]} result
	set guiServerPid none}
    prolog "gui_trace(0)."
    
}






# These defaults are PARC specific. The transfer rules are for a
# trivial sanity check, transfering the name "Ed" to the name "John",
# and nothing more

set defaultTranslator(source) \
	/project/pargram/english/standard/english.lfg 
set defaultTranslator(target) \
	/project/pargram/english/standard/english.lfg
set defaultTranslator(underspec) ""
if {[info exists env(XLEPATH)]} {
    set defaultTranslator(transfer) "$env(XLEPATH)/bin/dummy_translate_rules.pl"} else {
    set defaultTranslator(transfer)  "./dummy_translate_rules.pl"
}

set selectMostProbable 1


###############################################################################

set xfer_serv_input  "$tmpdir-xferin.pl"
set xfer_serv_output "$tmpdir-xferout.pl"





###############################################################################

set enumerate_parses        "all"
set enumerate_transfers       "all"
set transfer_beam_size 1

###############################################################################



###############################################################################
###############################################################################
## 
##  0. Setting up transfer menu
## 
###############################################################################
###############################################################################

#set transpipe0 [glob ~/]/.transpipe0
#set transpipe1 [glob ~/]/.transpipe1
#set transpinged 0
#set transpingpipe [glob ~/]/.trans_ping
#set transferListener 0

set transpipe0 /tmp/transpipe0_[pid]
set transpipe1 /tmp/transpipe1_[pid]
set transpinged 0
set transpingpipe /tmp/trans_ping_[pid]
set transferListener 0


proc create-transfer {} {
    global transferListener

    set transferListener 0
    install-transfer-menu
}


proc create-transfer-seq {} {
    global fsCommands fsChartCommands 
    create-transfer

    add-item-to-xle-menu \
	{command -label "TransferSeq" \
	     -command "send-to-transfer-seq 0 $self" \
	     -doc "Send f-structure through transfer sequence."} \
	fsCommands

    add-item-to-xle-menu \
	{command -label "Transfer" \
	     -command "send-to-transfer-seq 1 $self" \
	     -doc "Send f-structure through transfer."} \
	fsChartCommands

    add-xfr-help-doc \
     {set-xfr-sequence \
	 "set-xfr-sequence {<RuleSetId> ... <RuleSetId>}" \
	 "Specify sequence of transfer rulesets to apply to input. "
    }

    add-xfr-help-doc \
    {set-xfr-display-procedure \
	 "set-xfr-display-procedure <RuleSetId>  <ProcedureName>" \
	 "Specify procedure for file-to-file pretty printing of transfer " \
         "results derived by applying RuleSetId." \
         "ProcedureName = stdout will display to stdout without pretty printing."
    }

    add-xfr-help-doc \
    {set-xfr-results-display \
	 "set-xfr-results-display <RuleSetId>  (0)" \
	 "Turn on display of transfer results derived by applying RuleSetId. " \ 
         "(or off if optional argument of 0 is given)."
    }
    return
}


proc create-transfer-listener {} {
    global transferListener

    set transferListener 1
    install-transfer-menu

}


# For backwards compatibility
proc create-listener {} { 
    create-transfer-listener
}



proc install-transfer-menu {} {
    global transferListener fsCommands fsChartCommands 

    add-item-to-xle-menu \
	{command -label "Transfer" \
	     -command "send-to-transfer 0 $self" \
	     -doc "Send f-structure through transfer."} \
	fsCommands

    if {$transferListener == 0} {
	add-item-to-xle-menu \
	    {command -label "Translate" \
		 -command "send-to-translate 0 $self" \
		 -doc "Translate this f-structure."} \
	    fsCommands
    }



    add-item-to-xle-menu \
	{command -label "Load Transfer Rules" \
		 -command "command-to-transfer reload." \
		 -doc "Loads transfer rules."} \
	fsCommands

    add-item-to-xle-menu \
	{command -label "Debug Transfer" \
		 -command "command-to-transfer debug." \
		 -doc "Debugs transfer rules."} \
	fsCommands

    add-item-to-xle-menu \
	{command -label "Debug Off" \
		 -command "command-to-transfer no_debug." \
		 -doc "Turns off debugging of transfer rules."} \
	fsCommands

    if {$transferListener == 1} {
	add-item-to-xle-menu \
	    {command -label "Break Loop" \
		 -command "command-to-transfer break." \
		 -doc "Breaks out of transfer listener."} \
	    fsCommands
    }


    add-item-to-xle-menu \
	{command -label "Transfer" \
	     -command "send-to-transfer 1 $self" \
	     -doc "Send f-structure through transfer."} \
	fsChartCommands

    if {$transferListener == 0} {
	add-item-to-xle-menu \
	    {command -label "Translate" \
		 -command "send-to-translate 1 $self" \
		 -doc "Translate this f-structure."} \
	    fsChartCommands
    }
}


proc command-to-transfer {command} {
  global transferListener
  global transpipe0
  global transpipe1
  global transpinged
  global transpingpipe

    if {$transferListener == 0} {
	set plgcmd "run_transfer('$command', '$command')."
	set ack [prolog $plgcmd]
	if {$ack == 1} {
	    return 1
	} else {
	    puts stderr "Transfer failed."
	    return 0
	}
    } else {
	if {$transpinged == 0} {
	    set ping 1
	} else {
	    set ping 0
	}

	set commandString1 "echo '$command' > '$transpipe1'"
	set commandString2 "echo '$command' > '$transpipe0'"
	exec sh -c $commandString1
	exec sh -c $commandString2

	if {$transpinged == 0} {
	    check-transping-result $transpingpipe
	}
    }
}




proc send-to-transfer {packed window} {
    send-to-xfr $packed $window ready.
}

proc send-to-transfer-seq {packed window} {
    send-to-xfr $packed $window xfr_seq.
}



proc send-to-translate {packed window} {
    global xfer_serv_output transpipe0

    set cmd "translate(\\\'$xfer_serv_output\\\')."
    set ack [send-to-xfr $packed $window $cmd]

    if {$ack == 1} {
	generate-from-this-solution 1 all
	display-source-and-target $transpipe0 $xfer_serv_output}
}



proc send-to-xfr {packed window command} {

    global transferListener
    global transpipe0
    global transpipe1
    global transpinged
    global transpingpipe
    
    
    if {$transferListener == 0} {
	set data [get-window-prop $window data]
	set plgcmd "run_transfer('$data', '$command')."
	set ack [prolog $plgcmd]
	if {$ack == 1} {
	    return 1
	} else {
	    puts stderr "Transfer failed."
	    return 0
	}
    } else {
        # Use file based communication for listener loop
	if {$transpinged == 0} {
	    set ping 1
	} else {
	    set ping 0
	}
    
	if {$packed} {
	    print-prolog-chart-graph $transpipe0 $window
	} else {
	    print-fs-as-prolog $transpipe0 $window
	}
    
	set fileId [open $transpipe1 w]
	puts $fileId $command
	close $fileId

	if {$transpinged == 0} {
	    check-transping-result $transpingpipe
	}
    }
}



proc check-transping-result {transpingpipe} {
  global transpipe0
  global transpinged


  exec sh -c "sleep 1"

  if [file exists $transpingpipe] {
     exec sh -c "echo $transpingpipe | rm -f"
     set transpinged  1
  } else {
     puts stderr "Unable to contact listener via file $transpipe0"
     set transpinged  0
  }
}

###############################################################################
###############################################################################
## 
##  1. Setting up translation
##  This enables a translate(Sentence) command similar to parse(Sentence)
##  Much of this should be replaced by section 0
##
###############################################################################
###############################################################################




proc install-transfer-menu-items {} {
    global fsCommands fsChartCommands

    add-item-to-xle-menu \
	    {command -label "Transfer this FStructure" \
	    -command "transfer-this-solution 1 $self" \
	    -doc "Display the structures created by the transfer system."} \
	    fsCommands

    add-item-to-xle-menu \
	    {command -label "Translate this FStructure" \
	    -command "translate-this-solution $self" \
	    -doc "Translate (transfer, then generate) from this fstructure."} \
	    fsCommands

    add-item-to-xle-menu \
	    {command -label "Reload Transfer Rules" \
	    -command reload-transfer-rules \
	    -doc "Cause the transfer server to reload original rule file."} \
	    fsCommands

    add-item-to-xle-menu \
	    {command -label "Transfer this FSChart" \
	    -command "transfer-this-solution 1 $self" \
	    -doc "Display the structures created by the transfer system."} \
	    fsChartCommands

    add-item-to-xle-menu \
	    {command -label "Translate this FSChart" \
	    -command "translate-this-solution $self" \
	    -doc "Translate (transfer, then generate) from this fschart."} \
	    fsChartCommands

    add-item-to-xle-menu \
	    {command -label "Reload Transfer Rules" \
	    -command reload-transfer-rules \
	    -doc "Cause the transfer server to reload original rule file."} \
	    fsChartCommands

}


###############################################################################

set translator_started 0

proc create-translator-menu {{src_grammar ""} {trg_grammar ""} \
				{xfer_grammar ""} {gen_underspec ""}} {
    install-transfer-menu-items
    create-translator $src_grammar $trg_grammar \
	              $xfer_grammar $gen_underspec 
}


proc create-translator {{src_grammar ""} {trg_grammar ""} \
	                {xfer_grammar ""} {gen_underspec ""}} {
    global translator_started defaultTranslator
    global defaultparser gen_serv_started transferRulesLoaded

    if {$translator_started} {
	puts "Translator already started ..."
	return 1
    }
    set translator_started 1

    if {$src_grammar == ""} {
	set src_grammar   $defaultTranslator(source)
    }
    if {$trg_grammar == ""} {
	set trg_grammar   $defaultTranslator(target)
    }
    if {$xfer_grammar == ""} {
	set xfer_grammar  $defaultTranslator(transfer)
    }
    if {$gen_underspec == ""} {
	set gen_underspec $defaultTranslator(underspec)
    }

    if {![info exists defaultparser] || $defaultparser == ""} {
	create-parser $src_grammar
    } else {
	puts "Parser has already been created."
    }

    if {$gen_serv_started == 0} {
	if {[start-generation-server $trg_grammar] == 0} {
	    return 0
	}
	if {$gen_underspec != ""} {
	    set-gen-adds add $gen_underspec
	}
    } else {
	puts "Generation server has already been started."
    }

    if {$xfer_grammar == ""} {
	set xfer_grammar  $defaultTranslator(transfer)
    } else {
        set defaultTranslator(transfer) $xfer_grammar
    } 
    if {$transferRulesLoaded == 0} {
	reload-transfer-rules $xfer_grammar
    } else {
	puts "Transfer rules have already been loaded."
    }
    return 1
}


###############################################################################

set transferRulesLoaded 0

proc reload-transfer-rules {{rules ""}} {
    global transferRulesLoaded
    global defaulttranslator
    if {$defaulttranslator != ""} {return}
    if {$rules == ""} {
	set command "reload_rules."
    } else {
	set command "load_rules('$rules')."
    }
    set ack [prolog $command]
    if {$ack != 1} {
	puts "Transfer system couldn't reload rules."
    } else {
	set transferRulesLoaded 1
    }
}


###############################################################################

proc transfer-this-solution {display_result {solution ""} {beam_size ""}} {
    global xfer_client_socket xfer_serv_input xfer_serv_output
    global defaultTranslator transferRulesLoaded
    global defaulttranslator transfer_beam_size
    global no_Tk
    
    if {!$display_result && !$no_Tk && [winfo exists .transfer]} {
	clear-data-props .transfer
	destroy .transfer
    }

    if {!$transferRulesLoaded} {
	reload-transfer-rules $defaultTranslator(transfer)
    }

    if {[string index $solution 0] == "."} {
	set solution [get-window-prop $solution data]
    }

    if {$solution == ""} {
	print-fs-as-prolog $xfer_serv_input [default-window fstructure]
    } else {
	set chart [get_field $solution chart]
	set sentence [get-chart-prop $chart sentence]
	if {[get_field $solution disjunctions] != "" &&
	    [info procs choose-transfer-solution] != ""} {
	    set solution [choose-transfer-solution $sentence $solution]
	}   
	print_graph_as_prolog $solution $xfer_serv_input $sentence
    }
    file attributes $xfer_serv_input -permissions 0666

    if {$defaulttranslator != ""} {
	set translator $defaulttranslator
	if {[scan $beam_size "%d" dummy] != 1} {
	    set beam_size $transfer_beam_size
	}
	if {$transfer_beam_size > $beam_size} {
	    set beam_size $transfer_beam_size
	}
	set graph [transfer_fstructure $translator $xfer_serv_input $beam_size]
	if {$graph == "" || $graph == "(Graph)0"} {
	    set ack 0
	} else {
	    set ack 1
	    print_graph_as_prolog $graph $xfer_serv_output
	}
    } else {
	set cmd "transfer('$xfer_serv_input','$xfer_serv_output')."
	set ack [prolog $cmd]
    }

    if {$ack == 1} {
	file attributes $xfer_serv_output -permissions 0666
	if {$display_result} {
	    display-transferred-graph $xfer_serv_output
	}
	return 1
    } else {
	puts stderr "Transfer failed."
	return 0
    }
}



###############################################################################

proc display-transferred-graph {file} {
    global window_width window_height
    set graph [read_prolog_graph $file]
    display-fstructures $graph .transfer "" "Transfer Result" 0
    set xloc [expr {$window_width  + 20}]
    set yloc [expr {$window_height + 40}]
    wm geometry .transfer ${window_width}x${window_height}+${xloc}+${yloc}
}

proc display-ambiguous-transferred-graph {file} {
    set graph [read_prolog_graph $file]
    display-fschart $graph
    display-fschartchoices $graph
    set chart [get_field $graph chart]
    set fschart [chart-window-name $chart fschart]
    set choices [chart-window-name $chart choices]
    wm title $chart "transfer fschart"
    wm title $choices "transfer fschartchoices"

    raise $chart
    raise $choices
}

###############################################################################

proc translate-this-solution {data {enumerate ""} {action "print_all"} {outputFile ""} {id 1}} {
    global gen_serv_input xfer_serv_output xfer_serv_input
    global defaultTranslator enumerate_transfers
    global defaulttranslator
    global gen_out_strings gen_out_msgs

    if {$enumerate == ""} {
	set enumerate $enumerate_transfers
    }
    
    if {[string match "(Graph)*" $data]} {
	set parse $data
    } else {
	set parse [get-window-prop $window data]
    }
    
    if {[transfer-this-solution 0 $parse $enumerate] == 0} {
	return
    }

    if {[is-transferred-graph-ambiguous] && $enumerate == "choose"} {
	puts "Result of transfer is ambiguous ..."
	display-ambiguous-transferred-graph $xfer_serv_output
	return
    }
	
    set trg_grammar $defaultTranslator(target)
    if {[start-generation-server $trg_grammar] == 0} {
	puts stderr "Starting generation server failed.."
	return 0
    }

    file copy -force $xfer_serv_output $gen_serv_input
    file attributes $gen_serv_input -permissions 0666
    file attributes $xfer_serv_output -permissions 0666

    reset_prolog_storage
    set fschart_graph [read_prolog_graph $xfer_serv_output]

    if {$enumerate == "packed"} {
	set solution $fschart_graph
    } else {
	if {[scan $enumerate "%d" dummy] == 1} {
	    set fschart_graph [extract_n_best_graph $fschart_graph $enumerate]
	}
	# Initialize enumeration	
	set solution [get_next_fstructure $fschart_graph "(Graph)NULL"]
    }

    set generator [get-default-generator $fschart_graph]
    
    if {$action == "print_best"} {
	set old_gen_out_strings $gen_out_strings
	set gen_out_strings "/dev/null"
    }
    if {$action == "save_all"} {
	set-chart-prop $generator training 1
	set old_gen_out_strings $gen_out_strings
	set outputFile "${outputFile}/P$id"
	if {[catch {exec sh -c "mkdir -p $outputFile"} error]} {
	    puts $error
	}
	print-prolog-chart-graph ${outputFile}/parse.pl $parse
    }

    set solution_no 1
    set best ""
    set best_score -10000
    
    while {$solution != ""} {

	if {$action == "save_all"} {
	    set-gen-outputs $outputFile/gen$id.txt
	}
	set-chart-prop $generator "output" ""
	set count [generate-from-graph $solution $enumerate $generator]
	if {$count > 0} {
	    set stars [get_field $generator "ungrammatical"]
	    if {$stars != 0} {
		add_graph_feature $solution "GEN_STARRED" $stars VT_NUM
	    }
	}
	if {$action == "save_all"} {
	    close_gen_output $gen_out_strings
	    set gen_out_strings stdout
	    print-prolog-chart-graph $outputFile/fs$id.pl $solution
	}
	if {$count < 1} {
	    puts "skipping to next solution (id = $id)"
	    set solution [get_next_fstructure $fschart_graph $solution]
	    continue
	}
	if {$action == "print_best"} {
	    set output [get-chart-prop $generator "output"]
	    set ngram [get-chart-prop $generator "ngram_score"]
	    add_graph_feature $solution "GEN_OUTPUT" $output VT_STR
	    add_graph_feature $solution "GEN_SCORE" $ngram VT_PDFLOAT
	    
	    set chart [get_field $solution chart]
	    set property_weights_file [get_field $generator property_weights_file]
	    set score [select_most_probable_structure $solution $property_weights_file]
	    if {0} {
		puts "output = $output"
		puts "score = $score (ngram score + word count = $ngram)"
		puts ""
	    }
	    if {$score > $best_score} {
		set best_score $score
		set best $output
	    }
	}

	if {$enumerate == "first" || 
	    $enumerate == "most_probable" ||
	    $enumerate == "choose" || 
	    $enumerate == "packed"} {
	    break
	}
	incr id
	incr solution_no 1
	if {$enumerate != "all" &&
	    $solution_no > $enumerate} {
	    break
	}
	set solution [get_next_fstructure $fschart_graph $solution]
    }
    
    if {$action == "print_best"} {
	set gen_out_strings $old_gen_out_strings
    }
    if {$action == "save_all"} {
	set gen_out_strings $old_gen_out_strings
	set-chart-prop $generator training 0
    }
    
    return [list $best_score $best]

}

###############################################################################

proc translate {args} {
    global translator_started
    if {$translator_started == 0} {
	set rc [create-translator]
	if {$rc == 0} {
	    return 0
	}
    }

    if {[llength $args] == 2} {
	if {[lindex $args 0] == "-d"} {set display_results 1}
	set sentence [lindex $args 1]
    } else {
	set sentence [lindex $args 0]
	set display_results 0
    }
    
    clear-translation-windows

    puts "\nTranslating {$sentence}"

    set time [CPUtime {translate-sentence $sentence}]
    set time [expr {$time/[clock_rate]}]
    puts "Translation took $time seconds."
}

set print-translation-ids 0

proc translate-sentence {sentence {outputFile ""} {id ""}} {
    global translator_started defaultparser defaultTranslator
    global xfer_serv_input xfer_serv_output gen_serv_input
    global enumerate_parses enumerate_transfers
    global defaultchart defaultgenerator
    global print-translation-ids
    
    if {$translator_started == 0} {
	set rc [create-translator]
	if {$rc == 0} {
	    return 0
	}
    }

    set n_solutions [parse-sentence $sentence $defaultparser]
    # puts "There are $n_solutions parsing solutions."

    set action "print_best"
    if {[string match "*/" $outputFile]} {
	set action "save_all"
	set outputFile "${outputFile}S$id"
	if {[catch {exec sh -c "mkdir -p $outputFile"} error]} {
	    puts $error
	}
    }

    # when there are suboptimal solutions, parser reports n_solutions as N+M
    set n_solutions [optimal-count $n_solutions]

    if {$n_solutions > 1 && $enumerate_parses == "choose"} {
	show-solutions
	wm title [default-window fstructure] \
		"Choose a solution and select TRANSLATE from the menu"
	raise [default-window fstructure]
	raise [default-window choices]
	return 0
    }

    set root [root_edge $defaultparser]
    if {$root == ""} {
	puts "Parsing failed."
	if {$action == "print_best"} {
	    set best "{ }"
	    if {${print-translation-ids} == 1} {
		set best "$id: $best"
	    }
	    print-gen-output $best $defaultgenerator
	}
	return 0
    }

    if {$n_solutions > 1 && $enumerate_parses == "packed"} {
	# Don't enumerate.  Pass the parse graph to transfer as is.
	set chartgraph [extract_chart_graph $defaultchart]
	set solution $chartgraph
    } else {
	# Initialize enumeration
	set solution [get_next_fstructure $root "(Graph)NULL"]
	if {$solution == ""} {
	    puts "Parsing failed."
	    if {$action == "print_best"} {
		set best "{ }"
		if {${print-translation-ids} == 1} {
		    set best "$id: $best"
		}
		print-gen-output $best $defaultgenerator
	    }
	    return 0
	}
    }

    if {[fragment_parse $defaultparser]} {
	puts "\nfragment parse"
    }

    set parse_no 1
    set transfer_no 1
    set transfers $enumerate_transfers
    if {[scan $enumerate_transfers %d] && [scan $enumerate_parses %d]} {
	set parses $enumerate_parses
	if {$n_solutions < $enumerate_parses} {
	    set parses $n_solutions
	}
	set transfers [expr {$enumerate_transfers/$parses}]
    }
    
    set best ""
    set best_score -10000

    while {$solution != ""} {

	set result [translate-this-solution $solution $transfers \
		    $action $outputFile $transfer_no]
	incr transfer_no $transfers
	if {$action == "print_best"} {
	    set temp [lindex $result 1]
	    set temp_score [lindex $result 0]
	    if {$temp != "" && $temp_score > $best_score} {
		set best_score $temp_score
		set best $temp
	    }
	}

	if {$enumerate_parses == "first" || $enumerate_parses == 1 ||
	    $enumerate_parses == "choose" || $enumerate_parses == "packed"} {
	    break
	}
	incr parse_no 1
	if {$enumerate_parses != "all" &&
	    $parse_no > $enumerate_parses} {
	    break
	}
	set solution [get_next_fstructure $root $solution]
    }

    if {$action == "print_best"} {
	if {$best == ""} {set best "{ }"}
	if {${print-translation-ids} == 1} {
	    set best "$id: $best"
	}
	print-gen-output $best $defaultgenerator
    }
    return 1
}

proc get-nth-sentence {filename n} {
    if {$filename == ""} {return ""}
    set fileID [open $filename]
    set sentence ""
    while {$n > 0} {
	set sentence [get-next-sentence $fileID]
	incr n -1
    }
    close $fileID
    return $sentence
}

set goldTranslationFile ""
set baseTranslationFile ""

proc translateProc {sentence parser n parseData} {
    global defaulttranslator noTk translator_started
    global goldTranslationFile baseTranslationFile
    global defaultparser defaultgenerator
    if {$translator_started == 0} {
	if {![create-translator]} {return}
    }    
    puts "\n$n: Translating {$sentence}"
    set gold [get-nth-sentence $goldTranslationFile $n]
    set base [get-nth-sentence $baseTranslationFile $n]
    if {$gold != ""} {
	puts "gold = $gold"
    }
    
    if {$base != ""} {
	puts "base = $base"
    }
    set time [CPUtime {set count [translate-sentence $sentence $parseData $n]}]
    # print-prolog-chart-graph /tilde/maxwell/temp/transfer/S$n.pl $parser 1
    set time [expr {[lindex $time 0]/[clock_rate]}]
    set nsubtrees [count_chart_subtrees $defaulttranslator]
    incr nsubtrees [count_chart_subtrees $defaultparser]
    incr nsubtrees [count_chart_subtrees $defaultgenerator]
    return [list $count $time $nsubtrees]
}

proc translate-testfile {testfile start {end ""} {outputFile ""}} {
    global defaulttranslator
    global gen_out_strings
    if {$end == ""} {set end $start}
    if {$outputFile == ""} {
	set outputFile stdout
    } else {
	puts "Writing translations to $outputFile."
    }
    if {![string match "*/" $outputFile]} {
	set orig_gen_out_strings $gen_out_strings
	set gen_out_strings [open_gen_output $outputFile]
    }
    parse-testfile $testfile $start $end -parser $defaulttranslator \
	-parseProc translateProc -parseData $outputFile -suppressLogFiles
    if {![string match "*/" $outputFile]} {
	close_gen_output $gen_out_strings
	set gen_out_strings $orig_gen_out_strings
    }
    return ""
}

###############################################################################

proc display-transfer-results {} {
    display-source-and-target
}

proc display-source-and-target {{xferin ""} {xferout ""}} {
    global xfer_serv_input xfer_serv_output
    global window_width window_height

    if {$xferin == ""} {
	set xferin $xfer_serv_input
    }
    if {$xferout == ""} {
	set xferout $xfer_serv_output
    }

    set source_graph [read_prolog_graph $xferin]
    set target_graph [read_prolog_graph $xferout]

    display-fstructures $source_graph .source   "" "Source" 0
    display-fstructures $target_graph .transfer "" "Transfer Result" 0

    set xloc [expr {$window_width  + 20}]
    set yloc [expr {$window_height + 40}]
    wm geometry .transfer ${window_width}x${window_height}+${xloc}+${yloc}

    raise .source
    raise .transfer
}

proc is-transferred-graph-ambiguous {} {
    global xfer_serv_output
    set graph [read_prolog_graph $xfer_serv_output]
    set disj [get_field $graph disjunctions]
    if {$disj == ""} {
	return 0
    } else {
	return 1
    }
}

###############################################################################

proc clear-translation-windows {} {
    clear-window .source
    clear-window .transfer
    clear-window [default-window fschart]
    clear-window [default-window choices]
}



###############################################################################
###############################################################################
##
## 3. For sentence condensation 
##
###############################################################################
###############################################################################



proc initialize_summarizer {pGram gGram weightsFile} {
     global defaultparser
     global defaultgenerator
     global selectMostProbable

     set defaultparser [create-parser $pGram]
     set defaultgenerator [create-generator $gGram]
     set selectMostProbable 1
     setx property_weights_file "$weightsFile"
}


###############################################################################


proc silent_parse {sentence filename {parser 0}} {
    global defaultparser
    global selectMostProbable

    if {$parser == 0} {
	set parser $defaultparser
    }

    reset_storage $parser
    set return [parse-sentence $sentence $parser]
    if {$return == -1} {
	return $return
    } else {
	set fschart [extract_chart_graph $parser]
	unmark_all_choices $fschart
	if {$selectMostProbable == 1} {
	    select-most-probable-structure $fschart
	    set tree [get_chosen_tree $fschart]
	    if {$tree == ""} {set tree "(DTree)0"}
	    set solution [get_chosen_fstructure $fschart $tree]
	} else {
	    set solution $fschart
	}
	print_graph_as_prolog $solution $filename $sentence
	return $return
    }
}



proc silent_parse_ptr {sentence {parser 0}} {
    global defaultparser
    global selectMostProbable

    if {$parser == 0} {
	set parser $defaultparser
    }
    reset_storage $parser
    set return [parse-sentence $sentence $parser]
    if {$return == -1} {
	return $return
    } else {
	set fschart [extract_chart_graph $parser]
	unmark_all_choices $fschart
	if {$selectMostProbable == 1} {
	    select-most-probable-structure $fschart
	    set tree [get_chosen_tree $fschart]
	    if {$tree == ""} {set tree "(DTree)0"}
	    set solution [get_chosen_fstructure $fschart $tree]
	} else {
	    set solution $fschart
	}
	return $solution
    }
}



###############################################################################
###############################################################################
##
## 4.  Trying to re-do the transfer server set up(!)
##
###############################################################################
###############################################################################


# File names, etc.

set development_transfer_path ""
set seconds_wait_for_xfer_serv 10

set defaultSocketPorts(transfer) 2548

set serv_transferRulesLoaded 0
set serv_translator_started 0
set xfer_serv_started 0

###############################################################################

proc install-transfer-server-menu-items {} {
    global fsCommands fsChartCommands

    add-item-to-xle-menu \
	    {command -label "Transfer this FStructure (serv)" \
	    -command "server-transfer-this-solution 1 $self" \
	    -doc "Display the structures created by the transfer system."} \
	    fsCommands

    add-item-to-xle-menu \
	    {command -label "Translate this FStructure (serv)" \
	    -command "server-translate-this-solution $self" \
	    -doc "Translate (transfer, then generate) from this fstructure."} \
	    fsCommands

    add-item-to-xle-menu \
	    {command -label "Reload Transfer Rules (serv)" \
	    -command server-reload-transfer-rules \
	    -doc "Cause the transfer server to reload original rule file."} \
	    fsCommands

    add-item-to-xle-menu \
	    {command -label "Transfer this FSChart (serv)" \
	    -command "server-transfer-this-solution 1 $self" \
	    -doc "Display the structures created by the transfer system."} \
	    fsChartCommands

    add-item-to-xle-menu \
	    {command -label "Translate this FSChart (serv)" \
	    -command "server-translate-this-solution $self" \
	    -doc "Translate (transfer, then generate) from this fschart."} \
	    fsChartCommands

    add-item-to-xle-menu \
	    {command -label "Reload Transfer Rules (serv)" \
	    -command server-reload-transfer-rules \
	    -doc "Cause the transfer server to reload original rule file."} \
	    fsChartCommands

}

###############################################################################


proc create-translation-server-menu {{src_grammar ""} {trg_grammar ""} \
	                {xfer_grammar ""} {gen_underspec ""}} {
    install-transfer-server-menu-items
    create-translation-server $src_grammar $trg_grammar \
	                      $xfer_grammar $gen_underspec 
}

proc create-translation-server {{src_grammar ""} {trg_grammar ""} \
	                {xfer_grammar ""} {gen_underspec ""}} {
    global serv_translator_started defaultTranslator
    global defaultparser gen_serv_started serv_transferRulesLoaded

    if {$serv_translator_started} {
	puts "Translation server already started ..."
	return 1
    }
    set serv_translator_started 1

    if {$src_grammar == ""} {
	set src_grammar   $defaultTranslator(source)
    }
    if {$trg_grammar == ""} {
	set trg_grammar   $defaultTranslator(target)
    }
    if {$xfer_grammar == ""} {
	set xfer_grammar  $defaultTranslator(transfer)
    } else {
        set defaultTranslator(transfer) $xfer_grammar
    } 
    if {$gen_underspec == ""} {
	set gen_underspec $defaultTranslator(underspec)
    }

    if {![info exists defaultparser] || $defaultparser == ""} {
	create-parser $src_grammar
    } else {
	puts "Parser has already been created."
    }

    if {$gen_serv_started == 0} {
	if {$gen_underspec != ""} {
	    # set gen add in the parser process.  When the parser starts the
	    # generation process, it will send these settings to it.
	    set-gen-adds add $gen_underspec
	}
	if {[start-generation-server $trg_grammar] == 0} {
	    return 0
	}
    } else {
	puts "Generation server has already been started."
    }

    if {$serv_transferRulesLoaded == 0} {
	server-reload-transfer-rules $defaultTranslator(transfer)
    } else {
	puts "Transfer rules have already been loaded."
    }
    return 1
}

###############################################################################


proc start-transfer-server {{grammar ""} {port "default"} {host "localhost"}} {
    global development_transfer_path env defaultSocketPorts
    global xfer_client_socket seconds_wait_for_xfer_serv
    global xfer_serv_started xfer_serv_input xfer_serv_output

    if {$xfer_serv_started && \
	[catch {eof $xfer_client_socket}] == 0} {
	return 1
    }

    puts "Starting a transfer server.  Please wait..."

    if {$development_transfer_path == ""} {
	if {[info exists env(XLEPATH)]} {
	    set path "$env(XLEPATH)/bin"
	} else {
	    set path "$env(HOME)/xle/transfer"
	}
    } else {
	set path $development_transfer_path
    }

    if {$port == "default"} {
	set port $defaultSocketPorts(transfer)
    }

    catch {exec xterm -sb -sl 500 -title Transfer_Server -e \
	   $path/transfer server  $host $port &}

    catch {exec sleep $seconds_wait_for_xfer_serv}

    set xfer_client_socket [start-xle-client $port $host]

    set ack [tell-xle-server $xfer_client_socket \
		 "(nl,write('Transfer system is ready ...'),nl,nl)."]

    if {$ack != "yes"} {
	puts "Transfer system didn't start properly."
	return 0
    }
    set xfer_serv_started 1

    set ack [tell-xle-server $xfer_client_socket \
		 "load_rules('$grammar')."]

    if {$ack != "yes"} {
	puts "Transfer system couldn't load rules from $grammar."
	return 0
    }

    return 1
}

###############################################################################


proc server-reload-transfer-rules {{rules ""}} {
    global serv_transferRulesLoaded defaultTranslator
    global xfer_client_socket

    if {$rules == ""} {
	set command "reload_rules."
        set grammar $defaultTranslator(transfer)
    } else {
	set command "force_load_rules('$rules')."
        set grammar $rules
    }

    if {[start-transfer-server $grammar] == 0} {
	puts stderr "Starting transfer server failed!."
	return 0
    }

    set ack [tell-xle-server $xfer_client_socket $command]

    if {$ack != "yes"} {
	puts "Transfer system couldn't reload rules."
	return 0
    }
    set serv_transferRulesLoaded 1
    return 1
}


    

###############################################################################


proc server-transfer-this-solution {display_result {solution ""}} {
    global xfer_client_socket xfer_serv_input xfer_serv_output
    global defaultTranslator serv_transferRulesLoaded

    if {!$display_result && [winfo exists .transfer]} {
	clear-data-props .transfer
	destroy .transfer
    }

    if {[start-transfer-server $defaultTranslator(transfer)] == 0} {
	puts stderr "Starting transfer server failed!."
	return 0
    }

    if {!$serv_transferRulesLoaded} {
	server-reload-transfer-rules $defaultTranslator(transfer)
    }

    if {[string index $solution 0] == "."} {
	set solution [get-window-prop $solution data]
    }

    if {$solution == ""} {
	print-fs-as-prolog $xfer_serv_input [default-window fstructure]
    } else {
	set chart [get_field $solution chart]
	set sentence [get-chart-prop $chart sentence]
	if {[get_field $solution disjunctions] != "" &&
	    [info procs choose-transfer-solution] != ""} {
	    set solution [choose-transfer-solution $sentence $solution]
	}   
	print_graph_as_prolog $solution $xfer_serv_input $sentence
    }
    file attributes $xfer_serv_input -permissions 0666

    set cmd "transfer('$xfer_serv_input','$xfer_serv_output')."

    set ack [tell-xle-server $xfer_client_socket $cmd]

    if {$ack == "yes"} {
	file attributes $xfer_serv_output -permissions 0666
	if {$display_result} {
	    display-transferred-graph $xfer_serv_output
	}
	return 1
    } else {
	puts stderr "Transfer failed."
	return 0
    }
}

###############################################################################

proc server-translate-this-solution {window} {
    global gen_serv_input xfer_serv_output xfer_serv_input
    global defaultTranslator enumerate_transfers

    set solution [get-window-prop $window data]
    if {[server-transfer-this-solution 0 $solution] == 0} {
	return
    }

    if {[is-transferred-graph-ambiguous] && $enumerate_transfers == "choose"} {
	puts "Result of transfer is ambiguous ..."
	display-ambiguous-transferred-graph $xfer_serv_output
    } else {
	file copy -force $xfer_serv_output $gen_serv_input
	file attributes $gen_serv_input -permissions 0666

	set trg_grammar  $defaultTranslator(target)
	if {[start-generation-server $trg_grammar] == 0} {
	    puts stderr "Starting generation server failed!."
	    return 0
	}
	if {[generate-from-this-solution 0 "all"] == 0} {
	    display-source-and-target
	}
    }
    return
}

###############################################################################
# The following are semi-defunct
###############################################################################



proc pl {prolog} {
    global xfer_client_socket

    if {$prolog != ""} {
	puts [tell-xle-server $xfer_client_socket $prolog]
    }
}

###############################################################################




proc load-transfer-hook {module} {
    global xfer_client_socket xfer_serv_started

    if {$xfer_serv_started == 0} {
	puts "You have to first start a transfer system."
	return
    }

    set ack [tell-xle-server $xfer_client_socket "ensure_loaded($module)."]

    if {$ack != "yes"} {
	puts "Transfer system couldn't load module $module."
    }
    return
}

###############################################################################

proc print-compiled-transfer-rules {} {
    global xfer_client_socket xfer_serv_started

    set temp_file "/tmp/xfer_compiled_rules"

    if {$xfer_serv_started == 0} {
	puts "You have to first start a transfer system."
	return
    }

    set ack [tell-xle-server $xfer_client_socket \
	    "print_compiled_transfer_rules('$temp_file')."]

    if {$ack != "yes"} {
	puts "Transfer system couldn't print compiled rules."
    } else {
	set f [open $temp_file r]
	set i 0
	while {[gets $f line] >= 0} {
	    if {$line != ""} {
		incr i
	    }
	    puts $line
	}
	if {$i == 0} {
	    puts stderr "No compiled rules found."
	    return
	}
	close $f
    }
    return
}




##########################################################################
#
#  For summarization
#
##########################################################################


proc initialize_summarizer {pGram gGram weightsFile} {
     global defaultparser
     global defaultgenerator
     global selectMostProbable

     set defaultparser [create-parser $pGram]
     set defaultgenerator [create-generator $gGram]
     set selectMostProbable 1
     setx property_weights_file "$weightsFile"
}








###############################################################################
###############################################################################
##
## 5. The following are for the statistical translator
##
###############################################################################
###############################################################################

proc create-transfer-chart {file} {
    global defaulttranslator defaultchart
    set defaulttranslator [create_translator $file]
    if {$defaultchart == ""} {
	set defaultchart $defaulttranslator
    }
    return $defaulttranslator
}

###############################################################################

proc transfer-fstructure {input output 
    {cutoff 2} {selected 0} {translator ""}} {
    global defaulttranslator
    if {$translator == ""} {
	set translator $defaulttranslator
    }
    transfer_fstructure $translator $input $cutoff
    print-prolog-chart-graph $output $translator $selected
}

###############################################################################

proc extract-transfer-rules-from-subdirs {begin end} {
    global alignments dir1 dir2 outputPrefix
    for {set i $begin} {$i <= $end} {incr i} {
	set start [expr {$i*1000 + 1}]
	set stop  [expr {($i + 1) * 1000}]
	puts "gunzipping $dir1/$start-$stop/*.pl.gz"
	if {[catch {exec sh -c "gunzip $dir1/$start-$stop/*.pl.gz"} error]} {
	    puts $error
	}
	puts "gunzipping $dir2/$start-$stop/*.pl.gz"
	if {[catch {exec sh -c "gunzip $dir2/$start-$stop/*.pl.gz"} error]} {
	    puts $error
	}
	extract_transfer_rules -alignments $alignments -sourceStem $dir1/$start-$stop -targetStem $dir2/$start-$stop -outRules $outputPrefix${i}.pl -from $start -to $stop
	puts "gzipping $dir1/$start-$stop/*.pl"
	if {[catch {exec sh -c "gzip $dir1/$start-$stop/*.pl"} error]} {
	    puts $error
	}
	puts "gzipping $dir2/$start-$stop/*.pl"
	if {[catch {exec sh -c "gzip $dir2/$start-$stop/*.pl"} error]} {
	    puts $error
	}
    }
}

###############################################################################

proc disambiguate-sentence-pairs-in-subdirs {begin end} {
    global alignments dir1 dir2
    for {set i $begin} {$i <= $end} {incr i} {
	set start [expr {$i*1000 + 1}]
	set stop  [expr {($i + 1) * 1000}]
	puts "gunzipping $dir1/$start-$stop/S*.pl.gz"
	if {[catch {exec sh -c "gunzip $dir1/$start-$stop/S*.pl.gz"} error]} {
	    puts $error
	}
	puts "gunzipping $dir2/$start-$stop/S*.pl.gz"
	if {[catch {exec sh -c "gunzip $dir2/$start-$stop/S*.pl.gz"} error]} {
	    puts $error
	}
	disambiguate_sentence_pairs -alignments $alignments -sourceStem $dir1/$start-$stop -targetStem $dir2/$start-$stop -from $start -to $stop
	puts "gzipping $dir1/$start-$stop/S*.pl"
	if {[catch {exec sh -c "gzip $dir1/$start-$stop/S*.pl"} error]} {
	    puts $error
	}
	puts "gzipping $dir2/$start-$stop/S*.pl"
	if {[catch {exec sh -c "gzip $dir2/$start-$stop/S*.pl"} error]} {
	    puts $error
	}
    }
}

###############################################################################

proc extract-dominance-statistics-in-subdirs {begin end} {
    global dir1 outputPrefix
    for {set i $begin} {$i <= $end} {incr i} {
	set start [expr {$i*1000 + 1}]
	set stop  [expr {($i + 1) * 1000}]
	puts "extracting from $dir1/$start-$stop"
	if {0} {
	puts "gunzipping $dir1/$start-$stop/S*.pl.gz"
	if {[catch {exec sh -c "gunzip $dir1/$start-$stop/S*.pl.gz"} error]} {
	    puts $error
	}
    }
	set db $outputPrefix/dominance$i.db
	extract_dominance_statistics -sourceStem $dir1/$start-$stop -from $start -to $stop -db $db
        if {0} {
	puts "gzipping $dir1/$start-$stop/S*.pl"
	if {[catch {exec sh -c "gzip $dir1/$start-$stop/S*.pl"} error]} {
	    puts $error
	}
	}
    }
}

###############################################################################

proc extract-lexical-cooccurrences-in-subdirs {begin end} {
    global alignments dir1 dir2 outputPrefix
    for {set i $begin} {$i <= $end} {incr i} {
	set start [expr {$i*1000 + 1}]
	set stop  [expr {($i + 1) * 1000}]
	if {0} {
	puts "gunzipping $dir1/$start-$stop/S*.pl.gz"
	if {[catch {exec sh -c "gunzip $dir1/$start-$stop/S*.pl.gz"} error]} {
	    puts $error
	}
	puts "gunzipping $dir2/$start-$stop/S*.pl.gz"
	if {[catch {exec sh -c "gunzip $dir2/$start-$stop/S*.pl.gz"} error]} {
	    puts $error
	}
	}
	set db $outputPrefix/lexical${i}.db
	extract_lexical_cooccurrences -alignments $alignments -sourceStem $dir1/$start-$stop -targetStem $dir2/$start-$stop -from $start -to $stop -db $db
	if {0} {
	puts "gzipping $dir1/$start-$stop/S*.pl"
	if {[catch {exec sh -c "gzip $dir1/$start-$stop/S*.pl"} error]} {
	    puts $error
	}
	puts "gzipping $dir2/$start-$stop/S*.pl"
	if {[catch {exec sh -c "gzip $dir2/$start-$stop/S*.pl"} error]} {
	    puts $error
	}
	}
    }
}

###############################################################################

proc transfer-fstructures-in-subdirs {begin end cutoff {selected 0}} {
    global dir1 dir2
    for {set i $begin} {$i <= $end} {incr i} {
	set start [expr {$i*1000 + 1}]
	set stop  [expr {($i + 1) * 1000}]
	set subdir1 $dir1/$start-$stop
	set subdir2 $dir2/$start-$stop
	puts "gunzipping $subdir1/*.pl.gz"
	if {[catch {exec sh -c "gunzip $subdir1/*.pl.gz"} error]} {
	    puts $error
	}
	puts "gunzipping $subdir2/*.pl.gz"
	if {[catch {exec sh -c "gunzip $subdir2/*.pl.gz"} error]} {
	    puts $error
	}
	for {set j $start} {$j <= $stop} {incr j} {
	    transfer-fstructure $subdir1/S$j.pl $subdir2/T$j.pl \
		$cutoff $selected
	    puts "S$j.pl --> $subdir2/T$j.pl"
	}
	puts "gzipping $subdir1/S*.pl"
	if {[catch {exec sh -c "gzip $subdir1/S*.pl"} error]} {
	    puts $error
	}
	puts "gzipping $subdir1/T*.pl"
	if {[catch {exec sh -c "gzip $subdir1/T*.pl"} error]} {
	    puts $error
	}
	puts "gzipping $subdir2/S*.pl"
	if {[catch {exec sh -c "gzip $subdir2/S*.pl"} error]} {
	    puts $error
	}
	puts "gzipping $subdir2/T*.pl"
	if {[catch {exec sh -c "gzip $subdir2/T*.pl"} error]} {
	    puts $error
	}
    }
}

###############################################################################

proc add-rhs-frequencies {root inverse} {
    puts $root
    set files [glob -nocomplain $root/*.pl]
    foreach file $files {
	add_rhs_frequencies $file $inverse
    }
    set dirs [glob -nocomplain $root/*]
    foreach dir $dirs {
	if {![file isdirectory $dir]} {continue}
	add-rhs-frequencies $dir $inverse
    }
}

###############################################################################
###############################################################################
##
## 6. The remainder may (or may not) be only of historical interest
##
###############################################################################
###############################################################################


proc transfer-fschart {fschart_file xferin xferout
			{enum_solutions all} {fschart ""}} {
    global xfer_client_socket xfer_serv_input xfer_serv_output
    global defaultTranslator disamb_client_socket


    if {[start-transfer-server $defaultTranslator(transfer)] == 0} {
	puts stderr "Starting transfer server failed!."
	return 0
    }

    if {$enum_solutions == "packed"} {
	set xfer_command "transfer('$fschart_file','${xferout}.pl')."
	set ack [tell-xle-server $xfer_client_socket $xfer_command]

	if {$ack != "yes"} {
	    puts stderr "Transfer failed."
	}
	return
    }

    if {[string match "best*" $enum_solutions]} {

	 if {[start-disamb-server $defaultTranslator(disambiguator)] == 0} {
	     puts stderr "Starting disambiguation server failed!."
	     return 0
	 }

	 set disamb_command  "unfold_to_files('[file join [pwd] $fschart_file]','${xferin}')."

	 set ack [tell-xle-server $disamb_client_socket $disamb_command]

	 if {$ack != "yes"} {
	     puts stderr "Disambiguation failed."
	     return
	 }

	 foreach fschart_file [glob -nocomplain ${xferin}.*] {
	     set pref [file join [pwd] [file rootname [file rootname $fschart_file]]]
	     set ext [file extension $fschart_file]
	     set xfer_command "transfer('$fschart_file','${pref}.xferout${ext}.pl')."
	     set ack [tell-xle-server $xfer_client_socket $xfer_command]
	     
	     if {$ack != "yes"} {
		 puts stderr "Transfer failed, ack = `$ack'"
		 return 0
		 
	     }
	 }
	 return
     }
    
    if {$fschart == ""} {
	set fschart [read_prolog_graph $fschart_file]
	set sentence [get_field $fschart sentence]
	set chart [get_field $fschart chart]
	set-window-prop $chart sentence $sentence
    }

    if {$fschart == "" || ![string match "(Graph)*" $fschart]} {
	puts "Can't obtain a valid graph."
	return
    }

    set solution [get_next_fstructure $fschart "(Graph)NULL"]
    set solution_no 1

    while {$solution != ""} {
	
	puts "Transferring solution $solution_no ..."

	set xfer_serv_in_seq  ${xferin}.${solution_no}.pl
	set xfer_serv_out_seq ${xferout}.${solution_no}.pl

	print-prolog-chart-graph $xfer_serv_in_seq $solution

	set xfer_command "transfer('$xfer_serv_in_seq','$xfer_serv_out_seq')."
	set ack [tell-xle-server $xfer_client_socket $xfer_command]

	if {$ack != "yes"} {
	    puts stderr "Transfer failed."
	    return 0
	}
	
	incr solution_no
	if {$enum_solutions != "all" && $solution_no > $enum_solutions} {
	    break
	}
	set solution [get_next_fstructure $fschart $solution]
    }
    puts "... done."
}


###############################################################################

set defaultSocketPorts(disamb) 7654
set disamb_serv_started 0
set seconds_wait_for_disamb_serv 10


if {[file exists /home/eisele/lfg/disamb/pl/score_chart.pl]} {
    # we're at XRCE
    set disambiguator_startup /home/eisele/lfg/disamb/pl/score_chart.pl
    set defaultTranslator(disambiguator) /home/eisele/lfg/disamb/models/hc_en_bnc_fs
} else {
    set disambiguator_startup /tilde/aeisele//lfg/disamb/pl/score_chart.pl
    set defaultTranslator(disambiguator) /tilde/aeisele/lfg/disamb/models/hc_en_bnc_fs
}	


###############################################################################

#
# The following two functions were written for Martin, to enable an
# enumeration of results, when xle is being called from prolog.
#
proc parse-and-print-solution {sentence file {enumerate "first"}} {
    global defaultparser keep_current_fstructure keep_current_root

    reset_prolog_storage

    parse-sentence $sentence $defaultparser
    
    set root [root_edge $defaultparser]
    if {$root == ""} {
	puts "Parsing failed."
	return
    }

    set keep_current_fstructure "(Graph)NULL"
    set keep_current_root $root

    if {$enumerate == "all"} {
	print-prolog-chart-graph $file
	# BUT NOTE THAT PARSING MAY NOT HAVE BEEN SUCCESSFUL
    } else {
	set fstructure [get_next_fstructure $root "(Graph)NULL"]
	if {$fstructure == ""} {
	    puts "Parsing failed."
	    # Return an empty file.
	    set f [open $file w]
	    puts $f ""
	    return
	}
	print_graph_as_prolog $fstructure $file $sentence
	set keep_current_fstructure $fstructure
    }
}

proc print-next-parsing-solution {file} {
    global keep_current_fstructure keep_current_root

    set fstructure [get_next_fstructure $keep_current_root \
					$keep_current_fstructure]

    if {$fstructure != ""} {
	set chart [get_field $fstructure chart]
	set sentence [get-chart-prop $chart sentence]
	print_graph_as_prolog $fstructure $file $sentence
	set keep_current_fstructure $fstructure
    } else {
	# at end of cycle, simply return an empty file.
	set f [open $file w]
	puts $f ""
    }
}

###############################################################################

proc old-translate-testfile {file {directory ""} {genout ""}} {
    transfer-testfile $file $directory

    if {$directory == ""} {
	set directory "[file rootname $file]_dir"
    }

    if {$genout == ""} {
	set genout "${directory}/generation-results"
    }

    generate-from-transfer-directory $directory $genout
}

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

proc transfer-testfile {testfile {directory ""}} {
    global defaultTranslator  defaultparser
    global xfer_serv_started xfer_client_socket enumerate_parses

    set src_grammar   $defaultTranslator(source)
    set xfer_grammar  $defaultTranslator(transfer)

    if {$directory == ""} {
	set directory "[file rootname $testfile]_dir"
	puts "The files will be created in the \"$directory\" directory."
    }

    if {![info exists defaultparser] || $defaultparser == ""} {
	create-parser $src_grammar
    }

    if {$xfer_serv_started == 0} {
	if {[start-transfer-server $xfer_grammar] == 0} {
	    return 0
	}
	if {[tell-xle-server $xfer_client_socket \
		"assert(user:dont_print_timestamps)."] != "yes"} {
	    puts "Transfer system rejected a request."
	}
    }
    
    if {$directory == ""} {
	set directory "[file rootname $testfile]_dir"
    }

    if {[file exists $directory]} {
	puts stderr "Directory \"$directory\" already exists."
	return
    }
    file mkdir $directory

    parse-testfile $testfile -outputPrefix "${directory}/"

    foreach fschart_file [glob -nocomplain ${directory}/S\[0-9\]*.pl] {
	set pref [file join [pwd] [file rootname $fschart_file]]
	transfer-fschart $fschart_file "${pref}.xferin" "${pref}.xferout" \
		$enumerate_parses
    }
}

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

proc generate-from-transfer-directory {dir {outfile ""}} {
    global gen_serv_started gen_serv_input gen_serv_output xle_client_socket
    global defaultTranslator enumerate_parses

    if {[regexp {best[_]*([0-9]*)} $enumerate_parses match n]} {
	set max_gen_solution  $n
	if {$max_gen_solution == ""} {
	    set max_gen_solution 1
	}
    } else {
	set max_gen_solution 1000
    }

    set trg_grammar   $defaultTranslator(target)
    set gen_underspec $defaultTranslator(underspec)

    if {$gen_serv_started == 0} {
	if {$gen_underspec != ""} {
	    # set gen add in the parser process.  When the parser starts the
	    # generation process, it will send these settings to it.
	    set-gen-adds add $gen_underspec
	}
	if {[start-generation-server $trg_grammar] == 0} {
	    return 0
	}
    }

    if {$outfile == ""} {
	set outstream stdout
    } else {
	set outstream [open $outfile w]
	if {$outstream == ""} {
	    puts stderr "Opening output file ($outfile) failed!"
	    return
	}
    }

    foreach xferout [glob -nocomplain ${dir}/S\[0-9\]*.xferout.*.pl] {
	

	set solution_no [string trim [file extension [file rootname $xferout]] "."]
	puts "solution_no= ${solution_no} file= $xferout"

	if {$solution_no > $max_gen_solution} {

	    puts "ignored"

	} else {


	    file copy -force $xferout $gen_serv_input
	    file attributes $gen_serv_input -permissions 0666


	    
	    if {[string match "${dir}/S\[0-9\]*.xferout.1.pl" $xferout]} {
		set parsing_solution_no 1
		set src [file rootname [file rootname [file rootname $xferout]]]
		set label [file tail $src]
		set tmp [open ${src}.pl r]
		if {[gets $tmp line] >= 0} {
		    set f [string first "'" $line]
		    set l  [string last  "'" $line]
		    if {$f != -1 && $l != -1} {
			incr f +1
			incr l -1
			set label "$label: [string range $line $f $l]"
		    }
		}
		close $tmp
		puts $outstream "-------------------------------------------------------------------------------\n\n$label\n"
	    } else {
		incr parsing_solution_no
		puts $outstream "\n--- \[Parse no. $parsing_solution_no\] ---\n"
	    }
	    generate-from-this-solution 0 "all" $outstream
	    flush $outstream
	}
    }
    puts $outstream "-------------------------------------------------------------------------------\n"
    close $outstream
}


###############################################################################

proc get-next-ranked-solution {graph {first ""}} {
    global ranked_solutions

    if {$first == "first"} {
	set ranked_solutions \
		"(Clause)[get_external_graph_prop_addr $graph ranking]"
    } else {
	set ranked_solutions [get_field $ranked_solutions next]
    }

    set chart [get_field $graph chart]
    unmark_all_choices $graph
    choose_solution [get_field $ranked_solutions item]

    display-chosen-solution $chart
    redisplay-choices "." $chart
}

###############################################################################

proc diff-xfer {Source Target {diff_file "/tmp/diff_xfer.pl"}} {
    global env
    if {[info exists env(XLEPATH)]} {
	set path "$env(XLEPATH)/prolog"
    } else {
	set path "$env(HOME)/xle/prolog"
    }

    set SRC [file join [pwd] $Source]
    set TRG [file join [pwd] $Target]
    set DIF [file join [pwd] $diff_file]

    catch "exec prolog  -l ${path}/align-preds.ql -a $SRC $TRG $DIF"

    read-prolog-chart-graph $diff_file
}

###############################################################################

# this is still half-baked...

proc start-disamb-server {{grammar ""} {port "default"} {host "localhost"}} {
    global development_disamb_path env defaultSocketPorts
    global disamb_client_socket seconds_wait_for_disamb_serv
    global disamb_serv_started disamb_serv_input disamb_serv_output
    global disambiguator_startup

    if {$disamb_serv_started && \
	[catch {eof $disamb_client_socket}] == 0} {
	return 1
    }

    puts "Starting a disamb server.  Please wait..."


    if {$port == "default"} {
	set port $defaultSocketPorts(disamb)
    }

    catch {exec xterm -sb -sl 500 -iconic -title Disambiguation_Server -e \
	   sicstus -l $disambiguator_startup -a server $host $port &}

    catch {exec sleep $seconds_wait_for_disamb_serv}

    set disamb_client_socket [start-xle-client $port $host]

    set ack [tell-xle-server $disamb_client_socket \
		 "(nl,write('Disambiguation system is ready ...'),nl,nl)."]

    if {$ack != "yes"} {
	puts "Disambiguator didn't start properly."
	return 0
    }
    set disamb_serv_started 1

    set ack [tell-xle-server $disamb_client_socket \
		 "ensure_loaded('$grammar')."]

    if {$ack != "yes"} {
	puts "Disambiguator couldn't load rules from $grammar."
	return 0
    }

    return 1
}

proc print-best-alignments {filename} {
    set file [open $filename r]
    set out [open $filename.new w]
    set word ""
    puts "Writing best alignments in $filename to $filename.new"
    while {1} {
	set length [gets $file line]
	if {$length == -1} {break}
	set line [string trim $line]
	set words [split $line " \t\n\r"]
	set freq [lindex $words 0]
	set source [lindex $words 1]
	set target [lindex $words 2]
	if {$source != $word} {
	    if {$word != ""} {
		puts $out "$bestfreq $word $besttarget"
	    }
	    set word $source
	    set bestfreq 0
	}
	if {$freq > $bestfreq} {
	    set bestfreq $freq
	    set besttarget $target
	}
    }
    close $file
    close $out
}

