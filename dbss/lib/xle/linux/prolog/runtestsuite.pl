/* (c) 2001 by the Xerox Corporation.  All rights reserved */

/* ------------------------------------------------------------------------ */
/* Project:   XLE Test Suite                                                */
/* Author:    Dick Crouch                                                   */
/*            Jonathan Reichenthal                                          */
/* File:      testsuite.pl                                                  */
/* Language:  SICStus Prolog 3.7.1                                          */
/* Stuff for running syntactic test suites                                  */
/* ------------------------------------------------------------------------ */

% 1. Converts test suite in file TSFile to prolog readable form and reads
%    it in (mainly to use prolog reader to check syntax of f-descriptions).
%    Sets up records
%       stce(Num,WordString,FDescription)
%
% 2. Creates XLE test file sentences.out from WordStrings, and runs XLE in
%    batch mode on it.  Combines resulting f-strs in file ParserResults.pl
%    and reads this in.
%    Sets up records
%       sentence(Num,PackedFStr)
%
% 3. For each sentence, unpack its f-str and match against its f-description.
%    Assert record
%       ts_syn_result(WordString,Contexts,PackedFstr,FDescription)
%    where Contexts is list of contexts whose f-strs match the description
%    Note:
%       Contexts = no_parse   means  sentence did not receive any parse
%       Contexts = []         means  sentence parsed, but didn't match
%       Contexts = [[]]       means  true context matched
%
% 4. Read in file containing results from previous run of test suite
%    (if any).  Sets up records
%       ts_syn_previous(WordString,Contexts,PackedFstr,FDescription)
%
% 5. Compare results, write report to file TSFile.report
%    Write current results to TSFile.results.Date

:- use_module(library(system)).
:- use_module(library(lists)).


run_test(TSFileName) :-
	absolute_testsuite_file_name(TSFileName,AbsTSFileName),
	get_comparison_syn_ts_file_name(AbsTSFileName,TSPreviousResultsFile),
	run_test_suite(AbsTSFileName,TSPreviousResultsFile).

run_test_suite(TSFileName,TSPreviousResultsFile) :-
	absolute_testsuite_file_name(TSFileName,AbsTSFileName),
	(
	    TSPreviousResultsFile = no_comparison_file ->
	    AbsTSPreviousResultsFile = no_comparison_file
	;
	    absolute_testsuite_file_name(TSPreviousResultsFile,
					 AbsTSPreviousResultsFile)
	),
	name_syn_ts_results_file(AbsTSFileName,TSResultsFile),
	format("~nRunning syntax test suite:    ~w",  [AbsTSFileName]),
	format("~nStoring results in file:      ~w",  [TSResultsFile]),
	format("~nComparing results to:         ~w~n~n",
	       [AbsTSPreviousResultsFile]),
	abolish_test_suite_syn_records, 
	read_test_suite(AbsTSFileName),
	run_xle_on_sentences(AbsTSFileName),
	filter_xle_test_suite_fstrs(TSResultsFile),
	% defined in testreport.pl:
	ts_report(AbsTSPreviousResultsFile).



abolish_test_suite_syn_records :-
	safe_abolish(stce/3),
	safe_abolish(sentence/2),
	safe_abolish(fstructure/6),
	safe_abolish(ts_syn_result/4),
	safe_abolish(ts_syn_previous/4),
	safe_abolish(ts_syn_report/2).


%=============================
%  Invoke perl script to convert format of test suite file to prolog
%  Read in file, setting up  stce(N,Stce,FDescription) records

read_test_suite(FileName) :-
	safe_abolish(stce/3),
	testsuite_source_dir(Dir),
	concat_list([FileName, '.pl'],FileNamePL),
	% convert test suite to prolog readable form:
	concat_list(['perl ', Dir, '/TestSuiteToProlog.pl ',
		      FileName, ' ', FileNamePL],
	       Output),
	shell(Output),
	% read prolog version of test suite:
	% loads stce/3 records
	read_plg_ts_file(FileNamePL).
	%consult(FileNamePL).


read_plg_ts_file(File) :-
	retractall(stce(_,_,_)),
	open(File,read,Stream),
	repeat,
	   read_term(Stream,Term,[layout(LNs)]),
	   (
	       Term = (:- include_macros(FNames)) ->
	       expand_ts_macro_filenames(File,FNames,EFNames),
	       include_macros(EFNames)
	   ;
	       Term = stce(ID,Str,FDesc1) ->
	       LNs = [LN|_],
	       expand_macro_calls(FDesc1,FDesc,LN,File),
	       assert(stce(ID,Str,FDesc))
	   ;
	       Term == end_of_file -> true
	   ),
	   Term == end_of_file,
	   !,
	close(Stream).


expand_ts_macro_filenames(TSFile,FNames,EFNames) :-
	infer_ts_dir(TSFile,TSDir,_),
	(
	    TSDir = '.' ->
	    EFNames = FNames
	;
	    prefix_file_names(FNames,EFNames,TSDir)
	).

infer_ts_dir(FullFileName,Dir,FName) :-
	atom_chars(FullFileName,FileStr),
	reverse(FileStr,RFileStr),
	chop_to_first_item(RFileStr,0'/,RDirStr),
	(
	    RDirStr = [] -> Dir = '.'
	;
	    reverse(RDirStr,DirStr),
	    atom_chars(Dir1,DirStr),
	    absolute_file_name(Dir1,Dir2),
	    concat_list([Dir2,'/'],Dir)
	),
	append(RFNameStr,RDirStr,RFileStr),
	reverse(RFNameStr,FNameStr),
	atom_chars(FName,FNameStr).

chop_to_first_item([],_,[]).
chop_to_first_item([X|T],X,[X|T]) :- !.
chop_to_first_item([_|T],X,CT) :- chop_to_first_item(T,X,CT).

prefix_file_names([],[],_).
prefix_file_names([F|Fs],[PF|PFs],P) :-
	atom_chars(F,[First|_]),
	(
	    member(First,[0'/, 0'~]) ->
	    PF = F
	;
	    concat_list([P,F],PF)
	),
	!,
	prefix_file_names(Fs,PFs,P).

absolute_testsuite_file_name(FName,AbsFName) :-
	absolute_file_name(FName,AbsFNamePL),
	% strip off an ".pl" or ".ql" added to filename:
	atom_chars(AbsFNamePL,AbsFNamePLStr),
	reverse(AbsFNamePLStr,RAbsFNamePLStr),
	(
	    RAbsFNamePLStr = [0'l, 0'p, 0'.|RAbsFNameStr] ->
	    reverse(RAbsFNameStr, AbsFNameStr),
	    atom_chars(AbsFName,AbsFNameStr)
	;
	    RAbsFNamePLStr = [0'l, 0'q, 0'.|RAbsFNameStr] ->
	    reverse(RAbsFNameStr, AbsFNameStr),
	    atom_chars(AbsFName,AbsFNameStr)
	;
	    AbsFName = AbsFNamePL
	).

	
%=============================
%  Writes out sentences, in numerical order, to file sentences.out
%  Run XLE on sentences.out, printing f-str for each sentence to
%      file OutS<Num>.pl
%  Run perl script to combine all the OutS<Num>.pl files into
%      ParserResults.pl and delete the OutS<Num>.pl files
%  Read in ParserResults.pl to set up sentence(Num,FStructure) records

run_xle_on_sentences(TSFileName) :-
      safe_abolish(sentence/2),
      concat_list([TSFileName,'.sentences'],TSSentences),
      write_ts_sentences(TSSentences),
      format("~n Running XLE on test sentences...~n",[]),
      concat_list(['xle -e \"parse-testfile ',TSSentences,
		   ' -outputPrefix Out;exit\"'],
		  Cmd1),
      shell(Cmd1),
      testsuite_source_dir(Dir),
      concat_list(['perl ', Dir, '/CombineResults.pl Out ',TSSentences],
		  Cmd2),
      shell(Cmd2),
      % loads sentence/2 records
      consult('ParserResults.pl'),
      delete_file('ParserResults.pl',[ignore]).




write_ts_sentences(File) :-
	retractall(stce_num(_)),
	assert(stce_num(0)),
	open(File,write,Stream),
	repeat,
	    increment_stce_num(N),
	    get_stce_n(N,Stce),
	    (
		Stce == eos -> true
	    ;
		format(Stream,"~s~n~n",[Stce]), 
		fail
	    ),
	    Stce == eos,
	    !,
	close(Stream).


get_stce_n(N,Stce) :-
	stce(N,Stce,_), !.
get_stce_n(_N,eos).

increment_stce_num(N) :-
	stce_num(M),
	N is M+1,
	retractall(stce_num(_)),
	assert(stce_num(N)).

%=============================
%

filter_xle_test_suite_fstrs(TSResultsFile) :-
	safe_abolish(ts_syn_result/4),
	open(TSResultsFile,write,Stream),
	filter_xle_test_suite_fstrs1(Stream),
	close(Stream).

filter_xle_test_suite_fstrs1(Stream) :-
	stce(N,Stce,FDesc),
	matching_ts_parsed_sentence(N,Stce,PackedFStructure),
	matching_unpacked_fstrs(PackedFStructure,FDesc,Contexts),
	assert_ts_rec_syn(Contexts,PackedFStructure,Stce,FDesc,Stream),
	fail.
filter_xle_test_suite_fstrs1(_).


matching_ts_parsed_sentence(N,_Stce,PackedFStructure) :-
	sentence(N,PackedFStructure),
	!.
	% Could do further safety check that Stce strings are the same.

matching_unpacked_fstrs(no_parse,_FDesc,no_parse) :- !.
matching_unpacked_fstrs(PackedFS,FDesc,Contexts) :-
	compile_fdescription(FDesc,CFDesc,Locals),
	findall(Ctx,
	         (unpack_fstr(PackedFS,Ctx-FS),
		  assert_fstr(FS),
		  \+ \+ check_descriptions(CFDesc,0,Locals)),
		Contexts).




assert_ts_rec_syn(no_parse,_PackedFStructure,Stce,FDesc,Stream) :- !,
	assert(ts_syn_result(Stce,no_parse,no_parse,FDesc)),
	format(Stream,'ts_syn_previous("~s",no_parse,no_parse,~q).~n',
	       [Stce,FDesc]).
assert_ts_rec_syn(Contexts,PackedFStructure,Stce,FDesc,Stream) :-
	assert(ts_syn_result(Stce,Contexts,PackedFStructure,FDesc)),
	format(Stream,'ts_syn_previous("~s",~q,~q,~q).~n',
	       [Stce,Contexts,PackedFStructure,FDesc]).





%=============================
%

%====================================================
% Creating, sorting and matching files names
% Test suite results file naming convention: <TSFileName><date>

name_syn_ts_results_file(TSFile,TSResultsFile) :-
	datime(datime(Yr,Mo1,Da1,Hr1,Mn1,Sc1)),
	pad_digits(Mo1,Mo),
	pad_digits(Da1,Da),
	pad_digits(Hr1,Hr),
	pad_digits(Mn1,Mn),
	pad_digits(Sc1,Sc),
	concat_list([TSFile,Yr,Mo,Da,Hr,Mn,Sc],'_',TSResultsFile).


pad_digits(X,X) :- X > 9, !.
pad_digits(0,'00').
pad_digits(1,'01').
pad_digits(2,'02').
pad_digits(3,'03').
pad_digits(4,'04').
pad_digits(5,'05').
pad_digits(6,'06').
pad_digits(7,'07').
pad_digits(8,'08').
pad_digits(9,'09').

%==========================

purge_old_results(TestFileName) :-
	infer_ts_dir(TestFileName,Dir,FName),
	directory_files(Dir,DirFiles),
	match_by_prefix_and_sort(DirFiles,FName,[_|Delete]),
	delete_files_from_dir(Dir,Delete).

delete_files_from_dir('.',Delete) :- !,
	delete_files(Delete).
delete_files_from_dir(Dir,Delete) :-
	working_directory(Current,Dir),
	delete_files(Delete),
	working_directory(_,Current).

delete_files([]).
delete_files([F|Fs]) :-
	delete_file(F,[ignore]),
	!,
	delete_files(Fs).


%==================================================
% Find name of default comparison results file:
%  Most recently produced (as indicated by date in file name)
%  file with test suite file name (TSF) as prefix

get_comparison_syn_ts_file_name(TSF,TSPrevF) :-
	infer_ts_dir(TSF,TSDir,FName),
	directory_files(TSDir,DirFiles),
	(
	    match_by_prefix_and_sort(DirFiles,FName,[TSPrev|_]) ->
	    (
		TSDir = '.' ->
		TSPrevF = TSPrev
	    ;
		concat_list([TSDir,TSPrev],TSPrevF)
	    )
	;
	    TSPrevF = no_comparison_file
	).

match_by_prefix_and_sort(DirFiles,Prefix,MatchingSortedFiles) :-
	atom_chars(Prefix,SPrefix),
	match_atoms_by_prefix_and_key(DirFiles,SPrefix,[],KMatch),
	keysort(KMatch,KMatch1),
	reverse(KMatch1,KMatch2),
	unkey(KMatch2,MatchingSortedFiles).

match_atoms_by_prefix_and_key([],_Prefix,KMatch,KMatch).
match_atoms_by_prefix_and_key([A|As],Prefix,KMatch0,KMatch2) :-
	atom_chars(A,AChars),
	(
	    append(Prefix,[0'_|KeyChars],AChars) ->
	    atom_chars(Key,KeyChars),
	    KMatch1 = [Key-A|KMatch0]
	;
	    KMatch1 = KMatch0
	),
	match_atoms_by_prefix_and_key(As,Prefix,KMatch1,KMatch2).

unkey([],[]).
unkey([_Key-Item|KItems],[Item|Items]) :-
	unkey(KItems,Items).


safe_abolish(Pred/Arity) :-
	current_predicate(Pred,Head),
	functor(Head,Pred,Arity),
	!,
	abolish(Pred/Arity).
safe_abolish(_).


