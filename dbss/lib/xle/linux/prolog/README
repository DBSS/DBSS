README File for XLE Test Suite Facilities
=========================================


This documentation describes a set of prolog and perl programs that
can be used in conjunction with the XLE's parse-testfile command to

  (a) parse collections of sentences to determine whether they produce
      f-structures matching specified descriptions

  (b) summarize the results, compare them with the results from
      previous runs of the test suite, and display individual c- and
      f-structures to allow more detailed comparison of individual cases.

The tools are not callable from an XLE process.  Rather, a prolog
process needs to be set up which both calls the XLE and runs the test
suite commands.


Full html documentation is in the file testsuitedoc.html.  This
document just describes installation

1. Installation
===============

The distribution directory contains the following files

  a) Prolog source files
  ----------------------
    main.pl		% Directives to load prolog sources
    utl.pl		% Various utilities
    runtestsuite.pl	% Procedures to run test suite
    testreport.pl	% Procedures to report in test suite results
    xlesub.pl		% Code to call XLE as subroutine from prolog
    fdescriptions.pl	% To match f-structures against descriptions
    fdescmacros.pl      % To compile and expand macros            
    tsunpack.pl		% To unpack packed f-structures
    create_testfile.pl  % Create a test suite from handpicked f-structures
    format_convert.pl   % Turn f-strs into f-descriptions
      

  b) Perl source files
  --------------------
     TestSuiteToProlog.pl	% Convert test suite file to prolog syntax
     CombineResults.pl		% Merge XLE output files
     QuoteProlog.pl		% Quote various atoms to make them
				%       prolog readable

  c) Documentation
  ----------------
     README			% This file

  d) Example test suites
  ----------------------
     demo/demosuite		% An example test suite 
     demo/demomacros		% And the macros it assumes
     demo/verbsuite		% Another test suite
     demo/config_test           % Configuration file for extracting
                                %   f-descriptions from f-structures


The system runs under Sicstus prolog (3.7.1).  Unfortunately it makes
use of the  boolean constraint satisfaction clpb library for the
unpacking of f-structures, and I don't know how portable this is to
other prologs.  Otherwise, neither the prolog nor the perl makes use
of anything fancy.


Probably the most convenient way of installing the test suite code is
to create a saved prolog image.  This can be done as follows.

To create a saved image of the test suite code
----------------------------------------------

 a) Invoke prolog while in the distribution directory.
 b) At the prolog prompt (| ?-), load the source code by consulting main.pl

        | ?- [main].

 c) After all the messages have printed out, type

        | ?- save_system.

    This will create an executable file, run_test, in the distribution
    directory. 

 d) To exit prolog, type

        | ?- halt.


You can now copy, soft link, or move the run_test file to wherever you
like, or leave it in the distribution directory.  It is, however,
important to leave the three perl scripts in the distribution
directory. This is because the saved image will always look for the
scripts in that directory.



2. Running a Test Suite
=======================
There are two ways of firing up a (prolog) test suite process,
depending on whether or not you created a saved prolog image during
installation.

2.1 Starting from the saved run_test image
------------------------------------------
Assuming that you did create a saved image during installation
(and copied or moved it to, say, the directory ~/bin), simply invoke
the run_test command.
You might do this in one of three ways

 *    If you copied or soft-linked the saved image to your current working
      directory, just type
            run_test


 *   If you set up your PATH environment variable to search through
     the directory containing the saved image, just type
            run_test

 *   If you have done neither of the above, figure out the path name
     that will get you to the saved state (e.g. /xle/testsuite), and type 
           /xle/testsuite/run_test

Note that it may be necessary to prefix the command with "sh" if the
image does not have an executable permission.


If you do this successfully, you will get the following set of messages:


   SICStus 3.7.1 (SunOS-5.5.1-sparc): Tue Oct 06 13:38:15 MET DST 1998
   Licensed to parc.xerox.com
   {SICStus state restored from /tilde/crouch/gt/run_test}


   XLE Test Suite Facility 5/4/2001
   Copyright (c) 2001 by the Xerox Corporation. All rights reserved.
   This software is made available AS IS, and Xerox Corporation makes no
   warranty about the software, its performance or its conformity to any
   specification.

                     XLE Test Suite Commands

   run_test(<File>).
         * Run test suite in &lt; File &gt;
   ts_report.
         * Report on results of test suite
   ts_report(<PreviousFile>).
         * Report on results of test suite
           and compare with previous results stored in &lt;PreviousFile&gt

    ..... etc ....

   ts_help.
         * Print this message
   ts_exit.
         * Exit test suite facility

   pwd.
         * Prints out current working directory
   cd('<Dir>').
         * Changes current working directory to &lt;Dir&gt
           (single quotes necessary if Dir contains / symbols)

   % Prolog note: periods terminating commands are required
   %              as are double quotes around strings

   | ?-

This leaves you at a prolog prompt, where you can enter a command to
run a specified test suite (see below).


2.2. Starting from Source
-------------------------
If you did not create a saved image, or for some other reason want to
run directly from the prolog source code, do the following

 *    Ensure that you are in the directory containing the source code
      (including the required perl scripts)

 *    Start prolog, and at the prompt (| ?-) enter

         | ?- [main].

     This will load the source code (and also check that the necessary
     perl scripts are present in the directory), and print out the
     same menu of options as running from a saved state.

This again leaves you at a prolog prompt, where you can enter a command to
run a specified test suite.

2.3. Processing a Specified Test Suite
--------------------------------------
Having successfully started a test suite process, you will be sitting
at a prolog prompt. At the prolog prompt, you can run a test suite 
by entering the command

       | ?- run_test(<suite>).

where <suite> is the name of the file containing the test
suite items.  This will read in the test suite file, invoke the
XLE (with whatever grammar is specified in your .xlerc file),
parse the sentences, match f-structures against the specified descriptions, 
and give a preliminary report on the results.  

If the test suite file is not in the directory in which the test suite
process is running, type the full file name of the directory, e.g.

       | ?- run_test('~/gram/testsuites/ts1').

