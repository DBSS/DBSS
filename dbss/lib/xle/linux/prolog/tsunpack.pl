:- use_module(library(lists)).
:- use_module(library(clpb)).


% reads in a packed prolog f-str file (InputFile), and
% returns a list of [TrueVars]-fstructure/6
% as Unpacked


unpack(InputFile,Unpacked) :-
	consult(InputFile),
	fstructure(Sent, Props, ChoiceDefs, VarDefs, FS_Facts, CS_Facts),
	define_choices(ChoiceDefs, Terms),
	choices_to_symbols(ChoiceDefs, Symbols),
	define_equivalence(VarDefs, _VarTerms),
	% vars_to_symbols(VarDefs, _VarSymbols, Terms, Symbols),
	enumerate_unpacked_solutions(Sent, Props, FS_Facts, CS_Facts,
				     Terms, Symbols, Unpacked).


unpack_fstr(PackedFStr,UnpackedFStr) :-
	PackedFStr =
	    fstructure(Sent, Props, ChoiceDefs, VarDefs, FS_Facts, CS_Facts),
	define_choices(ChoiceDefs, Terms),
	choices_to_symbols(ChoiceDefs, Symbols),
	define_equivalence(VarDefs, _VarTerms),
	unpacked_solution(Sent, Props, FS_Facts, CS_Facts, Terms,
			  Symbols,UnpackedFStr). 
	

	    
%------------------------------------------------------------------


define_choices([], []).
define_choices([choice(PropVars, Cond)|Choices], Terms) :-
	convert_formula(Cond, CondVar),
	construct_or_clause(PropVars, OrClause),
	sat(OrClause =:= CondVar),
	apply_exclusivity_cond(PropVars),
	define_choices(Choices, MoreTerms), 
	append(PropVars, MoreTerms, Terms).

% -----------------------------------------------------------------------------

define_equivalence([], []).
define_equivalence([define(Var, Cond)|Equivs], [Var|Vars]) :-
	convert_formula(Cond, CondExpression),
	sat(Var =:= CondExpression),
	define_equivalence(Equivs, Vars).
define_equivalence([select(Var, Cond)|Equivs], [Var|Vars]) :-
	packed_fs_selection_flag(1),
	convert_formula(Cond, CondExpression),
	sat(Var =:= CondExpression),
	define_equivalence(Equivs, Vars).
define_equivalence([select(_Var, _Cond)|Equivs], Vars) :-
	\+ packed_fs_selection_flag(1) ->
	define_equivalence(Equivs, Vars).
% define_equivalence([Equiv|Equivs], [Var|Vars]) :-
% 	(Equiv = define(Var, Cond) ;
% 	 Equiv = select(Var, Cond)),
% 	convert_formula(Cond, CondExpression),
% 	sat(Var =:= CondExpression),
% 	define_equivalence(Equivs, Vars).

% -----------------------------------------------------------------------------

construct_or_clause([], 0).
construct_or_clause([PV|PropVars], T + Terms) :-
	convert_formula(PV, T),
	construct_or_clause(PropVars, Terms).

% -----------------------------------------------------------------------------

construct_and_clause([], 1).
construct_and_clause([PV|PropVars], T * Terms) :-
	convert_formula(PV, T),
	construct_and_clause(PropVars, Terms).

% -----------------------------------------------------------------------------

apply_exclusivity_cond(PropVars) :-
	pairwise_negation(PropVars).

pairwise_negation([]).
pairwise_negation([I|Items]) :-
    pairwise_negation2(I, Items),
    pairwise_negation(Items).

pairwise_negation2(_, []).
pairwise_negation2(I1, [I2|Items]) :-
	sat(~(I1 * I2)),
	pairwise_negation2(I1, Items).


% -----------------------------------------------------------------------------

convert_formula(CondVar, CondVar) :-
	var(CondVar), !.

convert_formula(not(Cond), ~(CondVar)) :-
	convert_formula(Cond, CondVar).

convert_formula(AndCond, AndCondVar) :-
	AndCond =.. [and|AndTerms], !,
	construct_and_clause(AndTerms, AndCondVar).

convert_formula(OrCond, OrCondVar) :-
	OrCond =.. [or|OrTerms], !,
	construct_or_clause(OrTerms, OrCondVar).
convert_formula(1, 1).
convert_formula(0, 0).



%---------------------------------------------------------------------

unpacked_solution(Sent, Props, FS_Facts, CS_Facts, Terms, Symbols, Solution) :-
	labeling(Terms),
	list_true_variables(Terms,Symbols,TVs),
	list_implied_facts(FS_Facts, Terms, Symbols, IFS_Facts),
	list_implied_facts(CS_Facts, Terms, Symbols, ICS_Facts),
	fix_unpacked_node_equalities(IFS_Facts,IFS_Facts,ICS_Facts,FS,CS),
	Solution =
	  TVs-fstructure(Sent,Props,[],[],FS,CS).

enumerate_unpacked_solutions(Sent, Props, FS_Facts, CS_Facts, Terms,
			     Symbols, Solutions) :-
	findall(Solution,
		unpacked_solution(Sent, Props, FS_Facts, CS_Facts,
				  Terms, Symbols, Solution),
		Solutions).

fix_unpacked_node_equalities([],FS,CS,FS,CS).
fix_unpacked_node_equalities([cf(1,eq(var(N),var(M)))|FSs],
			     FS0,CS0,FS2,CS2) :- !,
	(
	    N < M ->
	    subst(var(M),var(N),FS0,FS1),
	    subst(var(M),var(N),CS0,CS1)
	;
	    subst(var(N),var(M),FS0,FS1),
	    subst(var(N),var(M),CS0,CS1)
	),
	fix_unpacked_node_equalities(FSs,FS1,CS1,FS2,CS2).
fix_unpacked_node_equalities([_|FSs],FS0,CS0,FS2,CS2) :- !,
	fix_unpacked_node_equalities(FSs,FS0,CS0,FS2,CS2).
    

list_implied_facts([], _, _, []).
list_implied_facts([Fact|Facts], T, S, IFs) :-
	Fact = cf(C, F),
	convert_formula(C, C2),
	if(\+ taut(C2, 0),
	   IFs = [cf(1,F)|IFs1],
	   IFs = IFs1),
	list_implied_facts(Facts, T, S, IFs1).


list_true_variables([], [], []).
list_true_variables([1|Labeling], [TrueVar|Vars], [TrueVar|TVs]) :-
	list_true_variables(Labeling, Vars, TVs).
list_true_variables([0|Labeling], [_FalseVar|Vars], TVs) :-
	list_true_variables(Labeling, Vars, TVs).


% -----------------------------------------------------------------------------

choices_to_symbols(Choices, Symbols) :-
	choices_to_symbols(Choices, Symbols, 1).

choices_to_symbols([], [], _).
choices_to_symbols([choice(Terms, _)|Choices], Symbols, ID) :-
	choice_label(ID, Label),
	choice_symbols(Label, Terms, Symbols1),
	% instantiate choice variables:
	%Terms = Symbols1,   
	ID1 is ID + 1,
	append(Symbols1, Symbols2, Symbols), !,
	choices_to_symbols(Choices, Symbols2, ID1).


instantiate_choices(Choices) :-
	instantiate_choices_to_symbols(Choices, 1).

instantiate_choices_to_symbols([], _).
instantiate_choices_to_symbols([choice(Terms, _)|Choices],  ID) :-
	choice_label(ID, Label),
	choice_symbols(Label, Terms, Symbols),
	% instantiate choice variables:
	Terms = Symbols,   
	ID1 is ID + 1, !,
	instantiate_choices_to_symbols(Choices, ID1).

% -----------------------------------------------------------------------------

choice_symbols(Prefix, ListOfTerms, ListOfSymbols) :-
	length(ListOfTerms, N),
	choice_symbols(Prefix, 0, N, ListOfSymbols).

choice_symbols(Prefix, I, N, [Sym|Symbols]) :-
	I < N,
	I_plus_1 is I + 1,
	name(I_plus_1, NumChars),
	append(Prefix, NumChars, SymChars),
	name(Sym, SymChars),
	choice_symbols(Prefix, I_plus_1, N, Symbols).
choice_symbols(_, N, N, []).

% -----------------------------------------------------------------------------

choice_label(0, []) :- !.
choice_label(ID, Label) :-
	R is ((ID-1) mod 26) + 65,
	Q is ((ID-1) // 26),
	choice_label(Q, Label1),
	append(Label1, [R], Label).

% -----------------------------------------------------------------------------
