
% Converts XLE output from old Prolog format to new Prolog format.

% 2 main predicates:

% convert_file(FileIn,FileOut): structure in file FileIn is converted
% to new format and written to file FileOut.

% convert_constraints(FStructureIn,FStructureOut): the f-structure
% FStructureIn (old format) is converted to FStructureOut.  Both
% arguments are of the form:
% fstructure(String,Props,Choices,Eqs,Constraints,Cstructure) 



% Declare the input to be dynamic so we can clear the database after
% each conversion.
:- dynamic fstructure/6.
% Declare ":" as a new operator.
%:- op(500,xfx,:).

% Read in FileIn, convert it, and write the result to Fileout.
convert_file(Filein,Fileout) :-
   % Clear the database.
   abolish(fstructure,6),
   % Read the input.
   load_files(Filein),
   % Look at the input we just read in and convert it.
   fstructure(String,Props,Choices,Eqs,Constraints,Cstructure),
   convert_format(fstructure(String,Props,Choices,Eqs,Constraints,Cstructure),
                  NewFormat),
   % Open the output file, write the output to it, and close it.
   open(Fileout,write,Filestream),
   prettyprint(Filestream,NewFormat),
   close(Filestream).

% Convert the old format to the new format.  The only thing that
% changes is the constraints.
convert_format(
         fstructure(String,Props,Choices,Eqs,OldConstraints,Cstructure),
         fstructure(String,Props,Choices,Eqs,NewConstraints,Cstructure)) :-
   convert_constraints(OldConstraints,NewConstraints).

% Convert each constraint, stripping off the outer cf(_,_) predicate.
convert_constraints([],[]).
convert_constraints([cf(_,C)|Rest],[NewC|NewRest]) :-
   convert_constraint(C,NewC),
   convert_constraints(Rest,NewRest).

% Constraint equivalences.
convert_constraint(var(0),^) :- !.
convert_constraint(var(ID),v:ID).
convert_constraint(eq(C1,C2),eq:[NewC1,NewC2]):-
   convert_constraint(C1,NewC1),
   convert_constraint(C2,NewC2).
convert_constraint(scopes(C1,C2),'<s':[NewC1,NewC2]):-
   convert_constraint(C1,NewC1),
   convert_constraint(C2,NewC2).
convert_constraint(attr(C1,C2),p:[NewC1,NewC2]):-
   convert_constraint(C1,NewC1),
   convert_constraint(C2,NewC2).
convert_constraint(in_set(C1,C2),eq:[p:[NewC2,'$'],NewC1]):-
   convert_constraint(C1,NewC1),
   convert_constraint(C2,NewC2).
convert_constraint(proj(C1,C2),ae:[NewC1,NewC2]):-
   convert_constraint(C1,NewC1),
   convert_constraint(C2,NewC2).
convert_constraint(semform(S,N,C1,C2),sf(S,N,NewC1,NewC2)) :-
   convert_constraint(C1,NewC1),
   convert_constraint(C2,NewC2).  
convert_constraint([C1|Rest],[NewC1|NewRest]) :-
   convert_constraint(C1,NewC1),
   convert_constraint(Rest,NewRest).      
convert_constraint(C,C) :-
   atom(C).

  

% Prettyprint the f-structure output.
prettyprint(Filestream,
      fstructure(String,Props,Choices,Eqs,Constraints,Cstructure)) :-
   format(Filestream, 'fstructure(~s~a~s,~n', ["'",String,"'"]),
   format(Filestream, "~+~s~n~+~s", ["% Properties:","["]),
   format_list(Filestream, Props),
   format(Filestream, ",~n~+~s~n~+~s", ["% Choices:","["]),
   format_list(Filestream, Choices),
   format(Filestream, ",~n~+~s~n~+~s", ["% Equivalences:","["]),
   format_list(Filestream, Eqs),
   format(Filestream, ",~n~+~s~n~+~s", ["% Constraints:","["]),
   format_list(Filestream, Constraints),
   format(Filestream, ",~n~+~s~n~+~s", ["% C-Structure:","["]),
   format_list(Filestream, Cstructure),
   format(Filestream, "~s",[")."]).
% If we get unexpected input just write it out.
prettyprint(Filestream,Any) :-
   portray_clause(Filestream,Any).

format_list(Filestream,[]) :-
   format(Filestream, "~+~s",["]"]).
format_list(Filestream,[Last]) :-
   format(Filestream, "~+~q~s", [Last,"]"]).
format_list(Filestream,[First,Next|Rest]) :-
   format(Filestream, "~+~q,~n", First),
   format_list(Filestream,[Next|Rest]).
