#########################################################################
## Test the grammar 
##
## for the moment, just use standard test
proc panda-test { args } {

    set testsuite [lindex $args 0]

    parse-testfile $testsuite

    if {[file exists ${testsuite}.new]} {
	exec rm $testsuite
	exec mv ${testsuite}.new $testsuite
    }
}
puts stdout "defined new procedure `panda-test'"



################################################
# add button in f-structure window:
# 'transfer and print': transfers an f-structure and prints prolog file

add-item-to-xle-menu \
	{command -label "Transfer and Print" \
             -command "transfer-and-print * $self"\
	     -accelerator "<t>" \
             -doc "Transfer this FStructure and print prolog file"} \
	fsCommands

# add some buttons in fschart window:
# 'read next file': reads next prolog file from a directory
# 'exit loop': exits read-next-file loop
# new accelerator 's' for 'print prolog file'

set fsChartCommands {
    {command -label "Print Postscript" \
         -command "print-fschart N $self.value" \
         -doc "Writes postscript file on fschartN.ps."}
    {command -label "Print Prolog File" \
         -command "print-prolog-chart-graph * \"\" 1" \
         -accelerator "<s>" \
         -doc "Writes a packed prolog representation on fschartN.pl."}
    {command -label "Read Next File" \
         -command "set next 1"\
	 -accelerator "<n>" \
         -doc "Reads next prolog file from a directory"}
    {command -label "Exit Loop" \
         -command "set next 0"\
	 -accelerator "<q>" \
         -doc "Exits next file processing loop"}}


################################################
# transfers an fstructure
# and prints transfer output as prolog file in $transfer_out 
# (by Martin Emele)

# default: save transfer output in directory /tmp/transfer_out/
set transfer_out "/tmp/transfer_out"

proc transfer-and-print {file {window .fstructure}} {
#    global windowProps currentsentence xfer_serv_output transfer_out
# currentsentence: seems to be deleted in new xle release 
# (distrib-000517/)
    global windowProps sentence xfer_serv_output transfer_out
    if {![info exists windowProps($window,data)]} {
	puts "Please display something to print."
	return
    }
    if {[file exists $transfer_out]} {
#	puts stderr "Warning: directory \"$transfer_out\" already exists."
    } else {
    file mkdir $transfer_out
    }
    transfer-this-solution 1
    set fstructure $windowProps($window,data)
    if {$file == "*"} {
	set filename [default-filename "" "pl" $fstructure]
    } else {
	set filename $file
    }
#    set filename [print_graph_as_prolog $fstructure $filename $currentsentence]
    file copy -force $xfer_serv_output $transfer_out/$filename

    if {$file == "*"} {
	puts "fstructure printed on $transfer_out/$filename"
    }
}

#########################################################################
## Set parameters
##
set timeout 100
puts "timeout set to $timeout seconds"

set max_xle_scratch_storage 100
puts "maximum scratch storage set to $max_xle_scratch_storage MB"


#########################################################################
## Frequently occurring mistakes: copy&paste wrong commands
##
proc parsing {sentence {parser ""}} {
#    puts stdout "You should have typed \"parse\"...\n"
    parse $sentence $parser
}

proc analyzing {sentence {elements ""}} {
#    puts stdout "\nYou should have typed \"analyze-string\"...\n"
    analyze-string $sentence $elements
}

#########################################################################
## Shortcuts for create-parser/parse/parse-testfile/analyze-string
##
proc p {sentence {parser ""}} {
    parse $sentence $parser
}

proc pt  {sentenceFile args} {
    parse-testfile $sentenceFile $args
}

proc a {sentence {elements ""}} {
    analyze-string $sentence $elements
}

proc c {grammar} {
    create-parser $grammar
}

proc cg {grammar} {
    create-generator $grammar
}

proc r {chgrph} {
    read-prolog-chart-graph $chgrph
}

proc pfs {file} {
    print-fs-as-prolog $file
}
