#!/usr/bin/perl

# Glue Test Suite
# File: TestSuiteToProlog.pl
# Author: Dick Crouch
# 
# This program takes a test suite file and produces a prolog readable
# of it.
#
#   perl TestSuiteToProlog.pl inputfile plg_outputfile
#
#
# The input test suite file has the format
#
#    
#    This is a sentence to be parsed, and occupies just one line.
#  
#    fd
#    This is an (optional) description of the f-structure(s) desired,
#    which may occupy more than one line
#    efd
#
# Any number of blank lines or comment lines (denoted by %, # or ; at the
# beginning of the line, or with only whitespace preceding) are allowed.  
# The first line not preceded by a comment is taken to be the first sentence.
#
# Want to try and preserve line numbering in prolog output file, since we
# are going to rely on prolog to read in the file and check for syntax errors,
# especially in the f-descriptions.
# 
# Basically, we number the sentences, wrap a prolog functor stce/3
# around the sentence number, the quoted sentence string, and the 
# f-descrioption.


open (INFILE,  $ARGV[0]);
open (OUTFILE, ">$ARGV[1]");  # File to write prolog terms

$sentnum=0;                   # Sentence counter



   #=====================================================
   # Print out any initial comment lines before first sentence,
   # (or before line including macro definition files)
   # preceded by prolog comment character 
   #=====================================================
$line = <INFILE>; 
while ($line ne "" && ($line =~ /^[\t ]*[%\#\;]/ || $line =~/^[\t ]*[\n]/)) {
    print OUTFILE ("%$line");
    $line = <INFILE>;
}

   #=====================================================
   # Print out macro inclusion directive, if any
   #=====================================================

if ($line =~/^[\t ]*include macros/) {
    chop($line);
    @macros = split(/[\t ]+/, $line);
    print OUTFILE (":- include_macros([");
    $index = 3;
    while ($index <= @macros) {
	print OUTFILE $macros[$index-1];
	if ($index < @macros) {
	    print OUTFILE (", ");
	};
	$index++;
    };
    print OUTFILE ("]).\n");
    $line = <INFILE>; 
}
    
   #=====================================================
   # Print out any remaining comment lines before first sentence,
   # preceded by prolog comment character 
   #=====================================================
while ($line ne "" && ($line =~ /^[\t ]*[%\#\;]/ || $line =~/^[\t ]*[\n]/)) {
    print OUTFILE ("%$line");
    $line = <INFILE>;
}

    #==============================================================
    # Loop to print each sentence and f-description
    # Increments sentence counter; note f-descriptions are optional
    #==============================================================


while ($line ne "") {
    $sentnum++;

    #=======================================================
    # Print (quoted) sentence
    #    Invariant: we know that $line is on sentence at this point
    # =======================================================
    chop($line);
    print OUTFILE ("stce($sentnum, \"$line\",\n");
    $line = <INFILE>; 
    
    #=======================================================
    # read to f-description or next sentence
    #=======================================================
    while ($line =~ /^[\t ]*[%\#\;]/ || $line =~/^[\t ]*[\n]/) {
        # delete comments
	print OUTFILE ("\n");
	$line = <INFILE>;
    }

    
    if ($line =~ /^fd/) {
	#=======================================================
	# Print explicit f-description given
	#=======================================================
	print OUTFILE ("%$line");
	$line = <INFILE>;
	while ($line ne "" && $line !~ /^efd/) {
	    if ($line =~ /^[\t ]*[%\#\;]/) {
		print OUTFILE ("%$line");
	    }
	    else {
		# wrap quotes around stuff:
		    #1 upper case attribute names
		$line =~ s/([\[\,\( ])([A-Z][\w\-\_]*)([\]\)\, ])/$1'$2'$3/g;
		    #2 local names beginning with %
		$line =~ s/([\[\,\( ])(%[\w\-\_]*)([\]\,\)])/$1'$2'$3/g;
		    #3 the +_ value
		$line =~ s/([\[\,\( ])\+\_([\]\,\) ])/$1'+_'$2/g;
		    #4 the $ value
		$line =~ s/([\[\,\( ])\$([\]\,\) ])/$1'\$'$2/g;
		print OUTFILE ($line);
	    }
	    $line = <INFILE>;
	}
	print OUTFILE (").");
	$line = <INFILE>;
    }
    else {
	#=======================================================
	# print default f-description 
	#=======================================================
	print OUTFILE ("true).  ");
    }

    #=======================================================
    # Read until next sentence, omitting any comment lines
    # Note: if there was no explicit f-description for the previous
    # sentence, then we are already looking at the next sentence
    #=======================================================

    while ($line =~ /^[\t ]*[%\#\;]/ || $line =~/^[\t ]*[\n]/) {
	print OUTFILE ("\n");
	$line = <INFILE>;
    }
}
