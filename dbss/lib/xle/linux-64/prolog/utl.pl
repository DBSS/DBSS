%% Various prolog utilities, inclduing
%%    pretty printer pp
%%       pp_debug   --  use pp for prolog debugging
%%       setsys(ppdepth,25) --- set depth of printing
%%       setsys(pagewidth,100) --- increase line length


%-------------------------------------------------------
%
%  General utility procedures
%
%-------------------------------------------------------

% changing directories etc:
cd(Dir) :- working_directory(_,Dir).
pwd :- working_directory(D,D), format("~n~q~n",[D]).

% Concatenate lists of atoms

concat_list(ListAtomsOrNums, ConcatAtom) :-
	concat_list(ListAtomsOrNums, '', ConcatAtom).

concat_list(ListAtomsOrNums, SeparatorAtom, ConcatAtom) :-
	atom_chars(SeparatorAtom,Sep),
	concat_list1(ListAtomsOrNums, Sep, ConcatString),
	atom_chars(ConcatAtom,ConcatString).


concat_list1([],_,[]).
concat_list1([H],_Sep,HStr) :- !,
	(
	    atom(H) -> atom_chars(H,HStr)
	;
	    integer(H) -> number_chars(H,HStr)
	).
concat_list1([H|T],Sep,String) :-
	(
	    atom(H) -> atom_chars(H,HStr)
	;
	    integer(H) -> number_chars(H,HStr)
	),
	append(Sep,TString,TString1),
	append(HStr,TString1,String),
	concat_list1(T,Sep,TString).


strict_member(X,[XX|_]) :- X == XX.
strict_member(X,[_|T]) :- strict_member(X,T).

%-------------------------------------
% Variable tail list stuff
%-------------------------------------

vartail(T,T) :- var(T), !.
vartail([_|T],T) :- var(T), !.
vartail([_|T],T1) :- vartail(T,T1).

vt_append(L1,L2) :-
	vartail(L1,T1),
	T1 = L2.


vt_member(_I,variable_standing_for_empty_list) :- !, fail.
vt_member(I,[I|_]).
vt_member(I,[_|T]) :- vt_member(I,T).

list_to_vtlist([],_V) :- !.
list_to_vtlist([H|T],[H|VT]) :- append(T,_V,VT).

null_vtlist(L) :- var(L), !.




try_all(Prog) :- 
     ((call(Prog),fail); true).  


try(Prog) :-
     (call(Prog); true), !.

%% defined in lists library
%is_list(L) :-
%    nonvar(L),
%    ( L = [] -> true
%    ; L = [_|L0] -> is_list(L0) ).

%% defined in lists library
%same_length(List1,List2) :-
%    length(List1,L),
%    length(List2,L).


two_level_copy(A1,A2) :-
     functor(A1,Functor,Arity),
     functor(A2,Functor,Arity),
     two_level_arg(Arity,A1,A2).


two_level_arg(0,_,_) :- !.
two_level_arg(N,A1,A2) :-
     arg(N,A1,A1Arg),
     functor(A1Arg,Functor,Arity),
     functor(A2Arg,Functor,Arity),
     arg(N,A2,A2Arg),
     M is N-1,
     two_level_arg(M,A1,A2).


renamed_copy(Term,Copy,Bindings) :-
     var(Term), !,
     assoc_var(Term,Copy,Bindings).
renamed_copy(Term,Copy,Bindings) :-
     functor(Term,Functor,Arity),
     functor(Copy,Functor,Arity),
     renamed_copy_arg(Arity,Term,Copy,Bindings).


renamed_copy_arg(0,_,_,_) :- !.
renamed_copy_arg(N,Term,Copy,Bindings) :-
     arg(N,Term,TermArg),
     renamed_copy(TermArg,CopyArg,Bindings),
     arg(N,Copy,CopyArg),
     M is N-1, !,
     renamed_copy_arg(M,Term,Copy,Bindings).


assoc_var(V1,V2,Bindings) :-
     var(Bindings), !,
     Bindings = [(V1,V2)|_].
assoc_var(V1,V2,[(V3,V2)|_Bindings]) :-
     V1 == V3, !.
assoc_var(V1,V2,[_|Bindings]) :- !,
     assoc_var(V1,V2,Bindings).




%%   subst(Old,New,OldStruc,NewStruct)
%% Substitues Old for New in OldStruc to get NewStruct

subst(Lhs, Rhs, Old, Rhs) :-
     Old == Lhs, !.
subst(_Lhs, _Rhs, Old, Old) :-
     var(Old), !.
subst(Lhs, Rhs, Old, New) :-
     functor(Old, Functor, Arity),
     functor(New, Functor, Arity),
     subst_arg(Arity, Lhs, Rhs, Old, New).


subst_arg(0,_,_,_,_) :- !.
subst_arg(N, Lhs, Rhs, Old, New) :-
     arg(N, Old, OldArg),
     subst(Lhs, Rhs, OldArg, NewArg),
     arg(N, New, NewArg),
     M is N-1, !,
     subst_arg(M, Lhs, Rhs, Old, New).


subst_several(Substs,Old,New) :-
	member(OOld-New,Substs),
	OOld == Old, !.
subst_several(_Substs,Old,Old) :-
	var(Old),
	!.
subst_several(Substs,Old,New) :-
	functor(Old,F,N),
	functor(New,F,N),
	subst_several_arg(N,Substs,Old,New).
subst_several_arg(0,_,_,_) :- !.
subst_several_arg(N,Substs,Old,New) :-
	M is N-1,
	arg(N,Old,OldArg),
	arg(N,New,NewArg),
	subst_several(Substs,OldArg,NewArg),
	!,
	subst_several_arg(M,Substs,Old,New).


reverse_concatenate([X|L1],L2,L3) :-
     reverse_concatenate(L1,[X|L2],L3).
reverse_concatenate([],L,L).


atom_concat(A1,A2,A3) :-
	atom_chars(A1,C1),
	atom_chars(A2,C2),
	append(C1,C2,C3), 
	atom_chars(A3,C3).


segment_from_to(List,From,To,Segment) :-
     tail_of_length(List,From,Tail),
     reverse(Tail,Rev),
     Length is From-To,
     tail_of_length(Rev,Length,RevSegment),
     reverse(RevSegment,Segment).


tail_of_length(List,Length,Tail) :-
     length(List,L),
     L >= Length, !,
     N is L-Length,
     remove_n_heads(List,N,Tail).
tail_of_length(List,_Length,List).


remove_n_heads(List,0,List) :- !.
remove_n_heads([_H|T],N,Tail) :-
     M is N-1,
     remove_n_heads(T,M,Tail).


% atomic generated symbols:
% restart by abolish(generated_symbol_suffix,1).

generated_symbol(Prefix,Symbol) :-
     new_gensym_suffix(Suffix),
     atom_chars(Prefix,PrefixChars),
     number_chars(Suffix,SuffixChars),
     append(PrefixChars,SuffixChars,SymbolChars),
     atom_chars(Symbol,SymbolChars).


new_gensym_suffix(Suffix) :-
     current_predicate(generated_symbol_suffix,_), !,
     retract(generated_symbol_suffix(Suffix)),
     NextSuffix is Suffix+1,
     assert(generated_symbol_suffix(NextSuffix)).
new_gensym_suffix(1) :-
     assert(generated_symbol_suffix(2)).
     


% non-atomic generated symbols:

symbol_suffix(Symbol,Suffix,Symbol-Suffix) :-
     retract(suffix_counter(LastSuffix)),
     Suffix is LastSuffix+1,
     assert(suffix_counter(Suffix)).


reset_symbol_suffix :-
     abolish(suffix_counter,1),
     assert(suffix_counter(0)).



script(Call,File) :-
     current_output(OldStream),
     open(File,write,OutStream),
     set_output(OutStream),
     predicate_property(Call,_),
     call(Call),
     flush_output(OutStream),
     close(OutStream),
     set_output(OldStream).



%-----------------------------------------------------------





%----------------------------------------------------

% do_n_times: make the call Call N times, and print out the CPU time
% used (adjusted by the time taken to call "true" N times). Instantiate
% variables in Call to those produced on the last call.

time_call(Call) :-
  do_n_times(Call,1).

time_call(Call,N) :-
  do_n_times(Call,N).

do_n_times(Call,N) :-
  statistics(runtime,[T1|_]),
  do_n_times1(Call,N),
  statistics(runtime,[T2|_]),
  do_n_times1(true,N),
  statistics(runtime,[T3|_]),
  T is T2-T1+T2-T3,
  write('Total cpu time: '),
  write(T),write(' ms'),nl,
  write('Time per call:  '),
  Tpc is T/N,
  write(Tpc),write(' ms'),nl,
  !.

do_n_times1(_,0).
do_n_times1(Call,1) :-
  !,
  (call(Call);true),
  !.
do_n_times1(Call,N) :-
  (\+(call(Call)),!;true),
  N1 is N-1,
  !,do_n_times1(Call,N1).


% **********************************************************************
% A general purpose generalizer. For any input structures S1, S2,
% returns S (which should initially be uninstantiated) as the most
% specific term subsuming both S1 and S2.

generalize_structures(S1,S2,S) :-
  generalize_structures(S1,S2,[],_,SG),
  !,S=SG.

% If both S1 and S2 are compound and share the same skeleton,
% recurse inside them.
generalize_structures(S1,S2,BdgsIn,BdgsOut,S) :-
  compound(S1),
  compound(S2),
  functor(S1,F,N),
  functor(S2,F,N),!,
  functor(S,F,N),
  generalize_structures_args(S1,S2,BdgsIn,BdgsOut,1,N,S).
% If S1 and S2 are identical, set S to them.
generalize_structures(S1,S2,Bdgs,Bdgs,S) :-
  S1==S2,
  !,
  S=S1.
% If we've already encountered S1 and S2 together, use the result S.
% (This ensures variables get repeated where possible).
generalize_structures(S1,S2,Bdgs,Bdgs,S) :-
  member(bdg(S1p,S2p,Sp),Bdgs),
  S1==S1p,
  S2==S2p,!,
  S=Sp.
% Otherwise create a new variable S and record it as the result of
% binding S1 and S2.
generalize_structures(S1,S2,Bdgs,[bdg(S1,S2,S)|Bdgs],S).

generalize_structures_args(_,_,Bdgs,Bdgs,M,N,_) :-
  M>N,!.
generalize_structures_args(S1,S2,BdgsIn,BdgsOut,M,N,S) :-
  arg(M,S1,A1),
  arg(M,S2,A2),
  arg(M,S,A),
  generalize_structures(A1,A2,BdgsIn,BdgsMid,A),
  M1 is M+1,
  generalize_structures_args(S1,S2,BdgsMid,BdgsOut,M1,N,S).

% floating_number_chars: a version of number_chars that is guaranteed
% to give a floating point outcome rather than an exponential one.

% Arguments:
%   Number
%   DecimalPlaces     defaults to 2 if absent; if zero, integer form is used
%   String
%
% Can be used in either direction (though it acts the same as number_chars
% if String is not variable on entry). DecimalPlaces must be instantiated
% to a number if it's present.

floating_number_chars(N,Chars) :-
  floating_number_chars(N,2,Chars).

% See if number_chars avoids exponential notation
floating_number_chars(N,_,Chars) :-
  nonvar(Chars),!,
  number_chars(N,Chars).
floating_number_chars(N,_,Chars) :-
  number(N),
  number_chars(N,Chars),
  \+(member(0'E,Chars)),!.
% It didn't. Do it ourselves.
floating_number_chars(N,Decs,Chars) :-
  Whole is integer(N),
  Rem is N-Whole,
  number_chars(Whole,Chars1),
  (Whole=0,N<0->Chars1M=[0'-|Chars1];
   Chars1M = Chars1),
  (Rem<0 -> Abs is -Rem;Abs is Rem),
  (Decs=0 -> Chars=Chars1M;
   remainder_number_chars(Abs,Decs,Chars2),
   append(Chars1M,[0'.|Chars2],Chars)).

remainder_number_chars(_,0,[]) :- !.
remainder_number_chars(Num,Decs,[H|T]) :-
  Num2 is Num*10,
  Units is integer(Num2),
  Rem is Num2-Units,
  number_chars(Units,[H]),
  Decs2 is Decs-1,
  remainder_number_chars(Rem,Decs2,T).


contains_somewhere(X,K) :-
  var(X),!,K==X.
contains_somewhere(X,X) :- !.
contains_somewhere(X,K) :-
  functor(X,F,N),
  (F=K;
   contains_somewhere1(X,K,1,N)).

contains_somewhere1(X,K,M,N) :-
  M=<N,
  arg(M,X,A),
  contains_somewhere(A,K),!.
contains_somewhere1(X,K,M,N) :-
  M<N,
  M1 is M+1,
  contains_somewhere1(X,K,M1,N).

is_alphabetic_code(Code) :-
  char_code_type(Code,Type),
  (Type=upper;Type=lower).


% Note that colons and underscores are allowed as alphanums, so that
% paradigm names qualify. We also allow hyphens as these can appear in
% words.

is_alphanum(0':).
is_alphanum(0'_).
is_alphanum(0'-).
is_alphanum(Code) :-
  char_code_type(Code,Type),
  (Type=lower;Type=upper;Type=digit).



%-------------------------------------------------------------
%
%    setsys(KeyWord,[Item1,...,Itemn])
%    setsys(KeyWord,Item)
%
% Finds a parameter associated with KeyWord (or else uses KeyWord
% itself), ands make sure that the only assertions of
%    setsys_parameter_value(Parm,Item)
% are for Item1, Itemn -- or Item.
%
% If the second argument is a variable, then this is bound to the
% list of current values, or fails if there are none.
%
%----------------------------------------------------------------

:- dynamic setsys_parameter_value/2.

setsys(Parameter,Var) :-
     var(Var), !,
     bagof(Value,setsys_parameter_value(Parameter,Value),Values),
     Var = Values.
setsys(Parameter,Values) :-
     set_value_list(Parameter,Values).

set_value_list(Parameter,Args) :-
     retractall(setsys_parameter_value(Parameter,_)),
     assert_value_list(Parameter,Args).

assert_value_list(_Parameter,[]) :- !.
assert_value_list(Parameter,[Arg|As]) :- !,
     assert_value_list(Parameter,Arg),
     assert_value_list(Parameter,As).
assert_value_list(Parameter,Arg) :-
     assert(setsys_parameter_value(Parameter,Arg)).

% This is done by an indirection just in case we ever want to make it
% into something more complicated.

getsys(Parameter,Val) :-
     setsys_parameter_value(Parameter,Val).


%--------------------------------------------------------
%             pp(Term)
%             pp_in_mode(Term,Mode)
% 
% Pretty prints Term. 
%
%
% Variables are printed via numbervars: A copy
% of the structure to a limited depth is made and numbervars
% is applied to this copy.
%
% The behaviour of the printer can be modified by altering
% the following predicates (e.g. using setsys/2):
%
%   getsys(ppmode,Mode)    controls the way lists (including argument
%                   lists) which do not fit on a line are 
%                   printed. If Mode is 'vertical', then they
%                   are printed on separate lines. If Mode
%                   is 'block' then they are printed in an
%                   indented block with one or more on a line.
%                   Also, there is a mode 'sorted' which flattens
%                   sorts in logical form terms before printing
%                   them in 'block' mode.
%           
%   getsys(ppdepth,Depth)   depth of printing elliptical dots (...)
%
%   getsys(pagewidth,Width) the width of the display in characters
%
%   getsys(ppnumber,yes) display numbers as '#:'(<number>)
%
% Alternative entry point which first instantiates singleton 
% variables in a copy to '_':
%
%     pp_underscore(Term)
%
% This is more expensive than pp because it copies below depth limit.
%
% Subroutines:
% block_print, block_print_punc, term_print_components,
% print_as_block, block_new_line, print_indentation, fits_on_line,
% item_print_size, item_print_size, limited_renamed_copy,
% limited_renamed_list, infix_printed
%------------------------------------------------------------


% set default values.
:- ( getsys(ppmode,_M)
   ; setsys(ppmode,(block))).
:- ( getsys(ppdepth,_N)
   ; setsys(ppdepth,25)).
:- ( getsys(pagewidth,_N)
   ; setsys(pagewidth,70)).

pp_debug :-
  assert((portray(X) :- pp_underscore(X))).

:- pp_debug.

nopp_debug :-
  retract((portray(X) :- pp_underscore(X))),
  nopp_debug.
nopp_debug.


pp(Term) :-
     getsys(ppmode,Mode), !,
     pp_in_mode(Term,Mode).


pp_in_mode(SortedTerm,sorted) :-
     flat_sorted_term(SortedTerm,Term),
     pp_in_mode(Term,(block)), !.
pp_in_mode(Term,Mode) :-
     getsys(ppdepth,Depth),
     limited_renamed_copy(Term,RenamedTerm,_,Depth),
     numbervars(RenamedTerm,0,_),
     nl,
     getsys(pagewidth,Page),
     block_print(RenamedTerm,Page,[],_Rem,Page,Mode),
     nl, !.
pp_in_mode(_Term,_Mode) :-
     getsys(pagewidth,Page),
     print_indentation(Page),
     write('...'),
     nl.
%pp_in_mode(Term,_Mode) :-
%     nl,
%     write('"pagewidth" too small for printing at specified "ppdepth": '),
%     nl,
%     write(Term).

 
% The clauses of block_print that are commented out give 
% alternative styles for printing infix operators.
block_print(Term,Width,Punctuation,Rem3,Page,Mode) :-
     term_print_components(Term,Principal,Left,Middle,Right), !,
     fits_on_line(Principal,Width,Width1),
     write1(Principal),
     print_as_block(Left,Width1,Width1,Rem1,Page,Mode),
     % Middle block indented to after Left
     print_as_block(Middle,Rem1,Rem1,Rem2,Page,Mode),
     % Right and End indented to after Principal
     print_as_block(Right,Width1,Rem2,Rem3,Page,Mode),
     block_print_punc(Punctuation,Width,Page).
block_print(Term,Width,Punctuation,Rem,Page,_Mode) :-
     % term_print_components failed so infix operator or '|'
     fits_on_line(Term,Width,Rem), !,
     write1(Term),
     block_print_punc(Punctuation,Width,Page).
% '|' on separate line unless it all fits
%block_print(Term,Width,Punctuation,0,Page,Mode) :-
%     Term =.. ['.',Left,Right],!,
%     print_as_block(['['],Width,Width,Rem1,Page,Mode),
%     print_as_block([Left],Rem1,Rem1,_,Page,Mode),
%     print_as_block(['|'],Rem1,0,_,Page,Mode),
%     print_as_block([Right],Rem1,Rem1,Rem2,Page,Mode),
%     print_as_block([']'],Rem1,Rem2,_,Page,Mode),
%     block_print_punc(Punctuation,Width,Page).
% All in sequence, possibly indented after '|'
block_print(Term,Width,Punctuation,Rem5,Page,Mode) :-
     Term =.. ['.',Left,Right],!,
     print_as_block(['['],Width,Width,Rem1,Page,Mode),
     print_as_block([Left],Rem1,Rem1,Rem2,Page,Mode),
     print_as_block(['|'],Rem1,Rem2,Rem3,Page,Mode),
     print_as_block([Right],Rem1,Rem3,Rem4,Page,Mode),
     print_as_block([']'],Rem1,Rem4,Rem5,Page,Mode),
     block_print_punc(Punctuation,Width,Page).
block_print(Term,Width,Punctuation,Rem5,Page,Mode) :-
     Term =.. [',',Left,Right],!,
     print_as_block(['('],Width,Width,Rem1,Page,Mode),
     print_as_block([Left],Rem1,Rem1,Rem2,Page,Mode),
     print_as_block([',comma'],Rem1,Rem2,Rem3,Page,Mode),
     print_as_block([Right],Rem1,Rem3,Rem4,Page,Mode),
     print_as_block([')'],Rem1,Rem4,Rem5,Page,Mode),
     block_print_punc(Punctuation,Width,Page).
% Second argument to the right of first argument:
block_print(Term,Width,Punctuation,0,Page,Mode) :-
     Term =.. [InfixOp,Left,Right],
     \+ bracket_op(InfixOp),
     print_as_block([Left],Width,Width,Rem1,Page,Mode),
     print_as_block([InfixOp],Rem1,Rem1,Rem2,Page,Mode),
     print_as_block([Right],Rem2,Rem2,_,Page,Mode),
     block_print_punc(Punctuation,Width,Page).
% Operator and second arg have same indentation as first
block_print(Term,Width,Punctuation,Rem5,Page,Mode) :-
     Term =.. [InfixOp,Left,Right],
     bracket_op(InfixOp),
     print_as_block(['('],Width,Width,Rem1,Page,Mode),
     print_as_block([Left],Rem1,Rem1,_Rem2,Page,Mode),
     write(' '),
     print_as_block([InfixOp],Rem1,0,Rem3,Page,Mode),
     write(' '),
     print_as_block([Right],Rem3,Rem3,Rem4,Page,Mode),
     print_as_block([')'],Rem1,Rem4,Rem5,Page,Mode),
     block_print_punc(Punctuation,Width,Page).
%block_print(Term,Width,Punctuation,0,Page,Mode) :-
%     Term =.. [InfixOp,Left,Right],
%     print_as_block([Left],Width,Width,_,Page,Mode),
%     print_as_block([InfixOp],Width,0,Rem2,Page,Mode),
%     print_as_block([Right],Rem2,Rem2,_,Page,Mode),
%     block_print_punc(Punctuation,Width,Page).
% First, operator, and second printed in seq or at same indent
% i.e. can have any subsequence of them on same line
%block_print(Term,Width,Punctuation,Rem3,Page,Mode) :-
%     Term =.. [InfixOp,Left,Right],
%     print_as_block([Left],Width,Width,Rem1,Page,Mode),
%     print_as_block([InfixOp],Width,Rem1,Rem2,Page,Mode),
%     print_as_block([Right],Width,Rem2,Rem3,Page,Mode),
%     block_print_punc(Punctuation,Width,Page).


block_print_punc([],_Width,_Page) :- !.
block_print_punc(Punctuation,Width,Page) :-
     write(Punctuation),
     block_new_line(Width,0,Page).


term_print_components(Var,Var,[],[],[]) :-
     var(Var), !.
term_print_components('$VAR'(N),'$VAR'(N),[],[],[]) :- !.
term_print_components(Atom,Atom,[],[],[]) :-
     atomic(Atom), !.
term_print_components([Head|Tail],'[',[],[Head|Tail],[']']) :-
     length(Tail,_L),
     !.
term_print_components([_Head|_Tail],_,_,_,_) :-
     !, fail.
term_print_components(Term,_,_,_,_)  :-
     functor(Term,InfixOp,2),
     infix_printed(InfixOp), !,
     fail.
term_print_components(Term,Principal,['('],Items,[')']) :-
     Term =.. [Principal|Items].



%    print_as_block(Items,Width,RemNow,RemNext,Page,Mode)
% The second, third, and fourth arguments are the block width
% (which determines the indentation of the block), the remaining 
% number of charaters on the current line before printing the items, 
% and the remaining number of characters after printing the items.
% (In particular, having the second argument 0 forces printing
% the items on a new line). Multiple items are separated by
% commas.


print_as_block([],_Width,Rem,Rem,_Page,_Mode) :- !.
print_as_block([Singleton],_Width,Rem1,Rem2,_Page,_Mode) :-
     fits_on_line(Singleton,Rem1,Rem2),
     !,
     write1(Singleton).
print_as_block([Singleton],Width,Rem1,Rem2,Page,Mode) :-
     !,    
     block_new_line(Width,Rem1,Page),   
     block_print(Singleton,Width,[],Rem2,Page,Mode).
print_as_block([Head|Tail],Width,_Rem1,Rem2,Page,vertical) :-
     fits_on_line(Head,Width,_Rem), !,
     % write Head on line and new line for Tail
     write2(Head),
     write(','),
     block_new_line(Width,0,Page),
     print_as_block(Tail,Width,Width,Rem2,Page,vertical).
print_as_block([Head|Tail],Width,Rem1,Rem2,Page,vertical) :- !,
     % pretty print Head and start Tail on new line
     block_new_line(Width,Rem1,Page),
     block_print(Head,Width,',',_Rem,Page,vertical),
     print_as_block(Tail,Width,Width,Rem2,Page,vertical).
print_as_block([Head|Tail],Width,Rem1,Rem4,Page,(block)) :-
     fits_on_line(Head,Rem1,Rem2),
     !,
     % Head on same line and continue with Tail
     write2(Head),
     write(','),
     Rem3 is Rem2-1,
     print_as_block(Tail,Width,Rem3,Rem4,Page,(block)).
print_as_block([Head|Tail],Width,Rem1,Rem2,Page,(block)) :-
     Width > Rem1,
     fits_on_line(Head,Width,_Rem),!,
     % Head on new line and continue with Tail
     block_new_line(Width,Rem1,Page),
     print_as_block([Head|Tail],Width,Width,Rem2,Page,(block)).
print_as_block([Head|Tail],Width,Rem1,Rem2,Page,(block)) :- !,
     % pretty print Head on new line, then restart at
     % current indentation
     block_new_line(Width,Rem1,Page),
     block_print(Head,Width,',',_Rem,Page,(block)),
     print_as_block(Tail,Width,Width,Rem2,Page,(block)).



write1(V) :-
     var(V), !,
     write(V).
write1(',comma') :- !, write(',').
write1('_') :- !, write('_').
write1('(') :- !, write('(').
write1(')') :- !, write(')').
write1('[') :- !, write('[').
write1(']') :- !, write(']').
write1((L,R)) :- !,
     write('('),
     write1(L),
     write(','),
     write1_tuple(R),
     write(')').
write1((L;R)) :-!,
     write('('),
     write1(L),
     write(';'),
     write1(R),
     write(')').
write1(BracketInfix) :-
     BracketInfix =.. [InfixOp,L,R],
     bracket_op(InfixOp),
     !,
     write('('),
     write1(L),
     write(' '),
     write(InfixOp),
     write(' '),
     write1(R),
     write(')').
write1(T) :- writeq(T).


write1_tuple((L,R)) :- !,
	write1(L),
	write(','),
	write1_tuple(R).
write1_tuple(X) :- write1(X).

write2('_') :- !, writeq('_').
write2('(') :- !, writeq('(').
write2(')') :- !, writeq(')').
write2('[') :- !, writeq('[').
write2(']') :- !, writeq(']').
write2(X) :- write1(X).



block_new_line(Width,Width,_Page) :- !.
block_new_line(Width,_Rem,Page) :-
     Indent is Page-Width,
     Indent >= 0,
     nl,
     print_indentation(Indent).


print_indentation(0) :- !.
print_indentation(Indent) :-
     write(' '),
     Indent1 is Indent-1,
     print_indentation(Indent1).


fits_on_line(Item,Rem1,Rem2) :-
     item_print_size(Item,Rem1,Rem2), !,
     Rem2 >= 0.


% For efficiency the size of the item is not computed
% if it will not fit (hence the first clause).
item_print_size(_Term,Rem1,_Rem2) :-
     Rem1 < 1, !,
     fail.
item_print_size(Var,Rem1,Rem2) :-
     var(Var), !,
     % case should not arise
     Rem2 is Rem1-4.
item_print_size('$VAR'(N),Rem1,Rem2) :-
     % The number check is necessary because
     % the expressions that Quintus 2.4 gives to the
     % printer as binding results for a goal contain
     % terms like '$VAR'(res_mutabilis([65]))
     number(N),
     N < 26, !,
     Rem2 is Rem1-1.
item_print_size('$VAR'(N),Rem1,Rem2) :-
     number(N),
     N < 234, !,
     Rem2 is Rem1-2.
item_print_size('$VAR'(_),Rem1,Rem2) :- !,
     Rem2 is Rem1-3.
item_print_size(Atom,Rem1,Rem2) :-
     atomic(Atom), !,
     name(Atom,AtomChars),
     length(AtomChars,Size),
     Rem2 is Rem1-Size.
item_print_size([],Rem1,Rem2) :- !,
     Rem2 is Rem1-2.
item_print_size([Singleton],Rem1,Rem3) :- !,
     item_print_size(Singleton,Rem1,Rem2),
     Rem3 is Rem2-2.
item_print_size([Head|Tail],Rem1,Rem4) :- !,
     item_print_size(Head,Rem1,Rem2),
     item_print_size(Tail,Rem2,Rem3),
     Rem4 is Rem3-1.
item_print_size(Term,Rem1,Rem3) :-
     % could be made more efficient
     % by decrementing on argument index
     Term =.. FunctorAndArgs,
     % adjust for list
     Rem2 is Rem1+1,
     item_print_size(FunctorAndArgs,Rem2,Rem3).


limited_renamed_copy(Atom,Abbrev,_Bindings,_Depth) :-
     atomic(Atom),
     current_predicate(atom_print_abbrev,_),
     atom_print_abbrev(Atom,Abbrev), !.
limited_renamed_copy(Number,'#:'(Number),_Bindings,_Depth) :-
     number(Number),
     getsys(ppnumber,yes), !.
limited_renamed_copy(Atom,Atom,_Bindings,_Depth) :-
     atomic(Atom), !.
limited_renamed_copy(Var1,Var2,Bindings,_Depth) :-
     var(Var1), !,
     assoc_var(Var1,Var2,Bindings).
limited_renamed_copy([_|_],['...'],_Bindings,1) :- !.
limited_renamed_copy([Head|Tail],Copy,Bindings,Depth) :- !,
     Depth1 is Depth-1,
     limited_renamed_list([Head|Tail],Copy,Bindings,Depth1).
limited_renamed_copy(Term,TruncatedTerm,_Bindings,1) :- !,
     functor(Term,Functor,_),
     functor(TruncatedTerm,Functor,1),
     arg(1,TruncatedTerm,'...').
limited_renamed_copy(Term,Copy,Bindings,Depth) :-
     Term =.. [Func|Args],
     Depth1 is Depth-1,
     limited_renamed_copy(Func,Func1,_Bindings,_Depth),
     limited_renamed_list(Args,ArgsCopy,Bindings,Depth1),
     Copy =.. [Func1|ArgsCopy].


%limited_renamed_list([],[],_,_) :- !.
limited_renamed_list([HD1|TL1],[HD2|TL2],Bindings,Depth) :- !,
     limited_renamed_copy(HD1,HD2,Bindings,Depth),
     Depth1 is Depth+1,
     limited_renamed_copy(TL1,TL2,Bindings,Depth1).


infix_printed(Functor) :-
     current_op(_,xfx,Functor), !.
infix_printed(Functor) :-
     current_op(_,xfy,Functor), !.
infix_printed(Functor) :-
     current_op(_,yfx,Functor), !.


%--------------------------------------------------

pp_underscore(Term) :-
     underscore_singleton_copy(Term,UnderscoreCopy),
     pp(UnderscoreCopy).


underscore_singleton_copy(Term,UnderscoreCopy) :-
     copy_term(Term,Term1),
     mark_multiple_vars(Term1),
     underscore_sing_marks(Term1,UnderscoreCopy).


mark_multiple_vars(V) :-
     var(V), !,
     V = '$Mark1$'(_).
mark_multiple_vars('$Mark1$'(V)) :- 
     var(V), !,
     V = '$Mark2$'(_).
mark_multiple_vars('$Mark1$'('$Mark2$'(_))) :- 
     !.
mark_multiple_vars(T) :-
     functor(T,_,A),
     mark_multiple_arg(A,T).


mark_multiple_arg(0,_) :- !.
mark_multiple_arg(N,T) :-
     arg(N,T,Arg),
     mark_multiple_vars(Arg),
     M is N-1,
     mark_multiple_arg(M,T).


underscore_sing_marks('$Mark1$'(V),'_') :-
     var(V), !.
underscore_sing_marks('$Mark1$'('$Mark2$'(V)),V) :- 
     !.
underscore_sing_marks(T,TU) :-
     functor(T,F,A),
     functor(TU,F,A),
     underscore_sing_arg(A,T,TU).


underscore_sing_arg(0,_,_) :- !.
underscore_sing_arg(N,T,TU) :-
     arg(N,T,Arg),
     arg(N,TU,ArgU),
     underscore_sing_marks(Arg,ArgU),
     M is N-1,
     underscore_sing_arg(M,T,TU).

