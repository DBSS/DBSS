#!/usr/bin/perl

# Glue Test Suite
# File: CombineResults.pl
# Author: Jonathan Reichenthal
# 
# This program takes multiple output files from the XLE (with names
# outS#.pl) and combines them into one file called
# ParserResults.pl. If there are no XLE output files, then
# ParserResults.pl will be empty. It deletes the origonal files.


open (OutFile, '>./ParserResults.pl');
$n = 0;

open (StatFile, "$ARGV[1].new");
while (1) {
   if (!($line = <StatFile>)) {	# beware the EOF
      print ("\nXLE stat file $ARGV[1].new empty/incorrect format.\n\n");
      last;
   }
   if ($line =~ /^\# (\d+)/) { 	# look for sentence number data
      $num = $1;
      last;
   }
}

while (1) {
   $n++;
   if ($n > $num) {		# we've seen all the results
      last;
   }
   if (!(open (InFile, "$ARGV[0]S$n.pl"))) { # no file means no parse
      print (OutFile "sentence($n,no_parse).\n");
      next;
   }
   if ($line = <InFile>) {
      print (OutFile "sentence($n,$line");
   }
   while ($line = <InFile>) {
      if ($line =~ /^.+\./) {	# look for end of data
	 $line =~ s/\./\)\./;	# add ) before .
      }
      print (OutFile $line);
   }
   unlink ("$ARGV[0]S$n.pl");	# delete input file
}
