
include_macros(FileNames) :-
	retractall(tmp_macro(_,_,_,_)),
	read_macro_files(FileNames),
	findall(macro(Call,Body,LN,FN), tmp_macro(Call,Body,LN,FN), Macros),
	retractall(tmp_macro(_,_,_,_)),
	compile_macros(Macros,FileNames).

read_macro_files([]).
read_macro_files([FN|FileNames]) :-
	format("Reading macro file ~w~n",[FN]),
	concat_list([FN,'.pl'],FNPL),
	testsuite_source_dir(Dir),
	concat_list(['perl ', Dir, '/QuoteProlog.pl ', FN, ' ', FNPL], Cmd),
	shell(Cmd),
	open(FNPL,read,InStream),
	repeat,
	   read_term(InStream,Macro,[layout(L)]),
	   assert_tmp_macro(Macro,L,FN),
	   Macro == end_of_file,
	   !,
	close(InStream),
	read_macro_files(FileNames).


load_macros(FileName) :-
	retractall(tmp_macro(_,_,_,_)),
	open(FileName,read,InStream),
	repeat,
	   read_term(InStream,Macro,[layout(L)]),
	   assert_tmp_macro(Macro,L,FileName),
	   Macro == end_of_file,
	   !,
	close(InStream),
	findall(macro(Call,Body,LN,FN), tmp_macro(Call,Body,LN,FN), Macros),
	retractall(tmp_macro(_,_,_,_)),
	compile_macros(Macros,[FileName]).




assert_tmp_macro(macro(Call,Body),[LineNum|_],FN) :-
	assert(tmp_macro(Call,Body,LineNum,FN)).
assert_tmp_macro(end_of_file,_,_).



compile_macros(Macros,FileNames) :-
	retractall_macros_from_files(FileNames),
	group_macros(Macros,GMacros),
	compile_macro_groups(GMacros,GMacros).

retractall_macros_from_files(FileNames) :-
	member(FN,FileNames),
	retractall(macro(_,_,FN)),
	fail.
retractall_macros_from_files(_).

group_macros(Macros,GMacros) :-
	label_macros_by_pred_arity(Macros,LMacros),
	sort(LMacros,SMacros),
	group_by_pred_arity(SMacros,GMacros).


label_macros_by_pred_arity([],[]).
label_macros_by_pred_arity([macro(Call,Body,LN,FName)|Macros],
			   [(F/N)-macro(Call,Body,LN,FName)|LMacros]) :-
	functor(Call,F,N),
	label_macros_by_pred_arity(Macros,LMacros).

group_by_pred_arity([],[]).
group_by_pred_arity([(F/N)-Macro|SLMacros],GMacros) :-
	group_by_pa(SLMacros,F/N,[Macro],GMacros).

group_by_pa([],F/N,FNMacros,[macros(F/N,FNMacros,_NotDone)]).
group_by_pa([(F/N)-Macro|SLMacros], F/N, FNMacros, GMacros) :- !,
	group_by_pa(SLMacros, F/N, [Macro|FNMacros], GMacros).
group_by_pa([(F1/N1)-Macro|SLMacros], F/N, FNMacros,
	    [macros(F/N,FNMacros,_NotDone)|GMacros]) :- 
	\+ (F1/N1) = (F/N),
	!,
	group_by_pa(SLMacros, F1/N1, [Macro], GMacros).


compile_macro_groups([],_All).
compile_macro_groups([macros(_PA,_PAMacros,Done)|GMacros],All) :-
	Done == done,
	!,
	compile_macro_groups(GMacros,All).
compile_macro_groups([macros(PA,PAMacros,NotDone)|GMacros],All) :-
	var(NotDone),
	comp_pa_macros(PAMacros, [PA], All),
	NotDone = done,
	!,
	compile_macro_groups(GMacros,All).

comp_pa_macros([],_,_).
comp_pa_macros([macro(Call,Body,LN,FN)|PAMacros], Visited, All) :-
	compile_submacro_calls(Body,Visited,LN,FN,All),
	expand_and_assert_macro_defs(Call,Body,LN,FN),
	comp_pa_macros(PAMacros,Visited,All).

compile_submacro_calls(Body,_Visited,_LN,_FN,_All) :-
	(var(Body) ; atomic(Body)), !.
compile_submacro_calls(+@(Macro),Visited,LN,FN,All) :-
	functor(Macro,F,N),
	(
	    member(F/N, Visited) ->
	    nl, write('Circular macro definition for '), write(F/N),
	    nl, write('  in line '), write(LN-FN),
	    nl, write('  trace: '), write(Visited), nl,
	    fail
	;
	    member(macros(F/N,FNMacros,Done), All) ->
	    (
		Done == done -> true
	    ;
		comp_pa_macros(FNMacros,[F/N|Visited],All),
		Done = done
	    )
	;
	    nl, write('Undefined macro ('), write(F/N),
	    write(') called in definition in line: '),
	        write(LN-FN), nl,
	    fail
	),
	!,
	compile_submacro_calls(Macro,Visited,LN,FN,All).
compile_submacro_calls(Expr,Visited,LN,FN,All) :-
	functor(Expr,_,Arity),
	compile_submacro_calls_arg(Arity,Expr,Visited,LN,FN,All).


compile_submacro_calls_arg(0,_Expr,_Visited,_LN,_FN,_All) :- !.
compile_submacro_calls_arg(N,Expr,Visited,LN,FN,All) :-
	arg(N,Expr,Arg),
	compile_submacro_calls(Arg,Visited,LN,FN,All),
	M is N-1, !,
	compile_submacro_calls_arg(M,Expr,Visited,LN,FN,All).
	
expand_and_assert_macro_defs(Call,Body,LN,FileName) :-
	expand_macro_calls(Body,Expansion,LN,FileName),
	assert(macro(Call,Expansion,FileName)),
	fail.
expand_and_assert_macro_defs(_,_,_,_).




expand_macro_calls(X,X,_,_) :-
	(var(X) ; atomic(X)) , !.
expand_macro_calls('+@'(Macro),Exp,LN,FN) :-
	expand_macro_calls(Macro,Macro1,LN,FN),
	(
	    macro(Macro1,_,_) -> true
	;
	    nl, write('WARNING: Call to undefined macro starting in line '),
	    write(LN-FN),
	    nl, write(+@Macro), nl,
	    fail
	),
	macro(Macro1,Exp,_).
expand_macro_calls(X,Exp,LN,FN) :-
	functor(X,F,N),
	\+ F = '+@',
	functor(Exp,F,N),
	expand_macro_calls_arg(N,X,Exp,LN,FN).

expand_macro_calls_arg(0,_,_,_,_) :- !.
expand_macro_calls_arg(N,X,Exp,LN,FN) :-
	N > 0,
	arg(N,X,Arg),
	arg(N,Exp,ExpArg),
	expand_macro_calls(Arg,ExpArg,LN,FN),
	M is N-1,
	expand_macro_calls_arg(M,X,Exp,LN,FN).




% test_m([
%   macro(m1(A,B), and(+m2(A),d), 1),
%   macro(m1(C,D), and(+m2(C),+m3(D,e)), 2),
%   macro(m2(E), +m3(E,xxx), 3),
%   macro(m2(F,G), +m4(F,G), 4),
%   macro(m4(H,I), +m1(+m6(H,I),I), 5),
%   macro(m3(J,K),[m3,J,l], 6)]).
