/* (c) 2001 by the Xerox Corporation.  All rights reserved */

/* ------------------------------------------------------------------------ */
/* Project:   XLE Testsuites                                                */
/* Author:    Dick Crouch                                                   */
/*            Jonathan Reichenthal                                          */
/* Language:  SICStus Prolog 3.7.1                                          */
/* This is the top-level file that loads in the other necessary files.      */
/* ------------------------------------------------------------------------ */

:- use_module(library(lists)).
:- use_module(library(system)).

:- op(300, fy, +@).		% macro call

% to control pretty printing of connectives:
bracket_op('@').
bracket_op('-*').


% Load in the required files
%
:- compile(utl).                 % various prolog utilities
:- compile(xlesub).	         % allow prolog to call xle
:- compile(fdescriptions).	 % check fstrs against descriptions
:- compile(fdescmacros).	 % load macros for fdescriptions
:- compile(tsunpack).            % unpacking f-strs
:- compile(runtestsuite).        % run test suite
:- compile(testreport).          % for analyzing results of testsuite
:- compile(format_convert).      % for extracting f-descriptions
				 % from f-structures
:- compile(create_testfile).     % for generating testsuites from XLE
				 % output

%% Other perl scripts required:
%%     TestSuiteToProlog.pl      Reformats test suite file to make it
%%                               prolog readable
%%     CombineResults.pl         Merges all the XLE fstructure files
%%                               into one big file
%%                               (called ParserResults.pl)
%%     QuoteProlog.pl            Wraps quotes around upper case atoms
%%                               to ensure they are not treated as
%%                               prolog variables


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Stuff to set things up if you want to create a saved state by
%% calling save_system

set_ts_params :-
	working_directory(Dir,Dir),
	datime(datime(Y,M,D,_Hr,_Min,_Sec)),
	retractall(testsuite_source_dir_string(_)),
	retractall(testsuite_source_date(_)),
	atom_chars(Dir,DirStr),
	assert(testsuite_source_dir_string(DirStr)),
	assert(testsuite_source_date(date(Y,M,D))).

testsuite_source_dir(Dir) :-
	% stored as a string because prolog saved states (un)helpfully
	% modify any atoms whose prefix is the original file name of
	% the saved state!  We therefore store it as a string and
	% reconvert to avoid this.
	testsuite_source_dir_string(DirStr),
	atom_chars(Dir,DirStr).

check_ts_scripts :-
	ts_banner,
	ts_perl_scripts_exist(['TestSuiteToProlog.pl',
			       'QuoteProlog.pl',
			       'CombineResults.pl']),
	ts_help.


ts_banner :-
	testsuite_source_date(date(Y,M,D)),
	format(
"~n~nXLE Test Suite Facility ~d/~d/~d~n",[M,D,Y]),
	format(
"Copyright (c) 2001 by the Xerox Corporation. All rights reserved.
This software is made available AS IS, and Xerox Corporation makes no
warranty about the software, its performance or its conformity to any
specification.~n~n",[]).


ts_perl_scripts_exist([]).
ts_perl_scripts_exist([F|Fs]) :-
	testsuite_source_dir(Dir),
	concat_list([Dir,'/',F],FName),
	(
	    file_exists(FName) -> true
	;
	    format("~n~nERROR: Perl Script ~w missing from directory ~w~n",
		   [F,Dir]),
	    halt
	),
	ts_perl_scripts_exist(Fs).


:- set_ts_params.
:- check_ts_scripts.

% To create a saved state
save_system :-
	save_program(run_test,check_ts_scripts).


