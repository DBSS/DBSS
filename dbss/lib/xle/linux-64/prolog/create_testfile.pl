/* (c) 2001 by the Xerox Corporation.  All rights reserved */

/* ------------------------------------------------------------------------ */
/* Project:   XLE Test Suite                                                */
/* Author:    Mary Dalrymple                                                */
/* File:      testsuite.pl                                                  */
/* Language:  SICStus Prolog 3.7.1                                          */
/* Stuff for creating test suite files                                      */
/* ------------------------------------------------------------------------ */

:- use_module(library(system)).
:- use_module(library(lists)).

% How to use Prolog:

% (1) Enable prolog.
% (2) Give the command "prolog" at the command line:
% thinkalot% 149: prolog
% SICStus 3.7.1 (SunOS-5.5.1-sparc): Tue Oct 06 13:38:15 MET DST 1998
% Licensed to parc.xerox.com
% | ?- 
% (3) Load the file you are interested in.  To load this file:
% | ?- [create_testfile].
% You will need to enclose the file name in back quotes '...' if the
% name contains a path.

% Converts XLE output (in new Prolog format) to testfile format.

% create_testfile(all): a testfile is produced for each file in the
% current directory of the form fs<number>.pl according to the default
% config file test_config.pl.  Each testfile has the extension .test.

% create_testfiles(all,ConfigFile): a testfile is produced for each
% file in the current directory of the form fs<number>.pl according to
% the configuration file ConfigFile.  Each testfile has the extension
% .test. 

% create_testfiles(FileList): a testfile is produced for each file in
% the list of files FileList.  Each testfile has the extension
% .test.

% create_testfile(FileIn,FileOut,ConfigFile): XLE output in file
% FileIn is converted to testfile format according to the 
% configuration file ConfigFile and written to file FileOut.

% create_testfile(FileIn,FileOut): XLE output in file
% FileIn is converted to testfile format according to the default
% configuration file test_config.pl and written to file FileOut.

% create_testfile(FileIn): XLE output in file FileIn is converted to
% testfile format according to the default configuration file
% test_config.pl and written to the file FileIn.test.

% create_constraints(StructureIn,TestOut): the structure
% StructureIn (new format) looks like:  
% fstructure(String,Props,Choices,Eqs,Constraints,Cstructure) 
% TestOut is a list of f-structure constraints that the f-structure
% for the string must meet, according to the configuration file that
% has been loaded. 

% Load in the file that turns XLE output from the old format to the
% new format.
:- ensure_loaded(format_convert).

% Declare ":" as a new operator for f-structure descriptions.
%:- op(700,xfx,:).
:- op(500,xfx,:).
% Declare new operators for configuration files.
% Definition of terms like ggf.
:- op(500,xfx,'means').
% Definition of the preds and features we are interested in.
:- op(500,fx,'preds').
:- op(500,fx,'features').

% 
:- dynamic(already_explored_fstrs/1).

%------------------------------------------------------------
% Run create_testfile on a list of files containing selected
% f-structures, writing output test suite to file testsuite.out
% Creates description of f-structure on basis of declarations in
% configuration file (default: test_config)

create_testfile(Files) :-
	create_testfile(Files,test_config,'testsuite.out').

create_testfile(Files,ConfigFile) :-
	create_testfile(Files,ConfigFile,'testsuite.out').

create_testfile(FileSpec,ConfigFile,OutputFile) :-
	(
	    FileSpec = all ->
	    directory_files('.',Files),
	    fsfiles(Files,FSFiles)
	;
	    FSFiles = FileSpec
	),
	clean_database,
	consult(ConfigFile),
	open(OutputFile,write,OStream),
	create_testfile_outputs(FSFiles,OStream),
	close(OStream).

% Create testfile output for a list of files.
create_testfile_outputs([],_).
create_testfile_outputs([Filein|Files],OStream) :-
   create_test_item(Filein,Props,String,TestConstraints),
   prettyprint_testfile(OStream,Props,String,TestConstraints),
   !,
   create_testfile_outputs(Files,OStream).

% Clean the database of predicates from old config files.
clean_database :- 
   abolish(means,2),
   abolish(preds,1),
   abolish(features,1).

% XLE output should be of the form fs<number>.pl.  
fsfiles([],[]).
fsfiles([File|Rest0],[ShortFile|Rest]) :-
   name(File,[102,115|Filename]),
   append(Filenumber,".pl",Filename),
   name(ShortFile,[102,115|Filenumber]), 
   !,
   fsfiles(Rest0,Rest).
fsfiles([_File|Rest0],Rest) :-
   fsfiles(Rest0,Rest).


create_test_item(FileIn,Props,String,TestConstraints) :-
	% Clean the database of old f-structure information.
	abolish(fstructure,6),
	abolish(constraint,1),
	retractall(already_explored_fstrs(_)),
	assert(already_explored_fstrs([])),
	consult(FileIn),
	% Look at the description that we just read in.
	fstructure(String,Props,Choices,Eqs,Constraints,Cstructure),
	% Convert it to the new format (soon to be unnecessary).
	convert_format(
	     fstructure(String,Props,Choices,Eqs,Constraints,Cstructure),
             fstructure(String,Props,Choices,Eqs,NewConstraints,Cstructure)),
	% Create the constraints for the testfile based on the f-structure
	% description input.
	create_constraints(NewConstraints,TestConstraints).


%-----------------------------------------------------------------
% Optional, if you want to create a single file for a single test
% item, (create_single_testfile), or if you want to write each test
% suite item to a separate file

% Read in FileIn, convert it, and write the result to Fileout.
% Default config file is test_config.pl.  Default filename for result
% is Filein.test.

create_single_testfile(Filein) :-
   name(Filein,Filename),
   append(Filename,".test",Fileoutname),
   name(Fileout,Fileoutname),
   create_testfile(Filein,Fileout,test_config).
create_single_testfile(Filein,Fileout) :- 
   create_testfile(Filein,Fileout,test_config).
create_single_testfile(Filein,Fileout,ConfigFile) :-
   clean_database,
   % Read the configuration file.
   consult(ConfigFile),
   create_testfile_output(Filein,Fileout).

create_testfile_output(FileIn,Fileout) :-
	create_test_item(FileIn,Props,String,TestConstraints),
	open(Fileout,write,Filestream),
	prettyprint_testfile(Filestream,Props,String,TestConstraints),
	close(Filestream).


create_multiple_testfiles(FilesIn) :-
	create_multiple_testfiles(FilesIn,test_config).

create_multiple_testfiles(FilesIn,ConfigFile) :-
	clean_database,
	consult(ConfigFile),
	create_multiple_testfiles_aux(FilesIn).

create_multiple_testfiles_aux([]).
create_multiple_testfiles_aux([Filein|Fs]) :-
	atom_chars(Filein,Filename),
	append(Filename,".test",Fileoutname),
	atom_chars(Fileout,Fileoutname),
	create_testfile_output(Filein,Fileout),
	!,
	create_multiple_testfiles_aux(Fs).
	





%-----------------------------------------------------------------




create_constraints(Constraints,TestConstraints) :-
   % Assert all the constraints we are interested in into the database.
   assert_constraints(Constraints),
   % From information given in the config file, determine which are
   % the relevant constraints and features.
   relevant_preds_and_features(Preds,Features),
   % Based on the relevant preds and features, figure out what the
   % relevant constraints for the test file should be.
   extract_relevant_constraints(^,Preds,Features,TestConstraints).

% Assert all of the f-structure constraints into the database,
% normalizing the constraints to f-structure local names and removing
% unique identifiers in semantic forms.
assert_constraints([]).
assert_constraints([C|Rest]) :-
   anonymize(C,AnonC),
   assert(constraint(AnonC)),
   assert_constraints(Rest).

% Replace f-structure names by local names.
anonymize(v:ID,LocalName) :-
   name(ID,IDString),
   name(LocalName,[37|IDString]).
% Remove the unique constant in semantic forms.
anonymize(sf(S,_N,C1,C2),sf(S,NewC1,NewC2)) :-
   anonymize(C1,NewC1),
   anonymize(C2,NewC2).
anonymize(R:[C1,C2],R:[NewC1,NewC2]) :-
   anonymize(C1,NewC1),
   anonymize(C2,NewC2).
anonymize([C|R],[NewC|NewR]) :-
   anonymize(C,NewC), 
   anonymize(R,NewR).
anonymize(X,X) :-
   atom(X).

relevant_preds_and_features([],all) :-
    % If we have specified features as "all", we are already looking
    % at everything, so we don't need to look again at the preds.
   features all, !.
relevant_preds_and_features(Preds,Features) :-
    relevant_preds(Preds),
    relevant_features(Features).
   
% Figure out the preds we are interested in as given in the config
% file.
relevant_preds(Preds) :-
   (preds PredSpec,
    ( (PredSpec = none ; PredSpec = []),
      Preds = [] ;
      PredSpec = all,
      Preds = all ;
      PredSpec = [Spec|Specs],
      expand_pred_specs([Spec|Specs],Preds));
    Preds = all).

% In a list of pred specifications, a member can either represent a
% list of grammatical functions and we will have a definition of the
% form Spec means [...] in the config file, or it is a grammatical
% function itself. 
expand_pred_specs([],[]).
expand_pred_specs([Spec|Specs],FullSpecs) :-
    (Spec means FullSpec ; [Spec] = FullSpec),
     expand_pred_specs(Specs,RestSpecs),
     append(FullSpec,RestSpecs,FullSpecs).
      

% Figure out the features we are interested in.  Assume that we are
% always interested in at least the top-level pred.
relevant_features(Feats) :-
   (features [Feat|Features],
    normalize_feature_specs(['PRED':eq,Feat|Features], Feats); 
    features none,
    Feats = ['PRED':eq];
    features all,
    Feats = all;
    Feats = ['PRED':eq]).

% Every feature needs to be specified as either "exist" or "eq", so if
% there is no specification, fill in the default "eq".
normalize_feature_specs([],[]).
normalize_feature_specs([F:S|Rest0],[F:S|Rest]) :-
   !, normalize_feature_specs(Rest0,Rest).
normalize_feature_specs([F|Rest0],[F:eq|Rest]) :-
   !, normalize_feature_specs(Rest0,Rest).

% Figure out which the relevant constraints are, based on the preds
% and features we are interested in, and return them in a list.
extract_relevant_constraints(FStr,Preds,Features,TestConstraints) :-
   already_explored_fstrs(PrevFStrs),
     % First see if we have looked at this one before (this will happen
     % if we are given a circular f-structure); if we have, don't bother to
     % look at it again.
   ( memberchk(FStr,PrevFStrs), !;
     % Update the list of already-explored f-structures.
     retractall(already_explored_fstrs(_)),
     assert(already_explored_fstrs([FStr|PrevFStrs])),
     % Gather up the features TestFeatures of the fstructure FStr we are
     % looking at; we are looking for any feature in the list Features.
     test_features(Features,FStr,TestFeatures),
     % Gather up the relevant f-structures that are values of any
     % grammatical function name in the list Preds.
     test_preds(Preds,FStr,TestPreds),
     % If FStr is a set, explore its members.
     test_set(FStr,Preds,Features,TestMembers),
     % Recurse on the relevant preds from the current f-structure.
     test_list(TestPreds,Preds,Features,TestCs),
     % Append the constraints together for the final result.
     append_lists([TestFeatures,TestPreds,TestMembers,TestCs],TestConstraints)).

% Apply extract_relevant_constraints/4 to each member of a list.   
test_list([],_Preds,_Features,[]).
test_list([_R:[_GF,F]|Fs],Preds,Features,TestConstraints) :-
   extract_relevant_constraints(F,Preds,Features,TestConstraintsF),
   test_list(Fs,Preds,Features,TestConstraintsFs),
   append(TestConstraintsF,TestConstraintsFs,TestConstraints).

append_lists([], []).
append_lists([List|Lists], Result) :-
	append_lists(Lists, Result0), 
	append(List, Result0, Result).

% Find the features TestFeatures of the f-structure FStr that
% intersect with the list of features Features that we are looking for.
test_features(all,FStr,FullTestFeatures) :-
   findall(Rel:[p:[FStr,Feat],Val],
          constraint(Rel:[p:[FStr,Feat],Val]),
	  TestFeatures),
   test_feature_values(all,TestFeatures,FullTestFeatures),
   !.
test_features(Features,FStr,TestFeatures) :-
   findall(Rel:[p:[FStr,Feat],Val],
         (constraint(Rel:[p:[FStr,Feat],Val]),
          memberchk(Feat:eq,Features)),
	  EqTestFeatures),
   test_feature_values(Features,EqTestFeatures,FullEqTestFeatures),   
   findall(exists:[p:[FStr,Feat]],
         (constraint(Rel:[p:[FStr,Feat],Val]),
          memberchk(Feat:exist,Features)),
	  ExistTestFeatures),
   append(FullEqTestFeatures,ExistTestFeatures,TestFeatures).
   

% See if any of the features we are interested in have f-structures as
% their values.  If they do, record the features of that f-structure
% that we are interested in.
test_feature_values(_,[],[]).
test_feature_values(Features,[R:[P,FStr]|Rest0],[R:[P,FStr]|Rest]) :-
   test_feature_values(Features,Rest0,Rest1),
   (local_fstr_name(FStr), !,
    test_features(Features,FStr,FStrFeatures),
    append(FStrFeatures,Rest1,Rest);
    Rest1 = Rest).

local_fstr_name(FStr) :-
   atom(FStr),
   name(FStr,[37|_]).   

% This is the case where we are interested in all features that take
% f-structures with a pred (or sets of such f-structures) as their value.
test_preds(all,FStr,TestPreds) :- !,
   findall(Rel:[p:[FStr,Feat],Val],
         (constraint(Rel:[p:[FStr,Feat],Val]),
          has_pred(Val)),
	  TestPreds).
% This is the case where there is a specific list of grammatical
% functions that we are interested in.
test_preds(Preds,FStr,TestPreds) :-
   findall(Rel:[p:[FStr,GF],Val],
         (constraint(Rel:[p:[FStr,GF],Val]),
          memberchk(GF,Preds)),
	  TestPreds).   

has_pred(Val) :-
  (constraint(_:[p:[Val,'PRED'],_]);
   constraint(_:[p:[Val,'$'],SetMember]),
   has_pred(SetMember)),
  !.

% See if FStr is a set; if it is, explore its members.
test_set(FStr,Preds,Features,TestMembers) :-
   findall(eq:[p:[FStr,'$'],Member],constraint(eq:[p:[FStr,'$'],Member]),SetMembers),
   test_list(SetMembers,Preds,Features,MemberFeats),
   append(SetMembers,MemberFeats,TestMembers).

% Prettyprint the f-structure output.
prettyprint_testfile(Filestream,Props,String,TestConstraints) :-
   find_root_category(Props,Cat),
   format(Filestream, '~a: ~a~nfd~nand:[', [Cat,String]),
   format_constraint_list(Filestream, TestConstraints),
   format(Filestream, "~nefd~2n", []).
% If we get unexpected input just write it out.
prettyprint_testfile(Filestream,Any) :-
   portray_clause(Filestream,Any).

find_root_category([rootcategory(Cat)|_],Cat) :- !.
find_root_category([_|Rest],Cat) :-
   find_root_category(Rest,Cat).

format_constraint_list(Filestream,[]) :-
   format(Filestream, "~5+~s",["]"]).
format_constraint_list(Filestream,[Last]) :-
   format(Filestream, "~5+~q~s", [Last,"]"]).
format_constraint_list(Filestream,[First,Next|Rest]) :-
   format(Filestream, "~5+~q,~n", [First]),
   format_constraint_list(Filestream,[Next|Rest]).
