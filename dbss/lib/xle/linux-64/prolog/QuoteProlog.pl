#!/usr/bin/perl

# Glue Test Suite
# File: QuoteProlog.pl
# Author: Dick Crouch
#  Wraps prolog quotes around
#   1.  Identifiers beginning with upper case letters
#   2.  Local variables beginning with %  (tricky!)
#   3.  +_


open (INFILE,  $ARGV[0]);
open (OUTFILE, ">$ARGV[1]");  # File to write prolog terms

$line = <INFILE>; 
while ($line ne "") {
    # wrap quotes around stuff:
	#1 upper case attribute names
    $line =~ s/([\[\,\( ])([A-Z][\w\-\_]*)([\]\)\, ])/$1'$2'$3/g;
	#2 local names beginning with %
    $line =~ s/([\[\,\( ])(%[\w\-\_]*)([\]\,\)])/$1'$2'$3/g;
	#3 the +_ value
    $line =~ s/([\[\,\( ])\+\_([\]\,\) ])/$1'+_'$2/g;
	#4 the $ value
    $line =~ s/([\[\,\( ])\$([\]\,\) ])/$1'\$'$2/g;
    print OUTFILE ($line);
    $line = <INFILE>;
}

