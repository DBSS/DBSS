# (c) 2002-2005 by the Palo Alto Research Center.  All rights reserved.
# (c) 1996-2001 by the Xerox Corporation.  All rights reserved.
# this is the TCL/TK interface to the XLE parser
# the commands exported by the XLE parser are:
# get_field (type)address field

#determine whether we are in OpenLook
#set olwm 0
#foreach item [split [exec ps -axc] \n] {
#    if [regexp (olwm|olvw) $item] {set olwm 1}
#}

# GENERIC INSPECTOR CODE

proc inspect {address {raw ""} {spawner ""}} {
    # because this procedure may be called from the debugger
    xle-initialization-script
    if {$raw == 1} {
	;# force the raw inspector to be used
	set window [inspect-raw $address]
    } else {switch [type $address]  {
	AVPair {set window [show-avpair $address]}
	Graph {set window [inspect-graph $address]}
	STATE {set window [inspect-state $address]}
	InternalSolution {set window [display-internal-solution $address]}
	default {set window [inspect-raw $address]}
    }}
#	Clause {set window [inspect-clause $address]}
#    if {$spawner != ""} {
#	set $spawn($spawner) [concat $window $spawn($spawner)]
#    }
    return $window
}

proc inspect-state {address} {
    set self [inspect-raw $address]
    if {[get_field $address "is_virtual"] != "T"} {return $self}
    set panel $self.panel
    button $panel.realize -text realize -command "realize-state $address"
    pack $panel.realize -in $panel -anchor nw
    return $self
}

proc realize-state {address} {
    set self .${address}+
    realize_state $address
    recompute-inspector $self 1
}

proc inspect-raw {address} {
    set fields [get_field $address *fields*]
    if {$fields == ""} {return ""}
    set self .${address}+ 
    ;# add "+" to distinguish from non-raw inspector
    if {[winfo exists $self]==0} {
	toplevel $self -class Inspector
	wm title $self [get-label $address]
    } elseif {[winfo children $self] != ""} {
	;# if the window has children it already exists
	;# otherwise, it is being recomputed
	focus-window $self
	return $self
    }
    inspector-panel $self 1
    foreach field $fields {
	display-field $field [get_field $address $field] $self top
    }
    return $self
}

proc inspect-clause {address} {
    set self .${address}+
    if {[winfo exists $self]!=0 && [winfo children $self] != ""} {
	;# if the window has children it already exists
	;# otherwise, it is being recomputed
	focus-window $self
	return $self
    }
    set self [inspect-raw $address]
    set type [get_field $address type]
    if {$type == "AND" || $type == "OR" || $type == "CONS"} {
	set field 1
	while {1} {
	    incr field
	    set address [get_field $address next]
	    if {[get_field $address type] != $type} {
		display-field item${field} $address $self top
		break
	    } elseif {$field == 11} {
		display-field "rest" $address $self top
		break
	    } else {
		set item [get_field $address item]
		display-field item${field} $item $self top
	    }
	}
    }
    return $self
}

proc inspector-panel {parent {raw ""}} {
    set self $parent.panel
    frame $self 
    pack $self -in $parent -anchor nw
    button $self.kill -text "kill" -command "destroy $parent"
    bind $self.kill <Control-Button-1> "kill-descendents $parent"
    bind $self.kill <Button-3> {puts "destroys the window"}
    button $self.recompute -text "recompute" \
	-command "recompute-inspector $parent $raw"
    bind $self.recompute <Button-3> {puts "refreshes the display to reflect a changed data structure"}
    set address [get-address $parent]
    button $self.address -text "address" -command "puts $address"
    bind $self.address <Button-3> {puts "prints out the address of the underlying data structure"}
    button $self.done -text "done" -command "set suspend_tcl 1"
    bind $self.done <Button-3> {puts "returns you to the debugger if you got to here from the debugger"}
    pack $self.kill $self.recompute $self.address $self.done \
	-in $self -anchor nw -side left
    if {$raw == ""} {
	set  address [get-address $parent]
	button $self.raw -text "raw" -command "inspect-raw $address"
	bind $self.raw <Button-3> {puts "gives you a non-specialized, standard inspector on the data structure"}
	pack $self.raw -in $self -anchor nw -side left
    }
}

proc recompute-inspector {self {raw ""}} {
    set address [get-address $self]
    foreach w [winfo children $self] {destroy $w}
    update idletasks
    inspect $address $raw
}    

proc display-field {name value self {side} {cutoff 2}} {
    if {$value == ""} {return}
    frame $self.a$name
    pack $self.a$name -anchor nw
    label $self.a${name}label -text $name
    frame $self.a${name}value
    set anchor "w"
    if {[llength $value] > 1} {
	set anchor "nw"
    }
    pack $self.a${name}label -in $self.a$name -anchor $anchor -side left
    pack $self.a${name}value -in $self.a$name
    set rest ""
    if {[llength $value] > [expr {$cutoff + 1}]} {
	set rest [lrange $value $cutoff end]
	set value [lrange $value 0 [expr {$cutoff - 1}]]
    }
    set n 1
    foreach item $value {
	set label [get-label $item]
	set button $self.a${name}value.$n
	incr n
	button $button -text $label
	inspect-button $button $item
	pack $button -anchor nw -side $side
    }
    if {$rest !=""} {
	set button $self.a${name}value.rest
	if {[winfo exists $button]} {return}
	set length [llength $rest]
	button $button -text "...($length)"\
	    -command "display-rest ${self}${name} $name $rest"
	bind $button <Button-3> {puts "displays the rest"}
	pack $button -anchor nw -side $side
    }
}

proc display-rest {self name args} {
    set self "${self}rest"
    if {[winfo exists $self]==0} {
	toplevel $self -class Inspector
	wm title $self $self
    } elseif {[winfo children $self] != ""} {
	;# if the window has children it already exists
	;# otherwise, it is being recomputed
	focus-window $self
	return $self
    }
    set panel $self.panel
    frame $panel
    pack $panel -in $self -anchor nw
    button $panel.kill -text "kill" -command "destroy $self"
    bind $panel.kill <Button-3> {puts "destroys the window"}
    button $panel.recompute -text "recompute"
    bind $panel.recompute <Button-3> {puts "does nothing"}
    button $panel.done -text "done" -command "set suspend_tcl 1"
    bind $panel.done <Button-3> {puts "returns you to the debugger if you got to here from the debugger"}
    pack $panel.kill $panel.recompute $panel.done \
	-in $panel -anchor nw -side left
    display-field $name $args $self top 10
}

proc get-toplevel {window} {
# get the name of the top-level window
    set tail [string range $window 1 end]
    set top [string range $window 0 [string first . $tail]]
    if {$top == ""} {set top [string range $window 0 end]}
    return $top
}

proc get-address {window} {
# get the address being inspected
    set address [get-toplevel $window]
    set last [expr {[string length $address] - 1}]
    if {[string index $address $last] == "+"} {
	set address [string range $address 1 [expr {$last-1}]]
    } else {set address [string range $address 1 end]}
    return $address
}

proc truncate {string} {
    if {[string length $string] > 30} {
	set string [string range $string 0 26]
	append string " ..."
    } else {return $string}
}

proc inspect-button {button address} {
    if {[string first " " $address] != -1} {return}
    set top [get-toplevel $button]
    bind $button <Button-1> "inspect $address 0 $top"
    bind $button <M4-Button-1> "inspect-and-destroy $address 0 $top"
    bind $button <Control-Button-1> "inspect $address 1 $top"
    bind $button <M4-Control-Button-1> "inspect-and-destroy $address 1 $top"
    bind $button <Meta-Button-1> "puts $address"
    bind $button <Button-3> {
	puts ""
	puts "<Button-1> creates an inspector for this value"
	inspect-button-doc
    }
}

proc inspect-button-doc {} {
	puts "<Control-Button-1> creates a non-specialized (raw) inspector for this value"
	puts "<Meta-Button-1> prints the address of the data structure"
}    

proc inspect-and-destroy {address raw top} {
    set window [inspect $address $raw $top]
    if {$window != $top} {update idletasks; destroy $top}
}

# GRAPH inspector

proc inspect-graph {address {noflash ""}} {
    set self .$address
    if {[winfo exists $self]==0} {
	toplevel $self -class Inspector
	wm title $self [get-label $address]
    } elseif {[winfo children $self] != ""} {
	;# if the window has children it already exists
	;# otherwise, it is being recomputed
	focus-window $self $noflash
	return $self
    }
    inspector-panel $self
#    button $self.constraints -text "constraints" \
	-command "get_field $address constraints"
#    button $self.statistics -text "statistics" \
	-command "get_field $address statistics"
#    pack $self.constraints $self.statistics \
	-in $self.panel -side left -anchor nw
    frame $self.value
    pack $self.value -in $self -anchor nw
    set n 0
    set attrs [get_field $address attrs]
    foreach attr $attrs {
        display-avpair $attr $self.value
	incr n
	if {$n < 20} {continue}
	display-field more [lrange $attrs 20 end] $self left
	break
    }
    set restrictions [get_field $address solutions]
    display-field nogoods [get_field $address nogoods] $self left
    display-field "solution sets" $restrictions $self left
#    foreach restr $restrictions {
#	display-restriction $restr $self [llength $restrictions]
#    }
    display-field daughters [get_field $address daughters] $self left
    return $self
}

# DISPLAYING AVPAIRS (within a graph)

proc display-avpair {address parent} {
    global maxavpairdepth
    set id [get_field $address attr]
    set expanded [get_field $address expanded]
    set partly [get_field $address partly_expanded]
    if {$partly != "" || $expanded != "" || [meta-var $id]} {
	set self $parent.$address
	frame $self
	button $self.attr -text $id -relief flat -bd 0
	inspect-button $self.attr $address
	pack $self.attr -in $self -side left -anchor nw
	set context [get_field $address context]
	set defined [get_field $address defined]
	if {$context != "" && [get_field $context type] != "TRUE"} {
	    button $self.context -text [get-label $context] -bd 1
	    inspect-button $self.context $context
	    pack $self.context -in $self -side left -anchor nw
	    set graph [get_field $address graph]
	    set edge [get_field $graph edge]
	    set-button-context $edge $self.context $context
	}
	if {$defined != "" && $defined != $context} {
	    button $self.defined -text [get-label $defined] -bd 1
	    inspect-button $self.defined $defined
	    pack $self.defined -in $self -side left -anchor nw
	    set graph [get_field $address graph]
	    set edge [get_field $graph edge]
	    set-button-context $edge $self.defined $defined
	}
	pack $self -in $parent -anchor nw
	set info 0
	set depth [avpair-depth $address]
	if {$depth < $maxavpairdepth} {
	    bind $self.attr <Button-1> "toggle-avpair $parent $address"
	    bind $self.attr <Button-3> {
		puts "<Button-1> toggles display of this attribute"
		inspect-button-doc
	    }
	    set info [display-avpair-content $self $address]
	} else {
	    bind $self.attr <Button-1> "inspect-avpair $address"
	    bind $self.attr <Button-3> {
		puts "<Button-1> inspects the avpair"
		inspect-button-doc
	    }
	}
    
#	pack $self -in $parent -anchor nw -pady [expr {$info >= 2 ? 5 : 0}]
	return 1
    }
}

proc display-avpair-content {self address} {
    frame $self.value -bd 1 -relief raised
    pack $self.value -in $self -side left -anchor nw
    set self $self.value
    set info 0
    set rels [get_field $address rels]
    set attrs [get_field $address attrs]
    set graph [get_field $address graph]
    set values [filter-cvpairs [get_field $address values] "IL"]
    if {$values != ""} {
	incr info
	display-rvpair == $values $self $graph
    }
    foreach attr $attrs {
	set displayed [display-avpair $attr $self]
	if {$displayed == 1} {incr info}
    }
    foreach rel $rels {
	set displayed [display-rvpair $rel [get_field $rel values] \
			   $self $graph]
	if {$displayed == 1} {incr info}
    }
    return info
}
    
proc inspect-avpair {address {noflash ""}} {
    set self .$address
    if {[winfo exists $self]==0} {
	toplevel $self -class Inspector
	wm title $self [get-label $address]
    } elseif {[winfo children $self] != ""} {
	;# if the window has children it already exists
	;# otherwise, it is being recomputed
	focus-window $self $noflash
	return $self
    }
    inspector-panel $self
    display-avpair-content $self $address
    return $self
}

proc toggle-avpair {parent address} {
    set self $parent.$address
    if {[lsearch [pack slaves $self] $self.value] >= 0} {
	pack forget $self.value
    } elseif {[winfo exists $self.value] == 1} {
	pack $self.value -in $self -side left
    } else {
	inspect-raw $self
    }
}

proc show-avpair {address} {
    set expanded [get_field $address expanded]
    set partly [get_field $address partly_expanded]
    if {$partly == "" && $expanded == ""} {
	return [inspect $address 1]
    }
    set self [open-avpair $address]
    if {$self == ""} {return [inspect $address 1]}
    update idletasks
    if {[winfo exists $self.attr]} {
	$self.attr flash
    } else {
	focus-window $self
    }
    return [get-toplevel $self]
}

set maxavpairdepth 2

proc avpair-depth {address} {
    global maxavpairdepth
    set prefix [get_field $address prefix]
    if {$prefix == ""} {
	set graph [get_field $address graph]
	set first [lindex [get_field $graph attrs] 0]
	;# expand the first meta-variable in place
	if {$address == $first} {return 0}
	return $maxavpairdepth
    }
    set depth [avpair-depth $prefix]
    incr depth
    if {$depth > $maxavpairdepth} {return 1}
    return $depth
}

proc open-avpair {address} {
    global maxavpairdepth
    set depth [avpair-depth $address]
    if {$depth == $maxavpairdepth} {
	set self [inspect-avpair $address 1]
	return $self
    }
    set prefix [get_field $address prefix]
    if {$prefix == ""} {
	set parent [inspect-graph [get_field $address graph] 1]
    } else {set parent [open-avpair $prefix]}
    if {[lsearch [pack slaves $parent] $parent.value] == -1} {
	pack $parent.value -in $parent -side left
    }
    set self $parent.value.$address
    return $self
}
	
proc get-avpair-label {address {root ""}} {
    set attr [get_field $address attr]
    set prefix [get_field $address prefix]
    set graph [get_field $address graph]
    if {$prefix != ""} {
	set prefix [get-label [get_field $address prefix] $root]
    } elseif {$graph == $root} {
	return $attr
    } else {
	set prefix [get-label $graph]
    }
    return ${prefix}.$attr
}

proc meta-var {id} {expr {$id == "UP" || $id == "DOWN"}}

proc filter-cvpairs {values skip} {
    set cvpairs ""
    foreach value $values {
	if {[get_field $value props] != $skip} {
	    set cvpairs [concat $cvpairs $value]
	}
    }
    return $cvpairs
}

# DISPLAYING RELATIONS (within a graph)

proc display-rvpair {relation values parent graph} {
    set self $parent.$relation
    frame $self
    pack $self -in $parent -anchor nw
    button $self.button -text [get-label $relation] -bd 0
    inspect-button $self.button $relation
    bind $self.button <Button-1> "toggle-avpair $parent $relation"
    bind $self.button <Button-3> {
	puts "<Button-1> toggles display of this relation"
	inspect-button-doc
    }
    frame $self.value -bd 0 -relief raised
    foreach value $values {display-cvpair $value $self.value $graph}
    pack $self.button $self.value -in $self -side left -anchor nw
}

proc set-button-context {edge button context} {
    global cbuttons bcontext
    if {![info exists cbuttons($edge)]} {set cbuttons($edge) ""}
    if {[lsearch $cbuttons($edge) $button] < 0} {
	set cbuttons($edge) [linsert $cbuttons($edge) 0 $button]
    }
    set bcontext($button) $context
}

proc display-cvpair {address parent graph} {
    set self $parent.$address
    frame $self
    pack $self -in $parent -anchor nw
    set context [get_field $address context]
    set context2 [get_field $address new-context]
    set props [get_field $address props]
    set value [get_field $address value]
    label $self.< -text "<" -bd 0
    button $self.context -text [get-label $context] -bd 0
    inspect-button $self.context $context
    ;# save the context and button for toggling
    set edge [get_field $graph edge]
    set-button-context $edge $self.context $context
    label $self.> -text ">" -bd 0
    pack $self.< $self.context -in $self -side left
    if {$context2 != ""} {
	button $self.context2 -text [get-label $context2] -bd 0
	inspect-button $self.context2 $context2
	pack $self.context2 -in $self -side left
    }
    set n 0
    foreach item $value {
	incr n
	set itemID "item$n"
	button $self.$itemID -text [get-label $item $graph] -bd 0
	inspect-button $self.$itemID $item
	pack $self.$itemID -in $self -side left
    }
    if {$props != ""} {
	label $self.props -text $props -bd 0
	pack $self.props -in $self -side left
    }
    pack $self.> -in $self -side left
}

# DISPLAYING SOLUTIONS (within a graph)

proc display-restriction {address parent count} {
    set self $parent.$address
    frame $self
    pack $self -in $parent -anchor nw
    set restriction [get_field $address restriction]
    set solutions [get_field $address solutions]
    if {$count > 0} {
	set graph [get-address $parent]
	button $self.attr -text [get-label $restriction]
	inspect-button $self.attr $restriction
	pack $self.attr -in $self -side left
	frame $self.value -bd 1 -relief raised
	pack $self.value -in $self -side left
    } else {
	;# create a dummy frame only
	frame $self.value
	pack $self.value -in $self -side left
    }
    foreach solution $solutions {
	display-solution $solution $self.value
    }
}

proc display-solution {address parent} {
    set self $parent.$address
    frame $self
    pack $self -in $parent -anchor nw
    set map [get_field $address map]
    set clauses [get_field $address clauses]
    set graph [get-address $parent]
    set inconsistent [get_field $address inconsistent]
    set incomplete [get_field $address incomplete]
    set unoptimal [get_field $address unoptimal]
    button $self.attr -text [get-label $clauses]
    inspect-button $self.attr $address
    pack $self.attr -in $self -side left
    if {$inconsistent == "T"} {
	label $self.inconsistent -text "X"
	pack $self.inconsistent -in $self -side left
    }
    if {$unoptimal == "T"} {
	label $self.unoptimal -text "U"
	pack $self.unoptimal -in $self -side left
    }
    if {$incomplete == "T"} {
	label $self.incomplete -text "O"
	pack $self.incomplete -in $self -side left
    }
    frame $self.value -bd 1 -relief raised
#    label $self.count -text [get_field $address count]
    pack $self.value -in $self -side left
    foreach solution $map {
	display-internal-solution $solution $self.value
    }
}
    
proc display-internal-solution {address {parent ""}} {
    if {$parent != ""} {
	set self $parent.$address
	set graph [get-address $parent]
	frame $self
	pack $self -in $parent -anchor nw
    } else {
	set self .$address
	set graph [get_field $address graph]
	set edge [get_field $graph edge]
	set graph [get_field $edge graph]
	if {[winfo exists $self]==0} {
	    toplevel $self -class Inspector
	    wm title $self [get-label $address]
	} elseif {[winfo children $self] != ""} {
	    ;# if the window has children it already exists
	    ;# otherwise, it is being recomputed
	    focus-window $self
	    return $self
	}
	inspector-panel $self
    }
    set subtree [get_field $address subtree_context]
    set local [get_field $address local_choices]
    set partial [get_field $address partial]
    set complete [get_field $address complete]
    button $self.on -text "show" -command "highlight-solution $graph $address"
    bind $self.on <Button-3> {
	puts "highlights the context variables that are valid under this solution"}
    pack $self.on  -in $self -side left
    if {[get_field $partial *label*] != "Empty"} {
	label $self.p -text p:
	set label [get-label [get_field $partial clauses] $graph]
	button $self.partial -text $label
	inspect-button $self.partial $partial
	bind $self.partial <Button-1> "show-solution2 $graph $partial"
	bind $self.partial <Button-3> {
	    puts "<Button-1> jumps to this solution in the daughter graph"
	    inspect-button-doc 
	}
	pack $self.p $self.partial -in $self -side left
    }
    label $self.c -text c:
    set label [get-label [get_field $complete clauses] $graph]
    button $self.complete -text $label
    inspect-button $self.complete $complete 
    bind $self.complete <Button-1> "show-solution2 $graph $complete"
    bind $self.complete <Button-3> {
	puts "<Button-1> jumps to this solution in the daughter graph"
	inspect-button-doc
    }
    label $self.t -text t:
    button $self.subtree -text [get-label $subtree]
    inspect-button $self.subtree $subtree
    label $self.l -text l:
    button $self.local -text [get-label $local]
    inspect-button $self.local $local
    set count [expr {[get_field $partial count]*[get_field $complete count]}]
    label $self.n -text "\#: $count"
    pack $self.c $self.complete $self.t $self.subtree $self.l $self.local \
	$self.n -in $self -side left
}

proc highlight-solution {graph solution} {
    global cbuttons bcontext marks
    inspect $graph
    set edge [get_field $graph edge]
    if {![info exists cbuttons($edge)]} {set cbuttons($edge) ""}
    mark-solution $solution 1
    foreach button $cbuttons($edge) {
	if {![winfo exists $button]} {
	    continue
	}
	highlight-button $button [evaluate-clause $bcontext($button)]
    }
    ;# we mark solutions with "" rather than 0 to avoid cross-talk 
    ;# on other solutions
    mark-solution $solution ""
}

proc mark-solution {solution {value ""}} {
    global marks
    set partial [get_field $solution partial]
    set complete [get_field $solution complete]
    set subtree [get_field $solution subtree_context]
    set local [get_field $solution local_choices]
    mark-clauses [get_field $partial clauses] $value
    mark-clauses [get_field $complete clauses] $value
    mark-clauses $subtree $value
    mark-clauses $local $value
}

proc mark-clauses {clause {value ""}} {
    global marks
    if {$clause == ""} {return}
    set marks($clause) $value
    set type [get_field $clause type]
    switch $type {
	CONS {
	    set item [get_field $clause item]
	    set marks($item) $value
	    mark-clauses [get_field $clause next] $value
	}
	AND {
	    set item [get_field $clause item]
	    set marks($item) $value
	    mark-clauses [get_field $clause next] $value
	}
	CHOICE {
	    set disj [get_field $clause disjunction]
	    mark-clauses [get_field $disj context] $value
	}
    }
}

proc evaluate-clause {clause {depth 0}} {
    global marks
    set type [get_field $clause type]
    if {$type == "TRUE"} {return 1}
    if {[info exists marks($clause)] && $marks($clause) == 1} {
	return $marks($clause)}
    if {$depth > 1} {return 0}
    if {$clause == ""} {return 0}
    switch $type {
	AND {set item [evaluate-clause [get_field $clause item] $depth]
	    set next [evaluate-clause [get_field $clause next] $depth]
	    set result [expr {$item  && $next}]
	}
	OR  {set item [evaluate-clause [get_field $clause item] $depth]
	    set next [evaluate-clause [get_field $clause next] $depth]
	    set result [expr {$item || $next}]
	 }
	OPAQUE {incr depth
	    set result [evaluate-clause [get_field $clause opaque] $depth]}
	JUSTIFIED {set result [evaluate-clause [get_field $clause clause] $depth]}
	CHOICE {set result 0}
	NOT {
	    set result [evaluate-clause [get_field $clause not_clause] $depth]
	    set result [expr {!$result}]
	}
	default {error "$type type not implemented ($clause)"}
    }
    return $result
}   

proc hightlight-button {button value} {
    if {![evaluate-clause [get_field [get-address $button] context]]} {
	set value 0}
    if {$value} {
	$button configure -foreground white
	$button configure -background black
    } else {
	$button configure -foreground black
	$button configure -background white
    }
}

proc show-solution2 {graph solution} {
    set window [inspect $solution]
    update idletasks
    # ${window}.restrictionvalue.1 flash
}

proc show-solution {graph solution {depth 0}} {
    ;# show solution in the context of graph (or its daughters)
    ;# search daughters exhaustively to depth of 2
    if {$depth > 2} {return}
    set window ""
    foreach rset [get_field $graph solutions] {
	foreach s [get_field $rset solutions] {
	    if {$s == $solution && $window == ""} {
		set window [inspect-graph $graph 1]
		update idletasks
		.$graph.$rset.value.$s.attr flash
	    }
	}
    }
    foreach daughter [get_field $graph daughters] {
	if {$window == ""} {
	    set window [show-solution $daughter $solution [expr {$depth+1}]]
	}
    }
    if {$depth == 0 && $window == ""} {
	set window [inspect $solution]
    }
    return $window
}

# PRINTING CLAUSES

 proc print-clause {clause {opaque ""}} {
    if {$clause == ""} {return "nil"}
    set type [get_field $clause type]
    if {$opaque != "" || $type == "AND" || $type == "OR" || $type == "CONS"} {
	set result [get-opaque-id $clause]
	set nogood [get_field $clause nogood]
	if {$nogood == "INCOMPLETE"} {
	    append result "C"
	} elseif {$nogood == "INCONSISTENT"} {
	    append result "X"
	} elseif {$nogood != ""} {
	    append result "*"
	}
	return $result
    }
    switch $type {
	CHOICE {
	    set graphid [get-graph-id [get_field $clause graph]]
	    set result "($graphid)[get_field $clause *label*]"
	}
	TRUE {return "True"}
	FALSE {return "False"}
	OPAQUE {set graph [get_field $clause graph]
	    set edge [get_field $graph edge]
	    set internal [get_field $clause opaque]
	    set graph2 [get_field $internal graph]
	    set edge2 [get_field $graph2 edge]
	    if {$edge != $edge2} {
		set id [print-clause $internal T]
	    } else {set id [print-clause $internal]}
	    set c2 [get_field $graph2 context]
	    if {[get_field $c2 type] == "TRUE"} {
		set result \[$id\]
	    } else {
		set id2 [print-clause $c2]
		set result "${id2}&\[$id\]"
	    }}
	AND {set item [print-clause [get_field $clause item]]
	    set next [print-clause [get_field $clause next]]
	    set result [concat "(&" $item $next]
	    append result ")"}
	OR  {set item [print-clause [get_field $clause item]]
	    set next [print-clause [get_field $clause next]]
	    set result [concat "(V" $item $next]
	    append result ")"}
	CONS {set item [print-clause [get_field $clause item] $opaque]
	    set next [print-clause [get_field $clause next] $opaque]
	    if {$next == "nil"} {set result $item} else {
		set result "("
		append result [concat $item $next]
		append result ")"}
	}
	NOT {set item [print-clause [get_field $clause not_clause] $opaque]
	    set result "~"
	    append result $item}
	JUSTIFIED {set result "JUSTIFIED"}
	default {error "unknown type: $type"}
    }
     if {[get_field $clause nogood] != ""} {append result "*"}
     return $result
 }
	
proc get-opaque-id {clause} {
    set graph [get_field $clause graph]
    return [get-graph-id $graph][get_field $clause exported-id]
}

proc get-edge-id {edge} {
    global edgeid
    if {[info exists edgeid($edge)]} {return $edgeid($edge)}
    if {[get_field $edge from] == [get_field $edge to]} {return "??"}
    foreach subtree [get_field $edge subtrees] {
	get-edge-id [get_field $subtree partial]
	get-edge-id [get_field $subtree complete]
    }
    if {[info exists edgeid(last)]} {
	set lastid $edgeid(last)
    } else {set lastid -1}
    incr lastid
    set edgeid(last) $lastid
    if {$lastid < 26} {
	set label [format "%c" [expr {$lastid + 65}]]
    } else {
	set left [expr {$lastid/26}]
	set right [expr {($lastid - ($left*26)) + 65}]
	set label [format "%c%c" [expr {$left + 64}] $right]
    }
    set edgeid($edge) $label
    return $label
}	

proc get-graph-id {graph} {
    set id [get_field $graph id]
    if {$id == ""} {return "???"}
    set pos4 [expr {${id}/(26*26*26)}]
    set id [expr {$id - ($pos4*26*26*26)}]
    set pos3 [expr {${id}/(26*26)}]
    set id [expr {$id - ($pos3*26*26)}]
    set pos2 [expr {${id}/26}]
    set pos1 [expr {$id - ($pos2*26)}]
    incr pos4 65
    incr pos3 65
    incr pos2 65
    incr pos1 65
    if {$pos4 > 90} {incr pos4 6}
    if {$pos3 > 90} {incr pos3 6}
    if {$pos2 > 90} {incr pos2 6}
    if {$pos1 > 90} {incr pos1 6}
    
    if {$pos4 > 65} {
	return [format "%c%c%c%c" $pos4 $pos3 $pos2 $pos1]
    } elseif {$pos3 > 65} {
	return [format "%c%c%c" $pos3 $pos2 $pos1]
    } elseif {$pos2 > 65} {
	set label [format "%c%c" $pos2 $pos1]
	return $label
    } else {
	return [format "%c" $pos1]
    }
}	

proc get-ptr {object} {
    set first [string first \) $object]
    string range $object [expr {$first+1}] end
    return $object
}

proc define-imported-ids {edge} {
    global label
    set eptr [get-ptr $edge]
    if {[info exists label($eptr,Pid)]} {
	set Pid $label($eptr,Pid)
    } else {set Pid 1}
    if {[info exists label($eptr,Qid)]} {
	set Qid $label($eptr,Qid)
    } else {set Qid 1}
    foreach clause [get_field $edge imported-clauses] {
	set cptr [get-ptr $clause]
	if {![info exists label($eptr,$cptr)]} {
	    set prefix [label-prefix $edge $clause]
	    if {$prefix == "P"} {
		set label($eptr,$cptr) P$Pid
		incr Pid
	    } else {
		set label($eptr,$cptr) Q$Qid
		incr Qid
	    }
	}
    }
    set label($eptr,Pid) $Pid
    set label($eptr,Qid) $Qid
}

proc label-prefix {edge clause} {
    set graph [get_field $clause graph]
    foreach traversal [get_field $edge subtrees] {
	set partial [get_field $traversal partial]
	set complete [get_field $traversal complete]
	if {[get_field $partial graph] == $graph} {return "P"}
	if {[get_field $complete graph] == $graph} {return "Q"}
    }
    return "X"
}

#DEBUGGING AIDS

# put a breakpoint in a function and let examine the environment
proc breakpoint {} {
    set max [expr {[info level] -1}]
    set current $max
    show $current
    while (1) {
	puts -nonewline stderr "#$current: "
	gets stdin line
	while {![info complete $line]} {
	    puts -nonewline stderr "? "
	    append line \n[gets stdin]
	}
	switch -- $line {
	    + {
		if {$current < $max} {
		    show [incr current]
		}
	    }
	    - {
		if {$current > 0 } {
		    show [incr current -1]
		}
	    }

	    C {puts stderr "Resuming execution";return}
	    ? {show $current}
	    default {
		catch {uplevel #$current $line } result
		puts stderr $result
	    }
	}
    }
}

# 
proc show {current} {
    if {$current > 0 } {
	set info [info level $current]
	set proc [lindex $info 0]
	puts stderr "$current: Procedure $proc \
		{[info args $proc]}"
	set index 0
	foreach arg [info args $proc] {
	    puts stderr \
		    "\t$arg = [lindex $info [incr index]]"
	}
    } else {
	puts stderr "Top level"
    }
}
