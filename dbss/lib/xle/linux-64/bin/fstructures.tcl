# (c) 2002-2005 by the Palo Alto Research Center.  All rights reserved.
# (c) 1997-2001 by the Xerox Corporation.  All rights reserved.  
# This is the Tcl/Tk interface to displaying fstructures.  The entry 
# procedures defined in this file are: show-fstructures, print-fs,
# print-fs-as-lex, print-fs-as-prolog, display-fschart

# HELP COMMANDS

proc add-help-doc {doc} {
    global help_doc
    lappend help_doc $doc
}

proc help {{command "help"}} {
    global help_doc
    if {$command == "help"} {
	puts "help <command>"
	puts "Displays help for a particular command."
	puts -nonewline "Commands known: "
	set first 1
	set length 16
	foreach doc $help_doc {
	    if {!$first} {
		puts -nonewline ", "
	    }
	    set first 0
	    set name [lindex $doc 0]
	    set delta [expr {[string length $name]+2}]
	    incr length $delta
	    if {$length > 75} {
		puts ""
		set length $delta
	    }
	    puts -nonewline "$name"
	}
	puts ""
	return
    }
    foreach doc $help_doc {
	set name [lindex $doc 0]
	if {![string match $command $name]} {continue}
	foreach line [lrange $doc 1 end] {
	    puts $line
	}
	puts ""
    }
}


	
set browser ""

proc documentation {} {
    global env xledocfile browser
    if {$browser =="" && [info exists env(WEB_BROWSER)]} {
	set browser $env(WEB_BROWSER)
    }
    if {$browser == ""} {
	set browser "netscape"
	puts "WEB_BROWSER undefined, trying $browser."
    }
    set file $xledocfile
    if {$browser == "netscape"} {
	# first try for an existing netscape
	set result [catch "exec netscape -remote openFile($file)" msg]
	if {!$result} {return}
	# puts $msg
    }
    puts "Opening $browser (please wait)."
    set result [catch "exec $browser $file &" msg]
    if {$result} {puts $msg}
    return ""
}

proc variables {} {
    global xledir
    # Print the contents of variables.tcl
    set found 0
    set dirs "${xledir}/bin ${xledir} ${xledir}/share/xle ${xledir}/../share/xle"
    foreach dir $dirs {
	if {![catch {set fileID [open "$dir/variables.tcl" r]}]} {
	    puts "Displaying the content of $dir/variables.tcl..."
	    set found 1
	    break
	}
    }
    if {!$found} {
	puts "Could not find variables.tcl in $dirs."
	return
    }
    puts ""
    while {1} {
	set length [gets $fileID line]
	if {$length == -1} {break}
	puts $line
    }
    close $fileID
    
    # Print inline documentation
    puts ""
    puts "To find out what the default value of a variable is, type:"
    puts {     puts "$variable"}
    puts "To change the default add the following to your .xlerc file:"
    puts {     set variable newvalue}
    puts "Variables that turn things on or off should be set to 1 for on"
    puts "and 0 for off."
}

# DISPLAYING AN FSTRUCTURE

set showWeights 0
set showOnlyChoices all
set showOnlyChoicesArray(all) 1
set showOnlyChoicesArray(abbreviated) 0
set showOnlyChoicesArray(OTmarks) 0
set showOnlyChoicesArray(disjunctions) 0
set showOnlyChoicesArray(weights) 0

proc show-fstructures {tree {chart ""}} {
    debug show-fstructures
    if {$chart == ""} {
	if {$tree == "" || $tree == "(DTree)0"} {
	    print-stack
	    error "No chart given!"
	}
	set chart [get_field $tree chart]
    }
    set fswindow [chart-window-name $chart fstructure]
    set-window-prop $fswindow fsID 0
    show-next-fstructure $tree "(Graph)0" $chart
}

proc show-next-fstructure {tree fstructure chart} {
    debug show-next-fstructure
    set fswindow [chart-window-name $chart fstructure]
    set fsID [get-window-prop $fswindow fsID]
    if {$fstructure == "" || $fsID == ""} {
	set fsID 1
    } else {
	incr fsID
    }
    set solution [get_next_fstructure $tree $fstructure]
    if {$solution == ""} {
	set fsID 0
    }
    set-window-prop $fswindow fsID $fsID
    set treeid [get_field $tree label]
    display-fstructures $solution $fswindow "" $treeid $tree $chart
    display-other-structures $fswindow $solution

    mark-chosen-solution-choices $solution
}
    
proc show-prev-fstructure {tree fstructure chart} {
    set fswindow [chart-window-name $chart fstructure]
    set fsID [get-window-prop $fswindow fsID]
    if {$fstructure == "(Graph)0" || $fsID == ""} {
	set fsID [count_tree_solutions $tree -all]
    } else {
	incr fsID -1
    }
    set solution [get_next_fstructure $tree $fstructure -prev]
    if {$solution == ""} {
	set fsID 0
    }
    set treeid [get_field $tree label]
    set-window-prop $fswindow fsID $fsID
    display-fstructures $solution $fswindow "" $treeid $tree $chart
    display-other-structures $fswindow $solution

    mark-chosen-solution-choices $solution
}

proc original-chart {chart} {
    set pos [string first "-" $chart]
    if {$pos != -1} {
	set chart [string range $chart 0 [expr {$pos - 1}]]
    }
    return $chart
}

proc copy-chart {chart} {
    # we don't actually copy the chart
    # instead, we append the next available number to it
    set chart [original-chart $chart]
    set max 1
    while {1} {
	incr max
	set chart1 ${chart}-${max}
	set found 0
	foreach child [winfo children .] {
	    set chart2 [get-window-prop $child chart]
	    if {$chart2 != $chart1} {continue}
	    set found 1
	    break
	}
	if {!$found} {break}
    }
    return $chart1
}

proc copy-window {self} {
    if {[get-window-prop $self type] == "fstructure"} {
	set data [get-window-prop $self data]
	set projpath [get-window-prop $self projpath]
	set treeid [get-window-prop $self treeid]
	set tree [get-window-prop $self tree]
	set chart [copy-chart [get-window-prop $self chart]]
	set self [chart-window-name $chart fstructure]
	display-fstructures $data $self $projpath $treeid $tree $chart
    }
    if {[get-window-prop $self type] == "tree"} {
	set data [get-window-prop $self data]
	set chart [copy-chart [get-window-prop $self chart]]
	show-trees-only $data $chart
    }
}

set fsCommands {
    {command -label "Resize Window" \
	-command "resize-canvas $self.value" \
	-accelerator "<r>" \
	-doc "Toggles resizing the window to fit the f-structure."}
    {command -label "Copy Window" \
	-command "copy-window $self" \
	-doc "Makes a copy of the window."}
    {command -label "Print Postscript" \
	-command "print-fs * $self" \
	-doc "Writes the fstructure as a postscript file (.eps)."}
    {command -label "Print LFG File" \
	-command "print-fs-as-lex * $self" \
	-doc "Writes the fstructure as a lexical entry (.lfg)."}
    {command -label "Print Prolog File" \
	-command "print-fs-as-prolog * $self" \
	-doc "Writes the fstructure as a prolog term (.pl)."}
    {command -label "Generate from this FS" \
	-command "generate-from-window $self" \
	 -doc "Generate from the current fstructure.\nThe generator can be set using 'setx defaultgenerator <generator> <chart>', \nwhere <chart> is the chart of the current fstructure."}
}

set fsViews {
    {checkbutton -label "abbreviate attributes" -font xleuifont \
	-variable "abbreviatedFStruct" \
	-command "redisplay-fstructure $self" \
	 -accelerator "<a>" \
	-doc "Toggles whether attributes are abbreviated." \
	-doc "When attributes are abbreviated, only the attributes " \
	-doc "listed in the variable abbrevAttributes are displayed."
	-default "set abbreviatedFStruct 0"}
    {checkbutton -label "constraints" -font xleuifont \
	-variable showConstraintsFStruct \
	-command "redisplay-fstructures" \
	 -accelerator "<c>" \
	-doc "Toggles the display of negative and sub-c constraints."
	-default "set showConstraintsFStruct 0"}
    {checkbutton -label "node numbers" -font xleuifont \
	 -variable "showFSNumbers" \
	 -command "redisplay-fstructures" \
	 -accelerator "<n>" \
	 -doc "Toggles display of node numbers on f-structure." \
	 -default "set showFSNumbers 1"}
    {checkbutton -label "subc constraints" -font xleuifont \
	-variable showSubcConstraintsFStruct \
	-command "redisplay-fstructures" \
	 -accelerator "<s>" \
	-doc "Toggles the display of sub-c constraints."
	-default "set showSubcConstraintsFStruct 0"}
}

proc setup-fswindow {self treeid tree fstructure projpath chart} {
    global window_width window_height
    global fsCommands fsViews
    debug setup-fswindow
    if {[winfo exists $self]==0} {
	debug setup-fswindow-1
	set-window-prop $self type fstructure
	set-window-prop $self chart $chart
	if {$projpath != ""} {
	    set-window-prop $self location lowerright
	} else {
	    set-window-prop $self location lowerleft
	}
	set position [window-position $self]
	toplevel $self
	wm minsize $self 200 200
	wm geometry $self ${window_width}x${window_height}$position
	set-window-prop $self position $position
	set color [highlight-color $self]
	$self configure -highlightcolor $color -highlightbackground $color \
		-highlightthickness 1
	frame $self.panel
	pack $self.panel -in $self -anchor nw

	# Buttons
	add-kill-button $self $self.panel
	if {$tree != 0} {
	    # Prev and Next buttons
	    button $self.prev -text "prev" -font xleuifont
	    pack $self.prev -in $self.panel -side left -anchor nw
	    bind $self.prev <Button-3> {
		puts ""
		puts "Shows prev fstructure."
		print-button-command %W
	    }
	    button $self.next -text "next" -font xleuifont
	    pack $self.next -in $self.panel -side left -anchor nw
	    bind $self.next <Button-3> {
		puts ""
		puts "Shows next fstructure."
		print-button-command %W
	    }
	}

	# Commands
	set cmenu $self.panel.commands.menu
	menubutton $self.panel.commands -text Commands \
	    -font xleuifont -menu $cmenu
	pack $self.panel.commands -in $self.panel -side left
	create-xle-menu $self $cmenu fsCommands
	
	# Views
	set omenu $self.panel.views.menu
	menubutton $self.panel.views -text Views \
	    -font xleuifont -menu $omenu
	pack $self.panel.views -in $self.panel -side left
	create-xle-menu $self $omenu fsViews
	tk_menuBar $self.panel $self.panel.commands $self.panel.views

	# View accelerators
	# create-accelerator-buttons $self $self.panel fsViews

	set scrollable [scrollable-canvas $self.value $self]
	pack $scrollable -in $self -expand 1 -fill both
    } else {
	debug setup-fswindow-2
	clear-window $self
	focus-window $self 1
    }
    if {$tree != 0 && $tree != ""} {
	debug setup-fswindow-3
	;# treeid has spaces that we need to protect
	if {$fstructure == ""} {set fstructure "(Graph)0"}
	set command "show-next-fstructure $tree $fstructure $chart"
	$self.next configure -command $command
	set command "show-prev-fstructure $tree $fstructure $chart"
	$self.prev configure -command $command
    }
    debug setup-fswindow-end
} 

proc display-fstructures {fstructures window projpath treeid tree {chart ""}} {
    global showConstraintsFStruct showSubcConstraintsFStruct showFSNumbers
    global abbreviatedFStruct
    global currentFSIDarray defaultWindows
    debug display-fstructures
    if {$chart == ""} {
	if {$fstructures != "" && $fstructures != "(Graph)0"} {
	    set chart [default-chart $fstructures]
	} else {
	    set chart [default-chart $tree]
	}
    }
    set self $window
    setup-fswindow $self $treeid $tree $fstructures $projpath $chart
    set-window-prop $self data $fstructures
    set-window-prop $self projpath $projpath
    set-window-prop $self treeid $treeid
    set-window-prop $self tree $tree
    set lastproj [last-projection $projpath]
    set bg [background-color $self]
    set chart [get-window-prop $self chart]
    if {$chart == ""} {
	puts "display-fstructures is missing a chart!"
	print-stack
    }
    if {$tree != 0 && $tree != "" && $treeid != ""} {
	set all [count_tree_solutions $tree -all]
	set good [count_tree_solutions $tree]
	set bad [expr {$all - $good}]
	if {$good == 1} {set s ""} else {set s "s"}
	if {$bad > 1000} {set all ">$all"}
	set allmsg ""
	set goodmsg "0"
	if {$good != 0 || $bad != 0} {
	    set goodmsg "$good valid"
	    if {$bad > 0} {
		set allmsg "($all total)"
	    }
	}
	wm title $self \
	    "$goodmsg ${lastproj}-structure${s} for $treeid $allmsg"
    } else {
	wm title $self "${lastproj}-structure window for $treeid"
    }
    set canvas $self.value
    set frame $canvas.frame
    frame $frame -bg $bg -highlightthickness 0
    $canvas create window 0 0 -window $frame -anchor nw
    set n 1
    set maxwidth 0
    set maxheight 0
    foreach fs $fstructures {
	set cmenu $canvas.${n}menu
	frame $cmenu -bg $bg -highlightthickness 0
	set locked 0
	set selection [get_tree_selection $tree]
	set solution [get_field $fs solution]
	set fsID [get-window-prop $self fsID]
	if {$selection != ""} {
	    set locked 1
	    # use the previous ID of the selection
	    if {[info exists currentFSIDarray($selection)]} {
		set fsID $currentFSIDarray($selection)
	    }
	    # save the selection's id for future use
	    set currentFSIDarray($selection) $fsID
	}
	# Add a select button unless the fstructure is inconsistent
	if {[get_field $fs inconsistent] == "" && $projpath == ""} {
	    button $cmenu.select -text "lock" -font xleuifont \
		-command "toggle-fs-selection $cmenu.select $tree $fs" \
		-bg $bg -highlightthickness 0
	    bind $cmenu.select <Button-3> {
		puts ""
		puts "Selects/deselects this f-structure to the exclusion of the other f-structures"
		puts "on this tree.  This selection gets propagated up the tree."
		print-button-command %W
	    }
	    pack $cmenu.select -in $cmenu -side left -anchor nw
	    highlight-button $cmenu.select $locked
	}

	# Add an X-structure button
	if {$fsID == 0} {set fsID 1}
	set-window-prop $self fsID $fsID
	if {$fsID != ""} {
	    set fslabel "${lastproj}-structure \#$fsID"
	} else {
	    set fslabel "${lastproj}-structure"
	}
	button $cmenu.label -text $fslabel -font xleuifont -relief flat \
		-bg $bg -highlightthickness 0
	bind $cmenu.label <Control-Shift-Button-1> "inspect $fs"
	bind $cmenu.label <Button-3> {
	    puts ""
	    puts "<Control-Shift-Button-1> inspects the f-structure"
	}
	pack $cmenu.label -in $cmenu -side left -anchor nw
	add-projection-buttons $fs $projpath $fslabel $window $cmenu
	set explanations [get_field $fs "explanations"]
	if  {[get_field $fs "unoptimal"] != ""} {
	    lappend explanations "UNOPTIMAL"
	}
	if {$explanations != ""} {
	    set label [join $explanations ", "]
	    label $cmenu.nogood -text "($label)"
	    pack $cmenu.nogood -in $cmenu -side left -anchor w
	} elseif {[get_field $fs "bad"] != ""} {
	    label $cmenu.nogood -text "(EVENTUALLY BAD)"
	    pack $cmenu.nogood -in $cmenu -side left -anchor w
	}
	if {[get_field $fs "ungrammatical"] != 0} {
	    label $cmenu.ungrammatical -text "(UNGRAMMATICAL)"
	    pack $cmenu.ungrammatical -in $cmenu -side left -anchor w
	}    
	pack $cmenu -in $frame -anchor nw
	incr maxheight 21 ;# I don't know how to compute this number
        set tcanvas $canvas.$n
        incr n 1
	set bg [background-color $canvas]
	canvas $tcanvas -bg $bg -highlightthickness 0
	pack $tcanvas -in $frame -anchor nw -side top
	if {$showConstraintsFStruct} {
	    set constraints "-showConstraints"
	} elseif {$showSubcConstraintsFStruct} {
	    set constraints "-showSubcConstraints"
	} else {
	    set constraints ""
	}
	if {$showFSNumbers} {
	    set shownumbers "-showFSNumbers"
	} else {
	    set shownumbers ""
	}
	if {$abbreviatedFStruct} {
	    set abbreviated "-abbreviated"
	} else {
	    set abbreviated ""
	}
	display_fstructure $fs $tcanvas $projpath $constraints \
	    $shownumbers $abbreviated \
	    -sentence [extract-sentence $tree]
	set-window-prop $tcanvas choicevalues [get_choice_values $fs]
	set-window-prop $tcanvas graph $fs
	$tcanvas addtag all all ;# mark all items on canvas
	set bbox [$tcanvas bbox all] ;# get bounding box of items
	set width [lindex $bbox 2]
	set height [lindex $bbox 3]
	if {$width == ""} {set width 100}
	if {$height == ""} {set height 100}
	set height [expr {$height+20}] ;# leave space between
	$tcanvas configure -width $width -height $height
	incr maxheight $height
	set maxwidth [max $maxwidth $width]
    }
    $canvas configure -scrollregion [list 0 0 $maxwidth $maxheight]
    debug display-fstructures-end
    raise $window
}

proc redisplay-fstructures {} {
    # redisplay all of the f-structure windows
    foreach child [winfo children .] {
	set type [get-window-prop $child type]
	if {$type != "fstructure"} {continue}
	redisplay-fstructure $child
    }
}

proc redisplay-fstructure {window} {
    set fstructures [get-window-prop $window data]
    set projpath [get-window-prop $window projpath]
    set treeid [get-window-prop $window treeid]
    set tree [get-window-prop $window tree]
    display-fstructures $fstructures $window $projpath $treeid $tree
}

proc inspect-fstructures {tree} {
    set fs "(Graph)0"
    while {1} {
	set fs [get_next_fstructure $tree $fs]
	if {$fs == ""} break
	inspect $fs
    }
}

proc display-other-structures {window fstructure} {
    set chart [get-window-prop $window chart]
    foreach child [winfo children .] {
	if {$child == $window} {continue}
	if {![string match "${window}*" $child]} {continue}
	if {[get-window-prop $child chart] != $chart} {continue}
	set projpath [get-window-prop $child projpath]
	if {$projpath == ""} {continue}
	set treeid [get-window-prop $child treeid]
	display-fstructures $fstructure $child $projpath $treeid 0 $chart
    }
}

proc last-projection {projpath} {
    set proj [lindex $projpath [expr {[llength $projpath]-1}]]
    if {$proj == ""} {
	set proj "F"
    }
    return $proj
}

set debug 0

proc debug {displaypoint} {
    global debug
    if {$debug} {puts $displaypoint}
}

set windowProps(window,prop) ""

# PRINTING FSTRUCTURES

proc print-fs {{file "*"} {window ""}} {
    if {$window == ""} {
	set window [default-window fstructure]
    }
    set fstructure [get-window-prop $window data]
    if {$fstructure == ""} {
	puts "Please display something to print."
	return
    }
    
    # Determine the prefix for the file name
    # We want the optimality window to have "os" as a prefix
    set projpath [get-window-prop $window projpath]
    set length [llength $projpath]
    set last [lindex $projpath [expr {$length - 1}]]
    set type [string index $last 0]
    if {$type == ""} {
	set type "f"
    }
	
    if {$file == "*"} {set file [default-filename ${type}s "eps" $fstructure]}
    print-canvas $file $window.value.1
    if {$type == "f"} {
	puts "fstructure printed on $file."
    } else {
	puts "${type}-structure printed on $file."
    }
}

proc print-fschart {{file "*"} {window ""}} {
    if {$window == ""} {
	set window [default-window fschart]
    }
    set fstructure [get-window-prop $window data]
    if {$fstructure == ""} {
	puts "Please display something to print."
	return
    }
    if {$file == "*"} {set file [default-filename "fschart" "eps" $fstructure]}
    print-canvas $file $window.value
    puts "fschart printed on $file."
}

proc print-fs-as-lex {{file "*"} {window ""}} {
    if {$window == ""} {
	set window [default-window fstructure]
    }
    set fstructure [get-window-prop $window data]
    if {$fstructure == ""} {
	puts "Please display something to print."
	return
    }
    if {$file == "*"} {set file [default-filename "fs" "lfg" $fstructure]}
    set file [print_graph_as_lex $fstructure $file]
    puts "fstructure printed on $file."
}

proc print-fs-as-prolog {{file "*"} {window ""}} {
    if {$window == ""} {
	set window [default-window fstructure]
    }
    set fstructure [get-window-prop $window data]
    if {$fstructure == ""} {
	puts "Please display something to print."
	return
    }
    set chart [get-window-prop $window chart]
    set sentence [get-chart-prop $chart sentence]
    if {$file == "*"} {
	set filename [default-filename "fs" "pl" $fstructure]
    } else {
	set filename $file
    }
    set filename [print_graph_as_prolog $fstructure $filename $sentence]
    if {$file == "*"} {
	puts "fstructure printed on $filename."
    }
}

# DISPLAYING AN FSCHART (A PACKED REPRESENTATION OF ALL SOLUTIONS)

set fsChartCommands {
    {command -label "Resize Window" \
	 -command "resize-canvas $self.value" \
	 -accelerator "<r>" \
	 -doc "Toggles resizing window to fit fschart."}
    {command -label "Print Postscript" \
	 -command "print-fschart * $self" \
	 -doc "Writes postscript file (.eps)."}
    {command -label "Print Prolog File" \
	 -command "print-prolog-chart-graph * $self 1" \
	 -doc "Writes a packed prolog representation (.pl)."}
    {command -label "Generate from this FSChart" \
	-command "generate-from-window $self" \
	 -doc "Generate from the current fschart.\nThe generator can be set using 'setx defaultgenerator <generator> <chart>', \nwhere <chart> is the chart of the current fstructure."}
    }

set fsChartViews {
    {checkbutton -label "abbreviate attributes" -font xleuifont \
	 -variable "abbreviatedFSChart" \
	 -command "redisplay-fschart $self" \
	 -accelerator "<a>" \
	 -doc "Toggles whether attributes are abbreviated." \
	 -doc "When attributes are abbreviated, only attributes listed " \
	 -doc "in the variable abbrevAttributes are displayed."\
	 -default "set abbreviatedFSChart 0"}
    {checkbutton -label "constraints" -font xleuifont \
	 -variable "showConstraintsFSChart" \
	 -command "redisplay-fschart $self" \
	 -accelerator "<c>" \
	 -doc "Toggles display of negative and sub-c constraints." \
	 -default "set showConstraintsFSChart 0"}
    {checkbutton -label "linear" -font xleuifont \
	 -variable "showLinearFSChart" \
	 -command "redisplay-fschart $self" \
	 -accelerator "<l>" \
	 -doc "Toggles linear display of f-structure." \
	 -default "set showLinearFSChart 0"}
}

proc disambiguate-window {window} {
    global property_weights_file
    if {[string match "(Chart)*" $window]} {
	set chart $window
	set fschart [extract_chart_graph $chart]
    } elseif {[string match "(Graph)*" $window]} {
	set fschart $window
	set chart [get_field $fschart chart]
    } else {
	set fschart [get-window-prop $window data]
	set chart [get_field $fschart chart]
    }
    if {$fschart == ""} {
	puts "Please display something to disambiguate."
    }
    if {[get-chart-prop $chart property_weights_file] == "" &&
        $property_weights_file != ""} {
	setx property_weights_file $property_weights_file $chart
    }
    if {[get-chart-prop $chart property_weights_file] == ""} {
	puts "Please specify a property weights file."
	return
    }
    unmark_all_choices $fschart
    select-most-probable-structure $fschart
    display-chosen-solution $fschart
    display-fschart $fschart
    display-fschartchoices $fschart
    redisplay-choices "." $chart
}

proc select-most-probable-structure {graph} {
    select_most_probable_structure $graph
}

proc print-most-probable-structure {parser filename {contexted ""}} {
    set sentence ""
    if {[string match "(Chart)*" $parser]} {
	set graph [extract_chart_graph $parser]
	set sentence [get-chart-prop $parser sentence]
	if {$graph == ""} {
	    set graph [dummy_chart_graph $parser]
	}
    } elseif {[string match "(Graph)*" $parser]} {
	set graph $parser
    }
    unmark_all_choices $graph
    select-most-probable-structure $graph
    if {$contexted == ""} {
	# Only print the selected structure
	set tree [get_chosen_tree $graph]
	if {$tree == ""} {set tree "(DTree)0"}
	set graph [get_chosen_fstructure $graph $tree]
    }
    print_graph_as_prolog $graph $filename $sentence 1
}

proc setup-fschartwindow {self} {
    global window_width window_height
    global fsChartCommands fsChartViews
    if {[winfo exists $self]} {
	set chart [get-window-prop $self chart]
	set testfile [get-chart-prop $chart testfile]
	if {[winfo exists $self.nextSentence] != ($testfile != "")} {
	    destroy $self
	}
    }
    if {[winfo exists $self]==0} {
	set position [window-position $self]
	toplevel $self
	wm title $self "fschart"
	set-window-prop $self type fschart
	set moreversion [more-version $self]
	wm minsize $self 200 200
	wm geometry $self ${window_width}x${window_height}$position
	set-window-prop $self position $position
	set color [highlight-color $self]
	$self configure -highlightcolor $color -highlightbackground $color \
		-highlightthickness 1
	set tree 1
	set scrollable [scrollable-canvas $self.value $self]
	pack $scrollable -in $self -expand 1 -fill both -side bottom
	frame $self.panel
	pack $self.panel -in $self -anchor nw
	add-kill-button $self $self.panel
	if {$moreversion > 0} {return}
	set chart [get-window-prop $self chart]
	set testfile [get-chart-prop $chart testfile]
	if {$testfile != ""} {
	    add-sentence-buttons $self
	}
	button $self.panel.best -text "most probable" \
	    -font xleuifont -command "disambiguate-window $self"
	pack $self.panel.best -in $self.panel -side left -anchor nw
	bind $self.panel.best <Button-3> {
	    puts ""
	    puts "Shows most probable structure."
	    print-button-command %W
	}
	
	# Commands
	set cmenu $self.panel.commands.menu
	menubutton $self.panel.commands -text Commands \
	    -font xleuifont -menu $cmenu
	pack $self.panel.commands -in $self.panel -side left
	create-xle-menu $self $cmenu fsChartCommands
	
	# Views
	set omenu $self.panel.views.menu
	menubutton $self.panel.views -text Views \
	    -font xleuifont -menu $omenu
	pack $self.panel.views -in $self.panel -side left
	create-xle-menu $self $omenu fsChartViews
	tk_menuBar $self.panel $self.panel.commands $self.panel.views

	# View accelerators
	# create-accelerator-buttons $self $self.panel fsChartViews
    } else {
	clear-window $self
	focus-window $self 1
    }
}

proc add-sentence-buttons {self} {
    button $self.prevSentence -text "prev sentence" -font xleuifont
    bind $self.prevSentence <Button-1> "prev-sentence $self"
    bind $self.prevSentence <Button-3> {
	puts ""
	puts "<Button-1> causes the previous sentence in a test file to be parsed"
	puts "after parse-testfile has been called once."
	print-button-command %W
    }
    pack $self.prevSentence -in $self.panel -side left -anchor nw
    button $self.nextSentence -text "next sentence" -font xleuifont
    bind $self.nextSentence <Button-1> "next-sentence $self"
    bind $self.nextSentence <Button-3> {
	puts ""
	puts "<Button-1> causes the next sentence in a test file to be parsed"
	puts "after parse-testfile has been called once."
	print-button-command %W
    }
    pack $self.nextSentence -in $self.panel -side left -anchor nw
}

proc next-sentence {self} {
    set chart [get-window-prop $self chart]
    set testfile [get-chart-prop $chart testfile]
    if {$testfile == ""} {
	puts "Please call parse-testfile on a testfile."
	return
    }
    set done [get-chart-prop $chart testfileDone]
    if {$done} {
	$self.nextSentence flash
	return
    }
    set index [get-chart-prop $chart testfileIndex]
    incr index
    parse-testfile $testfile $index -parser $chart
}

proc prev-sentence {self} {
    set chart [get-window-prop $self chart]
    set testfile [get-chart-prop $chart testfile]
    if {$testfile == ""} {
	puts "Please call parse-testfile on a testfile."
	return
    }
    set index [get-chart-prop $chart testfileIndex]
    incr index -1
    if {$index == 0} {
	$self.prevSentence flash
	return
    }
    parse-testfile $testfile $index -parser $chart
}

proc display-fschart {chart {self ""} {start ""}} {
    global defaultWindows
    global abbreviatedFSChart \
	    showLinearFSChart showConstraintsFSChart
    if {[lsearch -exact $defaultWindows "fschart"] == -1} {return}
    if {[string match "(Graph)*" $chart]} {
	set chartgraph $chart
	set chart [get_field $chartgraph chart]
    } else {
	set chartgraph [extract_chart_graph $chart]
	if {$chartgraph == ""} {return}
    }
    if {$self == ""} {
	if {$chart == ""} {
	    puts "Missing chart!"
	    print-stack
	    return
	}
	set self [chart-window-name $chart fschart]
	set-window-prop $self location upperright
    }
    delete-more-windows $self
    setup-fschartwindow $self
    set-window-prop $self data $chartgraph
    set topcanvas $self.value
    set canvas $topcanvas.1

    set fstype "F-structure chart"
    set window $self
    set bg [background-color $self]
    set frame $topcanvas.frame
    frame $frame -bg $bg -highlightthickness 0
    $topcanvas create window 0 0 -window $frame -anchor nw
    set cmenu $topcanvas.menu
    frame $cmenu -bg $bg -highlightthickness 0
    pack $cmenu -in $frame -anchor nw
    canvas $canvas -bg $bg -highlightthickness 0
    pack $canvas -in $frame -anchor nw -side top
    button $cmenu.label -text $fstype -font xleuifont -relief flat \
		-bg $bg -highlightthickness 0
    bind $cmenu.label <Control-Shift-Button-1> "inspect $chartgraph"
    bind $cmenu.label <Button-3> {
	puts ""
	puts "<Control-Shift-Button-1> inspects the f-structure"
    }
    pack $cmenu.label -in $cmenu -side left -anchor nw
    add-projection-buttons $chartgraph "" $fstype $window $cmenu
    set linear ""
    if {$showLinearFSChart} {set linear "-linear"}
    set constraints ""
    if {$showConstraintsFSChart} {set constraints "-showConstraints"}
    set abbreviated ""
    if {$abbreviatedFSChart} {set abbreviated "-abbreviated"}
    set sentence [get-chart-prop $chart sentence]
    display_fstructure $chartgraph $canvas "" $constraints \
	$abbreviated -sentence $sentence $linear -start $start
    set-window-prop $canvas choicevalues [get_choice_values $chartgraph]
    set-window-prop $canvas graph $chartgraph
    $canvas addtag all all ;# mark all items on canvas
    set bbox [$canvas bbox all] ;# get bounding box of items
    set width [lindex $bbox 2]
    set height [lindex $bbox 3]
    if {$width == ""} {set width 100}
    if {$height == ""} {set height 100}
    set command "$canvas itemconfigure current -stipple gray50"
    $canvas bind context <Any-Enter> $command
    set command "$canvas itemconfigure current -stipple \"\""
    $canvas bind context <Any-Leave> $command
    set-window-prop $canvas fschartcanvas 1
    $canvas configure -width $width -height $height
    # Add in the height of the projection buttons
    # (I don't know how to compute this)
    set maxheight [expr {$height+21}]
    $topcanvas configure -scrollregion [list 0 0 $width $maxheight]
    raise $self
}

proc add-projection-buttons {graph projpath fslabel window cmenu} {
    global defaultWindows
    set bg [background-color $window]
    set chart [get-window-prop [winfo toplevel $window] chart]
    foreach proj [get_projections $graph $projpath] {
	set newpath [concat $projpath $proj]
	set w [join [concat $window $proj] ""]
	set-window-prop $w chart $chart
	set command "display-fstructures $graph $w \{$newpath\} \{$fslabel\} 0 $chart"
	if {[string first "::*" $proj] != -1} {
	    button $cmenu.proj$proj -text "$proj" -font xleuifont \
		-bg $bg -highlightthickness 0 \
		-command $command
	} else {
	    button $cmenu.proj$proj -text "$proj" -font xleuifont \
		-bg $bg -highlightthickness 0 \
		-command $command
	}
	bind $cmenu.proj$proj <Button-3> {
	    puts ""
	    puts "Displays this projection."
	    print-button-command %W
	}
	pack $cmenu.proj$proj -in $cmenu -side left -anchor nw
	set path [join $newpath ""]
	if {[lsearch -exact $defaultWindows $path] != -1} {
	    eval $command
	}
    }  
}

proc redisplay-fschart {window} {
    set data [get-window-prop $window data]
    display-fschart $data $window
}

proc more-fschart {window avpair} {
    set data [get-window-prop $window data]
    set more [more-window $window]
    display-fschart $data $more $avpair
}

proc add-more-button {canvas avpair x y} {
    set period [string first "." $canvas]
    set window [winfo toplevel $canvas]
    set command "more-fschart $window $avpair"
    set more $canvas.more
    button $more -text "more" -font xleuifont -command $command
    bind $more <Button-3> {
	puts ""
	puts "Show more fstructure pieces."
	print-button-command %W
    }
    $canvas create window $x $y -anchor nw -window $more
}

proc print-stipple {window} {
    set stipple [$window itemcget current -stipple]
    puts "stipple = $stipple"
}

proc more-window {window} {
    set version [more-version $window]
    if {$version == 0} {
	set new ${window}more1
    } else {
	set last [string last $version $window]
	set prefix [string range $window 0 [expr {$last - 1}]]
	incr version
	set new ${prefix}$version
    }
    set-window-prop $new chart [get-window-prop $window chart]
    set-window-prop $new location [get-window-prop $window location]
    return $new
}

proc more-version {window} {
    set first [string first "more" $window]
    if {$first == -1} {return 0}
    return [string range $window [expr {$first + 4}] end]
}

# DISPLAYING THE FSCHART CHOICES

set fsChoicesCommands {
    {command -label "Resize Window" \
	-command "resize-canvas $self.value" \
	-accelerator "<r>" \
	-doc "Toggles resizing window to fit choices."}
    {command -label "Clear Selection" \
	-command "unmark-solution-choices $self" \
	-doc "Clear the selection."}
    {command -label "Refactor" \
	-command "refactor-graph $self"
        -doc "Attempt to find an alternative encoding of the choices"}
}

set fsChoicesViews {
    {checkbutton -label "abbreviate attributes" -font xleuifont \
	 -variable "showOnlyChoicesArray(abbreviated)" \
	 -command "redisplay-chartchoices $self" \
	 -accelerator "<a>" \
	 -doc "Toggles where attributes are abbreviated." \
	 -doc "When attributes are abbreviated, only attributes listed" \
	 -doc "in the variable abbrevAttributes are displayed." \
	 -default "set showOnlyChoices all"}
    {checkbutton -label "constraints" -font xleuifont \
	 -variable "showConstraintsChoices" \
	 -command "redisplay-chartchoices $self" \
	 -accelerator "<c>" \
	 -doc "Toggles display of negative and sub-c constraints." \
	 -default "set showConstraintsChoices 0"}
    {checkbutton -label "disjunctions only" -font xleuifont \
	 -variable "showOnlyChoicesArray(disjunctions)" \
	 -command "redisplay-chartchoices $self" \
	 -accelerator "<d>" \
	 -doc "Toggles display of disjunctions only (no constraints)." \
	 -default "set showOnlyChoices all"}
    {checkbutton -label "OT marks only" -font xleuifont \
	 -variable "showOnlyChoicesArray(OTmarks)" \
	 -command "redisplay-chartchoices $self" \
	 -accelerator "<o>" \
	 -doc "Only shows optimality marks." \
	 -default "set showOnlyChoices all"}
    {checkbutton -label "subtrees" -font xleuifont \
	 -variable "showSubTrees" \
	 -command "redisplay-chartchoices $self" \
	 -accelerator "<s>" \
	 -doc "Toggles display of subtree rules used." \
	 -default "set showSubTrees 0"}
    {checkbutton -label "unoptimal" -font xleuifont \
	 -variable "showUnoptimal" \
	 -command "set-unoptimal current $self" \
	 -accelerator "<u>" \
	 -doc "Toggles the inclusion of unoptimal solutions." \
	 -default "set-unoptimal 0 $self"}
    {checkbutton -label "weights" -font xleuifont \
	 -variable "showWeights" \
	 -command "redisplay-chartchoices $self" \
	 -accelerator "<w>" \
	 -doc "Toggles the inclusion of feature weights." \
	 -default "set showWeights 0"}
    {separator}
    {radiobutton -label "flat choices" -font xleuifont \
	 -variable "showNested" \
	 -value "flat" \
	 -command "redisplay-chartchoices $self" \
	 -doc "Shows each disjunction on a separate line." \
	 -default "set showNested rentrant"}
    {radiobutton -label "nested choices" -font xleuifont \
	 -variable "showNested" \
	 -value "nested" \
	 -command "redisplay-chartchoices $self" \
	 -doc "Shows disjunctions nested under the disjunction(s) in which they occur." \
	 -default "set showNested reentrant"}
    {radiobutton -label "re-entrant choices" -font xleuifont \
	 -variable "showNested" \
	 -value "reentrant" \
	 -command "redisplay-chartchoices $self" \
	 -doc "Shows disjunctions with simple contexts nested." \
	 -doc "Shows disjunctions with complex contexts on separate lines." \
	 -default "set showNested reentrant"}
}

proc setup-fschartchoiceswindow {self} {
    global window_width window_height
    global fsChoicesViews fsChoicesCommands
    if {[winfo exists $self]==0} {
	set position [window-position $self]
	toplevel $self
	set-window-prop $self type choices
	wm minsize $self 200 300
	set moreversion [more-version $self]
	wm geometry $self ${window_width}x${window_height}$position
	set-window-prop $self position $position
	set color [highlight-color $self]
	$self configure -highlightcolor $color -highlightbackground $color \
		-highlightthickness 1
	set scrollable [scrollable-canvas $self.value $self]
	pack $scrollable -in $self -expand 1 -fill both -side bottom
	frame $self.panel
	pack $self.panel -in $self -anchor nw -fill x
	add-kill-button $self $self.panel
	if {$moreversion > 0} {return}
	add-enum-choices-buttons $self

	# Commands
	set cmenu $self.panel.commands.menu
	menubutton $self.panel.commands -text Commands \
	    -font xleuifont -menu $cmenu
	pack $self.panel.commands -in $self.panel -side left
	create-xle-menu $self $cmenu fsChoicesCommands
	
	# Views
	set omenu $self.panel.views.menu
	menubutton $self.panel.views -text Views \
	    -font xleuifont -menu $omenu
	pack $self.panel.views -in $self.panel -side left
	create-xle-menu $self $omenu fsChoicesViews

	tk_menuBar $self.panel $self.panel.commands $self.panel.views

	# View accelerators
	# create-accelerator-buttons $self $self.panel fsChoicesViews

    } else {
	clear-window $self
	focus-window $self 1
	wm title $self "fschartchoices"
    }
}

proc add-enum-choices-buttons {self} {
    button $self.prevChoiceSolution -text "prev" -font xleuifont
    bind $self.prevChoiceSolution <Button-3> {
	puts ""
	puts "<Button-1> causes the previous narrowed solution to be displayed."
	puts "<Shift Button-1> causes the previous solution to be displayed."
    }
    pack $self.prevChoiceSolution -in $self.panel -side left -anchor nw
    button $self.nextChoiceSolution -text "next" -font xleuifont 
    bind $self.nextChoiceSolution <Button-3> {
	puts ""
	puts "<Button-1> causes the next narrowed solution to be displayed."
	puts "<Shift Button-1> causes the next solution to be displayed."
    }
    pack $self.nextChoiceSolution -in $self.panel -side left -anchor nw

}

proc narrow-disjunction {choice button} {
    narrow_disjunction $choice
    set window [winfo toplevel $button]
    redisplay-chartchoices $window
}

proc narrow-choice {choice button} {
    narrow_choice $choice
    set value [get_field $choice selected]
    set nogood [get_field $choice nogood]
    highlight-button $button $value $nogood
    $button flash
    set window [winfo toplevel $button]
    set graph [get_field $choice graph]
    update-fschartchoice-title $window $graph
    redisplay-choices "." [get_field $graph chart]
}

# similar to the above, used for canvas text items.
proc narrow-choice-text {choice canvas} {
    narrow_choice $choice
    set graph [get_field $choice graph]
    redisplay-choices "." [get_field $graph chart]
    set window [winfo toplevel $canvas]
    update-fschartchoice-title $window $graph
}

proc mark-chosen-solution-choices {fstructure} {
    set chart [get_field $fstructure chart]
    if {$chart == ""} {return}
    if {[root_edge $chart] != [get_field $fstructure edge]} {return}
    set data [extract_chart_graph $chart]
    if {$data == ""} {return}
    unmark_all_choices $data
    if {[get_field $fstructure justifications] == ""} {
	mark_chosen_solution_choices $fstructure
    }
    redisplay-choices "." $chart
}

proc unmark-solution-choices {{window ""}} {
    if {$window == ""} {
	set window [default-window choices]
    }
    set data [get-window-prop $window data]
    if {$data == ""} {return}
    unmark_all_choices $data
    redisplay-choices "." [get_field $data chart]
    update-fschartchoice-title $window $data
}

proc refactor-graph {{graph ""}} {
    if {$graph == ""} {
	set window [default-window choices]
	set graph [get-window-prop $window data]
    }
    if {[string index $graph 0] == "."} {
	set graph [get-window-prop $graph data]
    }
    if {$graph == ""} {return}
    set refgraph [refactor_graph $graph]
    if {$refgraph==""} {
	set refgraph $graph
    }
    display-fschart $refgraph
    display-fschartchoices $refgraph
}

set showNested reentrant
set showUnoptimal 0

proc set-unoptimal {value {window ""}} {
    global showUnoptimal
    if {$value == "current"} {set value $showUnoptimal}
    if {$window == ""} {
	set chart [default-chart]
    } else {
	set chart [get-window-prop $window chart]
    }
    set showUnoptimal $value
    set recomputed [recompute_solutions $chart $showUnoptimal]
    if {$recomputed == 1} {
	show-solutions $chart
    }
}

proc display-disjunction {self disj disjunctions graph maxheight} {
    global showNested
    set maxwidth 0
    frame $self.choices -bd 1 -relief raised
    pack $self.choices -in $self -side left
    set choices [get_field $disj choices]
    foreach choice $choices {
	if {[get_field $choice skip] == "T"} {continue}
	set frame $self.choices.$choice
	frame $frame
	pack $frame -in $self.choices -anchor nw
	add-context-to-frame $choice $frame 2
	canvas $frame.constraints -bd 1 -relief raised
	set constraints [get-choice-constraints $choice]
	$frame.constraints create text 5 7 -text $constraints \
	    -font xlefsfont -anchor nw
	set bbox [$frame.constraints bbox all] ;# get bounding box of items
	set width [lindex $bbox 2]
	set height [lindex $bbox 3]
	if {$width == "" || $width < 100} {set width 100}
	if {$height == "" || $height < 15} {set height 15}
	$frame.constraints configure -width $width -height $height
	set maxheight [expr {$maxheight + $height + 8}]
	set maxwidth [max $maxwidth $width]
	pack $frame.constraints -in $frame -side top -anchor nw
	if {$showNested == "flat"} {continue}
	set printed 0
	foreach disj2 $disjunctions {
	    if {[get_field $disj2 internal] == "T"} {continue}
	    if {![embedded-disjunction $disj2 $choice]} {continue}
	    if {$maxheight > 20000} {
		return [list $maxwidth $maxheight $disj2]
	    }
	    if {$printed} {
		# add a line
		set line $frame.line$printed
		frame $line -bd 1 -relief raised -height 4
		pack $line -in $frame -anchor w -expand 1 -fill x
		$line configure -background black
		incr maxheight 6
	    }
	    incr printed
	    set frame2 $frame.$disj2
	    frame $frame2
	    set xy [display-disjunction $frame2 $disj2 $disjunctions \
			$graph $maxheight]
	    set maxwidth [max $maxwidth [lindex $xy 0]]
	    set maxheight [lindex $xy 1]
	    pack $frame2 -in $frame -side top -anchor nw
	    set breakdisj [lindex $xy 2]
	    if {$breakdisj != ""} {
		return [list $maxwidth $maxheight $breakdisj]
	    }
	}
    }
    return [list $maxwidth $maxheight ""]
}

proc embedded-disjunction {disj choice} {
    global showNested
    set context [get_field $disj context]
    if {$context == $choice} {return 1}
    if {$showNested == "reentrant"} {return 0}
    set type [get_field $context type]
    if {$type != "OR"} {return 0}
    foreach item [get_field $context items] {
	if {$item == $choice} {return 1}
    }
    return 0
}

proc get-choice-constraints {choice} {
    global showSubTrees showConstraintsChoices 
    global showOnlyChoices showWeights
    set subtrees ""
    if {$showSubTrees} {set subtrees "-subtrees"}
    set constraints ""
    if {$showConstraintsChoices} {set constraints "-showConstraints"}
    set weights ""
    if {$showWeights} {set weights "-showWeights"}
    set graph [get_field $choice graph]
    set constraints  [print_choice_constraints $graph $choice \
	    $subtrees $constraints $weights \
	    -showOnly $showOnlyChoices]
    set constraints [string trim $constraints]
    return $constraints
}

proc set-showOnlyChoices {} {
    global showOnlyChoices
    global showOnlyChoicesArray
    # See if a new view was selected
    foreach name [array names showOnlyChoicesArray] {
	if {$showOnlyChoicesArray($name) && $showOnlyChoices != $name} {
	    set showOnlyChoices $name
	    break
	}
    }
    # See if an view was deselected
    if {!$showOnlyChoicesArray($showOnlyChoices)} {
	set showOnlyChoices all
    }
    # Clear the array
    foreach name [array names showOnlyChoicesArray] {
	set showOnlyChoicesArray($name) 0
    }
    # Set the selection
    set showOnlyChoicesArray($showOnlyChoices) 1
}

proc OTorder {a b} {
    set a [lindex $a 2]
    set b [lindex $b 2]
    if {[expr {$a > $b}]} {return 1}
    if {[expr {$a < $b}]} {return -1}
    return 0
}

proc display-OTmarks {data frame} {
    global onColor
    set marks [gather_active_OT_marks $data]
    if {$marks == ""} {return [list 0 0]}
    set marks [lsort -command OTorder $marks]
    set maxheight 16
    set maxwidth 0
    set OTframe $frame.otmarks
    frame $OTframe
    pack $OTframe -in $frame -anchor nw
    set charwidth 8
    label $OTframe.label -text "OTOrder:"
    incr maxwidth [expr {8*$charwidth}]
    pack $OTframe.label -in $OTframe -side left -anchor nw
    set lastvalue -1
    while {$marks != ""} {
	set mark [lindex $marks 0]
	set name [lindex $mark 0]
	set value [lindex $mark 1]
	set origvalue [lindex $mark 2]
	set next [lindex $marks 1]
	set nextvalue [lindex $next 2]
	if {$origvalue == $nextvalue && $lastvalue != $value} {
	    set openparen $OTframe.open$name
	    label $openparen -text "("
	    pack $openparen -in $OTframe -side left -anchor nw
	    incr maxwidth [expr {2*$charwidth}]
	}
	set markbutton $OTframe.ot$name
	set color "black"
	set background "lightgrey"
	if {$value != $origvalue} {
	    set color "white"
	    set background "black"
	}
	set tag [lindex $mark 3]
	set showname ${tag}$name
	button $markbutton -text $showname -font xleuifont -bd 0 \
	    -fg $color -bg $background \
	    -command "set-OT-override $data $name $value $origvalue"
	pack $markbutton -in $OTframe -side left -anchor nw
	incr maxwidth [expr {([string length $name]+1)*$charwidth}]
	if {$origvalue == $lastvalue && $origvalue != $nextvalue} {
	    set closeparen $OTframe.close$name
	    label $closeparen -text ")"
	    pack $closeparen -in $OTframe -side left -anchor nw
	    incr maxwidth [expr {2*$charwidth}]
	}
	set lastvalue $origvalue
	set marks [lrange $marks 1 end]
    }
    return [list $maxwidth $maxheight]
}

proc set-OT-rank {name1 name2 {chart ""}} {
    global defaultchart
    if {$chart == ""} {
	set chart $defaultchart
    }
    return [set_OT_rank $chart $name1 $name2]
}

proc set-OT-override {data name value origvalue} {
    if {$value == $origvalue} {
	set value -1
    } else {
	set value $origvalue
    }
    if {[string match "(Graph)*" $data]} {
	set chart [get_field $data chart]
    }
    set_OT_override $data $name $value
    set graph [extract_chart_graph $chart]
    count_choice_solutions $graph narrowed
    choose-choice next $graph
}

proc display-fschartchoices {data {self ""} {start ""}} {
    global showNested defaultWindows showWeights
    if {[lsearch -exact $defaultWindows "choices"] == -1} {return}
    if {[string match "(Graph)*" $data]} {
	set chart [get_field $data chart]
    } else {
	set chart $data
	set data [extract_chart_graph $chart]
	if {$data == ""} {return}
    }
    if {$self == ""} {
       set self [chart-window-name $chart choices]
       set-window-prop $self location lowerright
    }
    if {$showWeights && 
	[string match "(Graph)*" $data] && 
	[get_field $data add_choice_feature_weights] != "T"} {
	set_graph_prop $data add_choice_feature_weights "T"
	disambiguate-window $self
	return
    }
    delete-more-windows $self
    setup-fschartchoiceswindow $self
    if {$data == ""} {return}
    update-fschartchoice-title $self $data

    set-window-prop $self data $data
    set disjunctions [get_field $data disjunctions]
    set maxwidth 0
    set maxheight 0
    set canvas $self.value
    frame $canvas.frame
    $canvas create window 0 0 -window $canvas.frame -anchor nw 
    set window $self
    set printed 0
    set xy [display-OTmarks $data $canvas.frame]
    set maxwidth [lindex $xy 0]
    set maxheight [lindex $xy 1]
    foreach disj $disjunctions {
	if {$start != "" && $start != $disj} {continue}
	set start ""
	if {[get_field $disj internal] == "T"} {continue}
	set context [get_field $disj context]
	if {$context == ""} {continue}
	set type [get_field $context type]
	set internal [get_field $context internal]
	if {$showNested != "flat" && $type == "CHOICE" && $internal != "T"} {continue}
	if {$showNested == "nested" && $type != "TRUE"} {continue}
	if {$maxheight > 20000} {
	    set more $canvas.more
	    button $more -text "more" -font xleuifont \
		-command "more-choices $data $window $disj"
	    bind $more <Button-3> {
		puts ""
		puts "Show more disjunctions."
		print-button-command %W
	    }
	    pack $more -in $canvas.frame -anchor nw
	    break
	}
	if {$printed} {
	    # add some space
	    set canvprt $canvas.$printed
	    frame $canvprt -bd 1 -relief raised
	    pack $canvprt -in $canvas.frame -anchor w -pady 8 -expand 1 -fill x
	    incr maxheight 16
	}
	incr printed
	set canvdisj $canvas.$disj
	frame $canvdisj
	pack $canvdisj -in $canvas.frame -anchor nw
	set contexts [add-context-to-frame $context $canvdisj 0 w]
	set xy [display-disjunction $canvdisj $disj $disjunctions $data \
		    $maxheight]
	set width [expr {[lindex $xy 0]+(70*($contexts+1))}]
	set maxwidth [max $maxwidth $width]
	set maxheight [lindex $xy 1]
	set breakdisj [lindex $xy 2]
	if {$breakdisj != ""} {
	    set more $canvas.more
	    button $more -text "more" -font xleuifont \
		-command "more-choices $data $window $breakdisj"
	    bind $more <Button-3> {
		puts ""
		puts "Show more disjunctions."
		print-button-command %W
	    }
	    pack $more -in $canvas.frame -anchor nw
	    if {$showNested != "flat"} {
		set showNested flat
		puts ""
		puts "A nested disjunction was too big to fit in a single window."
		puts "Nesting has been turned off so that the disjunctions"
		puts "can be displayed on multiple windows via the 'more' button."
		puts ""
	    }
	    break
	}
	    
    }

    if {![string match "*more*" $self]} {
	# Reconfigure choice buttons of root window only.
	bind $self.nextChoiceSolution <Button-1> "choose-choice next $data"
	bind $self.prevChoiceSolution <Button-1> "choose-choice prev $data"
	
	bind $self.nextChoiceSolution <Shift-Button-1> \
	    "choose-choice next $data nogood"
	bind $self.prevChoiceSolution <Shift-Button-1> \
	    "choose-choice prev $data nogood"
    }
    $canvas configure -scrollregion [list 0 0 $maxwidth $maxheight]
    raise $self
}

proc print-disjunctions {graph {file "stdout"}} {
    global showNested
    if {$file != "stdout"} {
	set file [open $file w]
    }
    set disjunctions [get_field $graph disjunctions]
    foreach disj $disjunctions {
	if {[get_field $disj internal] == "T"} {continue}
	set context [get_field $disj context]
	set type [get_field $context type]
	set internal [get_field $context internal]
	if {$showNested != "flat" \
		&& $type == "CHOICE" && $internal != "T"} {continue}
	if {$showNested == "nested" && $type != "TRUE"} {continue}
	puts $file ""
	print-context $context $file
	puts $file ""
	print-disjunction $disj $disjunctions 1 $file
    }
    if {$file != "stdout"} {
	close $file
	puts "Done."
    }
}

proc print-disjunction {disj disjunctions depth file} {
    set choices [get_field $disj choices]
    foreach choice $choices {
	indent $depth $file
	print-context $choice $file
	puts $file ""
	incr depth
	set constraints [get-choice-constraints $choice]
	if {$constraints != ""} {
	    foreach line [split $constraints "\n"] {
		indent $depth $file
		puts $file $line
	    }
	}
	set printed 0
	foreach disj2 $disjunctions {
	    if  {[get_field $disj2 internal] == "T"} {continue}
	    if {![embedded-disjunction $disj2 $choice]} {continue}
	    if {$printed} {
		indent $depth $file
		puts $file "------------------------"
	    }
	    incr printed
	    print-disjunction $disj2 $disjunctions $depth $file
	}
	incr depth -1
    }
}

proc indent {i file} {
    while {$i > 0} {
	puts -nonewline $file "  "
	incr i -1
    }
}

proc more-choices {chart window start} {
    set morewindow [more-window $window]
    display-fschartchoices $chart $morewindow $start
}

proc update-fschartchoice-title {self data} {
    global treesOnly
    set all [count_solutions $data]
    # set all [count_choice_solutions $data "all"]
    set title "$all solutions"
    if {$treesOnly} {return}
    set narrow [count_choice_solutions $data "narrowed"]
    if {$narrow != [optimal-count $all]} {
	set title "$title (narrowed to $narrow)"
    }
    wm title $self $title
}


set NGColor "gray66"		;# for nogoods

# invoked from the c function 'display_context'
proc add-context-node {canvas context xpos ypos font fontwidth} {
    global onColor offColor
    set type [get_field $context type]
    set color $offColor
    switch $type {
	CHOICE {
	    set label [get_field $context *label*]
	    set value [get_field $context selected]
	    if {$value == 1} {set color "$onColor"}
	    $canvas create text $xpos $ypos \
		-fill $color -anchor nw -font $font \
		-text $label -tags "context $context"
	    # the tag "context" is common to all such texts.  It is used in
	    # redisplay-choices to identify all the choices on the canvas
	    set command "choose-choice $context"
	    $canvas bind $context <Button-1> $command
	    set command "narrow-choice-text $context $canvas"
	    $canvas bind $context <Button-2> $command
	    $canvas bind $context <Button-3> {
		puts ""
		puts "Displays a solution."
	    }
	    return $label
	}
	TRUE {
	    $canvas create text $xpos $ypos \
		-fill $color -anchor nw -font $font \
		-text "True"
	    return "True"
	}
	AND {;# display as "a:1 b:2"
# This is now done inside the C code that invokes this script
	    set label ""
	    set first 1
	    foreach item [get_field $context items] {
		if {$first == 0} {
		    $canvas create text $xpos $ypos \
			-fill $color -anchor nw -font $font \
			-text " "
		    append label " "
		    incr xpos $fontwidth
		}
		set il [add-context-node $canvas $item $xpos $ypos\
			    $font $fontwidth]
		append label $il
		incr xpos [expr {[string length $il]*$fontwidth}]
		set first 0
	    }
	    return $label
	}
	OR  {;# display as "{a:1|a:2|b:1}"
# This is now done inside the C code that invokes this script
	    $canvas create text $xpos $ypos \
		 -fill $color -anchor nw -font $font \
		 -text "{"
	    set label "{"
	    incr xpos $fontwidth
	    set first 1
	    foreach item [get_field $context items] {
		if {$first == 0} {
		    $canvas create text $xpos $ypos \
			-fill $color -anchor nw -font $font \
			-text "|"
		    append label "|"
		    incr xpos $fontwidth
		}
		set il [add-context-node $canvas $item $xpos $ypos\
			    $font $fontwidth]
		append label $il
		incr xpos [expr {[string length $il]*$fontwidth}]
		set first 0
	    }

	    $canvas create text $xpos $ypos \
		 -fill $color -anchor nw -font $font \
		 -text "}"
	    append label "}"
	    return $label
	}
	NOT {;# display as "~[a:1 a:2]"
# This is now done inside the C code that invokes this script
	    $canvas create text $xpos $ypos \
		 -fill $color -anchor nw -font $font \
		 -text "~"
	    set label "~"
	    incr xpos $fontwidth
	    set next [get_field $context not_clause]
	    if {[get_field $next type] == "AND"} {
		$canvas create text $xpos $ypos \
		    -fill $color -anchor nw -font $font \
		    -text "\["
		append label "\["
		incr xpos $fontwidth
	    }
	    set il [add-context-node $canvas $item $xpos $ypos \
			$font $fontwidth]
	    append label $il
	    incr $xpos [expr {[string length $il]*$fontwidth}]
	    if {[get_field $next type] == "AND"} {
		$canvas create text $xpos $ypos \
		    -fill $color -anchor nw -font $font \
		    -text "\]"
		append label "\]"
	    }
	    return $label
	}
	default {error "unknown type: $type"}
    }
}

proc redisplay-chartchoices {window} {
    set-showOnlyChoices
    set data [get-window-prop $window data]
    display-fschartchoices $data $window
}

proc delete-more-windows {self} {
    if {[more-version $self] != 0} {return}
    set more $self
    while {1} {
	set more [more-window $more]
	if {![winfo exists $more]} {return}
	destroy $more
    }
}

proc new-window {window} {
    set id 0
    foreach child [winfo children $window] {
	set last [string last "." $child]
	set name [string range $child [expr {$last+1}] end]
	if {![regexp {^[0-9]+$} $name]} {continue}
	if {$name > $id} {set id $name}
    }
    incr id
    return $window.$id
}

proc choose-choice {choice {graph ""} {mode ""}} {
    if {$graph == ""} {
	set graph [get_field $choice graph]
    }
    set chart [get_field $graph chart]
    if {[choose_choice $choice $graph $mode]} {
	display-chosen-solution $graph
    } else {
	set window [chart-window-name $chart choices]
	if {[winfo exists $window] != 0} {
	    $window.nextChoiceSolution flash
	    $window.prevChoiceSolution flash
	}
    }
    redisplay-choices "." $chart
}

proc display-chosen-solution {{chart ""}} {
    global defaultchart
    if {[string match "(Graph)*" $chart]} {
	set graph $chart
	set chart [get_field $graph chart]
    } else {
	if {$chart == ""} {
	    set chart $defaultchart
	}
	set graph [extract_chart_graph $chart]
	if {![string match "(Graph)*" $graph]} {
	    puts "Bad graph: $graph"
	    print-stack
	    return
	}
    }
    set tree [get_chosen_tree $graph]
    if {$tree == ""} {set tree "(DTree)0"}
    if {[pruned_tree $tree]} {
	set edge [root_edge $chart]
	set tree [get_next_tree (DTree)0 $edge]
	show-trees $tree $chart
	return
    }
    set solution [get_chosen_fstructure $graph $tree]
    if {$tree != "(DTree)0"} {
	show-trees-only $tree
	update idletasks
	set treeid "[get_field $tree label]"
    } else {
	set treeid "Selected transfer result"
    }
    if {$solution != ""} {
	set fswindow [chart-window-name $chart fstructure]
	display-fstructures $solution $fswindow "" $treeid $tree
	display-other-structures $fswindow $solution
    }
}    

proc highlight-button {button value {nogood ""}} {
    global NGColor
    set color "black"
    set background [background-color $button]
    if {$value == 1} {
	set color white
	set background black
    }
    if {$nogood != ""} {
	set color $NGColor
    }
    set origColor [lindex [$button configure -foreground] 4]
    if {$origColor == "Black"} {set origColor black}
    set origBackground [lindex [$button configure -background] 4]
    if {$origColor != $color || $origBackground != $background} {
	$button configure -foreground $color
	$button configure -background $background
	raise [winfo toplevel $button]
    }
}

proc redisplay-choices {window {chart ""}} {
    global onColor offColor NGColor
    set choice [get-window-prop $window choice]
    if {$choice != ""} {
	set value [get_field $choice selected]
	set nogood [get_field $choice nogood]
	highlight-button $window $value $nogood
    }
    set changes 0
    foreach choicevalue [get-window-prop $window choicevalues] {
	set choice [lindex $choicevalue 0]
	set value [get_field $choice selected]
	if {$value == [lindex $choicevalue 1]} {continue}
	incr changes
    }
    if {$changes} {
	set contexts [$window find withtag "context"]
	set complexity [expr $changes * [llength $contexts]]
	if {$complexity < 1000000} {
	    foreach choicevalue [get-window-prop $window choicevalues] {
		set choice [lindex $choicevalue 0]
		set value [get_field $choice selected]
		if {$value == [lindex $choicevalue 1]} {continue}
		set nogood [get_field $choice nogood]
		set color $offColor
		if {$nogood != ""} {set color "$NGColor"}
		if {$value == 1}   {set color "$onColor"}
		foreach item [$window find withtag $choice] {
		    $window itemconfigure $item -fill $color
		}
	    }
	} else {
	    puts "Too many choices in the fschart window to change the colors"
	    puts "to reflect the current selection."
	}
	raise [winfo toplevel $window]
    }
    set graph [get-window-prop $window graph]
    if {$graph != ""} {
	set-window-prop $window choicevalues [get_choice_values $graph]
    }
	
    foreach child [winfo children $window] {
	if {$chart != ""} {
	    if {[get-window-prop $child chart] != $chart} {continue}
	}
	redisplay-choices $child
    }
}


proc new-choice-button {choice frame border} {
    global offColor onColor NGColor
    set label [get_field $choice *label*]
    set nogood [get_field $choice nogood]
    if {$nogood != ""} {
#	set label "${label}*"
    }
    set button [new-window $frame]
    set-window-prop $button choice $choice
    set color "black"
    set background "lightgrey"

    set value [get_field $choice selected]
    if {$value == 1} {
	set color white
	set background black
    }
    if {$nogood != ""} {
	set color $NGColor
    }

    button $button -text $label -font xleuifont -bd $border -fg $color\
	-bg $background

    bind $button <Button-1> "choose-choice $choice"
    bind $button <Control-Shift-Button-1> "inspect $choice"
    bind $button <Button-2> "narrow-choice $choice $button"
    if {$border > 0} {
	bind $button <Shift-Button-2> "narrow-disjunction $choice $button"
	bind $button <Button-3> {
	    puts ""
	    puts "<Button-1> displays a solution."
	    puts "<Button-2> masks this choice."
	    puts "<Shift Button-2> masks all the sisters of this choice."
	}
    } else {
	bind $button <Button-3> {
	    puts ""
	    puts "<Button-1> displays a solution."
	    puts "<Button-2> masks this choice."
	    print-button-command %W
	}
    }

    return $button
}

proc print-context {clause file} {
    set type [get_field $clause type]
    switch $type {
	CHOICE {
	    set label [get_field $clause *label*]
	    puts -nonewline $file $label
	}
	TRUE {
	    puts -nonewline $file "TRUE"
	}
	AND {
	    puts -nonewline $file "["
	    set first 1
	    foreach item [get_field $clause items] {
		if {!$first} {puts -nonewline " "}
		print-context $item $file
		set first 0
	    }
	    puts -nonewline $file "]"
	}
	OR {
	    puts -nonewline $file "{"
	    set first 1
	    foreach item [get_field $clause items] {
		if {!$first} {puts -nonewline $file " | "}
		print-context $item $file
		set first 0
	    }
	    puts -nonewline $file "}"
	}
	default {error "unknown type: $type"}
    }
}
   

proc add-context-to-frame {clause frame border {anchor "nw"}} {
    if {$clause == ""} {return 0}
    set type [get_field $clause type]
    set total 0
    switch $type {
	CHOICE {
	    set button [new-choice-button $clause $frame $border]
	    pack $button -in $frame -side left -anchor $anchor
	    return 1
	}
	TRUE {
	    set label [new-window $frame]
	    label $label -text "True" -font xleuifont
	    pack $label -in $frame -side left -anchor $anchor
	    return 1
	}
	AND {;# display as "[a:1 b:2]"
	    set label [new-window $frame]
	    label $label -text "[" -font xleuifont
	    pack $label -in $frame -side left -anchor $anchor
	    foreach item [get_field $clause items] {
		set count [add-context-to-frame $item $frame $border $anchor]
		incr total $count
	    }
	    set label [new-window $frame]
	    label $label -text "]" -font xleuifont
	    pack $label -in $frame -side left -anchor $anchor
	    return $total
	}
	OR  {;# display as "{a:1 | a:2 | b:1}"
	    set label [new-window $frame]
	    label $label -text "{" -font xleuifont -bd 0
	    pack $label -in $frame -side left -anchor $anchor
	    set first 1
	    foreach item [get_field $clause items] {
		if {$first == 0} {
		    set label [new-window $frame]
		    label $label -text "|" -font xleuifont -bd 0
		    pack $label -in $frame -side left -anchor $anchor
		}
		set count [add-context-to-frame $item $frame $border $anchor]
		incr total $count
		incr total 2
		set first 0
	    }
	    set label [new-window $frame]
	    label $label -text "}" -font xleuifont -bd 0
	    pack $label -in $frame -side left -anchor $anchor
	    return $total
	}
	NOT {;# display as "~[a:1 a:2]"
	    set label [new-window $frame]
	    label $label -text "~" -font xleuifont
	    pack $label -in $frame -side left -anchor $anchor
	    return [add-context-to-frame $item $frame $border $anchor]
	}
	default {error "unknown type: $type"}
    }
}
    
proc clause-label {clause} {
    if {$clause == ""} {return "nil"}
    set type [get_field $clause type]
    switch $type {
	CHOICE {
	    set result [get_field $clause *label*]
	}
	TRUE {return "True"}
	FALSE {return "False"}
	AND {
	    set result {[}
	    set first 1
	    foreach item [get_field $clause items] {
		if {!$first} {append result " "}
		set first 0
		append result [clause-label $item]
	    }
		append result {]}
	}
	OR {
	    set result "{"
	    set first 1
	    foreach item [get_field $clause items] {
		if {!$first} {append result "|"}
		set first 0
		append result [clause-label $item]
	    }
	    append result "}"
	}
	NOT {set item [clause-label [get_field $clause not_clause]]
	    set result "~"
	    append result $item}
	default {error "unknown type: $type"}
    }
     return $result
}
	
proc toggle-fs-selection {button tree fstructure} {
    if {[get_field $tree dummy] == "T"} {
	puts "You cannot lock a read-in solution."
	return
    }
    toggle_fs_selection $tree $fstructure
    set chart [get-window-prop [winfo toplevel $button] chart]
    show-trees-only [tree-root $tree] $chart
    set fswindow [chart-window-name $chart fstructure]
    display-fstructures $fstructure $fswindow "" [get_field $tree label] $tree
}

proc tree-root {node} {
    while {1} {
	set mother [get_field $node mother]
	if {$mother != ""} {
	    set node $mother
	} else {
	    return $node
	}
    }
}

###############################################################################

proc keep-solution {} {
    set fswindow [default-window fstructure]
    set graph [get-window-prop $fswindow data]
    set chart [get-window-prop $fswindow chart]
    set chartgraph [extract_chart_graph $chart]
    keep_selected_solution $graph $chartgraph
    return
}

set current_kept_solution ""
proc show-kept-solution {} {
    global current_kept_solution

    if {$current_kept_solution == ""} {
	set window [default-window choices]
	set chartgraph [get-window-prop $window data]
	set current_kept_solution $chartgraph
    }
    set current_kept_solution [get_next_kept_solution $current_kept_solution]
    if {$current_kept_solution != ""} {
	set chart [get_field $current_kept_solution chart]
	set window [chart-window-name $chart choices]
	unmark-solution-choices $window
	choose_solution $current_kept_solution
	display-chosen-solution $chart
	redisplay-choices "." $chart
    }
    return
}
