# (c) 2002-2006 by the Palo Alto Research Center.  All rights reserved.
# (c) 1996-2001 by the Xerox Corporation.  All rights reserved.  This is the
# TCL/TK interface to the XLE parser.  The commands exported by the XLE
# parser are in xle-interface.c.  TCL commands implemented in C use
# underscore rather than dash so that it is easier to search for the
# commands across .c and .tcl files.  trees.tcl has the tree-specific
# Tcl/Tk commands for xle, and fstructures.tcl has the fstructure-specific
# Tcl/Tk commands for xle.

#determine whether we are in OpenLook
# (this code does not run under Solaris)
#set olwm 0
#foreach item [split [exec ps -axc] \n] {
#    if [regexp (olwm|olvw) $item] {set olwm 1}
#}

set xle-initialized ""
set print_subtrees_file ""

#set xleFontMap("-adobe-courier-medium-r-normal--10-100*")	"Courier 10"
#set xleFontMap("-adobe-courier-medium-r-normal--12-120*")	"Courier 12"
#set xleFontMap("-adobe-courier-medium-r-normal--14-140*")	"Courier 14"
#set xleFontMap("-adobe-courier-medium-r-normal--18-180*")	"Courier 18"
#set xleFontMap("-adobe-times-bold-r-normal--10-100*")		"Times 10"
#set xleFontMap("-adobe-times-bold-r-normal--12-120*")		"Times 12"
#set xleFontMap("-adobe-times-bold-r-normal--14-140*")		"Times 14"

# This procedure is invoked via the TraceVar facility, whenever one of the
# fontname variables gets modified.
proc create-xle-font {xlename fontname} {
    set fontindex [lsearch -exact [font names] $xlename]
    if {$fontindex != -1} {
	font delete [lindex [font names] $fontindex]
    }
    set fontprop [font actual $fontname]
    set command  "font create $xlename $fontprop"
    eval $command
}

proc xle-initialization-script {} {
    global xle-initialized original_tk_scaling
    global gen_out_strings gen_out_msgs gen_out_format no_Tk

    if {${xle-initialized} != ""} {return}
    set xle-initialized 1
    set xle "done"
    puts stderr [xle-version]
    puts stderr "Type 'help' for more information."

    set gen_out_strings [open_gen_output "stdout"]
    set gen_out_msgs    [open_gen_output "stderr"]
    set gen_out_format  "abbrev"
    set-character-encoding stdio iso8859-1

    if {$no_Tk} {return}

    wm withdraw "."

    ;# the following was added to override the new defaults in Tk4.0
    option add *padX 3
    option add *padY 0
    # option add *background white

    set original_tk_scaling [tk scaling]
    tk scaling 1.0

    create-xle-font xleuifont   "-adobe-courier-medium-r-normal--12-120*"
    create-xle-font xlefsfont   "-adobe-courier-medium-r-normal--12-120*"
    create-xle-font xletextfont "-adobe-courier-medium-r-normal--12-120*"
    create-xle-font xletreefont "-adobe-courier-medium-r-normal--12-120*"
}

proc next-xle-focus {} {
    # Make sure that next-xle-focus is bound for all windows
    foreach child [winfo children .] {
	bind $child <F9> next-xle-focus
    }
    set windows [winfo children .]
    # Focus the next window
    set focus [focus]
    set found 0
    if {$focus == ""} {
	set found 1
    }
    foreach w $windows {
	if {$found} {
	    update idletasks
	    if {$focus == ""} {
		focus -force $w
	    } else {
		focus $w
	    }
	    update idletasks
	    raise $w
	    return
	}
	if {$w == $focus} {
	    set found 1
	}
    }
    # Focus the Tcl shell window
    foreach w $windows {
	lower $w
    }
    update idletasks
    focus_shell
}

proc xle-version {} {
    global xle_version
    return $xle_version
}

set constantCPUtime 0

proc CPUtime {command} {
    global constantCPUtime
    set start [CPU_clock]
    uplevel 1 $command
    set stop [CPU_clock]
    # constantCPUtime is useful for diffs on regression tests
    if {$constantCPUtime} {return 0}
    # The clock sometimes wraps around
    if {$stop < $start} {set stop [expr {$stop + pow(2,32)}]}
    return [expr {$stop - $start}]
}

proc % args {
    # This procedure is just used to eliminate an annoying problem
    # in Tcl, which is that if you type CR on an existing line,
    # Tcl interprets the % marker as a procedure.
    return [eval $args]
}

# MENU SUPPORT

proc noop {args} {
    return $args
}

proc create-xle-menu {self menu menuItemsVar} {
    # Creates an XLE menu given the window, the menu name, and
    # the name of the variable with the menu items.  The menu
    # items are used as input to "eval [concat .menu add $item]".
    # They can have multiple -doc "documentation" pairs at the end.
    # These are stripped by strip-documentation.
    upvar $menuItemsVar items
    menu $menu
    set n 1
    set panel $self.panel
    foreach item $items {
	set origitem $item
	set item [strip-documentation $item]
	if {[lindex $item 0] == "separator"} {
	    set command [concat $menu add $item]
	} else {
	    set command [concat $menu add $item -font xleuifont]
	}
	eval $command
	set key [menu-item-prop $item -accelerator]
	if {$key != ""} {
	    bind $self $key "$menu invoke $n"
	    if {$panel != "" && [lindex $item 0] == "checkbutton"} {
		set key [string range $key 1 1]
		set var [menu-item-prop $item -variable]
		set cmd [menu-item-prop $item -command]
		set cmd [eval "noop $cmd"]
		set cb [checkbutton $panel.$key -text $key -variable $var\
			   -command $cmd]
		pack $cb -in $panel -side left
		# document the item
		set doccmd ""
		while {$origitem != ""} {
		    if {[lindex $origitem 0] == "-doc"} {
			set doc "puts \"[lindex $origitem 1]\";"
			append doccmd $doc
		    }
		    set origitem [lrange $origitem 1 end]
		}
		append doccmd "puts \"Sets $var.\";"
		append doccmd "puts \"Calls $cmd.\";"
		append doccmd "puts \"\""
		bind $cb <Button-3> $doccmd
	    }
	}
	incr n
    }
    bind $menu <KeyPress-h> "print-menu-item-doc $menu $menuItemsVar"
}

proc create-accelerator-buttons {self menu menuItemsVar} {
    upvar $menuItemsVar items
    foreach item $items {
	set accelerator [menu-item-prop $item -accelerator]
	if {[string length $accelerator] != 3} {continue}
	set accelerator [string index $accelerator 1]
	set command [menu-item-prop $item -command]
	set variable [menu-item-prop $item -variable]
	set button ${menu}.${accelerator}
	set script "checkbutton $button -text $accelerator \
		-font xleuifont -variable $variable"
	set script [concat $script -command "\"" $command "\""]
	eval $script
	pack $button -in $menu -side left -anchor nw
	set label [menu-item-prop $item -label]
	set doc [concat "puts" "\{" "accelerator for the '$label' menu item" "\}"]
	bind $button <Button-3> "$doc"
    }
}

proc add-item-to-xle-menu {new_item menuItemsVar} {
    upvar $menuItemsVar items

    set items [lappend items $new_item]
    return ""
}

proc strip-documentation {item} {
    # Strips documentation from the end of the menu item.
    # It is important that menu items always have a backslash
    # before any carriage returns.  Otherwise, the 'eval' in
    # create-xle-menu won't work properly.
    set docIndex [string first -doc $item]
    if {$docIndex == -1} {return $item}
    set item [string range $item 0 [expr {$docIndex - 1}]]
    return $item
}

proc print-menu-item-doc {menu menuItemsVar} {
    # Prints the documentation for the current menu item.
    upvar $menuItemsVar menuItems
    set label [$menu entrycget active -label]
    if {$label == ""} {return}
    set found 0
    puts ""
    foreach menuItem $menuItems {
	if {$label != [menu-item-prop $menuItem -label]} {continue}
	set rest $menuItem
	while {$rest != ""} {
	    set first [lindex $rest 0]
	    set rest [lrange $rest 1 end]
	    if {$first != "-doc"} {continue}
	    puts [lindex $rest 0]
	    set found 1
	}
	set default [menu-item-prop $menuItem -default]
	if {$default != ""} {
	    puts "Default = $default."
	}
	set command [$menu entrycget active -command]
	if {$command != ""} {
	    puts "Calls $command."
	}
    }
    if {!$found} {puts "No documentation for $label in $menuItemsVar."}
}

proc menu-item-prop {item prop} {
    # Retrieves the list item that follows 'prop' in 'item'.
    while {$item != ""} {
	if {[lindex $item 0] == "$prop"} {return [lindex $item 1]}
	set item [lrange $item 1 end]
    }
    return ""
}

proc add-kill-button {window parent} {
    button $parent.kill -text "kill" \
	-font xleuifont -command [list destroy "$window"]
    bind $parent.kill <Button-3> {
	puts ""
	puts "Destroys window."
	print-button-command %W
    }
    pack $parent.kill -in $parent -side left -anchor nw
    # Add F9 binding here for convenience
    bind $window <F9> next-xle-focus
}

proc print-button-command {button} {
    set command [$button cget -command]
    if {$command == ""} {return}
    puts "Calls $command."
}

# PARSE COMMANDS

set defaultchart ""
set defaultparser ""
set defaultgenerator ""
set defaulttranslator ""
set grammarProps(grammar,prop) ""

proc create-parser {grammar} {
    global defaultparser grammarProps defaultchart
    if {$grammar != "" && [string index $grammar 0] != "/"} {
	set grammar "[pwd]/$grammar"
    }
    if {$grammar != ""} {
	puts "loading $grammar..."
    }
    set time [CPUtime {set parser [create_parser $grammar default]}]
    #   temporary until figure out how to restart
    if {$parser == ""} {
	return $parser
    }
    if {$defaultparser == "" || 
	[get_field $defaultparser grammarfile] == $grammar} {
	set defaultparser $parser
	set defaultchart $parser
    }
    if {$grammar == ""} {return $parser}
    puts [format "%.2f CPU seconds" [expr {[lindex $time 0]/[clock_rate]}]]
    puts "$grammar loaded"
    puts "Grammar last modified on [grammar_date $parser]."
    set grammarProps($parser,filename) $grammar
    # check_grammar_disjuncts $parser
    return $parser
}

set prolog-chart-file ""

proc parse {sentence {parser ""}} {
    ;# this is the user interface to the parser
    global defaultparser defaultgenerator prolog-chart-file
    if {$parser == ""} {
	set parser $defaultparser
    }
    if {$parser == "" && $defaultgenerator != ""} {
	puts "No default parser defined, creating one from the default generator."
	set file [get_field $defaultgenerator grammarfile]
	set parser [create-parser $file]
    }
    if {$parser == ""} {
	puts "Can't parse without a grammar."
	return
    }
    if {[string match "(Chart)*" $parser] == 0} {
	puts "$parser is not a valid parser."
	return
    }
    ;# extract the performance info, if any
    set info [strip-performance-data $sentence]
    set sentence [lindex $info 0]
    reset_storage $parser
    puts "parsing \{$sentence\}"
    set count [parse-sentence $sentence $parser ${prolog-chart-file}]
    set message [get_chart_prop $parser "statistics"]
    puts $message
    show-solutions $parser
    return $count
}

proc parse-sentence {sentence parser {outputFile ""}} {
    global defaultchart test_extract parse_literally
    global sum-print-subtrees print_subtrees print_subtrees_file
    clear-all $parser
    reset_storage $parser
    set rootcat ""
    if {!$parse_literally} {
	set sentence [string trim $sentence]
	set savesent $sentence
	# extract the root category, if any
	set words [split $sentence]
	set first [lindex $words 0]
	if {[string match *: $first]} {
	    # the first word ends in a colon, subtract it from sentence
	    set colon [string first ":" $sentence]
	    incr colon -1
	    set rootcat [string range $sentence 0 $colon]
	    set rootcat [string trim $rootcat]
	    if {[valid_category $rootcat $parser]} {
		set colon [expr {$colon+2}]
		set sentence [string range $sentence $colon end]
		set sentence [string trim $sentence]
	    } else {
		set rootcat ""
	    }
	}
	if {[string compare $sentence ""] == 0} {
	    if {[string compare $rootcat ""] == 0} {
		puts "error: empty sentence"
	    } else {
		set rootcat ""
		set sentence $savesent
	    }
	}
    }
    set defaultchart $parser
    set-unoptimal 0

    if {${sum-print-subtrees}} {
	set print_subtrees ${sum-print-subtrees}
	if {$print_subtrees_file == ""} {
	    set print_subtrees_file "subtrees.txt"
	}
    }
    set count [execute_parser $parser $sentence $rootcat]
    set-chart-prop $parser word_count [count-words $sentence]
    if {$test_extract && $count > 0 && [extract_chart_graph $parser] == ""} {
	# set count -4;
    }
    if {${outputFile} != ""} {
	print-prolog-chart-graph ${outputFile}
    }
    if {${sum-print-subtrees}} {
	sum-print-subtrees $print_subtrees_file
    }
    return $count
}

proc parse-word {word {rootcat ""} {parser ""}} {
    global defaultchart defaultparser
    if {$parser == ""} {
	set parser $defaultparser
    }
    if {$parser == ""} {
	puts "Can't parse without a grammar."
	return
    }
    if {[string match "(Chart)*" $parser] == 0} {
	puts "$parser is not a valid parser."
	return
    }
    reset_storage $parser
    clear-all $parser
    set defaultchart $parser
    set-unoptimal 1
    set time [CPUtime {set count [parse_word $parser $word $rootcat]}]
    puts [format "%s solutions, %.2f CPU seconds, %s subtrees unified" $count [expr {[lindex $time 0]/[clock_rate]}] [count_chart_subtrees $parser]]
    show-solutions $parser
    return $count
}

proc defaultParseFileProc {sentence parser n parseData} {
    set outputFile "$parseData/S$n.pl"
    set count [execute_parser $parser $sentence]
    set-chart-prop $parser word_count [count-words $sentence]
    print-most-probable-structure $parser $outputFile
    puts -nonewline "."
}

proc parse-file {file args} {
    global defaultparser
    # Extract the arguments
    set parser ""
    set parseProc ""
    set parseData ""
    set outputPrefix ""
    if {[llength $args] == 1} {
	set outputPrefix $args
	set args ""
    }
    while {$args != ""} {
	set arg [lindex $args 0]
	set args [lrange $args 1 end]
	set var ""
	if {$arg == "-parseProc"} {
	    set var parseProc
	} elseif {$arg == "-parser"} {
	    set var parser
	} elseif {$arg == "-parseData"} {
	    set var parseData
	} elseif {$arg == "-outputPrefix"} {
	    set var outputPrefix
	}
	if {$var == ""} {
	    puts "Unknown argument key: $arg"
	    return
	}
	set arg [lindex $args 0]
	if {$arg != "-parseProc" &&
	    $arg != "-parser" &&
	    $arg != "-parseData" &&
	    $arg != "-outputPrefix"} {
	    eval "set $var $arg"
	    set args [lrange $args 1 end]
	}
    }
    if {$parser == ""} {
	set parser $defaultparser
	if {$parser == ""} {
	    puts "Please specify a parser."
	    return
	}
    }
    if {$outputPrefix != "" && $parseData != ""} {
	puts "ERROR: Cannot specify -outputPrefix and -parseData together."
	return
    }
    set stream [open_c_file $file]
    if {$stream == ""} {
	puts "ERROR: Could not open $file."
	return
    }
    if {$parseProc == "" && $outputPrefix != ""} {
	set parseProc defaultParseFileProc
	set parseData $outputPrefix
	set outputPrefix ""
	puts -nonewline "Parsing $file to $parseData "
    } else {
	if {$parseProc == ""} {
	    puts "parse-file requires a parseProc or an outputPrefix."
	    return
	}
	puts -nonewline "Parsing $file with $parseProc "
    }
    # Process the sentences in the file
    set n 0
    while {1} {
	incr n
	reset_storage $parser
	set sentence [next_sentence_in_stream $stream $parser]
	if {$sentence == ""} {break}
	set-chart-prop $parser file $file
	$parseProc $sentence $parser $n $parseData
    }
    close_c_file $stream
    puts "done."
}

proc make-testfile {file {testfile ""} {parser ""}} {
    global defaultparser
    if {$parser == ""} {
	set parser $defaultparser
	if {$parser == ""} {
	    puts "Please specify a parser."
	    return
	}
    }
    if {$testfile == ""} {
	set testfile $file.new
    }
    break_text_file $parser $file $testfile
}

proc parse-lattice {filename {parser ""} {outputFile ""}} {
    global defaultchart defaultparser
    global sum-print-subtrees print_subtrees print_subtrees_file
    if {$parser == ""} {
	set parser $defaultparser
    }
    if {$parser == ""} {
	puts "Can't parse without a grammar."
	return
    }
    if {[string match "(Chart)*" $parser] == 0} {
	puts "$parser is not a valid parser."
	return
    }
    reset_storage $parser
    clear-all $parser
    set defaultchart $parser
    set-unoptimal 0

    if {${sum-print-subtrees}} {
	set print_subtrees ${sum-print-subtrees}
	if {$print_subtrees_file == ""} {
	    set print_subtrees_file "subtrees.txt"
	}
    }
    set time [CPUtime {set count [parse_lattice $parser $filename]}]
    if {${sum-print-subtrees}} {
	sum-print-subtrees $print_subtrees_file
    }
    puts [format "%s solutions, %.2f CPU seconds, %s subtrees unified" $count [expr {[lindex $time 0]/[clock_rate]}] [count_chart_subtrees $parser]]
    show-solutions $parser
    return $count
}

proc parse-lattice-testfile {testfile {start ""} {stop ""}} {
    parse-testfile $testfile $start $stop -parseProc parse-lattice-test
}

proc parse-lattice-test {sentence parser n output} {
    reset_storage $parser
    set time [CPUtime {set count [parse_lattice $parser $sentence]}]
    set time [expr {[lindex $time 0]/[clock_rate]}]
    set nsubtrees [count_chart_subtrees $parser]
    return [list $count $time $nsubtrees]
}

proc parse-lattice-dir {dirname {parser ""}} {
    global defaultparser
    if {$parser == ""} {
	set parser $defaultparser
    }
    if {$parser == ""} {
	puts "Can't parse without a grammar."
	return
    }
    set files [glob ${dirname}/*.fsmfile]
    set files [lsort $files]
    if {$files == ""} {
	puts "There are no .fsmfiles in $dirname"
	return
    }
    foreach file $files {
	parse-lattice $file $parser
    }
}

proc make-testfile-from-lattice-dir {dir} {
    set files [glob ${dir}/*.fsmfile]
    set files [lsort $files]
    if {$files == ""} {
	puts "There are no .fsmfiles in $dirname"
	return
    }
    set outputFile "${dir}/lattice.testfile"
    set output [open $outputFile "w"]
    foreach file $files {
	set times [get_fsmfile_prop $file TIMES]
	puts $output "$file $times"
	puts $output ""
    }
    close $output
    puts "Made $outputFile."
}

set printStats 1
set testdisplay 0
set today [exec date +%D]
set version-control nil

proc new-file {file} {
    global version-control
    if {[file exists $file]} {
	if {[file size $file] == 0} {
	    remove_file $file
	} else {
	    # make a backup file
	    set files [glob -nocomplain "${file}.~*~"]
	    if {${version-control} == "t" ||
		(${version-control} == "nil" && $files != "")} {
		# make a numbered backup
		set last 0
		foreach filename $files {
		    set pattern "${file}.~%d~"
		    set matches [scan $filename $pattern number]
		    if {$matches != 1} {
			puts "problem matching $filename against $pattern"
			puts "matches == $matches"
			continue
		    }
		    if {$number > $last} {
			set last $number
		    }
		}
		incr last
		rename_file $file "${file}.~${last}~"
	    } else {
		# make a single backup
		rename_file $file "${file}~"
	    }
	}
    }
    return [open $file w]
}

set testPrologOutput 0

proc parse-testfile-keyword {arg} {
    if {$arg == "-outputPrefix"} {return "outputPrefix"}
    if {$arg == "-outputFile"} {return "outputFile"}
    if {$arg == "-goldPrefix"} {return "goldPrefix"}
    if {$arg == "-statsPrefix"} {return "statsPrefix"}
    if {$arg == "-parseProc"} {return "parseProc"}
    if {$arg == "-outputProc"} {return "outputProc"}
    if {$arg == "-parseData"} {return "parseData"}
    if {$arg == "-parser"} {return "parser"}
    if {$arg == "-mostProbable"} {return "mostProbable"}
    if {$arg == "-suppressLogFiles"} {return "suppressLogFiles"}
    if {$arg == "-writeFailures"} {return "writeFailures"}
    return ""
}

proc parse-testfile {sentenceFile args} {
    global defaultparser printStats testdisplay today
    global testPrologOutput
    global test_extract no_Tk thread_uncertainties constantCPUtime
    global parse_testfile_resource1 parse_testfile_resource2
    global development_mode parse_literally time_parser
    set orig_parse_literally $parse_literally
    set start ""
    set stop ""
    set outputPrefix ""
    set outputFile ""
    set goldPrefix ""
    set parseProc defaultParseProc
    set parseData ""
    set outputProc ""
    set statsPrefix $sentenceFile
    set parser $defaultparser
    set mostProbable 0
    set sel 0
    set suppressLogFiles 0
    set writeFailures 0
    # See if the first argument is for start
    set arg [lindex $args 0]
    if {[parse-testfile-keyword $arg] == ""} {
	set start $arg
	set args [lrange $args 1 end]
	# See if the second argument is for stop
	set arg [lindex $args 0]
	if {[parse-testfile-keyword $arg] == ""} {
	    set stop $arg
	    set args [lrange $args 1 end]
	}
    }
    # Process the remaining keyword arguments
    while {$args != ""} {
	set var [parse-testfile-keyword [lindex $args 0]]
	if {$var == ""} {
	    puts "ERROR: [lindex $args 0] is an invalid keyword for parse-testfile."
	    return
	}
	set args [lrange $args 1 end]
	if {$args != "" && [parse-testfile-keyword [lindex $args 0]] == ""} {
	    eval "set $var [lindex $args 0]"
	    set args [lrange $args 1 end]
 	} else {
	    eval "set $var 1"
	}
   }
    if {$parser == "" && $parseProc == "defaultParseProc"} {
	puts "Can't parse without a grammar."
	return
    }
    if {$parse_testfile_resource1 != "seconds" &&
	$parse_testfile_resource1 != "events" &&
	$parse_testfile_resource1 != "megabytes"} {
	puts "parse_testfile_resource1 cannot be $parse_testfile_resource1."
	set parse_testfile_resource1 seconds
    }
    if {$parse_testfile_resource2 != "subtrees" &&
	$parse_testfile_resource2 != "percent_used_subtrees" &&
	$parse_testfile_resource2 != "raw_subtrees"} {
	puts "parse_testfile_resource2 cannot be $parse_testfile_resource2."
	set parse_testfile_resource2 subtrees
    }
    if {$outputPrefix != "" && $outputFile != ""} {
	puts "ERROR: you cannot use -outputPrefix and -outputFile."
	return
    }
    if {$outputFile != "" && $outputProc == ""} {
	puts "ERROR: you must use -outputProc when using -outputFile."
    }
    clear-all $parser
    set-chart-prop $parser testfile $sentenceFile
    set fileId [open $sentenceFile]
    set encoding [get-character-encoding $sentenceFile]
    if {$encoding == ""} {
	set encoding [get_file_encoding $sentenceFile]
    }
    if {$encoding == ""} {
	set encoding [get_field $parser grammar_encoding]
    }
    if {$encoding == ""} {
	set encoding [canonical_encoding iso8859-1]
    }
    fconfigure $fileId -encoding $encoding
    set newName ${statsPrefix}.new
    set errorName ${statsPrefix}.errors
    set statsName ${statsPrefix}.stats
    set max 0
    set n 0
    set nerrors 0
    set nmismatches 0
    set nsentences 0
    set modulus 0
    set absolutediff 0
    set absolutediff2 0
    set comparison 0
    set totaltime 0
    set totaloldtime 0
    set totalreftime 0
    set failed_prints 0
    set timeouts 0
    set timeouts_added 0
    set timeouts_removed 0
    set eventouts 0
    set eventouts_added 0
    set eventouts_removed 0
    set storageouts 0
    set storageouts_added 0
    set storageouts_removed 0
    set aborts 0
    set aborts_added 0
    set aborts_removed 0
    set zeros 0
    set zeros_added 0
    set zeros_removed 0
    set zerobangs 0
    set zerobang_errors 0
    set zerobang_errors_added 0
    set zerobang_errors_removed 0
    set stars 0
    set stars_added 0
    set stars_removed 0
    set skimmed 0
    set skimmed_added 0
    set skimmed_removed 0
    set skimmed_stars 0
    set gooddata ""
    set counts(good) 0
    set words(good) 0
    set subtrees(good) 0
    set times(good) 0
    set optimals(good) 0
    set suboptimals(good) 0
    set total(good) 0
    set maxdecade 0
    set max_events 0
    set total_events 0
    set total_subtrees 0
    set max_raw 0
    set max_raw_id 0
    set max_megabytes 0
    set max_megabytes_id 0
    set morph_tics 0
    set chart_tics 0
    set unifier_tics 0
    set completer_tics 0
    set solver_tics 0
    set-chart-prop $parser testfileDone 0
    set startTime [xle_time]
    # determine start and stop numbers
    if {$start == ""} {
	set startNum 1
	if {$stop == ""} {set stop "end"}
    } else {
	set isNum [scan $start "%d" startNum]
	if {$isNum} {set start ""} else {set startNum end}
    }
    if {$stop == "end"} {
	set stop ""
	set stopNum end
    } elseif {$stop == ""} {
	set stop $start
	set stopNum $startNum
    } else {
	set isNum [scan $stop "%d" stopNum]
	if {$isNum} {set stop ""} else {set stopNum end}
    }
    #determine whether to print
    set realStart $start
    if {$realStart == ""} {set realStart $startNum}
    set realStop $stop
    if {$realStop == ""} {set realStop $stopNum}
    if {$realStart != $realStop && !$suppressLogFiles} {
	set newFile [new-file $newName]
	set errorFile [new-file $errorName]
	set statsFile [new-file $statsName]
	fconfigure $newFile -encoding $encoding
	fconfigure $errorFile -encoding $encoding
	set print 1
	if {$test_extract} {
	    puts ""
	    puts "TESTING CHART EXTRACTION"
	    puts ""
	}
	if {$thread_uncertainties} {
	    puts ""
	    puts "THREADING UNCERTAINTIES"
	    puts ""
	}
	if {$development_mode} {
	    puts ""
	    puts "DEVELOPMENT MODE"
	    puts ""
	}
	puts "parsing sentences $realStart to $realStop in $sentenceFile..."
	if {$outputPrefix != ""} {
	    puts "storing results on $outputPrefix"
	    if {$goldPrefix != ""} {
		puts "comparing results against files on $goldPrefix"
	    }
	    if {$mostProbable == "packed"} {
		puts "printing most probable selection with packed solutions"
	    } elseif {$mostProbable == "only" || $mostProbable == 1} {
		puts "only printing most probable solutions"
	    } elseif {$mostProbable != 0} {
		puts "-mostProbable should be 'packed' or 'only'."
		puts "-mostProbable is being set to 'only'."
	    }
	}
	if {$outputFile != ""} {
	    puts "storing results in $outputFile."
	    if {$mostProbable == "only" || $mostProbable == 1} {
		puts "only using most probable solutions"
	    }
	}
	if {!$testdisplay} {close-all $parser}
    } else {
	set print 0
	set errorFile stdout
    }
    set comment 0
    while {1} {
	if {!$comment} {set sentence_id ""}
	set comment 0
	if {$parse_literally} {
	    set sentence [get-next-literal-sentence $fileId]
	} else {
	    set sentence [get-next-sentence $fileId]
	}
	if {[string compare $sentence "eof"] == 0} {
	    set-chart-prop $parser testfileDone 1
	    break}
	if {[string compare $sentence ""] == 0} {
	    if {$print} {puts $newFile $sentence}
	    continue}
	# See if we have reached a start or stop indicator.
	if {$start != ""} {
	    if {[string first $start $sentence] != -1} {
		set startNum [expr {$n + 1}]
	    }
	}
	if {$stop != "" && $stopNum == "end"} {
	    if {[string first $stop $sentence] != -1} {
		set stopNum [expr {$n + 1}]
	    }
	}
	# Skip over comments.
	set first [string index $sentence 0]
	if {!$parse_literally &&
	    [string compare $first "\#"] == 0} {
	    if {$print} {puts $newFile $sentence}
	    if {$print} {puts $newFile ""}
	    set sentence [string range $sentence 1 end]
	    set sentence [string trim $sentence]
	    set tokens [split $sentence]
	    set first [lindex $tokens 0]
	    if {$first == "SENTENCE_ID:"} {
		set sentence_id [lindex $tokens 1]
	    }
	    if {[string match "SENTENCE_ID:?*" $first]} {
		set sentence_id [string range $first 12 end]
	    }
	    if {([lindex $tokens 0] == "set" ||
		 [lindex $tokens 0] == "setx") &&
		[lindex $tokens 1] == "parse_literally"} {
		set parse_literally [lindex $tokens 2]
	    }
	    set comment 1
	    continue
	}
	incr n
	if {$n < $startNum} continue
	if {$n > $stopNum} break
	incr nsentences
	incr modulus
	if {$modulus == 50} {
	    puts "(parsing $sentenceFile)"
	    set modulus 0
	}
	set-chart-prop $parser testfileIndex $n
	;# extract the performance info, if any
	set origSentence $sentence
	set info [strip-performance-data $sentence]
	set sentence [lindex $info 0]
	set info [lindex $info 1]
	if {!$print && $parseProc == "defaultParseProc"} {
	    puts "parsing \{$sentence\}"
	}
	reset_storage $parser
	set CPUstart [CPU_clock]
	set newInfo [$parseProc $sentence $parser $n $parseData]
	if {$newInfo == ""} {continue}
	# determine the sentence id and results file
	set chart_id [sentence_id $parser]
	if {$chart_id != "" && $sentence_id == ""} {
	    set sentence_id $chart_id
	}
	if {$sentence_id == ""} {
	    set sentence_id [format "S%d" $n]
	}
	set_chart_prop $parser "sentence_id" $sentence_id
	set resultsFile ""
	if {$outputPrefix != ""} {
	    set id "$sentence_id.pl"
	    set resultsFile ${outputPrefix}${id}
	    remove_file $resultsFile
	}
	set sentence_id ""
	set count [lindex $newInfo 0]
	set time [lindex $newInfo 1]
	set nsubtrees [lindex $newInfo 2]
	set raw_subtrees [get_field $parser num_subtrees]
	if {$raw_subtrees > $max_raw} {
	    set max_raw $raw_subtrees
	    set max_raw_id $n
	}
	set morph_tics1 [expr {[get_field $parser morph_tics]/1000}]
	set chart_tics1 [expr {[get_field $parser chart_tics]/1000}]
	set unifier_tics1 [expr {[get_field $parser unifier_tics]/1000}]
	set completer_tics1 [expr {[get_field $parser completer_tics]/1000}]
	set solver_tics1 [expr {[get_field $parser solver_tics]/1000}]
	set total_tics1 [expr {$morph_tics1 + $chart_tics1 + $unifier_tics1 + $completer_tics1 + $solver_tics1}]
	incr morph_tics $morph_tics1
	incr chart_tics $chart_tics1
	incr unifier_tics $unifier_tics1
	incr completer_tics $completer_tics1
	incr solver_tics $solver_tics1
	if {$count != -1 && $count != -2} {
	    set events [get_field $parser total_events]
	    if {$events == ""} {set events 0}
	    if {$events > $max_events} {
		set max_events $events
	    }
	    set total_events [expr $total_events + $events/1000.0]
	    set total_subtrees [expr $total_subtrees + $nsubtrees/1000.0]
	}
	set megabytes [get_field $parser megabytes_in_use]
	if {$megabytes > $max_megabytes} {
	    set max_megabytes $megabytes
	    set max_megabytes_id $n
	}
	set results [format "%s solutions, %.2f CPU seconds, %s subtrees unified" $count $time $nsubtrees]
	set-chart-prop $parser results $results
	if {$parse_testfile_resource2 == "raw_subtrees"} {
	    set nsubtrees [count_chart_subtrees $parser -raw]
	}
	if {$parse_testfile_resource2 == "percent_used_subtrees" && $nsubtrees} {
	    set used_subtrees [count_chart_subtrees $parser -used]
	    set nsubtrees [expr {$used_subtrees*1.0/$nsubtrees}]
	    # set nsubtrees [count_chart_subtrees $parser -used]
	}
	set count1 [optimal-count $count]	
	if {$count1 == 0} {incr zeros}
	if {$count1 == -1} {
	    incr timeouts
	}
	if {$count1 == -2} {
	    incr storageouts
	}
	if {$count1 == -3} {
	    # the user aborted the parse
	    incr aborts
	    puts ""
	    puts "If you want parse-testfile aborted, type y and CR."
	    puts "Otherwise, just type CR."
	    if {[gets stdin] == "y"} {
		puts "Aborting parse-testfile."
		break}
	}
	if {$count1 == -4} {
	    incr aborts
	}
	if {$count1 == -5} {
	    incr eventouts
	}
	if {$count1 <= -6} {
	    incr aborts
	}
	if {$testdisplay && $print} {show-solutions}
	set optimal [optimal-count $count]
	set subopt [unoptimal-count $count]
	set sel 0
	if {$resultsFile != "" || $outputFile != ""} {
		set sentence [get-chart-prop $parser sentence]
		set graph [extract_chart_graph $parser]
		if {$mostProbable != 0 && $optimal > 0} {
			set sel 1
			unmark_all_choices $graph
			select-most-probable-structure $graph
			if {$mostProbable != "packed"} {
				set tree [get_chosen_tree $graph -unmarked]
				if {$tree == ""} {set tree "(DTree)0"}
				set graph [get_chosen_fstructure $graph $tree]
			}
			set wfile [getx property_weights_file $parser]
			set_external_graph_prop $graph "property_weights_file" $wfile
		}
	        if {($graph == "" || $graph == "(Graph)0") && $writeFailures} {
		    set graph [dummy_chart_graph $defaultparser]
                }
		set file dummy
		if {$outputProc == ""} {
			if {$optimal > 0 || $writeFailures} {
				set file [print_graph_as_prolog $graph $resultsFile $sentence $sel]
			}
			} else {
				if {$outputFile != ""} {
					if {[catch "eof $outputFile"]} {
						set outputFile [open $outputFile w]
					}
					set file [$outputProc $parser $outputFile $n]
					} else {
						set file [$outputProc $parser $resultsFile $n]
					}
				}
				if {$graph == "(Graph)0" || $file == ""} {
					if {$print} {
						puts $errorFile "\# Unable to print output for sentence $n"
					}
					incr failed_prints
					set count -4
				}
			}
			set CPUstop [CPU_clock]
	# Sometimes the clock wraps around
	if {$CPUstop < $CPUstart} {set CPUstop [expr {$CPUstop + pow(2,32)}]}
	if {$parse_testfile_resource1 == "megabytes"} {
	    set time $megabytes
	} elseif {$parse_testfile_resource1 == "events"} {
	    set time [get_field $parser total_events]
	} elseif {$constantCPUtime} {
	    set time 0
	} else {
	    set time [expr {($CPUstop - $CPUstart)/[clock_rate]}]
	}
	if {$time > $max} {set max $time}
	set nwords [count-words $sentence $parser]
	set totaltime [expr {$totaltime + $time}]
	if {$info != ""} {set gold [lindex $info 3]} else {set gold ""}
	if {$gold != ""} {
	    set msg "%s (%s! %s %.2f %s)"
	    set output [format $msg $sentence $gold $count $time $nsubtrees]
	} else {
	    set msg "%s (%s %.2f %s)"
	    set output [format $msg $sentence $count $time $nsubtrees]
	}
	if {[ungrammatical-count $count]} {
	    incr stars
	}
	if {[skimmed-count $count]} {
	    incr skimmed
	}
	if {[ungrammatical-count $count] && [skimmed-count $count]} {
	    incr skimmed_stars
	}
	if {$gold == 0} {incr zerobangs}
	if {$gold == 0 && $count != 0} {incr zerobang_errors}
	set decade [expr {($nwords - 1)/10}]
	if {$decade > $maxdecade} {
	    set maxdecade $decade
	}
	if (![info exists total($decade)]) {
	    set counts($decade) 0
	    set times($decade) 0
	    set words($decade) 0
	    set subtrees($decade) 0
	    set optimals($decade) 0
	    set suboptimals($decade) 0.0
	    set total($decade) 0
	}
	set total(good) [expr {$total(good) + 1}]
	set total($decade) [expr {$total($decade) + 1}]
	if {$optimal > 0} {
	    lappend gooddata [list $time $nsubtrees $optimal $subopt $nwords]
	    set counts(good) [expr {$counts(good) + 1}]
	    set times(good) [expr {$times(good) + $time}]
	    set words(good) [expr {$words(good) + $nwords}]
	    set subtrees(good) [expr {$subtrees(good) + $nsubtrees}]
	    set optimals(good) [expr {$optimals(good) + $optimal}]
	    set suboptimals(good) [expr {$suboptimals(good) + $subopt}]
	    set counts($decade) [expr {$counts($decade) + 1}]
	    set times($decade) [expr {$times($decade) + $time}]
	    set words($decade) [expr {$words($decade) + $nwords}]
	    set subtrees($decade) [expr {$subtrees($decade) + $nsubtrees}]
	    set optimals($decade) [expr {$optimals($decade) + $optimal}]
	    set suboptimals($decade) [expr {$suboptimals($decade) + $subopt}]
      	}
	if {$print} {
	    puts $newFile $output
	    puts $newFile ""
	    flush $newFile
	}
	set wordlabel "word"
	if {$nwords != 1} {set wordlabel "words"}
	if {$info != ""} {
	    set comparison 1
	    set oldcount [lindex $info 0]
	    set oldtime [lindex $info 1]
	    set oldsubtrees [lindex $info 2]
	    set oldcount1 [optimal-count $oldcount]
	    if {$count1 == -1 && $oldcount != -1} {incr timeouts_added}
	    if {$count1 != -1 && $oldcount == -1} {incr timeouts_removed}
	    if {$count1 == -2 && $oldcount != -2} {incr storageouts_added}
	    if {$count1 != -2 && $oldcount == -2} {incr storageouts_removed}
	    if {$count1 == -5 && $oldcount != -5} {incr eventouts_added}
	    if {$count1 != -5 && $oldcount == -5} {incr eventouts_removed}
	    if {$count1 < -2 && $count1 != -5 && $oldcount1 >= -2} {
		incr aborts_added}
	    if {$count1 >= -2 && $oldcount1 < -2 && $oldcount1 != -5} {
		incr aborts_removed}
	    if {$count1 == 0 && $oldcount1 > 0} {incr zeros_added}
	    if {$count1 > 0 && $oldcount1 == 0} {incr zeros_removed}
	    if {[ungrammatical-count $count] && 
		![ungrammatical-count $oldcount]} {
		incr stars_added
	    }
	    if {![ungrammatical-count $count] && 
		[ungrammatical-count $oldcount]} {
		incr stars_removed
	    }
	    if {[skimmed-count $count] && 
		![skimmed-count $oldcount]} {
		incr skimmed_added
	    }
	    if {![skimmed-count $count] && 
		[skimmed-count $oldcount]} {
		incr skimmed_removed
	    }
	    if {$gold == 0} {
		if {$count1 == 0 && $oldcount1 > 0} {
		    incr zerobang_errors_removed
		}
		if {$count1 > 0 && $oldcount1 == 0} {
		    incr zerobang_errors_added
		}
	    }
	    if {$gold == ""} {
		set s "((%s) (%s %.2f %s) -> (%s %.2f %s) (%s $wordlabel))"
		set stats [format $s $n \
			$oldcount $oldtime $oldsubtrees \
			$count $time $nsubtrees $nwords]
	    } else {
		set s "((%s) (%s! %s %.2f %s) -> (%s! %s %.2f %s) (%s $wordlabel))"
		set stats [format $s $n \
			$gold $oldcount $oldtime $oldsubtrees \
			$gold $count $time $nsubtrees $nwords]
	    }
	    if {[expr {$time - $oldtime}] > $absolutediff} {
		set absolutediff [expr {$time - $oldtime}]
		set absoluteno $n
		set absolutetime $time
	    }
	    if {[expr {$oldtime - $time}] > $absolutediff2} {
		set absolutediff2 [expr {$oldtime - $time}]
		set absoluteno2 $n
		set absolutetime2 $time
	    }
	    set totalreftime [expr {$totalreftime + $time}]
	    set totaloldtime [expr {$totaloldtime + $oldtime}]
	    if {$printStats} {puts $stats}
	    report-error $errorFile $gold $oldcount $count \
		$sentence $origSentence $n
	} else {
	    set stats [format "((%s) (%s %.2f %s) (%s $wordlabel))" \
		    $n $count $time $nsubtrees $nwords]
	    set oldcount $count
	    set gold ""
	    if {$printStats} {puts $stats}
	}
	if {$resultsFile != "" && $goldPrefix != "" &&
	    $oldcount == $count && $count != 0} {
	    set goldFile "${goldPrefix}${id}"
	    set command "exec diff $resultsFile $goldFile"
	    set result [catch $command diff]
	    if {$result != 0} {
		puts "MISMATCH ON: $sentence (outputs differ on $id)"
		if {$print} {
		    puts $errorFile $output
		    puts $errorFile ""
		    puts $errorFile "\# MISMATCH: (outputs differ on $id)"
		    puts $errorFile ""
		    flush $errorFile
		}
		incr nmismatches
	    }
	}
	if {$print} {
	    if {$time_parser} {
		if {$total_tics1 == 0} {set total_tics1 1}
		set morph [expr {$morph_tics1*100.0/$total_tics1}]
		set chart [expr {$chart_tics1*100.0/$total_tics1}]
		set unifier [expr {$unifier_tics1*100.0/$total_tics1}]
		set completer [expr {$completer_tics1*100.0/$total_tics1}]
		set solver [expr {$solver_tics1*100.0/$total_tics1}]
		set msg "(morph = %.1f%% chart = %.1f%% unifier = %.1f%% completer = %.1f%% solver = %.1f%%)"
		set msg [format $msg $morph $chart $unifier $completer $solver]
		puts $statsFile $msg
	    }
	    puts $statsFile $stats	    
	    flush $statsFile
	}
	if {$testPrologOutput} {
	    set prefix [file rootname [file tail $sentenceFile]]
	    test-one-prolog-output $prefix $n
	}
    }
    close $fileId
    if {$outputFile != ""} {
	catch "close $outputFile"
    }
    if {$print} {
	puts "done parsing $sentenceFile"
	puts $statsFile ""
	puts [xle-version]
	puts $statsFile [xle-version]
	set message "Grammar = [get_field $parser grammarfile]."
	puts $message
	puts $statsFile $message
	set message "Grammar last modified on [grammar_date $parser]."
	puts $message
	puts $statsFile $message
	set machine "unknown"
	catch {set machine [exec sh -c hostname]}
	set message "Host machine is $machine."
	puts $message
	puts $statsFile $message
	set message "[pluralize $nsentences sentence s], [pluralize $nerrors error s], [pluralize $nmismatches mismatch es]"
	puts $message
	puts $statsFile $message
	if {$zeros || $zeros_added || $zeros_removed} {
	    set message "[pluralize $zeros sentence s] had 0 parses"
	    if {$zeros_added || $zeros_removed} {
		set message "$message (added $zeros_added, removed $zeros_removed)"
	    }
	    puts $message
	    puts $statsFile $message
	}
	if {$zerobangs} {
	    set sentences [pluralize $zerobangs sentence s]
	    set message "$sentences with 0!"
	    puts $message
	    puts $statsFile $message
	}
	if {$zerobang_errors || $zerobang_errors_added || 
	    $zerobang_errors_removed} {
	    set sentences [pluralize $zerobang_errors sentence s]
	    set message "$sentences with 0! have solutions"
	    if {$zerobang_errors_added || $zerobang_errors_removed} {
		set message "$message (added $zerobang_errors_added, removed $zerobang_errors_removed)"
	    }
	    puts $message
	    puts $statsFile $message
	}
	if {$stars || $stars_added || $stars_removed} {
	    set message [pluralize $stars "starred sentence" s]
	    if {$stars_added || $stars_removed} {
		set message "$message (added $stars_added, removed $stars_removed)"
	    }
	    puts $message
	    puts $statsFile $message
	}	    
	if {$skimmed || $skimmed_added || $skimmed_removed} {
	    set message [pluralize $skimmed "skimmed sentence" s]
	    if {$skimmed_added || $skimmed_removed} {
		set message "$message (added $skimmed_added, removed $skimmed_removed)"
	    }
	    set message "$message ($skimmed_stars with stars)"
	    puts $message
	    puts $statsFile $message
	}	    
	if {$timeouts || $timeouts_added || $timeouts_removed} {
	    set sentences [pluralize $timeouts sentence s]
	    set message "$sentences timed out"
	    if {$timeouts_added || $timeouts_removed} {
		set message "$message (added $timeouts_added, removed $timeouts_removed)"
	    }
	    puts $message
	    puts $statsFile $message
	}
	if {$eventouts || $eventouts_added || $eventouts_removed} {
	    set sentences [pluralize $eventouts sentence s]
	    set message "$sentences exceeded max_xle_events"
	    if {$eventouts_added || $eventouts_removed} {
		set message "$message (added $eventouts_added, removed $eventouts_removed)"
	    }
	    puts $message
	    puts $statsFile $message
	}
	if {$storageouts || $storageouts_added || $storageouts_removed} {
	    set sentences [pluralize $storageouts sentence s]
	    set message "$sentences ran out of storage"
	    if {$storageouts_added || $storageouts_removed} {
		set message "$message (added $storageouts_added, removed $storageouts_removed)"
	    }
	    puts $message
	    puts $statsFile $message
	}
	if {$aborts || $aborts_added || $aborts_removed} {
	    set sentences [pluralize $aborts sentence s]
	    set message "$sentences aborted for an unknown reason"
	    if {$aborts_added || $aborts_removed} {
		set message "$message (added $aborts_added, removed $aborts_removed)"
	    }
	    puts $message
	    puts $statsFile $message
	}
	if {$failed_prints} {
	    set sentences [pluralize $failed_prints sentence s]
	    set message "$sentences failed while printing output"
	    puts $message
	    puts $statsFile $message
	}
	set maxsolutions [get_chart_prop $parser max_solutions]
	if {$maxsolutions} {
	    set message "max_solutions = $maxsolutions"
	    puts $message
	    puts $statsFile $message
	}
	set timeout [get_chart_prop $parser timeout]
	if {$timeout} {
	    set message "timeout = $timeout"
	    puts $message
	    puts $statsFile $message
	}
	set max_xle_events [get_chart_prop $parser max_xle_events]
	if {$max_xle_events} {
	    set message "max_xle_events = $max_xle_events"
	    puts $message
	    puts $statsFile $message
	}
	set max_xle_scratch [get_chart_prop $parser max_xle_scratch_storage]
	if {$max_xle_scratch} {
	    set message "max_xle_scratch_storage = $max_xle_scratch MB"
	    puts $message
	    puts $statsFile $message
	}
	set prune [get_chart_prop $parser prune_subtree_cutoff]
	if {$prune != 0} {
	    set message "prune_subtree_cutoff = $prune"
	    puts $message
	    puts $statsFile $message
	}
	set prune [get_chart_prop $parser prune_subtree_location]
	if {$prune != 0} {
	    set message "prune_subtree_location = $prune"
	    puts $message
	    puts $statsFile $message
	}
	set max_raw_subtrees [get_chart_prop $parser max_raw_subtrees] 
	if {$max_raw_subtrees} {
	    set message "max_raw_subtrees = $max_raw_subtrees"
	    puts $message
	    puts $statsFile $message
	}
	set max_medial [get_chart_prop $parser max_medial_constituent_weight] 
	if {$max_medial} {
	    set message "max_medial_constituent_weight = $max_medial"
	    puts $message
	    puts $statsFile $message
	}
	set max_medial [get_chart_prop $parser max_medial2_constituent_weight] 
	if {$max_medial} {
	    set message "max_medial2_constituent_weight = $max_medial"
	    puts $message
	    puts $statsFile $message
	}
	set start_skimming_when_scratch_storage_exceeds [get_chart_prop $parser start_skimming_when_scratch_storage_exceeds]
	if {$start_skimming_when_scratch_storage_exceeds} {
	    set message "start_skimming_when_scratch_storage_exceeds = $start_skimming_when_scratch_storage_exceeds MB"
	    puts $message
	    puts $statsFile $message
	}
	set start_skimming_when_total_events_exceed [get_chart_prop $parser start_skimming_when_total_events_exceed]
	if {$start_skimming_when_total_events_exceed} {
	    set message "start_skimming_when_total_events_exceed = $start_skimming_when_total_events_exceed"
	    puts $message
	    puts $statsFile $message
	}
	set max_new_events_per_graph_when_skimming [get_chart_prop $parser max_new_events_per_graph_when_skimming]
	if {$max_new_events_per_graph_when_skimming} {
	    set message "max_new_events_per_graph_when_skimming = $max_new_events_per_graph_when_skimming"
	    puts $message
	    puts $statsFile $message
	}
	if {$max_megabytes} {
	    set message "maximum scratch storage per sentence = %.2f MB (\#$max_megabytes_id)"
	    set message [format $message $max_megabytes]
	    puts $message
	    puts $statsFile $message
	}
	if {$max_raw} {
	    set message "maximum raw subtrees per sentence = $max_raw (\#$max_raw_id)"
	    puts $message
	    puts $statsFile $message
	}
	if {$total_subtrees} {
	    set message "maximum event count per sentence = %d"
	    set message [format $message $max_events]
	    puts $message
	    puts $statsFile $message
	    set message "average event count per graph = %.2f"
	    set message [format $message [expr $total_events/$total_subtrees]]
	    puts $message
	    puts $statsFile $message
	}
	set total_tics [expr {$morph_tics + $chart_tics + $unifier_tics + $completer_tics + $solver_tics}]
	if {$total_tics != 0 && $parse_testfile_resource1 == "seconds"} {
	    set msg "morph = %.1f%%, chart = %.1f%%, unifier = %.1f%%, completer = %.1f%%, solver = %.1f%%"
	    set morph [expr {$morph_tics*100.0/$total_tics}]
	    set chart [expr {$chart_tics*100.0/$total_tics}]
	    set unifier [expr {$unifier_tics*100.0/$total_tics}]
	    set completer [expr {$completer_tics*100.0/$total_tics}]
	    set solver [expr {$solver_tics*100.0/$total_tics}]
	    set msg [format $msg $morph $chart $unifier $completer $solver]
	    puts $msg
	    puts $statsFile $msg
	}	
	if {$parse_testfile_resource1 == "megabytes"} {
	    set message "%.2f MB total, %.2f MB max"
	} elseif {$parse_testfile_resource1 == "events"} {
	    set message "%.2f events total, %.2f events max"
	} else {
	    set message "%.2f CPU secs total, %.2f CPU secs max"
	}
	set message [format $message $totaltime $max]
	puts $message
	puts $statsFile $message
	puts -nonewline $newFile "\# "
	if {$nerrors} {
	    puts -nonewline $newFile "$nerrors errors, "
	}
	if {$nmismatches} {
	    puts -nonewline $newFile "$nmismatches mismatches, "
	}
	puts $newFile "$nsentences sentences, $message ($today)"
	if {$totaloldtime && $parse_testfile_resource1 == "seconds"} {
	    set message [format "new time/old time = %.2f" \
			     [expr {$totalreftime/$totaloldtime}]]
	    puts $message
	    puts $statsFile $message
	}
	set message "elapsed time = %d seconds"
	set message [format $message [expr {[xle_time] - $startTime}]]
	puts $message
	puts $statsFile $message
	if {$parse_testfile_resource1 == "seconds"} {
	    if {$absolutediff} {
		set message "biggest increase = %.2f sec (\#%s = %.2f sec)"
		set message [format $message \
				 $absolutediff $absoluteno $absolutetime]
		puts $message
		puts $statsFile $message
	    } elseif {$comparison} {
		puts "no absolute increases"
	    }
	    if {$absolutediff2} {
		set message "biggest decrease = %.2f sec (\#%s = %.2f sec)"
		set message [format $message \
				 $absolutediff2 $absoluteno2 $absolutetime2]
		puts $message
		puts $statsFile $message
	    } elseif {$comparison} {
		puts "no decreases"
	    }
	}
	if {$counts(good) > 0} {
	    set ct $counts(good)
	    set ct ${ct}.0
	    puts "the stats of the following exclude failed sentences:"
	    # compute the means
	    set ms [expr {$subtrees(good)/$ct}]
	    set mt [expr {$times(good)/$ct}]
	    # compute the variances in a second pass
	    set vt 0
	    set vs 0
	    set crossdiff 0
	    foreach data $gooddata {
		set dt [expr [lindex $data 0] - $mt]
		set ds [expr [lindex $data 1] - $ms]
		set vt [expr $vt + ($dt * $dt)]
		set vs [expr $vs + ($ds * $ds)]
		set crossdiff [expr $crossdiff + ($dt * $ds)]
	    }
	    set vt [expr $vt/$ct]
	    set vs [expr $vs/$ct]
	    set crossdiff [expr $crossdiff/$ct]
	    set rh1 "  seconds"
	    set rh2 " subtrees"
	    if {$parse_testfile_resource1 != "seconds"} {
		set rh1 "resource1"
	    }
	    if {$parse_testfile_resource2 != "subtrees"} {
		set rh2 "resource2"
	    }
	    set h "  range parsed failed  words $rh1 $rh2     optimal  suboptimal"
	    puts $h
	    puts $statsFile $h
	    set format1 "%7s %6d %6d %6.2f %9.2f %9.2f %11.2f %11.2E"
	    set format2 "%7s %6d %6d %6.2f %9.2f %9.2f %11.2E %11.2E"
	    for {set i 0} {$i <= 100} {incr i} {
		if {$i == $maxdecade + 1} {
		    set i good
		    set range all
		} elseif {$i == 0} {
		    set range "1-10"
		} else {
		    set range "${i}1-[expr {$i+1}]0"
		}
		if {![info exists total($i)]} {
		    if {$i == "good"} {break}
		    continue
		}
		set ct $counts($i)
		set failed [expr {$total($i) - $ct}]
		if {$ct == 0} {
		    set message [format "%7s %6d %6d" $range $ct $failed]
		    puts $message
		    puts $statsFile $message
		    continue
		}
		set ct ${ct}.0
		set mw [expr {$words($i)/$ct}]
		set mst [expr {$subtrees($i)/$ct}]
		set mt [expr {$times($i)/$ct}]
		set mopt [expr {$optimals($i)/$ct}]
		set munopt [expr {$suboptimals($i)/$ct}]
		if {$mopt < 1000000} {
		    set format $format1
		} else {
		    set format $format2
		}
		set message [format $format $range $counts($i) $failed $mw $mt $mst $mopt $munopt]
		puts $message
		puts $statsFile $message
		if {$i == "good"} {break}
	    }
	    if {$parse_testfile_resource1 != "seconds"} {
		set message "(resource1 = $parse_testfile_resource1)"
		puts $message
		puts $statsFile $message
	    }
	    if {$parse_testfile_resource2 != "subtrees"} {
		set message "(resource2 = $parse_testfile_resource2)"
		puts $message
		puts $statsFile $message
	    }
	    # print the variance
	    if {$vt != 0 && $vs != 0} {
		set corr [expr ($crossdiff * $crossdiff)/($vs * $vt)]
		set message [format "%.2f of the variance in $parse_testfile_resource1 is explained by the number of $parse_testfile_resource2" $corr]
		puts $message
		puts $statsFile $message
	    }
	}
	if {$test_extract} {
	    puts $statsFile "TESTING CHART EXTRACTION"
	}
	if {$thread_uncertainties} {
	    puts $statsFile "THREADING UNCERTAINTIES"
	}
 	if {$development_mode} {
	    puts $statsFile "DEVELOPMENT MODE"
	}
	close $newFile
	close $errorFile
	close $statsFile
	puts "new testfile printed on $newName."
	puts "statistics printed on $statsName."
	if {$nerrors && $nmismatches} {
	    puts "errors and mismatches printed on $errorName."
	} elseif {$nerrors || $failed_prints} {
	    puts "errors printed on $errorName."
	} elseif {$nmismatches} {
	    puts "mismatches printed on $errorName."
	}
    }
    set parse_literally $orig_parse_literally
    if {$nsentences == 0} {
	puts "No sentences were parsed."
	return -1
    }
    if {$realStart != $realStop && !$print} {
	set message "%.2f CPU secs total, %.2f CPU secs max"
	set message [format $message $totaltime $max]
	puts $message
	set message "elapsed time = %d seconds"
	set message [format $message [expr {[xle_time] - $startTime}]]
	puts $message
    }
    if {$realStart == $realStop && !$no_Tk} {
	if {$sel} {
	    display-chosen-solution $parser
	    display-fschart $parser
	    display-fschartchoices $parser
	} else {
	    show-solutions $parser
	}
	set window [chart-window-name $parser fschart]
	if {[winfo exists $window]} {
	    set index [get-chart-prop $parser testfileIndex]
	    wm title $window "fschart for sentence $index"
	}
    }
    return [expr {$nerrors+$nmismatches}]
}

proc defaultParseProc {sentence parser n parseData} {
    reset_storage $parser
    set time [CPUtime {
	set count [parse-sentence $sentence $parser]
    }]
    set time [expr {[lindex $time 0]/[clock_rate]}]
    set nsubtrees [count_chart_subtrees $parser]
    return [list $count $time $nsubtrees]
}

proc count-subtree-features {sentence parser n parseData} {
    set results [defaultParseProc $sentence $parser $n $parseData]
    count_subtree_features $parser
    return $results
}	

proc pluralize {number noun suffix} {
    if {$number == 1} {
	# e.g. "1 sentence"
	return "1 $noun"
    } else {
	# e.g. "2 sentences"
	return "$number ${noun}${suffix}"
    }
}

proc get-next-sentence {fileId} {
    set sentence ""
    while {1} {
	set length [gets $fileId line]
	if {$length == -1} {
	    # we are at the end of the file
	    if {[string compare $sentence ""] == 0} {return eof}
	    break
	}
	# Extract sentences from test sets
	if {[string match "<tstset *>" $line]} {continue}
	if {[string match "<DOC *>" $line]} {continue}
	if {[string match "<seg>*</seg>" $line]} {
	    set sentence [string range $line 5 [expr {$length - 7}]]
	    break
	}
	if {[string compare [string trim $line] ""] == 0} {break}
	if {[string compare $sentence ""] == 0} {
	    set sentence $line
	} else {
	    set sentence "$sentence $line"
	}
    }
    if {[string compare $sentence "eof"] == 0} {
	# change the sentence slightly to avoid confusion
	return " eof"
    }
    return $sentence
}

proc unquote-blank-line {line} {
    # See if the line consists of whitespace followed by vertical bars.
    if {![quoted_blank_line $line]} {return $line}
    # Remove the last quote character
    set len [string length $line]
    return [string range $line 0 [expr $len - 2]]
}

proc get-next-literal-sentence {fileId} {
    set sentence ""
    set first 1
    while {1} {
	set length [gets $fileId line]
	if {$length == -1} {
	    # we are at the end of the file
	    if {[string compare $sentence ""] == 0} {return eof}
	    break
	}
	if {$line == "" && $sentence != ""} {break}
	set line [unquote-blank-line $line]
	if {$first} {
	    set sentence $line
	    set first 0
	} else {
	    set sentence "$sentence\n$line"
	}
    }
    if {[string compare $sentence "eof"] == 0} {
	# change the sentence slightly to avoid confusion
	return " eof"
    }
    return $sentence
}

proc strip-performance-data {sentence} {
    global parse_literally
    set info ""
    while {!$parse_literally} {
	set leftparen [string last "(" $sentence]
	set rightparen [string last ")" $sentence]
	if {$leftparen == -1} break
	if {$rightparen == -1} break
	set last [expr {[string length $sentence] - 1}]
	if {$rightparen == $last} {
	    set data [string range $sentence \
			  [expr {$leftparen+1}] [expr {$last-1}]]
	    set data [performance-data $data]
	    if {$data == 0} break;
	    if {$info == ""} {set info $data}
	    incr leftparen -1
	    set sentence [string range $sentence 0 $leftparen]
	    set sentence [string trim $sentence]
	} else {
	    break
	}
    }
    return [list $sentence $info]
}

proc print-tree-morphemes {chart file {id ""}} {
    set closefile 0
    if {[catch "eof $file"]} {
	puts "opening $file in print-tree-morphemes"
	if {[catch {set file [open $file w]} errors]} {
	    puts $errors
	    return ""
	}
	set closefile 1
    }
    set morphemes [get-tree-morphemes $chart]
    if {$morphemes == ""} {
	set morphemes "ERROR: NO TREE MORPHEMES FOR SENTENCE $id"
    }
    puts $file $morphemes
    puts $file ""
    flush $file
    if {$closefile} {close $file}
    return $file
}

proc performance-data {data} {
    # look for the gold standard (always followed by !)
    set index 0
    set item [lindex $data $index]
    if {[regexp  {^~?(\*|-)?[0-9]+(\+-?[0-9]+)?!$} $item] != 0} {
	set length [string length $item]
	incr length -2
	set gold [string range $item 0 $length]
	incr index
	set item [lindex $data $index]
    } else {
	set gold ""
    }
    # look for the actual number of solutions
    # set number {-?[0-9]+(\.[0-9]+E\+?[0-9]+)?}
    # set pattern "^~?\*$number(\+$number)$"
    if {[regexp {^~?\*?-?[0-9]+((\.[0-9]+)?E\+?[0-9]+)?(\+-?[0-9]+((\.[0-9]+)?E\+?[0-9]+)?)?$} $item] != 0} {
	set actual $item
    } else {
	return 0
    }
    #look for the time
    incr index
    set item [lindex $data $index]
    if {[regexp {^[0-9]+.?[0-9]*$} $item] != 0} {
	set time $item
    } else {
	return 0
    }
    # look for the number of subtrees
    incr index
    set item [lindex $data $index]
    if {[regexp {^[0-9]+} $item] != 0} {
	set subtrees $item
    } else {
	set subtrees ""
    }
    return [list $actual $time $subtrees $gold]
}

set word_separators " \t\n\r\u3000"

proc count-words {sentence {parser ""}} {
    global word_separators
    # split the sentence into tokens separated by white space
    set words [split $sentence $word_separators]
    set length 0
    # eliminate empty tokens or punctuation
    foreach word $words {
	# We use [string compare $word xx] == 0 instead of $word == xx
	# because of an Array Bounds Read in string_to_decimal
	# caused by words beginning with accented characters.
	if {[string length $word] == 0} {continue}
	if {[string compare $word ","] == 0} {continue}
	if {[string compare $word "."] == 0} {continue}
	if {[string compare $word ":"] == 0} {continue}
	if {[string compare $word ";"] == 0} {continue}
	incr length
    }
    # eliminate the root category, if any
    set root [lindex $words 0]
    set pos [expr {[string length $root] - 1}]
    set last [string index $root $pos]
    if {[string compare $last ":"] == 0} {
	set root [string range $root 0 [expr {$pos - 1}]]
	if {$parser != "" && [valid_category $root $parser]} {
	    incr length -1
	}
    }
    return $length
}

proc count-tokens {sentence {parser ""}} {
    global word_separators
    # split the sentence into tokens separated by white space
    set words [split $sentence $word_separators]
    set length 0
    # eliminate empty tokens
    foreach word $words {
	if {[string length $word] == 0} {continue}
	incr length
    }
    return $length
}

proc optimal-count {count} {
    if {[string index $count 0] == "~"} {
	set count [string range $count 1 end]
    }
    if {[string index $count 0] == "*"} {
	set count [string range $count 1 end]
    }
    set plus [string first "+" $count]
    if {$plus == -1} {return $count}
    if {[string index $count [expr {$plus - 1}]] == "E"} {
	set tail [string range $count [expr {$plus + 1}] end]
	set plus2 [string first "+" $tail]
	if {$plus2 == -1} {return $count}
	set plus [expr $plus + $plus2 + 1]
    }
    set count [string range $count 0 [expr {$plus - 1}]]
    return $count
}

proc unoptimal-count {count} {
    if {[string index $count 0] == "~"} {
	set count [string range $count 1 end]
    }
    if {[string index $count 0] == "*"} {
	set count [string range $count 1 end]
    }
    set plus [string first "+" $count]
    if {$plus == -1} {return 0}
    if {[string index $count [expr {$plus - 1}]] == "E"} {
	set tail [string range $count [expr {$plus + 1}] end]
	set plus2 [string first "+" $tail]
	if {$plus2 == -1} {return $count}
	set plus [expr $plus + $plus2 + 1]
    }
    set count [string range $count [expr {$plus + 1}] end]
    return $count
}

proc ungrammatical-count {count} {
    if {[string index $count 0] == "*"} {return 1}
    if {[string index $count 1] == "*"} {return 1}
    return 0
}

proc skimmed-count {count} {
    if {[string index $count 0] == "~"} {return 1}
    return 0
}

proc strip-unoptimal-count {count} {
    set skimmed ""
    set ungrammatical ""
    set optimal [optimal-count $count]
    if {[skimmed-count $count]} {set skimmed "~"}
    if {[ungrammatical-count $count]} {set ungrammatical "*"}
    return ${skimmed}${ungrammatical}${optimal}
}

proc report-error {errorFile gold count1 count2 sentence output n {printn 0}} {
    global ignoreUnoptimal
    upvar nerrors nerrors
    upvar nmismatches nmismatches
    set mismatch 0
    set error 0
    if {$ignoreUnoptimal} {
	set gold [strip-unoptimal-count $gold]
	set count1 [strip-unoptimal-count $count1]
	set count2 [strip-unoptimal-count $count2]
    }
    if {$gold != "" && $count2 != $gold} {
	set error 1
	incr nerrors
    }
    if {$count1 != $count2} {
	set mismatch 1
	incr nmismatches
    }
    if {$error && $mismatch} {
	set msg1 "ERROR AND MISMATCH ON:"
	set msg2 "($gold! $count1 -> $count2)"
    } elseif {$error} {
	set msg1 "ERROR ON:"
	set msg2 "($gold! -> $count2)"
    } elseif {$mismatch} {
	set msg1 "MISMATCH ON:"
	set msg2 "($count1 -> $count2)"
    }
    if {($error || $mismatch)} {
	if {$printn} {
	    puts "$n: $msg1 $sentence $msg2"
	} else {
	    puts "$msg1 $sentence $msg2"
	}
	if {$errorFile == "stdout" || $errorFile == "stderr"} {return}
	puts $errorFile $output
	puts $errorFile ""
	puts $errorFile "\# $msg1 $n $msg2"
	puts $errorFile ""
	flush $errorFile
    }
}

proc diff-testfiles {filename1 {filename2 ""}} {
    if {$filename2 == ""} {
	set filename2 $filename1
    }
    if {$filename1 == $filename2} {
	puts "Looking for errors on $filename1."
    } else {
	puts "Looking for mismatches between $filename1 and $filename2"
	puts "and looking for errors on $filename2."
    }
    set file1 [open $filename1]
    set file2 [open $filename2]
    set errorName ${filename2}.errors
    set errorFile [new-file $errorName]
    set n1 0
    set n2 0
    set n3 0
    set loc2 0
    set nerrors 0
    set nmismatches 0
    while {1} {
	# get the next sentence from file2
	set sent1 [get-next-sentence $file1]
	if {$sent1 == "eof"} {break}
	if {$sent1 == ""} {continue}
	if {[string index $sent1 0] == "\#"} {continue}
	incr n1
	set info1 [strip-performance-data $sent1]
	set sent1 [lindex $info1 0]
	set info1 [lindex $info1 1]
	if {$info1 == ""} {continue}

	# look for an equivalent sentence from file2
	# save the location in case we don't find one
	set loc2 [tell $file2]
	set orign2 $n2
	while {2} {
	    set sent2 [get-next-sentence $file2]
	    if {$sent2 == "eof"} {break}
	    if {$sent2 == ""} {continue}
	    if {[string index $sent2 0] == "\#"} {continue}
	    incr n2
	    set info2 [strip-performance-data $sent2]
	    set sent2 [lindex $info2 0]
	    set info2 [lindex $info2 1]
	    if {$info2 == ""} {continue}
	    if {$sent1 == $sent2} {break}
	}
	if {$sent2 == "eof"} {
	    # we did't find an equivalent sentence
	    puts "Skipping sentence $n1 in $filename1."
	    # reset file2 to where we were
	    seek $file2 $loc2
	    set n2 $orign2
	    # go to the next sentence in file1
	    continue
	}
	# report any skipped sentences
	if {$n2 == [expr {$orign2 + 2}]} {
	    puts "Skipping sentence [expr {$n2 - 1}] in $filename2."
	} elseif {$n2 > [expr {$orign2 + 2}]} {
	    puts "Skipping sentences [expr {$orign2 + 1}] to [expr {$n2 - 1}] in $filename2."
	}
	# report any differences
	set count1 [lindex $info1 0]
	set count2 [lindex $info2 0]
	set gold [lindex $info2 3]
	if {$n1 == $n2} {
	    set n $n1
	} else {
	    set n "($n1, $n2)"
	}
	incr n3
	report-error $errorFile $gold $count1 $count2 $sent1 $sent1 $n 1
    }
    # look for more skipped sentences
    set orign2 $n2
    while {2} {
	set sent2 [get-next-sentence $file2]
	if {$sent2 == "eof"} {break}
	if {$sent2 == ""} {continue}
	if {[string index $sent2 0] == "\#"} {continue}
	incr n2
    }
    if {$n2 == [expr {$orign2 + 1}]} {
	puts "Skipping sentence $n2 in $filename2."
    } elseif {$n2 > [expr {$orign2 + 1}]} {
	puts "Skipping sentences [expr {$orign2 + 1}] to $n2 in $filename2."
    }
    # clean things up
    close $file1
    close $file2
    close $errorFile
    puts "Compared $n3 sentences and found $nerrors errors and $nmismatches mismatches."
    set diff1 [expr {$n1 - $n3}]
    set diff2 [expr {$n2 - $n3}]
    if {$diff1} {
	if {$diff1 == 1} {set s ""} else {set s "s"}
	puts "Found $diff1 sentence$s in $filename1 not in $filename2."
    }
    if {$diff2} {
	if {$diff2 == 1} {set s ""} else {set s "s"}
	puts "Found $diff2 sentence$s in $filename2 not in $filename1."
    }
    if {$nerrors || $nmismatches} {
	puts "Errors and mismatches printed on $errorName."
    }
}

proc summarize-stats {statsFileName} {
    puts "summarizing stats in $statsFileName"
    set statsFile [open $statsFileName]
    set subtreesdiff 0
    set subtreesdiff2 0
    set printed 0
    set maxratio 0
    set minratio 1000
    set totalsubtrees 0
    set totaltime 0
    set maxratioID 0
    set minratioID 0
    set pattern1 "((%d) (%s %f %d) -> (%s %f %d) (%d words))"
    set pattern2 "((%d) (%s %s %f %d) -> (%s %s %f %d) (%d words))"
    while {1} {
	set length [gets $statsFile line]
	if {$length == -1} {break}
	set line [string trim $line]
	if {$line == ""} {continue}
	set matches [scan $line $pattern1 id c1 t1 st1 c2 t2 st2 w]
	if {$matches != 8} {
	    set matches [scan $line $pattern2 id g1 c1 t1 st1 g2 c2 t2 st2 w]
	    if {$matches != 10} {
		puts $line
		continue}
	}
	if {$t2 != 0 && $st2 > 100} {
	    set ratio [expr {$st2 / $t2}]
	    if {$ratio > $maxratio} {
		set maxratio $ratio
		set maxratioID $id
	    }
	    if {$ratio < $minratio} {
		set minratio $ratio
		set minratioID $id
	    }
	    incr totalsubtrees $st2
	    set totaltime [expr {$totaltime + $t2}]
	}
	if {$st1 == $st2} {continue}
	if {$printed == 0} {
	    puts "displaying stats where number of subtrees differ:"
	    set printed 1
	}
	puts $line
	if {$st2 > $st1} {
	    incr subtreesdiff
	} else {
	    incr subtreesdiff2
	}
    }
    if {$subtreesdiff || $subtreesdiff2} {
	set message "%s sentences increased subtrees, %s decreased"
	set message [format $message $subtreesdiff $subtreesdiff2]
	puts $message
    }
    if {$totaltime != 0} {
	set message "subtrees/second: %.0f (ave), %.0f (min = \#%s)"
	set averatio [expr {$totalsubtrees / $totaltime}]
	set message [format $message $averatio $minratio $minratioID]
	puts $message
	puts ""
    }
    close $statsFile
}

proc sort-stats {statsFileName {compareFn "subtrees-per-second"}} {
    puts "sorting stats in $statsFileName"
    set statsFile [open $statsFileName]
    set lines ""
    while {1} {
	set length [gets $statsFile line]
	if {$length == -1} {break}
	set line [string trim $line]
	if {$line == ""} {continue}
	set stat [canonical-stat $line]
	if {$stat == ""} {continue}
	lappend lines $line
    }
    close $statsFile
    set lines [lsort -command $compareFn $lines]
    set newFileName $statsFileName.sorted
    puts "storing sorted file on $newFileName"
    set newFile [new-file $newFileName]
    foreach line $lines {
	puts $newFile $line
    }
    close $newFile
}

proc subtrees-per-second {line1 line2} {
    set stat1 [canonical-stat $line1]
    set stat2 [canonical-stat $line2]
    set count1 [optimal-count [lindex $stat1 1]]
    set count2 [optimal-count [lindex $stat2 1]]
    if {$count1 == -1 && $count2 != -1} {
	return -1
    }
    if {$count1 != -1 && $count2 == -1} {
	return 1
    }
    set st1 [lindex $stat1 3]
    set sec1 [lindex $stat1 2]
    set ratio1 [expr {$st1/$sec1}]
    set st2 [lindex $stat2 3]
    set sec2 [lindex $stat2 2]
    set ratio2 [expr {$st2/$sec2}]
    if {1} {
	set cutoff 200
	if {$st1 < $cutoff && $st2 >= $cutoff} {return 1}
	if {$st2 < $cutoff && $st1 >= $cutoff} {return -1}
    }
    if {$ratio1 == $ratio2} {return 0}
    if {$ratio1 < $ratio2} {return -1}
    return 1
}

proc subtrees-per-word {line1 line2} {
    set stat1 [canonical-stat $line1]
    set stat2 [canonical-stat $line2]
    set ratio1 [expr {[lindex $stat1 3]/[lindex $stat1 4]}]
    set ratio2 [expr {[lindex $stat2 3]/[lindex $stat2 4]}]
    if {$ratio1 == $ratio2} {return 0}
    if {$ratio1 < $ratio2} {return 1}
    return -1
}

proc subtrees-per-word2 {line1 line2} {
    set stat1 [canonical-stat $line1]
    set stat2 [canonical-stat $line2]
    set words1 [lindex $stat1 4]
    set words2 [lindex $stat2 4]
    set ratio1 [expr {[lindex $stat1 3]/($words1 * $words1)}]
    set ratio2 [expr {[lindex $stat2 3]/($words2 * $words2)}]
    if {$ratio1 == $ratio2} {return 0}
    if {$ratio1 < $ratio2} {return 1}
    return -1
}

proc words-per-second {line1 line2} {
    set stat1 [canonical-stat $line1]
    set stat2 [canonical-stat $line2]
    set ratio1 [expr {[lindex $stat1 4]/[lindex $stat1 2]}]
    set ratio2 [expr {[lindex $stat2 4]/[lindex $stat2 2]}]
    if {$ratio1 == $ratio2} {return 0}
    if {$ratio1 < $ratio2} {return -1}
    return 1
}

proc seconds-per-sentence {line1 line2} {
    set stat1 [canonical-stat $line1]
    set stat2 [canonical-stat $line2]
    set sec1 [lindex $stat1 2]
    set sec2 [lindex $stat2 2]
    if {$sec1 == $sec2} {return 0}
    if {$sec1 < $sec2} {return 1}
    return -1
}

proc storage-per-sentence {line1 line2} {
    set stat1 [canonical-stat $line1]
    set stat2 [canonical-stat $line2]
    set sec1 [lindex $stat1 2]
    set sec2 [lindex $stat2 2]
    if {$sec1 == $sec2} {return 0}
    if {$sec1 < $sec2} {return 1}
    return -1
}

proc words-per-sentence {line1 line2} {
    set stat1 [canonical-stat $line1]
    set stat2 [canonical-stat $line2]
    set w1 [lindex $stat1 4]
    set w2 [lindex $stat2 4]
    if {$w1 == $w2} {return 0}
    if {$w1 > $w2} {return -1}
    return 1
}

proc subtrees-per-sentence {line1 line2} {
    set stat1 [canonical-stat $line1]
    set stat2 [canonical-stat $line2]
    set st1 [lindex $stat1 3]
    set st2 [lindex $stat2 3]
    if {$st1 == $st2} {return 0}
    if {$st1 > $st2} {return -1}
    return 1
}

proc solutions-per-sentence {line1 line2} {
    set stat1 [canonical-stat $line1]
    set stat2 [canonical-stat $line2]
    set n1 [optimal-count [lindex $stat1 1]]
    set n2 [optimal-count [lindex $stat2 1]]
    if {$n1 == $n2} {
	set n1 [lindex $stat1 3]
	set n2 [lindex $stat2 3]
	if {$n1 == $n2} {return 0}
    }
    if {$n1 > $n2} {return -1}
    return 1
}

proc subtree-ratio {line1 line2} {
    set ratio1 [get-subtree-ratio $line1]
    set ratio2 [get-subtree-ratio $line2]
    if {$ratio1 == $ratio2} {return 0}
    if {$ratio1 > $ratio2} {return 1}
    return -1
}

proc get-subtree-ratio {line} {
    set pattern2 "((%d) (%s %f %d) -> (%s %f %d) (%d words))"
    set matches [scan $line $pattern2 id c1 t1 st1 c2 t2 st2 w]
    if {$matches != 8} {return 0}
    if {$st1 == 0} {return 0}
    if {$st2 == 0} {return 0}
    return [expr {${st2} / (${st1} * 1.0)}]
}

proc time-ratio {line1 line2} {
    set ratio1 [get-time-ratio $line1]
    set ratio2 [get-time-ratio $line2]
    if {$ratio1 == $ratio2} {return 0}
    if {$ratio1 > $ratio2} {return -1}
    return 1
}

proc get-time-ratio {line} {
    set pattern2 "((%d) (%s %f %d) -> (%s %f %d) (%d words))"
    set matches [scan $line $pattern2 id c1 t1 st1 c2 t2 st2 w]
    if {$matches != 8} {return 0}
    if {$t1 == 0 || $t1 == 0.0} {return 0}
    if {$t2 == 0 || $t2 == 0.0} {return 0}
    return [expr {$t2 / $t1}]
}

proc canonical-stat {stat} {
    set pattern1 "((%d) (%s %f %f) (%d words))"
    set pattern2 "((%d) (%s %f %f) -> (%s %f %f) (%d words))"
    set pattern3 "((%d) (%s %s %f %f) -> (%s %s %f %f) (%d words))"
    set g2 ""
    set matches [scan $stat $pattern1 id c2 t2 st2 w]
    if {$matches != 5} {
	set matches [scan $stat $pattern2 id c1 t1 st1 c2 t2 st2 w]
	if {$matches != 8} {
	    set matches [scan $stat $pattern3 id c1 g1 t1 st1 c2 g2 t2 st2 w]
	    if {$matches != 10} {return ""}
	}
    }
    # avoid divide-by-zero errors
    if {$t2 == 0.00} {set t2 0.01}
    if {$w == 0} {set w 1}
    return [list $id $c2 $t2 $st2 $w]
}

# to diff nogood subtrees, execute the following in a shell:
# grep unifying subtrees1.txt | grep '*' | cut -f3 -d' '  > nogood1
# grep unifying subtrees2.txt | grep '*' | cut -f3 -d' '  > nogood2
# diff nogood1 nogood2 | more

# SHOWING SOLUTIONS

proc show-solutions {{chartdata ""}} {
    global defaultchart no_Tk
    global defaultWindows
    if {$no_Tk} {return}
    if {$chartdata == ""} {set chartdata $defaultchart}
    set root [root_edge $chartdata]
    if {$root != ""} {
	extract_chart_graph $chartdata
	set solutions [count_solutions $chartdata]
	set solutions [optimal-count $solutions]
	if {[lsearch -exact $defaultWindows "tree"] != -1} {
	    show-sample-tree $root
	}
	if {[expr {$solutions > 0}]} {
	    display-fschart $chartdata
	    display-fschartchoices $chartdata
	}
    } else {
	show-morph $chartdata
    }
}

proc show-input {{chart ""}} {
    if {[string index $chart 0] == "."} {
	set chart [get-window-prop $chart chart]
    }
    if {$chart == ""} {set chart [default-chart]}
    if {[get_field $chart mode] == "ChartParsing"} {
	puts [get-chart-prop $chart sentence]
    } else {
	set graph [get_field $chart goal_graph]
	set window [chart-window-name $chart input]
	display-fschart $graph $window
	wm title $window "generator input"
    }
}

proc extract-sentence {tree {display ""}} {
    if {$tree == ""} {return ""}
    set edge [get_field $tree edge]
    set chart [get_field $edge chart]
    set root [root_edge $chart]
    if {$root != "" && $root != $edge} {return ""}
    if {$display != ""} {
	return [get_field $chart display_sentence]
    }
    return [get_field $chart sentence]
}

proc close-all {{chart ""}} {
    global no_Tk
    if {$no_Tk} {return}
    foreach child [winfo children .] {
	if {$chart != "" && [get-window-prop $child chart] != $chart} {
	    continue
	}
	destroy $child
    }
    update idletasks
}

proc close-all-except {ignore} {
    global no_Tk
    if {$no_Tk} {return}
    foreach child [winfo children .] {
	if {[get-window-prop $child chart] == $ignore} {continue}
	destroy $child
    }
    update idletasks
}

proc clear-all {{chart ""}} {
    global windowProps chartProps currentFSIDarray
    global no_Tk
    if {$no_Tk} {return}
    set chart [original-chart $chart]
    kill-inspectors $chart
    foreach child [winfo children .] {
	set chart2 [get-window-prop $child chart]
	if {$chart != "" && [original-chart $chart2] != $chart} {continue}
	if {[string first "-" $chart2] != -1} {
	    # destroy windows of duplicate charts
	    destroy $child
	    continue
	}
	set type [get-window-prop $child type]
	if {$type == "bracket"} {
	    destroy $child
	    continue
	}
	if {$type == "chart" || $type == "morph"} {
	    destroy $child
	    continue
	}
	if {$type == "fstructure" && [winfo exists $child.next]} {
	    $child.next configure -command ""
	    $child.prev configure -command ""
	}
	clear-data-props $child
	clear-window $child
	if {$type == ""} {
	    set type [string range $child 1 end]
	}
	wm title $child $type
    }
    if {$chart != ""} {
	return
    }
    unset windowProps
    set windowProps(dummy) ""
    unset chartProps
    set chartProps(dummy) ""
    if {[info exists currentFSIDarray]} {
	unset currentFSIDarray
    }
}

proc clear-window {self} {
    ;# clear the window
    global windowProps
    global no_Tk
    if {$no_Tk} {return}
    if {[winfo exists $self]==0} {return}
    if {[winfo exists $self.value]==0} {
	return
    }
    if {[winfo class $self.value] == "Canvas"} {
	$self.value addtag all all
	$self.value delete all
    }
    foreach w [winfo children $self.value] {destroy $w}
    # set-window-prop $self orig-geometry ""
    update idletasks
}

proc clear-data-props {self} {
    # clear any references to C data structures
    # to avoid dangling references
    global no_Tk
    if {$no_Tk} {return}
    set-window-prop $self data ""
    set-window-prop $self tree ""
    set-window-prop $self edge ""
    set-window-prop $self choice ""
    set-window-prop $self choicevalues ""
    set-window-prop $self graph ""
    foreach child [winfo children $self] {
	clear-data-props $child
    }
}

proc kill-inspectors {{chart ""}} {
    global no_Tk
    if {$no_Tk} {return}
    foreach w [winfo children .] {
	if {[winfo class $w] != "Inspector"} {continue}
	set wchart [get-window-prop $w chart]
	set wchart [original-chart $wchart]
	if {$chart != "" && $wchart != "" && $chart != $wchart} {continue}
	destroy $w
    }
}

# CHART NAVIGATION

proc find-mothers {{chart ""}} {
    global defaultparser
    if {$chart == ""} {set chart $defaultparser}
    propagate_motherhood $chart
}

# FILE INPUT/OUTPUT

proc print-chart-graph {{file "*"} {chart ""}} {
    global defaultparser
    if {$chart == ""} {set chart $defaultparser}
    if {$file == "*"} {set file [default-filename "fschart" "lfg" $chart]}
    set time [CPUtime {set file [print_chart_graph $chart $file]}]
    puts [format "%.2f CPU seconds" [expr {[lindex $time 0]/[clock_rate]}]]
    if {$file == ""} {return}
    puts "fschart printed on $file."
}

proc print-prolog-chart-graph {{file "*"} {chart ""} {selected 0} {sttx 0}} {
    global defaultparser
    set window ""
    if {[string index $chart 0] == "."} {
	set window $chart
	set chart ""
    }
    if {$chart == ""} {
	if {$window == ""} {set window [default-window fschart]}
	set chart [get-window-prop $window data]
	if {$chart == ""} {
	    set chart [get-window-prop $window chart]
	}
    }
    if {$chart == ""} {set chart $defaultparser}
    if {$file == "*"} {
	set filename [default-filename "fschart" "pl" $chart]
    } else {
	set filename $file
    }
    
    if {[string match "(Graph)*" $chart]} {
	print_graph_as_prolog $chart $filename "" $selected $sttx
    } else {
	set sentence [get-chart-prop $chart sentence]
	set filename [print_chart_graph $chart $filename \
			  -prolog $sentence $selected $sttx]
    }

    if {$file == "*"} {
	puts "fschart printed on $filename."
    }
}

proc read-prolog-chart-graph {file {chart ""}} {
    global prologgraph no_Tk
    if {$chart == ""} {reset_prolog_storage}
    set graph [read_prolog_graph $file $chart]
    if {$graph == ""} {return}
    set chart [get_field $graph chart]
    set prologgraph $graph
    set sentence [get_field $graph sentence]
    if {$no_Tk} {return $graph}
    display-fschart $graph
    display-fschartchoices $graph
    display-chosen-solution $graph
    return $graph
}

proc read-treebank-file {sentence_id} {
    if {[string match "*.pl" $sentence_id]} {
	set file $sentence_id
    } else {
	set file ${sentence_id}-fschart.pl
    }
    read-prolog-chart-graph $file
}

proc sort-prolog-graphs {file} {
    reset_prolog_storage
    read_list_of_prolog_graphs $file ${file}.sorted
}

proc default-filename {prefix suffix {object ""}} {
    global default_print_dir
    
    # determine the directory
    
    set dir $default_print_dir
    if {$dir == ""} {
	set dir [pwd]
    }
    set length [string length $dir]
    incr length -1
    if {$length >= 0 && [string index $dir $length] != "/"} {
	set dir "${dir}/"
    }
    
    # determine the name
    
    set name ""
    if {$object != ""} {
	set name [get_field $object filename]
	if {![string match "*.$suffix" $name]} {
	    set name ""
	}
	if {$name != ""} {
	    if {$dir != "" && [string first "/" $name] == -1} {
		set name $dir$name
	    }
	    return $name
	}
	set name [sentence_id $object]
    }

    # determine the full file name
    
    if {$name == ""} {
	return [next-file $dir$prefix $suffix]
    } elseif {$prefix == "fschart" && $suffix == "pl"} {
	return $dir$name.$suffix
    } else {
	return [next-file $dir$name-$prefix $suffix]
    }
}

proc next-file {file suffix} {
    set i 0
    while {1} {
	incr i
	set fullname $file$i.$suffix
	if {![file exists $fullname]} {return $fullname}
    }
}

proc print-solutions {format file lexid {sorted ""}} {
    global defaultparser
    set edge [root_edge $defaultparser] 
    print_edge_solutions $edge $format $file $lexid $sorted
}

proc print-rule-network {rule {file ""}} {
    global defaultchart
    set string "String"
    set grammar $defaultchart
    print_rule_net $grammar $rule $file 
}

# prints nets for all rules 
proc print-grammar-nets {{file ""}} {
    global defaultgenerator
    set string "String"
    set grammar $defaultgenerator
    print_grammar_nets $grammar $file 
  }

#print an rule in LFG format
proc print-rule {rule {chart ""}} {
    global defaultchart
    if {$chart == ""} {
	set chart $defaultchart
    }
    print_rule $chart $rule
}

#print an lfg entry in LFG format
proc print-lex-entry {headword {cat ""} {chart ""}} {
    global defaultchart
    if {$chart == ""} {
	set chart $defaultchart
    }
    if {[string match "(Chart)*" $chart] == 0} {
	puts "$chart is not a valid chart."
	return
    }
    print_lex_entry $chart $headword $cat
}

proc print-prolog-grammar {args} {
    global defaultchart
    set chart ""
    set file ""
    set cfg ""
    set lexentries ""
    set bodies ""
    foreach arg $args {
	if {[string match "(Chart)*" $arg]} {
	    set chart $arg
	} elseif {$arg == "-cfg"} {
	    set cfg "-cfg"
	} elseif {$arg == "-bodies"} {
	    set bodies "-bodies"
	} elseif {$arg == "-lexentries"} {
	    set lexentries "-lexentries"
	} elseif {$file == ""} {
	    set file $arg
	}
    }
    if {$chart == ""} {
	set chart $defaultchart
    }
    prolog_print_grammar $chart $file $cfg $lexentries $bodies
}

proc print-prolog-grammar-all {file {chart ""}} {
    global defaultchart
    if {$chart == ""} {
	set chart $defaultchart
    }
    prolog_print_grammar $chart $file -bodies
}

proc print-attribute-props {name {chart ""}} {
    global defaultchart
    if {$chart == ""} {
	set chart $defaultchart
    }
    print_attribute_props $name $chart
}

# REGRESSION TESTING

proc test-parser {grammarFile sentenceFile {contextualized 0}} {
    set name $sentenceFile
    set end [string first "-" $name]
    if {$end == -1} {
	set end [string first "." $name]
    }
    set exit 0
    if {$end != -1} {
	set name [string range $name 0 [expr {$end - 1}]]
    }
    set begin [string last "/" $name]
    if {$begin == -1} {
	set upname [string toupper $name]
    } else {
	set upname [string range $name [expr {$begin + 1}] end]
	set upname [string toupper $upname]
    }
    set count 0
    set errors 0
    set timingFile "${name}-timings.txt"
    set timingId [open $timingFile w]
    ;# establish a benchmark for how fast the computer is running
    ;# this was chosen to be about a second on a SPARC 10
    ;# but is now a constant
    ;# set benchmark [CPUtime {factorial 3600}]
    put-time $timingId "benchmark" "[clock_rate] microseconds per iteration"
    set timeMsg [CPUtime {set parser [create-parser $grammarFile]}]
    put-time $timingId "grammar" $timeMsg
    puts "testing $sentenceFile"
    set fileId [open $sentenceFile]
    while {1} {
	set sentence ""
	while {1} {
	    set length [gets $fileId line]
	    if {$length == -1} {break}
	    set line [string trim $line]
	    if {$line == ""} {break}
	    if {$sentence == ""} {
		set sentence $line
	    } else {
		set sentence "$sentence $line"
	    }
	}
	if {$length == -1 && $sentence == ""} break;
	if {$sentence == ""} {continue}
	set first [string index $sentence 0]
	if {$first == "\#"} {continue}
	if {$first == "!"} {
	    ;# this sentence is known to not match the gold standard
	    set sentence [string range $sentence 1 end]
	    set expected 1
	} else {
	    set expected 0
	}
	incr count 1
	;# extract the performance info, if any
	set info [strip-performance-data $sentence]
	set sentence [lindex $info 0]
	;# parse the sentence
	reset_storage $parser
	set timeMsg [CPUtime {parse-sentence $sentence $parser}]
	put-time $timingId S$count $timeMsg
	set lexid "S$count $upname"
	if {$contextualized} {
	    set file ${name}-S${count}.pl
	    print-prolog-chart-graph $file $parser
	} else {
	    set file ${name}-S${count}.lfg
	    print_edge_solutions [root_edge $parser] -lex $file $lexid -sorted
	}
	set slash [string last "/" $file]
	set directory ""
	if {$slash != -1} {
	    set directory [string range $file 0 $slash]
	    incr slash
	    set file [string range $file $slash end]
	}
	set command "exec diff ${directory}$file ${directory}gold/$file"
	set result [catch $command diff]
	if {$result != $expected} {
	    if {$exit == 0} {
		set errorFile "${name}-errors.txt"
		set errorId [open $errorFile w]
	    }
	    puts $errorId "error in $lexid:"
	    if {$expected == 1} {
		puts $errorId "sentence unexpectedly matched"
	    } else {
		puts $errorId $diff
	    }
	    puts $errorId "" ;# blank line
	    incr errors 1
	    set exit 1
	}
	# parse $sentence $parser
	# test-display
    }
    close $fileId
    close $timingId
    if {$exit == 0} {
	puts "all $count sentences were OK"
    } else {
	close $errorId
	puts "$errors errors encountered; see $errorFile"
    }
    set slash [string last "/" $timingFile]
    set directory ""
    if {$slash != -1} {
	set directory [string range $timingFile 0 $slash]
	incr slash
	set timingFile [string range $timingFile $slash end]
    }
    compare-timings ${directory}$timingFile ${directory}gold/$timingFile
    return $errors
}

set break_test 0
set max_count 50

proc test-prolog-output {file {start ""} {stop ""}} {
    global defaultparser break_test max_count
    if {$start == ""} {
	set start 1
	set stop end
    } elseif {$stop == ""} {
	set stop $start
    }
    set num $start
    set name [file rootname [file tail $file]]
    set wd [exec pwd]
    puts "working directory: $wd"
    while {$num <= $stop} {
	set result [parse-testfile $file $num $num]
	if {$result < 0} {break}
	test-one-prolog-output $name $num
	incr num
    }
}

proc test-one-prolog-output {name num} {
    global defaultparser break_test max_count
    set count [count_solutions $defaultparser]
    set count [optimal-count $count]
    if {$count == 0} {
	puts "no solutions"
	return
    }
    if {$count > $max_count} {
	puts "too many solutions to check"
	return
    }
    set root ${name}-${num}
    set packed ${root}.packed.pl
    set unpacked ${root}.unpacked.pl
    set c ${root}.c.pl
    set sortedc ${root}.sorted_c.pl
    set sortedpl ${root}.sorted_pl.pl 
    set wd [exec pwd]
    print-prolog-chart-graph $packed
    print-solutions -prolog $c "" -sorted
    reset_prolog_storage
    read_list_of_prolog_graphs $c $sortedc
    if {[string match "*/xle*/test" $wd]} {
	set unpack "../prolog/unpack"
    } else {
	set unpack "../../prolog/unpack"
    }
    catch "exec prolog -l $unpack -a $wd/$packed $wd/$unpacked"
    read_list_of_prolog_graphs $unpacked $sortedpl
    set break ""
    if {$break_test} {set break "-break"}
    set rc [compare_prolog_graphs $sortedc $sortedpl $break]
    if {$rc == 0} {
	exec sh -c "rm -f ${root}.*.pl"
	exec sh -c "rm -f ${root}.diff"
    } else {
	catch "exec diff $sortedc $sortedpl > ${root}.diff" rc
	puts "problem with prolog output of $num, see ${root}.diff"
    }
}

proc compare-files {file1 file2} {
    set wd [exec pwd]
    reset_prolog_storage
    set sorted1 "${file1}.sorted"
    set sorted2 "${file2}.sorted"
    read_list_of_prolog_graphs $file1 $sorted1
    if {[string match "*/xle*/test" $wd]} {
	set unpack "../prolog/unpack"
    } else {
	set unpack "../../prolog/unpack"
    }
    set unpacked1 "${file1}.unpacked"
    set unpacked2 "${file2}.unpacked"
    puts "working directory = $wd"
    puts "unpacking $file1 using $unpack"
    catch "exec prolog -l $unpack -a $wd/$file1 $wd/$unpacked1"
    puts "unpacking $file2 using $unpack"
    catch "exec prolog -l $unpack -a $wd/$file2 $wd/$unpacked2"
    puts "sorting $file1"
    read_list_of_prolog_graphs $unpacked1 $sorted1
    puts "sorting $file2"
    read_list_of_prolog_graphs $unpacked2 $sorted2
    set break ""
    if {$break_test} {set break "-break"}
    puts "comparing $file1 and $file2"
    set rc [compare_prolog_graphs $sorted1 $sorted2 $break]
    return rc
}

proc test-packed-output {file {start 1} {stop end}} {
    global defaultparser break_test max_count
    global ignoreGetGround
    set num $start
    set name [file rootname [file tail $file]]
    set wd [exec pwd]
    puts "working directory: $wd"
    while {$num <= $stop} {
	set ignoreGetGround 0
	set result [parse-testfile $file $num $num]
	if {$result < 0} {break}
	set count [count_solutions $defaultparser]
	set count [optimal-count $count]
	if {$count == 0} {
	    incr num
	    continue}
	if {$count > $max_count} {
	    puts "too many solutions to check"
	    incr num
	    continue}
	set root ${name}-${num}
	set packed ${root}.packed.pl
	set unpacked ${root}.unpacked.pl
	set c ${root}.c.pl
	set sortedc ${root}.sorted_c.pl
	set sortedpl ${root}.sorted_pl.pl 
	print-prolog-chart-graph $packed
	print-solutions -prolog $sortedc "" -sorted
	reset_prolog_storage
	# read_list_of_prolog_graphs $c $sortedc
	catch "exec prolog -l ../../prolog/unpack -a $wd/$packed $wd/$unpacked"
	set ignoreGetGround 1
	read_list_of_prolog_graphs $unpacked $sortedpl
	set break ""
	if {$break_test} {set break "-break"}
	set rc [compare_prolog_graphs $sortedc $sortedpl $break]
	if {$rc == 0} {
	    exec sh -c "rm -f ${root}.*.pl"
	    exec sh -c "rm -f ${root}.diff"
	} else {
	    catch "exec diff $sortedc $sortedpl > ${root}.diff" rc
	    puts "problem with prolog output of $num, see ${root}.diff"
	}
	incr num
    }
}

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# temporary.  Used for generation testing.
proc unpack-prolog-graph {file nsolutions} {
    global chartgraph
    set chartgraph [read_prolog_graph ${file}.pl]

    # probably we want a more general mechanism for passing properties from
    # the packed structure down to the individual graphs.
    set rootcat [get_external_graph_prop $chartgraph "rootcategory"]

    if {$chartgraph == ""} {
	puts stderr "Failed to unpack file ${file}.pl"
	return
    }
	
    set count [count_choice_solutions $chartgraph all]

    if {[expr {$nsolutions < $count}]} {
	set count $nsolutions
    }

    while {$count} {
	choose_choice next $chartgraph nogood
	set graph [get_chosen_fstructure $chartgraph]
	print_graph_as_prolog $graph ${file}-${count}.pl ""
	incr count -1
    }
}

proc factorial {n} {
    set fact 1
    while {$n > 1} {
	set fact [expr {$fact * $n}]
	incr n -1
    }
    return $fact
}

proc put-time {fileId tag timeMsg} {
    puts $fileId [format "%s: %.2f" $tag [expr {[lindex $timeMsg 0]/[clock_rate]}]]
}

proc compare-timings {file gold} {
    if {$file == ""} {set file "test0-timings.txt"}
    if {$gold == ""} {set gold "gold/test0-timings.txt"}
    set fileId [open $file]
    set goldId [open $gold]
    set total1 0
    set total2 0
    while {1} {
	set length1 [gets $fileId timing1]
	set length2 [gets $goldId timing2]
	if {$length1 == -1} {break}
	if {$length2 == -2} {break}
	if {[lindex $timing1 0] != [lindex $timing2 0]} {continue}
	set tag [lindex $timing1 0]
	set tag [string range $tag 0 [expr {[string length $tag] - 2}]]
	if {$tag == "benchmark"} {
	    set factor [expr {[lindex $timing1 1] / [lindex $timing2 1]}]
	    if {$factor < .9 || $factor > 1.1} {
		puts "normalization factor = $factor"
	    }
	    continue
	}
	set time1 [lindex $timing1 1]
	set time2 [expr {[lindex $timing2 1] * $factor}]
	if {$time1 == 0} {set time1 0.0001}
	if {$time2 == 0} {set time2 0.0001}
	set total1 [expr {$total1 + $time1}]
	set total2 [expr {$total2 + $time2}]
	set ratio [expr {$time1 / $time2}]
	if {$ratio > 1.5} {
	    puts [format "%s was %.3f times slower" $tag $ratio]
	} elseif {$ratio < .666} {
	    set ratio [expr {1 / $ratio}]
	    puts [format "%s was %.3f times faster" $tag $ratio]
	}
    }
    set ratio [expr {$total1 / $total2}]
    set msg "total time = %s, ratio to gold (normalized) = %.3f"
    puts [format $msg $total1 $ratio]
}

#DISPLAY TESTING

proc test-display {} {
    global showPartials
    set showPartials 1
    set chart [default-chart]
    set window [chart-window-name $chart tree]
    set edge [get-window-prop $window edge]
    if {$edge == ""} {
	puts "NO EDGE!"
	return
    }
    show-trees "(DTree)0" $chart
    set count [count_trees $edge]
    while {1} {
	show-next-tree $window
	test-tree-display
	set tree [get-window-prop $window data]
	if {$tree == "(DTree)0"} {break}
	if {$tree == ""} {break}
	incr count -1
    }
    if {$count != 0} {puts "WRONG NUMBER OF NEXT TREES!"}
    set count [count_trees $edge]
    while {1} {
	show-prev-tree $window
	set tree [get-window-prop $window data]
	if {$tree == "(DTree)0"} {break}
	if {$tree == ""} {break}
	incr count -1
	if {$count < 0} {
	    puts "decremented below 0 on $tree"
	    break
	}
    }
    if {$count != 0} {puts "WRONG NUMBER OF PREV TREES!"}
    show-next-tree $window
    test-widget .
}

proc test-widget {widget} {
    set class [winfo class $widget]
    if {$class == "Canvas"} {return}
    foreach child [winfo children $widget] {
	test-widget $child
    }
    if {$class == "Button"} {
	set text [$widget cget -text]
	if {$text == "kill"} {return}
	puts "***Invoking $widget***"
	$widget invoke
	update idletasks
    }
    if {$class == "Menu"} {
	test-menu $widget
    }
}

proc test-menu {menu} {
    set last [$menu index last]
    set index 0
    while {$index <= $last} {
	set type [$menu type $index]
	if {$type == "command"} {
	    set text [$menu entrycget $index -label]
	    puts "***Invoking $menu $text***"
	    $menu invoke $index
	    update idletasks
	}
	if {$type == "checkbutton" || $type == "radiobutton"} {
	    set text [$menu entrycget $index -label]
	    puts "***Invoking $menu $text***"
	    $menu invoke $index
	    update idletasks
	}
	incr index
    }
}

proc test-tree-display {} {
    set chart [default-chart]
    set window [chart-window-name $chart tree]
    foreach node [winfo children $window.value.1] {
	foreach event [bind $node] {
	    if {![string match "<*Button-2*>" $event]} {continue}
	    update idletasks
	    set script [bind $node $event]
	    eval $script
	    if {$event == "<Button-2>"} {
		test-fstructure-display
	    }
	}
    }
}

proc test-fstructure-display {} {
    set window [default-window fstructure]
    while {1} {
	$window.next invoke
	update idletasks
	set fsID [get-window-prop $window fsID]
	if {$fsID == 0} break
	set children [winfo children $window.value.1menu]
	foreach node $children {
	    if {$node == "$window.value.1menu.nogood"} {continue}
	    if {[winfo class $node] != "Button"} continue
	    $node invoke
	    update idletasks
	    if {[string match "*.select" $node]} {
		$node invoke
	    }
	}
    }
    while {1} {
	$window.prev invoke
	update idletasks
	set fsID [get-window-prop $window fsID]
	if {$fsID == 0} break
    }
}


#FINITE-STATE-RELATED UTILITIES

# creates an fst from a simple specification
proc create-transducer { infile outfile } {
   build_fstfile $infile $outfile 
   return
}

# gets complete morph analysis of input string, based on current config 
# elements is token if tokenization only, morph if morphonly 
proc analyze-string {sentence {elements ""} {parser ""}} {
    global defaultparser
    if {[string match "(Chart)*" $elements]} {
	set parser $elements
	set elements ""
    }
    if {$parser == ""} {
	set parser $defaultparser
    }
    if {$parser == ""} {
	puts "Can't do morph analysis without grammar as source of config"
	return
    }
    set info [strip-performance-data $sentence]
    set sentence [lindex $info 0]
    puts "\nanalyzing \{$sentence\}\n"
    set output [analyze_string $parser $sentence $elements]
    if {$output == ""} {puts "Error in sentence morph analysis"}
    puts $output
    return
}

proc morphemes {string {type ""} {parser ""}} {
    analyze-string $string $type $parser
}

proc tokens {string {parser ""}} {
    analyze-string $string TOKEN $parser
}

# MISC UTILITIES

proc set-performance-vars {filename {chart ""}} {
    global defaultchart
    if {$chart == ""} {set chart $defaultchart}
    set_performance_vars $chart $filename
}

proc make-unencrypted-grammar {{chart ""} {targetdir ""}} {
    global defaultchart
    if {$chart == ""} {
	set chart $defaultchart
    }
    if {$chart == ""} {
	puts "Please load a grammar."
	return
    }
    if {[string match "(Chart)*" $chart] == 0} {
	puts "First argument must be a chart or \"\"."
	return
    }
    set sourcedir [get-grammar-directory $chart]
    if {$targetdir == ""} {
	set targetdir [make-bug-directory $sourcedir]
    }
    make-bug-grammar $chart $targetdir
    foreach pair [grammar_files $chart -pairs] {
	set file [lindex $pair 1]
	set shortname [file tail $file]
	if {[glob -nocomplain $file.NOENCRYPT] == ""} {continue}
	set target $targetdir/$shortname
	puts "Replacing $target with $file.NOENCRYPT"
	if {[catch {eval exec cp $file.NOENCRYPT ${targetdir}/$shortname}]} {
	    puts "ERROR: COULD NOT COPY $file.NOENCRYPT"
	    puts "make-unencrypted-grammar ABORTED."
	    puts "PLEASE FIX THE GRAMMAR AND TRY AGAIN."
	    return
	}
    }
}

proc get-grammar-directory {chart} {
    global grammarProps env
    set filename $grammarProps($chart,filename)
    set slash [string last "/" $filename]
    if {$slash == -1} {
	set dir "."
    } else {
	incr slash -1
	set dir [string range $filename 0 $slash]
    }
    if {[string range $dir 0 0] != "/"} {
	set dir $env(PWD)/$dir
    }
    return $dir
}

proc make-bug-directory {dir} {
    set n 1
    foreach bug [glob -nocomplain "${dir}/../bug*"] {
	set start [string last "/bug" $bug]
	set bug [string range $bug $start end]
	if {[scan $bug "/bug%d" k]} {
	    if {$k >= $n} {set n [expr {$k + 1}]}
	}
    }
    set targetdir "$dir/../bug$n"
    exec mkdir $targetdir
    return $targetdir
}

proc make-bug-grammar {args} {
    global defaultchart
    set chart ""
    set targetdir ""
    set argc 0
    while {$args != ""} {
	set arg [lindex $args 0]
	set args [lrange $args 1 end]
	incr argc
	if {[string match "(Chart)*" $arg] == 0} {
	    set chart $arg
	} elseif {$arg == "-targetdir"} {
	    set arg [lindex $args 0]
	    set args [lrange $args 1 end]
	    set targetdir $arg
	} elseif {$argc == 2 && $chart != ""} {
	    set targetdir $arg
	} else {
	    puts "Unknown make-release-grammar argument: $arg"
	    return
	}
    }    
    if {$chart == ""} {
	set chart $defaultchart
    }
    if {$chart == ""} {
	puts "Please load a grammar."
	return
    }
    if {[string match "(Chart)*" $chart] == 0} {
	puts "First argument must be a chart or \"\"."
	return
    }
    if {$targetdir == ""} {
	set sourcedir [get-grammar-directory $chart]
	set targetdir [make-bug-directory $sourcedir]
    }
    copy-grammar $chart $targetdir 0 1
    # Change the permissions
    puts "chmod g+w $targetdir/*"
    eval exec chmod g+w [glob $targetdir/*]
    puts "chmod g+w $targetdir"
    eval exec chmod g+w $targetdir
}

proc make-release-grammar {args} {
    global defaultchart
    set chart ""
    set targetdir ""
    set encrypt 1
    while {$args != ""} {
	set arg [lindex $args 0]
	set args [lrange $args 1 end]
	if {[string match "(Chart)*" $arg] == 1} {
	    set chart $arg
	} elseif {$arg == "-noencrypt"} {
	    set encrypt 0
	} elseif {$arg == "-targetdir"} {
	    set arg [lindex $args 0]
	    set args [lrange $args 1 end]
	    set targetdir $arg
	} else {
	    puts "Unknown make-release-grammar argument: $arg"
	    return
	}
    }
    if {$chart == ""} {
	set chart $defaultchart
    }
    if {$chart == ""} {
	puts "Please load a grammar."
	return
    }
    if {$targetdir == ""} {
	set date [exec date +%Y-%m-%d]
	set sourcedir [get-grammar-directory $chart]
	set targetdir "$sourcedir/../release-$date"
	if {[catch {eval exec mkdir $targetdir} error]} {
	    puts "$error"
	    puts "Aborting make-release-grammar."
	    return
	}
    }
    set version [file tail $targetdir]
    copy-grammar $chart $targetdir $encrypt 0 $version
}

proc copy-grammar {{chart ""} {targetdir ""} {encrypt 0} {saveorig 0} {version ""}} {
    global defaultchart env
    if {$chart == ""} {
	set chart $defaultchart
    }
    if {$chart == ""} {
	puts "Please load a grammar."
	return
    }
    if {[string match "(Chart)*" $chart] == 0} {
	puts "First argument must be a chart or \"\"."
	return
    }
    set sourcedir [get-grammar-directory $chart]
    if {[string range $targetdir 0 0] != "/"} {
	set targetdir $env(PWD)/$targetdir
    }
    if {[glob -nocomplain $targetdir] == ""} {
	puts "creating $targetdir"
	eval exec mkdir $targetdir
    }
    if {[glob -nocomplain "$targetdir/*"] != ""} {
	puts "$targetdir already has files in it."
	puts "Please remove files and try again."
	return
    }
    set files ""
    set shortfiles ""
    set mappings ""
    set config_mappings ""
    set config_files [grammar_files $chart -config]
    set encrypt_files [get_field $chart encrypt_files]
    foreach pair [grammar_files $chart -pairs] {
	set original [lindex $pair 0]
	set file [lindex $pair 1]
	set shortname [file tail $file]
	# original = how file appeared in config
	# file = fully qualified original file name
	# shortname = file name without any directories (new file name)
	if {$file == ""} {
	    # This is a library name (not a library file)
	    # Save the mapping but don't copy the file
	    set shortname [file tail $original]
	    lappend mappings [list $original $shortname]
	    continue
	}
	# decrypt any encrypted files
	if {[lsearch -exact $files $file] != -1} {continue}
	lappend files $file
	if {[lsearch -exact $shortfiles $shortname] != -1} {
	    while {[lsearch -exact $shortfiles $shortname] != -1} {
		set shortname "${shortname}.new"
	    }
	    puts "renaming $file"
	    puts "      to $shortname"
	}
	lappend shortfiles $shortname
	if {$original != ""} {
	    # library files don't have originals
	    # the library name is treated special instead
	    lappend mappings [list $original $shortname]
	}
	if {[lsearch -exact $config_files $file] != -1} {
	    # do the config files later
	    lappend config_mappings [list $file ${targetdir}/$shortname]
	    continue
	} else {
	    puts "copying $file"
	}
	if {$encrypt && [lsearch -exact $encrypt_files $file] != -1} {
	    if {![encrypt-lexicon-file $file ${targetdir}/$shortname]} {
		puts "ERROR: COULD NOT COPY $file"
		puts "copy-grammar ABORTED."
		puts "PLEASE FIX THE GRAMMAR AND TRY AGAIN."
		return
	    }
	    continue
	}
	if {[catch {eval exec cp $file ${targetdir}/$shortname}]} {
	    puts "ERROR: COULD NOT COPY $file"
	    puts "copy-grammar ABORTED."
	    puts "PLEASE FIX THE GRAMMAR AND TRY AGAIN."
	    return
	}
    }
	
    # Copy the config files
    foreach mapping $config_mappings {
	set source [lindex $mapping 0]
	set target [lindex $mapping 1]
	set shortname [file tail $source]
	set shortname $shortname.orig
	while {[lsearch -exact $shortfiles $shortname] != -1} {
	    set shortname "${shortname}.new"
	}
	if {$saveorig} {
	    puts "saving original of $source"
	    puts "                as $shortname"
	    eval exec cp $source ${targetdir}/$shortname
	}
	replace-filenames $source $target $mappings $version
    }
    
    if {$saveorig} {
	# Write out the performance variables
	set file current-performance-vars.txt
	while {[lsearch -exact $shortfiles $file] != -1} {
	    set file "${file}.new"
	}
	puts "Writing current performance variables to $file."
	print_performance_vars $chart $targetdir/$file
    }
    set mapping [lindex $config_mappings 0]
    set target [lindex $mapping 1]
    set shortname [file tail $target]
    set fileindexdir $targetdir/$shortname.fileindexdir
    puts "Creating $fileindexdir."
    if {[catch {eval exec mkdir $fileindexdir} errors]} {
	puts $errors
    }
    puts "chmod g+w $fileindexdir"
    if {[catch {eval exec chmod g+w $fileindexdir} errors]} {
	puts $errors
    }
    puts "Grammar files copied to $targetdir"
}

proc replace-filenames {oldfile newfile mappings version} {
    set oldfileID [open $oldfile]
    set newfileID [open $newfile w]
    fconfigure $oldfileID -encoding iso8859-1
    fconfigure $newfileID -encoding iso8859-1
    puts "Rewriting $oldfile"
    set section ""
    while {1} {
	set length [gets $oldfileID line]
	if {$length == -1} {
	    close $oldfileID
	    close $newfileID
	    return
	}
	if {$version != "" && [string trim $line] == "GRAMMARVERSION."} {
	    set oldline $line
	    set line [string trimright $line]
	    set length [string length $line]
	    set line [string range $line 0 [expr {$length-2}]]
	    set line "${line} $version."
	    puts "Replacing $oldline with $line"
	}
	if {[section-header $line] != ""} {
	    set section [section-header $line]
	}
	if {[section-footer $line]} {
	    set section ""
	}
	if {$section == "MORPHOLOGY" && [morphology-refs $line]} {
	    set line "\# $line"
	} elseif {$section == "MORPHOLOGY" || $section == "CONFIG"} {
	    set line [parallel-subst $line $mappings]
	} elseif {[string first "property_weights_file" $line] != -1} {
	    set line [parallel-subst $line $mappings]
	} elseif {[string first "prune_subtree_file" $line] != -1} {
	    set line [parallel-subst $line $mappings]
	}
	puts $newfileID $line
    }
}

proc section-header {line} {
    set words [get-tokens $line]
    if {[llength $words] != 4} {return ""}
    set version [lindex $words 3]
    if {![string match {([0-9]*.[0-9]*)} $version]} {return ""}
    return [lindex $words 2]
}

proc section-footer {line} {
    set line [string trim $line]
    if {[string length $line] < 3} {return 0}
    if {[string range $line 0 2] == "---"} {return 1}
    return 0
}

proc morphology-refs {line} {
    set words [get-tokens $line]
    set refs [lindex $words 0]
    if {$refs == "REFS"} {
	return 1
    }
    return 0
}

proc get-tokens {line} {
    set words [split $line]
    set new ""
    foreach word $words {
	if {$word == ""} {continue}
	lappend new $word
    }
    return $new
}

proc parallel-subst {line mappings} {
    # substitute line with mappings left to right, longest match
    # keep track of the last string position we matched on
    set startpos 0
    set maxpos 1000000
    while {1} {
	# look for the next best match after lastmatch
	set bestsource ""
	set besttarget ""
	set bestpos $maxpos
	foreach mapping $mappings {
	    set source [lindex $mapping 0]
	    set target [lindex $mapping 1]
	    set pos [string first $source $line $startpos]
	    if {$pos == -1} {continue}
	    if {$pos > $bestpos} {continue}
	    if {$pos < $bestpos || \
		    [string length $source] > [string length $bestsource]} {
		set bestsource $source
		set besttarget $target
		set bestpos $pos
		continue
	    }
	}
	if {$bestpos == $maxpos} {return $line}
	# substitute besttarget for bestsource at bestpos
	set prefix [string range $line 0 [expr {$bestpos - 1}]]
	set suffixstart [expr {$bestpos + [string length $bestsource]}]
	set suffix [string range $line $suffixstart end]
	set line ${prefix}${besttarget}${suffix}
	set startpos [expr {$bestpos + 1}]
    }
}

proc rewrite-optimality-order {file} {
    set fileID [open $file]
    set newFileID [open ${file}.new w]
    puts "Writing $file to ${file}.new."
    set ranking ""
    while {1} {
	# Get the next line, if any
	set length [gets $fileID line]
	if {$length == -1} {
	    close $newFileID
	    puts "done."
	    return
	}
	# See if we are at an OPTIMALITYRANKING.
	set trimmed [string trim $line]
	if {[string first "OPTIMALITYRANKING" $trimmed] == 0} {
	    puts "Rewriting OPTIMALITYRANKING as OPTIMALITYORDER."
	    set ranking " "
	    set gen 0
	}
	if {[string first "GENOPTIMALITYRANKING" $trimmed] == 0} {
	    puts "Rewriting GENOPTIMALITYRANKING AS GENOPTIMALITYORDER."
	    set ranking " "
	    set gen 1
	}
	# If we are in a ranking, append the current line.
	if {$ranking != ""} {
	    set ranking "$ranking $line"
	}
	# If we are at the end of a ranking, write out an OPTIMALITYORDER.
	if {[string first "." $ranking] != -1} {
	    if {$gen} {
		puts -nonewline $newFileID "GENOPTIMALITYORDER"
	    } else {
		puts -nonewline $newFileID "OPTIMALITYORDER"
	    }
	    # Convert the ranking into a list.
	    set ranks [split $ranking " \t."]
	    set filtered ""
	    foreach rank $ranks {
		if {$rank == ""} {continue}
		lappend filtered $rank
	    }
	    set ranks [lrange $filtered 1 end]
	    # Separate the marks into preference and dispreference lists.
	    set neutral 0
	    set preference ""
	    set dispreference ""
	    foreach rank $ranks {
		if {$rank == "NEUTRAL"} {
		    set neutral 1
		    continue
		}
		if {$neutral} {
		    lappend dispreference $rank
		} else {
		    lappend preference $rank
		}
	    }
	    if {!$neutral} {
		set dispreference $preference
		set preference ""
	    }
	    # Reverse the dispreference list
	    set reversed ""
	    foreach item $dispreference {
		set last [expr {[string length $item]-1}]
		if {[string index $item 0] == "("} {
		    set item [string range $item 1 end]
		    set item "${item})"
		} elseif {[string index $item $last] == ")"} {
		    incr last -1
		    set item [string range $item 0 $last]
		    set item "($item"
		}
		set reversed [concat $item $reversed]
	    }
	    set dispreference $reversed
	    # Write out the lists
	    foreach rank $dispreference {
		puts -nonewline $newFileID " $rank"
	    }
	    foreach rank $preference {
		if {$rank == "STOPPOINT"} {
		    puts -nonewline $newFileID " $rank"
		} elseif {[string index $rank 0] == "("} {
		    set rank [string range $rank 1 end]
		    puts -nonewline $newFileID " (+$rank"
		} else {
		    puts -nonewline $newFileID " +$rank"
		}
	    }
	    puts $newFileID "."
	    set ranking ""
	    continue
	}
	if {$ranking != ""} {continue}
	# Duplicate all other lines.
	puts $newFileID $line
    }
}

set sum-print-subtrees 0
set sum-print-subtrees-cutoff 0.01

proc sum-print-subtrees {file {outfile "stdout"}} {
    global sum-print-subtrees-cutoff
    set fileId [open $file]
    set cats(total:) "0 0"
    set slowsubtrees 0
    set cutoff 0.1
    while {1} {
	set length [gets $fileId line]
	if {$length == -1} {break}
	set line [string trim $line]
	if {$line == ""} {continue}
	if {[string index $line 0] != "("} {continue}
	set items [split $line " "]
	set item [lindex $items 0]
	if {$item != "(unifying" &&
	    $item != "(adding" &&
	    $item != "(completing" &&
	    $item != "(reusing" &&
	    $item != "(refining" &&
	    $item != "(solving"} {continue}
	set sec [string last "sec)" $line]
	if {$sec < 0} {
	    puts ""
	    puts "ABORTED AT: $line"
	    puts ""
	    continue}
	if {$item == "(adding"} {
	    set catid [lindex $items 2]
	    set end [expr {[string first ":" $catid]-1}]
	    set cat [string range $catid 0 $end]
	} else {
	    set cat [lindex $items 1]
	}
	set time [lrange $items end end]
	set end [expr {[string length $time] - 5}]
	set time [string range $time 0 $end]
	if {$time > $cutoff} {
	    if {$slowsubtrees == 0} {
		puts "\nSubtrees that took more than $cutoff second:"
	    }
	    incr slowsubtrees
	    puts $line
	}
	if {![info exists cats($cat)]} {
	    set cats($cat) "1 $time"
	} else {
	    set count [lindex $cats($cat) 0]
	    set total [lindex $cats($cat) 1]
	    incr count
	    set total [expr {$total + $time}]
	    set cats($cat) "$count $total"
	}
	set count [lindex $cats(total:) 0]
	set total [lindex $cats(total:) 1]
	incr count
	set total [expr {$total + $time}]
	set cats(total:) "$count $total"
    }
    close $fileId
    if {$slowsubtrees} {puts ""}
    set sid [array startsearch cats]
    while {[array anymore cats $sid]} {
	set next [array nextelement cats $sid]
	set nextvalue $cats($next)
	set count [lindex $nextvalue 0]
	set total [lindex $nextvalue 1]
	lappend list [concat $next $nextvalue]
    }
    set list [lsort -command time-order $list]
    if {$outfile != "stdout"} {
	if {[catch {set outfileID [open $outfile w]} error]} {
	    puts $error
	    set outfileID stdout
	} else {
	    puts "Printing sum-print-subtree to $outfile."
	}
    } else {
	set outfileID stdout
    }
    puts $outfileID "category count total"
    set cutoff ${sum-print-subtrees-cutoff}
    foreach item $list {
	set time [lindex $item 2]
	if {$time < $cutoff} {
	    puts $outfileID "remaining categories have time < $cutoff"
	    break
	}
	puts $outfileID "$item"
    }
    if {$outfile != "stdout"} {
	close $outfileID
    }
}

proc diff-print-subtrees {file1 file2} {
    set fileId1 [open $file1]
    set fileId2 [open $file2]
    while {1} {
	set length1 [gets $fileId1 line1]
	set length2 [gets $fileId2 line2]
	if {$length1 == -1} {break}
	if {$length2 == -1} {break}
	if {[string index $line1 0] != "("} {continue}
	set items1 [split $line1 " "]
	set item1 [lindex $items1 0]
	if {[string index $line2 0] != "("} {continue}
	set items2 [split $line2 " "]
	set item2 [lindex $items2 0]
	if {$item1 == "(unifying" && $item2 == "(unifying"} {
	    if {([lindex $items1 6] != [lindex $items2 6]) ||
		([lindex $items1 7] != [lindex $items2 7]) ||
		([lindex $items1 8] != [lindex $items2 8])} {
		puts $line1
		puts $line2
		puts ""
	    }
	}
    }
    close $fileId1
    close $fileId2
}

proc find-subtree-multipliers {file {outfile "stdout"}} {
    set fileId [open $file]
    set count 0
    while {1} {
	set length [gets $fileId line]
	if {$length == -1} {break}
	set line [string trim $line]
	if {$line == ""} {continue}
	if {[string index $line 0] != "("} {continue}
	set sec [string last "sec)" $line]
	if {$sec < 0} {continue}
	set items [split $line " "]
	set item [lindex $items 0]
	if {$item != "(solving"} {continue}
	set cat [lindex $items 1]
	set mult [lindex $items 5]
	set mult "$mult --> [lindex $items 7]"
	scan $mult {(%d[%d]x%d[%d]x%d --> %d)} ps pc cs cc ls ts
	if {$ts > $ps && $ts > $cs && $ts > $ls} {
	    puts "$line"
	    if {![info exists cats($cat)]} {set cats($cat) 0}
	    incr cats($cat)
	    incr count
	}
    }
    close $fileId
    puts "$count multipliers"
    set sid [array startsearch cats]
    while {[array anymore cats $sid]} {
	set next [array nextelement cats $sid]
	set nextvalue $cats($next)
	puts "${next}: $nextvalue"
    }
}

proc time-order {a b} {
    set a [lindex $a 2]
    set b [lindex $b 2]
    return [expr {$a < $b}]
}

proc print-ambiguity-sources {{root ""} {chart ""}} {
    global defaultchart
    # handle "print-ambiguity-sources (Chart)xxx"
    if {[string match "(Chart)*" $root]} {
	set chart $root
	set root ""
    }
    if {$chart == ""} {set chart $defaultchart}
    set edge ""
    set subtree ""
    if {$root != ""} {
	set count [scan $root "%d:%d" edge subtree]
	if {$count == 1} {
	    set subtree ""
	} elseif {$count != 2} {
	    puts "error: print-ambiguity-sources expects arguments of the form 17:1 or 23"
	    return
	}
    }
    print_ambiguity_sources $chart $edge $subtree
    puts ""
}

proc gather-solution-lexicon {chart sentence n data} {
    global solutionLexicon
    set preterms [print_solution_preterminals $chart]
    foreach preterm $preterms {
	set name [lindex $preterm 0]
	set cat [lindex $preterm 1]
	if {![info exists solutionLexicon($name)]} {
	    set solutionLexicon($name) ""
	}
	set cats $solutionLexicon($name)
	if {[lsearch $cats $cat] != -1} {continue}
	lappend solutionLexicon($name) $cat
    } 
}

proc print-solution-lexicon {{chart ""} {file ""}} {
    global solutionLexicon
    if {$chart != ""} {
	if {[info exists solutionLexicon]} {
	    unset solutionLexicon
	}
	gather-solution-lexicon $chart "" "" ""
    }
    set list ""
    foreach name [array names solutionLexicon] {
	lappend list [list $name $solutionLexicon($name)]
    }
    set list [lsort $list]
    set ambiguous ""
    if {$file == ""} {
	set fileID stdout
    } else {
	set fileID [open $file w]
    }
    puts $fileID "SOLUTION XXX LEXICON (1.0)\n"
    foreach entry $list {
	set name [lindex $entry 0]
	set cats [lindex $entry 1]
	set cats [lsort $cats]
	foreach char [split $name ""] {
	    if {[regexp {[0-9a-zA-Z]} $char]} {
		puts -nonewline $fileID $char
	    } else {
		puts -nonewline $fileID "`$char"
	    }
	}
	puts $fileID " "
	set count 0
	foreach cat $cats {
	    if {$cat == "SURFACE" && $cats != "SURFACE"} {continue}
	    set len [string length $cat]
	    set suffix [string range $cat [expr {$len - 5}] $len]
	    if {$suffix == "_BASE"} {
		set cat [string range $cat 0 [expr {$len - 6}]]
		puts $fileID "  =$cat XLE;"
	    } else {
		puts $fileID "  =$cat *;"
	    }
	    incr count
	}
	puts $fileID "  ONLY."
	puts $fileID ""
	if {$count < 2} {continue}
	lappend ambiguous $name
    }
    puts $fileID "----"
    if {$file != ""} {close $fileID}
    puts "ambiguous = $ambiguous"
}

proc print-unused-grammar-choices {{rule ""} {chart ""}} {
    global defaultparser
    if {[string match "(Chart)*" $rule] != 0} {
	set temp $chart
	set chart $rule
	set rule $temp
    }
    if {$chart == ""} {
	set chart $defaultparser
    }
    if {$chart == ""} {
	puts "Please specify a chart."
	return
    }
    if {$rule == ""} {
	puts "Printing the unused grammar choices for all rules."
    } else {
	puts "Printing the unused grammar choices for $rule."
    }
    print_unused_grammar_choices $chart $rule
}

proc print-unused-feature-declarations {{chart ""}} {
    global defaultchart
    if {$chart == ""} {
	set chart $defaultchart
    }
    if {$chart == ""} {
	puts "Please specify a chart."
	return
    }
    print_unused_feature_declarations $chart
}

proc print-feature-declarations {{chart ""}} {
    global defaultchart
    if {$chart == ""} {
	set chart $defaultchart
    }
    if {$chart == ""} {
	puts "Please specify a chart."
	return
    }
    print_feature_declarations $chart
}

proc write-gif-for-window {window filename} {
    update
    set id [wm frame $window]
    puts "writing $window on ${filename}.gif"
    # must use eps, or get an overflow message
    # exec xgrabsc -id $id -eps -o ${filename}.eps
    exec xgrabsc -id $id -xwd -o ${filename}.xwd
    # Convert to a gif file
    # exec pstogif ${filename}
    # NOTE: You must enable pbmplus to use ppmtogif
    catch {exec xwdtopnm < ${filename}.xwd | ppmtogif > ${filename}.gif}
    # exec xwdtopnm < treefile.xwd | ppmquant 256 | ppmtogif > treefile.gif
    # exec xwdtopnm < treefile.xwd | ppmtogif > treefile.gif
}

proc rewrite-gif-for-window {window filename} {
    write-gif-for-window $window ${filename}.new
    exec mv ${filename}.new.gif ${filename}.gif
    # exec rm ${filename}.new
}

#proc chart_root {} {
#  set edge [root_edge]
#  set graph [get_field $edge graph]
#  if {$graph == "(Graph)0"} {
#    return $edge
#  } else {
#    return $graph
#  }
#}

proc try-new-look {} {
    create-xle-font xleuifont   "-adobe-helvetica-medium-r-normal--12-120*"
    create-xle-font xlefsfont   "-adobe-courier-medium-r-normal--12-120*"
    create-xle-font xletextfont "-adobe-times-bold-r-normal--14-140*"
    create-xle-font xletreefont "-adobe-courier-medium-r-normal--12-120*"

    set CSTRUCTUREMOTHERD 1.9
}

proc try-underlining-text {} {
    font configure xletextfont -underline 1
}

proc find-long-sentences {filename minwordlength} {
    set fileId [open $filename]
    set sentence ""
    while {1} {
	set length [gets $fileId line]
	if {$length == -1} {break}
	set line [string trim $line]
	set words [split $line]
	if {[llength $words] <= 1} {
	    set sentence ""
	    continue
	}
	foreach word $words {
	    lappend sentence $word
	    if {[string first "." $word] != -1 ||
		[string first "!" $word] != -1 ||
		[string first "?" $word] != -1} {
		set length [llength $sentence]
		if {$length >= $minwordlength} {
		    puts "$length: $sentence"
		    puts ""
		}
		set sentence ""
	    }
	}
	
    }
    close $fileId
}

proc attached-punctuation {string i delta punctchars} {
    set whitespace " \t\n"
    while {1} {
	set char [string index $string $i]
	if {[string first $char $punctchars] != -1} {
	    incr i $delta
	    continue
	}
	if {[string first $char $whitespace] != -1} {
	    return 0
	} else {
	    return 1
	}
    }
}

proc insert-brackets {sentence {punctchars ""}} {
    set sentence [join [list " " $sentence " "] ""]
    set length [string length $sentence]
    set whitespace " \t\n"
    for {set i 0} {$i < $length} {incr i} {
	set char [string index $sentence $i]
	if {[string first $char $punctchars] == -1 &&
	    [string first $char $whitespace] == -1} {
	    continue
	}
	if {[string first $char $punctchars] != -1 &&
	    [attached-punctuation $sentence $i -1 $punctchars]} {
	    continue
	}
	for {set j [expr $i+1]} {$j < $length} {incr j} {
	    set char [string index $sentence $j]
	    if {[string first $char $punctchars] == -1 &&
		[string first $char $whitespace] == -1} {
		continue
	    }
	    if {[string first $char $punctchars] != -1 &&
		[attached-punctuation $sentence $j 1 $punctchars]} {
		    continue
	    }
	    set prefix [string range $sentence 0 $i]
	    set middle [string range $sentence [expr $i+1] [expr $j-1]]
	    set suffix [string range $sentence $j end]
	    set result [join [list $prefix "\[" $middle "\]" $suffix] ""]
	    puts $result
	    puts ""
	}
    }
}

proc y {} {
    # This is to avoid confusion caused by typing too many y's
    # when starting XLE from gdb.
    puts "invalid command name \"y\""
}

proc setx {var value {chart ""}} {
    if {![uplevel \#0 "info exists $var"]} {
	error "Error: $var doesn't exist"
    } elseif {$chart != ""} {
	set-chart-prop $chart $var $value
    } else {
	uplevel \#0 "set $var \"$value\""
    }
}

proc getx {var {chart ""}} {
    if {![uplevel \#0 "info exists $var"]} {
	error "Error: $var doesn't exist"
    } elseif {$chart != "" && [get-chart-prop $chart $var] != ""} {
	return [get-chart-prop $chart $var]
    } else {
	upvar \#0 $var my_local_var
	return $my_local_var
    }
}

proc check-copyrights {pattern} {
    set currentYear 2006
    set beginYear [clock scan "Jan 1, $currentYear"]
    foreach file [glob $pattern] {
	set fileID [open $file]
	set mtime [file mtime $file]
	set line [gets $fileID]
	set period [string first "." $line]
	set copyright [string range $line 0 $period]
	if {[string range $copyright 0 0] == "#"} {
	    set filetype "tcl"
	    set pattern1 "# (c) %d by"
	    set pattern2 "# (c) %d-%d by"
	} else {
	    set filetype "c"
	    set pattern1 "/* (c) %d by"
	    set pattern2 "/* (c) %d-%d by"
	}
	set d1 ""
	set d2 ""
	set matches [scan $copyright $pattern2 d1 d2]
	if {$matches != 2} {
	    set matches [scan $copyright $pattern1 d2]
	    if {$matches != 1} {
		set d2 ""
	    }
	    set d1 $d2
	}
	if {$d2 == ""} {
	    puts "$file doesn't have a valid copyright notice."
	} elseif {$d2 < $currentYear && $mtime > $beginYear} {
	    puts "$file needs to be updated from $d2."
	}
	close $fileID
    }
}

proc check-xle-copyrights {} {
    check-copyrights {../*.[ch]}
    check-copyrights {../tcl/*.tcl}
}

proc print-stack {} {
    set max [info level]
    for {set i 1} {$i < $max} {incr i} {
	puts "Stack $i: [info level -$i]"
    }
}

###############################################################################

set windowProps(dummy) ""

proc get-window-prop {window prop} {
    global windowProps
    if {![info exists windowProps($window,$prop)]} {
	return ""
    }
    return $windowProps($window,$prop)
}

proc set-window-prop {window prop value} {
    global windowProps
    set windowProps($window,$prop) $value
}

###############################################################################

set encodings(dummy} ""

proc set-character-encoding {name encoding {chart ""}} {
    global encodings defaultchart stdio_character_encoding
    global prolog_character_encoding
    set encoding [canonical_encoding $encoding]
    if {[lsearch -exact [encoding names] $encoding] == -1} {
	puts "$encoding is not a valid Tcl character encoding."
	puts "Try 'encoding names' for a complete list."
	return
    }
    if {$name == "*"} {
	puts "Setting the character encoding of stdio to $encoding."
	puts "Setting the default prolog encoding to $encoding."
	puts "Setting the default encoding of files to $encoding."
	set-character-encoding stdio $encoding
	set-character-encoding prolog $encoding
    }
    if {$name == "tcl"} {
	puts "Setting the character encoding of stdio to $encoding."
	set name stdio
    }
    if {$name == "stdio"} {
	set stdio_character_encoding $encoding
	fconfigure stdin -encoding $encoding
	fconfigure stdout -encoding $encoding
	fconfigure stderr -encoding $encoding
	return
    }
    if {$name == "prolog"} {
	if {$chart == ""} {
	    set prolog_character_encoding $encoding
	} else {
	    set_chart_prop $chart default_prolog_encoding $encoding
	}
	return
    }
    set encodings($name) $encoding
}

proc get-character-encoding {name} {
    global encodings stdio_character_encoding
    global prolog_character_encoding
    if {$name == "tcl"} {
	return utf-8
    }
    if {$name == "stdio"} {
	return $stdio_character_encoding
    }
    if {$name == "prolog"} {
	if {$chart == ""} {
	    return $prolog_character_encoding
	} else {
	    return [get_chart_prop $chart default_prolog_encoding]
	}
    }
     if {![info exists encodings($name)]} {
	 if {[info exists encodings(*)]} {
	     return $encodings(*)
	 }
	return ""
    }
    return $encodings($name)
}

###############################################################################

set charts ""
set chartProps(dummy) ""

proc chart-identifier {chart} {
    global charts
    if {![string match "(Chart)*" $chart]} {
	puts "Bad chart: $chart!"
	print-stack
    }
    set i 0
    foreach c $charts {
	incr i
	if {$c == $chart} {
	    return $i
	}
    }
    lappend charts $chart
    incr i
    return $i
}

proc chart-window-name {chart windowType} {
    set id [chart-identifier $chart]
    set name ".chart${id}${windowType}"
    set-window-prop $name chart $chart
    return $name
}

proc get-chart-prop {chart prop} {
    global chartProps
    if {$prop == "sentence"} {
	return [get_field $chart sentence]
    }
    if {[get_chart_prop $chart $prop] != ""} {
	return [get_chart_prop $chart $prop]
    }
    if {![info exists chartProps($chart,$prop)]} {
	return ""
    }
    return $chartProps($chart,$prop)
}

proc set-chart-prop {chart prop value} {
    global chartProps
    if {[set_chart_prop $chart $prop $value] != 1} {
	set chartProps($chart,$prop) $value
    }
}

proc background-color {window} {
    global backgroundColor
    return $backgroundColor
}

proc highlight-color {window} {
    set bg lightgrey
    if {![winfo exists $window]} {return $bg}
    set top [winfo toplevel $window]
    set chart [get-window-prop $window chart]
      if {$chart == ""} {return $bg}
    set i [chart-identifier $chart]
    while {$i > 7} {incr i -6}
    if {$i == 1} {
	return $bg
    } elseif {$i == 2} {
	return red
    } elseif {$i == 3} {
	return green
    } elseif {$i == 4} {
	return yellow
    } elseif {$i == 5} {
	return purple
    } elseif {$i == 6} {
	return orange
    } else {
	return blue
    }
}

proc window-position {window} {
    global window_width window_height
    global window_width_overhead window_height_overhead
    global window_width_offset window_height_offset
    global window_width_gap window_height_gap
    set x $window_width_offset
    set y $window_height_offset
    set location [get-window-prop $window location]
    if {$location == "upperright" || $location == "lowerright"} {
	set x [expr {$x + $window_width + $window_width_overhead + $window_width_gap}]
    }
    if {$location == "lowerleft" || $location == "lowerright"} {
	set y [expr {$y + $window_height + $window_height_overhead + $window_height_gap}]
    }
    while {[window-exists-at $x $y]} {
	incr x 15
	incr y 15
    }
    return "+$x+$y"
}

proc window-exists-at {x y} {
    foreach child [winfo children .] {
	set position [get-window-prop $child position]
	if {$position == "+$x+$y"} {
	    return 1
	}
    }
    return 0
}

###############################################################################

proc fixBidi s {
    set res {}; set buf {}
    foreach c [split $s ""] {
        if {[r2l $c] || [regexp {[ -]} $c] && $buf ne ""} {
            set buf $c$buf ;# prepend
        } else {
            append res $buf$c ;# empty buffer won't hurt
            set buf {}
        }
    }
    append res $buf ;# in case some were left at end of line
 }

proc r2l c {
    scan $c %c uc
    expr {$uc >= 0x05b0 && $uc <= 0x065f
       || $uc >= 0x066a && $uc <= 0x06bf
    } ;# Arabic context glyphs need not be reverted for IE
 }

###############################################################################

proc link-common-sentence-files {labdir1 unlabdir1 labdir2 unlabdir2 maxcount} {
    set files [lsort -dictionary [glob -nocomplain ${labdir1}/S\[0-9\]*.pl]]
    set failed 0
    set overmax 0
    set notsubset 0
    set total 0
    set command "rm -fr $labdir2; mkdir -p $labdir2"
    if {[catch {exec sh -c $command} error]} {
	puts $error
    }
    set command "rm -fr $unlabdir2; mkdir -p $unlabdir2"
    if {[catch {exec sh -c $command} error]} {
	puts $error
    }
    foreach labfile $files {
	incr total
	set tail  [file tail $labfile]
	set unlabfile [glob -nocomplain [format "%s/%s" $unlabdir1 $tail]]
	if {$unlabfile == ""} {
	    puts "$unlabdir1/$tail is missing."
	    incr failed
	    continue
	}
	set labstats [read-prolog-chart-graph-prop $labfile statistics]
	set labcount [optimal-count [lindex $labstats 0]]
	if {$labcount <= 0} {
	    puts "$labfile count = $labcount."
	    incr failed
	    continue
	}
	if {$maxcount != 0 && $labcount > $maxcount} {
	    puts "$labfile count = $labcount."
	    incr overmax
	    continue
	}
	set unlabstats [read-prolog-chart-graph-prop $unlabfile statistics]
	set unlabcount [optimal-count [lindex $unlabstats 0]]
	if {$unlabcount <= 0} {
	    puts "$unlabfile count = $unlabcount."
	    incr failed
	    continue
	}
	if {$maxcount != 0 && $unlabcount > $maxcount} {
	    puts "$unlabfile count = $unlabcount."
	    incr overmax
	    continue
	}
	if {$labcount == $unlabcount} {
	    # This is not useful for discriminative training
	    puts "$labfile count same ($labcount)."
	    incr notsubset
	    continue
	}
	if {$labcount > $unlabcount} {
	    # This is not useful for discriminative training
	    puts "$labfile count too much ($labcount vs. $unlabcount)."
	    incr notsubset
	    continue
	}
	set command "ln -s $labfile $labdir2/$tail"
	if {[catch {exec sh -c $command} error]} {
	    puts $error
	    incr failed
	    continue
	}
	set command "ln -s $unlabfile $unlabdir2/$tail"
	if {[catch {exec sh -c $command} error]} {
	    puts $error
	    incr failed
	    continue
	}
    }
    set good [expr {$total - ($failed + $notsubset + $overmax)}]
    puts "$failed pairs had a sentence that failed."
    puts "$overmax pairs had a sentence with more than $maxcount solutions."
    puts "$notsubset pairs were not in a proper subset relation."
    puts "$good pairs out of $total were good."
}

proc read-prolog-chart-graph-prop {file prop} {
    set fileID [open $file r]
    set value ""
    while {1} {
	set length [gets $fileID line]
	if {$length == -1} {break}
	set line [string trim $line]
	set result [scan $line "${prop}('%\[^'\]')" value]
	if {$result == 1} {break}
	set result [scan $line "'${prop}'('%\[^'\]')" value]
	if {$result == 1} {break}
	set result [scan $line "${prop}(%\[^)\])" value]
	if {$result == 1} {break}
	set result [scan $line "'${prop}'(%\[^)\])" value]
	if {$result == 1} {break}
    }
    close $fileID
    return $value
}

proc remove-non-subset-pairs {labeledfiles unlabeledfiles extension {cutoff 0}} {
    if {$extension == ""} {
	puts "Please specify an extension."
	return
    }
    set labeledfiles2 $labeledfiles$extension
    set unlabeledfiles2 $unlabeledfiles$extension
    set lfile [open $labeledfiles r]
    set ufile [open $unlabeledfiles r]
    set lfile2 [open $labeledfiles2 w]
    set ufile2 [open $unlabeledfiles2 w]
    set total 0
    set failures 0
    while {1} {
	set length [gets $lfile labeled]
	if {$length == -1} {break}
	set length [gets $ufile unlabeled]
	if {$length == -1} {break}
	incr total
	if {$cutoff > 0} {
	    set size [file size $unlabeled]
	    if {$size > $cutoff} {
		puts "$unlabeled is too big."
		flush stdout
		incr failures
		continue
	    }
	    set size [file size $labeled]
	    if {$size > $cutoff} {
		puts "$labeled is too big."
		flush stdout
		incr failures
		continue
	    }
	}
	set subset [labeled_is_subset_unlabeled $labeled $unlabeled]
	if {!$subset} {
	    puts "$labeled is not a subset of $unlabeled."
	    flush stdout
	    incr failures 
	    continue
	}
	puts $lfile2 $labeled
	puts $ufile2 $unlabeled
    }
    close $lfile
    close $ufile
    close $lfile2
    close $ufile2
    puts "Removed $failures out of $total pairs."
    puts "Labeled data stored at $labeledfiles2."
    puts "Unlabeled data stored at $unlabeledfiles2."
    puts "Number of pairs remaining = [expr {$total - $failures}]."
}

proc print-subtree-features {subtreefile featurefile} {
    set subtrees [open $subtreefile "r"]
    if {$subtrees == ""} {
	puts "Could not open $subtreefile."
	return
    }
    set features [open $featurefile "w"]
    if {$features == ""} {
	puts "Could not open $featurefile."
	return
    }
    while {1} {
	set length [gets $subtrees line]
	if {$length == -1} {break}
	if {$line == ""} {break}
	if {[string first "\{" $line] != -1} {continue}
	if {[string first "\}" $line] != -1} {continue}
	if {[string index $line 0] == "\#"} {continue}
	if {[lindex $line 0] < 10} {continue}
	if {[lindex $line 2] != 0} {continue}
	if {[lindex $line 3] != 0} {continue}
	if {[lindex $line 4] != 0} {continue}
	set cat [lindex $line 1]
	if {$cat != "A_BASE" && 
	    $cat != "N_BASE" 
	    && $cat != "V_BASE"} {continue}
	set lex [lindex $line 5]
	if {$lex == ""} {
	    puts $line
	    continue}
	puts $features "cs_adjacent_label $cat $lex"
    }
    close $subtrees
    close $features
}

###############################################################################

# get things started

xle-initialization-script
