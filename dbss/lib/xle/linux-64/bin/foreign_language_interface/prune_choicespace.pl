:- use_module(library(lists)).

prune_choice_space(CS0,Facts0, CS3,Facts3) :-
	choice_space_type(CS0,Type),
	(
	  Type == external ->
	  prune_plg_choice_space(CS0,Facts0, CS3,Facts3)
	;
	  Type == internal ->
	  int2ext_contexts(CS0,Facts0,CS1,_,Facts1),
	  prune_plg_choice_space(CS1,Facts1, CS2,Facts2),
	  ext2int_contexts(CS2,[],Facts2,CS3a,Facts3),
	  (
	    CS0 = choice(_) -> CS3 = choice(CS3a)
	  ;
	    otherwise -> CS3 = CS3a
	  )
	).

prune_plg_choice_space(CS0,Facts0, CS2,Facts2) :-
	copy_term(x(CS0,Facts0), x(CS1, Facts1)),
	mark_cs(CS1),
	mark_used_choices(Facts1),
	propagate_used_choices(CS1),
	eliminate_unused_choices(CS1, CS2),
	clean_choiced_facts(Facts1, Facts2).



%----------------------------------------
% mark_cs
% Each choice variable gets instantiated to
%  m(_Used, _NewVar)

mark_cs([]).
mark_cs([Choice|Choices]) :-
	(
	  Choice = choice(CVs,_) ->
	  mark_ext_cvars(CVs)
	;
	  otherwise -> true
	),
	!,
	mark_cs(Choices).


mark_ext_cvars([]).
mark_ext_cvars([CV|CVs]) :-
	CV = m(_,_),
	!,
	mark_ext_cvars(CVs).


%--------------------------

mark_used_choices([]).
mark_used_choices([cf(_,'$blocked')|Fs]) :-
	!,
	mark_used_choices(Fs).
mark_used_choices([cf(C,_)|Fs]) :-
	mark_used_choice(C),
	!,
	mark_used_choices(Fs).

mark_used_choice(1) :- !.
mark_used_choice(m(y,_)) :- !.
mark_used_choice(Boolean) :-
	functor(Boolean,F,N),
	(F == or ; F == and),
	!,
	mark_used_choice(N,Boolean).
mark_used_choice(Unexpected) :-
	raise_exception(unexpected_choice_boolean(Unexpected)).

mark_used_choice(0,_) :- !.
mark_used_choice(N,Boolean) :-
	arg(N,Boolean,Arg),
	N1 is N-1,
	mark_used_choice(Arg),
	!,
	mark_used_choice(N1,Boolean).



unmark_used_choice(1,1) :- !.
unmark_used_choice(m(y,V), V) :- !.
unmark_used_choice(Boolean, Boolean1) :-
	functor(Boolean,F,N),
	functor(Boolean1,F,N),
	(F == or ; F == and),
	!,
	unmark_used_choice(N,Boolean,Boolean1).

unmark_used_choice(0,_,_) :- !.
unmark_used_choice(N,Boolean,Boolean1) :-
	arg(N,Boolean,Arg),
	arg(N,Boolean1,Arg1),
	N1 is N-1,
	unmark_used_choice(Arg,Arg1),
	!,
	unmark_used_choice(N1,Boolean,Boolean1).


%--------------------------

propagate_used_choices(CS) :-
	reverse(CS, CS1),
	propagate_used_choices1(CS1).

propagate_used_choices1([]).
propagate_used_choices1([choice(CVars,Boolean)|Choices]) :-
	(
	  member(m(Used,_), CVars), Used == y ->
	  mark_used_choice(Boolean)
	;
	  otherwise -> true
	),
	!,
	propagate_used_choices1(Choices).

%--------------------------

eliminate_unused_choices([],[]).
eliminate_unused_choices([choice(CVs0,Bool0)|Choices0], Choices2) :-
	used_choices(CVs0, Used),
	(
	  Used = [] ->
	  Choices2 = Choices1
	;
	  Used = [V] ->
	  Choices2 = Choices1,
	  unmark_used_choice(Bool0,Bool1),
	  V = Bool1
	;
	  otherwise ->
	  unmark_used_choice(Bool0,Bool1),
	  Choices2 = [choice(Used,Bool1)|Choices1]
	),
	!,
	eliminate_unused_choices(Choices0,Choices1).

used_choices([],[]).
used_choices([m(Used,V)|CVars], CVars2) :-
	(
	  Used == y ->
	  CVars2 = [V|CVars1]
	;
	  otherwise ->
	  CVars2 = CVars1
	),
	!,
	used_choices(CVars,CVars1).

%----------------------------------------------------

clean_choiced_facts([], []).
clean_choiced_facts([cf(_,'$blocked')|CFs], CF1s) :-
	!,
	clean_choiced_facts(CFs, CF1s).
clean_choiced_facts([cf(C,F)|CFs], [cf(C1,F)|CF1s]) :-
	unmark_used_choice(C,C1),
	!,
	clean_choiced_facts(CFs, CF1s).
