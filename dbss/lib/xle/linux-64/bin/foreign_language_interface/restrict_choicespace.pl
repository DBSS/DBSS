:- module(restrict_choicespace,
	  [restrict_choice_space/4,
	   remove_deselected_facts/2,
	   restrict_choice_space/6]). %,
	   %eliminate_empty_contexts/6]).

:- use_module(library(lists)).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% restrict_choice_space(+Choices0,+Equivs0,-Choices,-Equivs)
% In case any selections are made in the equivalence, set all the
% selected choice variables to their values, and then prune the
% choices to eliminate and take account of these assignments.

restrict_choice_space(Choices0,Equivs0,Choices,Equivs) :-
	member(select(_,_), Equivs0),
	!,
	instantiate_selections(Equivs0,Equivs1),
	instantiate_implicit_selections(Choices0),
	eliminate_false_choices(Choices0,Choices1,NoGoods0),
	eliminate_instantiable_nogoods(NoGoods0,Choices1,
				       NoGoods2,Choices),
	(
	  NoGoods2 = [_|_] ->
	  format(user_error,
		 "~nERROR: yIncomplete selection in choice space~n", []),
	  fail
	;
	  otherwise -> true
	),
	append(NoGoods2,Equivs1,Equivs).
restrict_choice_space(Choices,Equivs,Choices,Equivs).


instantiate_selections([],[]).
instantiate_selections([select(Var,Value)|Equivs],IEquivs) :- !,
	Var = Value,
	instantiate_selections(Equivs,IEquivs).
instantiate_selections([E|Equivs],[E|IEquivs]) :- 
	instantiate_selections(Equivs,IEquivs).


instantiate_implicit_selections([]).
instantiate_implicit_selections([NonChoice|Choices]) :-
	NonChoice \= choice(_,_),
	!,
	instantiate_implicit_selections(Choices).
instantiate_implicit_selections([choice(CVars,Condition)|Choices]) :-
	(
	  var(Condition) ->
	  (
	    strict_member(1,CVars) ->
	    Condition = 1
	  ;
	    otherwise -> true
	  )
	;
	  otherwise -> true
	),
	instantiate_implicit_selections(Choices).


eliminate_false_choices([],[],[]).
eliminate_false_choices([NonChoice|Choices],
			Choices1, NoGoods) :-
	NonChoice \= choice(_,_),
	!,
	eliminate_false_choices(Choices,Choices1,NoGoods).
eliminate_false_choices([choice(CVars,Condition)|Choices],
			Choices1, NoGoods) :-
	false_through_selection(Condition),
	!,
	\+ strict_member(1,CVars), 
	set_all_uninstantiated_to_false(CVars),
	eliminate_false_choices(Choices,Choices1,NoGoods).
eliminate_false_choices([choice(CVars,Condition)|Choices],
			Choices1, NoGoods) :-
	strict_member(1,CVars), 
	!,
	set_all_uninstantiated_to_false(CVars),
	(
	  true_through_selection(Condition) ->
	  NoGoods = NoGoods1
	;
	  otherwise ->
	  NoGoods = [nogood(not(Condition)) | NoGoods1]
	),
	eliminate_false_choices(Choices,Choices1,NoGoods1).
eliminate_false_choices([choice(CVars,Condition)|Choices],
			[choice(CVars1,Condition)|Choices1], NoGoods) :-
	remove_false_selections(CVars,CVars1),
	eliminate_false_choices(Choices,Choices1,NoGoods).

set_all_uninstantiated_to_false(CVars) :-
	set_all_uninstantiated_to_false(CVars,TrueVars),
	\+ TrueVars = [_,_|_].

set_all_uninstantiated_to_false([],[]).
set_all_uninstantiated_to_false([0|CVs],True) :- !,
	set_all_uninstantiated_to_false(CVs,True).
set_all_uninstantiated_to_false([1|CVs],[1|True]) :- 
	set_all_uninstantiated_to_false(CVs,True).


remove_false_selections([],[]).
remove_false_selections([False|Vs],NFVs) :-
	False == 0, !,
	remove_false_selections(Vs,NFVs).
remove_false_selections([NotFalse|Vs],[NotFalse|NFVs]) :-
	remove_false_selections(Vs,NFVs).

false_through_selection(Var) :- var(Var), !, fail.
false_through_selection(0) :- !.
false_through_selection(Num) :-
	number(Num), !, fail.
false_through_selection(Compound) :-
	functor(Compound,F,N),
	false_through_selection(N,F,Compound).
false_through_selection(0,or,_) :- !.
false_through_selection(N,or,Compound) :-
	arg(N,Compound,Arg),
	N1 is N-1,
	false_through_selection(Arg),
	false_through_selection(N1,or,Compound).
false_through_selection(1,not,Compound) :-
	arg(1,Compound,Arg),
	true_through_selection(Arg).
false_through_selection(N,and,Compound) :-
	N > 0,
	arg(N,Compound,Arg),
	(
	  false_through_selection(Arg) -> true
	;
	  otherwise ->
	  N1 is N-1,
	  false_through_selection(N1,and,Compound)
	).


true_through_selection(Var) :- var(Var), !, fail.
true_through_selection(1) :- !.
true_through_selection(Num) :-
	number(Num), !, fail.
true_through_selection(Compound) :-
	functor(Compound,F,N),
	true_through_selection(N,F,Compound).
true_through_selection(0,and,_) :- !.
true_through_selection(N,and,Compound) :-
	arg(N,Compound,Arg),
	N1 is N-1,
	true_through_selection(Arg),
	true_through_selection(N1,and,Compound).
true_through_selection(1,not,Compound) :-
	arg(1,Compound,Arg),
	false_through_selection(Arg).
true_through_selection(N,or,Compound) :-
	N > 0,
	arg(N,Compound,Arg),
	(
	  true_through_selection(Arg) -> true
	;
	  otherwise ->
	  N1 is N-1,
	  true_through_selection(N1,or,Compound)
	).

%=========================================
eliminate_instantiable_nogoods(NoGoods0,Choices0,NoGoods2,Choices2) :-
	instantiate_nogoods(NoGoods0, NoGoods1),
	(
	  NoGoods1 == NoGoods0 ->
	  NoGoods2 = NoGoods0,
	  Choices2 = Choices0
	;
	  otherwise ->
	  eliminate_false_choices(Choices0, Choices1, _NG),
	  eliminate_instantiable_nogoods(NoGoods1,Choices1, NoGoods2,
					 Choices2)
	).

instantiate_nogoods([],[]).
instantiate_nogoods([nogood(not(1))|NGs], NGs1) :- !,
	% This will instantiate any single variable conditions to 1
	instantiate_nogoods(NGs,NGs1).
instantiate_nogoods([nogood(not(Condition))|NGs], NGs2) :-
	(
	  true_through_selection(Condition) ->
	  NGs2 = NGs1
	;
	  false_through_selection(Condition) ->
	  format(user_error, "~n!Unsatisfiable choice selection~n",[]),
	  fail
	;
	  otherwise ->
	  NGs2 = [nogood(not(Condition))|NGs1]
	),
	instantiate_nogoods(NGs,NGs1).

	  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% restrict the choice space, and remove any facts that then become
% de-selected.

restrict_choice_space(Choices0,Equivs0,Facts0,Choices,Equivs,Facts) :-
	member(select(_,_), Equivs0),
	!,
	restrict_choice_space(Choices0,Equivs0,Choices,Equivs),
	remove_deselected_facts(Facts0,Facts).

remove_deselected_facts([],[]).
remove_deselected_facts([cf(C,F)|Fs],SFs) :-
	(
	  false_through_selection(C) ->
	  SFs = SFs0
	;
	  otherwise ->
	  SFs = [cf(C,F)|SFs0]
	),
	remove_deselected_facts(Fs,SFs0).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%eliminate_empty_contexts(Choices0,Equivs0,Facts0,Choices,Equivs,Facts)
%:-



strict_member(X,[XX|_]) :- X == XX.
strict_member(X,[_|T]) :- strict_member(X,T).
