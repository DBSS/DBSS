%           -*-Mode: prolog;-*-

%%% This file defines the xleprologlib module predicates:
%%%
%%%   ext2int_contexts
%%%   int2ext_contexts      
%%%   named2ext_contexts
%%%   named2int_contexts
%%%   int2named_contexts
%%%   ext2named_contexts
%%%   instantiated2named_contexts
%%%   external_context
%%%  
%%% These all deal with converting between three different formats for
%%% representing contexts and choice spaces.  These three formats are
%%%
%%% 1. Internal: The choice space is an (integer) pointer to a C/XLE data
%%%              structure, and contexts are also (integer) pointers.  No
%%% 	     equivalences are explicitly represented.
%%%
%%% 2. External: Contexts are prolog variables, 1, 0, or boolean combinations
%%%              of such variables, e.g.  and(A,B,or(C,not(D)),E). The
%%% 	     choice space is a list of choice specifications, of the
%%% 	     form choice([C1,C2,...,Cn], Condition), where C1..Cn are
%%% 	     variables and Condition is a possibly boolean context.
%%%              Equivalences are a list of specifications of the form
%%% 	     define(V,Condition), select(V,Value).  In other words,
%%% 	     the external format is just the standard xle prolog
%%% 	     format (but without significance attached to variable
%%% 	     names).
%%%
%%% 3. Named:    This is just like the external format, except that instead
%%%              of prolog variables we have terms of the form
%%% 	     cv(Label,Number), where the concatenation of Label and
%%% 	     number reflects the naming convention employed by the xle
%%% 	     prolog format.   Named contexts are primarily useful as
%%% 	     an intermediate stage for printing/displaying, but not
%%% 	     for computation.
%%%
%%% 4. Instantiated: like external, except that variables are
%%%          instantiated to context pointers
%%%
%%% Note that converting external to internal contexts instantiates
%%% context variables to pointer, so that we don't have pass in a separate
%%% list of contexted facts.   Similarly when naming external contexts,
%%% except that we make a named copy of the facts (and choices and equivs).
%%%
%%% Additionally, this file defines
%%%
%%%    write_fs/1, write_fs/2, write_fs_file/2,
%%%    write_cf_list/1, write_cf_list/2
%%%    write_named_context/1, write_named_context/2


choice_space_type(CS, Type) :-
	nonvar(CS),
	(
	  CS = [] ->
	  Type = external
	;
	  CS = [choice([V|_],_)|_], var(V) -> 
	  Type = external
	;
	  CS = [choice([V|_],_)|_], integer(V) -> 
	  Type = instantiated_external
	;
	  CS = [choice([cv(_,_)|_],_)|_], integer(V) -> 
	  Type = named
	;
	  CS = choice(_) -> 
	  Type = internal
	;
	  integer(CS) -> 
	  Type = internal
	).

ext2int_contexts(ExtChoices,ExtEquivs,IntChoices) :-
	create_fs_choice_space(ExtChoices,ExtEquivs,IntChoices).

ext2int_contexts(ExtChoices,ExtEquivs,ExtFacts,IntChoices,IntFacts) :-
	create_fs_choice_space(ExtChoices,ExtEquivs,IntChoices),
	map_safe_xle_context(ExtFacts,IntFacts,IntChoices).


int2ext_contexts(choice(IntChoices),IntFacts,ExtChoices,ExtEquivs,ExtFacts) :-
	!,
	int2ext_contexts(IntChoices,IntFacts,ExtChoices,ExtEquivs,ExtFacts).
int2ext_contexts(IntChoices,IntFacts,ExtChoices,ExtEquivs,ExtFacts) :-
	name_internal_choices(IntChoices, NamedChoices),
	%name_internal_equivs(IntChoices, NamedEquivs),
	NamedEquivs = [],
	var_name_tab(NamedChoices,NamedEquivs,VTab),
	unname_choices(NamedChoices,ExtChoices,VTab),
	unname_choices(NamedEquivs,ExtEquivs,VTab),
	externalize_facts(IntFacts,ExtFacts,VTab).


named2ext_contexts(NChoices,NEquivs,NFacts,ExtChoices,ExtEquivs,ExtFacts) :-
	var_name_tab(NChoices,NEquivs,VTab),
	unname_choices(NChoices,ExtChoices,VTab),
	unname_choices(NEquivs,ExtEquivs,VTab),
	unname_facts(NFacts,ExtFacts,VTab).


named2int_contexts(NChoices,NEquivs,NFacts,IntChoices,IntFacts) :-
	named2ext_contexts(NChoices,NEquivs,NFacts,
			   ExtChoices,ExtEquivs,IntFacts),
	ext2int_contexts(ExtChoices,ExtEquivs,IntChoices).


int2named_contexts(choice(IntChoices),IntFacts,NChoices,NEquivs,NFacts) :-
	!,
	int2named_contexts(IntChoices,IntFacts,NChoices,NEquivs,NFacts).
int2named_contexts(IntChoices,IntFacts,
		   NChoices,NEquivs,NFacts) :-
	name_internal_choices(IntChoices, NChoices),
	%name_internal_equivs(IntChoices, NEquivs),
	NEquivs=[],
	name_internal_facts(IntFacts,NFacts).

ext2named_contexts(ExtChoices,ExtEquivs,ExtFacts,
		   NChoices,NEquivs,NFacts) :-
	copy_term(x(ExtChoices,ExtEquivs,ExtFacts),
		  x(NChoices,NEquivs,NFacts)),
	name_external_choices(NChoices,1),
	name_external_equivs(NEquivs,1).



instantiated2named_contexts(Choices,Eqv,Data,
			    NChoices,NEqv,NData) :-
	empty_assoc(Tab0),
	instantiated_eqv_name_tab(Eqv,1,Tab0,EqvNameTab),
	name_instantiated_choices(Choices,NChoices),
	name_instantiated_equivs(Eqv,EqvNameTab,NEqv),
	name_instantiated_facts(Data,EqvNameTab,NData).
	

%========================================================================

unname_choices([],[],_VTab).
unname_choices([choice(NVs,NC)|NChoices], [choice(Vs,C)|Choices],
	       VTab) :-
	map_unname_ctx(NVs,Vs,VTab),
	unname_ctx(NC,C,VTab),
	unname_choices(NChoices,Choices,VTab).
unname_choices([define(NV,NC)|NChoices], [define(V,C)|Choices],
	       VTab) :-
	unname_ctx(NV,V,VTab),
	unname_ctx(NC,C,VTab),
	unname_choices(NChoices,Choices,VTab).
unname_choices([select(NV,Val)|NChoices], [select(V,Val)|Choices],
	       VTab) :-
	unname_ctx(NV,V,VTab),
	unname_choices(NChoices,Choices,VTab).
unname_choices([weight(NV,Val)|NChoices], [weight(V,Val)|Choices],
	       VTab) :-
	unname_ctx(NV,V,VTab),
	unname_choices(NChoices,Choices,VTab).


unname_ctx(V,V,_) :- (var(V) ; atomic(V)), !.
unname_ctx(1,1,_) :- !.
unname_ctx(0,0,_) :- !.
unname_ctx(cv(L,N), Val, VTab) :- !,
	vtab_value(cv(L,N), Val, VTab).
unname_ctx(or(VarList), Val, VTab) :-
	VarList = [_|_],
	!,
	map_unname_ctx(VarList,Vals,VTab),
	Val =..[or|Vals].
unname_ctx(and(VarList), Val, VTab) :-
	VarList = [_|_],
	!,
	map_unname_ctx(VarList,Vals,VTab),
	Val =..[and|Vals].
unname_ctx(not(VarList), Val, VTab) :-
	VarList = [_|_],
	!,
	map_unname_ctx(VarList,Vals,VTab),
	Val =..[not|Vals].
unname_ctx(Named,UnNamed,VTab) :-
	functor(Named,F,N),
	(
	  F = or ->
	  functor(UnNamed,F,N),
	  unname_ctx_args(N,Named,UnNamed,VTab)
	;
	  F = and ->
	  functor(UnNamed,F,N),
	  unname_ctx_args(N,Named,UnNamed,VTab)
	;
	  F = not ->
	  functor(UnNamed,F,N),
	  unname_ctx_args(N,Named,UnNamed,VTab)
	;
	  otherwise ->
	  UnNamed = Named
	).

unname_ctx_args(0,_,_,_).
unname_ctx_args(N,Named,UnNamed,VTab) :-
	arg(N,Named,NArg),
	arg(N,UnNamed,UArg),
	unname_ctx(NArg,UArg,VTab),
	N1 is N-1,
	unname_ctx_args(N1,Named,UnNamed,VTab).


map_unname_ctx([],[],_VTab).
map_unname_ctx([NV|NVs],[V|Vs],VTab) :-
	unname_ctx(NV,V,VTab),
	map_unname_ctx(NVs,Vs,VTab).

%---------------------------------  

externalize_facts(VA, VA, _VTab) :-
	(atomic(VA) ; var(VA)), !.
externalize_facts(cf(IC,F), cf(EC,EF), VTab) :-
	F = in_set(rule_trace(A,B,C,D,Match,Applies), Set),
	Applies = [_|_],
	!,
	EF = in_set(rule_trace(A,B,C,D,EMatch,EApplies), Set),
	name_internal_context(Match,NMatch),
	unname_ctx(NMatch,EMatch,VTab),
	(
	  Applies = [Ap] ->
	  EApplies = [EAp],
	  name_internal_context(Ap,NAp),
	  unname_ctx(NAp,EAp,VTab)
	;
	  Applies = [Ap, NotAp] ->
	  EApplies = [EAp,ENotAp],
	  name_internal_context(Ap,NAp),
	  unname_ctx(NAp,EAp,VTab),
	  name_internal_context(NotAp,NNotAp),
	  unname_ctx(NNotAp,ENotAp,VTab)
	),
	name_internal_context(IC,NC),
	unname_ctx(NC,EC,VTab).
externalize_facts(cf(IC,F), cf(EC,F), VTab) :-
	!,
	name_internal_context(IC,NC),
	unname_ctx(NC,EC,VTab).
externalize_facts([F|Fs],[EF|EFs], VTab) :- !,
	externalize_facts(F, EF, VTab),
	externalize_facts(Fs, EFs, VTab).
externalize_facts(Compound, ECompound, VTab) :-
	functor(Compound, F, N),
	functor(ECompound, F, N),
	externalize_facts_arg(N, Compound, ECompound, VTab).

externalize_facts_arg(0, _Compound, _ECompound, _VTab) :- !.
externalize_facts_arg(N, Compound, ECompound, VTab) :-
	arg(N,Compound,Arg),
	arg(N,ECompound,EArg),
	N1 is N-1,
	externalize_facts(Arg, EArg, VTab),
	externalize_facts_arg(N1, Compound, ECompound, VTab).


name_internal_facts(VA, VA) :-
	(atomic(VA) ; var(VA)), !.
name_internal_facts(cf(IC,F), cf(NC,NF)) :-
	F = in_set(rule_trace(A,B,C,D,Match,Applies), Set),
	Applies = [_|_],
	!,
	NF = in_set(rule_trace(A,B,C,D,NMatch,NApplies), Set),
	name_internal_context(Match,NMatch),
	(
	  Applies = [Ap] ->
	  NApplies = [NAp],
	  name_internal_context(Ap,NAp)
	;
	  Applies = [Ap, NotAp] ->
	  NApplies = [NAp,NNotAp],
	  name_internal_context(Ap,NAp),
	  name_internal_context(NotAp,NNotAp)
	),
	name_internal_context(IC,NC).
name_internal_facts(cf(IC,F), cf(NC,F)) :-
	!,
	name_internal_context(IC,NC).
name_internal_facts(Compound, ECompound) :-
	functor(Compound, F, N),
	functor(ECompound, F, N),
	name_internal_facts_arg(N, Compound, ECompound).

name_internal_facts_arg(0, _Compound, _ECompound) :- !.
name_internal_facts_arg(N, Compound, ECompound) :-
	arg(N,Compound,Arg),
	arg(N,ECompound,EArg),
	N1 is N-1,
	name_internal_facts(Arg, EArg),
	name_internal_facts_arg(N1, Compound, ECompound).



unname_facts(VA, VA, _VTab) :-
	(atomic(VA) ; var(VA)), !.
unname_facts(cf(IC,F), cf(NC,NF), VTab) :-
	F = in_set(rule_trace(A,B,C,D,Match,Applies), Set),
	Applies = [_|_],
	!,
	NF = in_set(rule_trace(A,B,C,D,NMatch,NApplies), Set),
	unname_ctx(Match,NMatch),
	(
	  Applies = [Ap] ->
	  NApplies = [NAp],
	  unname_ctx(Ap,NAp)
	;
	  Applies = [Ap, NotAp] ->
	  NApplies = [NAp,NNotAp],
	  unname_ctx(Ap,NAp),
	  unname_ctx(NotAp,NNotAp)
	),
	unname_ctx(IC,NC,VTab).
unname_facts(cf(IC,F), cf(NC,F), VTab) :-
	!,
	unname_ctx(IC,NC,VTab).
unname_facts(Compound, ECompound, VTab) :-
	functor(Compound, F, N),
	functor(ECompound, F, N),
	unname_facts_arg(N, Compound, ECompound, VTab).

unname_facts_arg(0, _Compound, _ECompound, _VTab) :- !.
unname_facts_arg(N, Compound, ECompound, VTab) :-
	arg(N,Compound,Arg),
	arg(N,ECompound,EArg),
	N1 is N-1,
	unname_facts(Arg, EArg, VTab),
	unname_facts_arg(N1, Compound, ECompound, VTab).



map_safe_xle_context(VA, VA, _) :-
	(atomic(VA) ; var(VA)), !.
map_safe_xle_context(cf(IC,IF), cf(EC,EF), CS) :-
	IF = in_set(rule_trace(A,B,C,D,IMatch,IApplies), Set),
	IApplies = [_|_],
	!,
	EF = in_set(rule_trace(A,B,C,D,EMatch,EApplies), Set),
	xle_safe_context(IMatch,CS,NMatch),
	unname_ctx(NMatch,EMatch,CS),
	(
	  IApplies = [IAp] ->
	  EApplies = [EAp],
	  xle_safe_context(IAp,CS,NAp),
	  unname_ctx(NAp,EAp,CS)
	;
	  IApplies = [IAp, INotAp] ->
	  EApplies = [EAp,ENotAp],
	  xle_safe_context(IAp,CS,NAp),
	  xle_safe_context(INotAp,CS,NNotAp),
	  unname_ctx(NAp,EAp,CS),
	  unname_ctx(NNotAp,ENotAp,CS)
	),
	xle_safe_context(IC,CS,NC),
	unname_ctx(NC,EC,CS).
map_safe_xle_context(cf(IC,F), cf(EC,F), CS) :-
	!,
	xle_safe_context(IC,CS,NC),
	unname_ctx(NC,EC,CS).
map_safe_xle_context(Compound, ECompound, CS) :-
	functor(Compound, F, N),
	functor(ECompound, F, N),
	map_safe_xle_context_arg(N, Compound, ECompound, CS).

map_safe_xle_context_arg(0, _Compound, _ECompound, _CS) :- !.
map_safe_xle_context_arg(N, Compound, ECompound, CS) :-
	arg(N,Compound,Arg),
	arg(N,ECompound,EArg),
	N1 is N-1,
	map_safe_xle_context(Arg, EArg, CS),
	map_safe_xle_context_arg(N1, Compound, ECompound, CS).



%---------------------------------  

name_external_choices([],_).
name_external_choices([NonChoice|Choices],N) :-
	NonChoice \= choice(_,_),
	!,
	name_external_choices(Choices,N).
name_external_choices([choice(CVs,_)|Choices],N) :-
	choice_string(N,Lab),
	name_ext_cvars(CVs,1,Lab),
	N1 is N+1,
	name_external_choices(Choices,N1).

name_external_equivs([],_).
name_external_equivs([define(cv('CV_',N),_) | Equivs], N) :- !,
	N1 is N+1,
	name_external_equivs(Equivs, N1).
name_external_equivs([_|Equivs], N) :-
	name_external_equivs(Equivs, N).

name_ext_cvars([],_,_).
name_ext_cvars([CV|CVs],N,Lab) :-
	number_chars(N,NChars),
	append(Lab,NChars,LabNChars),
	atom_chars(CV,LabNChars),
	N1 is N+1,
	name_ext_cvars(CVs,N1,Lab).

choice_name(N,M,Name) :-
	choice_string(N,String),
	number_chars(M,Cs),
	append(String,Cs,CNString),
	atom_chars(Name,CNString).

choice_label(N,Label) :-
	choice_string(N,String),
	atom_chars(Label,String).

choice_string(0, []) :- !.
choice_string(ID, Label) :-
	R is ((ID-1) mod 26) + 65,
	Q is ((ID-1) // 26),
	choice_string(Q, Label1),
	append(Label1, [R], Label).


%---------------------------------------------------------------

% create a look up table that associates named contexts with prolog
% variables.

var_name_tab(NamedChoices,NamedEquivs,VTab) :-
	empty_assoc(VTab0),
	var_name_tab_choices(NamedChoices,VTab0,VTab1),
	var_name_tab_equivs(NamedEquivs,VTab1,VTab).


vtab_value(1, 1, _).
vtab_value(0, 0, _).
vtab_value(cv(Label,Num), Val, VTab) :-
	get_assoc(Label, VTab, ValArray),
	cc_array_val(Num, Val, ValArray).

%----------------------------------------

var_name_tab_choices([],VTab,VTab).
var_name_tab_choices([NonChoice | Named], VTab0, VTab2) :-
	NonChoice \= choice(_,_),
	!,
	var_name_tab_choices(Named, VTab0, VTab2).
var_name_tab_choices([choice(CNames,_) | Named], VTab0, VTab2) :-
	CNames = [cv(Label,_)|_],
	length(CNames,N),
	cc_create_array(N,CVars),
	put_assoc(Label, VTab0, CVars, VTab1),
	var_name_tab_choices(Named, VTab1, VTab2).

var_name_tab_equivs(Equivs, VTab0, VTab1) :-
	equiv_label_nums(Equivs,[],LabelNums),
	var_name_tab_labelnums(LabelNums,VTab0,VTab1).


var_name_tab_labelnums([],VTab,VTab).
var_name_tab_labelnums([Lab-Num|LabelNums], VTab0, VTab2) :-
	cc_create_array(Num, CVars),
	put_assoc(Lab, VTab0, CVars, VTab1),
	var_name_tab_labelnums(LabelNums, VTab1, VTab2).

equiv_label_nums([],LNums,LNums).
equiv_label_nums([define(cv(Lab,Num),_) | Equivs], LNums0, LNums2) :- !,
	(
	  select(Lab-N0, LNums0, LNs) ->
	  (
	    Num > N0 ->
	    LNums1 = [Lab-Num|LNs]
	  ;
	    otherwise -> LNums1 = LNums0
	  )
	;
	  otherwise ->
	  LNums1 = [Lab-Num|LNums0]
	),
	equiv_label_nums(Equivs, LNums1, LNums2).
equiv_label_nums([_ | Equivs], LNums0, LNums2) :-
	equiv_label_nums(Equivs, LNums0, LNums2).


%--------------------------------------------------------------

external_choice_nametab(ExtChoice,NameTab) :-
	external_choice_nametab(ExtChoice,1,[],NameTab).

external_choice_nametab([],_,NameTab,NameTab).
external_choice_nametab([NonChoice|Choices],
			N,NameTab0,NameTab2) :-
	NonChoice \= choice(_,_),
	!,
	external_choice_nametab(Choices,N,NameTab0,NameTab2).
external_choice_nametab([choice(CVars,_)|Choices],
			N,NameTab0,NameTab2) :-
	choice_label(N,Label),
	ec_nametab(CVars,1,Label,NameTab0,NameTab1),
	N1 is N+1,
	external_choice_nametab(Choices,N1,NameTab1,NameTab2).

ec_nametab([],_,_,NT,NT).
ec_nametab([V|Vs],N,Label,NT0,NT2) :-
	number_chars(N,NChars),
	atom_chars(NA,NChars),
	atom_concat(Label,NA,NLabel),
	N1 is N+1,
	ec_nametab(Vs,N1,Label,[cv(Label,N)-V, NLabel-V|NT0], NT2).



external_context(Var,_,Var) :- var(Var), !.
external_context(1,_,1) :- !.
external_context(0,_,0) :- !.
external_context(Internal,NameTab,External) :-
	integer(Internal),
	!,
	name_internal_context(Internal,Named),
	external_context(Named,NameTab,External).
external_context(and(A,B),NameTab,and(AE,BE)) :- !,
	external_context(A,NameTab,AE),
	external_context(B,NameTab,BE).
external_context(or(A,B),NameTab,or(AE,BE)) :- !,
	external_context(A,NameTab,AE),
	external_context(B,NameTab,BE).
external_context(not(A),NameTab,not(AE)) :- !,
	external_context(A,NameTab,AE).
external_context(and(ConjList),NameTab,Conj) :- !,
	external_context_list(ConjList,and,NameTab,Conj).
external_context(or(DisjList),NameTab,Disj) :- !,
	external_context_list(DisjList,or,NameTab,Disj).
external_context(ConjorDisjList,NameTab,ConjorDisj) :-
	ConjorDisjList =..[AndOr|List],
	(AndOr = and ; AndOr = or),
	!,
	external_context_list(List,AndOr,NameTab,ConjorDisj).
external_context([Disj|DisjList], NameTab, Disjunction) :-
	!,
	external_context_list([Disj|DisjList],or,NameTab,Disjunction).
external_context(C, NameTab, Var) :-
	member(C-Var, NameTab),
	!.
external_context(X, _, X).

external_context_list([[]], Connective, _NameTab, TF) :- !,
	(
	  Connective = or -> TF = 1
	;
	  Connective = and -> TF = 0
	).
external_context_list([[H|T]], Connective, NameTab, Context) :- !,
	(
	  Connective = and -> Connective1 = or
	; Connective = or -> Connective1 = and
	),
	external_context_list([H|T], Connective1, NameTab,Context).
external_context_list([C], _Connective, NameTab, Context) :- !,
	external_context(C, NameTab,Context).
external_context_list([[H|T]|Cs], Connective, NameTab, Context) :- !,
	(
	  Connective = and -> Connective1 = or
	; Connective = or -> Connective1 = and
	),
	external_context_list([H|T], Connective1, NameTab,Context1),
	Context =..[Connective, Context1, Context2],
	external_context_list(Cs, Connective, NameTab, Context2).
external_context_list([C|Cs], Connective, NameTab, Context) :-
	external_context(C, NameTab,Context1),
	Context =..[Connective, Context1, Context2],
	external_context_list(Cs, Connective, NameTab, Context2).

%---------------------------------------------------------------------

instantiated_eqv_name_tab([],_N,Tab,Tab).
instantiated_eqv_name_tab([define(C1,C2)|Eqvs],N,Tab0,Tab3) :-
	!,
	(
	  get_assoc(C1,Tab0,_CopyAbbrev) ->
	  % For some reason, we occasionally get different
	  % abbreviations corresponding to the same context. I think
	  % this is a bug.  Print a warning message, and set the
	  % abbreviation table to empty, so that we don't try to print
	  % out variable abbreviations
	  %format(user_error,
	  %	 "~nWarning: Multiple definitions for abbreviation ~w~n",
	  %	 [CopyAbbrev]),
	  empty_assoc(Tab3)
	;
	  otherwise ->
	  put_assoc(C1, Tab0, cv('CV_',N), Tab1),
	  put_assoc(C2, Tab1, cv('CV_',N), Tab2),
	  N1 is N+1,
	  instantiated_eqv_name_tab(Eqvs,N1,Tab2,Tab3)
	).
instantiated_eqv_name_tab([_|Eqvs],N,Tab0,Tab3) :-
	instantiated_eqv_name_tab(Eqvs,N,Tab0,Tab3).


name_instantiated_eqv_context(C, EqvTab, MaxDef,NC) :-
	(
	  C=1 -> NC = 1
	;
	  C=0 -> NC = 0
	;
	  (get_assoc(C,EqvTab, NC),
	   NC @< MaxDef) -> true
	;
	  integer(C) ->
	  name_internal_context(C,NC)
	;
	  otherwise ->
	  functor(C,F,N),
	  functor(NC,F,N),
	  name_instantiated_eqv_context(N,C,EqvTab,MaxDef,NC)
	).

name_instantiated_eqv_context(N,C,EqvTab,MaxDef,NC) :-
	(
	  N =< 0 -> true
	;
	  otherwise ->
	  arg(N,C,Arg),
	  arg(N,NC,NArg),
	  name_instantiated_eqv_context(Arg,EqvTab,MaxDef,NArg),
	  N1 is N-1,
	  name_instantiated_eqv_context(N1,C,EqvTab,MaxDef,NC)
	).



name_instantiated_context(C, EqvTab, NC) :-
	(
	  C=1 -> NC = 1
	;
	  C=0 -> NC = 0
	;
	  get_assoc(C,EqvTab, NC) -> true
	;
	  integer(C) ->
	  name_internal_context(C,NC)
	;
	  otherwise ->
	  functor(C,F,N),
	  functor(NC,F,N),
	  name_instantiated_context(N,C,EqvTab,NC)
	).

name_instantiated_context(N,C,EqvTab,NC) :-
	(
	  N =< 0 -> true
	;
	  otherwise ->
	  arg(N,C,Arg),
	  arg(N,NC,NArg),
	  name_instantiated_context(Arg,EqvTab,NArg),
	  N1 is N-1,
	  name_instantiated_context(N1,C,EqvTab,NC)
	).


name_instantiated_context_list([],_,[]).
name_instantiated_context_list([C|Cs], EqvTab, [NC|NCs]) :-
	name_instantiated_context(C,EqvTab,NC),
	name_instantiated_context_list(Cs, EqvTab, NCs).



name_instantiated_facts(X,_,X) :-
	(var(X) ; atomic(X)), !.
name_instantiated_facts(cf(C,F), EqvTab, cf(NC,F)) :- !,
	name_instantiated_context(C,EqvTab,NC).
name_instantiated_facts(E,EqvTab,NE) :-
	functor(E,F,N),
	functor(NE,F,N),
	name_instantiated_facts(N, E,EqvTab,NE).

name_instantiated_facts(N, E,EqvTab,NE) :-
	(
	  N =< 0 -> true
	;
	  otherwise ->
	  arg(N,E,Arg),
	  arg(N,NE,NArg),
	  name_instantiated_facts(Arg,EqvTab,NArg),
	  N1 is N-1,
	  name_instantiated_facts(N1, E,EqvTab,NE)
	).


name_instantiated_choices([],[]).
name_instantiated_choices([weight(Ctx, Val)|Choices],
			  [weight(NCtx, Val) | NChoices]) :-
	name_instantiated_context(Ctx,t,NCtx),
	name_instantiated_choices(Choices,NChoices).
name_instantiated_choices([NonChoice|Choices],
			  NChoices) :-
	NonChoice \= choice(_,_), NonChoice \= weight(_,_),
	!,
	name_instantiated_choices(Choices,NChoices).
name_instantiated_choices([choice(List,Ctx)|Choices],
			  [choice(NList,NCtx) | NChoices]) :-
	name_instantiated_context_list(List,t,NList),
	name_instantiated_context(Ctx,t,NCtx),
	name_instantiated_choices(Choices,NChoices).




name_instantiated_equivs([],_EqvNameTab,[]).
name_instantiated_equivs([define(C1,C2)|Eqvs], EqvNameTab,
			 [define(NC1,NC2)|NEqvs]) :-
	\+ empty_assoc(EqvNameTab),
	% Empty name tab means that we have an error with duplicated
	% defines, in which case, we scrap all equivalences bar selections
	!,
	get_assoc(C1,EqvNameTab,NC1),
	functor(C2,F,N),
	functor(NC2,F,N),
	name_instantiated_eqv_context(N,C2,EqvNameTab,NC1,NC2),
	name_instantiated_equivs(Eqvs, EqvNameTab, NEqvs).
name_instantiated_equivs([select(C1,Val)|Eqvs], EqvNameTab,
			 [select(NC1,Val)|NEqvs]) :- !,
	get_assoc(C1,EqvNameTab,NC1),
	name_instantiated_equivs(Eqvs, EqvNameTab, NEqvs).
name_instantiated_equivs([_|Eqvs],EqvNameTab,NEqvs) :-
	% Ignore anything we don't like the look of
	name_instantiated_equivs(Eqvs, EqvNameTab, NEqvs).



%---------------------------------------------------------------------
% write_fstructure_to_stream
%   This writes an fstructure  to a
%   stream in the same  format as the XLE's prolog notation.  That is
%     cv('A',2)                  ===>  A2
%     or([cv('B',1), cv('B',2)]) ===> or(B1,B2)
%   (Need to ensure no more than 255 arguments to functors)
%---------------------------------------------------------------------

write_fs_file(FS,File) :-
	open(File,read,Out),
	write_fs(Out,FS),
	format(Out, ".~n", []),
	close(Out).

write_fs(FS) :- write_fs(user_output,FS).

write_fs(Output,FS) :-
	% note: does not write final period
	FS = fstructure(String, Properties0, Choices, Eqv,
			FStruct,CStruct),
	(
	  select('$blocked'(BlockedCtx), Properties0, Properties) ->
	  append(NEqv0, [nogood(NBlockedCtx)], NEqv)
	;
	  otherwise ->
	  Properties = Properties0,
	  BlockedCtx = 0,
	  NEqv = NEqv0
	),
	(
	  Choices = choice(ChoiceSpace) ->
	  int2named_contexts(ChoiceSpace,
			     [cf(BlockedCtx, blocked),CStruct,FStruct],
			     NChoices,NEqv0,
			     [cf(NBlockedCtx, blocked),NCStruct,NFStruct])
	;
	  ext2named_contexts(Choices,Eqv,
			     [cf(BlockedCtx, blocked),CStruct,FStruct],
			     NChoices,NEqv0,
			     [cf(NBlockedCtx, blocked),NCStruct,NFStruct])
	),
	write_named_structure(
	    fstructure(String, Properties, NChoices, NEqv,
		       NFStruct,NCStruct),
	    Output).


write_named_structure(
	    fstructure(String, Properties, NChoices, NEqv,
		       NFStruct,NCStruct),
	    Output) :- !,
	format(Output, "fstructure(~q,~n", [String]),
	format(Output, "	% Properties:~n	[~n", []),
	write_list(Properties, Output),
	format(Output, "	],~n", []),
	format(Output, "	% Choices:~n	[~n", []),
	write_list(NChoices, Output),
	format(Output, "~n	],~n", []),
	format(Output, "	% Equivalences:~n	[~n", []),
	write_list(NEqv, Output),
	format(Output, "~n	],~n", []),
	format(Output, "	% FS Facts:~n	[~n", []),
	write_list(NFStruct, Output),
	format(Output, "~n	],~n", []),
	format(Output, "	% CS Facts:~n	[~n", []),
	write_list(NCStruct, Output),
	format(Output, "~n	]~n)", []).

write_named_structure(Expr,Output) :-
	(atomic(Expr) ; var(Expr)),
	!,
	format(Output, "~q", [Expr]).

write_named_structure(Expr,Output) :-
	functor(Expr,F,N),
	format(Output, "~q(~n", [F]),
	write_named_structure_arg(1,N,Expr,Output),
	format(Output, "~n)", []).


write_named_structure_arg(N, Max, Expr, Output) :-
	N =< Max,
	arg(N, Expr, Arg),
	(
	  Arg = [_|_] ->
	  format(Output, "[~n", []),
	  write_list(Arg,Output),
	  format(Output, "~n]", [])
	;
	  otherwise ->
	  format(Output, "~q", [Arg])
	),
	(
	  N = Max -> true
	;
	  otherwise ->
	  format(Output, ",~n", []),
	  N1 is N-1,
	  write_named_structure_arg(N1, Max, Expr, Output)
	).
	  
	
% ----------------------------------------------------------------------

write_cf_list(List) :- write_list(List,user_output).
write_cf_list(Stream,List) :- write_list(List,Stream).

write_list([], _) :- !.
write_list([cf(A, B) | Items], Output) :-
	!,
	format(Output, "  cf(", []),
	write_context(A, Output),
	format(Output, ", ", []),
	write_fact(B, Output),
	format(Output, ")", []),
	write_list_comma(Items, Output).
write_list([choice(A, B) | Items], Output) :-
	!,
	format(Output, "  choice([", []),
	write_context_list(A, Output),
	format(Output, "], ", []),
	write_context(B, Output),
	format(Output,")", []),
	write_list_comma(Items, Output).
write_list([define(A, B) | Items], Output) :-
	!,
	format(Output, "  define(", []),
	write_context(A, Output),
	format(Output, ", ", []),
	write_context(B, Output),
	format(Output,")", []),
	write_list_comma(Items, Output).
write_list([select(A, B) | Items], Output) :-
	!,
	format(Output, "  select(", []),
	write_context(A, Output),
	format(Output, ", ~q)", [B]),
	write_list_comma(Items, Output).
write_list([nogood(A) | Items], Output) :-
	!,
	format(Output, "  nogood(", []),
	write_context(A, Output),
	format(Output, ")", []),
	write_list_comma(Items, Output).
write_list([weight(A, B) | Items], Output) :-
	!,
	format(Output, "  weight(", []),
	write_context(A, Output),
	format(Output, ", ~q)", [B]),
	write_list_comma(Items, Output).
write_list([Item], Output) :-
	!,
	format(Output, "  ~q~n", [Item]).
write_list([Item | Items], Output) :-
	!,
	format(Output, "  ~q,~n", [Item]),
	write_list(Items, Output).

write_list_comma([],_). :- !.
write_list_comma([H|T], Output) :-
	format(Output, ",~n", []),
	write_list([H|T], Output).



write_fact(in_set(rule_trace(RuleNum,Id,Pattern,Replacement,
			     MatchContext,ApplyContexts),
		  attr(var(0), 'RULE-TRACE')),
	   Output) :-
	ApplyContexts = [_|_],
	!,
	escape_quotes(Pattern,Pattern1),
	escape_quotes(Replacement,Replacement1),
	format(Output, "in_set(rule_trace(~w,~w,'~w','~w',",
	       [RuleNum,Id,Pattern1,Replacement1]),
	write_context(MatchContext, Output),
	format(Output,",[",[]),
	(
	  ApplyContexts = [App] ->
	  write_context(App, Output)
	;
	  ApplyContexts = [App, NotApp]->
	  write_context(App, Output),
	  format(Output,",",[]),
	  write_context(NotApp, Output)
	),
	format(Output, "]), attr(var(0), 'RULE-TRACE'))", []).
write_fact(in_set(rule_trace(RuleNum,Id,Pattern,Replacement),
		  attr(var(0), 'RULE-TRACE')),
	   Output) :-
	!,
	escape_quotes(Pattern,Pattern1),
	escape_quotes(Replacement,Replacement1),
	format(Output,
	"in_set(rule_trace(~w,~w,'~w','~w'), attr(var(0), 'RULE-TRACE'))",
	       [RuleNum,Id,Pattern1,Replacement1]).
write_fact(eq(Attr, semform(Atom,Id,Th,NTh)), Output) :-
	atom(Atom),
	!,
	% really annoying: certain atoms like '...' don't actually
	% need quoting by prolog, but causes the xle prolog reader to
	% fall over. So quote all atomic values
	escape_quotes(Atom,Atom1),
	format(Output, "eq(~q, semform('~w',~q,~q,~q))",
	       [Attr,Atom1,Id,Th,NTh]).
write_fact(eq(Attr, Atom), Output) :-
	atom(Atom),
	!,
	% really annoying: certain atoms like '...' don't actually
	% need quoting by prolog, but causes the xle prolog reader to
	% fall over. So quote all atomic values
	escape_quotes(Atom,Atom1),
	format(Output, "eq(~q, '~w')", [Attr,Atom1]).
write_fact(terminal(N,T,Ns), Output) :- !,
	% really annoying: certain atoms like '...' don't actually
	% need quoting by prolog, but causes the xle prolog reader to
	% fall over. So quote all atomic values
	escape_quotes(T,T1),
	format(Output, "terminal(~q, '~w', ~q)", [N,T1,Ns]).
write_fact(surfaceform(N,T,N1,N2), Output) :- !,
	% really annoying: certain atoms like '...' don't actually
	% need quoting by prolog, but causes the xle prolog reader to
	% fall over. So quote all atomic values
	escape_quotes(T,T1),
	format(Output, "surfaceform(~q, '~w', ~q, ~q)", [N,T1,N1,N2]).
%write_fact(Fact, Output) :-
%	format(Output, "~q", [Fact]).
write_fact(Fact, Output) :-
	write_fact1(Fact, Output),
	!.
write_fact(Fact, _Output) :-
	raise_exception(write_fact(Fact)).

write_fact1(Var, Output) :-
	(var(Var); number(Var)),
	!,
	format(Output, "~q", [Var]).
write_fact1([], Output) :-
	!,
	format(Output, "[]", []).
write_fact1(Atom, Output) :-
	atom(Atom),
	!,
	atom_chars(Atom, Chars),
	quote_string(Chars, QChars),
	format(Output, "~s", [QChars]).
write_fact1([H|T], Output) :-
	!,
	format(Output, "[", []),
	write_fact1_list([H|T], Output),
	format(Output, "]", []).
write_fact1(Infix, Output) :-
	current_predicate(utl:infix_compound/5),
	utl:infix_compound(Infix,Arg1,Op,Arg2,NeedsBracket),
	!,
	( NeedsBracket -> format(Output, "(", [])
	; otherwise -> true),
	write_fact1(Arg1, Output),
	format(Output, "~w", [Op]),
	write_fact1(Arg2, Output),
	( NeedsBracket -> format(Output, ")", [])
	; otherwise -> true).
write_fact1(Compound, Output) :-
	Compound =.. [F|Args],
	format(Output, "~q(", [F]),
	write_fact1_list(Args, Output),
	format(Output, ")", []).

write_fact1_list([],_) :- !.
write_fact1_list([H], Output) :-
	!,
	write_fact1(H,Output).
write_fact1_list([H|T], Output) :-
	write_fact1(H,Output),
	format(Output, ",", []),
	!,
	write_fact1_list(T, Output).

% quote_string copied from utl.pl
quote_string([H|S], QS) :-
	(
	  H < 97 -> Quoted0 = y
	;
	  H > 122 -> Quoted0 = y
	;
	  otherwise -> Quoted0 = n
	),
	quote_string([H|S], Quoted0, Quoted1, QS0),
	(
	  Quoted1 == n ->
	  QS = QS0
	;
	  otherwise ->
	  QS = [0''|QS0]
	).

quote_string([],Quoted, Quoted, String) :-
	(
	  Quoted == y -> String = "'"
	;
	  otherwise -> String = []
	).
quote_string([H|T],Quoted0, Quoted2, String) :-
	(
	  H == 0'' ->
	  Quoted1 = y,
	  String = [0'\\ , H|String1]
	;
	  H == 0'\\ ->
	  Quoted1 = y,
	  String = [0'\\ , H|String1]
	;
	  memberchk(H, " +-*/^<>=~:.,?@#$%&()[]{}|!") ->
	  Quoted1 = y,
	  String = [H|String1]
	;
	  memberchk(H, [0'\b, 0'\t, 0'\n, 0'\v, 0'\f, 0'\r, 0'\e,
			0'\d, 0'\a, 0'"]),    %"
	  Quoted1 = y,
	  String = [H|String1]
	;
	  % following the Sicstus 3.12.7 manual, section 50.4, the
	  % following character ranges can appear in atoms (and
	  % so appear verbatim and do not force the string to be quoted)
	  
	  (0'A =< H, H =< 0'Z ; 0'a =< H, H =< 0'z ; % 7 bit upper/lower case, 
	   0'0 =< H, H =< 0'9 ; H= 0'_ ;             % digits and underscore
	  
	  192 =< H, H =< 214 ; 216 =< H, H =< 222 ;  % upper case letters
	  223 =< H, H =< 246 ; H >= 248)             % lower case letters
	                               ->
	  Quoted1 = Quoted0,
	  String = [H | String1]

	;
	  % anything else appears verbatim but forces the string to be quoted
	  otherwise ->
	  Quoted1 = y,
	  String = [H | String1]
	),
	quote_string(T, Quoted1, Quoted2, String1).


escape_quotes(Term,ETerm) :-
	(
	  atom(Term) ->
	  atom_chars(Term,Chars),
	  escape_quotes_chars(Chars,EChars),
	  atom_chars(ETerm,EChars)
	;
	  compound(Term) ->
	  functor(Term,F,N),
	  escape_quotes(F,EF),
	  functor(ETerm, EF, N),
	  escape_quotes_arg(N,Term,ETerm)
	;
	  otherwise -> ETerm = Term
	).


escape_quotes_arg(N,Term,ETerm) :-
	(
	  N < 1 -> true
	;
	  otherwise ->
	  arg(N,Term,Arg),
	  arg(N,ETerm,EArg),
	  escape_quotes(Arg,EArg),
	  N1 is N-1,
	  escape_quotes_arg(N1,Term,ETerm)
	).

escape_quotes_chars([],[]).
escape_quotes_chars([0''|Cs],[0'\\, 0''|ECs]) :- !,
	escape_quotes_chars(Cs, ECs).
escape_quotes_chars([C|Cs],[C|ECs]) :- !,
	escape_quotes_chars(Cs, ECs).


%-----------------------------------
write_named_context(C) :- write_context(C, user_output).
write_named_context(Stream,C) :- write_context(C,Stream).

write_context(cv(A,N), Output) :- !,
	format(Output, "~w~w", [A, N]).
write_context(1, Output) :- !,
	write(Output,1).
write_context(0, Output) :- !,
	write(Output,0).
write_context(Boolean, Output) :-
	functor(Boolean,F,N),
	write(Output,F),
	(
	  N>0 ->
	    write(Output,'('), 
	    write_context_args(1,N,Boolean,Output),
	    write(Output,')')
	;
	  otherwise -> true
	).

write_context_args(Last,Last,Boolean,Output) :- !,
	arg(Last,Boolean,Arg),
	write_context(Arg,Output).
write_context_args(N,Last,Boolean,Output) :-
	N < Last,
	arg(N,Boolean,Arg),
	write_context(Arg,Output),
	write(Output,','),
	N1 is N+1,
	write_context_args(N1,Last,Boolean,Output).




write_named_context_no_commas(C) :- write_context_no_commas(C, user_output).
write_named_context_no_commas(Stream,C) :- write_context_no_commas(C,Stream).

write_context_no_commas(cv(A,N), Output) :- !,
	format(Output, "~w~w", [A, N]).
write_context_no_commas(1, Output) :- !,
	write(Output,1).
write_context_no_commas(0, Output) :- !,
	write(Output,0).
write_context_no_commas(Boolean, Output) :-
	functor(Boolean,F,N),
	write(Output,F),
	(
	  N>0 ->
	    write(Output,'('), 
	    write_context_no_commas_args(1,N,Boolean,Output),
	    write(Output,')')
	;
	  otherwise -> true
	).

write_context_no_commas_args(Last,Last,Boolean,Output) :- !,
	arg(Last,Boolean,Arg),
	write_context_no_commas(Arg,Output).
write_context_no_commas_args(N,Last,Boolean,Output) :-
	N < Last,
	arg(N,Boolean,Arg),
	write_context_no_commas(Arg,Output),
	write(Output,' '),
	N1 is N+1,
	write_context_no_commas_args(N1,Last,Boolean,Output).


write_context_list([],_Output).
write_context_list([C|Cs],Output) :-
	write_context(C,Output),
	(
	  Cs = [] -> true
	;
	  otherwise ->
	  format(Output,", ", []),
	  write_context_list(Cs,Output)
	).


%==========================================================

/* (c) 2001 by the Xerox Corporation.  All rights reserved */

% File: arrays.pl
% Author: Dick Crouch
%% Creating and accessing prolog "arrays":
%%   Array represented as a single prolog vector if size is 255 or less
%%   Otherwise, a root vector contains subvectors holding values.
%%   At the moment, max array size is 64770 (255 * 254)
%%      where 255 is max size of a vector,
%%        and 254 because first arg of root vector holds size of array
%%   No update of array values possible, other than through instantiating
%%      variables
%%   Gives constant time access to values


cc_create_array(Size,Array) :-
	(
	    Size < 256 ->
	    functor(Array,'$array',Size)
	;
	    Size > 64770 ->
	    nl,write('Maximum array size (64770) exceeded: '), write(Size),nl,
	    fail
	;
	    NumberOfVectors is (Size//255)+1,
	    RootVecSize is NumberOfVectors+1,
	    functor(Array,'$root_array',RootVecSize),
	    arg(1,Array,Size),
	    cc_create_sub_arrays(2,Size,Array)
	).

cc_array_size(Array, Size) :-
	functor(Array, F, N),
	(
	  F = '$array' -> Size = N
	;
	  otherwise -> arg(1, Array, Size)
	).


cc_create_sub_arrays(Arg,Size,RootArray) :-
	Size =< 255,
	!,
	arg(Arg,RootArray,SubArray),
	functor(SubArray,'$array',Size).
cc_create_sub_arrays(Arg,Size,RootArray) :-
	Size > 255,
	arg(Arg,RootArray,SubArray),
	functor(SubArray,'$array',255),
	Arg1 is Arg+1,
	Size1 is Size-255,
	!,
	cc_create_sub_arrays(Arg1,Size1,RootArray).



cc_array_val(Arg,Val,Array) :-
	(
	    functor(Array,'$array',Max) ->
	    Arg =< Max,
	    arg(Arg,Array,Val)
	;
	    arg(1,Array,Max) ->
	    Arg =< Max,
	    SubArrayNum is ((Arg-1)//255)+2,
	    Offset0 is Arg mod 255,
	    (	Offset0 = 0 -> Offset = 255 ; Offset = Offset0),
	    arg(SubArrayNum,Array,SubArray),
	    arg(Offset,SubArray,Val)
	).

