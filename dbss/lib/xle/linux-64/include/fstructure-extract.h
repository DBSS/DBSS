/* (c) 2002-2004 by the Palo Alto Research Center.  All rights reserved */
/* (c) 1996-2001 by the Xerox Corporation.  All rights reserved */

#ifndef FSTRUCTURE_EXTRACT_H
#define FSTRUCTURE_EXTRACT_H

CSubTree *allocate_csubtree(Edge *edge);
CSubTreeList *allocate_csubtree_list(Chart *chart);

Graph *extract_fstructure(RestrictedSolution *solution, 
			  DUCompState *compstate, DTree *tree);
AVPair *follow_links(AVPair *avp, RestrictedSolution **solution);
AVPair *find_copied_avpair(AVPair *avp, Graph *graph, 
			   RestrictedSolution *solution);
AVPair *find_copied_avpair2(AVPair *original, AVPair *avp,
			    RestrictedSolution *solution);
void clear_copy_links(AVPair *avp);
void reconstruct_graph_semforms(Graph *graph);

int empty_tree_avpair(AVPair *avp);

RestrictedSolution *add_consumers(RestrictedSolution *solution, 
				  RestrictedSolution *consumer);
RestrictedSolution *copy_dnf_solution(RestrictedSolution *solution);
RestrictedSolution *copy_local_solution(RestrictedSolution *solution,
					InternalSolution *internal);
void move_solution_up(RestrictedSolution **solution, Graph *graph);
void move_solution_down(RestrictedSolution **solution, Graph *graph);
void reset_solution(RestrictedSolution **solution, 
		    RestrictedSolution *original);
void mark_local_solution(RestrictedSolution *solution, int mark);
void mark_dnf_solution(RestrictedSolution *solution, int mark, int top);
int in_context(Clause *context, RestrictedSolution *solution);
void add_phi_inverse(AVPair *avp, Edge *edge, Clause *context);
CVPair *get_phi_inverse(AVPair *avp);
CVPair *minimum_phi_inverse(AVPair *avp);

Graph *extract_chart_graph(Chart *chart);
Graph *extract_n_best_graph(Graph *graph, int n, char *weightsFile);
Graph *dummy_chart_graph(Chart *chart);
void cleanup_extracted_chart(Chart *chart);
void *telescope_bypass_value(TypedValue value);
char *get_pred_name(AVPair *avp);
void repack_metavars(Graph *graph);
RestrictedSolution *chosen_fschart_solution(Graph *graph);
DTree *chosen_fschart_tree(Graph *graph);
Graph *extract_selected_fstructure(Graph *graph);
extern int normalize_chart_graphs;
void add_surface_form(Graph *graph, CSubTree *cterminal, Edge *surface);
void contextualize_surface_forms(Graph *graph);
void clean_up_surface_forms(Graph *graph);
void record_statistics_as_props(Graph *graph);
void cache_select_best_parse_state(Chart *chart, char *weightsFName);
SemformEdge *add_semform_edge_to_list(SemformEdge *edge, SemformEdge *list);

CVPair *get_constant_value(AVPair *avp, Clause *context, int);

void mark_accessible_graph_facts(Graph *graph, char *structures);
/* Mark all of the graph facts in structures with the contexts */
/* that they are accessible in.  The contexts are stored in contexts[1]. */

void clear_accessibility_contexts(Graph *graph);
/* Clear the accessibility contexts set by mark_accessible_graph_facts. */

void mark_accessible_fstructures(Graph *graph, int mark);
/* Set AVPair->mark to mark in all of the avpairs */
/* that are accessible in the phi structure. */

void collapse_spurious_semform_ids(Graph *graph);
void eliminate_variables(Graph *graph, int normalize);

/* Data structures used to refactor the chart graph */

typedef struct ContextCombData ContextCombData;
typedef struct ChoiceCombArray ChoiceCombArray;

/* A record of two disjoint clauses disjoined */
struct ContextCombData {
  Clause *combined;
  Clause *clause1;
  Clause *clause2;
  ContextCombData *next;
};

/* An array of combinations indexed by the first choice of the combination */
struct ChoiceCombArray {
  int size;
  Graph *graph;
  ContextCombData **data;
};


#endif

 
