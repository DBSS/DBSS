/* (c) 2002-2006 by the Palo Alto Research Center.  All rights reserved */
/* (c) 1996-2001 by the Xerox Corporation.  All rights reserved */
#ifndef ERRORS_H
#define ERRORS_H

#include <stdio.h>
#include <signal.h>
#include <assert.h>
#include <setjmp.h>
#include <time.h>
#include <unistd.h>

#define print_error_and_exit(msg) \
{ \
    fprintf(stderr, "Error in %s:%d (%s)\n", __FILE__, __LINE__, (msg)); \
    assert(0); \
}

extern int print_subtrees;

extern FILE *pstout;   /* the file to print subtrees on. */

extern int catch_signals;
extern int abort_xle;
extern jmp_buf out_of_time;
extern jmp_buf out_of_storage;
extern clock_t last_check;
extern char *last_check_file;
extern int last_check_line;

#define CLOCKRATE (float)CLOCKS_PER_SEC

/* --------------------------------- */
/* Check whether we are out of time. */
/* --------------------------------- */
#if 0
#define STOP_IF_OUT_OF_TIME                                         \
if (catch_signals) {                                                \
   clock_t now = clock();                                           \
   if (last_check && (now - last_check) > CLOCKS_PER_SEC) {         \
     printf("checked %s:%d more than 1 sec after %s:%d\n",          \
	    __FILE__, __LINE__, last_check_file, last_check_line);  \
   }                                                                \
   last_check = now;                                                \
   last_check_file = __FILE__;                                      \
   last_check_line = __LINE__;                                      \
   if (abort_xle) longjmp(out_of_time, -1);                         \
}
#else
#define STOP_IF_OUT_OF_TIME                                        \
if (abort_xle && catch_signals) {                                  \
  last_check_file = __FILE__;                                      \
  last_check_line = __LINE__;                                      \
  longjmp(out_of_time, -1);                                        \
}
#endif

#endif

