/* (c) 2002-2004 by the Palo Alto Research Center.  All rights reserved */
/* (c) 1996-2001 by the Xerox Corporation.  All rights reserved */

#ifndef ATTRIBUTES_H
#define ATTRIBUTES_H
#define ATTRVARS

#include "instantiate-attribute-id.h"
		  /* to get the definition of "enum AttrID" */

/* ------------------------------------------------------------------------ */
/* An AVPair represents an attribute-value pair within an attribute-value   */
/* tree. The tree is never re-entrant.  Instead, re-entrancies are given by */
/* explicit equality links.  Each AVPair represents a unique path           */
/* designator that can be determined by following the prefixes until there  */
/* are no more and then recursively concatenating the AttrIDs.              */
/* ------------------------------------------------------------------------ */

struct AVPair {

  AttrID attr;		    /* The name of this attribute  */

  AVPair *attrs;	    /* list of (subordinate) avpairs.  Each AVPair */
			    /* in this list will point back to this AVPair */
			    /* through the prefix field. */

#ifdef ATTRVARS
  AVPair *vars;             /* list of variables local to this AVPair. */
#endif

  CVPair *equals;	    /* The contexted values for the equality */
			    /* relation.  The equality relation is built in */
			    /* since it is so common and since it requires */
			    /* special handling. Having a contexted value */
			    /* here is the same as saying that the path */
			    /* designator that this AVPair corresponds to */
			    /* is equal to (or not equal to or equal subc */
			    /* to) that value. The equality relation has */
			    /* LAZY links similar to the lazy links of the */
			    /* subusumption relation. */

  RVPair *rels;		    /* list of relations and their contexted values */
			    /* that are true of this AVPair.  */

  CVPair *copies;	    /* list of vertical copy links, lazy and */
			    /* not. The lazy links point to lower graphs, */
			    /* the regular links point to upper graphs that */
			    /* this AVPair has been copied to. */

  Clause *contexts[2];	    /* contexts in which avpair already */
  			    /* has constraints. This is similar to */
                            /* CVPair->contexts, only applied to the */
			    /* existence of the AVPair. context[0] is the */
			    /* old context, context[1] is the new context. */

  Clause *defined[2];	    /* contexts in which avpair is defined */
  			    /* This is equivalent to the disjunction of all */
			    /* of the non-negative, non-subc constraints. */
			    /* It replaces all REL_EXISTS with props = 0, */
			    /* and is used by REL_ARGLIST. context[0] is */
			    /* the old context, context[1] is the new */
			    /* context. */

  AVPair *prefix;	    /* pointer to dominating avpair */

  AVPair *next;		    /* pointer to the next avpair in the list */
                            /* (corresponds to a sister in the avpair tree) */
  
  Graph *graph;		    /* pointer to the containing graph */

  unsigned short lock : 1;    /* is the avpair locked? If it is, then when */
			    /* new facts are added, their contexts are */
			    /* queued rather than being put in context[1]. */

  unsigned short cyclic : 1;   /* the AVPair is cyclic */

  unsigned short processing : 1;   /* the AVPair is being processed  */

  unsigned short accessible : 1;   /* the AVPair is accessible */

  unsigned short mark1 : 1;   /* mark bit for clients */

  unsigned short mark2 : 1;   /* mark bit for clients */

  unsigned short mark3 : 1;   /* mark bit for clients */

  unsigned short bypassed: 1; /* The AVPair is equal to some other AVPair in */
			    /* all contexts, and so this AVPair need never */
			    /* be copied up.  See next_bypass_context and */
			    /* copy_link. */

  unsigned short expanded: 4; /* have the avpair's lazy links been expanded? */
			    /* The value of expanded indicates the reason */
			    /* for the expansion. */

  unsigned short partly_expanded: 1;
  
  unsigned short expanding: 1;

  unsigned short completed: 1; /* Marks whether the AVPair has been */
			     /* completed.  This is important since if a */
			     /* completion function calls expand_lazy_links */
			     /* on something that has already been */
			     /* completed, then the completion functions */
			     /* should be called again. */

  unsigned short pre_completed: 1; 
                             /* Marks whether pre_complete_avpair  */
                             /* has been called.  See note on the */
                             /* completed field above.*/

  unsigned int equalities_copied: 1; /* The equalities have been copied
				        by extract_chart_graph. */

  unsigned int empty_tree: 2;  /* result of empty_tree_avpair */

};

/* ----------------------------------------------------------------------- */

struct AVPArray {
  int size;
  int next_index;
  AVPair **items;
};

/* ----------------------------------------------------------------------- */

typedef enum {         /* types of AVPair->vars */
  UNKNOWN_VAR = -1,    /* we don't know what type of var this is */
  ELT_VAR = 0,         /* the element of a set  */
  FU_VAR = 1,          /* the var of a functional uncertainty */
  NEG_FU_VAR = 2,      /* negative fu vars. */
  SUBSUMEE_VAR = 4,    /* the subsumee of an AVPair. */
                       /* copy_attr_var assumes that 
                          SUBSUMEE_VAR comes last. */
  LAST_VAR_TYPE = 5,   /* the end of the VarTypes */
} VarType ;

#define attr_var_type_start(type) ((type) * 5000)

/* ----------------------------- */
/* General interface procedures. */
/* ----------------------------- */

char *get_attr_str(AVPair* avp);
/* Returns the AttrID of avp as a string. */

AVPair *find_metavariable(Graph *graph, char *name);
/* Finds the metavariable in graph->attrs named 'name'. */

AVPair *find_attribute(AVPair *avp, char *name);
/* Finds the attribute in graph->attrs named 'name'. */

CVPair *get_attr_inverse_values(AVPair *avp, int attr);
/* Return a list of inverse values for a particular attribute. */

int has_slash(AVPair *avp);

int tree_op_avpair(AVPair *avp);

/* ----------------------------------------------------------------------- */
/* procedures for asking about properties of particular AttrIDs            */
/* ----------------------------------------------------------------------- */

int attr_is_governable(AVPair *attr);
/* Is attr governable? (e.g. like SUBJ, OBJ, etc.) */

int attr_is_nondistributive(AVPair *attr);
/* Is attr nondistributive? (e.g. like CONJ, ADJ, etc.) */

int attr_is_semantic(AVPair *attr);
/* Is attr semantic? (e.g. like MODS, TOPIC which must have a PRED) */

int attr_is_projection(AVPair *attr);
/* Is attr a projection? (like f::, s::) */

int attr_in_uncertainty(AVPair *attr);
/* Is attr in an uncertainty? */

int attr_is_invertible(AVPair *attr);
/* Is attr in an inside-out uncertainty? */

int attr_can_be_constant(AVPair *attr);
/* Can attr have a constant value ? (heuristic) */

int is_abbrev_attribute(AVPair *attr);
/* is this a abbreviation attribute? */

void set_abbrev_attributes(DUCompState *compstate, char *attributes);
/* set the abbreviation attributes from 'attributes'. */

void print_attribute_props(char *name, DUCompState *compstate);

void make_all_nondistributive(void *grammar);
/* Make all of the attributes in the grammar be nondistributive. */

char *get_attr_str_from_id(AttrID attr, DUCompState *compstate); 
/* Get the attribute name for attr. Returns NULL if <= GR_GLOBAL. */

char *get_attr_str_from_id2(AttrID attr, DUCompState *compstate); 
/* Get the attribute name for attr. Always returns something. */

int get_meta_symbol_code(Graph *graph, char *symbol_buffer);
/* Get the meta symbol code for a string. */

int attr_id_is_nondistributive(AttrID attr_id, DUCompState *compstate);
/* Is attr nondistributive? (e.g. like CONJ, ADJ, etc.) */

int attr_id_is_external(AttrID attr, Grammar *grammar);
/* Is attr listed in EXTERNALATTRIBUTES? */

int attr_id_is_non_local(AttrID attr_id, DUCompState *compstate);
/* Is attr_id non local? */

int attr_id_is_governable(AttrID attid, void *grammar);

int attr_id_is_semantic(AttrID attid, void *grammar);

int attr_id_is_addable(AttrID, DUCompState *);

int get_governable_id(DUCompState *compstate, char *attr);
/* Returns the id of a governable attribute. */
/* Returns 0 if the attribute is not governable. */

char *get_governable_id_name(DUCompState *compstate, int id);
/* Converts a governable id back to its name. */

int slash_attr_id(AttrID id);

/* ----------------------------------------------------------------------- */

#endif


