/* (c) 2002-2004 by the Palo Alto Research Center.  All rights reserved */
/* (c) 1996-2001 by the Xerox Corporation.  All rights reserved */
#ifndef CLAUSE_FUNC_H
#define CLAUSE_FUNC_H
#include "values.h" /* for parameters of assert_nogood */
#include "DU-typedefs.h"
#include "clause.h"
#include "graph-func.h"
#include "graph.h"
#include "instantiate-attribute-id.h"
#include "compstate.h"

/* -------------------------------------------------------------------- */
/* This procedures are the interface to the boolean clause management   */
/* package.  They implement a simple ATMS over propositional variables. */
/* -------------------------------------------------------------------- */

extern /*const*/ Clause *True_Context;
extern /*const*/ Clause *False_Context;

/* -------------------------------------------------------------------- */
/*                      CREATING DISJUNCTIONS                           */
/* -------------------------------------------------------------------- */

Disjunction* create_disjunction(Graph *graph, Clause *context, int n_choices);
/* Create a Disjunction in context with n_choices in it. */
/* If n_choices = 1, return NULL. */

Clause *get_choice(Graph *graph, Disjunction *disj, int ith_choice);
/* Get the ith_choice of disj. */

DisjunctionList *get_choice_dependents(Clause *choice);
/* Return the disjunctions that have choice in their context. */

Clause *true_context(Graph *graph);
/* Returns a true context for the graph (not True_Context). */
/* This can be used as input to get_choice_dependents. */

void label_choice(Graph *graph, Disjunction *disj, int ith_choice,
		  char *label, ChoiceLabelType type, void *data, void *avp);
/* Label the ith_choice of disj. */

void set_choice_weight(Graph *graph, Clause *clause, double weight);

double get_choice_weight(Graph *graph, Clause *clause);

void set_choice_probability(Graph *graph, Clause *clause, double probability);

double get_choice_probability(Graph *graph, Clause *clause);

void add_choice_feature_weights(Clause *clause, feature_weight_pair *features);

feature_weight_pair *get_choice_feature_weights(Clause *clause);

Disjunction* create_prolog_disjunction(Graph *graph, Clause *context, 
				       int n_choices, int disj_id);
/* Create a Disjunction in context with n_choices in it. */
/* If n_choices = 1, return a unary disjunction. */

/* ---------------------------------------- */
/* The following are used by prolog-read.g. */
/* ---------------------------------------- */

Clause *get_prolog_choice(Graph *graph, char *name);

Disjunction *get_prolog_disjunction(Graph *graph, Clause *context);

void clear_prolog_choice_data(Graph *graph);


/* -------------------------------------------------------------------- */
/*                         BASIC OPERATIONS                             */
/* -------------------------------------------------------------------- */

Clause *conjoin_clauses(Graph *graph, Clause *c1, Clause *c2, int prune);
/* Conjoin c1 and c2 and return the result in canonical form.  If prune is */
/* 1, then return False_Context if the result is nogood. */

int conjoinable_clauses(Graph *graph, Clause *c1, Clause *c2);
/* Returns 1 if c1 and c2 are probably conjoinable. */
/* Returns 0 if they are definitely not conjoinable. */

Clause *disjoin_clauses(Graph *graph, Clause *c1, Clause *c2);
/* Disjoin c1 and c2 and return the result in canonical form. */

Clause *subtract_clause(Graph *graph, Clause *neg, Clause *pos);
/* Subtract neg from pos using negate_clause. */

Clause *negate_clause(Graph *graph, Clause *c, Clause *in);
/* Negate c and in the context of "in" and */
/* return the result in canonical form. */

Clause *not_clause(Graph *graph, Clause *c);
/* Negate a choice.  Wrap a not clause around anything else. */

Clause *import_clause(Graph *graph, Clause *c, Graph *from);
/* Create an opaque clause in graph that puts a wrapper around c. */

void assert_nogood(Graph *graph, Clause *clause, 
		   AVPair *avp, CVPair *fact1, 
		   ValueType type2, void *fact2, char *justification);
/* Assert that clause is nogood in graph.  The avp, fact1, type2, and fact2 */
/* arguments are optionally used to keep track of what caused the nogood to */
/* help the grammar writer. */

void assert_incomplete(Graph *graph, Clause *clause, 
		       AVPair *avp, CVPair *fact, 
		       char *justification, int noOT);
/* Assert that clause is incomplete in graph.  The avp, fact1, type2, and
   fact2 arguments are optionally used to keep track of what caused the
   nogood to help the grammar writer. */

void add_optimality_mark(Graph *graph, char *mark, Clause *clause, 
			 AVPair *avp, int rel, char *data);
/* Adds an optimality mark instead of asserting a nogood. */

void distribute_optimality_mark(Graph *graph, char *mark, Clause *clause, 
				AVPair *avp, int rel, char *data);
/* Distributes an optimality mark instead of distributing an incomplete. */

Clause *local_subtract_clause(Graph *graph, Clause *neg, Clause *pos,
			      int *changed);
/* This procedure is primarily for use by the garbage collector     */
/* in the summarization code.  The garbage collector should call    */
/* mark_choices_non_local on the contexts of facts that are outside */
/* of the facts that are being garbage collected.  Then the         */
/* garbage should be collected by calling local_subtract_clause.    */
/* Call clear_non_local_marks when the garbage collector is done.   */
/* 'changed' is a return value that records whether subtracting neg */
/* from pos changed anything.                                       */

void mark_choices_non_local(Clause *clause);
/* Used in conjunction with local_subtract_clause. */

void clear_non_local_marks(Graph *graph);
/* Used in conjunction with local_subtract_clause. */

void mark_data_choices(Clause *clause);
/* Mark choices in clause as appearing somewhere in the data. */

void mark_graph_data_clauses(Graph *graph);
/* Mark all of the data clause in graph. */

void prune_spurious_graph_choices(Graph *graph);
/* Prune spurious choices.  This heuristic depends on */
/* mark_data_choices being called on every data clause. */

/* -------------------------------------------------------------------- */
/*                            UTILITY PROCEDURES                        */
/* -------------------------------------------------------------------- */

/* ------------------------------------------------------------------------- */
  
/* Is the clause or graph nogood? */

int nogood_avpair_clause(Graph *graph, AVPair *avp, Clause *clause);
/* See if clause is nogood based on the nogoods attached to avp. */

#define compound_clause(c) ((c) > True_Context && (c)->type >= AND)

#define real_clause(c) ((c) > True_Context)

#define clause_type(c) ((c) > True_Context ? (c)->type : \
			((c) == True_Context ? CT_TRUE : CT_FALSE))
/* What is the type of this clause? (clause->type doesn't work for
   True_Context and False_Context) */

#define clause_item(c, t) (((c) > True_Context && (t) >= AND && \
			    (c)->type == (t)) ? (c)->body.compound.item : (c))

#define clause_next(c, t) (((c) > True_Context && (t) >= AND && \
			    (c)->type == (t)) ? (c)->body.compound.next : NULL)

#define unjustify(c) ((c) > True_Context && (c)->type == JUSTIFIED \
		      ? (c)->body.justified.clause : (c))

static XLE_INLINE int
nogood(Clause* clause, Graph *graph)
{
  clause = unjustify(clause);
  if (clause == False_Context) return 1;
  if (clause == True_Context) return 0;
  if (!graph->prune || !graph->compstate->prune) return 0;
  if (graph->nogood) return 1;
  if (clause->pruned) {
    clause = clause->pruned;
    if (clause == True_Context) return 0;
  }
  while (clause->type == OR) {
    Clause *item = clause->body.compound.item;
    if (item->pruned) item = item->pruned;
    if (item == True_Context || !item->nogood) break;
    clause = clause->body.compound.next;
  }
  return clause->nogood;
}

Clause *root_disj_context(Clause *clause);
/* Get the root disjunction context of a choice clause. */

int negated_clause(Clause *c);
/* Does c have a negated clause in it? */

int clause_count_items(Clause *clause, ClauseType type);
/* Count the items in clause if clause is a compound clause of type 'type'. */
/* Otherwise, return 1.  */

char *print_clause(Clause *clause, char *buffer);
/* Print clause on buffer.  If no buffer is given, a new one is created. */
/* Calls print_clause_. */

char *print_clause_(Clause *clause, char *buffer, size_t bufsize);
/* Print clause on buffer unless bufsize is too small. */

char *print_disjunction(Disjunction *disj, char *buffer, size_t bufsize);
/* Print disj on buffer unless bufsize is too small. */

char get_disjunction_id(Disjunction *disj);
/* Return a letter representing the disjunction. Only works up to 26 */
/* disjunctions. (For debugging only.) */

char *get_disjunction_symbol(Disjunction *disj, char symbol[], int uppercase);
/* Return a string that represents a disjunction.  The storage for */
/* the string needs to be passed in. uppercase determines whether the */
/* string is upper case or lower case. */

void get_choice_arm_id(Clause *clause, int *id1, int *id2);
/* Determines the range of ids for the choice clause.  The results are */
/* returned in 'id1' and 'id2'.  If id2==0, then there is only 1 choice. */

char *print_choice(Clause *choice, char *buffer);
/* Prints a choice as "a:1-3" or "b:2" */

char *get_clause_op_name(ClauseOp op);
/* Return the name of op as a string. */

char *get_clause_type_name(ClauseType type);
/* Return the name of type as a string. */

int get_exported_clause_id(Clause *clause);
/* Return a id for clause that is unique to its graph.  Exported clauses */
/* get positive ids, clauses that haven't been exported get negative ids. */

int evaluate_clause(Clause *clause, int closed);
/* Evaluate the clause under the assumption that if clause->value is 1, */
/* the clause evaluates to True.  If closed is 1, then treat NOT clauses */
/* like Prolog's negation-as-failure. */

int evaluate_choices(Clause *clause);
/* Return 1 if clause evaluates to True, 0 if it evaluates to */
/* False, and -1 if the value is unknown because of opaque    */
/* variables. */

void select_choice(Clause *clause);

void select_choices(RestrictedSolution *rs, Graph *graph);

void choose_among_implied_choices(Disjunction *disj, int direction);
/* Make sure that the selection is complete. */

int selected_context(Clause *clause);
/* The context has been selected by the user. */

int selected_context_goodness(Graph *graph);
/* Whether the selected context is good (e.g. not nogood). */

void set_disj_prop(Graph *, Disjunction *, char *, void *);
void *get_disj_prop(Disjunction *, char *);
/* LISP-like feature for adding information to disjunctions */

Clause *justify_clause(Graph *graph, Clause *clause,
		       ValueType type1, void *fact1,
		       ValueType type2, void *fact2,
		       char *explanation);
/* Create a justified clause for clause with the given justifications. */

Clause *explain_clause(Graph *graph, Clause *clause, char *explanation);
/* Create a justified clause for clause with the given explanation. */

Clause *graph_item(Clause *clause, Graph *graph);
/* Get the item in clause that corresponds to graph. */

int equivalent_clause_lists(Clause *list1, Clause *list2);
/* Check whether the clause lists are equivalent. */

int subset_clause_list(Clause *list1, Clause *list2);
/* Check whether list1 is a subset of list2. */

Clause *make_clause_disjoint(Clause *clause);
/* Makes the choices of an OR-clause be disjoint. */
/* Requires that graph->chart_clauses is not 0.   */

/* -------------------------------------------------------------------- */
/*                            INTERNAL PROCEDURES                       */
/* -------------------------------------------------------------------- */

Clause *new_clause(Graph *graph, ClauseType type, 
		   Clause *clause1, Clause *clause2);
/* Create a new clause of type 'type'.  clause2 is only used if type is a */
/* compound type.  new_clause does no canonicalization. */

Clause *uniqueify_clause(Graph *graph, Clause *clause);
/* Look for a clause in graph that is equivalent to clause.  If such a */
/* clause exists, return it.  Otherwise make a copy of clause and return it. */

int clause_cache_order(Clause *c1, Clause *c2);
/* Return 1 if c1>c2, 0 if c1==c2, and -1 if c1<c2. */

int clause_sort_order(Graph *graph, Clause *c1, Clause *c2);
/* Return 1 if c1>c2, 0 if c1==c2, and -1 if c1<c2. */

int edge_choice_order(Graph *graph, Clause *c1, Clause *c2);
/* Return 1 if c1>c2, 0 if c1==c2, and -1 if c1<c2. */

int overlapping_edge_choices(Graph *graph, Clause *c1, Clause *c2);
/* Return 1 if c1 and c2 are overlapping edge choices. */

void sort_clauses(Clause *clause_items[], int n_items);
/* Sort clause_items using clause_sort_order (effectively). */

void swap_clauses(Clause **c1, Clause **c2);
/* This should be an inline or a macro. */

Clause *construct_compound_clause(Graph *graph, Clause *clause_items[], 
				  int n_items, ClauseType type);
/* Create a compound clause of type 'type' out of the n_items clauses in */
/* clause_items. */

void mark_clauses(Clause *clauses, ClauseType type, int mark);
/* Mark the items in clauses if clauses is a compound clause of type */
/* 'type'.  Otherwise, mark clauses. */

void set_item_values(Clause *clause, ClauseType type, int mark);
/* Set the values of the items in clause  if clause is a */
/* compound clause of type 'type'.  Otherwise, set clause->value. */

void set_choice_values(Clause *clause, int mark);
/* Set the value of a choice and its disjunction's context (recursively). */

int implied_by_nogoods(Graph *graph, Clause *nogoods, int closed);
/* Returns 1 if the nogoods evaluate to true.  closed means that we have a */
/* closed world, and NOT clauses can be evaluated.  Otherwise, NOT clauses */
/* are ignored. */

int check_choice_consistency(Clause *c1, Clause *c2);
/* See if the choices c1 and c2 are consistent (can be conjoined). */

void mark_choice_contexts(Clause *context, int mark);
/* Mark a choice and its contexts with mark. */

Clause *check_choice_subsumption(Clause *c1, Clause *c2, ClauseType type);
/* See whether the c1 choice subsumed c2. */

int clause_marked_completely(Clause *clause, ClauseType type, int n);
/* Is the clause completely marked? */

Clause *reduce_clause(Graph *graph, Clause *c1);
/* See if the OR clause c1 can be reduced. */

Clause *reduce_clauses(Graph *graph, Clause *c1, Clause *c2);
/* See if the OR clauses c1 and c2 can be disjoined and reduced. */

Clause *reduce_clauses_list(Graph *graph, Clause **pclause_items[], 
			    int k, int n);
/* See if the clauses in clause_items (which are implicitly disjoined) can */
/* be reduced. n is the number of clauses, and k is the cut point between */
/* sets of clauses that have already been reduced. When clause_items is */
/* realloc'ed, store the new pointer in *pclause_items so the caller can */
/* free it.  Return an OR clause constructed from the reduced clause_items. */

Clause *collapse_clauses(Graph *graph, Clause *c1, Clause *c2);
/* See if the non-OR clauses c1 and c2 can be reduced */
/* (e.g. p&&q || p&&~q --> p). */

Clause *collapse_conjunction(Graph *graph, Clause *c1, Clause *c2);
/* See if the AND clauses c1 and c2 can be reduced. */

void set_cached_op(Graph *graph, ClauseOp op, int pruned,
		    Clause *operand1, Clause *operand2, Clause *result);
/* Cache the operation OP(OPERAND1, OPERAND2) -> RESULT on OPERAND1.  */
/* Replace an existing value.                                         */

void cache_clauseop(Graph *graph, ClauseOp op, int pruned,
		    Clause *operand1, Clause *operand2, Clause *result);
/* Cache the operation OP(OPERAND1, OPERAND2) -> RESULT on OPERAND1.  */
/* Don't check for an existing value.                                 */

ClauseCache *get_cached_op(ClauseOp op, unsigned int pruned, 
			   Clause *clause, Clause *operand);
/* See if the operation OP(CLAUSE, OPERAND) is in the cache for */
/* CLAUSE.  If so, return the result. */

ClauseCache *get_cached_op_type(Clause *clause, ClauseOp op);
/* Return the first ClauseCache in clause that has operation = op. */

ClauseCache *get_first_cached_op(Clause *clause);
/* Return the first ClauseCache in clause. */

void test_clause_cache(Graph *graph);

int covers_clause(Graph *graph, Clause *cover, Clause *covered);
/* Return 1 if cover covers covered in the Venn diagram sense.  For instance
   covers_clause(graph, True_Context, False_Context) returns  1. */

Clause *uncovered_clause(Graph *graph, Clause *clause, Clause *cover);
/* Return the parts of clause that are not covered by cover. */

/* Return False_Context if the clause is nogood and otherwise clause. */
static XLE_INLINE Clause *pruned_clause(Graph *graph, Clause *clause) {
  if (!graph->prune || !graph->compstate->prune) return clause;
  if (graph->nogood) return False_Context;
  if (!real_clause(clause)) return clause;
  if (clause->nogood) return False_Context;
  while (clause->type == OR && clause->body.compound.item->nogood) {
    clause = clause->body.compound.next;
    if (clause == True_Context) return True_Context;
    if (clause->nogood) return False_Context;
  }
  if (clause->type == OR && clause->body.compound.next->nogood) {
    clause = clause->body.compound.item;
    if (clause->nogood) return False_Context;
  }
  if (!clause->pruned) return clause;
  clause = clause->pruned;
  if (clause == True_Context) return clause;
  if (clause->nogood) return False_Context;
  return clause;
}


Clause *prune_clause(Graph *graph, Clause *clause, int limited);
/* Return a new version of clause which has had all of the nogoods in the
   local database subtracted from it. If limited is true, then only use a
   limited number of nogoods. */

Clause *simplify_clause(Graph *graph, Clause *clause, int limited);
/* Return a new version of clause which has had all of the nogoods in the
   local database added to it (e.g. disjoined with it). If limited is true,
   then only use a limited number of nogoods. */

void propagate_nogood(Graph *graph, Clause *nogood);
/* Subtract nogood from all of the clauses in graph. */

Clause *subtract_nogood(Graph *graph, Clause *clause, Clause *nogood, 
			int simplify);
/* Called by the procedures above. */

void mark_in_nogood(Clause *clause);

Clause *resolve_clause(Graph *graph, Clause *clause);
/* Resolve the clause with relevant nogoods. */

void precompute_resolve_relations(Clause *clause);
/* Precompute resolve relations when exporting a clause. */

extern int chart_clauses;
Clause *conjoin_chart_clauses(Graph *graph, Clause *c1, Clause *c2, 
			      int subtract, int test);
Clause *disjoin_chart_clauses(Graph *graph, Clause *c1, Clause *c2);
Clause *disjoin_clause_trees(Graph *graph, Clause *c1, Clause *c2,
			     ClauseStack *stack);
void add_dependent_to_choices(Disjunction *dependent, Clause *context);


#endif
