/* (c) 2002-2004 by the Palo Alto Research Center.  All rights reserved */
/* (c) 1996-2001 by the Xerox Corporation.  All rights reserved */
/* trees.h defines display tree data structures */

#ifndef TREES_H
#define TREES_H

/* a DTree is a Displayed Tree Node */
/* it has layout information as well as structural information */

typedef enum DTreeProps 
                {DTREE_NUMBERS = 1, /* show the node numbers */
		 DTREE_PARTIALS = 2, /* show partials */
		 DTREE_ATTRIBUTES = 4,  /* show the attributes that */
					/* contains each  node */
		 DTREE_TOKENS = 8} DTreeProps;     /* show tokens */

/* ------------------------------------------------------ */
/* When the user selects a solution to a particular tree, */
/* the <tree, solution> pair is stored on the edge        */
/* using the TreeSelection data structure.                */
/* ------------------------------------------------------ */
struct TreeSelection {
  DTree *tree;
  RestrictedSolution *selection;
  TreeSelection *next;
};

struct DTree {
  Edge *edge;  /* the edge in the chart that this is a tree for */
  SubTree *subtree;  /* the subtree chosen from that edge */
  DTree *mother;  /* this makes navigation easier */
  DTree *partial;  /* all DTrees are binary branching, */
  DTree *complete; /* though the display may be n-ary */
  int xpos, ypos; /* layout information */
  char *label; /* the label displayed */
  RestrictedSolution *solutions; /* the packed solutions to this tree. */
  EdgeList *surface_corr;  /* The surface correlations for this tree. */
  DTree *surface;  /* The surface form for this tree */
  unsigned int internal:1; /* this node is an internal partial (not
			      displayed) */
  unsigned int show_internals:1; /* Show internal structure. */
  unsigned int surface_checked:1; /* the tree has been checked to see if it
				     has a surface form. */
  unsigned int invalid:1; /* the tree has no solutions. */
  unsigned int dummy:1;   /* the tree is a dummy tree used to display
			     solutions from a prolog term that has been
			     read in. */
  unsigned int all:1;     /* All solutions have been computed. */
  unsigned int has_selection:1; /* the tree has a selection somewhere */
};

struct DTreeList {
  DTree *item;
  SubTree *subtree;
  InternalSolution *solution;
  DTreeList *next;
};

#define GOODTREES "GoodTrees"
#define TREES "TREES"

/* these procedures are exported to TCL */

void print_tree_as_sexp(FILE *file, DTree *tree, int depth);

int count_trees(Edge*, int);
int tree_id(DTree *tree);
DTreeList *get_good_trees(Edge *edge);
RestrictedSolution *get_tree_solutions(DTree *tree, int all);
int tree_has_solutions(DTree *tree);
int box_tree_p(DTree *tree);
DTree *get_next_tree(DTree *tree, Edge *edge, int prev, int goodOnly);
DTree* get_next_subtree(DTree* node, Edge *edge, int prev);
DTree *get_next_display_tree(DTree *tree, Edge *edge, int prev);

Graph *get_next_fstructure(DTree *tree, Graph *fstructure, int prev);
RestrictedSolution *first_dnf_tree_solution(RestrictedSolution *solutions, 
					    DTree *tree, int prev);
RestrictedSolution *next_dnf_tree_solution(RestrictedSolution *solution,
					   DTree *tree, int prev);
RestrictedSolution *chosen_solution(RestrictedSolution *solutions);
int count_tree_solutions(DTree *tree, int all);

void toggle_fs_selection(DTree *tree, Graph *fstructure);
RestrictedSolution *get_tree_selection(DTree *tree);

DTree* get_sample_tree(Edge*, RestrictedSolution*, DTree*);
void mark_alternatives(DTree*);
void check_tree(DTree *node);
DTree *surface_form(DTree *node);
DTree* new_tree(Edge *head);
DTree* copy_tree(DTree *tree);
void set_show_internals(DTree *tree, int);
DTree *find_tree(DTree *pattern, Edge *edge, int surface);
void mark_tree_validity(DTree *tree);
void find_spanning_edges(DTree *leftnode, DTree *rightnode);
int equal_trees(DTree *tree1, DTree *tree2);
DTree *tree_root(DTree *tree);
void clear_selections(DTree *tree);
int tree_has_restricted_solution(DTree *tree, RestrictedSolution *rs);
int tree_has_internal_solution(DTree *tree, InternalSolution *internal);
DTree *sample_subtree(Edge *edge, SubTree *subtree);
void clear_tree_node_solutions(DTree *node);
int pruned_tree(DTree *tree);

#endif



