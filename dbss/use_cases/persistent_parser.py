#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import getopt
import codecs
import os
import sys

from dbss.config import *
from dbss.core.sentence import *
from dbss.core.parsability_table import *
from dbss.core.lfg_parser import LfgParser
import dbss.core.simplifier as simplifier


## A wrapper around a constituency parser.
##
## Given an <input_file> with sentences in conll09 format, tries to parse
## a sentence. If no full parse is obtained, simplifies the sentence and tries
## again until either:
##  a) finds one solution (--max_solutions 1)
##  b) finds up to certain number of solutions (--max_solutions <number here>)
##  c) finds all possible solutions (default)
## or runs out of simplification options
##
## For each sentence i in <input_file>, creates a file named i.conll09 in
## <output_directory>, and writes successfully parsed versions of original
## sentence i to it.
##
## For different simplification and sorting modes, see test_* files in
## acceptance-tests/T_* directories.
##
USAGE = 'persistent_parser.py -i <input_file>\n' \
        '                            -c <candidates_directory>\n' \
        '                            -p <parses_directory>\n' \
        '                            -o <output_directory>\n' \
        '                            -m <simplification_mode>\n' \
        '                           [-s <sorting_mode>]\n' \
        '                           [--max_candidates <max nbr of candidates>]\n' \
        '                           [--max_solutions <max nbr of solutions>]\n' \
        'ORDER OF OPTIONS IS IMPORTANT.'
## UNIT TESTS: py.test persistent_parser.py (py.test should be installed)
## ACCEPTANCE_TESTS: bash acceptance-tests/run_all.sh (also see README)


## =================
## Functions:


# Templates blended:
#  - breadth-first search (see e.g. http://jeapostrophe.github.io/2013-04-01-dfs-in-r-post.html)
#  - over an arbitrary-arity tree
#  - with child nodes of a node being generated (generative recursion, using lazy evaluation)


## Sentence DirectoryName DirectoryName String String -> (generator Sentence)
def parse(s, candidates_dir, parses_dir, simplification_mode, sorting_mode, max_nbr_of_candidates):
    """From the set comprising original sentence and its simplified versions,
    generate "sentences" for which ParGram can give a full (=non-fragmented) parse tree.

    (See MODE2GENERATOR_RECURSIVE_LOCAL_GLOBAL_TAKE_N and SORTING_MODE2LOCAL_GLOBAL
    dictionaries at the end of the simplifier.py for possible values for
    the simplification_mode and sorting_mode arguments.)

    Leave trace in the 'candidates_dir' directory:
    - store original sentence and simplified candidates explored in <sentence_id_here>.conll09 file
    - store all simplified candidates generated (if using the constituency_parser.parse_batch
      for parsing) in <sentence_id_here>.lfg

    MUTATE: - candidates_dir/<sentence_id>.conll09 (in utf8)
            - candidates_dir/<sentence_id>.lfg, .lfg.new, .lfg.errors and .lfg.stats (in ISO-8859-1)
            - parses_dir/<sentence_id>/S<number>.pl (one for each candidate)
    """
    conll_file = candidates_dir + get_id(s) + CONLL_EXTENSION
    if not os.path.exists(os.path.dirname(conll_file)):
        os.makedirs(os.path.dirname(conll_file))
    lfg_file = candidates_dir + get_id(s) + LFG_EXTENSION
    if not os.path.exists(os.path.dirname(lfg_file)):
        os.makedirs(os.path.dirname(lfg_file))

    with open(conll_file, "w") as cf, codecs.open(lfg_file, 'w', 'ISO-8859-1') as lf:
        cf.write(s2conll09(s) + "\n")
        lf.write(s2string(s) + "\n" + "\n")

        print('Sentence ' + get_id(s) + '...')
        if parsed(*next(LFG_PARSER.parse([s], '/tmp/orig/', '/tmp/origs_parse/'))):
            print('ORIGINAL PARSED')
            yield s

        n = 1
        for simpler_s, exit_status in \
                LFG_PARSER.parse(simplifier.simplify(s,
                                                                    simplification_mode,
                                                                    sorting_mode,
                                                                    max_nbr_of_candidates),
                                                candidates_dir, parses_dir):
            print('    Candidate ' + str(n) + '...')
            cf.write(s2conll09(simpler_s) + "\n")
            lf.write(s2string(simpler_s) + "\n" + "\n")

            if parsed(simpler_s, exit_status):
                print('PARSED')
                yield simpler_s
            n += 1

def test_parse():
    assert list(map(s2string, parse(S_0, DEFAULT_CANDIDATES_DIR, DEFAULT_XLE_PARSES_DIR,
                                    'OneTokenShorterRecursiveBFSearch', 'DoNotSort',
                                    DEFAULT_MAX_NUMBER_OF_CANDIDATES))) == []
    assert list(map(s2string, parse(S_2, DEFAULT_CANDIDATES_DIR, DEFAULT_XLE_PARSES_DIR,
                                    'OneTokenShorterRecursiveBFSearch', 'DoNotSort',
                                    DEFAULT_MAX_NUMBER_OF_CANDIDATES))) == \
           ["Ich sehe ihn .", "Ich sehe .", "Ich sehe ihn", "ihn .", "Ich .", "Ich sehe", "ihn", "Ich"]
    assert list(map(s2string, parse(S_3, DEFAULT_CANDIDATES_DIR, DEFAULT_XLE_PARSES_DIR,
                                    'OneTokenShorterRecursiveBFSearch', 'DoNotSort',
                                    DEFAULT_MAX_NUMBER_OF_CANDIDATES))) == \
           ["Ich sehe .", "ihm .", "Ich .", "Ich sehe", "ihm", "Ich"]
    assert list(map(s2string, parse(S_4, DEFAULT_CANDIDATES_DIR, DEFAULT_XLE_PARSES_DIR,
                                    'OneTokenShorterRecursiveBFSearch', 'DoNotSort',
                                    DEFAULT_MAX_NUMBER_OF_CANDIDATES))) == []
    assert list(map(s2string, parse(S_3, DEFAULT_CANDIDATES_DIR, DEFAULT_XLE_PARSES_DIR,
                                    'OneTokenShorterRecursiveBFSearch', 'DoNotSort',
                                    4))) == \
           ["Ich sehe ."]


## ---------------------------
## Goal test


## Sentence Boolean -> Boolean
def parsed(s, parsers_exit_status):
    """Return True if parser has parsed the sentence successfully, False otherwise."""
    return parsers_exit_status

def test_parsed():
    assert parsed(S_0, False) == False
    assert parsed(S_1, True) == True


## =================
## Runner


def runner(argv):
    max_nbr_of_solutions = DEFAULT_MAX_NUMBER_OF_SOLUTIONS
    max_nbr_of_candidates = DEFAULT_MAX_NUMBER_OF_CANDIDATES
    sorting_mode = None
    try:
        opts, args = getopt.getopt(argv, 'hi:c:p:o:m:s:', ['help', 'in_file=', 'candid_dir=', 'parses_dir=',
                                                           'output_dir=', 'mode=', 'sort=', 'max_candidates=',
                                                           'max_solutions='])
    except getopt.GetoptError as err:
        print(err)
        print('USAGE: ' + USAGE)
        sys.exit(2)
    for opt, arg in opts:
        if opt in ('-h', '--help'):
            print('USAGE: ' + USAGE)
            sys.exit()
        elif opt in ('-i', '--in_file'):
            input_file = arg
        elif opt in ('-c', '--candid_dir'):
            candidates_directory = arg
        elif opt in ('-p', '--parses_dir'):
            parses_directory = arg
        elif opt in ('-o', '--output_dir'):
            output_directory = arg
        elif opt in ('-m', '--mode'):
            simplification_mode = arg
        elif opt in ('-s', '--sort'):
            sorting_mode, parsabilities_file = arg.split('=')
            if parsabilities_file:
                simplifier.PARSABILITY = read_parsabilities(parsabilities_file)
        elif opt == '--max_candidates':
            max_nbr_of_candidates = int(arg)
        elif opt == '--max_solutions':
            max_nbr_of_solutions = int(arg)

    with open(input_file) as inf:
        for s in read_sentences(inf):
            output_file = output_directory + get_id(s) + CONLL_EXTENSION
            if not os.path.exists(os.path.dirname(output_file)):
                os.makedirs(os.path.dirname(output_file))

            with open(output_file, 'w') as outf:
                for parsed_s in simplifier.take_n(max_nbr_of_solutions,  # TODO: take_n has nothing to do with the
                                                                         # sentence simplification
                                                           parse(s,
                                                                 candidates_directory,
                                                                 parses_directory,
                                                                 simplification_mode,
                                                                 sorting_mode,
                                                                 max_nbr_of_candidates)):
                    outf.write(s2conll09(parsed_s) + '\n')

if __name__ == '__main__':
    runner(sys.argv[1:])
