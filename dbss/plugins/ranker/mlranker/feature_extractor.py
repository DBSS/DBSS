#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from nltk import bigrams, trigrams

from dbss.core import token, sentence

## Turn (original sentence, simplified variant, actions taken to simplify)
## triple into a FeatureSet.


## =================
## Data definitions:


## FeatureSet is a Dictionary which maps Strings to Strings / Booleans /
##     Integers / Floats.
## interp. Representation of a sentence as a feature vector.
##     Name of a feature is mapped to its value.
## In feature names (and in function names), letters mean the following:
##     'h'    - head
##     'd'    - dependent
##     'type' - either h or d
##     'w'    - word (surface form)
##     'l'    - lemma
##     'p'    - POS tag
##     'f'    - morphological features
##     'dir'  - direction of the edge (from head), either 'left' or 'right'
##     'lbl'  - edge label


## ==========
## Constants:


BOS = '<s>'    ## beginning of a sentence
EOS = '</s>'   ## end of a sentence


## ==========
## Functions:


## Sentence Sentence (listof Strings) -> FeatureSet
def extract_features(original, simplified, actions):
    """Represent (original sentence, simplified variant, actions taken
    to simplify) triple as a feature set.
    """
    return {**length_of_candidate(simplified),
            **word_onegrams(simplified),
            **word_bigrams(simplified),
            **word_trigrams(simplified),
            **pos_onegrams(simplified),
            **pos_bigrams(simplified),
            **pos_trigrams(simplified),
            **type_w(original, simplified),
            **type_p(original, simplified),
            **type_w_p(original, simplified),
            **hwp_dwp_dir(original, simplified),
            **type_w(original, simplified),
            **lbl_hf_df(original, simplified)}

def test_extract_features():
    assert len(extract_features(sentence.S_7, sentence.S_7, [])) == 39


## --------------------------
## 'Non-dependency' features:


## Sentence -> FeatureSet
def length_of_candidate(s):
    """The length of the simplified sentence in terms of tokens."""
    return {'length': len(s)}

def test_length_of_candidate():
    assert length_of_candidate(sentence.S_7) == {'length': 3}


## Sentence -> FeatureSet
def word_onegrams(s):
    """Surface form one-grams."""
    return {('w', t.form): True for t in s}

def test_word_onegrams():
    assert word_onegrams(sentence.S_7) == {('w', 'Ich'): True,
                                           ('w', 'lese'): True,
                                           ('w', '.'): True}


## Sentence -> FeatureSet
def word_bigrams(s):
    """Surface form bigrams."""
    return {('ww', bigram): True for bigram in bigrams([BOS] +
                                                       [t.form for t in s] +
                                                       [EOS])}

def test_word_bigrams():
    assert word_bigrams(sentence.S_7) == {('ww', (BOS, 'Ich')): True,
                                          ('ww', ('Ich', 'lese')): True,
                                          ('ww', ('lese', '.')): True,
                                          ('ww', ('.', EOS)): True}


## Sentence -> FeatureSet
def word_trigrams(s):
    """Surface form trigrams."""
    return {('www', trigram): True for trigram in
            trigrams([BOS] + [t.form for t in s] + [EOS])}

def test_word_trigrams():
    assert word_trigrams(sentence.S_7) == {('www', (BOS, 'Ich', 'lese')): True,
                                           ('www', ('Ich', 'lese', '.')): True,
                                           ('www', ('lese', '.', EOS)): True}


## Sentence -> FeatureSet
def pos_onegrams(s):
    """Port-of-speech tag one-grams."""
    return {('p', t.pos): True for t in s}

def test_pos_onegrams():
    assert pos_onegrams(sentence.S_7) == {('p', 'PPER'): True,
                                          ('p', 'VVFIN'): True,
                                          ('p', '$.'): True}


## Sentence -> FeatureSet
def pos_bigrams(s):
    """Port-of-speech tag bigrams."""
    return {('pp', bigram): True for bigram in bigrams([BOS] +
                                                       [t.pos for t in s] +
                                                       [EOS])}

def test_pos_bigrams():
    assert pos_bigrams(sentence.S_7) == {('pp', (BOS, 'PPER')): True,
                                         ('pp', ('PPER', 'VVFIN')): True,
                                         ('pp', ('VVFIN', '$.')): True,
                                         ('pp', ('$.', EOS)): True}


## Sentence -> FeatureSet
def pos_trigrams(s):
    """Port-of-speech tag trigrams."""
    return {('ppp', trigram): True for trigram in trigrams([BOS] +
                                                           [t.pos for t in s] +
                                                           [EOS])}

def test_pos_trigrams():
    assert pos_trigrams(sentence.S_7) == \
           {('ppp', (BOS, 'PPER', 'VVFIN')): True,
            ('ppp', ('PPER', 'VVFIN', '$.')): True,
            ('ppp', ('VVFIN', '$.', EOS)): True}


## ----------------------------
## Unigram dependency features:


## See McDonald, R., Crammer, K., & Pereira, F. (2005, June).
## Online large-margin training of dependency parsers.
## In Proceedings of the 43rd annual meeting on association for computational
## linguistics (pp. 91-98). Association for Computational Linguistics.


## Sentence Sentence -> FeatureSet
def type_w(original, simplified):
    """Type along with surface form."""
    feats = {}
    for t in simplified:
        feats[('dw', t.form)] = True
        if t.head != "0":
            head_index = int(t.head) - 1
            feats[('hw', original[head_index].form)] = True
    return feats

def test_type_w():
    assert type_w(sentence.S_7, sentence.S_7) == {('dw', 'Ich'): True,
                                                  ('dw', 'lese'): True,
                                                  ('dw', '.'): True,
                                                  ('hw', 'lese'): True}


## Sentence Sentence -> FeatureSet
def type_p(original, simplified):
    """Type along with POS tag."""
    feats = {}
    for t in simplified:
        feats[('dp', t.pos)] = True
        if t.head != "0":
            head_index = int(t.head) - 1
            feats[('hp', original[head_index].pos)] = True
    return feats

def test_type_p():
    assert type_p(sentence.S_7, sentence.S_7) == {('dp', 'PPER'): True,
                                                  ('dp', 'VVFIN'): True,
                                                  ('dp', '$.'): True,
                                                  ('hp', 'VVFIN'): True}


## Sentence Sentence -> FeatureSet
def type_w_p(original, simplified):
    """Type along both surface form and POS tag."""
    feats = {}
    for t in simplified:
        feats[('dwp', t.form, t.pos)] = True
        if t.head != "0":
            head_index = int(t.head) - 1
            feats[('hwp', original[head_index].form,
                   original[head_index].pos)] = True
    return feats

def test_type_w_p():
    assert type_w_p(sentence.S_7, sentence.S_7) == \
           {('dwp', 'Ich', 'PPER'): True,
            ('dwp', 'lese', 'VVFIN'): True,
            ('dwp', '.', '$.'): True,
            ('hwp', 'lese', 'VVFIN'): True}


## ---------------------------
## Bigram dependency features:


'''
p-pos, c-word, c-pos : DONE -> hwp_dwp_dir
p-word, c-word, c-pos
p-word, p-pos, c-pos
p-word, p-pos, c-word
p-word, c-word
p-pos, c-pos
'''


## Sentence Sentence -> FeatureSet
def hwp_dwp_dir(original, simplified):
    """Surface form and POS tag of head, surface form and pos of dependent,
    direction from head to dependent.
    """

    def direction(head_index, dependent_index):
        if head_index < dependent_index:
            return 'right'
        else:
            return 'left'

    feats = {}
    for t in simplified:
        self_index = int(token.get_id(t.id)) - 1
        head_index = int(t.head) - 1
        if t.head != "0":
            feats[('hwp_dwp_dir', (original[head_index].form,
                                   original[head_index].pos,
                                   original[self_index].form,
                                   original[self_index].pos,
                                   direction(head_index, self_index)))] = True
        else:
            feats[('hwp_dwp_dir', ('ROOT',
                                   'ROOT',
                                   original[self_index].form,
                                   original[self_index].pos,
                                   direction(head_index, self_index)))] = True
    return feats

def test_hwp_dwp_dir():
    assert hwp_dwp_dir(sentence.S_7, sentence.S_7) == \
           {('hwp_dwp_dir', ('lese', 'VVFIN', 'Ich', 'PPER', 'left')): True,
            ('hwp_dwp_dir', ('ROOT', 'ROOT', 'lese', 'VVFIN', 'right')): True,
            ('hwp_dwp_dir', ('lese', 'VVFIN', '.', '$.', 'right')): True}


## The next one is not from McDonald et al. 2005, but should be useful
## to learn to identify (dis)agreement issues in a sentence.


## Sentence Sentence -> FeatureSet
def lbl_hf_df(original, simplified):
    """Edge label, (morphological) features of head and dependent."""
    feats = {}
    for t in simplified:
        self_index = int(token.get_id(t.id)) - 1
        head_index = int(t.head) - 1
        if t.head != "0":
            feats[('lbl_hf_df', (original[self_index].deprel,
                                 original[head_index].feat,
                                 original[self_index].feat))] = True
        else:
            feats[('lbl_hf_df', (original[self_index].deprel,
                                 'ROOT',
                                 original[self_index].feat))] = True
    return feats

def test_lbl_hf_df():
    assert lbl_hf_df(sentence.S_7, sentence.S_7) == \
           {('lbl_hf_df', ('SB', 'sg|1|pres|ind', 'nom|sg|*|1')): True,
            ('lbl_hf_df', ('--', 'ROOT', 'sg|1|pres|ind')): True,
            ('lbl_hf_df', ('--', 'sg|1|pres|ind', '_')): True}
