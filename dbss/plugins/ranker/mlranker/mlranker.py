#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pickle

from dbss.core.ranker import Ranker
from dbss.plugins.ranker.mlranker import feature_extractor

## Implements Ranker abstract class from dbss/core/ranker.py.
## Outputs the probability the classifier gives for the 'Parsed' class.


class MLRanker(Ranker):

    def __init__(self, classifier):
        with open(classifier, 'rb') as pf:
            self.classifier = pickle.load(pf)

    ## (listof/generator (Sentence, Sentence, (listof String))) -> (generator (Sentence, Sentence, Float))
    def score(self, input):
        for original, simplified, actions in input:
            yield original, \
                  simplified, \
                  self.classifier.prob_classify(feature_extractor.extract_features(original, simplified, actions)).prob('Parsed')
