#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
sys.path.append('/usr/lib/python2.7/site-packages/') # TODO: ask pexpect for python3 to be installed on ims
import pexpect
import re
from collections import OrderedDict
import os
import codecs

from dbss.core import sentence
from dbss.core.lfg_parser import LfgParser

## Implements LfgParser abstract class from dbss/core/lfg_parser.py.
##
## Given a list of sentences, calls XLE and returns (Sentence, Boolean) tuples,
## where Boolean indicates whether the sentence was successfully parsed or not.
##
## InteractiveXleParser does that using XLE's 'parse' command (i.e. interactively),
## while BatchmodeXleParser first writes all given sentences into a file and then
## tries to parse all of them using XLE's 'parse-testfile' command.
##
## UNIT TESTS: py.test-3 xle.py OR python3 -m pytest xle.py
##             (py.test has to be installed)


## =================
## Constants:


## Commands for starting the XLE parser and parsing a sentence in
## interactive mode
PARSER = pexpect.spawnu('xle -noTk -f xlerc', encoding='iso-8859-1')
PARSER_PROMT = u'% '
PARSER.expect(PARSER_PROMT)


## How long python (pexpect, to be exact) will wait for an answer from XLE:
## * when using the 'parse' command
INTERACTIVE_MODE_TIMEOUT = 1000000  # seconds. Read: only timeout set in xlerc has an effect.
BATCH_MODE_TIMEOUT = 1000000  # seconds. Read: only timeout set in xlerc has an effect.

## Directories to write to when parsing test sentences
CANDIDATES_DIR = '/tmp/Candidates'
XLE_PARSES_DIR = '/tmp/XLEParses'
PARSED_SENTENCES_DIR = '/tmp/ParsedSentences'

## Other
LFG_EXTENSION = '.lfg'


## ======================================
## Interactive (via the 'parse' command):


class InteractiveXleParser(LfgParser):

    ## (listof/generator Sentence) DirectoryName DirectoryName -> (generator (Sentence, Boolean))
    def parse(sentences, candidates_dir, xle_parses_dir):  # TODO: is there -outputPrefix option for 'parse'?
        """Parse sentences one by one and yield (Sentence, Boolean) tuples where
        boolean indicates whether the sentence was parsed successfully or not.

        ASSUME: - xle expects strings to be in iso-8859-1
        """
        for s in sentences:
            if not s:
                yield s, False
            else:
                yield s, xle_gives_complete_parse(sentence.s2string(s))

    ## (listof/generator String) DirectoryName DirectoryName -> (generator (String, Boolean))
    def parse_plain(sentences, candidates_dir, xle_parses_dir):  # TODO: is there -outputPrefix option for 'parse'?
        """Parse sentences one by one and yield (String, Boolean) tuples where
        boolean indicates whether the sentence was parsed successfully or not.

        ASSUME: - xle expects strings to be in iso-8859-1
        """
        for s in sentences:
            if not s:
                yield s, False
            else:
                yield s, xle_gives_complete_parse(s)

def test_parse_interactive():
    assert list(InteractiveXleParser.parse([sentence.S_0,
                                            sentence.S_1,
                                            sentence.S_2,
                                            sentence.S_3,
                                            sentence.S_4], CANDIDATES_DIR, XLE_PARSES_DIR)) == \
           [(sentence.S_0, False), (sentence.S_1, True), (sentence.S_2, True),
            (sentence.S_3, False), (sentence.S_4, False)]


## String -> Boolean
def xle_gives_complete_parse(string):
    """Return True if ParGram parser returns a complete (=non-fragmented) parse tree for string, False otherwise.
    ASSUME: - string is not empty
            - output of xle's "parse" command to stderr is of the following format:
                % parse "Ich sehe ihm ."
                parsing {Ich sehe ihm .}
                *2+26 solutions, 0.060 CPU seconds, 6.640MB max mem, 352 subtrees unified
                *2+26
            - completely parsed sentences are sentences for which xle gives more than 0 solutions
              but without preceding '*' or '-' characters:
                *2+26  # not completely parsed (no solutions)
                -6     # not completely parsed
                1+1    # completely parsed
                2      # completely parsed
                0      # not completely parsed (xle on ims computers will sometimes give this)
            - xle expects strings in iso-8859-1
    """

    ## Void -> String
    def xle_parse():
        """Return number of solutions xle gives for the string."""
        global PARSER

        try:
            PARSER.sendline('parse "' + string + '"')
            PARSER.expect(PARSER_PROMT, timeout=INTERACTIVE_MODE_TIMEOUT)
            return re.search('\n(\S+) solutions', PARSER.before).group(1)
        except pexpect.TIMEOUT:
            print("Timeout in interactive mode.")
            PARSER = pexpect.spawnu('xle -noTk -f xlerc', encoding='iso-8859-1')
            PARSER.expect(PARSER_PROMT)
            return '0'
        except AttributeError:  # TODO: investigate: AttributeError is raised on sentences like 30886 containing only one punctuation character, why?
            #       Looks like xle won't give a number of solutions for them.
            #print("Probably the sentence contains only one token which is a punctuation symbol.")
            PARSER = pexpect.spawnu('xle -noTk -f xlerc', encoding='iso-8859-1')
            PARSER.expect(PARSER_PROMT)
            return '0'

    return good_nbr_of_solutions(xle_parse())

def test_xle_gives_complete_parse():
    assert xle_gives_complete_parse(sentence.s2string(sentence.S_1)) == True
    assert xle_gives_complete_parse(sentence.s2string(sentence.S_2)) == True
    assert xle_gives_complete_parse(sentence.s2string(sentence.S_3)) == False
    assert xle_gives_complete_parse(sentence.s2string(sentence.S_4)) == False


## ==============================================
## Batch mode (via the 'parse-testfile' command):


class BatchmodeXleParser(LfgParser):

    ## (listof/generator Sentence) DirectoryName DirectoryName -> (generator (Sentence, Boolean))
    def parse(self, sentences, candidates_dir, xle_parses_dir):  # TODO: is there -outputPrefix option for 'parse'?
        """Parse simplified versions of a sentence in batch mode and yield (Sentence, Boolean)
        tuples where boolean indicates whether the sentence was parsed successfully or not.
        ASSUME: - all sentences in the given list have the same id
                - xle expects files to be in iso-8859-1

        Leave trace in the 'candidates_dir' (in .new, .errors and .stats files) and in
        'xle_parses_dir' (in .pl parse trees) directories.

        This is presumably faster than parse_interactive.
        """

        ## convert Sentences to Strings and map Strings to Sentences for easy inverse conversion
        d_of_s = OrderedDict((sentence.s2string(s), [s]) for s in sentences)
        try:
            d_of_s[''] = [(), False]
        except KeyError:
            pass

        ## no need to write to a file if input was a base case (read: empty) sentence
        if d_of_s == OrderedDict([('', [[], False])]):
            yield [], False
            raise StopIteration

        ## name an .lfg file where sentences will be written by the id of the first sentence
        first_s = list(d_of_s.values())[0][0]
        lfg_file = candidates_dir + sentence.get_id(first_s) + LFG_EXTENSION
        if not os.path.exists(os.path.dirname(lfg_file)):
            os.makedirs(os.path.dirname(lfg_file))

        ## set up a subdirectory to store parse trees
        parses_sub_dir = xle_parses_dir + sentence.get_id(first_s) + '/'
        if not os.path.exists(os.path.dirname(parses_sub_dir)):
            os.makedirs(os.path.dirname(parses_sub_dir))

        write_candidates_to_file(d_of_s, lfg_file)
        xle_parse_testfile(lfg_file, parses_sub_dir)

        ## find successful parses
        with codecs.open(lfg_file, 'r', 'ISO-8859-1') as original_lfg, \
                codecs.open(lfg_file + '.new', 'r', 'ISO-8859-1') as dot_new_lfg:
            for original, new in zip(original_lfg, dot_new_lfg):
                if original != "\n":
                    if good_nbr_of_solutions(get_nbr_of_solutions(new)):
                        d_of_s[original.rstrip()].append(True)
                    else:
                        d_of_s[original.rstrip()].append(False)

        for k, v in d_of_s.items():
            yield v[0], v[1]

def test_parse_batch():
    parser = BatchmodeXleParser()
    assert list(parser.parse([sentence.S_0,
                             sentence.S_1,
                             sentence.S_2,
                             sentence.S_3,
                             sentence.S_4], CANDIDATES_DIR, XLE_PARSES_DIR)) == \
           [(sentence.S_0, False), (sentence.S_1, True), (sentence.S_2, True),
            (sentence.S_3, False), (sentence.S_4, False)]


## (listof String) FileName -> Void
def write_candidates_to_file(sentences, f):
    """Write sentences into the file (in the format and encoding suitable for xle)."""
    with codecs.open(f, 'w', 'ISO-8859-1') as lfg_file:
        for simpler_s in sentences:
            if simpler_s:
                lfg_file.write(simpler_s + "\n" + "\n")


## FileName DirectoryName -> Void
def xle_parse_testfile(f, xle_parses_dir):
    """Parse sentences in the f(ile) using XLE's parse-testfile command, write
    parse trees into the xle_parses_dir directory.
    ASSUME: - both f and xle_parses_dir exist
    MUTATE: - f.new, f.errors and f.stats in the directory where the f(ile) is located.
    """
    global PARSER

    try:
        PARSER.sendline('parse-testfile ' + f + ' -outputPrefix ' + xle_parses_dir)
        PARSER.expect(PARSER_PROMT, timeout=BATCH_MODE_TIMEOUT)
    except pexpect.TIMEOUT:
        print('Timeout in batch mode')
        PARSER = pexpect.spawnu('xle -noTk -f xlerc', encoding='iso-8859-1')
        PARSER.expect(PARSER_PROMT)

def test_parse_testfile(tmpdir):
    in_file = tmpdir.join('input.txt')
    in_file.write("Ich sehe .\n")
    xle_parse_testfile(str(in_file), XLE_PARSES_DIR)
    new_file = tmpdir.join('input.txt.new')
    assert "Ich sehe . (2 " in new_file.read()


## ========
## Helpers:


## String -> String
def get_nbr_of_solutions(line):
    """Return number of solutions xle gave for a line in .lfg.new file."""
    return re.search('\((\S+) (\S+) (\S+)\)', line).group(1)

def test_get_nbr_of_solutions():
    assert get_nbr_of_solutions('sehe ihm . (*1+3 0.036 63)') == '*1+3'
    assert get_nbr_of_solutions('Ich ihm . (*1+15 0.030 107)') == '*1+15'
    assert get_nbr_of_solutions('Ich sehe . (2 0.034 65)') == '2'
    assert get_nbr_of_solutions('Ich sehe ihm (*2+12 0.052 346)') == '*2+12'


## String -> Boolean
def good_nbr_of_solutions(nbr_of_solutions):
    """Return True if number of solutions returned by xle indicates that a complete parse tree was found."""
    return not nbr_of_solutions[0] in ['*', '-', '0']

def test_good_nbr_of_solutions_in_dot_new():
    assert good_nbr_of_solutions('0') == False
    assert good_nbr_of_solutions('2') == True
    assert good_nbr_of_solutions('*4+1866') == False
    assert good_nbr_of_solutions('-1') == False
    assert good_nbr_of_solutions('*24+7.6066333E+10') == False
