from dbss.plugins.lfg_parser import xle
from dbss.plugins.ranker import bylengthranker
from dbss.plugins.ranker.mlranker import mlranker

## Configurations relevant for all other modules.


## Selecting an implementation from dbss.plugins for the abstract
## classes/interfaces in dbbs.core.
LFG_PARSER = xle.InteractiveXleParser
RANKER = mlranker.MLRanker('dbss/plugins/ranker/mlranker/models/me_classifier3.pickle')


## =================
## Constants:


## Generated candidates, parsed sentences, XLE output and other
## logs will be stored in subfolders of this
#BASE_DIR = "/home/selimcan/1Working/2DependencyBasedTextSimplification/"
#BASE_DIR = "/mount/arbeitsdaten12/users/"
BASE_DIR = "/vagrant/"


## Where generated candidates, XLE's parses (.pl files) and parsed sentences
## will be stored by default
DEFAULT_CANDIDATES_DIR = BASE_DIR + "salimzir/Candidates/"
DEFAULT_XLE_PARSES_DIR = BASE_DIR + "salimzir/XLEParses/"
DEFAULT_PARSED_SENTENCES_DIR = BASE_DIR + "salimzir/ParsedSentences/"


## Extensions to use when naming intermediary or output files with sentences
CONLL_EXTENSION = ".conll09"  # sentences as conll dependency trees
LFG_EXTENSION = ".lfg"  # sentences as strings


## Followed [Cetinoglu et al. (2013)]
DELETABLE_SUBTREES = ['AG', 'APP', 'DA', 'JU', 'MNR', 'MO', 'NG', 'PAR', 'PG',
                      'PH', 'PNC', 'RC', 'RE', 'SBP', 'UC', 'VO', 'NK']
# NOTE: punctuation (labelled as '--') could be treated as the head of
## a deletable subtree (containing only punctuation itself) as well,
## which means that in recursive modes, all possible combinations
# of them will be considered for deletion


## Maximum number of candidates to consider,
## number of solutions (=fully parsed sentences received) to stop at
DEFAULT_MAX_NUMBER_OF_CANDIDATES = 1000000000  # read: take all of them
DEFAULT_MAX_NUMBER_OF_SOLUTIONS = 1000000000  # read: take all of them
