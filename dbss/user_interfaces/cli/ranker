#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import json
from io import StringIO

from dbss import config
from dbss.core import token, sentence

# INPUT:
# {'Original sentence here': ['Simplified Candidate 1',
#                              ['action1', ..., 'actionN']]}
# {'Same original sentence here': ['Simplified Candidate 2',
#                                   ['action1', ..., 'actionM']]}

def provide_input():  ## or, rather, simulate for now, untill simplifier's output is adjusted
    for line in sys.stdin:
        if line.strip():
            for orig, simplified in json.loads(line.strip()).items():
                yield next(sentence.read_sentences(StringIO(orig))), [token.Token(*t) for t in simplified], []

for orig, simplified, score in config.RANKER.score(provide_input()):
    print(json.dumps({sentence.s2string(orig): (sentence.s2string(simplified), score)},
                     ensure_ascii=False))

# OUTPUT:
# {'Original sentence here': ['Simplified Candidate 1', 0.1234]}
# {'Same original sentence here': ['Simplified Candidate 2', 0.5678]}
