#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import json
from operator import itemgetter

from dbss.core import token, sentence
from dbss.plugins.ranker.mlranker import mlranker

## INPUT: a tsv file of the form
## X\tY
## where X is the id of the original sentence, and
##       Y is a space-separated list of deleted *token* id's.
## E.g.:
## 2\t5 6
##
## ASSUME: id field of the conll09 file  with original sentences (ORIGINAL_SENTS_CONLL09)
##         is of the form: number_number, where the first number is the sentence id,
##         the second number is the token id proper.

ORIGINAL_SENTS_CONLL09 = '../DBSS_DATA/experiments/input/tiger_train_easyPunct_xleFail.conll09'
CLASSIFIER = sys.argv[1]

with open(ORIGINAL_SENTS_CONLL09) as osf:
    origs = {sentence.get_id(s): s for s in sentence.read_sentences(osf)}

RANKER = mlranker.MLRanker(CLASSIFIER)

def provide_input():
    for line in sys.stdin:
        if line.strip():
            x, y = line.split('\t')
            original = origs[x]
            to_delete = set(y.split())
            simplified = [t for t in original if token.get_id(t.id) not in to_delete]
            yield original, simplified, []

result = []
for original, simplified, score in RANKER.score(provide_input()):
    result.append((sentence.s2string(simplified), score))

try:
    print(json.dumps({sentence.s2string(original): sorted(result, key=itemgetter(1), reverse=True)},
                      ensure_ascii=False))
except NameError:
    pass

## OUTPUT:
## {'Original sentence here': ['Simplified Candidate 1', 0.1234]}
## {'Same original sentence here': ['Simplified Candidate 2', 0.5678]}
